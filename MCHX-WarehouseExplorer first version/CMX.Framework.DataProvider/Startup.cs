﻿using Owin;
using System.Net.Http.Headers;
using System.Web.Http;

namespace CMX.Framework.DataProvider
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {            
            HttpConfiguration config = new HttpConfiguration();
            config.EnableCors();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);
        }
    } 
}