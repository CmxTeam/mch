﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class IdNameEntity : IdEntity
    {
        public string Name { get; set; }
    }
}