﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class Partner : IdNameEntity
    {        
        public string Address { get; set; }
        public string City { get; set; }
        public string Postal { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
    }
}