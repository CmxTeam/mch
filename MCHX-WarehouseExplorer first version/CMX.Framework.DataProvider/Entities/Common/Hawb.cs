﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class Hawb : OriginDestinationEntity
    {
        public int TotalPieces { get; set; }
        public int TotalSlac { get; set; }
        public string TotalWeight { get; set; }
        public string GoodsDescription { get; set; }
        public Partner Shipper { get; set; }
        public Partner Consignee { get; set; }
    }
}