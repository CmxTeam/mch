﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class OriginDestinationEntity : IdNameEntity
    {
        public Port Origin { get; set; }
        public Port Destination { get; set; }
    }
}