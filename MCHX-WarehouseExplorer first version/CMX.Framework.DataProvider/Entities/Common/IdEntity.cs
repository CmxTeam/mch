﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class IdEntity
    {
        public long Id { get; set; }
    }
}