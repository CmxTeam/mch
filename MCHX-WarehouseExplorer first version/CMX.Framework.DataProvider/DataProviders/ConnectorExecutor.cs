﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.DataProviders
{
    public class ConnectorExecutor
    {
        public T Execute<T>(Func<IDatabaseConnector<T>> action)
        {
            using (var instance = action())
            {
                return instance.Execute();
            }            
        }        
    }
}
