﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.DataProviders.Common.Units
{
    public class PortsCreator : DatabaseConnector<IEnumerable<Port>>
    {
        public override string CommandQuery
        {
            get { return @"SELECT Id, IATACode FROM Ports Order By IATACode"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override IEnumerable<Port> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(r => new Port
            {
                Id = Convert.ToInt64(r["Id"]),
                Name = r["IATACode"].ToString()
            });
        }
    }
}
