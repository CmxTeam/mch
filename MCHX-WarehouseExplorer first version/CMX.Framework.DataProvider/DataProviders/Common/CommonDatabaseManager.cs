﻿using CMX.Framework.DataProvider.DataProviders.Common.Units;
using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders.Common
{
    public class CommonDatabaseManager
    {
        public IEnumerable<Port> GetPorts()
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute<IEnumerable<Port>>(() => new PortsCreator());
        }
    }
}