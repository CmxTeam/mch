﻿using System;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.DataProviders.LiveScreening.Units
{
    public class GetHawbInfoUnit: DatabaseConnector<Hawb>
    {
        private readonly string _hawbNumber;
        private readonly string _connectionString;

        public GetHawbInfoUnit(string connectionString, string hawbNumber)
        {
            _hawbNumber = hawbNumber;
            _connectionString = connectionString;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select top 1 RecId, AirbillNo, SenderName as Shipper, ReceiverName as Consignee, Origin, Destination,
                        NoOfPackages as TotalPieces, isnull(Slac, 0) as TotalSlac,
                        cast(GrossWeight as nvarchar(10)) + ' ' + WeightUom as TotalWeight,
                        Description as GoodsDescription
                        from Hawb 
                        Where AirbillNo = @AirbillNo
                        Order By RecDate Desc";
            }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }

        public override Hawb ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var r = rows[0];

            return new Hawb
            {
                Id = Convert.ToInt64(r["RecId"]),
                Name = r["AirbillNo"].ToString(),
                Shipper = new Partner { Name = r["Shipper"].ToString() },
                Consignee = new Partner { Name = r["Consignee"].ToString() },
                Origin = new Port { Name = r["Origin"].ToString() },
                Destination = new Port { Name = r["Destination"].ToString() },
                TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                TotalSlac = Convert.ToInt32(r["TotalSlac"]),
                TotalWeight = r["TotalWeight"].ToString(),
                GoodsDescription = r["GoodsDescription"].ToString()
            };
        }

        public override IEnumerable<CommandParameter> Parameters
        {
            get
            {
                return new List<CommandParameter> { new CommandParameter("@AirbillNo", _hawbNumber) };
            }
        }
    }
}
