﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.DataProviders.LiveScreening.Units
{
    public class ScreeningQueueCreator : DatabaseConnector<long>
    {
        private readonly int _slac;
        private readonly long _userId;
        private readonly string _userName;
        private readonly long _deviceId;        
        private readonly string _hawbNumber;
        private readonly string _connectionString;

        public ScreeningQueueCreator(string connectionString, string hawbNumber, long deviceId, long userId, string userName, int slac)
        {
            _slac = slac;
            _userId = userId;
            _userName = userName;
            _deviceId = deviceId;
            _hawbNumber = hawbNumber;
            _connectionString = connectionString;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Insert Into fsp.ScreeningQueue (Reference, DeviceId, UserId, UserName, Timestamp, StatusId, Slac)
                        Values (@HawbNumber, @DeviceId, @UserId, @UserName, getutcdate(), 500, @Slac)
                        Select Scope_Identity() as Id";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override long ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0)
                throw new Exception("No ScreeningQueue has been created");
            return Convert.ToInt64(rows[0][0]);
        }

        public override IEnumerable<CommandParameter> Parameters
        {
            get
            {
                return new List<CommandParameter> 
                { 
                    new CommandParameter("@HawbNumber", _hawbNumber),
                    new CommandParameter("@DeviceId", _deviceId),
                    new CommandParameter("@UserId", _userId),
                    new CommandParameter("@UserName", _userName),
                    new CommandParameter("@Slac", _slac) 
                };
            }
        }
    }
}
