﻿using System.Collections.Generic;
using System.Configuration;

namespace CMX.Framework.DataProvider.DataProviders.LiveScreening.Units
{
    public class GetLocalDatabaseConnection : DatabaseConnector<string>
    {
        private readonly long _location;        

        public GetLocalDatabaseConnection(long location)
        {
            _location = location;            
        }

        public override string CommandQuery
        {
            get { return @"Select top 1 Connection From HostPlus_locations where RecId = @Location"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override string ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0) return null;
            return rows[0][0].ToString();
        }

        public override IEnumerable<CommandParameter> Parameters
        {
            get
            {
                return new List<CommandParameter> { new CommandParameter("@Location", _location) };
            }
        }
    }
}
