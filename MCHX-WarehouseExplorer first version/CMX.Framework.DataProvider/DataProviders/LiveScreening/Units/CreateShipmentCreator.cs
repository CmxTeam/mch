﻿using System.Linq;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders.LiveScreening.Units
{
    public class CreateShipmentCreator : DatabaseConnector<string>
    {
        private List<CommandParameter> _parameters;
        private string _connectionString;
        public CreateShipmentCreator(string connectionString, long userId, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            _connectionString = connectionString;
            _parameters = new List<CommandParameter> 
                {                     
                    new CommandParameter("@UserId", userId),
                    new CommandParameter("@LocationId", locationId),
                    new CommandParameter("@Origin", origin),
                    new CommandParameter("@Destination", destination),
                    new CommandParameter("@AirbillNo", number),
                    new CommandParameter("@NoOfPackages", pieces),
                    new CommandParameter("@Slac", slac)
                };
        }

        public override string CommandQuery
        {
            get { return "MCHNew_CreateHawb"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }

        public override string ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.First()[0].ToString();
        }

        public override IEnumerable<CommandParameter> Parameters
        {
            get { return _parameters; }            
        }
    }
}
