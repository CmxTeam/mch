﻿using CMX.Framework.DataProvider.DataProviders.LiveScreening.Units;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.DataProviders.LiveScreening
{
    public class LiveScreeningDatabaseManager
    {             
        public Hawb TryGetHawb(long locationId, string number, long deviceId, long userId, string userName, int slac)
        {
            var _connectorExecutor = new ConnectorExecutor();

            var localConnection = _connectorExecutor.Execute<string>(() => new GetLocalDatabaseConnection(locationId));
            var hawb = _connectorExecutor.Execute<Hawb>(() => new GetHawbInfoUnit(localConnection, number));

            if (hawb != null)
                _connectorExecutor.Execute<long>(() => new ScreeningQueueCreator(localConnection, number, deviceId, userId, userName, slac));

            return hawb;
        }

        public string CreateShipment(long userId, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            var _connectorExecutor = new ConnectorExecutor();
            var localConnection = _connectorExecutor.Execute<string>(() => new GetLocalDatabaseConnection(locationId));
            return _connectorExecutor.Execute<string>(() => new CreateShipmentCreator(localConnection, userId, locationId, number, origin, destination, pieces, slac));
        }

        public void CreateScreeningQueue()
        { 
        
        }
    }
}
