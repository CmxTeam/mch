﻿using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders
{
    public abstract class DatabaseConnector<T> : IDisposable, IDatabaseConnector<T>
    {
        //private SqlCommand _command;
        private SqlDataAdapter _adapter;
        private SqlConnection _connection;

        private System.Data.CommandType _sqlCommandType
        {
            get { return CommandType == CommandType.Text ? System.Data.CommandType.Text : System.Data.CommandType.StoredProcedure; }
        }

        public abstract string CommandQuery { get; }
        public abstract string ConnectionString { get; }

        public abstract T ObjectInitializer(List<DataRow> rows);
        public virtual CommandType CommandType
        {
            get { return CommandType.Text; }
        }

        public virtual IEnumerable<CommandParameter> Parameters { get { return null; } }

        public virtual T Execute()
        {
            var table = new DataTable();
            _connection = new SqlConnection(ConnectionString);
            _adapter = new SqlDataAdapter(CommandQuery, _connection);
            _adapter.SelectCommand.CommandType = _sqlCommandType;

            if (Parameters != null)
            {
                foreach (var param in Parameters)
                    _adapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, param.Value));
            }

            _adapter.Fill(table);
            return ObjectInitializer(table.Rows.OfType<DataRow>().ToList());
        }

        #region [ -- IDisposable -- ]

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~DatabaseConnector()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_adapter != null)
                _adapter.Dispose();

            //if (_command != null)
            //    _command.Dispose();

            if (_connection != null)
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _connection.Close();
                _connection.Dispose();
            }
        }

        #endregion
    }
}
