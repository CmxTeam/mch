﻿namespace CMX.Framework.DataProvider.DataProviders
{
    public class CommandParameter
    {
        public CommandParameter(string key, object value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; private set; }
        public object Value { get; private set; }
    }
}