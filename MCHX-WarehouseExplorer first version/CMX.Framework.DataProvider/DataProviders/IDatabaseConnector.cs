﻿using System;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders
{
    public interface IDatabaseConnector<T> : IDisposable
    {
        T Execute();
    }
}