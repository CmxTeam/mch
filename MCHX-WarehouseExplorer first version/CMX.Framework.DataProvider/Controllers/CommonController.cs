﻿using System.Web.Http;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.DataProviders.Common;

namespace CMX.Framework.DataProvider.Controllers
{
    public class CommonController : ApiController
    {
        public IEnumerable<Port> GetPorts()
        {
            var manager = new CommonDatabaseManager();
            return manager.GetPorts();
        }
    }
}
