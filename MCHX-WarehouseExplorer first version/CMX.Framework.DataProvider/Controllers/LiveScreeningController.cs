﻿using CMX.Framework.DataProvider.DataProviders.LiveScreening;
using CMX.Framework.DataProvider.Entities.Common;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CMX.Framework.DataProvider.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LiveScreeningController : ApiController
    {
        [HttpGet]
        public Hawb TryFindHawb(long locationId, string number, long deviceId, long userId, string userName, int slac)
        {
            var manager = new LiveScreeningDatabaseManager();
            return manager.TryGetHawb(locationId, number, deviceId, userId, userName, slac);
        }

        [HttpGet]
        public string CreateShipment(long userId, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            var manager = new LiveScreeningDatabaseManager();
            return manager.CreateShipment(userId, locationId, number, origin, destination, pieces, slac);
        }
    }
}
