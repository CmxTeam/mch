﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WarehouseExplorer.Startup))]
namespace WarehouseExplorer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
