﻿var inventoryGrid = function (cell, onRowDoubleClick) {
    this.onRowDoubleClick = onRowDoubleClick;
    this.warehouseId = null;
    this.onActionClicked = null;

    this.show = function () {
        cell.showView('inventory');
    }

    this.filter = function (warehouseId, inventoryDate, run) {
        this.warehouseId = warehouseId;
        cell.progressOn();
        this.grid.clearAll();        
        this.grid.load('../api/Common/GetWharehouseInventoryView?warehouseId=' + warehouseId +
        '&inventoryDate=' + inventoryDate + '&taskId=' + run,
        function () { cell.progressOff(); }, 'json');

    }

    init = function () {
        cell.showView('inventory');
        cell.hideHeader();
        var toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar><item type="text" id="button_text_1" text="QUICK FINDER:" title="" /><item type="buttonInput" id="button_input_1" width="225" title="" /><item type="separator" id="button_separator_1" /><item type="buttonSelect" id="button_select_export" text="EXPORT" title="" ><item type="button" id="button_select_option_1" text="Excel" image="" /><item type="button" id="button_select_option_3" text="CSV" image="" /><item type="button" id="button_select_option_2" text="PDF" image="" /></item><item type="separator" id="button_separator_4" /><item type="buttonSelect" id="button_select_4" text="Actions" title="" ><item type="button" id="enterAcceptanceButton" text="Enter Acceptance" image="" /></item></toolbar>', function () { });

        toolbar.attachEvent('onClick', function (name) {
            if (this.onActionClicked)
                this.onActionClicked(name);
        }.bind(this));

        this.grid = cell.attachGrid();
        this.grid.setIconsPath(dhtmlx.image_path);
        this.grid.setHeader(["Carrier", "Shipment#", "Origin", "Dest", "Pieces", "Weight", "First Scan", "Inventory Date", "Overage", "Shotage", "Damages", "Left Behind", "Type"]);
        this.grid.setInitWidths('*,100,*,*,*,*,150,150,*,*,*,*,*');
        this.grid.setEditable(false);
        this.grid.init();
        this.grid.setColumnHidden(12, true);
        this.grid.enableSmartRendering(true);

        this.grid.attachEvent("onRowSelect", function (rId, cInd) {
            var type = this.grid.cells(this.grid.getSelectedId(), 12).getValue();
            if (this.onRowDoubleClick) {
                this.onRowDoubleClick(rId, type, this.warehouseId);
            }
        }.bind(this));        
    }.call(this);

    return this;
};