﻿var detailsView = function (cell) {

    this.filter = function (parentId, entityType, taskId, gridType, wahrehouseId) {
        cell.progressOn();
        this.locationsGrid.clearAll();
        this.locationsGrid.load('../api/Common/GetLocationDetails?parentId=' + parentId +
            '&entityType=' + entityType + '&taskId=' + taskId + '&gridType=' + gridType,
        function () { cell.progressOff(); }, 'json');

        this.historyGrid.clearAll();
        this.historyGrid.load('../api/Common/GetHistory?parentId=' + parentId +
            '&entityType=' + entityType + '&warehouseId=' + wahrehouseId + '&gridType=' + gridType,
        function () { cell.progressOff(); }, 'json');

        var $this = this;

        this.detailsForm.load('../api/Common/GetDetails?parentId=' + parentId + '&warehouseId=' + wahrehouseId + '&entityType=' + entityType + '&gridType=' + gridType, function () {
            var formData = this.getFormData();
            $this.detailsForm.setItemLabel('carrier', formData.CarrierCode);
            $this.detailsForm.setItemLabel('shippingReference', formData.ShipmentReference);
            $this.detailsForm.setItemLabel('origin', formData.Origin);
            $this.detailsForm.setItemLabel('dest', formData.Destination);
            $this.detailsForm.setItemLabel('pieces', formData.TotalPieces);
            $this.detailsForm.setItemLabel('weight', formData.TotalWeight);            
            $this.detailsForm.setItemLabel('shipper', formData.Shipper.Name + '</br> ' +
                                                      formData.Shipper.Address + ', ' + formData.Shipper.Postal + '</br> ' +
                                                      (!!formData.Shipper.State ? formData.Shipper.State + ', ' : '') +
                                                      (!!formData.Shipper.City ? formData.Shipper.City + ', ' : '') +
                                                      formData.Shipper.Country);
            
            $this.detailsForm.setItemLabel('consignee', formData.Consignee.Name + '</br> ' +
                                                     formData.Consignee.Address + ', ' + formData.Consignee.Postal + '</br> ' +
                                                     (!!formData.Consignee.State ? formData.Consignee.State + ', ' : '') +
                                                     (!!formData.Consignee.City ? formData.Consignee.City + ', ' : '') +
                                                     formData.Consignee.Country);
            $this.detailsForm.setItemLabel('handlingCodes', formData.HandlingCodes);            
            $this.detailsForm.setItemLabel('overpack', formData.Overpack);
            
            $this.media.progressOn();
            $this.viewData.clearAll();
            dhx4.ajax.get('../api/Common/GetMediaDetails?parentId=' + parentId +
                '&entityType=' + entityType + '&warehouseId=' + wahrehouseId + '&gridType=' + gridType, function (a) {
                    $this.viewData.clearAll();
                    $this.viewData.parse(a.xmlDoc.response || a.xmlDoc.responseText, "json");
                    $this.media.progressOff();
                });            
        });
    }

    this.clear = function () {
        this.locationsGrid.clearAll();
        this.historyGrid.clearAll();
        this.detailsForm.setItemLabel('carrier', '');
        this.detailsForm.setItemLabel('shippingReference', '');
        this.detailsForm.setItemLabel('origin', '');
        this.detailsForm.setItemLabel('dest', '');
        this.detailsForm.setItemLabel('pieces', '');
        this.detailsForm.setItemLabel('weight', '');
        this.detailsForm.setItemLabel('shipper', '');
        this.detailsForm.setItemLabel('consignee', '');
        this.detailsForm.setItemLabel('handlingCodes', '');
        this.detailsForm.setItemLabel('overpack', '');
        this.viewData.clearAll();
    }    

    init = function () {
        var tabbar = cell.attachTabbar();

        tabbar.addTab('details', 'Details');
        var detailsTab = tabbar.cells('details');

        tabbar.addTab('locations', 'Locations');
        var locations = tabbar.cells('locations');

        tabbar.addTab('history', 'History');
        var history = tabbar.cells('history');

        tabbar.addTab('media', 'Media');
        this.media = tabbar.cells('media');

        var formData = [
		{
		    type: "block", list: [
            { type: "label", label: "Carrier:", labelHeight: 14 },
            { type: "label", label: "Shipping Reference:", labelHeight: 14 },
            { type: "label", label: "Origin:", labelHeight: 14 },
            { type: "label", label: "Dest:", labelHeight: 14 },
            { type: "label", label: "Pieces:", labelHeight: 14 },
            { type: "label", label: "Weight:", labelHeight: 14 },            
		    ]
		},
		{ type: "newcolumn" },
		{
		    type: "block", width: 130, className: 'detailsContainer', list: [
            { type: "label", name: "carrier", labelHeight: 14 },
            { type: "label", name: "shippingReference", labelHeight: 14 },
            { type: "label", name: "origin", labelHeight: 14 },
            { type: "label", name: "dest", labelHeight: 14 },
            { type: "label", name: "pieces", labelHeight: 14 },
            { type: "label", name: "weight", labelHeight: 14 }
		    ]
		},
        { type: "newcolumn" },
        {
            type: "block", width: 280,  list: [
            { type: "label", label: "Shipper:", labelHeight: 5 },
            { type: "label", name: "shipper", labelHeight: 70, className: 'detailsContainer' },
            { type: "label", label: "Consignee:", labelHeight: 5 },
            { type: "label", name: "consignee", className: 'detailsContainer' }
            ]
        },				
        { type: "newcolumn" },
        {
            type: "block", list: [
                { type: "label", label: "Handling Codes:", labelHeight: 5 },
                { type: "label", name: "handlingCodes", className: 'detailsContainer', labelWidth: 220 },
                { type: "label", label: "Overpack:", labelHeight: 5 },
                { type: "label", name: "overpack", className: 'detailsContainer', labelWidth: 220 }
                
            ]
        }        
        ];
        this.detailsForm = detailsTab.attachForm(formData);

        this.locationsGrid = locations.attachGrid();
        this.locationsGrid.setIconsPath(dhtmlx.image_path);
        this.locationsGrid.setHeader(["Warehouse", "Zone", "Location", "Pieces", "User"]);
        this.locationsGrid.setEditable(false);
        this.locationsGrid.init();

        this.historyGrid = history.attachGrid();
        this.historyGrid.setIconsPath(dhtmlx.image_path);
        this.historyGrid.setHeader(["Description", "Timestamp", "User"]);
        this.historyGrid.setEditable(false);
        this.historyGrid.init();
        

        var mapItemTemplate = "<div style='height:135px'>" +				
					                "<img style='width:220px; max-height:100%;' src='#Image#' />" +
                                    "<div style='display: inline-block; float: right; max-width:240px'>" +
                                        "<div><span style='font-weight:bold'>Condition: </span> #Condition#</div>" +
                                        "<div><span style='font-weight:bold'>Pieces: </span> #Pieces#</div>" +
                                        "<div><span style='font-weight:bold'>Date: </span> #Date#</div>" +
                                        "<div><span style='font-weight:bold'>User: </span> #UserName#</div>" +
                                    "</div>" +
			                  "</div>";

        this.viewData = this.media.attachDataView({
            type: {
                template: mapItemTemplate,
                height: 135,
                width: 480
            }
        });

        detailsTab.setActive();

    }.call(this);

    return this;
};