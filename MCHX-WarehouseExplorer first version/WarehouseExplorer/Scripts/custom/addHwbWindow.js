﻿var addHwbWindow = function (onComplete) {
    var $this = this;
    this.onComplete = onComplete;

    this.show = function (awbId) {
        this.window.setModal(1);
        this.window.show();
        this.resertForm();
        this._awbId = awbId;
        if (this._awbId) {
            this.loadGrid();
        }
    }.bind(this);

    this.hide = function () {
        this.window.hide();
        this.window.setModal(0);
    }.bind(this);

    this.loadGrid = function () {
        this.window.progressOn();
        this.grid.clearAll();
        if (this._awbId) {
            this.grid.load('../api/Common/GetAwbHwbs?awbId=' + this._awbId, function () { this.window.progressOff(); }.bind(this), 'json');
        }
    },

    this.resertForm = function () {
        this.window.hwbForm.getCombo('originCombo').unSelectOption();
        this.window.hwbForm.getCombo('destinationCombo').unSelectOption();
        this.window.hwbForm.setItemValue('hwbNumberTextbox', '');
        this.window.hwbForm.setItemValue('piecesTextbox', '');
        this.window.hwbForm.setItemValue('shipperNameTextbox', '');
        this.window.hwbForm.setItemValue('consigneeNameTextbox', '');
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('addHwbWindow', 0, 0, 580, 417);

        var formStructure = [
            {
                type: 'block', list: [
                    { type: "combo", name: "originCombo", label: "Origin", position: "label-top", inputWidth: 100, labelWidth: 110, connector: '../api/Common/GetPorts' },
                    { type: "newcolumn" },
                    { type: "input", name: "hwbNumberTextbox", label: "HWB", position: "label-top", labelWidth: 200, inputWidth: 190 },
                    { type: "newcolumn" },
                    { type: "combo", name: "destinationCombo", label: "Destination", position: "label-top", inputWidth: 100, labelWidth: 110, connector: '../api/Common/GetPorts' },
                    { type: "newcolumn" },
                    { type: "input", name: "piecesTextbox", label: "Pieces", position: "label-top", inputWidth: 90 },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "shipperNameTextbox", label: "Shipper Name (optional)", position: "label-top", inputWidth: 200, labelWidth: 210 },
                    { type: "newcolumn" },
                    { type: "input", name: "consigneeNameTextbox", label: "Consignee Name (optional)", position: "label-top", inputWidth: 200 },
                    { type: "newcolumn" },
                    { type: "button", name: "addHwbButton", value: "Add HWB", offsetLeft: 10, offsetTop: 29 },
                ]
            },
            {
                type: 'block', list: [
                    { type: "container", name: "grid", inputWidth: 505, inputHeight: 200 },
                ]
            },
            {
                type: 'block', list: [
                    { type: "button", name: "completeButton", value: "COMPLETE", offsetLeft: 414 }
                ]
            },
        ];
        window.hwbForm = window.attachForm(formStructure);

        window.hwbForm.attachEvent('onButtonClick', function (name) {
            if (name == 'addHwbButton') {
                var values = this.getValues();
                if (!values.hwbNumberTextbox || !values.originCombo || !values.destinationCombo || !values.piecesTextbox) {
                    return;
                }

                dhx4.ajax.get('../api/Common/AddAwbHwb?awbId=' + $this._awbId +
                       '&hwbSerialNumber=' + values.hwbNumberTextbox + '&originId=' + values.originCombo + '&destinationId=' + values.destinationCombo
                       + '&totalPieces=' + values.piecesTextbox + '&shipperName=' + values.shipperNameTextbox + '&consigneeName=' + values.consigneeNameTextbox, function (a) {
                           $this.loadGrid();
                           $this.resertForm();
                       });
            } else if (name == 'completeButton') {
                $this.window.progressOn();
                dhx4.ajax.get('../api/Common/CompleteManualAccept?awbId=' + $this._awbId + '&userId=3&warehouseId=1', function () {
                    $this.window.progressOff();
                    $this.hide();

                    if ($this.onComplete) {
                        $this.onComplete();
                    }
                });
            }
        });

        var grid = new dhtmlXGridObject(window.hwbForm.getContainer("grid"));
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["HWB", "PCS", "SHIPPER", "CONSIGNEE"]);
        grid.setInitWidths('150,50,*,*');
        grid.setEditable(false);
        grid.init();

        this.grid = grid;

        window.setText('ENTER HWB DETAILS');
        window.hide();
        window.denyResize();
        window.centerOnScreen();
        window.button('minmax').show();
        window.button('minmax').enable();

        window.button('close').attachEvent('onClick', function () {
            window.setModal(0);
            window.hide();
        });

        this.window = window;
    }.apply(this);
};