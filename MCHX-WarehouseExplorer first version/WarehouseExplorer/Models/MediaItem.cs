﻿using System;

namespace WarehouseExplorer.Models
{
    public class MediaItem
    {
        public string Image { get; set; }
        public string Condition { get; set; }
        public int Pieces { get; set; }
        public string Date { get; set; }
        public string UserName { get; set; }        
    }
}