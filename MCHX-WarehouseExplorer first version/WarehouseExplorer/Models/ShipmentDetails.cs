﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WarehouseExplorer.Models
{
    public class ShipmentDetails
    {
        public string CarrierCode { get; set; }
        public string ShipmentReference { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public int TotalPieces { get; set; }
        public string TotalWeight { get; set; }
        public Partner Shipper { get; set; }
        public Partner Consignee { get; set; }
        public string HandlingCodes { get; set; }
        public string Overpack { get; set; }
    }

    public class Partner
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Postal { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
    }
}