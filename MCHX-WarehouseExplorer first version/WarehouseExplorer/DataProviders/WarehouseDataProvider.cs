﻿using DhtmlxComponents.DataProviders;

namespace WarehouseExplorer.DataProviders
{
    public class WarehouseDataProvider : BaseDataProvider, IDataProvider
    {
        public override string DatabaseName
        {
            get { return "WarehouseManagement"; }
        }
    }
}