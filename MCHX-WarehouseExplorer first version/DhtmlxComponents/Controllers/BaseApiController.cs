﻿using System.Web.Http;
using System.Linq;
using System.Collections.Generic;
using DhtmlxComponents.DataProviders;
using DhtmlxComponents.Widgets;

namespace DhtmlxComponents.Controllers
{
    public abstract class BaseApiController<T> : ApiController where T : IDataProvider, new()
    {
        public BaseApiController()
        {
            DataProvider = new T();
        }
        public Grid MakePageableGrid(string query, string primaryKeyColumnName, int? posStart, int count, int maxPageSize = 20, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var ps = new List<KeyValuePair<string, object>> 
                { 
                    new KeyValuePair<string, object>("@Start", posStart),
                    new KeyValuePair<string, object>("@End", count),
                };
            if (parameters != null)
                ps.AddRange(parameters);

            var source = DataProvider.GetGridSource(query, primaryKeyColumnName, ps);

            source.Count = source.Count > 100000 ? 100000 : source.Count;
            var position = posStart.GetValueOrDefault();

            if (position + maxPageSize > source.Count)
                maxPageSize = maxPageSize - position;

            var totalCount = posStart.GetValueOrDefault() > 0 ? string.Empty : source.Count.ToString();

            return new Grid
            {
                total_count = totalCount,
                pos = position,
                rows = source.Data.Select(d => new Row
                {
                    id = d.Id,
                    data = d.Cells
                })
            };
        }

        public IEnumerable<ValueText> GetComboData(string query, string idColumn, string textColumn, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            return DataProvider.Fill<IEnumerable<ValueText>>(query,
            rows => rows.Select(r => new ValueText
            {
                value = r[idColumn].ToString(),
                text = r[textColumn].ToString()
            }), parameters);
        }

        protected IDataProvider DataProvider { get; private set; }

        protected override void Dispose(bool disposing)
        {
            DataProvider.Dispose();
            base.Dispose(disposing);
        }
    }
}