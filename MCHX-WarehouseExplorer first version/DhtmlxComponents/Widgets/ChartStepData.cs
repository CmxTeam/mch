﻿using System.Collections.Generic;

namespace DhtmlxComponents.Widgets
{
    public class ChartStepData
    {
        public string id { get; set; }
        public string xValue { get; set; }
        public string yValue { get; set; }
    }
}