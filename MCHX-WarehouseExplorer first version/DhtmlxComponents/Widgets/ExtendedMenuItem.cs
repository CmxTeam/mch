﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DhtmlxComponents.Widgets
{
    public class ExtendedMenuItem<T> : MenuItem
    {
        private T _userdata;

        public T userdata
        {
            get { return _userdata; }
            set { _userdata = value; }
        }
    }
}
