﻿namespace DhtmlxComponents.DataProviders
{
    public enum CommandType
    {
        Text = 1,
        StoredProcedure = 2
    }
}