﻿using System;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using DhtmlxComponents.Widgets;
using DhtmlxComponents.DataProviders.Models;

namespace DhtmlxComponents.DataProviders
{
    public abstract class BaseDataProvider : IDisposable
    {
        private SqlCommand _command;
        private SqlDataAdapter _adapter;
        private SqlConnection _connection;
        private CommandType? _commandType;

        protected virtual string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings[DatabaseName].ConnectionString; }
        }

        public abstract string DatabaseName { get; }

        public CommandType CommandType
        {
            get { return _commandType.HasValue ? _commandType.Value : CommandType.Text; }
            set { _commandType = value; }
        }

        public virtual Source GetGridSource(string commandText, string primaryKeyColumnName, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var set = new DataSet();
            _connection = new SqlConnection(ConnectionString);
            _adapter = new SqlDataAdapter(commandText, _connection);
            _adapter.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
            if (parameters != null)
            {
                foreach (var param in parameters)
                    _adapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, param.Value));
            }

            _adapter.Fill(set);

            var columnsCount = set.Tables[0].Columns.Count;

            var data = set.Tables[0].Rows.OfType<DataRow>().Select(r =>
            {
                var list = new List<string>();
                for (var i = 0; i < columnsCount; ++i)
                    list.Add(r[i].ToString());

                return new KeyValuePair<string, IEnumerable<string>>(r[primaryKeyColumnName].ToString(), list);
            }).ToList();

            return new Source
            {
                Count = Convert.ToInt32(set.Tables[1].Rows[0][0]),
                Data = data.Select(d => new SourceItem
                {
                    Id = d.Key,
                    Cells = d.Value
                })
            };
        }

        public virtual T Fill<T>(string commandText, Func<List<DataRow>, T> objectInitializer, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var table = new DataTable();
            _connection = new SqlConnection(ConnectionString);
            _adapter = new SqlDataAdapter(commandText, _connection);
            _adapter.SelectCommand.CommandType = CommandType == CommandType.Text ? System.Data.CommandType.Text : System.Data.CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (var param in parameters)
                    _adapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, param.Value));
            }

            _adapter.Fill(table);
            return objectInitializer(table.Rows.OfType<DataRow>().ToList());
        }

        public virtual T FillSingleOrDefailt<T>(string commandText, Func<DataRow, T> objectInitializer, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var table = new DataTable();
            _connection = new SqlConnection(ConnectionString);
            _adapter = new SqlDataAdapter(commandText, _connection);
            _adapter.SelectCommand.CommandType = CommandType == CommandType.Text ? System.Data.CommandType.Text : System.Data.CommandType.StoredProcedure;
            if (parameters != null)
            {
                foreach (var param in parameters)
                    _adapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, param.Value));
            }
            _adapter.Fill(table);
            return objectInitializer(table.Rows.OfType<DataRow>().FirstOrDefault());
        }

        public IEnumerable<KeyValuePair<string, IEnumerable<string>>> GetDataAsString(string commandText, string primaryKeyColumnName, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var table = new DataTable();
            _connection = new SqlConnection(ConnectionString);
            _adapter = new SqlDataAdapter(commandText, _connection);
            _adapter.SelectCommand.CommandType = CommandType == CommandType.Text ? System.Data.CommandType.Text : System.Data.CommandType.StoredProcedure;
            if (parameters != null)
            {
                foreach (var param in parameters)
                    _adapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, param.Value));
            }

            _adapter.Fill(table);

            var columnsCount = table.Columns.Count;

            return table.Rows.OfType<DataRow>().Select(r =>
            {
                var list = new List<string>();
                for (var i = 0; i < columnsCount; ++i)
                    list.Add(r[i].ToString());

                return new KeyValuePair<string, IEnumerable<string>>(r[primaryKeyColumnName].ToString(), list);
            }).ToList();
        }

        public virtual object ExecuteScalar(string commandText, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            return ExecuteImpl<object>(commandText, command => command.ExecuteScalar(), parameters);
        }

        public virtual int ExecuteNonQuery(string commandText, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            return ExecuteImpl<int>(commandText, command => command.ExecuteNonQuery(), parameters);
        }

        private T ExecuteImpl<T>(string commandText, Func<SqlCommand, T> action, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            _connection = new SqlConnection(ConnectionString);
            _command = new SqlCommand(commandText, _connection);
            _command.CommandType = CommandType == CommandType.Text ? System.Data.CommandType.Text : System.Data.CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (var param in parameters)
                    _command.Parameters.Add(new SqlParameter(param.Key, param.Value));
            }

            if (_connection.State != System.Data.ConnectionState.Open)
                _connection.Open();

            return action(_command);
        }

        #region [ -- IDisposable -- ]

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~BaseDataProvider()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_adapter != null)
                _adapter.Dispose();

            if (_command != null)
                _command.Dispose();

            if (_connection != null)
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _connection.Close();
                _connection.Dispose();
            }
        }

        #endregion
    }
}