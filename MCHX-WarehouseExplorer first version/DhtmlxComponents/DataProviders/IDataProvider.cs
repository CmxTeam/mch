﻿using DhtmlxComponents.DataProviders.Models;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.Data;

namespace DhtmlxComponents.DataProviders
{
    public interface IDataProvider
    {
        CommandType CommandType { get; set; }
        Source GetGridSource(string commandText, string primaryKeyColumnName, IEnumerable<KeyValuePair<string, object>> parameters = null);
        T Fill<T>(string commandText, Func<List<DataRow>, T> objectInitializer, IEnumerable<KeyValuePair<string, object>> parameters = null);
        T FillSingleOrDefailt<T>(string commandText, Func<DataRow, T> objectInitializer, IEnumerable<KeyValuePair<string, object>> parameters = null);
        IEnumerable<KeyValuePair<string, IEnumerable<string>>> GetDataAsString(string commandText, string primaryKeyColumnName, IEnumerable<KeyValuePair<string, object>> parameters = null);        
        object ExecuteScalar(string commandText, IEnumerable<KeyValuePair<string, object>> parameters = null);
        int ExecuteNonQuery(string commandText, IEnumerable<KeyValuePair<string, object>> parameters = null);
        
        void Dispose();
    }
}
