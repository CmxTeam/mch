﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.Tester
{
    class Program
    {
        private static IDisposable _host;

        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:9000/";
            _host = WebApp.Start<Startup>(url: baseAddress);
            
            Console.ReadLine();
            _host.Dispose();
        }
    }
}
