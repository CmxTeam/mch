﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

namespace RapiscanBLL
{
    public class ScreeningProcessor : IDisposable
    {
        public ScreeningProcessor(string connectionString, string imageSaveFolder)
        {
            _connectionString = connectionString;
            _imageSaveFolder = imageSaveFolder;
        }

        public void ProcessRapiscanScreeningData()
        {
            const string screeningInsertQuery = @"INSERT INTO Fsp.Screening (RecDate, DeviceId,ResultCode,UserName,SampleNumber,SampleMethod,ScreeningDate,NumberOfPieces,HousebillNumber,IsProcessed,Remark,ScreeningMethodId,UserId) VALUES (getdate(),@DeviceId,@ResultCode,@UserName,@SampleNumber,@SampleMethod,@ScreeningDate,@NumberOfPieces,@HousebillNumber,@IsProcessed,@Remark, (select top 1 ScreeningMethodId from fsp.Devices where RecId = @DeviceId), @UserId); select Scope_Identity();";
            const string imgQuery = @"Select * from TransactionsUploadedImages where ReferenceId = @ReferenceId and ScanId = @ScanId";
            const string imgRectangleQuery = @"select R.X1, R.Y1, R.X2, R.Y2 from TransactionsScansRectangles R Inner Join TransctionScans S ON R.TransactionScanId = S.RecId Where S.ReferenceId = @ReferenceId and R.ScanId = @ScanId and ImageIndex = @ImageIndex";

            List<Transaction> transactions = GetNotProcessedTransactions();

            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Connection.Open();
                    foreach (var screenTrans in transactions)
                    {
                        //1. Add Db records.
                        command.CommandText = screeningInsertQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add(new SqlParameter("@DeviceId", screenTrans.MachineId));
                        command.Parameters.Add(new SqlParameter("@ResultCode", screenTrans.Clear ? "PASS" : "ALARM"));
                        command.Parameters.Add(new SqlParameter("@UserName", screenTrans.OperatorId));
                        command.Parameters.Add(new SqlParameter("@SampleNumber", screenTrans.ReferenceId));
                        command.Parameters.Add(new SqlParameter("@SampleMethod", "XRAY"));
                        command.Parameters.Add(new SqlParameter("@NumberOfPieces", 1));
                        command.Parameters.Add(new SqlParameter("@HousebillNumber", screenTrans.ReferenceName));
                        command.Parameters.Add(new SqlParameter("@IsProcessed", 0));
                        command.Parameters.Add(new SqlParameter("@Remark", screenTrans.ScanComments));
                        command.Parameters.Add(new SqlParameter("@UserId", screenTrans.UserId));

                        var screeningId = (long)command.ExecuteScalar();

                        command.CommandText = imgQuery;
                        command.Parameters.Clear();
                        command.Parameters.Add(new SqlParameter("@ReferenceId", screenTrans.ReferenceId));
                        command.Parameters.Add(new SqlParameter("@ScanId", screenTrans.ScanId));

                        var reader = command.ExecuteReader();

                        //2. Process images and save to Db.
                        var imgs = new Dictionary<int, Image>();

                        while (reader.Read())
                        {
                            byte[] image = (byte[])reader["Bytes"];
                            MemoryStream memstr = new MemoryStream(image);
                            Image img = Image.FromStream(memstr);

                            imgs.Add((int)reader["ImageIndex"], img);
                        }
                        reader.Close();

                        foreach (int k in imgs.Keys)
                        {
                            command.CommandText = imgRectangleQuery;
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@ReferenceId", screenTrans.ReferenceId));
                            command.Parameters.Add(new SqlParameter("@ScanId", screenTrans.ScanId));
                            command.Parameters.Add(new SqlParameter("@ImageIndex", k));

                            reader = command.ExecuteReader();
                            List<Rectangle> rectangles = new List<Rectangle>();
                            while (reader.Read())
                            {
                                rectangles.Add(new Rectangle()
                                               {
                                                   TopLeftX = (float)reader["X1"],
                                                   TopLeftY = (float)reader["Y1"],
                                                   BottomRightX = (float)reader["X2"],
                                                   BottomRightY = (float)reader["Y2"]
                                               });
                            }
                            reader.Close();

                            string filePath = ImageEditor.ProcessAndSaveImage(imgs[k], screenTrans.ReferenceId + "_" + k.ToString(),
                                _imageSaveFolder, rectangles);


                            command.CommandText = @"Insert into Fsp.ScreeningImages(RecDate,ScreeningId,ImagePath) values (getdate(),@screeningId,@imagePath)";
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@screeningId", screeningId));
                            command.Parameters.Add(new SqlParameter("@imagePath", filePath));
                        }

                        //3. Update status on Fsp.Queue.

                        command.CommandText = "UpdateFspQueueStatus";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.Add(new SqlParameter("@housebillNo", screenTrans.ReferenceName));
                    }
                }
            }
        }
        private List<Transaction> GetNotProcessedTransactions()
        {
            List<Transaction> retval = new List<Transaction>();
            const string refQuery =
                @"select T.*,TS.TransactionScanId,TS.Clear,TS.Comments, TS.CreatedDate as ScanDate from Transactions T inner join TransactionsScans TS on T.ReferenceId = TS.ReferenceId where exists (select top 1 1 from TransactionsUploadedImages TUI where TUI.ReferenceId = T.ReferenceId) and not exists (select top 1 1 from Fsp.Screening where SampleNumber = T.ReferenceId)";

            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(refQuery, connection))
                {
                    command.Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    command.Connection.Close();

                    while (reader.Read())
                    {
                        retval.Add(
                                    new Transaction()
                                        {
                                            ReferenceId = reader["ReferenceId"].ToString(),
                                            MachineId = (int)reader["ReferenceId"],
                                            AuthorizationToken = reader["AuthorizationToken"].ToString(),
                                            ScanId = reader["ScanId"].ToString(),
                                            OperatorId = reader["OperatorId"].ToString(),
                                            UserId = reader["ReferenceId"] == DBNull.Value ? null : (long?)reader["ReferenceId"],
                                            ReferenceName = reader["ReferenceName"].ToString(),
                                            Time = (DateTime)reader["Time"],
                                            CreatedDate = (DateTime)reader["CreatedDate"],
                                            Clear = (bool)reader["Clear"],
                                            ScanComments = reader["Comments"].ToString(),
                                            ScanDate = (DateTime)reader["ScanDate"]
                                        }
                                    );
                    }
                }
            }
            return retval;
        }

        private readonly string _connectionString;

        private readonly string _imageSaveFolder;

        #region IDisposable Implementation
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.

                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                _disposed = true;

            }
        }

        ~ScreeningProcessor()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion
    }

    internal class Transaction
    {
        public string ReferenceId { get; set; }
        public int MachineId { get; set; }
        public string AuthorizationToken { get; set; }
        public string ScanId { get; set; }
        public string OperatorId { get; set; }
        public long? UserId { get; set; }
        public string ReferenceName { get; set; }
        public DateTime Time { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Clear { get; set; }
        public string ScanComments { get; set; }
        public DateTime ScanDate { get; set; }

    }

}
