﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace RapiscanBLL
{
    public class ImageEditor
    {

        public static string ProcessAndSaveImage(string imagePath, string savePath, List<Rectangle> rectangles)
        {
            string ext = Path.GetExtension(imagePath);

            if (ext != null && ext.Trim(new[] { '.' }).ToLower() == "gif")
            {
                throw new NotSupportedException("GIF image format not supported.");
            }

            string flName = Path.GetFileNameWithoutExtension(imagePath);
            Image image = Image.FromFile(imagePath);

            rectangles.ForEach(
                r =>
                    AddRectangleToAnImage(image, r.TopLeftX, r.TopLeftY, (r.BottomRightX - r.TopLeftX),
                        (r.BottomRightY - r.TopLeftY)));

            string fileName = flName + "_" + DateTime.Now.ToString("yyMMddhhmmss") + ext;

            if (!Directory.Exists(savePath))
                Directory.CreateDirectory(savePath);

            string fileNameWithPath = Path.Combine(savePath, fileName);

            image.Save(fileNameWithPath);

            return fileNameWithPath;
        }

        public static string ProcessAndSaveImage(Image image, string reference, string savePath, List<Rectangle> rectangles)
        {
            rectangles.ForEach(
                r =>
                    AddRectangleToAnImage(image, r.TopLeftX, r.TopLeftY, (r.BottomRightX - r.TopLeftX),
                        (r.BottomRightY - r.TopLeftY)));

            string fileName = reference + "_" + DateTime.Now.ToString("yyMMddhhmmss") + ".png";

            if (!Directory.Exists(savePath))
                Directory.CreateDirectory(savePath);

            string fileNameWithPath = Path.Combine(savePath, fileName);

            image.Save(fileNameWithPath, ImageFormat.Png);

            return fileNameWithPath;
        }

        public static void AddRectangleToAnImage(Image img, float x, float y, float width, float height)
        {
            if (x + width > img.Width)
            {
                width = img.Width - x - 1;
            }

            if (y + height > img.Height)
            {
                height = img.Height - y - 1;
            }

            using (Graphics g = Graphics.FromImage(img))
            {
                Color customColor = Color.FromArgb(255, Color.Red);
                Pen shadowBrush = new Pen(customColor, 5);
                g.DrawRectangle(shadowBrush, x, y, width, height);
            }
        }
    }

    public struct Rectangle
    {
        public float TopLeftX { get; set; }
        public float TopLeftY { get; set; }
        public float BottomRightX { get; set; }
        public float BottomRightY { get; set; }

    }
}
