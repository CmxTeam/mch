﻿using CSN.DataProviders;
using DhtmlxComponents.Controllers;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace CSN.ApiControllers
{
    public class CommonController : BaseApiController<CsnBaseDataProvider>
    {
        public IEnumerable<ValueText> GetPeriods()
        {
            return new List<ValueText> 
            { 
                new ValueText { value = "1", text = "Today" },
                new ValueText { value = "2", text = "Yesterday" },
                new ValueText { value = "3", text = "Last 3 Days" },
                new ValueText { value = "7", text = "Last Week" }, 
                new ValueText { value = "30", text = "Last Month" },
                new ValueText { value = "90", text = "Last Quarter" } 
            };
        }


        public List<ChartStepData> GetScreeningVolumesChartData()
        {
            var data = new List<ChartStepData> {
            new ChartStepData {id = "1", xValue = "Lalala", yValue = "8000" },
            new ChartStepData {id = "2", xValue = "Ooror", yValue = "3450" },
            new ChartStepData {id = "3", xValue = "amanama", yValue = "6900" },
            };

            return data;
        }
        //public HttpResponseMessage GetScreeningVolumesChartData()
        //{
        //    var xml = "<data><item id=\"1\"><ccsf>ATL</ccsf><pieces>5000</pieces></item><item id=\"2\"><ccsf>MIA</ccsf><pieces>3500</pieces></item><item id=\"3\"><ccsf>LAX</ccsf><pieces>2500</pieces></item><item id=\"4\"><ccsf>ORD</ccsf><pieces>15000</pieces></item><item id=\"5\"><ccsf>JFK</ccsf><pieces>12000</pieces></item></data>";
        //    return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        //}

        public HttpResponseMessage GetHourlyProductionChartData()
        {
            var xml = "<data><item id=\"1\"><hour>00</hour><pieces>100</pieces></item><item id=\"2\"><hour>01</hour><pieces>500</pieces></item><item id=\"3\"><hour>02</hour><pieces>500</pieces></item><item id=\"4\"><hour>03</hour><pieces>500</pieces></item><item id=\"5\"><hour>04</hour><pieces>220</pieces></item><item id=\"6\"><hour>05</hour><pieces>30</pieces></item><item id=\"7\"><hour>06</hour><pieces>87</pieces></item><item id=\"8\"><hour>07</hour><pieces>450</pieces></item><item id=\"9\"><hour>08</hour><pieces>600</pieces></item><item id=\"10\"><hour>09</hour><pieces>1800</pieces></item><item id=\"11\"><hour>10</hour><pieces>600</pieces></item><item id=\"12\"><hour>11</hour><pieces>50</pieces></item><item id=\"13\"><hour>12</hour><pieces>234</pieces></item><item id=\"14\"><hour>13</hour><pieces>222</pieces></item><item id=\"15\"><hour>14</hour><pieces>800</pieces></item><item id=\"16\"><hour>15</hour><pieces>80</pieces></item><item id=\"17\"><hour>16</hour><pieces>20</pieces></item><item id=\"18\"><hour>17</hour><pieces>10</pieces></item><item id=\"19\"><hour>18</hour><pieces>5</pieces></item><item id=\"20\"><hour>19</hour><pieces>1</pieces></item><item id=\"21\"><hour>20</hour><pieces>2</pieces></item><item id=\"22\"><hour>21</hour><pieces>0</pieces></item><item id=\"23\"><hour>22</hour><pieces>1</pieces></item><item id=\"24\"><hour>23</hour><pieces>0</pieces></item></data>";
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        public HttpResponseMessage GetRescreenedVolumesChartData()
        {
            var xml = "<data><item id=\"1\"><ccsf>ATL</ccsf><pieces>5000</pieces></item><item id=\"2\"><ccsf>MIA</ccsf><pieces>3500</pieces></item><item id=\"3\"><ccsf>LAX</ccsf><pieces>2500</pieces></item><item id=\"4\"><ccsf>ORD</ccsf><pieces>15000</pieces></item><item id=\"5\"><ccsf>JFK</ccsf><pieces>12000</pieces></item></data>";
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        public HttpResponseMessage GetScreeningResultsChartData()
        {
            var xml = "<data><item id=\"1\"><result>PASS</result><pieces>500</pieces></item><item id=\"2\"><result>ALARM</result><pieces>85</pieces></item><item id=\"3\"><result>ALARM CLEARED</result><pieces>75</pieces></item></data>";
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        public HttpResponseMessage GetScreeningTechnologiesUtilizationChartData()
        {
            var xml = "<data><item id=\"1\"><method>ETD</method><percent>25</percent></item><item id=\"2\"><method>X-RAY</method><percent>60</percent></item><item id=\"3\"><method>PHYSICAL</method><percent>10</percent></item><item id=\"4\"><method>CANINE</method><percent>5</percent></item></data>";
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        public HttpResponseMessage GetChainOfFactoryChartData()
        {
            var xml = "<data><item id=\"1\"><seal>Verified</seal><airwaybills>500</airwaybills></item><item id=\"2\"><seal>Missing</seal><airwaybills>85</airwaybills></item><item id=\"3\"><seal>Broken</seal><airwaybills>60</airwaybills></item></data>";
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        public IEnumerable<MenuItem> GetMenu()
        {
            return new List<MenuItem> { new MenuItem
            {
                id="liveScreening",
                text = "LIVE SCREENING",
                type = "item",
            }};

            //return new List<MenuItem> { new MenuItem
            //{
            //    id="file",
            //    text = "File",
            //    type = "item",                
            //    items = new List<MenuItem>
            //    {
            //        new ExtendedMenuItem<TestData>
            //        {
            //            id="clickable",
            //            text = "Clickable",
            //            type = "item",
            //            img = "../../imgs_menu/new.gif",
            //            imgdis = "../../imgs_menu/new_dis.gif",
            //            userdata = new TestData{Name = "aram", SurnameName = "Test"}
            //        },
            //        new ExtendedMenuItem<TestData>
            //        {
            //            id="clickable1",
            //            text = "Clickable 1",
            //            type = "item",
            //            img = "../../imgs_menu/new.gif",
            //            imgdis = "../../imgs_menu/new_dis.gif",
            //            userdata = new TestData{Name = "test", SurnameName = "test"}
            //        },
            //        new MenuItem
            //        {
            //            id="new",
            //            text = "Open New Page",
            //            type = "item",
            //            img = "../../imgs_menu/new.gif",
            //            imgdis = "../../imgs_menu/new_dis.gif",
            //            href_link = "http://google.com",
            //            href_target = "blank_",                        
            //        },
            //        new MenuItem
            //        {
            //            id="asd",
            //            text = "Some Menu",
            //            type = MenuItem.MenuItemTypes.Checkbox,
            //            img = "../../imgs_menu/new.gif",
            //            imgdis = "../../imgs_menu/new_dis.gif"                                                
            //        }
            //    }
            //}};
        }
    }

    public class TestData
    {
        public string Name { get; set; }
        public string SurnameName { get; set; }
    }
}