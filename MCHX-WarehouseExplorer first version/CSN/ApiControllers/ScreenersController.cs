﻿using CSN.DataProviders;
using DhtmlxComponents.Controllers;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace CSN.ApiControllers
{
    public class ScreenersController : BaseApiController<CsnBaseDataProvider>
    {
        [HttpGet]
        public Grid GetGridData(string screener = "")
        {
            string filter = string.Empty;
            
            if (screener.EndsWith("_pp"))
                filter = string.Format("and HostPlus_locations.RecId = {0}", screener.Replace("_pp", ""));
            else if (screener.EndsWith("_p")) 
            {
                filter = screener.Replace("_p", "");
                filter = string.Format("and HostPlus_Locations.RecId = {0} and ScreeningMethods.RecId = {1}", filter.Split('_')[0], filter.Split('_')[1]);
            }
            else if (screener.EndsWith("_i"))
            {
                filter = screener.Replace("_i", "");
                filter = string.Format("and HostPlus_Locations.RecId = {0} and ScreeningMethods.RecId = {1} and Screeners.RecId = {2}", filter.Split('_')[0], filter.Split('_')[1], filter.Split('_')[2]);
            }            

            var query = string.Format(@"Select Screeners.RecId as Id, PhotoImage as Photo, Screeners.FirstName +  ' ' + Screeners.LastName as Name,
                            Location, Phone, Email, '' as CellPhone
                            from Screeners Inner join ScreenerCertifications on ScreenerCertifications.ScreenerId = Screeners.RecId
                            Inner join CertificationTypes on CertificationTypes.RecId = ScreenerCertifications.CertificationTypeId
                            inner join ScreeningMethods on ScreeningMethods.RecId = CertificationTypes.ScreeningMethodId
                            left join HostPlus_locations on HostPlus_locations.RecId = Screeners.LocationId
                            Where Screeners.IsActive = 1 and ScreenerCertifications.ValidTo >= getutcdate() {0}", filter);
            var data = DataProvider.GetDataAsString(query, "Id");

            return new Grid
            {
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        [HttpGet]
        public IEnumerable<ValueText> GetScreeners(int? locationId = 0, int? screeningMethodId = 0)
        {
            return DataProvider.Fill<IEnumerable<ValueText>>(string.Format(
                        @"Select Screeners.FirstName +  '' + Screeners.LastName as Screener, Screeners.RecId as ScreenerId
                            from Screeners Inner join ScreenerCertifications on ScreenerCertifications.ScreenerId = Screeners.RecId
                            Inner join CertificationTypes on CertificationTypes.RecId = ScreenerCertifications.CertificationTypeId
                            inner join ScreeningMethods on ScreeningMethods.RecId = CertificationTypes.ScreeningMethodId
                            Where Screeners.IsActive = 1 and ScreenerCertifications.ValidTo >= getutcdate() 
                            and ({0} = 0 or Screeners.LocationId = {0}) and ({1} = 0 or CertificationTypes.ScreeningMethodId = {1})
                            Order By Screener", locationId, screeningMethodId),
                rows => rows.Select(r => new ValueText
                {
                    value = r["ScreenerId"].ToString(),
                    text = r["Screener"].ToString()
                }));
        }

        [HttpGet]
        public HttpResponseMessage GetScreenersTree(string filter = "")
        {
            var treeData = DataProvider.Fill<List<string>>(string.Format(@"Select HostPlus_Locations.RecId as LocationId, HostPlus_Locations.Location,
	                ScreeningMethods.RecId as MethodId, ScreeningMethods.Code as MethodName,
	                Screeners.FirstName +  '' + Screeners.LastName as Screener, Screeners.RecId as ScreenerId
                    from Screeners Inner join ScreenerCertifications on ScreenerCertifications.ScreenerId = Screeners.RecId
                    Inner join CertificationTypes on CertificationTypes.RecId = ScreenerCertifications.CertificationTypeId
                    inner join ScreeningMethods on ScreeningMethods.RecId = CertificationTypes.ScreeningMethodId
                    inner join HostPlus_Locations on HostPlus_Locations.RecId = Screeners.LocationId
                    Where ScreenerCertifications.ValidTo >= getutcdate() and ('{0}' = '' or Screeners.FirstName +  '' + Screeners.LastName like '%' + '{0}' + '%')", filter),
                                rows => rows.GroupBy(r => new { ParentIdColumn = r["LocationId"].ToString(), ParentTextColumn = r["Location"].ToString() }).
                Select(pg => string.Format("<item text=\"{0}({1})\" id=\"{2}_pp\">{3}</item>",
                    (string.IsNullOrEmpty(pg.Key.ParentTextColumn) ? "UNASSIGNED" : pg.Key.ParentTextColumn),
                    pg.Count(),
                    (pg.Key.ParentIdColumn == "0" ? "-1" : pg.Key.ParentIdColumn),
                    string.Join(Environment.NewLine, pg.GroupBy(g => new { Gid = g["MethodId"].ToString(), Gname = g["MethodName"].ToString() }).
                        Select(g => string.Format("<item text=\"{0}({1})\" id=\"{2}_{3}_p\">{4}</item>",
                            (string.IsNullOrEmpty(g.Key.Gname) ? "UNASSIGNED" : g.Key.Gname),
                            g.Count(),
                            pg.Key.ParentIdColumn, (g.Key.Gid == "0" ? "-1" : g.Key.Gid),
                            string.Join(Environment.NewLine, g.Select(i => string.Format("<item text=\"{0}\" id=\"{1}_{2}_{3}_i\"></item>",
                                string.IsNullOrEmpty(i["Screener"].ToString()) ? "N/A" : i["Screener"].ToString(), pg.Key.ParentIdColumn, (g.Key.Gid == "0" ? "-1" : g.Key.Gid), i["ScreenerId"].ToString())).ToList())
                                )
                    ).ToList()))).ToList());

            // there is a bug with 3 dimensional json
            var xml = string.Format("<tree id=\"0\">{0}</tree>", string.Join(Environment.NewLine, treeData));
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }
    }
}