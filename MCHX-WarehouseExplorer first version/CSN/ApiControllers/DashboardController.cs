﻿using CSN.DataProviders;
using DhtmlxComponents.Controllers;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace CSN.ApiControllers
{
    public class DashboardController : BaseApiController<CsnBaseDataProvider>
    {

        public HttpResponseMessage GetChartsList()
        {
            var xml = @"<data><item id='1'><title>Screening Volumes</title></item>
                              <item id='2'><title>Hourly Production</title></item>
                              <item id='3'><title>Rescreened Volumes</title></item>
                                <item id='4'><title>Screening Technologies Utilization</title></item>
                                <item id='5'><title>ScreeningResults</title></item>
                                <item id='6'><title>Chain Of Factory</title></item>
                        </data>";
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }
        public IEnumerable<ChartStepData> GetScreeningVolumesChartData(long? location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var query = new StringBuilder(@"Select FS.LocationId as LocId, HL.Location as Loc, SUM(FS.NumberOfPieces) as ScreeningVolume
                           From Fsp.Screening FS WITh (NOLOCK)
                            left join HostPlus_Locations HL WITH (NOLOCK) On FS.LocationId = HL.RecId");

            if (dateFrom != null || dateTo != null || location!=null)
            {
                query.Append(" where");
            }
            if (dateFrom != null)
            {
                query.Append(" ScreeningDate >= '{0}' and");
            }
            if (dateTo != null)
            {
                query.Append(" ScreeningDate <= '{1}' and");
            }
            if (location != null)
            {
                query.Append(" FS.LocationId = '{2}' and");
            } 

             query.Append(@" and SampleMethod not in ('VERIFICATION')
                            group by FS.LocationId, HL.Location");
            query.Replace("and and", "and");

            var queryStr = query.ToString();

            return DataProvider.Fill<IEnumerable<ChartStepData>>(string.Format(queryStr, dateFrom, dateTo, location),
               rows => rows.Select(r => new ChartStepData
               {
                   id = r["LocId"].ToString(),
                   xValue = r["Loc"].ToString(),
                   yValue = r["ScreeningVolume"].ToString()
               }));

            //var data = new List<ChartStepData> {
            //new ChartStepData {id = "1", xValue = "Lalala", yValue = "8000" },
            //new ChartStepData {id = "2", xValue = "Ooror", yValue = "3450" },
            //new ChartStepData {id = "3", xValue = "amanama", yValue = "6900" },
            //};

            //return data;
        }

        public IEnumerable<ChartStepData> GetHourlyProductionChartData(long? location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var data = new List<ChartStepData> {
            new ChartStepData {id = "1", xValue = "00", yValue = "8000" },
            new ChartStepData {id = "2", xValue = "01", yValue = "3450" },
            new ChartStepData {id = "3", xValue = "02", yValue = "6900" },
            new ChartStepData {id = "4", xValue = "03", yValue = "1900" },
            new ChartStepData {id = "5", xValue = "04", yValue = "6900" },
            new ChartStepData {id = "6", xValue = "05", yValue = "1300" },
            new ChartStepData {id = "7", xValue = "06", yValue = "6900" },
            new ChartStepData {id = "8", xValue = "07", yValue = "3100" },
            new ChartStepData {id = "9", xValue = "08", yValue = "6900" },

            new ChartStepData {id = "10", xValue = "09", yValue = "2400" },
            new ChartStepData {id = "11", xValue = "10", yValue = "4200" },
            new ChartStepData {id = "12", xValue = "11", yValue = "6700" },
            new ChartStepData {id = "13", xValue = "12", yValue = "6900" },
            new ChartStepData {id = "14", xValue = "13", yValue = "6900" },

            new ChartStepData {id = "15", xValue = "14", yValue = "6900" },
            new ChartStepData {id = "16", xValue = "15", yValue = "9800" },
            new ChartStepData {id = "17", xValue = "16", yValue = "6900" },
            new ChartStepData {id = "18", xValue = "17", yValue = "900" },
            new ChartStepData {id = "19", xValue = "18", yValue = "5500" },
            new ChartStepData {id = "20", xValue = "19", yValue = "6900" },
            new ChartStepData {id = "21", xValue = "20", yValue = "2100" },
            new ChartStepData {id = "22", xValue = "21", yValue = "6000" },
            new ChartStepData {id = "23", xValue = "22", yValue = "1000" },
            new ChartStepData {id = "24", xValue = "23", yValue = "5000" },
            };

            return data;
        }

        public IEnumerable<ChartStepData> GetRescreenedVolumesChartData(long? location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var data = new List<ChartStepData> {
            new ChartStepData {id = "1", xValue = "Lalala", yValue = "8000" },
            new ChartStepData {id = "2", xValue = "Ooror", yValue = "3450" },
            new ChartStepData {id = "3", xValue = "amanama", yValue = "6900" },
            };

            return data;
        }

        public IEnumerable<ChartStepData> GetScreeningResultsChartData(long? location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var data = new List<ChartStepData> {
            new ChartStepData {id = "1", xValue = "Lalala", yValue = "8000" },
            new ChartStepData {id = "2", xValue = "Ooror", yValue = "3450" },
            new ChartStepData {id = "3", xValue = "amanama", yValue = "6900" },
            };

            return data;
        }

        public IEnumerable<ChartStepData> GetScreeningTechnologiesUtilizationChartData(long? location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var query = new StringBuilder(@"Select FS.LocationId, HL.Location, 
                            REPLACE(FS.SampleMethod, 'NORMAL','ETD') as Method, 
                            COUNT(FS.SampleMethod) AS methodCOUNT
                            From Fsp.Screening FS WITh (NOLOCK)
                            left join HostPlus_Locations HL WITH (NOLOCK) On FS.LocationId = HL.RecId");

            if (dateFrom != null || dateTo != null || location != null)
            {
                query.Append(" where");
            }
            if (dateFrom != null)
            {
                query.Append(" ScreeningDate >= '{0}' and");
            }
            if (dateTo != null)
            {
                query.Append(" ScreeningDate <= '{1}' and");
            }
            if (location != null)
            {
                query.Append(" FS.LocationId = '{2}' and");
            }

            query.Append(@" and SampleMethod not in ('VERIFICATION')
group by FS.LocationId, HL.Location, FS.SampleMethod
order by FS.LocationId, FS.SampleMethod");
            query.Replace("and and", "and");

            var queryStr = query.ToString();

            return DataProvider.Fill<IEnumerable<ChartStepData>>(string.Format(queryStr, dateFrom, dateTo, location),
               rows => rows.Select(r => new ChartStepData
               {
                   id = r["Method"].ToString(),
                   xValue = r["Method"].ToString(),
                   yValue = r["methodCOUNT"].ToString()
               }));


            //var data = new List<ChartStepData> {
            //new ChartStepData {id = "1", xValue = "Lalala", yValue = "8000" },
            //new ChartStepData {id = "2", xValue = "Ooror", yValue = "3450" },
            //new ChartStepData {id = "3", xValue = "amanama", yValue = "6900" },
            //};

            //return data;
        }

        public IEnumerable<ChartStepData> GetChainOfFactoryChartData(long? location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var data = new List<ChartStepData> {
            new ChartStepData {id = "1", xValue = "Lalala", yValue = "8000" },
            new ChartStepData {id = "2", xValue = "Ooror", yValue = "3450" },
            new ChartStepData {id = "3", xValue = "amanama", yValue = "6900" },
            };

            return data;
        }

    }

}