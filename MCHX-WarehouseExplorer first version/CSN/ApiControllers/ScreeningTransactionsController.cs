﻿using CSN.DataProviders;
using CSN.Models.ScreeningTransaction;
using DataExporter.Excel;
using DhtmlxComponents.Controllers;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace CSN.ApiControllers
{
    public class ScreeningTransactionsController : BaseApiController<CsnBaseDataProvider>
    {
        #region [ -- Queries. Move To SP -- ]

        private const string gridSelectQuery = @"Select * from (
                Select Top 10000 UPPER(HousebillNumber) as Airbillno, SampleNumber, ScreeningMethods.Code as Method, 
                Fsp.Devices.SerialNumber AS DeviceSerial,
                ScreeningDate as ScreenedOn, 
                HostPlus_Locations.Location AS ScreeningLocation, 
                (Select Sum(isnull(HBSlac, 0)) From fsp.Queue where fsp.Queue.HousebillNo = Fsp.screening.HousebillNumber) as TotalPieces, 
                NumberOfPieces as ScreenedPieces, ResultCode As ScreeningResult,
                Addressbook.FirstName +  ' ' + Addressbook.LastName As Screener, 
                (select TOP 1 IsValid from Fsp.Queue Where HouseBillNo = HousebillNumber) as IsValid,
                Fsp.screening.RecId, ROW_NUMBER() OVER(ORDER BY HousebillNumber) AS NUMBER
                from Fsp.screening left join HostPlus_Locations on HostPlus_Locations.RecId = Fsp.screening.LocationID
                left join ScreeningMethods on ScreeningMethods.RecId = Fsp.screening.ScreeningMethodId
                left join Fsp.Devices on Fsp.Devices.RecId = Fsp.screening.DeviceID
                left join Addressbook on Addressbook.Id = Fsp.screening.UserId
                Where PGDBEnabled = 1 and 
                ('{2}' = '' or ScreeningDate >= '{2}') and ('{3}' = '' or ScreeningDate <= '{3}') and
                ('{4}' = '0' or HousebillNumber like '%' + '{4}' + '%') and
                ('{5}' = '0' or cast(SampleNumber as nvarchar) like '%' + '{5}'  + '%') and
                ({6} = 0 or Fsp.screening.LocationID = {6}) and
                ({7} = 0 or exists(Select Top 1 1 from WarehouseLocations WL
                     inner Join Warehouses On Warehouses.RecId = WL.WarehouseId where WL.TypeId = 16 and Warehouses.GatewayId = HostPlus_Locations.EntityId 
                     and WL.RecId = {7})) and
                ({8} = 0 or Fsp.screening.ScreeningMethodId = {8}) and 
                ({9} = 0 or Fsp.Devices.RecId = {9}) and   
                ({10} = 0 or Fsp.screening.UserId = {10}) and                 
                ('{11}' = '0' or ResultCode = '{11}') and 
                SampleMethod <> 'verification' and ScreeningDate > '01-01-2014'
                ) as T
                Where NUMBER BETWEEN {0} AND ({0} + {1})
                Order By Airbillno, SampleNumber";

        private const string gridCountQuery = @"Select Count(0)
                from Fsp.screening left join HostPlus_Locations on HostPlus_Locations.RecId = Fsp.screening.LocationID
                left join ScreeningMethods on ScreeningMethods.RecId = Fsp.screening.ScreeningMethodId
                left join Fsp.Devices on Fsp.Devices.RecId = Fsp.screening.DeviceID
                left join Addressbook on Addressbook.Id = Fsp.screening.UserId
                Where PGDBEnabled = 1 and 
                ('{2}' = '' or ScreeningDate >= '{2}') and ('{3}' = '' or ScreeningDate <= '{3}') and
                ('{4}' = '0' or HousebillNumber like '%' + '{4}' + '%') and
                ('{5}' = '0' or cast(SampleNumber as nvarchar) like '%' + '{5}'  + '%') and
                ({6} = 0 or Fsp.screening.LocationID = {6}) and
                ({7} = 0 or exists(Select Top 1 1 from WarehouseLocations WL
                     inner Join Warehouses On Warehouses.RecId = WL.WarehouseId where WL.TypeId = 16 and Warehouses.GatewayId = HostPlus_Locations.EntityId 
                     and WL.RecId = {7})) and
                ({8} = 0 or Fsp.screening.ScreeningMethodId = {8}) and 
                ({9} = 0 or Fsp.Devices.RecId = {9}) and   
                ({10} = 0 or Fsp.screening.UserId = {10}) and                 
                ('{11}' = '0' or ResultCode = '{11}') and 
                SampleMethod <> 'verification' and ScreeningDate > '01-01-2014'";

        #endregion

        [HttpGet]
        public Grid GetMainGridData(int? posStart = 0, int? count = 0, DateTime? dateFrom = null, DateTime? dateTo = null,
            string shippingReference = "", string sample = "", int? location = 0, int? area = 0, int? method = 0, int? device = 0,
            int? screener = 0, int? screeningResult = 0, int? cctvCamera = 0)
        {
            var query = string.Format(gridSelectQuery, posStart.GetValueOrDefault(), count.GetValueOrDefault(50), dateFrom, dateTo,
                shippingReference, sample, location.GetValueOrDefault(), area.GetValueOrDefault(), method.GetValueOrDefault(), device.GetValueOrDefault(),
                screener.GetValueOrDefault(), screeningResult.GetValueOrDefault(), cctvCamera.GetValueOrDefault());
            var data = DataProvider.GetDataAsString(query, "RecId");

            var countQuery = string.Format(gridCountQuery, posStart.GetValueOrDefault(), count.GetValueOrDefault(), dateFrom, dateTo,
                shippingReference, sample, location.GetValueOrDefault(), area.GetValueOrDefault(), method.GetValueOrDefault(), device.GetValueOrDefault(),
                screener.GetValueOrDefault(), screeningResult.GetValueOrDefault(), cctvCamera.GetValueOrDefault());
            var totalCount = Convert.ToInt32(DataProvider.ExecuteScalar(countQuery));
            var tc = (totalCount > 10000 ? 10000 : totalCount).ToString();

            var position = posStart.GetValueOrDefault();
            var pageSize = 10;

            if (position + pageSize > totalCount)
                pageSize = totalCount - position;

            if (posStart.GetValueOrDefault() > 0)
                tc = string.Empty;

            return new Grid
            {
                total_count = tc,
                pos = position,
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        [HttpGet]
        public IEnumerable<ValueText> GetScreeningLocations()
        {
            return DataProvider.Fill<IEnumerable<ValueText>>("Select RecId, Location From HostPlus_locations where PGDBEnabled = 1 Order By Location", rows => rows.Select(r => new ValueText
            {
                value = r["RecId"].ToString(),
                text = r["Location"].ToString()
            }));
        }

        [HttpGet]
        public IEnumerable<ValueText> GetAreas(int? locationId = 0)
        {
            return DataProvider.Fill<IEnumerable<ValueText>>(string.Format(
                    @"Select Warehouses.Code + ' - ' + WL.Name AS ScreeningArea, WL.RecId from WarehouseLocations WL
                    left Join Warehouses On Warehouses.RecId = WL.WarehouseId
                    where WL.TypeId = 16 AND ({0} = 0 or 
                    Warehouses.GatewayId in (Select EntityId From HostPlus_Locations where RecId = {0}))
                    Order By ScreeningArea", locationId.GetValueOrDefault()),
                rows => rows.Select(r => new ValueText
                {
                    value = r["RecId"].ToString(),
                    text = r["ScreeningArea"].ToString()
                }));
        }

        [HttpGet]
        public IEnumerable<ValueText> GetScreeningMethods()
        {
            return DataProvider.Fill<IEnumerable<ValueText>>("Select RecId, Code from ScreeningMethods where IsEnabled = 1 Order By Code",
                rows => rows.Select(r => new ValueText
                {
                    value = r["RecId"].ToString(),
                    text = r["Code"].ToString()
                }));
        }

        [HttpGet]
        public IEnumerable<ValueText> GetScreeningResults()
        {
            return new List<ValueText>
            {
                new ValueText{value="1", text="PASS"}, new ValueText{value="2", text="ALARM"}, new ValueText{value="3", text="ALARM CLEARED"}   
            };
        }

        [HttpGet]
        public IEnumerable<ValueText> GetCctvCameras(int? locationId = 0)
        {
            return DataProvider.Fill<IEnumerable<ValueText>>(string.Format(
                        @"Select ND.RecId, HL.Location + '-' + Name As DeviceName from NetworkDevices ND
                        left join HostPlus_Locations HL On HL.RecId = ND.LocationId
                        Where ND.IsActive = 1 and ND.DeviceTypeId in (Select RR.TypeId From [References] RR
                        Where RR.ReferenceType = 'Network Devices' and RR.Code = 'CCTV') and ({0} = 0 or ND.LocationId = {0})
                        Order By DeviceName", locationId),
                rows => rows.Select(r => new ValueText
                {
                    value = r["RecId"].ToString(),
                    text = r["DeviceName"].ToString()
                }));
        }

        [HttpGet]
        public Form<HeaderInfo> GetTransactionDetails(long id)
        {
            var headerInfo = DataProvider.FillSingleOrDefailt<HeaderInfo>(
                string.Format(@"Select top 1
	                                Fsp.screening.RecId as Id, 
	                                UPPER(HousebillNumber) as ShipmentReference,
	                                (Select Sum(isnull(HBSlac, 0)) From fsp.Queue where fsp.Queue.HousebillNo = Fsp.screening.HousebillNumber) as TotalPieces, 
	                                NumberOfPieces as ScreenedPieces, 	
	                                'Screening In Progress' As Status,
	                                case when Isnull((select TOP 1 IsValid from Fsp.Queue Where HouseBillNo = HousebillNumber), 0) = 1 then 'Yes' else 'No' end as IsValid,
	                                SampleNumber, ScreeningMethods.Code as Method, 
	                                Fsp.Devices.SerialNumber AS Device,
	                                ScreeningDate as Timestamp, 
	                                HostPlus_Locations.Location AS Location, 
	                                ResultCode As ScreeningResult,
	                                isnull(Screeners.FirstName +  ' ' + Screeners.LastName, '') As Screener,
	                                Remark as Comment
                                from Fsp.screening left join HostPlus_Locations on HostPlus_Locations.RecId = Fsp.screening.LocationID
                                left join ScreeningMethods on ScreeningMethods.RecId = Fsp.screening.ScreeningMethodId
                                left join Fsp.Devices on Fsp.Devices.RecId = Fsp.screening.DeviceID
                                left join Screeners on Screeners.RecId = Fsp.screening.ScreenerId where Fsp.Screening.RecId = {0}", id), row => new HeaderInfo 
                                  {
                                      Id = Convert.ToInt64(row["Id"]),
                                      ShipmentReference = row["ShipmentReference"].ToString(),
                                      TotalPieces = Convert.ToInt32(row["TotalPieces"]),
                                      ScreenedPieces = Convert.ToInt32(row["ScreenedPieces"]),                                                                            
                                      Status = row["Status"].ToString(),
                                      Validated = row["IsValid"].ToString(),
                                      SampleNumber = row["SampleNumber"].ToString(),
                                      Method = row["Method"].ToString(),
                                      Device = row["Device"].ToString(),
                                      Timestamp = Convert.ToDateTime(row["Timestamp"]),
                                      Location = row["Location"].ToString(),
                                      Result = row["ScreeningResult"].ToString(),
                                      Screener = row["Screener"].ToString(),
                                      Comment = row["Comment"].ToString()
                                  });
            return new Form<HeaderInfo> { data = headerInfo };
        }

        [HttpGet]
        public Grid GetShipmentRemarks(long id, int? posStart = 0)
        {
            var data = DataProvider.GetDataAsString(string.Format(@"Select UserId, Remarks.RecDate, Remarks.Description as Remark, Remarks.RecId from Remarks 
                    Inner join fsp.Queue as Q On 
	                    (((Origin + '-' + Q.HousebillNo + '-' + Destination) = Remarks.Reference) 
	                    OR (('???-' + Q.HousebillNo + '-???') = Remarks.Reference))
                    Inner join Fsp.Screening as Screening ON Q.HousebillNo = Screening.HousebillNumber
                    where Screening.RecId = {0} Order By Remarks.RecDate desc", id), "RecId");

            return new Grid
            {                
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        [HttpGet]
        public IEnumerable<Image> GetImagesForTransaction(long id, string method)
        {
            //if (method == "XRAY")
                return new List<Image> { new Image { ImageName = "xray1.jpg" }, new Image { ImageName = "xray2.jpg" } };
            //return new List<Image>();
        }

        [HttpGet]
        public HttpResponseMessage ExportToExcel()
        {
            var data = DataProvider.Fill<IEnumerable<TestExcel>>("Select top 10 RecId, Location From HostPlus_Locations",
                rows => rows.Select(r => new TestExcel
                {
                    Id = Convert.ToInt32(r["RecId"]),
                    Name = r["Location"].ToString()
                }).ToList());

            var excelData = new ExcelData
            {
                SheetName = "Test",
                Headers = new List<string> { "Id", "Loaction" },
                DataRows = data.Select(d => new List<string> { d.Id.ToString(), d.Name }).ToList()
            };

            var excelManager = new ExcelManager();
            var excel = excelManager.GenerateExcel(excelData);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(new MemoryStream(excel));
            result.Content.Headers.Add("Content-Disposition", "attachment; filename=ExcelFile.xlsx");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return result;
        }
    }

    public class TestExcel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Image
    {
        public string ImageName { get; set; }       
    }
}