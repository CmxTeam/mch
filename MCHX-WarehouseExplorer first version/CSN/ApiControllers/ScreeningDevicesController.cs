﻿using CSN.DataProviders;
using DhtmlxComponents.Controllers;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace CSN.ApiControllers
{
    public class ScreeningDevicesController : BaseApiController<CsnBaseDataProvider>
    {
        [HttpGet]
        public Grid GetGridData(string device = "")
        {
            string filter = string.Empty;
            long result;
            if (long.TryParse(device, out result))
                filter = string.Format("Where Devices.RecId = {0}", result);
            else if (long.TryParse(device.Replace("_p_p", ""), out result))
            {
                if (result == 0)
                    filter = string.Format("Where HostPlus_Locations.RecId is null", result);
                else
                    filter = string.Format("Where HostPlus_Locations.RecId = {0}", result);
            }            

            var query = string.Format(@"Select Devices.RecId as Id, '' as Image,
                            Devices.SerialNumber as SerialNumber, CertifiedDevices.ModelNumber,
                            ScreeningMethods.Code as Tecnology, Location
                            from fsp.Devices as Devices
                            left join CertifiedDevices On CertifiedDevices.RecId = Devices.CertifiedDeviceId
                            left join ScreeningMethods On ScreeningMethods.RecId = CertifiedDevices.ScreeningMethodId
                            left join HostPlus_Locations on HostPlus_Locations.RecId = Devices.LocationId {0}"
                            , filter);
            var data = DataProvider.GetDataAsString(query, "Id");

            return new Grid
            {
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        [HttpGet]
        public IEnumerable<ValueText> GetScreeningDevices(int? locationId = 0, int? screeningMethodId = 0, int? screeningAreaId = 0)
        {
            return DataProvider.Fill<IEnumerable<ValueText>>(string.Format(
                        @"Select SM.Code + '-' + FD.SerialNumber as Device, FD.RecId from fsp.Devices FD
                        left join CertifiedDevices CD On CD.RecId = FD.CertifiedDeviceId
                        left join ScreeningMethods SM On SM.RecId = CD.ScreeningMethodId
                        where ({0} = 0 or SM.RecId = {0}) and ({1} = 0 or FD.LocationId = {1}) and ({2} = 0 or FD.WarehouseLocationId = {2})
                        Order By Device",
                        screeningMethodId.GetValueOrDefault(), locationId.GetValueOrDefault(), screeningAreaId.GetValueOrDefault()),
                rows => rows.Select(r => new ValueText
                {
                    value = r["RecId"].ToString(),
                    text = r["Device"].ToString()
                }));
        }

        [HttpGet]
        public HttpResponseMessage GetDevicesTree(string filter = "")
        {
            //no way to rebing json grid
            var treeData = DataProvider.Fill<List<string>>(string.Format(@"Select SM.Code + '-' + FD.SerialNumber as Device, FD.RecId, LocationId, Location from fsp.Devices FD
                    left join CertifiedDevices CD On CD.RecId = FD.CertifiedDeviceId
                    left join ScreeningMethods SM On SM.RecId = CD.ScreeningMethodId
                    left join HostPlus_Locations on HostPlus_Locations.RecId = LocationId
                    where '{0}' = '' or FD.SerialNumber like '%' + '{0}' + '%'", filter),
                    rows => rows.Select(r => new
                    {
                        Device = r["Device"].ToString(),
                        DeviceId = Convert.ToInt64(r["RecId"]),
                        LocationId = Convert.ToInt64(r["LocationId"]),
                        Location = r["Location"].ToString()
                    }).GroupBy(r => new { LocationId = r.LocationId + "_p", Location = string.IsNullOrEmpty(r.Location) ? "UNASSIGNED" : r.Location })
                    .Select(g => string.Format("<item text=\"{0}({1})\" id=\"{2}_p\">{3}</item>", g.Key.Location, g.Count(), g.Key.LocationId,
                        string.Join(Environment.NewLine, g.Select(i => string.Format("<item text=\"{0}\" id=\"{1}\"></item>", i.Device, i.DeviceId))))).ToList());

            var xml = string.Format("<tree id=\"0\"><item text=\"SCREENING DEVICES\" id=\"rootElement\">{0}</item></tree>", string.Join(Environment.NewLine, treeData));
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }
    }
}