﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CSN.ApiControllers
{
    public class MapController : ApiController
    {
        public object GetData()
        {
            var dataTable = new DataTable();
            using (var connction = new SqlConnection(ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString))
            {
                using (var command = new SqlCommand("Select RecId, Location, Latitude, Longitude From HostPlus_locations where PGDBEnabled = 1 Order By Location", connction))
                {
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }

            return dataTable.Rows.OfType<DataRow>().Select(r => new MapItem
            {
                id = Convert.ToInt32(r["RecId"]),
                Name = r["Location"].ToString(),
                Image = "flag.jpg",
                Zoom = 6,
                Lat = r["Latitude"].ToString(),
                Lng = r["Longitude"].ToString(),
                Pin = (new Random(Convert.ToInt32(r["RecId"]))).Next() % 2 == 1 ? "blue.png" : "red.png",

                //TODO: Aram, this data should come from DB
                Details = new MapItemDetails { 
                CCSFType = "IAC",
                SecurityManagerName = "John Baitz",
                Email = "jbaitz@dhl.com",
                Office = "(516)792 0400 x 10",
                Mobile = "(917)553 3100",
                TotalScannedPieces = 15, 
                TotalPassed = 8,
                TotalFailed = 7, 
                TotalScreenedPrimary = 10,
                TotalScreenedSecondary = 5
                }
            });
        }
    }

    public class MapItem
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int Zoom { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Pin { get; set; }
        public MapItemDetails Details { get; set; }
    }

    public class MapItemDetails
    {
        public string CCSFType { get; set; }
        public string SecurityManagerName { get; set; }
        public string Email { get; set; }
        public string Office { get; set; }
        public string Mobile { get; set; }
        public int TotalScannedPieces { get; set; }
        public int TotalPassed { get; set; }
        public int TotalFailed { get; set; }
        public int TotalScreenedPrimary { get; set; }
        public int TotalScreenedSecondary { get; set; }

    }
}