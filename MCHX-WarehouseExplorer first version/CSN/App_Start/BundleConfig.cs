﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace CSN.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/dhtmlx").Include(
                        "~/Script/dhtmlx.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Script/infobubble.js",
                        "~/Script/dashboard.js",
                        "~/Script/map.js",
                        "~/Script/screeningTransactions.js",
                        "~/Script/screeningDevices.js",
                        "~/Script/screenerModule.js",
                        "~/Script/transactionDetailsWindow.js",
                        "~/Script/enterToLiveScreening.js",
                        "~/Script/liveScreening.js",
                        "~/Script/liveScreeningCreateHawb.js"));            

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/dhtmlx.css",
                      "~/Content/style.css"));
        }
    }
}