﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CSN.Startup))]
namespace CSN
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}