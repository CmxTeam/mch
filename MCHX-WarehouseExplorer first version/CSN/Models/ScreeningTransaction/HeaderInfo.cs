﻿using System;

namespace CSN.Models.ScreeningTransaction
{
    public class HeaderInfo : IdModel
    {
        public string ShipmentReference { get; set; }
        public int ScreenedPieces { get; set; }
        public int TotalPieces { get; set; }
        public string Status { get; set; }
        public string Validated { get; set; }        
        public string SampleNumber { get; set; }
        public string Method { get; set; }
        public string Device { get; set; }
        public DateTime Timestamp { get; set; }
        public string Location { get; set; }
        public string Result { get; set; }    
        public string Screener { get; set; }                           
        public string Comment { get; set; }                   
    }
}