﻿var screeningDevicesModule = function (cell, accordion) {
    var grid, tree;
    var $this = this;

    this.show = function () {
        cell.setText('SCREENING DEVICES');
        cell.showView('devicesGridContainer');
    }

    this.filter = function (device) {
        cell.progressOn();
        grid.clearAll();       
        grid.load('api/ScreeningDevices/GetGridData?device=' + device, function () { cell.progressOff(); }, 'json');
    }

    this.isNumeric = function (input) {
        return (input - 0) == input && ('' + input).trim().length > 0;
    }

    grid = function () {
        cell.showView('devicesGridContainer');
        var grid = cell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Id", "Certified Device", "Serial No", "Model Number", "Technology", "Assigned To CCSF"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro");
        grid.setColSorting('str,str,str,str,str,str');
        grid.setInitWidths('*,*,*,*,*,*');
        grid.setColumnHidden(0, true);
        //grid.enableColumnMove(true);
        grid.init();
        return grid;
    }();

    tree = function () {
        var screeningDevicesContainer = accordion.addItem('screeningDevicesContainer', 'DEVICES');
        var devices = screeningDevicesContainer.attachToolbar();
        devices.setIconsPath(dhtmlx.image_path);
        devices.loadStruct('<toolbar><item type="buttonInput" id="button_input_3" /><item type="buttonSelect" id="button_select_3" text="New" /></toolbar>', function () { });

        devices.attachEvent('onEnter', function (name, value) {
            tree.setXMLAutoLoading('api/ScreeningDevices/GetDevicesTree?filter=' + value);
            tree.refreshItem(0);
            tree.openItem('rootElement');
        });

        var tree = screeningDevicesContainer.attachTree();
        tree.setImagePath(dhtmlx.tree_image_path);
        tree.loadXML('api/ScreeningDevices/GetDevicesTree');
        tree.attachEvent("onClick", function (id) {
            $this.filter(id);
        });
        return tree;
    }();

    this.filter();
    return this;
};