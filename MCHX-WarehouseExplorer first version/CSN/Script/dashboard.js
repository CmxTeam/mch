﻿var dashboardModule = function (cell, accordion) {
    var mainLayout, leftLayout;
    var screeningVolumesChart, hourlyChart, rescreenedVolumesChart, technologiesChart, screeningResultsChart, chainOfFactoryChart;
    var dataViewCharts;
    this.ChartsEnum = {
        ScreeningVolumes: '1',
        HourlyProduction: '2',
        RescreenedVolumes: '3',
        ScreeningTechnologies: '4',
        ScreeningResults: '5',
        ChainOfFactory: '6',
    }


    this.config = {
        cell: cell,
        accordion: accordion
    };

    this.show = function () {
        cell.showView('dashboardContainer');
    }

    this.filter = function (id) {
        this._filter(
                dashboardAccordionForm.getCombo('screeningLocations').getSelectedValue(),
                dashboardAccordionForm.getCalendar('fromDateCalendar').getFormatedDate(),
                dashboardAccordionForm.getCalendar('toDateCalendar').getFormatedDate(),
                id);
    }

    this.resetControls = function () {
        dashboardAccordionForm.getCombo('screeningLocations').unSelectOption();
        period.selectOption(0);
        this._setCalendarDate('fromDateCalendar', dashboardAccordionForm, new Date());
        this._setCalendarDate('toDateCalendar', dashboardAccordionForm, new Date());
    }


    this._setCalendarDate = function (name, form, date) {
        var calendar = form.getCalendar(name);
        var calendarToKey = Object.keys(calendar.i)[0];
        calendar.i[calendarToKey].input.value = window.dhx4.date2str(date, '%m-%d-%Y');
        calendar.setDate(date);
    }

    this._filter = function (location, dateFrom, dateTo, id) {
        //debugger;
        if (id == undefined) {
            id = dataViewCharts.getSelected();

            if (screeningVolumesChart != undefined) {
                screeningVolumesChart.clearAll();
            }
            if (hourlyChart != undefined) {
                hourlyChart.clearAll();
            }
            if (rescreenedVolumesChart != undefined) {
                rescreenedVolumesChart.clearAll();
            }
            if (technologiesChart != undefined) {
                technologiesChart.clearAll();
            }
            if (screeningResultsChart != undefined) {
                screeningResultsChart.clearAll();
            }
            if (chainOfFactoryChart != undefined) {
                chainOfFactoryChart.clearAll();
            }
        }

        switch (id) {
            case this.ChartsEnum.ScreeningVolumes:
                leftLayout.showView('ScreeningVolumes');
                leftLayout.setText('SCREENING VOLUMES');

                if (screeningVolumesChart == undefined) {
                    screeningVolumesChart = leftLayout.attachChart({
                        view: 'bar',
                        label: '#yValue#',
                        tooltip: {
                            template: '#yValue#'
                        },
                        gradient: false,
                        xAxis: { "title": "CCSFs", "template": "#xValue#", "step": "500" },
                        yAxis: { "title": "VOLUME", "start": "0", "end": "10000", "step": "2000" },
                        value: '#yValue#'
                    });
                }

                if (screeningVolumesChart.dataCount() == 0) {
                    screeningVolumesChart.load('api/Dashboard/GetScreeningVolumesChartData?' +
                        '&location=' + location +
                        '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
                break;
            case this.ChartsEnum.HourlyProduction:
                leftLayout.showView('HourlyProduction');
                leftLayout.setText('HOURLY PRODUCTIVITY');

                if (hourlyChart == undefined) {
                    hourlyChart = leftLayout.attachChart({
                        view: 'area',
                        tooltip: {
                            template: '#yValue#'
                        },
                        color: '#00ccff',
                        xAxis: { "title": "HOUR", "template": "#xValue#", "step": "500" },
                        yAxis: { "title": "VOLUME", "start": "0", "end": "10000", "step": "2000" },
                        value: '#yValue#'
                    });
                }
                if (hourlyChart.dataCount() == 0) {
                    hourlyChart.load('api/Dashboard/GetHourlyProductionChartData?' +
                       '&location=' + location +
                       '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
                break;
            case this.ChartsEnum.RescreenedVolumes:
                leftLayout.showView('RescreenedVolumes');
                leftLayout.setText('RESCREENED VOLUMES');

                if (rescreenedVolumesChart == undefined) {
                    rescreenedVolumesChart = leftLayout.attachChart({
                        view: 'bar',
                        label: '#yValue#',
                        tooltip: {
                            template: '#yValue#'
                        },
                        gradient: false,
                        xAxis: { "title": "CCSFs", "template": "#xValue#", "step": "500" },
                        yAxis: { "title": "VOLUME", "start": "0", "end": "10000", "step": "2000" },
                        value: '#yValue#'
                    });
                }

                if (rescreenedVolumesChart.dataCount() == 0) {
                    rescreenedVolumesChart.load('api/Dashboard/GetRescreenedVolumesChartData?' +
                       '&location=' + location +
                       '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
                break;
            case this.ChartsEnum.ScreeningTechnologies:
                debugger;
                leftLayout.showView('ScreeningTechnologies');
                leftLayout.setText('SCREENING TECHNOLOGIES UTILIZATION');

                if (technologiesChart == undefined) {
                    technologiesChart = leftLayout.attachChart({
                        view: 'pie',
                        label: '#xValue#',
                        tooltip: {
                            template: '#yValue#'
                        },
                        pieInnerText: '#yValue#',
                        gradient: false,
                        value: '#yValue#'
                    });
                }

                if (technologiesChart.dataCount() == 0) {
                    technologiesChart.load('api/Dashboard/GetScreeningTechnologiesUtilizationChartData?' +
                    '&location=' + location +
                    '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
                break;
            case this.ChartsEnum.ScreeningResults:
                leftLayout.showView('ScreeningResults');
                leftLayout.setText('SCREENING TECHNOLOGIES UTILIZATION');

                if (screeningResultsChart == undefined) {
                    screeningResultsChart = leftLayout.attachChart({
                              view: 'pie',
                              label: '#xValue#',
                              tooltip: {
                                  template: '#yValue#'
                              },            
                              pieInnerText: '#yValue#',
                              gradient: false,
                              value: '#yValue#'
                          });
                }
             
                if (screeningResultsChart.dataCount() == 0) {
                    screeningResultsChart.load('api/Dashboard/GetScreeningResultsChartData?' +
                     '&location=' + location +
                     '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
                break;
            case this.ChartsEnum.ChainOfFactory:

                leftLayout.showView('ChainOfFactory');
                leftLayout.setText('CHAIN OF CUSTODY');

                if (chainOfFactoryChart == undefined) {                  
                    chainOfFactoryChart = leftLayout.attachChart({
                          view: 'pie',
                          label: '#xValue#',
                          tooltip: {
                              template: '#yValue#'
                          },            
                          pieInnerText: '#yValue#',
                          gradient: false,
                          value: '#yValue#'
                      });
                }


                if (chainOfFactoryChart.dataCount() == 0) {
                    chainOfFactoryChart.load('api/Dashboard/GetChainOfFactoryChartData?' +
                    '&location=' + location +
                    '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
                break;
            default:
                leftLayout.showView('ScreeningVolumes');
                leftLayout.setText('SCREENING VOLUMES');

                if (screeningVolumesChart == undefined) {
                    screeningVolumesChart = leftLayout.attachChart({
                        view: 'bar',
                        label: '#yValue#',
                        tooltip: {
                            template: '#yValue#'
                        },
                        gradient: false,
                        xAxis: { "title": "CCSFs", "template": "#xValue#", "step": "500" },
                        yAxis: { "title": "VOLUME", "start": "0", "end": "10000", "step": "2000" },
                        value: '#yValue#'
                    });
                }

                if (screeningVolumesChart.dataCount() == 0) {
                    screeningVolumesChart.load('api/Dashboard/GetScreeningVolumesChartData?' +
                        '&location=' + location +
                        '&dateFrom=' + dateFrom + '&dateTo=' + dateTo, 'json');
                }
        }

    }

    var init = function (config) {
        var self = this;
        cell.showView('dashboardContainer');

        var dashboardAccordionContainer = config.accordion.addItem('dashboardAccordionContainer', 'DASHBOARD');

        var dashboardAccordionFormStructure = [
                { type: "combo", name: "screeningLocations", label: "CCSF:", position: "label-left", labelWidth: 60, connector: 'api/ScreeningTransactions/GetScreeningLocations', inputWidth: 210 },
                { type: "combo", name: "period", label: "Period:", position: "label-left", labelWidth: 60, inputWidth: 210 },
                { type: "calendar", name: "fromDateCalendar", label: "From Date:", position: "label-left", labelWidth: 60, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                { type: "calendar", name: "toDateCalendar", label: "To Date:", position: "label-left", labelWidth: 60, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                { type: "block", list: [{ type: "button", name: "resetButton", value: "RESET", offsetLeft: 80 }, { type: "newcolumn" }, { type: "button", name: "filterButton", value: "APPLY" }] }
        ];

        dashboardAccordionForm = dashboardAccordionContainer.attachForm(dashboardAccordionFormStructure);

        this._setCalendarDate('fromDateCalendar', dashboardAccordionForm, new Date());
        this._setCalendarDate('toDateCalendar', dashboardAccordionForm, new Date());
        var period = dashboardAccordionForm.getCombo('period');

        period.load("api/Common/GetPeriods", function () {
            period.selectOption(0);
        });

        period.attachEvent("onChange", function (value) {
            var dateNow = new Date();
            var newDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - (value - 1));

            this._setCalendarDate('fromDateCalendar', dashboardAccordionForm, newDate);
        }.bind(this));

        dashboardAccordionForm.attachEvent("onButtonClick", function (name) {
            if (name == 'filterButton') {
                this.filter();
            } else {
                this.resetControls()
            }
        }.bind(this));


        // Charts cell

        mainLayout = config.cell.attachLayout('2U');

        leftLayout = mainLayout.cells('a');

        // Charts Data View

        var rightLayout = mainLayout.cells('b');

        rightLayout.setWidth('230');
        rightLayout.hideHeader();
        dataViewCharts = rightLayout.attachDataView({
            type: {
                template: '<label style=\'color: #007DCA;font-size: 14px;font-weight: bold;\'>#title#<label/>',
                height: 50
            }
        });

        dataViewCharts.attachEvent('onSelectChange', function (sel_arr) {
            //debugger;
            this.filter(sel_arr[0]); // id of item
        }.bind(this));

        dataViewCharts.attachEvent('onAfterRender', function () {
            //dataViewCharts.select("1");
        });

        dataViewCharts.load('api/Dashboard/GetChartsList', function () {
            dataViewCharts.select(1);
        }, 'xml');

        // select first
        //debugger; do not commit with debugger please!!
      //  dataViewCharts.select(1);

    }.call(this, this.config);
};