﻿var liveScreeningCreateHawb = function (info) {
    this.info = info;
    this.onSubmit = null;

    this._refreshCombo = function (comboName, loadUrl) {
        var combo = this.form.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        combo.load(loadUrl, 'json');
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        var win = windows.createWindow('enterToLiveScreeningWindow', 0, 0, 340, 300);

        var formStructure = [
            {
                type: 'block', list: [
                    { type: "label", label: 'Shipment Reference#: ' + this.info.shipment},
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "origin", label: "Origin:", labelWidth: 120, position: "label-left", inputWidth: 150, required: true },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "destination", label: "Destination:", labelWidth: 120, position: "label-left", inputWidth: 150, required: true },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "totalPieces", label: "Total Pieces:", labelWidth: 120, position: "label-left", inputWidth: 150, required: true , validate: "NotEmpty,ValidNumeric" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "totalSlac", label: "Total SLAC:", labelWidth: 120, position: "label-left", inputWidth: 150, required: true },
                ]
            },
            {
                type: 'block', list: [
                    { type: "label", name: 'errorMessage', label: ''},
                ]
            },
            {
                type: 'block', list: [
                    { type: "button", name: "cancelButton", value: "CANCEL", offsetLeft: 112, offsetTop: 15 },
                    { type: "newcolumn" },
                    { type: "button", name: "submitButton", value: "CREATE", offsetTop: 15 }
                ]
            }
        ];        

        form = win.attachForm(formStructure);
        form.attachEvent("onButtonClick", function (name) {
            if (name == 'cancelButton') {
                this.win.close();
            } else if (name == 'submitButton') {
                if (form.validate()) {
                    this.win.progressOn();
                    var values = form.getValues();
                    //string origin, string destination, int pieces, int slac)
                    dhx4.ajax.get(window.WebApiUrl + "/LiveScreening/CreateShipment?userId=" + '44025' +
                        'locationId=' + this.info.location.id +
                        '&number=' + this.info.shipment +
                        '&origin=' + values.origin +
                        '&destination=' + values.destination +
                        '&pieces=' + values.totalPieces +
                        '&slac=' + values.totalSlac, function (a) {
                            var data = JSON.parse(a.xmlDoc.response || a.xmlDoc.responseText);
                            this.win.progressOff();
                            if (data === 'OK') {
                                this.win.close();
                                if (this.onSubmit) {
                                    this.onSubmit();
                                }
                            } else {
                                form.setItemLabel('errorMessage', data);
                            }                            
                        }.bind(this));                   
                }
            }            
        }.bind(this));
        form.setValidation('origin', function (data) {            
            return data.length == 3;
        });
        form.setValidation('destination', function (data) {
            return data.length == 3;
        });
        form.setValidation('totalSlac', function (data) {
            return parseInt(data) >= parseInt(this.info.slac);
        }.bind(this));

        win.setText('SHIPMENT DOES NOT EXIST');
        win.setModal(1);
        win.denyResize();
        win.centerOnScreen();
        this.form = form;
        this.win = win;
    }.apply(this);
};