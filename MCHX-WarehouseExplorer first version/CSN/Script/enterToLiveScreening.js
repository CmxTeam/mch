﻿var enterToLiveScreening = function () {
    this.onSubmit = null;

    this._refreshCombo = function (comboName, loadUrl) {
        var combo = this.form.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        combo.load(loadUrl, 'json');
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('enterToLiveScreeningWindow', 0, 0, 400, 200);

        var formStructure = [
            {
                type: 'block', list: [
                    { type: "combo", name: "screeningLocationCombo", label: "Screening Location:", labelWidth: 120, position: "label-left", inputWidth: 200, connector: 'api/ScreeningTransactions/GetScreeningLocations', validate: "NotEmpty" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "combo", name: "screeningDeviceIdCombo", label: "Screening Device ID:", labelWidth: 120, position: "label-left", inputWidth: 200, validate: "NotEmpty" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "combo", name: "operatorTypeCombo", label: "Operator Type:", labelWidth: 120, position: "label-left", inputWidth: 200, validate: "NotEmpty" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "button", name: "submitButton", value: "SUBMIT", offsetLeft: 243, offsetTop: 25 }
                ]
            }
        ];

        form = window.attachForm(formStructure);

        var screeningLocationCombo = form.getCombo('screeningLocationCombo');
        screeningLocationCombo.attachEvent("onChange", function (value) {
            this._refreshCombo('screeningDeviceIdCombo', "api/ScreeningDevices/GetScreeningDevices?locationId=" + value + "&screeningMethodId=2");
        }.bind(this));

        form.getCombo('operatorTypeCombo').load([{ value: "1", text: "Operator" }]);

        form.attachEvent("onButtonClick", function () {
            if (form.validate()) {
                if (this.onSubmit) {                    
                    var lc = form.getCombo('screeningLocationCombo');
                    var dc = form.getCombo('screeningDeviceIdCombo');
                    var oc = form.getCombo('operatorTypeCombo');

                    this.onSubmit({
                        location: { id: lc.getSelectedValue(), text: lc.getSelectedText() },
                        device: { id: dc.getSelectedValue(), text: dc.getSelectedText() },
                        operatorType: { id: oc.getSelectedValue(), text: oc.getSelectedText() }
                    });
                }
                this.window.close();
            }
        }.bind(this));

        window.setText('WELCOME');
        window.setModal(1);
        window.denyResize();
        window.centerOnScreen();
        this.form = form;
        this.window = window;
    }.apply(this);
};