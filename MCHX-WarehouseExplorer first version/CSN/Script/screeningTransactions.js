﻿var screeningTransactionsModule = function (cell, accordion, rowClickListener) {
    var grid, searchPanelForm;
    var $this = this;
    this.listener = rowClickListener;

    this.show = function () {
        cell.setText('SCREENING TRANSACTIONS');
        cell.showView('testa');
    }

    this.filter = function (use) {
        this._filter(searchPanelForm.getCalendar('fromDateCalendar').getFormatedDate(),
                searchPanelForm.getCalendar('toDateCalendar').getFormatedDate(),
                searchPanelForm.getItemValue('form_input_reference'),
                searchPanelForm.getItemValue('form_input_sample'),
                searchPanelForm.getCombo('screeningLocations').getSelectedValue(),
                searchPanelForm.getCombo('screeningAreas').getSelectedValue(),
                searchPanelForm.getCombo('screeningMethods').getSelectedValue(),
                searchPanelForm.getCombo('screeningDevices').getSelectedValue(),
                searchPanelForm.getCombo('screeners').getSelectedValue(),
                searchPanelForm.getCombo('screeningResults').getSelectedValue(),
                searchPanelForm.getCombo('cctvCameras').getSelectedValue());
    }

    this.resetControls = function () {
        this._clearCalendar('fromDateCalendar', searchPanelForm);
        this._clearCalendar('toDateCalendar', searchPanelForm);
        searchPanelForm.getCombo('screeningLocations').unSelectOption();
        searchPanelForm.getCombo('screeningAreas').unSelectOption();
        searchPanelForm.getCombo('screeningMethods').unSelectOption();
        searchPanelForm.getCombo('screeningDevices').unSelectOption();
        searchPanelForm.getCombo('screeners').unSelectOption();
        searchPanelForm.getCombo('screeningResults').unSelectOption();
        searchPanelForm.getCombo('cctvCameras').unSelectOption();
        searchPanelForm.setItemValue('form_input_reference', '');
        searchPanelForm.setItemValue('form_input_sample', '');
    }

    this.setLocation = function (id) {
        var ind = searchPanelForm.getCombo('screeningLocations').getIndexByValue(id);
        searchPanelForm.getCombo('screeningLocations').selectOption(ind);
    }

    this._filter = function (dateFrom, dateTo, shippingReference, sample, location, area, method, device, screener, screeningResult, cctvCamera) {
        cell.progressOn();
        grid.clearAll();
        grid.load('api/ScreeningTransactions/GetMainGridData?' +
            '&dateFrom=' + dateFrom + '&dateTo=' + dateTo + '&shippingReference=' + shippingReference +
            '&sample=' + sample + '&location=' + location + '&area=' + area + '&method=' + method +
            '&device=' + device + '&screener=' + screener + '&screeningResult=' + screeningResult + '&cctvCamera=' + cctvCamera,
        function () { cell.progressOff(); }, 'json');
    }

    this._fixCalendar = function (name, panel) {
        var calendarTo = panel.getCalendar(name);
        var calendarToKey = Object.keys(calendarTo.i)[0];
        calendarTo.i[calendarToKey].input.value = window.dhx4.date2str(new Date(), '%m-%d-%Y');
        calendarTo.setDate(new Date());
    }

    this._clearCalendar = function (name, panel) {
        var calendarTo = panel.getCalendar(name);
        var calendarToKey = Object.keys(calendarTo.i)[0];
        calendarTo.i[calendarToKey].input.value = '';
        calendarTo.setDate(null);
    }

    this._filterScreeningDevices = function () {
        var screeningLocation = searchPanelForm.getCombo('screeningLocations').getSelectedValue();
        var screeningArea = searchPanelForm.getCombo('screeningAreas').getSelectedValue();
        var screeningMethod = searchPanelForm.getCombo('screeningMethods').getSelectedValue();
        $this._refreshCombo('screeningDevices', "api/ScreeningDevices/GetScreeningDevices?locationId=" + screeningLocation + "&screeningMethodId=" + screeningMethod + "&screeningAreaId=" + screeningArea);
    }

    this._filterScreeners = function () {
        var screeningLocation = searchPanelForm.getCombo('screeningLocations').getSelectedValue();
        var screeningMethod = searchPanelForm.getCombo('screeningMethods').getSelectedValue();
        $this._refreshCombo('screeners', "api/Screeners/GetScreeners?locationId=" + screeningLocation + "&screeningMethodId=" + screeningMethod);
    }

    this._refreshCombo = function (comboName, loadUrl) {
        var combo = searchPanelForm.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        combo.load(loadUrl, 'json');
    }

    grid = function () {
        cell.showView('testa');
        var toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar><item type="text" id="button_text_1" text="QUICK FINDER:" title="" /><item type="buttonInput" id="button_input_1" width="225" title="" /><item type="separator" id="button_separator_1" /><item type="buttonSelect" id="button_select_export" text="EXPORT" title="" ><item type="button" id="button_select_option_1" text="Excel" image="" /><item type="button" id="button_select_option_3" text="CSV" image="" /><item type="button" id="button_select_option_2" text="PDF" image="" /></item><item type="separator" id="button_separator_4" /><item type="buttonSelect" id="button_select_8" text="Views" ><item type="button" id="button_select_option_13" text="Screening Transactions" image="" /><item type="button" id="button_select_option_14" text="Dashboard" image="" /><item type="button" id="button_select_option_15" text="Map" /><item type="button" id="button_select_option_16" text="Unassigned Devices" image="" /><item type="button" id="button_select_option_17" text="Unassigned Users" image="" /></item><item type="separator" id="button_separator_6" /><item type="buttonSelect" id="button_select_4" text="Actions" title="" ><item type="button" id="button_select_option_4" text="Screen Cargo" image="" /></item></toolbar>', function () { });

        var grid = cell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Shipping Reference#", "Sample#", "Method", "Device#", "Screened On", "Screened at", "Total Pieces", "Screened Pieces", "Result", "Screener", "Validated?"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ch");
        grid.setColSorting('str,str,str,str,str,str,str,str,str,str,str');
        grid.setInitWidths('200,*,*,*,*,*,*,*,*,*,*');
        //grid.enableColumnMove(true);
        grid.init();
        grid.enableSmartRendering(true);

        grid.attachEvent("onRowDblClicked", function (rId, cInd) {
            if (this.listener) {
                this.listener.call(null, rId);
            }
        }.bind(this));

        return grid;
    }.apply(this);

    searchPanelForm = function () {
        cell.showView('testa');
        var searchPanel = accordion.addItem('searchPanel', 'SCREENING TRANSACTIONS');
            var searchFormStructure = [
                    { type: "input", name: "form_input_reference", label: "Shipment#:", position: "label-left", labelWidth: 60, inputWidth: 210 },
                    { type: "input", name: "form_input_sample", label: "Sample#:", position: "label-left", labelWidth: 60, inputWidth: 210 },
                    { type: "calendar", name: "fromDateCalendar", label: "From Date:", position: "label-left", labelWidth: 60, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                    { type: "calendar", name: "toDateCalendar", label: "To Date:", position: "label-left", labelWidth: 60, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                    { type: "combo", name: "screeningLocations", label: "CCSF:", position: "label-left", labelWidth: 60, connector: 'api/ScreeningTransactions/GetScreeningLocations', inputWidth: 210 },
                    { type: "combo", name: "screeningMethods", label: "Method:", position: "label-left", labelWidth: 60, connector: "api/ScreeningTransactions/GetScreeningMethods", inputWidth: 210 },
                    { type: "combo", name: "screeningDevices", label: "Device:", position: "label-left", labelWidth: 60, connector: "api/ScreeningDevices/GetScreeningDevices", inputWidth: 210 },
                    { type: "combo", name: "screeners", label: "Screener:", position: "label-left", labelWidth: 60, connector: "api/Screeners/GetScreeners", inputWidth: 210 },
                    { type: "combo", name: "screeningResults", label: "Result:", position: "label-left", labelWidth: 60, connector: "api/ScreeningTransactions/GetScreeningResults", inputWidth: 210 },
                    { type: "combo", name: "screeningAreas", label: "Area:", position: "label-left", labelWidth: 60, connector: "api/ScreeningTransactions/GetAreas", inputWidth: 210 },
                    { type: "combo", name: "cctvCameras", label: "Camera:", position: "label-left", labelWidth: 60, connector: "api/ScreeningTransactions/GetCctvCameras", inputWidth: 210 },
                    {
                        type: "block", name: "form_block_1", list: [
                        { type: "checkbox", name: "form_checkbox_266", label: "List only validated shipments", position: "label-right" }]
                    },
                    { type: "block", list: [{ type: "button", name: "resetButton", value: "RESET", offsetLeft: 80 }, { type: "newcolumn" }, { type: "button", name: "filterButton", value: "SEARCH" }] }                    
            ];

            var searchPanelForm = searchPanel.attachForm(searchFormStructure);

            $this._fixCalendar('fromDateCalendar', searchPanelForm);
            $this._fixCalendar('toDateCalendar', searchPanelForm);

            var screeningLocations = searchPanelForm.getCombo('screeningLocations');
            screeningLocations.attachEvent("onChange", function (value) {
                $this._refreshCombo('screeningAreas', "api/ScreeningTransactions/GetAreas?locationId=" + value);
                $this._refreshCombo('cctvCameras', "api/ScreeningTransactions/GetCctvCameras?locationId=" + value);
                $this._filterScreeningDevices();
                $this._filterScreeners();
            });

            var customFilter = function (mask, option) {
                var r = false;
                if (mask.length == 0) {
                    r = true;
                } else if (option.text.toLowerCase().indexOf(mask.toLowerCase()) !== -1) {                
                    r = true;
                }
                return r;
            };

            screeningLocations.setFilterHandler(customFilter);

            var screeningMethods = searchPanelForm.getCombo('screeningMethods');
            screeningMethods.attachEvent("onChange", function (value) {
                $this._filterScreeningDevices();
            });
            screeningMethods.setFilterHandler(customFilter);

            var screeningDevices = searchPanelForm.getCombo('screeningDevices');
            screeningDevices.setFilterHandler(customFilter);

            var screeners = searchPanelForm.getCombo('screeners');
            screeners.setFilterHandler(customFilter);

            searchPanelForm.getCombo('screeningAreas').attachEvent("onChange", function (value) {
                $this._filterScreeningDevices();
            });

            searchPanelForm.attachEvent("onButtonClick", function (name) {
                if (name == 'filterButton') {
                    $this.filter();
                }else {
                    $this.resetControls();
                    $this._fixCalendar('fromDateCalendar', searchPanelForm);
                    $this._fixCalendar('toDateCalendar', searchPanelForm);
                }
            });

            return searchPanelForm;
    }();

    this._filter(searchPanelForm.getCalendar('fromDateCalendar').getFormatedDate(), searchPanelForm.getCalendar('toDateCalendar').getFormatedDate());
    return this;
};