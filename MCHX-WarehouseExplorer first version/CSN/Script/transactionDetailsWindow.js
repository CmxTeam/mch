﻿var transactionDetailsWindow = function () {        
    var $this = this;

    this.show = function (id) {
        $this.grid.clearAll();

        this.window.headerInfoForm.load('api/ScreeningTransactions/GetTransactionDetails?id=' + id, function () {
            var formData = this.getFormData();
            $this.window.headerInfoForm.setItemLabel('ShipmentReference', formData.ShipmentReference);
            $this.window.headerInfoForm.setItemLabel('ScreenedPieces', formData.ScreenedPieces + ' of ' + formData.TotalPieces);
            $this.window.headerInfoForm.setItemLabel('Validated', formData.Validated);
            $this.window.headerInfoForm.setItemLabel('Status', formData.ScreenedPieces < formData.TotalPieces ? 'Screening In Progress' : 'Screening Completed');

            $this.window.screeningDetailsForm.setItemLabel('sampleNumberLabelValue', formData.SampleNumber);
            $this.window.screeningDetailsForm.setItemLabel('methodLabelValue', formData.Method);
            $this.window.screeningDetailsForm.setItemLabel('deviceLabelValue', formData.Device);
            $this.window.screeningDetailsForm.setItemLabel('screenerLabelValue', formData.Screener);
            $this.window.screeningDetailsForm.setItemLabel('piecesLabelValue', formData.ScreenedPieces);
            $this.window.screeningDetailsForm.setItemLabel('resultLabelValue', formData.Result);
            $this.window.screeningDetailsForm.setItemLabel('timestampLabelValue', formData.Timestamp);
            $this.window.screeningDetailsForm.setItemLabel('locationLabelValue', formData.Location);
            $this.window.screeningDetailsForm.setItemLabel('commentLabelValue', formData.Comment);
                       
            dhx4.ajax.get("api/ScreeningTransactions/GetImagesForTransaction?id=" + id + '&method=' + formData.Method, function (a) {
                $this.viewData.parse(a.xmlDoc.response || a.xmlDoc.responseText, "json");                
            });

            $this.remarksCell.progressOn();
            $this.grid.load('api/ScreeningTransactions/GetShipmentRemarks?id=' + id, function () { $this.remarksCell.progressOff(); }, 'json');
        });
        
        this.window.setModal(1);
        this.window.show();
    }.bind(this);

    this.hide = function () {
        this.window.hide();
        this.window.setModal(0);
    }.bind(this);

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('transactionDetailsWindow', 0, 0, 900, 700);

        var layout = window.attachLayout('4I');

        var headerInfoCell = layout.cells('a');
        headerInfoCell.setHeight('50');
        headerInfoCell.hideHeader();
        headerInfoCell.fixSize(0, 1);

        var headerInfoFormStructure = [
		    { type: "label", label: "Shipping Reference#:" },
		    { type: "newcolumn" },
		    { type: "label", name: "ShipmentReference", labelWidth: 150 },
            { type: "newcolumn" },           
            { type: "label", label: "Screened Pieces#:" },
            { type: "newcolumn" },
            { type: "label", name: "ScreenedPieces", labelWidth: 75 },
            { type: "newcolumn" },           
            { type: "label", label: "Validated?" },
            { type: "newcolumn" },
            { type: "label", name: "Validated", labelWidth: 40 },
            { type: "newcolumn" },
            { type: "label", label: "Status:" },
            { type: "newcolumn" },
            { type: "label", name: "Status" }
        ];
        
        window.headerInfoForm = headerInfoCell.attachForm(headerInfoFormStructure);        
        
        var screeningDetailsCell = layout.cells('b');
        screeningDetailsCell.setText('SCREENING DETAILS');
        screeningDetailsCell.fixSize(1, 0);

        var screeningDetailsFormStructure = [
		{
		    type: "block", list: [
            { type: "label", label: "Sample#:", labelHeight: 14 },
            { type: "label", label: "Method:", labelHeight: 14 },
            { type: "label", label: "Device#:", labelHeight: 14 },
            { type: "label", label: "Screener:", labelHeight: 14 },
            { type: "label", label: "Pieces:", labelHeight: 14 },
            { type: "label", label: "Result:", labelHeight: 14 },
            { type: "label", label: "Timestamp:", labelHeight: 14 },
            { type: "label", label: "Location:", labelHeight: 14 },
            { type: "label", label: "Comment:", labelHeight: 14 }
		    ]
		},
		{ type: "newcolumn" },
		{
		    type: "block", list: [
            { type: "label", name: "sampleNumberLabelValue", labelHeight: 14},
            { type: "label", name: "methodLabelValue", labelHeight: 14 },
            { type: "label", name: "deviceLabelValue", labelHeight: 14 },
            { type: "label", name: "screenerLabelValue", labelHeight: 14 },
            { type: "label", name: "piecesLabelValue", labelHeight: 14 },
            { type: "label", name: "resultLabelValue", labelHeight: 14 },
            { type: "label", name: "timestampLabelValue", labelHeight: 14 },
            { type: "label", name: "locationLabelValue", labelHeight: 14 },
            { type: "label", name: "commentLabelValue", labelHeight: 14 }
		    ]
		}
        ];
        window.screeningDetailsForm = screeningDetailsCell.attachForm(screeningDetailsFormStructure);

        var imagesCell = layout.cells('c');
        imagesCell.setText('IMAGES');
        imagesCell.fixSize(1, 0);

        var mapItemTemplate = "<div>" +				
					    "<img style='max-width:400px; max-height:135px;' src='Content/deleteme/#ImageName#' />" +				
			        "</div>";

        this.viewData = imagesCell.attachDataView({
            type: {
                template: mapItemTemplate,
                height: 135,
                width: 400
            }
        });
        
        this.viewData.attachEvent("onItemClick", function (t, r, u) {
            
        });

        this.remarksCell = layout.cells('d');
        this.remarksCell.setText('REMARKS');

        var grid = this.remarksCell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["User", "Timestamp", "Remark"]);
        grid.setColTypes("ro,ro,ro");
        grid.setColSorting('str,str,str');
        grid.setInitWidths('200,200,*');        
        grid.init();        
        this.grid = grid;

        window.button('close').attachEvent('onClick', function () {
            window.setModal(0);
            window.hide();
        });

        window.setText('TRANSACTION DETAILS');
        window.hide();
        window.centerOnScreen();
        window.button('minmax').show();
        window.button('minmax').enable();

        this.window = window;
    }.apply(this);    
};