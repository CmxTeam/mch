﻿var liveScreening = function (info) {
    this.info = info;
    this.onSubmit = null;

    this._resetShipment = function () {
        this.form.setItemValue('shipmentReference', '');
        this.form.setItemValue('slac', '');
        this.form.setItemValue('shipper', '');
        this.form.setItemValue('consignee', '');
        this.form.setItemValue('origin', '');
        this.form.setItemValue('destination', '');
        this.form.setItemValue('totalPieces', '');
        this.form.setItemValue('totalSlac', '');
        this.form.setItemValue('totalWeight', '');
        this.form.setItemValue('decription', '');
        this.form.setItemValue('screeningStatus', '');
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        this.window = windows.createWindow('liveScreeningWindow', 0, 0, window.innerWidth, window.innerHeight);

        var tabbar = this.window.attachTabbar();

        tabbar.addTab('currentTab', 'CURRENT SCREENING TRANSACTION');
        var currentTab = tabbar.cells('currentTab');
        currentTab.setActive();

        tabbar.addTab('historyTab', 'PREVIOUS SCREENING TRANSACTIONS');
        var historyTab = tabbar.cells('historyTab');

        var formStructure = [
            {
                type: "fieldset", name: "step1fieldset", label: "Step 1: Screen Shipment", width: 500, height: 300, list: [
                    { type: "input", name: "shipmentReference", label: "Scan Shipment Reference#:", position: "label-top", inputWidth: 250, className: "label-bold label-big", offsetTop: 29, validate: "NotEmpty" },
                    { type: "input", name: "slac", label: "Screened SLAC:", position: "label-top", inputWidth: 250, className: "label-bold label-big", offsetTop: 31, validate: "NotEmpty,ValidNumeric" },
                    {
                        type: "block", list: [{ type: "button", name: "resetButton", value: "CANCEL", offsetLeft: 270 }, { type: "newcolumn" }, { type: "button", name: "submitButton", value: "SUBMIT" }]
                    }
                ]
            },
            {
                type: "fieldset", name: "step2fieldset", label: "Step 2: Shipment Details", list: [
                    { type: "input", name: "shipper", label: "Shipper:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "consignee", label: "Consignee:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "origin", label: "Origin:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "destination", label: "Dest:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "totalPieces", label: "Total Pieces:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "totalSlac", label: "Total SLAC:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "totalWeight", label: "Total Weight:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "decription", label: "Goods Description:", labelWidth: 100, position: "label-left", inputWidth: 330, rows: 3, readonly: true },
                ]
            },
            { type: "newcolumn" },
            {
                type: "fieldset", name: "step3fieldset", label: "Step 3: Current Screening Transaction Details", offsetLeft: 50, list: [
                    { type: "input", name: "location", value: this.info.location.text, label: "Screening Location:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "operator", label: "Operator:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "device", value: this.info.device.text, label: "Device#:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "transaction", label: "Transaction ID#:", labelWidth: 140, position: "label-left", inputWidth: 320, readonly: true },
                    { type: "input", name: "transactionDate", label: "Transaction Timestamp:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "screeningStatus", label: "Screening Status:", labelWidth: 140, position: "label-left", inputWidth: 320, readonly: true },
                    {
                        type: "block", list: [{ type: "button", name: "completeButton", value: "COMPLETE SCREENING", offsetLeft: 279 }]
                    }
                ]
            },
        ];

        var form = currentTab.attachForm(formStructure);

        form.attachEvent('onButtonClick', function (name) {
            if (name == 'resetButton') {
                this._resetShipment();
            } else if (name == 'submitButton') {
                if (form.validate()) {
                    this._getShipment.apply(this);
                }
            }
        }.bind(this));

        this._getShipment = function () {
            dhx4.ajax.get(window.WebApiUrl + "/LiveScreening/TryFindHawb?locationId=" + this.info.location.id +
                        '&number=' + this.form.getValues().shipmentReference +
                        '&deviceId=' + this.info.device.id +
                        '&userId=' + '44025' +
                        '&userName=' + 'CMX' +
                        '&slac=' + this.form.getValues().slac, function (a) {
                            var data = JSON.parse(a.xmlDoc.response || a.xmlDoc.responseText);
                            if (data) {
                                this.form.setItemValue('shipper', data.Shipper.Name);
                                this.form.setItemValue('consignee', data.Consignee.Name);
                                this.form.setItemValue('origin', data.Origin.Name);
                                this.form.setItemValue('destination', data.Destination.Name);
                                this.form.setItemValue('totalPieces', data.TotalPieces);
                                this.form.setItemValue('totalSlac', data.TotalSlac);
                                this.form.setItemValue('totalWeight', data.TotalWeight);
                                this.form.setItemValue('decription', data.GoodsDescription);
                                this.form.setItemValue('screeningStatus', 'Item In Screening Queue');
                            } else {
                                info.shipment = this.form.getValues().shipmentReference;
                                info.slac = this.form.getValues().slac;
                                var _liveScreeningCreateHawb = new liveScreeningCreateHawb(info);
                                _liveScreeningCreateHawb.onSubmit = function () {
                                    this._getShipment.apply(this);
                                }.bind(this);
                            }
                        }.bind(this));
        }

        this.window.setText('LIVE SCREENING');
        this.window.setModal(1);
        this.window.denyResize();
        this.window.centerOnScreen();
        this.window.denyResize();
        this.window.denyMove();
        this.window.button('park').hide();
        this.window.button('minmax').hide();
        this.form = form;
    }.apply(this);
};