﻿var screenerModule = function (cell, accordion) {
    var grid, tree;
    var $this = this;

    this.show = function () {
        cell.setText('SCREENERS');
        cell.showView('screenersGridContainer');
    }

    this.filter = function (screener) {
        cell.progressOn();
        grid.clearAll();        
        grid.load('api/Screeners/GetGridData?screener=' + screener, function () { cell.progressOff(); }, 'json');
    }

    this.isNumeric = function (input) {
        return (input - 0) == input && ('' + input).trim().length > 0;
    }

    grid = function () {
        cell.showView('screenersGridContainer');
        var grid = cell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Id", "Photo", "Name", "Assigned To CCSF", "Work Phone", "Work Email", "Cell Phone"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
        grid.setColSorting('str,str,str,str,str,str,str');
        grid.setInitWidths('*,*,*,*,*,*,*');
        grid.setColumnHidden(0, true);
        //grid.enableColumnMove(true);
        grid.init();
        return grid;
    }();

    tree = function () {
        var screenersContainer = accordion.addItem('screenersContainer', 'SCREENERS');
        var toolbar = screenersContainer.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar><item type="buttonInput" id="button_input_2" value="" /><item type="buttonSelect" id="button_select_2" text="New" title="" /></toolbar>', function () { });

        toolbar.attachEvent('onEnter', function (name, value) {
            tree.setXMLAutoLoading('Screeners/GetScreenersTree?filter=' + value);
            tree.refreshItem(0);
        });

        var tree = screenersContainer.attachTree();
        tree.setImagePath(dhtmlx.tree_image_path);
        tree.loadXML('api/Screeners/GetScreenersTree');
        tree.attachEvent("onClick", function (id) {            
                $this.filter(id);            
        });       
        return tree;
    }();

    this.filter();
    return this;
};