﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSN.Controllers
{    
    public class HomeController : Controller
    {
        //public HomeController()
        //    : this(new UserManager<CSN.Models.IdentityModels.ApplicationUser>(new UserStore<CSN.Models.IdentityModels.ApplicationUser>(new CSN.Models.IdentityModels.ApplicationDbContext())))
        //{
        //}

        //public HomeController(UserManager<CSN.Models.IdentityModels.ApplicationUser> userManager)
        //{
        //    UserManager = userManager;
        //}

        //public UserManager<CSN.Models.IdentityModels.ApplicationUser> UserManager { get; private set; }
        
        public ActionResult Index()
        {
            return View();
        }        
    }
}