﻿using DhtmlxComponents.DataProviders;

namespace CSN.DataProviders
{
    public class CsnBaseDataProvider : BaseDataProvider, IDataProvider
    {
        public override string DatabaseName
        {
            get { return "CMXGlobalDB"; }
        }
    }
}