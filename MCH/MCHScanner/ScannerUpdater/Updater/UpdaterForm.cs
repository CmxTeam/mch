﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Updater
{
    public partial class UpdaterForm : Form
    {
        List<int> RecID = new List<int>();
        public UpdaterForm()
        {
            InitializeComponent();
        }

        private void treeView1_Click(object sender, EventArgs e)
        {

        }

        private void Updater_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'scannerUpdate._ScannerUpdate' table. You can move, or remove it, as needed.
            this.scannerUpdateTableAdapter.Fill(this.scannerUpdate._ScannerUpdate);
            checkedListBox1.Items.Clear();
            RecID.Clear();
            for (int i = 0; i < scannerUpdate._ScannerUpdate.Count; i++)
            {
                RecID.Add(scannerUpdate._ScannerUpdate[i].RecID);
                StringBuilder str = new StringBuilder();
                str.Append("Ver: ");
                str.Append(scannerUpdate._ScannerUpdate[i].RecID);
                str.Append(", ");
                str.Append(scannerUpdate._ScannerUpdate[i].RecDate);

                str.Append(", ");

                str.Append(scannerUpdate._ScannerUpdate[i].Description);

                checkedListBox1.Items.Add(str.ToString(), scannerUpdate._ScannerUpdate[i].IsActive);
            
            }

            buttonApply.Enabled = false;
            buttonDetails.Enabled = false;
             
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            ScannerUpdater form = new ScannerUpdater(RecID[checkedListBox1.SelectedIndex], scannerUpdate._ScannerUpdate[checkedListBox1.SelectedIndex].Description);
            form.ShowDialog();
            form.Dispose();
            Updater_Load(this, e);
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonDetails.Enabled = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ScannerUpdater form = new ScannerUpdater();
            form.ShowDialog();
            form.Dispose();
            Updater_Load(this, e);
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];

            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "update scannerupdate set isActive = 'false'";
                        comm.CommandType = CommandType.Text;
                        
                        comm.Connection.Open();
                        comm.ExecuteNonQuery();
                        comm.Connection.Close();

                    }

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        StringBuilder str = new StringBuilder();
                        str.Append("update scannerupdate set isActive = 'true' where recid in (");
                        
                        int i = 0;
                        CheckedListBox.CheckedIndexCollection indexes = checkedListBox1.CheckedIndices;
                        for (i = 0; i < indexes.Count - 1; i++)
                        {
                            str.Append(RecID[indexes[i]]);
                            str.Append(",");
                        }
                        str.Append(RecID[indexes[i]]);
                        str.Append(")");

                        comm.CommandText = str.ToString();
                        comm.CommandType = CommandType.Text;
                        //comm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;


                        comm.Connection.Open();
                        comm.ExecuteNonQuery();
                        comm.Connection.Close();
                    }
                }

            }
            buttonApply.Enabled = false;
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            buttonApply.Enabled = true;
        }
    }
}
