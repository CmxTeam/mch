﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;


namespace Updater
{
    public partial class ScannerUpdater : Form
    {

        List<string> m_filesList;
        int _versionRecID = -1;
        List<int> _gatewayRecID = new List<int>();
        public ScannerUpdater()
        {
            InitializeComponent();

        }

        public ScannerUpdater(int recID, string label)
        {
            InitializeComponent();
            button1.Enabled = false;
            _versionRecID = recID;
            textBox1.Text = label;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                m_filesList = new List<string>();
                GetFilesList(folderBrowserDialog1.SelectedPath, folderBrowserDialog1.SelectedPath, ref m_filesList);
                label1.Text = folderBrowserDialog1.SelectedPath;
                for (int i = 0; i < m_filesList.Count; i++)
                {
                    m_filesList[i] = m_filesList[i].Remove(0, folderBrowserDialog1.SelectedPath.Length);
                    checkedListBox1.Items.Add(m_filesList[i], true);

                }
            }


            //DataSet ds = new DataSet("GetFilesList");

        }

        private void GetFilesList(string path, string folder, ref List<string> filesList)
        {
            //string[] dirs = Directory.GetDirectories(folder);
            filesList.AddRange(Directory.GetFiles(folder));


            foreach (string dir in Directory.GetDirectories(folder))
            {
                GetFilesList(path, dir, ref filesList);

                //string trim = dir.Remove(0, path.Length);// "\\" + s.TrimStart(folderBrowserDialog1.SelectedPath.ToCharArray());

            }

        }


        //private int CreateNewVersion()
        //{
        //    ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];
        //    int version = 0;
        //    if (connectionString != null)
        //    {
        //        using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
        //        {
        //            using (SqlCommand comm = con.CreateCommand())
        //            {
        //                comm.CommandText = "Insert Into dbo.ScannerUpdate (RecDate, IsActive, Description) VALUEs (GETDATE(), @Activate, @Label); SET @RedID = SCOPE_IDENTITY()";
        //                comm.CommandType = CommandType.Text;
        //                comm.Parameters.Add("@Activate", SqlDbType.Bit).Value = false;// checkBoxActivate.Checked;
        //                comm.Parameters.Add("@Label", SqlDbType.VarChar).Value = textBox1.Text;

        //                comm.Parameters.Add("@RecID", SqlDbType.Int).Direction =  ParameterDirection.Output;
        //                comm.Connection.Open();
        //                comm.ExecuteNonQuery();
        //                comm.Connection.Close();

        //                //SqlDataAdapter adapter = new SqlDataAdapter(comm);
        //                version = (int) comm.Parameters["@RecID"].Value;
        //            }
        //        }

        //    }
        //    return version;
        //}
        private int CreateNewVersion()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];
            int version = 0;
            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "Insert Into dbo.ScannerUpdate (RecDate, IsActive, Description) VALUEs (GETDATE(), @Activate, @Label); SET @ID = SCOPE_IDENTITY()";
                        //comm.CommandText = "Insert Into dbo.ScannerUpdate (RecDate, IsActive) VALUEs (GETDATE(), @Activate); SET @ID = SCOPE_IDENTITY()";
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.Add("@Activate", SqlDbType.Bit).Value = false;
                        comm.Parameters.Add("@Label", SqlDbType.VarChar).Value = textBox1.Text;
                        comm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comm.Connection.Open();
                        comm.ExecuteNonQuery();
                        comm.Connection.Close();

                        //SqlDataAdapter adapter = new SqlDataAdapter(comm);
                        version = (int)comm.Parameters["@ID"].Value;
                    }
                }

            }
            return version;
        }
        private void UpdateVersion(int version)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];

            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "update scannerupdate set Description = @Description where recid = @RecID";

                        comm.CommandType = CommandType.Text;
                        comm.Parameters.Add("@Description", SqlDbType.VarChar).Value = textBox1.Text;
                        comm.Parameters.Add("@RecID", SqlDbType.Int).Value = version;

                        comm.Connection.Open();
                        comm.ExecuteNonQuery();
                        comm.Connection.Close();



                    }
                }

            }

        }

        void LinkGateways(int version)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];

            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        for (int i = 0; i < checkedListBoxGateway.CheckedItems.Count; i++)
                        {
                            comm.CommandText = "Insert Into dbo.ScannerUpdateGateway (RecDate, ScannerUpdateID, HostPlusLocationID) VALUEs (GETDATE(), @ScannerUpdate, @LocationID); SET @ID = SCOPE_IDENTITY()";
                            comm.CommandType = CommandType.Text;
                            comm.Parameters.Add("@ScannerUpdate", SqlDbType.Int).Value = version;// checkBoxActivate.Checked;
                            comm.Parameters.Add("@HostPlusLocationID", SqlDbType.Int).Value = i;// checkedListBoxGateway.CheckedItems[i];// checkBoxActivate.Checked;


                            comm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comm.Connection.Open();
                            comm.ExecuteNonQuery();
                        }
                        comm.Connection.Close();

                        //SqlDataAdapter adapter = new SqlDataAdapter(comm);

                    }
                }

            }

        }
        private void UploadFile(int version, string name, string fullpath)
        {
            byte[] fileData = null;
            if (System.IO.File.Exists(fullpath))
            {
                fileData = System.IO.File.ReadAllBytes(fullpath);
                CreateFileInDB(version, name, fileData);


            }


        }
        void CreateFileInDB(int version, string fileName, byte[] fileData)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];

            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "Insert Into dbo.ScannerUpdateFiles (RecDate, ScannerUpdateID, FileData, FilePath) VALUEs (GETDATE(), @Version, @fileData, @path)";
                        comm.CommandType = CommandType.Text;
                        //comm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comm.Parameters.Add("@Version", SqlDbType.Int).Value = version;
                        comm.Parameters.Add("@fileData", SqlDbType.Binary).Value = fileData;
                        comm.Parameters.Add("@path", SqlDbType.NVarChar).Value = fileName;

                        comm.Connection.Open();
                        comm.ExecuteNonQuery();
                        comm.Connection.Close();

                        //SqlDataAdapter adapter = new SqlDataAdapter(comm);

                    }
                }

            }


        }

        private void ScannerUpdater_Load(object sender, EventArgs e)
        {

            // TODO: This line of code loads data into the 'hostPlusDBDataSet.HostPlus_Locations' table. You can move, or remove it, as needed.
            //this.hostPlus_LocationsTableAdapter.Fill(this.hostPlusDBDataSet.HostPlus_Locations);
            // TODO: This line of code loads data into the 'scannerUpdateGateway._ScannerUpdateGateway' table. You can move, or remove it, as needed.
            this.scannerUpdateGatewayTableAdapter.Fill(this.scannerUpdateGateway._ScannerUpdateGateway);

            _gatewayRecID.Clear();
            checkedListBoxGateway.Items.Clear();
            // TODO: This line of code loads data into the 'hostPlusDBDataSet.HostPlus_Locations' table. You can move, or remove it, as needed.
            this.hostPlus_LocationsTableAdapter.Fill(this.hostPlusDBDataSet.HostPlus_Locations);

            List<int> list = null;
            if (_versionRecID >= 0)
            {
                list = Gateways(_versionRecID);
            }

            for (int i = 0; i < hostPlusDBDataSet.HostPlus_Locations.Count; i++)
            {
                if (_versionRecID >= 0)
                {
                    if (list.Contains(hostPlusDBDataSet.HostPlus_Locations[i].RecId))
                    {
                        checkedListBoxGateway.Items.Add(hostPlusDBDataSet.HostPlus_Locations[i].Location, true);

                    }
                    else
                    {
                        checkedListBoxGateway.Items.Add(hostPlusDBDataSet.HostPlus_Locations[i].Location, false);

                    }


                }
                else
                {
                    checkedListBoxGateway.Items.Add(hostPlusDBDataSet.HostPlus_Locations[i].Location, true);
                }

                _gatewayRecID.Add(hostPlusDBDataSet.HostPlus_Locations[i].RecId);

            }

            if (_versionRecID >= 0)
            {
                checkedListBox1.Enabled = false;
                LoadFilesFromDB(_versionRecID);
            }

        }
        void LoadFilesFromDB(int version)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];
            DataSet ds = new DataSet("Files");
            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandType = CommandType.Text;
                        SqlDataAdapter adapter = new SqlDataAdapter(comm);

                        comm.CommandText = "select FilePath  from ScannerUpdateFiles where ScannerUpdateID = @version";

                        comm.Parameters.Add("@version", SqlDbType.BigInt).Value = version;
                        adapter.Fill(ds);


                        if (ds.Tables.Count > 0)
                        {
                            for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                            {
                                DataRow dr = ds.Tables[0].Rows[k];
                                checkedListBox1.Items.Add(Convert.ToString(dr["FilePath"]));


                            }
                        }
                        ds.Clear();
                        adapter.Dispose();
                    }
                }
            }
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {

            //LinkGateways(version);


        }

        List<int> Gateways(int version)
        {
            List<int> list = new List<int>();
            DataSet ds = new DataSet("Gateways");
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];

            if (connectionString != null)
            {
                using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                {
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandType = CommandType.Text;
                        SqlDataAdapter adapter = new SqlDataAdapter(comm);

                        comm.CommandText = "select HostPlusLocationID  from ScannerUpdateGateway where ScannerUpdateID = @version";

                        comm.Parameters.Add("@version", SqlDbType.BigInt).Value = version;
                        adapter.Fill(ds);


                        if (ds.Tables.Count > 0)
                        {
                            for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                            {
                                DataRow dr = ds.Tables[0].Rows[k];
                                int locationID = Convert.ToInt32(dr["HostPlusLocationID"]);
                                list.Add(locationID);

                            }
                        }
                        ds.Clear();
                        adapter.Dispose();
                    }
                }
            }

            return list;

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {

            if (_versionRecID < 0)
            {
                _versionRecID = CreateNewVersion();
                foreach (string file in checkedListBox1.CheckedItems)
                {
                    UploadFile(_versionRecID, file, folderBrowserDialog1.SelectedPath + file);

                }

                MessageBox.Show("Scanner upload files are complete");
                checkedListBox1.Items.Clear();

            }
            else
            {
                UpdateVersion(_versionRecID);
            }
            if (_versionRecID >= 0)
            {

                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["CMXGlobalDB"];

                if (connectionString != null)
                {
                    using (SqlConnection con = new SqlConnection(connectionString.ConnectionString))
                    {
                        using (SqlCommand comm = con.CreateCommand())
                        {

                            comm.CommandText = "Delete from ScannerUpdateGateway where ScannerUpdateID = @version";
                            comm.CommandType = CommandType.Text;
                            //comm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comm.Parameters.Add("@Version", SqlDbType.Int).Value = _versionRecID;

                            comm.Connection.Open();
                            comm.ExecuteNonQuery();
                            comm.Connection.Close();

                        }
                        if (checkedListBoxGateway.CheckedItems.Count > 0)
                        {

                            using (SqlCommand comm = con.CreateCommand())
                            {
                                StringBuilder str = new StringBuilder();
                                str.Append("INSERT Into ScannerUpdateGateway(ScannerUpdateID, HostPlusLocationID, RecDate)");
                                str.Append(" Select ");
                                str.Append(_versionRecID);
                                str.Append(", RecID, GETDATE() from HostPlus_Locations where recid in (");

                                int i = 0;
                                CheckedListBox.CheckedIndexCollection indexes = checkedListBoxGateway.CheckedIndices;
                                for (i = 0; i < indexes.Count - 1; i++)
                                {
                                    str.Append(_gatewayRecID[indexes[i]]);
                                    str.Append(",");
                                }
                                str.Append(_gatewayRecID[indexes[i]]);
                                str.Append(")");

                                comm.CommandText = str.ToString();
                                comm.CommandType = CommandType.Text;
                                //comm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;


                                comm.Connection.Open();
                                comm.ExecuteNonQuery();
                                comm.Connection.Close();

                            }

                        }


                    }
                }
            }

            DialogResult = DialogResult.OK;
        }

        private void checkBoxCheckAll_CheckedChanged(object sender, EventArgs e)
        {

            for (int i = 0; i < checkedListBoxGateway.Items.Count; i++)
                checkedListBoxGateway.SetItemChecked(i, checkBoxCheckAll.Checked);

        }

    }
}
