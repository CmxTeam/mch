﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Parsing.Results
{
    public enum ResultTypeEnum
    {
        NA,
        HouseBill,
        MasterBill,
        Uld,
        Door,
        Area,
        Truck,
        User,
        ScreeningArea,
        Printer
    }
}
