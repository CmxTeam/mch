﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.TaskManager
{
    public enum TaskProgressTypeEnum
    {
        NotStarted,
        NotAssigned,
        InProgress,
        Completed
    }
}
