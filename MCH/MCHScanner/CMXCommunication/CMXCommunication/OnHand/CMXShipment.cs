﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.OnHand;

namespace CargoMatrix.Communication.WSOnHand
{
    public partial class CMXShipment : IShipment
    {
        #region IShipment Members

        public string ShipmentNO
        {
            get { return this.domesticNoField; }
        }

        public string MOT
        {
            get { return this.transportModeField; }
        }

        public string CustomerCode
        {
            get { return this.accountField; }
        }

        public string Vendor
        {
            get { return null; }
        }

        public string TruckingCompany
        {
            get { return this.carrierField.CarrierName; }
        }

        public long ShipmentID
        {
            get { return this.domesticRecIDField ?? 0; }
        }

        public int TotalPieces
        {
            get { return this.piecesField; }
        }

        public string PONumber
        {
            get { return null; }
        }

        ShipmentStatus IShipment.Status
        {
            get { return this.shipmentStatusField; }
        }

        #endregion
    }
}
