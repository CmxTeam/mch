﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.WSLoadConsol
{
    public partial class HouseBillItem : CargoMatrix.Communication.DTO.IHouseBillItem
    {
        #region IHouseBillItem Members


        public int Slac
        {
            get { return this.totalSlacField; }
        }

        int CargoMatrix.Communication.DTO.IHouseBillItem.Weight
        {
            get { return (int)this.weightField; }
        }

        public string Location
        {
            get { return this.lastLocationField; }
        }

        CargoMatrix.Communication.DTO.ScanModes CargoMatrix.Communication.DTO.IHouseBillItem.ScanMode
        {
            get
            {
                return (CargoMatrix.Communication.DTO.ScanModes)Enum.Parse(typeof(CargoMatrix.Communication.DTO.ScanModes), this.scanModeField.ToString(), true); ;
            }
        }

        public CargoMatrix.Communication.DTO.HouseBillStatuses status
        {
            get
            {
                switch (this.statusField)
                {
                    case HouseBillStatuses.All:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
                    case HouseBillStatuses.Completed:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Completed;
                    case HouseBillStatuses.InProgress:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress;
                    case HouseBillStatuses.Open:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Open;
                    case HouseBillStatuses.Pending:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Pending;
                    default:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
                }
            }
        }

        CargoMatrix.Communication.DTO.RemoveModes CargoMatrix.Communication.DTO.IHouseBillItem.RemoveMode
        {
            get
            {
                switch (this.removeModeField)
                {
                    case RemoveModes.NA:
                        return CargoMatrix.Communication.DTO.RemoveModes.NA;
                    case RemoveModes.All:
                        return CargoMatrix.Communication.DTO.RemoveModes.All;
                    case RemoveModes.Shortage:
                        return CargoMatrix.Communication.DTO.RemoveModes.Shortage;
                    case RemoveModes.TakeOff:
                        return CargoMatrix.Communication.DTO.RemoveModes.TakeOff;
                    default :
                        return CargoMatrix.Communication.DTO.RemoveModes.NA;
                };
            }
        }

        #endregion
    }
}
