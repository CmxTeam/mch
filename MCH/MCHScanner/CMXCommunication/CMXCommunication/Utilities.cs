﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace CargoMatrix.Communication
{
    public class Utilities
    {
        /// <summary>
        /// returns true if currently loged in user is Admin
        /// </summary>
        public static bool IsAdmin
        {
            get { return CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin; }
        }

        public static bool CameraPresent
        {
            get
            {
                try
                {
                    return Microsoft.WindowsMobile.Status.SystemState.CameraPresent;

                }
                catch (Exception)
                {

                    return false;
                }
            }
        }
        /// <summary>
        /// returns currenlty installed version of the application (reads from Versio.txt file)
        /// or -1 if error occured while reading the file
        /// </summary>
        public static int AppCurrentVersion
        {
            get
            {
                string currentpath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
                string versionFilePath = currentpath + @"\Version.txt";
                try
                {
                    using (FileStream fs = System.IO.File.Open(versionFilePath, System.IO.FileMode.OpenOrCreate))
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(fs);
                        return Convert.ToInt32(sr.ReadLine());
                    }
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        public static System.Drawing.Image ConvertToImage(string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(str);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);
            //Image image = Image.FromStream(ms, true);
            return bmp;
        }

        public static T Invoke<T>(Func<T> func, Action<SoapExceptionManager.SoapExceptionDetails> errorAction, int errroCode, T defaultReturn)
        {
            try
            {
                return func();
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Code == System.Web.Services.Protocols.SoapException.ClientFaultCode)
                {
                    if (errorAction != null)
                    {
                        var details = SoapExceptionManager.SoapExceptionDetails.Handle(ex);
                        errorAction(details);
                    }
                    else
                    {
                        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, errroCode);
                    }

                    return defaultReturn;
                }
                else
                {
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, errroCode);

                    return defaultReturn;
                }
            }

            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, errroCode);

                return defaultReturn;
            }

        }

        public static T Invoke<T>(Func<T> func, int errroCode, T defaultReturn)
        {
            return Invoke<T>(func, null, errroCode, defaultReturn);
        }

        public static void Invoke(Action action, int errroCode)
        {
            Func<bool> func = () =>
            {
                action();

                return true;
            };

            Invoke<bool>(func, errroCode, true);
        }

    }
}
