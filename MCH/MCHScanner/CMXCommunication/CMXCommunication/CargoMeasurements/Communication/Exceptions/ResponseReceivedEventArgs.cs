﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions
{
    public class ResponseReceivedEventArgs : EventArgs
    {
        CommandValues command;

        public CommandValues Command
        {
            get { return command; }
            set { command = value; }
        }

        public ResponseReceivedEventArgs(CommandValues command)
        {
            this.command = command;
        }
    }
}
