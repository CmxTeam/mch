﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements
{
    public enum MetricSystemTypeEnum
    {
        Metric,
        Imperial
    }
}
