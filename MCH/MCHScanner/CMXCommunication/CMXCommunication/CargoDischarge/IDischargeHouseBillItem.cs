﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CargoMatrix.Communication.WSCargoDischarge;

namespace CargoMatrix.Communication.CargoDischarge
{
    public interface IDischargeHouseBillItem : Communication.DTO.IHouseBillItem
    {
        DateTime ReleaseTime { get; }
        
        string DriverFullName { get; }

        /// <summary>
        /// Shipment sender
        /// </summary>
        string SenderName { get; }
        
        /// <summary>
        /// Consignee
        /// </summary>
        string ReceiverName{ get; }

        /// <summary>
        /// Trucking 3rd party company
        /// </summary>
        string CompanyName { get; }


        string AliasNumber { get; }

        string ScanName { get; }

        string Description { get; }

        int TaskID
        {
            get;
        }

        PieceItem[] Pieces
        {
            get;
        }

        int ScannedSlac
        {
            get;
        }

        int ReleseSlac
        {
            get;
        }

        int AvailableSlac
        {
            get;
        }

        int AvailablePieces
        {
            get;
        }

        ShipmentAlert[] ShipmentAlerts
        {
            get;
        }

        string MasterBillNumber
        {
            get;
        }

        TransactionStatus TransactionStatus
        {
            get;
        }

        ReleaseStatuses ReleaseStatus
        {
            get;
        }

    }


    public static class DischargeItemHelper
    {
        public static int CalculateMaxReleaseSlac(this IDischargeHouseBillItem houseBillItem, int piecesCountUser)
        {
            return houseBillItem.ReleseSlac - houseBillItem.ScannedSlac - (houseBillItem.AvailablePieces - piecesCountUser);
        }
    }
}
