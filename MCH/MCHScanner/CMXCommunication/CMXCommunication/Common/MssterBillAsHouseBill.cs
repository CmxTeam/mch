﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.Common
{
    public class MssterBillAsHouseBill : IHouseBillItem
    {
        IMasterBillItem masterBill;

        public MssterBillAsHouseBill(IMasterBillItem masterBill)
        {
            this.masterBill = masterBill;
        }

        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get
            {
                return this.masterBill.MasterBillNumber;
            }
        }

        public int HousebillId
        {
            get
            {
                return this.masterBill.MasterBillId;
            }
        }

        public string Origin
        {
            get
            {
                return this.masterBill.Origin;
            }
        }

        public string Destination
        {
            get
            {
                return this.masterBill.Destination;
            }
        }

        public int TotalPieces
        {
            get
            {
                return this.masterBill.Pieces;
            }
        }

        public int ScannedPieces
        {
            get
            {
                return this.masterBill.ScannedPieces;
            }
        }

        public int Slac
        {
            get
            {
                return this.masterBill.Slac;
            }
        }

        public int Weight
        {
            get
            {
                return this.masterBill.Weight;
            }
        }

        public string Location
        {
            get
            {
                return string.Empty;
            }
        }

        public int Flags
        {
            get
            {
                return this.masterBill.Flags;
            }
        }

        public ScanModes ScanMode
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public HouseBillStatuses status
        {
            get
            {
                switch (this.masterBill.Status)
                {
                    case MasterbillStatus.All: return HouseBillStatuses.All;

                    case MasterbillStatus.Completed: return HouseBillStatuses.Completed;

                    case MasterbillStatus.InProgress: return HouseBillStatuses.InProgress;

                    case MasterbillStatus.Open: return HouseBillStatuses.Open;

                    case MasterbillStatus.Pending: return HouseBillStatuses.Pending;

                    default: throw new NotImplementedException();

                }
            }
        }

        public RemoveModes RemoveMode
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
