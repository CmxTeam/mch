﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common
{
    public enum FilteringFieldsEnum
    {
        NOT_STARTED = 1,
        IN_PROGRESS = 2,
        COMPLETED = 3,
        NOT_COMPLETED = 4,
        ALL = 5
    }
}
