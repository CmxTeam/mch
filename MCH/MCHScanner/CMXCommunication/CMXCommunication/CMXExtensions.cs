﻿using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.ScannerUtilityWS;
using System;
namespace CMXExtensions
{
    public static class CMXExtension
    {
        public static string Reference(this IMasterBillItem mawb)
        {
            return string.Format("{0}-{1}-{2}-{3}", mawb.Origin, mawb.CarrierNumber, mawb.MasterBillNumber, mawb.Destination);
        }
        public static string Line2(this IMasterBillItem mawb) { return string.Format("HBs: {0} WT: {1}KGS", mawb.HouseBills, mawb.Weight); }
        public static string Line3(this IMasterBillItem mawb) { return string.Format("ULDs: {0}", mawb.Ulds); }

        public static string Reference(this IHouseBillItem hawb)
        {
            return string.Format("{0}-{1}-{2}", hawb.Origin, hawb.HousebillNumber, hawb.Destination);
        }
        public static string Line2(this IHouseBillItem hawb) { return string.Format("Weight: {0} KGS", hawb.Weight); }
        public static string Line3(this IHouseBillItem hawb) { return string.Format("Pieces: {0} of {1}  Slac: {2}", hawb.ScannedPieces, hawb.TotalPieces, hawb.Slac); }
        public static string Line4(this IHouseBillItem hawb) { return string.Format("Location: {0}", hawb.Location); }

        public static string Prefix(this LocationItem location)
        {
            return string.IsNullOrEmpty(location.LocationBarcode) == true ? string.Empty : location.LocationBarcode[0].ToString();
        }
        


        public static bool IsLoose(this IULD uld)
        {
            return string.Equals(uld.ULDType, "loose", StringComparison.OrdinalIgnoreCase);
        }
    }
}