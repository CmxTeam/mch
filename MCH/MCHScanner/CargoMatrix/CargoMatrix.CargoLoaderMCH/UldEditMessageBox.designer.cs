﻿namespace CustomUtilities
{
    partial class UldEditMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
      

            
            this.lblType = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.lblPrefix = new System.Windows.Forms.Label();

            this.cmxTextBoxNumber = new CargoMatrix.UI.CMXTextBox();
            this.cmxTextBoxPrefix = new CargoMatrix.UI.CMXTextBox();
        
     

            this.panel1.SuspendLayout();
            this.SuspendLayout();
       

            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonBrowse);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);

            this.panel1.Controls.Add(this.cmxTextBoxNumber);
            this.panel1.Controls.Add(this.cmxTextBoxPrefix);

            this.panel1.Controls.Add(this.lblType);
            this.panel1.Controls.Add(this.lblNumber);
            this.panel1.Controls.Add(this.lblPrefix);

            this.panel1.Location = new System.Drawing.Point(3, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 171);
   

            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 130);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
   

            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(56, 130);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);


            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.Location = new System.Drawing.Point(131, 64);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(40, 28);
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
        

            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 126);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            

            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
           
 
       
    
 
            // 
            // label2
            // 
            this.lblType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblType.Location = new System.Drawing.Point(12, 36);
            this.lblType.Name = "label2";
            this.lblType.Size = new System.Drawing.Size(170, 20);
            this.lblType.Text = "";
            // 
            // label3
            // 



            this.lblPrefix.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblPrefix.Location = new System.Drawing.Point(12,68);
            this.lblPrefix.Name = "lblPrefix";
            this.lblPrefix.Size = new System.Drawing.Size(57, 20);
            this.lblPrefix.Text = "Prefix:";
            this.lblPrefix.TextAlign = System.Drawing.ContentAlignment.TopCenter;


            this.lblNumber.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblNumber.Location = new System.Drawing.Point(12, 100);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(57, 20);
            this.lblNumber.Text = "Number:";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;



            this.cmxTextBoxNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxNumber.Location = new System.Drawing.Point(75,97);
            this.cmxTextBoxNumber.MaxLength = 5;
            this.cmxTextBoxNumber.Name = "cmxTextBoxNumber";
            this.cmxTextBoxNumber.Size = new System.Drawing.Size(137, 26);
            this.cmxTextBoxNumber.TabIndex = 3;
            this.cmxTextBoxNumber.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;


            this.cmxTextBoxPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxPrefix.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxPrefix.Location = new System.Drawing.Point(76, 65);
            this.cmxTextBoxPrefix.MaxLength = 3;
            this.cmxTextBoxPrefix.Name = "cmxTextBoxPrefix";
            this.cmxTextBoxPrefix.Size = new System.Drawing.Size(51, 26);
            this.cmxTextBoxPrefix.TabIndex = 3;
            this.cmxTextBoxPrefix.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Alphabetic;




            InitializeImages();
      
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "OriginDestinationMessageBox";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private System.Windows.Forms.PictureBox pictureBox1;


      
        private System.Windows.Forms.Label lblType;

        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblPrefix;

        public CargoMatrix.UI.CMXTextBox cmxTextBoxNumber;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxPrefix;

      

    }
}