﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using System.Collections.Generic;
using System.Text;
namespace CargoMatrix.CargoLoader
{


    public partial class UldViewerCargoItem : CustomListItems.ExpandableRenderListitem<UldViewItem>, ISmartListItem
    {
        

        public event EventHandler ButtonClick;

        public UldViewerCargoItem(UldViewItem shipment)
            : base(shipment)
        {
            InitializeComponent();

          
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonDelete_Click(object sender, System.EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, EventArgs.Empty);
            }
        }


        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonDelete = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDelete.Location = new System.Drawing.Point(198, 4);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Image = Resources.Skin.cc_trash;
            this.buttonDelete.PressedImage = Resources.Skin.cc_trash_over;
            this.buttonDelete.Size = new System.Drawing.Size(32, 32);
            this.buttonDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelete.TransparentColor = System.Drawing.Color.White;
            this.buttonDelete.Click += new System.EventHandler(buttonDelete_Click);




            this.Controls.Add(this.buttonDelete);
          


            this.buttonDelete.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.ResumeLayout(false);
        }

        

        private string GetLocationLine(string[] locations)
        {
            StringBuilder sb = new StringBuilder("");
            foreach (string l in locations)
            {
                if (sb.ToString() == string.Empty)
                {
                    sb.Append(l);
                }
                else
                {
                    sb.Append(", " + l);
                }
            }
            return sb.ToString();
        }

  

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("AWB: {0}", ItemData.AWBNumber); 
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line2 = string.Format("Pcs: {0}", ItemData.LoadCount);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 17 * vScale, 170 * hScale, 13 * vScale));
                }


                Image img;
                    IsSelected = false;
                    img = CargoMatrix.Resources.Icons.unselected;


           
            
                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = string.Empty;
            content = ItemData.AWBNumber;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
