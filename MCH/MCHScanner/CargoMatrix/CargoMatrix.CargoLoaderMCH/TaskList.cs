﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CMXBarcode;
using SmoothListbox;
using System.Linq;

namespace CargoMatrix.CargoLoader
{
    public class TaskList : FlightList
    {
        public TaskList()
            : base()
        {
            this.Name = "TaskList";
        }

        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();

            Cursor.Current = Cursors.WaitCursor;
            this.label1.Text = string.Format("{0}/{1}", filter, sort);
            this.searchBox.Text = string.Empty;
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;

            //smoothListBoxMainList.AddItemToFront(new StaticListItem("New", Resources.Icons.Notepad_Add));
            

            CargoLoaderFlight[] flightItems = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlights(TaskStatuses.All, "", "", "", ReceiverSortFields.Carrier);
            if (flightItems != null)
            {
                var rItems = from i in flightItems
                             select InitializeItem(i);
                smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
            }
 

            this.TitleText = string.Format("CargoLoader - ({0})", flightItems.Length);
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

     
        protected override void FlightList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
                MessageBox.Show("This feature is not available");
            else
                base.FlightList_ListItemClicked(sender, listItem, isSelected);
        }

        protected override void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoLoader - ({0})", smoothListBoxMainList.VisibleItemsCount - 1);
        }

    }
}
