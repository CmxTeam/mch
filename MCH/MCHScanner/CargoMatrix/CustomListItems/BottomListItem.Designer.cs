﻿namespace CustomListItems
{
    partial class BottomListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcxImage = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // pcxImage
            // 
            this.pcxImage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxImage.Location = new System.Drawing.Point(0, 106);
            this.pcxImage.Name = "pcxImage";
            this.pcxImage.Size = new System.Drawing.Size(150, 44);
            this.pcxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxImage.Image = Resources.Graphics.Skin.nav_bg;
            this.pcxImage.Paint += new System.Windows.Forms.PaintEventHandler(pcxImage_Paint);

            // 
            // BottomListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pcxImage);
            this.Name = "BottomListItem";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pcxImage;
    }
}
