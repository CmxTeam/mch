﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomListItems
{
    public class SubItemButtonClickEventArgs : EventArgs
    {
        ExpandListSubItem subItem;
        
        public CustomListItems.ExpandListSubItem SubItem
        {
            get { return subItem; }
        }

        public SubItemButtonClickEventArgs(ExpandListSubItem subItem)
        {
            this.subItem = subItem;
        }
    }
}
