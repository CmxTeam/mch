﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CustomListItems
{
    public abstract partial class MessageListBoxAsync<T> : CargoMatrix.Utilities.MessageListBox where T : class
    {
        Thread _asyncListThread;
        T[] _asyncItems = null;

        public MessageListBoxAsync()
        {
            InitializeComponent();
        }

        public void ReloadItemsAsync(T[] items)
        {
            if (_asyncListThread != null)
            {
                _asyncListThread.Abort();
            }

            _asyncItems = items;
            if (_asyncItems != null)
            {
                smoothListBoxBase1.RemoveAll();
                ThreadStart starter = new ThreadStart(LoadListAsync);
                _asyncListThread = new Thread(starter);
                _asyncListThread.Priority = ThreadPriority.BelowNormal;
                _asyncListThread.Start();
            }

        }
        private void LoadListAsync()
        {
            if (_asyncItems != null)
            {
                if (_asyncItems.Length > 0)
                {
                    this.Invoke(new EventHandler(WorkerBeginLoadLocationList));
                    for (int i = 0; i < _asyncItems.Length; i++)
                    {
                        object[] param = { _asyncItems[i], EventArgs.Empty };

                        this.Invoke(new EventHandler(WorkerLoadLocationList), param);


                    }
                    this.Invoke(new EventHandler(WorkerEndLoadLocationList));
                }

            }


        }
        protected abstract Control InitializeItem(T item);
        protected virtual void WorkerLoadLocationList(object sender, EventArgs e)
        {

            if (sender is T)
            {
                Control ctrl = InitializeItem(sender as T);
                smoothListBoxBase1.AddItem(ctrl);
                this.ProgressValue++;
            }

        }

        private void WorkerBeginLoadLocationList(object sender, EventArgs e)
        {
            this.Progress(0, _asyncItems.Length);
            this.ProgressBar = true;

        }
        private void WorkerEndLoadLocationList(object sender, EventArgs e)
        {
            this.ProgressBar = false;
            if (smoothListBoxBase1.ItemsCount == 0)
            {
                this.LabelEmpty = "No Records Found.";
            }
            else
            {
                LabelEmpty = string.Empty;
            }

        }
    }
}