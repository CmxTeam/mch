﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using System.Drawing;
using CMXExtensions;
using System.Windows.Forms;

namespace CustomListItems
{
    public class MawbRenderListItem : ExpandableRenderListitem<IMasterBillItem>
    {
        protected CargoMatrix.UI.CMXPictureButton buttonDetails;
        protected CustomUtilities.IndicatorPanel panelIndicators;
        public event EventHandler OnViewerClick;

        public MawbRenderListItem(IMasterBillItem mawb)
            : base(mawb)
        {
            this.LabelConfirmation = "Enter Masterbill Number";
            this.Size = new System.Drawing.Size(240, 65);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }
        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.MasterBillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio


                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    gOffScreen.DrawString(ItemData.Line2(), fnt, brush, new RectangleF(41 * hScale, 20 * vScale, 143 * hScale, 13 * vScale));
                    gOffScreen.DrawString(ItemData.Line3(), fnt, brush, new RectangleF(41 * hScale, 33 * vScale, 143 * hScale, 13 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.Clipboard;
                switch (ItemData.Status)
                {
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Open:
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Pending:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case CargoMatrix.Communication.DTO.MasterbillStatus.InProgress:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Completed:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }
                // logo
                gOffScreen.DrawImage(img, new Rectangle((int)(3 * hScale), (int)(3 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                // cutoff
                Rectangle cutoffRect = new Rectangle((int)(190 * hScale), (int)(12 * vScale), (int)(47 * hScale), (int)(28 * vScale));
                gOffScreen.DrawImage(CargoMatrix.Resources.Skin.cutoff, cutoffRect, new Rectangle(0, 0, CargoMatrix.Resources.Skin.cutoff.Width, CargoMatrix.Resources.Skin.cutoff.Height), GraphicsUnit.Pixel);


                // title line and cutoff
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    gOffScreen.DrawString(ItemData.Reference(), fnt, brush, new RectangleF(41 * hScale, 4 * vScale, 180 * hScale, 14 * vScale));
                    Rectangle topRect = new Rectangle(cutoffRect.Left, cutoffRect.Top, cutoffRect.Width, cutoffRect.Height / 2);
                    Rectangle botRect = new Rectangle(cutoffRect.Left, cutoffRect.Top + (cutoffRect.Height / 2), cutoffRect.Width, cutoffRect.Height / 2);
                    StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };

                    gOffScreen.DrawString(ItemData.CutOff.ToString("ddd"), fnt, new SolidBrush(Color.White), topRect, sf);
                    gOffScreen.DrawString(ItemData.CutOff.ToString("HH:mm"), fnt, brush, botRect, sf);

                }

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }
        protected override bool ExpandCondition()
        {
            return false;
        }
        protected override void InitializeControls()
        {
            // 
            // buttonDetails
            // 
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonDetails.Location = new System.Drawing.Point(3, 29);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // panelIndicators
            // 
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.panelIndicators.Location = new System.Drawing.Point(41, 47);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(196, 16);
            this.panelIndicators.Flags = ItemData.Flags;
            this.panelIndicators.PopupHeader = ItemData.Reference();
            this.Controls.Add(this.panelIndicators);
            this.Controls.Add(this.buttonDetails);
            foreach (var ctrl in this.Controls.OfType<Control>())
            {
                ctrl.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            }
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            if (OnViewerClick != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                OnViewerClick(this, e);
            }
        }
    }
}
