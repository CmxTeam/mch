﻿namespace CustomListItems
{
    partial class ExpandableListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>



        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLine2 = new System.Windows.Forms.Label();
            this.labelLine3 = new System.Windows.Forms.Label();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.title = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.panelCutoff = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new OpenNETCF.Windows.Forms.PictureBox2();
            this.SuspendLayout();
            // 
            // labelLine2
            // 
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.Location = new System.Drawing.Point(41, 19);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(140, 13);
            // 
            // labelLine3
            // 
            this.labelLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine3.Location = new System.Drawing.Point(41, 33);
            this.labelLine3.Name = "labelLine3";
            this.labelLine3.Size = new System.Drawing.Size(143, 13);
            // 
            // buttonDetails
            // 
            this.buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonDetails.Location = new System.Drawing.Point(3, 29);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(41, 3);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(180, 14);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Location = new System.Drawing.Point(41, 47);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(196, 16);
            //this.panelIndicators.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelIndicators_MouseDown);
            //this.panelIndicators.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelIndicators_MouseUp);
            // 
            // panelCutoff
            // 
            this.panelCutoff.BackColor = System.Drawing.Color.Black;
            this.panelCutoff.Image = CargoMatrix.Resources.Skin.cutoff;
            this.panelCutoff.Location = new System.Drawing.Point(190, 12);
            this.panelCutoff.Name = "panelCutoff";
            this.panelCutoff.Size = new System.Drawing.Size(47, 28);
            this.panelCutoff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBox1
            // 

            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.TransparentColor = System.Drawing.Color.White;
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            // 
            // ExpandableListItem
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panelCutoff);
            this.Controls.Add(this.panelIndicators);
            this.Controls.Add(this.title);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.labelLine2);
            this.Controls.Add(this.labelLine3);
            this.Name = "ExpandableListItem";
            this.Size = new System.Drawing.Size(240, 65);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label labelLine2;
        protected System.Windows.Forms.Label labelLine3;
        protected CargoMatrix.UI.CMXPictureButton buttonDetails;
        public System.Windows.Forms.Label title;
        protected CustomUtilities.IndicatorPanel panelIndicators;
        protected System.Windows.Forms.PictureBox panelCutoff;
        protected OpenNETCF.Windows.Forms.PictureBox2 pictureBox1;// OpenNETCF.Windows.Forms.PictureBox2 buttonDetails;

    }
}
