﻿namespace CustomListItems
{
    partial class DischargeListItem
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DischargeListItem));
           // this.lblTitle = new System.Windows.Forms.Label();
            //this.lblCompany = new System.Windows.Forms.Label();
            //this.lblDriver = new System.Windows.Forms.Label();
            //this.btnDetails = new CargoMatrix.UI.CMXPictureButton();
            this.btnDamageCapture = new CargoMatrix.UI.CMXPictureButton();
            this.btnMoreDetails = new CargoMatrix.UI.CMXPictureButton();
            //this.pnlIndicators = new CustomUtilities.IndicatorPanel();
            this.lblSlacHdr = new System.Windows.Forms.Label();
            this.lblReleaseSlac = new System.Windows.Forms.Label();
            this.lblReleaseSlacHdr = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblLocationHdr = new System.Windows.Forms.Label();
            //this.pcxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.lblSlac = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPieces = new System.Windows.Forms.Label();
            
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.BackColor = System.Drawing.Color.White;
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(24, 3);
            this.title.Name = "lblTitle";
            this.title.Size = new System.Drawing.Size(191, 14);
            // 
            // lblCompany
            // 
            this.labelLine2.BackColor = System.Drawing.Color.White;
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.Location = new System.Drawing.Point(41, 21);
            this.labelLine2.Name = "lblCompany";
            this.labelLine2.Size = new System.Drawing.Size(155, 13);
            // 
            // lblDriver
            // 
            this.labelLine3.BackColor = System.Drawing.Color.White;
            this.labelLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine3.Location = new System.Drawing.Point(41, 37);
            this.labelLine3.Name = "lblDriver";
            this.labelLine3.Size = new System.Drawing.Size(155, 13);
            // 
            // btnDetails
            // 
            this.buttonDetails.Image = ((System.Drawing.Image)(resources.GetObject("btnDetails.Image")));
            this.buttonDetails.Location = new System.Drawing.Point(4, 59);
            this.buttonDetails.Name = "btnDetails";
            this.buttonDetails.PressedImage = ((System.Drawing.Image)(resources.GetObject("btnDetails.PressedImage")));
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            // 
            // btnDamageCapture
            // 
            this.btnDamageCapture.Image = ((System.Drawing.Image)(resources.GetObject("btnDamageCapture.Image")));
            this.btnDamageCapture.Location = new System.Drawing.Point(204, 21);
            this.btnDamageCapture.Name = "btnDamageCapture";
            this.btnDamageCapture.PressedImage = ((System.Drawing.Image)(resources.GetObject("btnDamageCapture.PressedImage")));
            this.btnDamageCapture.Size = new System.Drawing.Size(32, 32);
            this.btnDamageCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDamageCapture.TransparentColor = System.Drawing.Color.White;
            // 
            // btnMoreDetails
            // 
            this.btnMoreDetails.Image = ((System.Drawing.Image)(resources.GetObject("btnMoreDetails.Image")));
            this.btnMoreDetails.Location = new System.Drawing.Point(204, 59);
            this.btnMoreDetails.Name = "btnMoreDetails";
            this.btnMoreDetails.PressedImage = ((System.Drawing.Image)(resources.GetObject("btnMoreDetails.PressedImage")));
            this.btnMoreDetails.Size = new System.Drawing.Size(32, 32);
            this.btnMoreDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMoreDetails.TransparentColor = System.Drawing.Color.White;
            // 
            // pnlIndicators
            // 
            this.panelIndicators.Location = new System.Drawing.Point(3, 99);
            this.panelIndicators.Name = "pnlIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(233, 16);
            this.panelIndicators.TabIndex = 12;
            // 
            // lblSlacHdr
            // 
            this.lblSlacHdr.BackColor = System.Drawing.Color.White;
            this.lblSlacHdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSlacHdr.Location = new System.Drawing.Point(41, 53);
            this.lblSlacHdr.Name = "lblSlacHdr";
            this.lblSlacHdr.Size = new System.Drawing.Size(34, 13);
            this.lblSlacHdr.Text = "Slac :";
            // 
            // lblReleaseSlac
            // 
            this.lblReleaseSlac.BackColor = System.Drawing.Color.White;
            this.lblReleaseSlac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblReleaseSlac.Location = new System.Drawing.Point(160, 53);
            this.lblReleaseSlac.Name = "lblReleaseSlac";
            this.lblReleaseSlac.Size = new System.Drawing.Size(36, 13);
            // 
            // lblReleaseSlacHdr
            // 
            this.lblReleaseSlacHdr.BackColor = System.Drawing.Color.White;
            this.lblReleaseSlacHdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblReleaseSlacHdr.Location = new System.Drawing.Point(133, 53);
            this.lblReleaseSlacHdr.Name = "lblReleaseSlacHdr";
            this.lblReleaseSlacHdr.Size = new System.Drawing.Size(29, 13);
            this.lblReleaseSlacHdr.Text = "Rls :";
            // 
            // lblLocation
            // 
            this.lblLocation.BackColor = System.Drawing.Color.White;
            this.lblLocation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblLocation.Location = new System.Drawing.Point(75, 84);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(121, 13);
            // 
            // lblLocationHdr
            // 
            this.lblLocationHdr.BackColor = System.Drawing.Color.White;
            this.lblLocationHdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblLocationHdr.Location = new System.Drawing.Point(41, 84);
            this.lblLocationHdr.Name = "lblLocationHdr";
            this.lblLocationHdr.Size = new System.Drawing.Size(30, 13);
            this.lblLocationHdr.Text = "Loc :";
            // 
            // pcxLogo
            // 
            this.pictureBox1.Location = new System.Drawing.Point(5, 21);
            this.pictureBox1.Name = "pcxLogo";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TransparentColor = System.Drawing.Color.White;
            // 
            // lblSlac
            // 
            this.lblSlac.BackColor = System.Drawing.Color.White;
            this.lblSlac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSlac.Location = new System.Drawing.Point(75, 53);
            this.lblSlac.Name = "lblSlac";
            this.lblSlac.Size = new System.Drawing.Size(57, 13);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(41, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.Text = "Pieces :";
             
            // lblPieces
            // 
            this.lblPieces.BackColor = System.Drawing.Color.White;
            this.lblPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPieces.Location = new System.Drawing.Point(99, 69);
            this.lblPieces.Name = "lblPieces";
            this.lblPieces.Size = new System.Drawing.Size(87, 13);
            // 
            // DischargeListItem
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblPieces);
            this.Controls.Add(this.label1);
            //this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.lblLocationHdr);
            this.Controls.Add(this.lblReleaseSlac);
            this.Controls.Add(this.lblReleaseSlacHdr);
            this.Controls.Add(this.lblSlac);
            this.Controls.Add(this.lblSlacHdr);
            //this.Controls.Add(this.lblTitle);
            //this.Controls.Add(this.lblCompany);
            //this.Controls.Add(this.lblDriver);
            //this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.btnDamageCapture);
            this.Controls.Add(this.btnMoreDetails);
            //this.Controls.Add(this.pnlIndicators);
            this.Name = "DischargeListItem";
            this.Size = new System.Drawing.Size(240, 117);

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
           
            this.ResumeLayout(false);

        }

        #endregion

        //protected System.Windows.Forms.Label lblTitle;
        //protected System.Windows.Forms.Label lblCompany;
        //protected System.Windows.Forms.Label lblDriver;
      
        
        
        //private   CargoMatrix.UI.CMXPictureButton btnDetails;
        private   CargoMatrix.UI.CMXPictureButton btnDamageCapture;
        private   CargoMatrix.UI.CMXPictureButton btnMoreDetails;
        //protected CustomUtilities.IndicatorPanel pnlIndicators;
        protected System.Windows.Forms.Label lblSlacHdr;
        protected System.Windows.Forms.Label lblReleaseSlac;
        protected System.Windows.Forms.Label lblReleaseSlacHdr;
        protected System.Windows.Forms.Label lblLocation;
        protected System.Windows.Forms.Label lblLocationHdr;
        //private OpenNETCF.Windows.Forms.PictureBox2 pcxLogo;
        protected System.Windows.Forms.Label lblSlac;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label lblPieces;
        
        
    }
}
