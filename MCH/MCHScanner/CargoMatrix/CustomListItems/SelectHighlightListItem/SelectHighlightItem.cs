﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems.SelectHighlightListItem
{
    public partial class SelectHighlightItem : ExpandListItem
    {
        Color notSelectedColor;
        Color partiallySelectedColor;
        Color completelySelectedColor;
        Func<string> descriptionLineUpdateText;
        int maxItemsSelectHighlightCount;
        int minItemsSelectHighlightCount;
        int itemsHeighlightCount;
        string manualEnterDialogHeader;
        string manualEnterDialogSubHeader;

       
        #region Properties
        /// <summary>
        /// Gets or Sets delegate used to update item description line
        /// </summary>
        public Func<string> DescriptionLineUpdateText
        {
            get { return descriptionLineUpdateText; }
            set { descriptionLineUpdateText = value; }
        }

        /// <summary>
        /// Gets or Sets not selected highlight color
        /// </summary>
        public Color NotSelectedColor
        {
            get { return notSelectedColor; }
            set { notSelectedColor = value; }
        }

        /// <summary>
        /// Gets or Sets partially selected highlight color
        /// </summary>
        public Color PartiallySelectedColor
        {
            get { return partiallySelectedColor; }
            set { partiallySelectedColor = value; }
        }

        /// <summary>
        /// Gets or Sets completely selected highlight color
        /// </summary>
        public Color CompletelySelectedColor
        {
            get { return completelySelectedColor; }
            set { completelySelectedColor = value; }
        }

        /// <summary>
        /// Gets or Sets maximum items to be selected for the highlight to consider selected. If value is 0 - all items should be selected
        /// </summary>
        public int MaxItemsSelectHighlightCount
        {
            get { return this.maxItemsSelectHighlightCount == 0 ? this.Items.Count() : this.maxItemsSelectHighlightCount; }
            set 
            {
                if (value < 0) throw new ArgumentOutOfRangeException("MaxItemsSelectHighlightCount", "Should be greater or equal to zero");

                maxItemsSelectHighlightCount = value; 
            }
        }

        /// <summary>
        /// Gets or Sets minimum items to be selected for the highlight to consider not selected.
        /// </summary>
        public int MinItemsSelectHighlightCount
        {
            get { return minItemsSelectHighlightCount; }
            set 
            {
                if (value < 0) throw new ArgumentOutOfRangeException("MinItemsSelectHighlightCount", "Should be greater or equal to zero");
                minItemsSelectHighlightCount = value; 
            }
        }

        /// <summary>
        /// Get or Sets manual enter items count dialog subheader
        /// </summary>
        public string ManualEnterDialogSubHeader
        {
            get { return manualEnterDialogSubHeader; }
            set { manualEnterDialogSubHeader = value; }
        }

        /// <summary>
        /// Gets or Sets manual enter items count dialog header
        /// </summary>
        public string ManualEnterDialogHeader
        {
            get { return manualEnterDialogHeader; }
            set { manualEnterDialogHeader = value; }
        }

        /// <summary>
        /// Gets number of selected items by user input
        /// </summary>
        public int ItemsHeighlightCount
        {
            get { return itemsHeighlightCount; }
        }

        /// <summary>
        /// Gets the select state of the control
        /// </summary>
        public SelectStateEnum SelectState
        {
            get 
            {
                int maxToSelect = this.MaxItemsSelectHighlightCount;
                int minToSelect = this.minItemsSelectHighlightCount;

                if (this.itemsHeighlightCount > minToSelect &&
                this.itemsHeighlightCount < maxToSelect) return SelectStateEnum.PartiallySelected;

                if (this.itemsHeighlightCount >= maxToSelect) return SelectStateEnum.CompletelySelected;

                return SelectStateEnum.NotSelected;
            }
        }

        #endregion
        
        #region Constructors
        public SelectHighlightItem(IExpandItemData data)
            : base(data)
        {
            this.InitializeFields();
        }

        public SelectHighlightItem(int ID)
            : base(ID)
        {
            this.InitializeFields();
        }

        public SelectHighlightItem(int ID, Image image)
            : base(ID, image)
        {
            this.InitializeFields();
        }

        public SelectHighlightItem(IExpandItemData data, Image image)
            : base(data, image)
        {
            this.InitializeFields();
        } 
        #endregion

        /// <summary>
        /// Updates control highlight
        /// </summary>
        public virtual void UpdateView()
        {
            var updateDescriptionText = this.descriptionLineUpdateText;
            
            if (updateDescriptionText != null)
            {
                this.DescriptionLine = updateDescriptionText();
            }

            Color stateColor = GetStateColor(this.SelectState);

            this.TitleForeColor = stateColor;
            this.DescriptionForeColor = stateColor;
        }

        void SelectHighlightItem_SubItemSelected(object sender, ExpandItemEventArgs e)
        {
            this.itemsHeighlightCount =  this.SelectedItems.Count();

            this.UpdateView();
        }

        /// <summary>
        /// Performs manual enter of the number of selected items
        /// </summary>
        /// <returns>True if user enter happened, otherwise - false</returns>
        
        public void HighlightAll(bool select)
        {
            if (select == false)
            {
                this.itemsHeighlightCount = 0;
            }
            else
            {
                this.itemsHeighlightCount = this.MaxItemsSelectHighlightCount;
            }

            this.SelectAll(select);

            this.UpdateView();
        }

        public void HighlightAll()
        {
            this.HighlightAll(this.IsSelected);
        }

        public void HighlightPieces(IEnumerable<int> ids, bool select)
        {
            this.SelectItems(ids, select);

            this.itemsHeighlightCount = this.SelectedItems.Count();

            this.UpdateView();
        }

        
        public virtual bool HighlightItemsByUserInput()
        {
            if (this.MaxItemsSelectHighlightCount - this.itemsHeighlightCount <= 0) return false;

            bool isReadonly = this.itemsHeighlightCount >= this.MaxItemsSelectHighlightCount;

            int itemsCount;
            bool result = this.GetPiecesCountFromUser(this.manualEnterDialogHeader,
                                                      this.manualEnterDialogSubHeader,
                                                      this.MaxItemsSelectHighlightCount - this.itemsHeighlightCount,
                                                      isReadonly,
                                                      out itemsCount);
            if (result == false) return false;

            this.itemsHeighlightCount+= itemsCount;

            this.UpdateView();

            return true;
        }

        #region Additional Functionallity
        private void InitializeFields()
        {
            this.InitializeComponent();

            this.notSelectedColor = Color.Black;
            this.partiallySelectedColor = Color.Red;
            this.completelySelectedColor = Color.Green;
            this.maxItemsSelectHighlightCount = 0;
            this.minItemsSelectHighlightCount = 0;
            this.itemsHeighlightCount = 0;
         
            this.SubItemSelected += new EventHandler<ExpandItemEventArgs>(SelectHighlightItem_SubItemSelected);
        }

        private Color GetStateColor(SelectStateEnum selectState)
        {
            switch (selectState)
            {
                case SelectStateEnum.NotSelected: return this.notSelectedColor;

                case SelectStateEnum.PartiallySelected: return this.partiallySelectedColor;

                case SelectStateEnum.CompletelySelected: return this.completelySelectedColor;

                default: return Color.Black;
            }
        }

        private SelectStateEnum GetSelectState(int maxSelect, int minSelect, int currentSelect)
        {
            if (currentSelect > minSelect &&
                currentSelect < maxSelect) return SelectStateEnum.PartiallySelected;

            if (currentSelect >= maxSelect) return SelectStateEnum.CompletelySelected;

            return SelectStateEnum.NotSelected;

        }

        private bool GetPiecesCountFromUser(string header, string subHeder, int maxPiecesCount, bool isReadonly, out int itemsCount)
        {
            itemsCount = 0;
            
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = header;
            cntMsg.LabelReference = subHeder;
            cntMsg.LabelDescription = "Enter pieces count";
            cntMsg.PieceCount = maxPiecesCount;
            cntMsg.Readonly = isReadonly;

            if (DialogResult.OK != cntMsg.ShowDialog()) return false;

            itemsCount = cntMsg.PieceCount;

            return true;
        }

        #endregion
    }


    public class SelectHighlightItem<T> : SelectHighlightItem
    {
        T customData;

        /// <summary>
        /// Gets or Sets control custom data
        /// </summary>
        public T CustomData
        {
            get { return customData; }
        }

        #region Constructors
        public SelectHighlightItem(T customData, IExpandItemData data)
            : base(data)
        {
            this.InitializeFields(customData);
        }

        public SelectHighlightItem(T customData, int ID)
            : base(ID)
        {
            this.InitializeFields(customData);
        }

        public SelectHighlightItem(T customData, IExpandItemData data, Image image)
            : base(data, image)
        {
            this.InitializeFields(customData);
        }

        public SelectHighlightItem(T customData, int ID, Image image)
            : base(ID, image)
        {
            this.InitializeFields(customData);
        } 
        #endregion

        #region Additional Functionallity
        private void InitializeFields(T customData)
        {
            this.customData = customData;
        } 
        #endregion

    }

}
