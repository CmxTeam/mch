﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems
{
    public partial class CMXScrollableText : UserControl
    {
        private System.Windows.Forms.Timer Clock;
        int _waitInterval = 3;
        int _waiting;
        bool _enableScroll;
        private int iTimer;

        public CMXScrollableText()
        {
            InitializeComponent();
            mainLabel.Height = this.Height;
        }

        private void SetTimer()
        {
            if (Clock == null)
            {
                Clock = new System.Windows.Forms.Timer();
                Clock.Tick += new EventHandler(Timer_Tick);
            }
            if (iTimer == 0)
                iTimer = 200;
            Clock.Interval = iTimer;
            Clock.Enabled = true;
            
        }
        public void Timer_Tick(object sender, EventArgs eArgs)
        {

            if (CargoMatrix.UI.CMXAnimationmanager.IsScrolling == false)
            {
                //Point p =  this.PointToScreen(new Point(this.Left, this.Top);

                //if( control rectange is inside screen)
                //{
                if (_waiting <= _waitInterval)
                {
                    _waiting++;
                    return;
                }
                if (sender == Clock)
                {
                    int i = mainLabel.Location.X;
                    i = (i - 5);
                    if (i < (-1 * mainLabel.Size.Width))
                    {
                        i = 0;
                        _waiting = 0;

                    }
                    mainLabel.Location = new System.Drawing.Point(i, 0);
                }
                //}
            }
        }
        private void SetWidths()
        {
            if (mainLabel.Text == "")
                return;
            Graphics graphics = this.CreateGraphics();
            SizeF textSize = graphics.MeasureString(mainLabel.Text, mainLabel.Font);
            // mainLabel.Width = (int)(textSize.Width + 1);
            if (textSize.Height < this.Height)
            {
                mainLabel.Height = (int)(textSize.Height + 1);
            }
            else
            {
                mainLabel.Height = this.Height;
            }

            if (textSize.Width > this.Width)
            {
                SetTimer();
                mainLabel.Width = (int)(textSize.Width + 3);
            }
            else
            {
                mainLabel.Width = this.Width;
            }
        }

        public new System.Drawing.Color ForeColor
        {
            get
            {

                if (mainLabel != null)
                    return mainLabel.ForeColor;

                return Color.White;
            }
            set
            {

                mainLabel.ForeColor = value;
            }
        }

        
        public new string Text
        {
            get
            {
                return mainLabel.Text;
            }
            set
            {
                mainLabel.Text = value;
                SetWidths();

            }
        }

        public new Font Font
        {
            get
            {
                return mainLabel.Font;
            }
            set
            {
                mainLabel.Font = value;
                SetWidths();
            }
        }
        public new bool Visible
        {
            set
            {
                base.Visible = value;
                if (value == false)
                    Clock.Enabled = false;
            }
            get
            {
                return base.Visible;
            }

        }
        public bool EnableScrolling
        {
            get { return _enableScroll; }
            set
            {
                _enableScroll = value;
                if (value)
                    SetWidths();
            }
        }
        public int Timer
        {
            get
            {
                return iTimer;
            }
            set
            {
                iTimer = value;
            }
        }
        public int WaitInterval
        {
            get
            {
                return _waitInterval;
            }
            set
            {
                _waitInterval = value;
            }
        }
    }
}
