﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using CargoMatrix.Communication.CargoDischarge;
using CMXExtensions;
using CargoMatrix.Communication.DTO;
using System.Windows.Forms;

namespace CustomListItems
{
    public class DischargeListItemRendered : SmoothListbox.ListItems.RenderItemBase, SmoothListbox.ISmartListItem
    {
        private CargoMatrix.UI.CMXPictureButton btnDamageCapture;
        private CargoMatrix.UI.CMXPictureButton btnMoreDetails;
        private CustomUtilities.IndicatorPanel pnlIndicators;
        private CargoMatrix.UI.CMXPictureButton btnDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 pcxLogo;
        private IDischargeHouseBillItem houseBillItem;
      
        public event EventHandler OnViewerClick;
        public event EventHandler OnDamageCaptureClick;
        public event EventHandler OnMoreDetailsClick;
     
        public IDischargeHouseBillItem HouseBillItem
        {
            get { return houseBillItem; }
            set
            {
                this.houseBillItem = value;

                this.Dirty = true;
                this.Render();
            }
        }
        
        public Image Logo
        {
            get
            {
                return this.pcxLogo.Image;
            }
            set
            {
                this.pcxLogo.Image = value;
            }
        }
  
        public DischargeListItemRendered()
        {
            this.SuspendLayout();

            this.BackColor = Color.White;

            this.InitializeItems();

            this.ResumeLayout();

            this.RetentionLevel = 2;
        }

        protected override void InitializeControls()
        {
             this.btnDetails = new CargoMatrix.UI.CMXPictureButton();
            this.pnlIndicators = new CustomUtilities.IndicatorPanel();
            this.pcxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.btnMoreDetails = new CargoMatrix.UI.CMXPictureButton();
            this.btnDamageCapture = new CargoMatrix.UI.CMXPictureButton();

            var rHeight = this.CurrentAutoScaleDimensions.Height;
            var rWidth = this.CurrentAutoScaleDimensions.Width; 


            // 
            // pictureBox1
            // 

            this.pcxLogo.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), (int)(3 * rHeight / 96F));
            this.pcxLogo.Name = "pcxLogo";
            this.pcxLogo.Size = new System.Drawing.Size((int)(24 * rWidth / 96F), (int)(24 * rHeight / 96F));
            this.pcxLogo.TransparentColor = System.Drawing.Color.White;
            this.pcxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;



            // 
            // pnlIndicators
            // 
            //this.pnlIndicators.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), this.Height - 16);
            this.pnlIndicators.Name = "pnlIndicators";
            //this.pnlIndicators.Size = new System.Drawing.Size(this.Width - 3, (int)(16 * rHeight / 96F));
            //this.pnlIndicators.Dock = DockStyle.Bottom;

            // 
            // btnDetails
            // 
            this.btnDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.btnDetails.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), (int)(49* rHeight / 96F));
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.btnDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.btnDetails.Size = new System.Drawing.Size((int)(32 * rWidth / 96F), (int)(32 * rHeight / 96F));
            this.btnDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetails.TransparentColor = System.Drawing.Color.White;
            this.btnDetails.Click += new EventHandler(buttonDetails_Click);

            // 
            // btnDamageCapture
            // 
            this.btnDamageCapture.Image = CargoMatrix.Resources.Skin.damage;
            this.btnDamageCapture.Location = new System.Drawing.Point((int)(204 * rWidth / 96F), (int)(21 * rHeight / 96F));
            this.btnDamageCapture.Name = "btnDamageCapture";
            this.btnDamageCapture.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.btnDamageCapture.Size = new System.Drawing.Size((int)(32 * rWidth / 96F), (int)(32 * rHeight / 96F));
            this.btnDamageCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDamageCapture.TransparentColor = System.Drawing.Color.White;
            this.btnDamageCapture.Click += new EventHandler(btnDamageCapture_Click);

            // 
            // btnMoreDetails
            // 
            this.btnMoreDetails.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.btnMoreDetails.Location = new System.Drawing.Point((int)(204 * rWidth / 96F), (int)(59 * rHeight / 96F));
            this.btnMoreDetails.Name = "btnMoreDetails";
            this.btnMoreDetails.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;
            this.btnMoreDetails.Size = new System.Drawing.Size((int)(32 * rWidth / 96F), (int)(32 * rHeight / 96F));
            this.btnMoreDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMoreDetails.TransparentColor = System.Drawing.Color.White;
            this.btnMoreDetails.Click += new EventHandler(btnMoreDetails_Click);

            this.Controls.Add(this.pcxLogo);
            this.Controls.Add(this.btnDamageCapture);
            this.Controls.Add(this.btnMoreDetails);
            this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.pnlIndicators);
        }


        private void InitializeItems()
        {
            this.SuspendLayout();

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            var rHeight = this.CurrentAutoScaleDimensions.Height;
            var rWidth = this.CurrentAutoScaleDimensions.Width;

            this.Size = new System.Drawing.Size(240,117);

            this.ResumeLayout(false);
        }

        public void SetHeightDimensions(int heigth)
        {
            this.SuspendLayout();

            this.Height = heigth;

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.ResumeLayout(false);
        
        }

        void btnMoreDetails_Click(object sender, EventArgs e)
        {
            var click = OnMoreDetailsClick;

            if (click != null)
            {
                OnMoreDetailsClick(this, EventArgs.Empty);
            }
        }

        void btnDamageCapture_Click(object sender, EventArgs e)
        {
            var click = OnDamageCaptureClick;

            if(click != null)
            {
                OnDamageCaptureClick(this, EventArgs.Empty);            
            }
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            this.SuspendLayout();

            int fontSize = 9;
            var rHeight = this.CurrentAutoScaleDimensions.Height;
            var rWidth = this.CurrentAutoScaleDimensions.Width; 


            using(Brush brush = new SolidBrush(Color.Black))
            {
                gOffScreen.DrawString(string.Format("{0} ({1:HH:mm})", houseBillItem.Reference(), houseBillItem.ReleaseTime),
                                     new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), brush, 41 * rWidth / 96F, 3 * rHeight / 96F);
                gOffScreen.DrawString(houseBillItem.CompanyName,
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 41 * rWidth / 96F, 19 * rHeight / 96F);
                gOffScreen.DrawString(houseBillItem.DriverFullName,
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 41 * rWidth / 96F, 33 * rHeight / 96F);

                gOffScreen.DrawString("Slac :", new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold), brush, 41 * rWidth / 96F, 53 * rHeight / 96F);


                Brush slacBrush = null;

                if (houseBillItem.ScannedSlac == 0)
                {
                    slacBrush = brush;
                }
                else
                {
                    slacBrush = new SolidBrush(houseBillItem.ScannedSlac != houseBillItem.ReleseSlac ? Color.Red : Color.Green);
                }

                gOffScreen.DrawString(string.Format("{0} of {1}", houseBillItem.ScannedSlac, houseBillItem.ReleseSlac),
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), slacBrush, 75 * rWidth / 96F, 53 * rHeight / 96F);

                if (object.ReferenceEquals(brush, slacBrush) == false)
                {
                    slacBrush.Dispose();
                }

                gOffScreen.DrawString("Rls :",
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold), brush, 133 * rWidth / 96F, 53 * rHeight / 96F);
                gOffScreen.DrawString(houseBillItem.ReleseSlac.ToString(),
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 160 * rWidth / 96F, 53 * rHeight / 96F);


                gOffScreen.DrawString("Pieces :",
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold), brush, 41 * rWidth / 96F, 69 * rHeight / 96F);

                gOffScreen.DrawString((houseBillItem.AvailablePieces + houseBillItem.ScannedPieces).ToString(),
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 99 * rWidth / 96F, 69 * rHeight / 96F);


                gOffScreen.DrawString("Loc :",
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold), brush, 41 * rWidth / 96F, 84 * rHeight / 96F);

                gOffScreen.DrawString(houseBillItem.Location,
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 75 * rWidth / 96F, 84 * rHeight / 96F);

                switch (this.houseBillItem.status)
                {
                    case HouseBillStatuses.Open:
                    case HouseBillStatuses.Pending:
                        this.Logo = CustomListItemsResource.Clipboard;
                        break;
                    case HouseBillStatuses.InProgress:
                        this.Logo = CustomListItemsResource.History;
                        break;
                    case HouseBillStatuses.Completed:
                        this.Logo = CustomListItemsResource.Clipboard_Check;
                        break;

                }

            }

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }

            this.pnlIndicators.Flags = houseBillItem.Flags;
            this.pnlIndicators.PopupHeader = houseBillItem.Reference();
            this.pnlIndicators.BackColor = this.BackColor;
            this.pnlIndicators.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), this.Height - (int)(18 * rHeight / 96F));
            this.pnlIndicators.Size = new System.Drawing.Size(this.Width - 3, (int)(16 * rHeight / 96F));

            this.ResumeLayout(false);
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            if (OnViewerClick != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                OnViewerClick(this, e);
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string textToMatch = text.ToUpper();

            if (houseBillItem.DriverFullName.ToUpper().Contains(textToMatch)) return true;

            if (houseBillItem.CompanyName.ToUpper().Contains(textToMatch)) return true;

            if (houseBillItem.Reference().ToUpper().Contains(textToMatch)) return true;

            return false;
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
