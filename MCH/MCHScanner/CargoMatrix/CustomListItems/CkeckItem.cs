﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems
{
    public partial class CkeckItem<T> : SmoothListbox.ListItems.StandardListItem<T>
    {
        public CkeckItem(T data):base(data)
        {
            InitializeComponent();
            itemPicture.Image = CustomListItemsResource.unselected;
        }
        public CkeckItem(string label1,int id,T data):this(data)
        {
            ID = id;
            title.Text = label1.Replace(Environment.NewLine," ");
        }

        public override void SelectedChanged(bool isSelected)
        {
            base.SelectedChanged(isSelected);
            if (isSelected)
                itemPicture.Image = CustomListItemsResource.selected;
            else
                itemPicture.Image = CustomListItemsResource.unselected;
        }
    }
}
