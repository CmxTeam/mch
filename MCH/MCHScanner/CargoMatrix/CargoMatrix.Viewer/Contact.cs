﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class Contact : UserControl
    {
        public Contact(string header, string address)
        {
            InitializeComponent();
            labelHeader.Text = header;
            labelDescription.Text = address;


            
            OpenNETCF.Drawing.FontEx descriptionFont = new OpenNETCF.Drawing.FontEx(labelDescription.Font.Name, labelDescription.Font.Size, labelDescription.Font.Style);
            OpenNETCF.Drawing.GraphicsEx g = OpenNETCF.Drawing.GraphicsEx.FromControl(this);
            int height = (int)g.MeasureString(address, descriptionFont, labelDescription.Width).Height;
            
            
            //labelDescription.Text = description;
            //Size size = CargoMatrix.UI.CMXMeasureString.MeasureString(CreateGraphics(), address, labelDescription.ClientRectangle, false);
            int diff = Height - labelDescription.Height;
            this.Height = height + diff;
        }
    }
}
