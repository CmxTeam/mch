﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class ShellImageItemB : UserControl
    {
        public ShellImageItemB(string heading, string descrption, string image)
        {
            InitializeComponent();
            labelHeading.Text = heading;
            labelDescription.Text = descrption;
            if (image != null && image != "")
            { 
                byte []img = Convert.FromBase64String(image);
                System.IO.MemoryStream stream = new System.IO.MemoryStream(img);
                Bitmap bitmap = new Bitmap(stream);
                float aspect = (float)bitmap.Width / (float)bitmap.Height;
                float width = pictureBoxLogo.Width * aspect;
                width = width - pictureBoxLogo.Width;
                
                pictureBoxLogo.Left -= (int)width;
                pictureBoxLogo.Width += (int)width;
                pictureBoxLogo.Image = bitmap;
                stream.Close();
            }

        }

     
    }
}
