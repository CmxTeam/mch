﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.TaskManager
{
    public static class CommonMethods
    {
        public static bool SameValuesLists(List<string> list1, List<string> list2)
        {
            if (list1.Count == 0 && list2.Count == 0) return true;

            if (list1.Count != list2.Count) return false;

            foreach (var str in list1)
            {
                if (list2.Exists(str2 => string.Compare(str, str2, StringComparison.OrdinalIgnoreCase) == 0) == false) return false;
            }

            return true;
        }
    }
}
