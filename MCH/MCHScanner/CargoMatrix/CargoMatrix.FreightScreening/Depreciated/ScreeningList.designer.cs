﻿namespace CargoMatrix.FreightScreening
{
    partial class ScreeningList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.buttonScannedList = new CargoMatrix.UI.CMXCounterIcon();
            this.topLabel = new CustomUtilities.CMXBlinkingLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.topPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            this.topPanel.SuspendLayout();
            // 
            // buttonScannedList
            // 
            this.buttonScannedList.Location = new System.Drawing.Point(202, 4);
            this.buttonScannedList.Name = "buttonScannedList";
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn_34x34;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_34x34_over;
            this.buttonScannedList.Size = new System.Drawing.Size(36, 36);
            this.buttonScannedList.TabIndex = 1;
            this.buttonScannedList.Click += new System.EventHandler(buttonScannedList_Click);
            // 
            // topLabel
            // 
            this.topLabel.BackColor = System.Drawing.Color.White;
            this.topLabel.Blink = true;
            this.topLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.topLabel.ForeColor = System.Drawing.Color.Red;
            this.topLabel.Location = new System.Drawing.Point(3, 15);
            this.topLabel.Name = "topLabel";
            this.topLabel.Size = new System.Drawing.Size(190, 15);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 43);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.topLabel);
            this.topPanel.Controls.Add(this.buttonScannedList);
            this.topPanel.Controls.Add(this.splitter1);
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(240, 44);

            this.panelHeader2.Height = topPanel.Height;
            this.panelHeader2.Controls.Add(topPanel);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.topLabel.ResumeLayout(false);
            this.ResumeLayout(false);
        }


        #endregion
        private System.Windows.Forms.Splitter splitter1;
        private CustomUtilities.CMXBlinkingLabel topLabel;
        internal CargoMatrix.UI.CMXCounterIcon buttonScannedList;
        private System.Windows.Forms.Panel topPanel;
    }
}
