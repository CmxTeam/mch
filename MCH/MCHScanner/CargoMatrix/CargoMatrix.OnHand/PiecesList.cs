﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.OnHand;
using CustomListItems;

namespace CargoMatrix.OnHand
{
    public partial class PiecesList : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        private OptionsListITem EditPiecesBatchOption = new OptionsListITem(OptionsListITem.OptionItemID.EDIT_MEASURMENTS) { Title = "Edit Selected Pieces", Enabled = false };
        private OptionsListITem DeletePiecesBatchOption = new OptionsListITem(OptionsListITem.OptionItemID.DELETE_TASK) { Title = "Remove Selected Pieces", Enabled = false };

        public PiecesList()
        {
            InitializeComponent();
            this.TitleText = "CargoOnhand - Details";
            this.smoothListBoxMainList.MultiSelectEnabled = true;
            this.smoothListBoxMainList.UnselectEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(PiecesList_ListItemClicked);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(PiecesList_MenuItemClicked);
            this.LoadOptionsMenu += new EventHandler(PiecesList_LoadOptionsMenu);
        }

        private void DisplayHeaderInfo()
        {
            this.labelOnhandNo.Text = SharedMethods.CurrentPickup.ShipmentNO + " - " + SharedMethods.CurrentPickup.Destination;
            this.labelPickupRef.Text = SharedMethods.CurrentPONumber;
            this.labelDest.Text = SharedMethods.CurrentVendorName;
            this.itemPicture.Image = SharedMethods.GetStatusIcon(SharedMethods.CurrentPickup.Status);
            this.panelIndicators.Flags = SharedMethods.CurrentPickup.Flags;
            this.panelIndicators.PopupHeader = SharedMethods.CurrentPickup.ShipmentNO;
        }

        private void PiecesList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //var item = listItem as CustomListItems.ThreeLineItem<CMXDomesticPackage>;
            //AddEditPiece(item.ItemData);
            bool enable = smoothListBoxMainList.SelectedItems.Count > 1;
            EditPiecesBatchOption.Enabled = DeletePiecesBatchOption.Enabled = enable;


        }
        public override void LoadControl()
        {
            this.smoothListBoxMainList.RemoveAll();
            var onhandDetails = Communication.OnHand.OnHand.Instance.GetOnHandDetails(SharedMethods.CurrentPickup.ShipmentNO);
            if (onhandDetails == null)
            {
                SharedMethods.ShowFailMessage("OnHand was not found");
                return;
            }
            else
            {
                SharedMethods.CurrentPickup = onhandDetails;
            }
            DisplayHeaderInfo();
            foreach (var pieceItem in onhandDetails.DomesticPackages)
            {
                string line1 = string.Format("Piece: {0} of {1} / Slac {2}", pieceItem.PieceNumber, onhandDetails.DomesticPackages.Length, pieceItem.Slac);
                string line2 = string.Format("Pkg: {0}  Wgt: {1} {2}", pieceItem.Type, pieceItem.Weight.HasValue ? pieceItem.Weight.Value.ToString("0.") : "0", pieceItem.WeightUOM);
                string line3 = string.Format("Dims : {0} x {1} x {2} {3}", pieceItem.Length, pieceItem.Width, pieceItem.Height, pieceItem.DimUOM);

                var pieceListItem = new CustomListItems.ThreeLineItem<CMXDomesticPackage>(pieceItem, line1, line2, line3, Resources.Skin.PieceMode)
                {
                    ButtonVisible = true,
                    ButtonImage = global::Resources.Graphics.Skin._3dots,
                    ButtonPressedImage = global::Resources.Graphics.Skin._3dots_over,
                };
                pieceListItem.ButtonClicked += new EventHandler(pieceListItem_ButtonClicked);

                this.smoothListBoxMainList.AddItem2(pieceListItem);
            }

            LayoutItems();
            smoothListBoxMainList.RefreshScroll();
        }

        private void pieceListItem_ButtonClicked(object sender, EventArgs e)
        {
            var listItem = (sender as CustomListItems.ThreeLineItem<CMXDomesticPackage>);
            var package = listItem.ItemData;
            MessageListBox optionsLIst = new MessageListBox();
            optionsLIst.HeaderText = (sender as CustomListItems.ThreeLineItem<CMXDomesticPackage>).Line1;
            optionsLIst.HeaderText2 = "Select action to continue";
            optionsLIst.OneTouchSelection = true;
            optionsLIst.MultiSelectListEnabled = false;
            optionsLIst.AddItem(new StandardListItem("Edit Piece Details", null, 1));
            optionsLIst.AddItem(new StandardListItem("Attributes", null, 2));
            optionsLIst.AddItem(new StandardListItem("PO Matching", null, 5));
            optionsLIst.AddItem(new StandardListItem("Print Label", null, 3));
            optionsLIst.AddItem(new StandardListItem("Delete Piece", null, 4));

            if (DialogResult.OK == optionsLIst.ShowDialog())
            {
                switch ((optionsLIst.SelectedItems[0] as StandardListItem).ID)
                {
                    case 1:
                        AddEditPiece(package);
                        break;
                    case 2:
                        SharedMethods.EditCurretShipmentAttributes();
                        break;
                    case 3:
                        PrintLabel(package.PieceNumber);
                        break;
                    case 4:
                        DeletePiece(package);
                        break;
                    case 5:
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new PODetails(package.RecID, package.PieceNumber, listItem.Line1));
                        break;
                }
            }
        }

        private void AddEditPiece(IPackage package)
        {
            AddEditPackagePopup addEditPkg = new AddEditPackagePopup(false);
            if (package != null)
                addEditPkg.Package = package;
            if (DialogResult.OK == addEditPkg.ShowDialog())
            {
                var pkg = addEditPkg.Package as CMXDomesticPackage;

                /// populate piece number if its 0
                if (pkg.PieceNumber == 0)
                    pkg.PieceNumber = smoothListBoxMainList.ItemsCount + 1;

                TransactionStatus status;
                if (addEditPkg.Quantitiy > 1)
                    status = Communication.OnHand.OnHand.Instance.SaveOnhandPackageBatch(SharedMethods.CurrentPickup.ShipmentID, pkg, addEditPkg.Quantitiy, null);
                else
                    status = Communication.OnHand.OnHand.Instance.AddOnHandPackage(SharedMethods.CurrentPickup.ShipmentID, pkg);

                if (status.TransactionStatus1 == false)
                {
                    SharedMethods.ShowFailMessage(status.TransactionError);
                }
                LoadControl();
            }
        }

        private void DeletePiece(IPackage package)
        {
            string msg = string.Format(OnhandResources.Text_RemovePkgConfirmation, package.PieceNumber);
            if (DialogResult.OK == UI.CMXMessageBox.Show(msg, "ConfirmDelete", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                var status = Communication.OnHand.OnHand.Instance.RemoveOnHandPackage(package.PackageID, SharedMethods.CurrentPickup.ShipmentID);
                if (status.TransactionStatus1 == false)
                    SharedMethods.ShowFailMessage(status.TransactionError);
                LoadControl();
            }
        }

        private void EditPiecesBatch()
        {
            AddEditPackagePopup addEditPkg = new AddEditPackagePopup(true);
            addEditPkg.Quantitiy = smoothListBoxMainList.SelectedItems.Count;
            if (DialogResult.OK == addEditPkg.ShowDialog())
            {
                var pkg = addEditPkg.Package as CMXDomesticPackage;

                /// populate piece number if its 0
                if (pkg.PieceNumber == 0)
                    pkg.PieceNumber = smoothListBoxMainList.ItemsCount + 1;
                long[] packageIds = smoothListBoxMainList.SelectedItems.OfType<ThreeLineItem<CMXDomesticPackage>>().Select(item => item.ItemData.PackageID).ToArray<long>();
                var status = Communication.OnHand.OnHand.Instance.SaveOnhandPackageBatch(SharedMethods.CurrentPickup.ShipmentID, pkg, packageIds.Length, packageIds);
                if (status.TransactionStatus1 == false)
                {
                    SharedMethods.ShowFailMessage(status.TransactionError);
                }
                LoadControl();
            }
        }

        private void DeletePieceBatch()
        {
            string msg = string.Format(OnhandResources.Text_RemovePackagesBatch, smoothListBoxMainList.SelectedItems.Count);
            if (DialogResult.OK == UI.CMXMessageBox.Show(msg, "ConfirmDelete", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                long[] packageIds = smoothListBoxMainList.SelectedItems.OfType<ThreeLineItem<CMXDomesticPackage>>().Select(item => item.ItemData.PackageID).ToArray<long>();
                var status = Communication.OnHand.OnHand.Instance.RemoveOnhandPackageBatch(packageIds);
                if (status.TransactionStatus1 == false)
                    SharedMethods.ShowFailMessage(status.TransactionError);
                LoadControl();
            }
        }


        private void PrintLabel(int pcNo)
        {
            CustomUtilities.OnhandLabelPrinter.Show(SharedMethods.CurrentPickup.ShipmentID, SharedMethods.CurrentPickup.ShipmentNO, SharedMethods.CurrentPickup.TotalPieces);
        }

        private void PiecesList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(EditPiecesBatchOption);
            this.AddOptionsListItem(DeletePiecesBatchOption);
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL) { Title = "Complete On-Hand" });
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        private void PiecesList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        CompleteOnhand();
                        break;
                    case OptionsListITem.OptionItemID.DELETE_TASK:
                        DeletePieceBatch();
                        break;
                    case OptionsListITem.OptionItemID.EDIT_MEASURMENTS:
                        EditPiecesBatch();
                        break;
                }
        }

        private void CompleteOnhand()
        {
            MessageListBox optionsLIst = new MessageListBox();
            optionsLIst.HeaderText = SharedMethods.CurrentPickup.ShipmentNO;
            optionsLIst.HeaderText2 = "Select Action To Continue";
            optionsLIst.OneTouchSelection = true;
            optionsLIst.MultiSelectListEnabled = false;
            optionsLIst.AddItem(new StandardListItem("STAGE", null, 1));
            optionsLIst.AddItem(new StandardListItem("DROP TO LOCATION", null, 2));

            if (DialogResult.OK == optionsLIst.ShowDialog())
            {
                switch ((optionsLIst.SelectedItems[0] as StandardListItem).ID)
                {
                    case 1:
                        StageToLocation();
                        break;
                    case 2:
                        UI.CMXAnimationmanager.DisplayForm(new DropToLocation());
                        break;
                }
            }
        }

        private void StageToLocation()
        {
            CustomUtilities.ScanEnterPopup_old consolStagePop = new CustomUtilities.ScanEnterPopup_old(CustomUtilities.BarcodeType.Area);
            consolStagePop.Title = SharedMethods.CurrentPickup.ShipmentNO;
            consolStagePop.TextLabel = "Scan or Enter Stage Location";
            if (DialogResult.OK == consolStagePop.ShowDialog())
            {
                var status = Communication.OnHand.OnHand.Instance.CompleteOnhand(SharedMethods.CurrentPickup.ShipmentID, consolStagePop.Text);
                if (status.TransactionStatus1 == false)
                    SharedMethods.ShowFailMessage(status.TransactionError);
                else
                    UI.CMXAnimationmanager.GoToContol(OnHandTasksList.FORM_NAME);
            }
        }

        private void buttonAdd_Click(object sender, System.EventArgs e)
        {
            AddEditPiece(null);
        }

    }
}
