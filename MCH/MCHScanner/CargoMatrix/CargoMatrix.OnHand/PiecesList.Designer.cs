﻿namespace CargoMatrix.OnHand
{
    partial class PiecesList
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPickupRef = new System.Windows.Forms.Label();
            this.labelDest = new System.Windows.Forms.Label();
            this.labelOnhandNo = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonAdd = new CargoMatrix.UI.CMXPictureButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.Location = new System.Drawing.Point(0, 0);
            this.Size = new System.Drawing.Size(240, 292);
            this.SuspendLayout();

            this.panelHeader2.Height = 65;
            // 
            // labelOnhandNo
            // 
            this.labelOnhandNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOnhandNo.BackColor = System.Drawing.Color.White;
            this.labelOnhandNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelOnhandNo.Location = new System.Drawing.Point(42, 6);
            this.labelOnhandNo.Name = "labelOnhandNo";
            this.labelOnhandNo.Size = new System.Drawing.Size(158, 13);
            // 
            // labelPickupRef
            // 
            this.labelPickupRef.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPickupRef.BackColor = System.Drawing.Color.White;
            this.labelPickupRef.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPickupRef.Location = new System.Drawing.Point(42, 20);
            this.labelPickupRef.Name = "labelPickupRef";
            this.labelPickupRef.Size = new System.Drawing.Size(158, 13);
            // 
            // labelDest
            // 
            this.labelDest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDest.BackColor = System.Drawing.Color.White;
            this.labelDest.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDest.Location = new System.Drawing.Point(42, 34);
            this.labelDest.Name = "labelDest";
            this.labelDest.Size = new System.Drawing.Size(158, 13);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(4, 47);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(232, 16);
            // 
            // itemPicture
            // 
            this.itemPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPicture.BackColor = System.Drawing.Color.White;
            this.itemPicture.Location = new System.Drawing.Point(4, 4);//new System.Drawing.Point(202, 41);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // buttonDetails
            // 
            this.buttonAdd.BackColor = System.Drawing.Color.White;
            this.buttonAdd.Location = new System.Drawing.Point(202, 6);
            this.buttonAdd.Name = "buttonDetails";
            this.buttonAdd.Size = new System.Drawing.Size(32, 32);
            this.buttonAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonAdd.TransparentColor = System.Drawing.Color.White;
            this.buttonAdd.Click += new System.EventHandler(buttonAdd_Click);
            this.buttonAdd.Image = global::Resources.Graphics.Skin.add_btn;
            this.buttonAdd.PressedImage = global::Resources.Graphics.Skin.add_btn_over;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            // 
            // PiecesList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelHeader2.Controls.Add(this.labelPickupRef);
            this.panelHeader2.Controls.Add(this.labelDest);
            this.panelHeader2.Controls.Add(this.labelOnhandNo);
            this.panelHeader2.Controls.Add(this.itemPicture);
            this.panelHeader2.Controls.Add(this.buttonAdd);
            this.panelHeader2.Controls.Add(this.panelIndicators);
            this.panelHeader2.Controls.Add(this.splitter1);
            this.Name = "PiecesList";
            this.ResumeLayout(true);

        }

        #endregion

        private System.Windows.Forms.Label labelOnhandNo;
        private System.Windows.Forms.Label labelPickupRef;
        private System.Windows.Forms.Label labelDest;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonAdd;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private System.Windows.Forms.Splitter splitter1;
    }
}
