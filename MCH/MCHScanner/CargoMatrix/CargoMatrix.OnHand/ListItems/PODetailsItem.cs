﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.InventoryWS;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    public partial class PODetailsItem : UserControl
    {
        public event EventHandler ButtonApplyClick;
        public event EventHandler ButtonListClick;
        public event EventHandler ButtonPrintClick;
        public event EventHandler ButtonDoneClick;
        public long? ID { get; private set; }
        public PODetailsItem()
        {
            InitializeComponent();
        }

        private void InitializeComponentHelper()
        {
            this.buttonPrint.Image = Resources.Skin.printerButton;
            this.buttonPrint.PressedImage = Resources.Skin.printerButton_over;
            this.buttonList.Image = Resources.Skin.btn_listView;
            this.buttonList.PressedImage = Resources.Skin.btn_listView_over;
            this.pictureBoxBottomPane.Image = global::Resources.Graphics.Skin.nav_bg;
        }

        public void DisplayPODetails(CMXInvoiceOrder invoicOrder)
        {
            if (invoicOrder.References!= null && invoicOrder.References.Length > 0)
            {
                var DueDocNo = invoicOrder.References.FirstOrDefault(f => f.ID == 6);
                if (DueDocNo != null)
                    this.labelItemNo.Text = DueDocNo.ReferenceValue;
            }
            this.labelOrderQty.Text = invoicOrder.OrderQty.ToString();
            this.labelPartDescr.Text = invoicOrder.PartDescription;
            this.labelPartNo.Text = invoicOrder.PartNumber;
            this.labelRemQty.Text = invoicOrder.RemainingQty.ToString();
            this.ID = invoicOrder.ID;
            this.textBoxShipQty.Focus();
        }

        void buttonApply_Click(object sender, System.EventArgs e)
        {
            if (ButtonApplyClick != null)
                ButtonApplyClick(this, e);
        }

        void buttonList_Click(object sender, System.EventArgs e)
        {
            if (ButtonListClick != null)
                ButtonListClick(this, e);
        }

        void buttonDone_Click(object sender, System.EventArgs e)
        {
            if (ButtonDoneClick != null)
                ButtonDoneClick(this, e);
        }

        void buttonPrint_Click(object sender, System.EventArgs e)
        {
            if (ButtonPrintClick != null)
                ButtonPrintClick(this, e);
        }

        public double? ShipQuantity
        {
            get
            {
                try
                {
                    return double.Parse(textBoxShipQty.Text);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        void textBoxShipQty_TextChanged(object sender, EventArgs e)
        {
            buttonApply.Enabled = !string.IsNullOrEmpty(textBoxShipQty.Text);
        }
    }
}
