﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CustomUtilities;
using CargoMatrix.Communication.PieceScanWCF;
using CargoMatrix.Communication.WSOnHand;
using CustomListItems;

namespace CargoMatrix.OnHand
{
    public partial class OnHandDetails : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        private TaskDetailsItem detailsItem;
        public OnHandDetails()
        {
            InitializeComponent();
            this.TitleText = "CargoOnhand - Receving Details";
            this.detailsItem = new TaskDetailsItem();
            this.Scrollable = false;
            this.panelHeader2.Height = this.smoothListBoxMainList.Bottom - this.panelHeader2.Top;
            this.panelHeader2.Controls.Add(detailsItem);
            this.detailsItem.ButtonDetailsClick += new EventHandler(detailsItem_ButtonDetailsClick);
            this.detailsItem.ButtonRecoverClick += new EventHandler(detailsItem_ButtonRecoverClick);
            this.detailsItem.ButtonTruckerClick += new EventHandler(detailsItem_ButtonTruckerClick);
            this.detailsItem.ButtonVendorClick += new EventHandler(detailsItem_ButtonVendorClick);
            this.LoadOptionsMenu += new EventHandler(OnHandDetails_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(OnHandDetails_MenuItemClicked);
        }

        void OnHandDetails_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as OptionsListITem).ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:
                    LoadControl();
                    break;
            }
        }
        public override void LoadControl()
        {
            Application.DoEvents();
            detailsItem.Focus();
        }
        void OnHandDetails_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new OptionsListITem(OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }


        #region Event Handlers

        void detailsItem_ButtonVendorClick(object sender, EventArgs e)
        {
            CMXCarrier carier = SharedMethods.DisplayTruckingCompanies(detailsItem.TruckingCompany);
            if (null != carier)
            {
                this.detailsItem.TruckingCompany = carier.CarrierName;
            }
        }

        void detailsItem_ButtonTruckerClick(object sender, EventArgs e)
        {
            string trucker = SharedMethods.DisplayTruckersList(detailsItem.TruckingCompany);
            if (null != trucker)
            {
                this.detailsItem.TruckerName = trucker;
            }

        }
        void detailsItem_ButtonDetailsClick(object sender, EventArgs e)
        {
            Control[] actions = new Control[]
            {
                new StandardListItem("ORDER REFERENCES",null,1),
                new StandardListItem("PHOTO CAPTURE",null,2) {Enabled = false},
            };

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.OneTouchSelection = true;
            actPopup.MultiSelectListEnabled = false;
            actPopup.HeaderText = "Onhand";
            actPopup.HeaderText2 = "Select action to continue";
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        ReferenceEditor refeditor = new ReferenceEditor();
                        refeditor.ShowDialog();
                        break;
                    case 2:
                        //Reasons reasonsControl = null;
                        //var onhand = Communication.OnHand.OnHand.Instance.GetOnHandDetails(SharedMethods.CurrentPickup.ShipmentNO);
                        //ReasonsLogic.DisplayReasons(onhand.MawbCarrier, onhand.MawbNo, CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL, this, ref reasonsControl);
                        break;
                }
            }
        }

        void detailsItem_ButtonRecoverClick(object sender, EventArgs e)
        {
            CustomUtilities.MultiScanPopup mscanPop = new CustomUtilities.MultiScanPopup();
            mscanPop.Header = SharedMethods.CurrentPickup.ShipmentNO;

            mscanPop.Caption = "Scan or enter number";
            mscanPop.Label1 = "Scan door barcode";
            mscanPop.Label2 = "Scan truck barcode (optional)";
            mscanPop.Text2ISOptional = true;
            mscanPop.Prefix1 = CustomUtilities.BarcodeType.Door;
            mscanPop.Prefix2 = CustomUtilities.BarcodeType.Truck;
            if (DialogResult.OK == mscanPop.ShowDialog())
            {
                byte[] signature = CaptureSignature();
                if (signature == null)
                    return;
                string msg = string.Format(OnhandResources.Text_ContinueOnhand, SharedMethods.CurrentPickup.ShipmentNO);
                var recoverStatus = Communication.OnHand.OnHand.Instance.RecoverPickup(SharedMethods.CurrentPickup.ShipmentNO, SharedMethods.CurrentPickup.ShipmentID, detailsItem.TruckingCompany,
                    detailsItem.TruckerName, detailsItem.PRO, detailsItem.Pieces, mscanPop.Text1, mscanPop.Text2, signature);

                if (recoverStatus.TransactionStatus1 == false)
                {
                    SharedMethods.ShowFailMessage(recoverStatus.TransactionError);
                    return;
                }

                if (DialogResult.OK == UI.CMXMessageBox.Show(msg, OnhandResources.Text_ContinueOnhandHeader, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new PiecesList());
                else
                {
                    ScanEnterPopup_old dropLoc = new ScanEnterPopup_old(BarcodeType.Area);
                    dropLoc.Title = "Scan Or Enter Drop Location";
                    if (DialogResult.OK == dropLoc.ShowDialog())
                    {
                        var status = Communication.OnHand.OnHand.Instance.StagePickupAtLocation(SharedMethods.CurrentPickup.ShipmentID, dropLoc.ScannedText);
                        if (status.TransactionStatus1 == false)
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        else
                            UI.CMXAnimationmanager.GoToContol(OnHandTasksList.FORM_NAME);
                    }
                }
            }
        }

        #endregion

        #region Popups

        private byte[] CaptureSignature()
        {
            SignatureCaptureForm signatureForm = new SignatureCaptureForm();


            signatureForm.Header = SharedMethods.CurrentPickup.ShipmentNO;

            //signatureForm.pi

            Cursor.Current = Cursors.Default;

            DialogResult result = signatureForm.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                return signatureForm.SugnatureAsBytes;
            }
            else
                return null;
        }
        #endregion
    }
}
