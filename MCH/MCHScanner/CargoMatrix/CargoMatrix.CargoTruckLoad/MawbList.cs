﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.Common;
using CargoTruckWebService = CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.UI;
using CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.Utilities;
using CMXExtensions;

namespace CargoMatrix.CargoTruckLoad
{
    public partial class MawbList : CargoUtilities.SmoothListBoxAsyncOptions<ITruckLoadMasterBillItem>
    {
        MasterBillFilterCriteria filterCriteria;
        CargoTruckWebService.MasterBillSortCriteria sortCriteria;
        OptionsListITem filterMenuItem = new OptionsListITem(OptionsListITem.OptionItemID.FILTER);
        
        private int listItemHeight;
        private CargoMatrix.UI.CMXTextBox txtSearch;
        
        #region Constructors
        public MawbList()
        {
            InitializeComponent();
            this.TitleText = "CargoTruckLoad";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_ListItemClicked);

            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
            
            this.filterCriteria = new MasterBillFilterCriteria();
            this.sortCriteria = new CargoTruckWebService.MasterBillSortCriteria();
        } 
        #endregion

        #region Handlers
        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            IMasterBillItem masterBill = null;
            bool scanResult = false;

            Cursor.Current = Cursors.WaitCursor;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    
                    MasterBillTruckLoadItem listItem = this.GetListItem(item => string.Compare(item.MasterBill.MasterBillNumber, scanItem.MasterBillNumber, true) == 0);

                    if (listItem == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtMasterBillNotInList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        scanResult = false;
                        break;
                    }

                    masterBill = listItem.MasterBill;
                    scanResult = true;

                    break;

                case CMXBarcode.BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    scanResult = this.GetMasterBillByHouseBillNumber(scanItem.HouseBillNumber, out masterBill);

                    if (scanResult == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtHouseBillNotInMawbList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        masterBill = null;
                    }

                    break;

                case CMXBarcode.BarcodeTypes.Uld:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    scanResult = this.GetMasterBillByUldNumber(scanItem.UldNumber, out masterBill);

                    if (scanResult == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtUldBillNotInMawbList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        masterBill = null;
                    }

                    break;


                case CMXBarcode.BarcodeTypes.Door:

                    scanResult = this.GetMasterBillByDoor(scanItem.Location, out masterBill);

                    if(scanResult == true)
                    {
                        this.EnableUI(scanItem.Location, false);
                        BarcodeEnabled = true;
                        Cursor.Current = Cursors.Default;

                        return;
                    }

                    break;

                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongBarcodeType, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                    masterBill = null;
                    scanResult = false;
                    break;
            }

            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;

            if (scanResult == false) return;

            this.DisplayDoorTruckSetUp(masterBill);
        }

        void MawbList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
            {
                var masterBilllistItem = (MasterBillTruckLoadItem)listItem;

                this.DisplayDoorTruckSetUp(masterBilllistItem.MasterBill);
            }
            else
            {
                smoothListBoxMainList.MoveControlToTop(listItem);
                smoothListBoxMainList.RefreshScroll();
            }
        }

        void MawbList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.FILTER:

                    FilterPopup fp = new FilterPopup(this.filterCriteria, this.sortCriteria);
                    if (DialogResult.OK == fp.ShowDialog())
                    {
                        this.filterCriteria = fp.FilterCriteria;
                        this.sortCriteria = fp.SortCriteria;

                        this.filterMenuItem.DescriptionLine = this.GetFilteMenuItemDescriptionText();

                        LoadControl();
                    }

                    break;

                case OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;

                default: return;
            }
        }

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));

            this.filterMenuItem.DescriptionLine = this.GetFilteMenuItemDescriptionText();

            this.AddOptionsListItem(this.filterMenuItem);

            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void tempMAWB_OnMoreClick(object sender, EventArgs e)
        {
            var masterBill = ((MasterBillTruckLoadItem)sender).MasterBill;

            MessageListBox actPopup = new MessageListBox();

            var printLabesListItem = new SmoothListbox.ListItems.StandardListItem("PRINT LABELS", null, 1);

            actPopup.AddItem(printLabesListItem);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = masterBill.Reference();
            actPopup.HeaderText2 = "Select action to continue";

            var dialogResult = actPopup.ShowDialog();

            if (DialogResult.OK != dialogResult) return;

            switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:

                    CommonMethods.PrintMasterBillLabels(masterBill);

                    break;

                default: return;
            }


        }

        void tempMAWB_OvDamageViewClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var masterBillListItem = (MasterBillTruckLoadItem)sender;

            MssterBillAsHouseBill masterAsHouseBill = new MssterBillAsHouseBill(masterBillListItem.MasterBill);

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(masterAsHouseBill, DamageCapture.DamageCaptureApplyTypeEnum.MasterBill);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionsSummarys(masterBillListItem.MasterBill.TaskId);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.ScannerUtility.Instance.UpdateMasterBillConditions(masterBillListItem.MasterBill.TaskId, args.ShipmentConditions);
            };

            CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }

        private void MAWB_Viewer_Click(object listItem, EventArgs e)
        {
            var masterBillLIstItem = (MasterBillTruckLoadItem)listItem;

            if (masterBillLIstItem == null) return;

            var masterBill = masterBillLIstItem.MasterBill;

            CargoMatrix.Viewer.HousebillViewer billViewer = new CargoMatrix.Viewer.HousebillViewer(masterBill.CarrierNumber, masterBill.MasterBillNumber);
            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }

        private void MAWB_Enter_Click(object sender, EventArgs e)
        {
            var masterBillListItem = (MasterBillTruckLoadItem)sender;

            this.DisplayDoorTruckSetUp(masterBillListItem.MasterBill);
        }

        void searchBox_TextChanged(object sender, EventArgs e)
        {
            foreach (var item in smoothListBoxMainList.Items)
            {
                var masterBillListItem = item as MasterBillTruckLoadItem;

                if (masterBillListItem == null) return;

                var masterBill = masterBillListItem.MasterBill;

                if (this.ItemTextMatch(txtSearch.Text, masterBill) == true)
                {
                    if (masterBillListItem.Height == 0)
                        masterBillListItem.Height = listItemHeight;
                }
                else
                {
                    if (masterBillListItem.IsSelected)
                        smoothListBoxMainList.Reset();

                    masterBillListItem.Height = 0;
                }
            }
            LayoutItems();
            smoothListBoxMainList.RefreshScroll();
        }
        #endregion

        #region Overrides
        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            var items = CargoMatrix.Communication.LoadConsol.Instance.GetLoadTruckMasterBills(this.filterCriteria, this.sortCriteria);

            this.TitleText = this.GetMainListHeaderText(items.Count());

            ReloadItemsAsync(items);

            this.EnableUI("", true);

            Cursor.Current = Cursors.Default;

            this.Focus();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

        }

        protected override Control InitializeItem(ITruckLoadMasterBillItem item)
        {
            MasterBillTruckLoadItem tempMAWB = new MasterBillTruckLoadItem(item);
            tempMAWB.OnViewerClick += new EventHandler(MAWB_Viewer_Click);
            tempMAWB.OnEnterClick += new EventHandler(MAWB_Enter_Click);
            tempMAWB.OnDamageViewClick += new EventHandler(tempMAWB_OvDamageViewClick);
            tempMAWB.OnMoreClick += new EventHandler(tempMAWB_OnMoreClick);

            listItemHeight = tempMAWB.Height;
            return tempMAWB as Control;
        }

        #endregion

        #region Additional Functionallity
        private string GetMainListHeaderText(int listItemsCount)
        {
            return string.Format("CargoTruckLoad ({0})", listItemsCount);
        }

        private string GetFilteMenuItemDescriptionText()
        {
            return string.Format("{0} / {1}", this.filterCriteria, this.sortCriteria);
        }

        private bool GetMasterBillByHouseBillNumber(string houseBillNumber, out IMasterBillItem masterBill)
        {
            masterBill = null;

            int? masterBillID = CargoMatrix.Communication.LoadConsol.Instance.GetMasterBillId(houseBillNumber);

            if (masterBillID.HasValue == false) return false;

            masterBill = (
                            from item in this.smoothListBoxMainList.Items.OfType<MasterBillTruckLoadItem>()
                            where item.MasterBill.MasterBillId == masterBillID
                            select item.MasterBill
                         ).FirstOrDefault();

            return masterBill != null;

        }

        private bool GetMasterBillByUldNumber(string uldNumber, out IMasterBillItem masterBill)
        {
            masterBill = null;

            int? masterBillID = CargoMatrix.Communication.LoadConsol.Instance.GetMasterBillIdByULDNumber(uldNumber);

            if (masterBillID.HasValue == false) return false;

            masterBill = (
                            from item in this.smoothListBoxMainList.Items.OfType<MasterBillTruckLoadItem>()
                            where item.MasterBill.MasterBillId == masterBillID
                            select item.MasterBill
                         ).FirstOrDefault();

            return masterBill != null;
        }

        private bool GetMasterBillByDoor(string doorName, out IMasterBillItem masterBill)
        {
            masterBill = (
                            from item in this.smoothListBoxMainList.Items.OfType<MasterBillTruckLoadItem>()
                            from door in item.MasterBill.Doors
                            where door.Door.Equals(doorName)
                            select item.MasterBill
                         ).FirstOrDefault();

            return masterBill != null;
        }

        private void DisplayDoorTruckSetUp(IMasterBillItem masterBlllItem)
        {
            //Cursor.Current = Cursors.WaitCursor;

            //var status = CargoMatrix.Communication.LoadConsol.Instance.CanLoadTruckStart(masterBlllItem.MasterBillId);

            //Cursor.Current = Cursors.Default;

            //if (status.TransactionStatus1 == true)
            //{
            CMXAnimationmanager.DisplayForm(new DoorTruckSetUp(masterBlllItem));
            //}
            //else
            //{
            //    var presentMasterBill = this.GetListItem(item => item.MasterBill.MasterBillId == status.TransactionId);

            //    if (presentMasterBill == null)
            //    {
            //        CargoMatrix.UI.CMXMessageBox.Show("Unable to start load. There are pieces in the forklift for masterbill ID " + status.TransactionId + " in the list", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);
            //        Cursor.Current = Cursors.Default;
            //        return;
            //    }

            //    string message = string.Format("{0} {1}. {2}", LoadTruckResources.TxtForkliftContains, presentMasterBill.MasterBill.Reference(), LoadTruckResources.TxtCompleteLoad);

            //    var dResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message, MessageBoxButtons.OKCancel, DialogResult.OK);

            //    if (dResult != DialogResult.OK) return;

            //    Cursor.Current = Cursors.WaitCursor;

            //    CMXAnimationmanager.DisplayForm(new DoorTruckSetUp(presentMasterBill.MasterBill));
            //}
     
            
        }
        
        private MasterBillTruckLoadItem GetListItem(Func<MasterBillTruckLoadItem, bool> wherePredicate)
        {
            return (
                        from item in this.smoothListBoxMainList.Items.OfType<MasterBillTruckLoadItem>().Where(wherePredicate)
                        select item
                    ).FirstOrDefault();
        }

       
        private void EnableUI(string searchText, bool enable)
        {
            this.txtSearch.Text = searchText;
            this.txtSearch.Enabled = enable;
        }

        bool ItemTextMatch(string text, ITruckLoadMasterBillItem masterBill)
        {
            string textToMatch = text.TrimEnd(new char[]{' '}).ToUpper();

            if (masterBill.Reference().ToUpper().Contains(textToMatch)) return true;

            var door = (from item in masterBill.Doors
                        where item.Door.ToUpper().Contains(textToMatch)
                        select item).FirstOrDefault();

            return door != null;
        }


        #endregion
    }
}
