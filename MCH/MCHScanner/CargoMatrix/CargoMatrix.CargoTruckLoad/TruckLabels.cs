﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;

namespace CargoMatrix.CargoTruckLoad
{
    public partial class TruckLabels : CargoMatrix.Utilities.MessageListBox
    {
        CargoMatrix.Communication.WSLoadConsol.LocationItem[] trucks;

        public TruckLabels(CargoMatrix.Communication.WSLoadConsol.LocationItem[] trucks)
        {
            InitializeComponent();

            this.trucks = trucks;

            smoothListBoxBase1.MultiSelectEnabled = true;
            this.OneTouchSelection = false;
            this.HeaderText = "Print truck labels View";
            this.HeaderText2 = "Trucks";

            this.PopulateTruckLabelsContent();
        }

        private void PopulateTruckLabelsContent()
        {
            foreach (var truck in this.trucks)
            {
                var listItem = new StandardListItem<CargoMatrix.Communication.WSLoadConsol.LocationItem>(truck.Location, CargoMatrix.Resources.Icons.Truck, truck.LocationId, truck);
                this.AddItem(listItem);
            }
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            base.pictureBoxOk_Click(sender, e);

            var trucks = from item in this.smoothListBoxBase1.SelectedItems.OfType<StandardListItem<CargoMatrix.Communication.WSLoadConsol.LocationItem>>()
                           where item.IsSelected == true
                           select new
                           {
                               ID = item.ItemData.LocationId,
                               Name = item.ItemData.Location
                           };

            var ids = from truck in trucks
                      select truck.ID;


            var names = from truck in trucks
                        select truck.Name;

            CommonMethods.PrintTruckLabels(names, ids);

        }
    }
}