﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CustomListItems;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.DTO;
using CargoTruckWebService = CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.Communication.CargoTruckLoad;


namespace CargoMatrix.CargoTruckLoad
{
    public partial class FilterPopup : MessageListBox
    {
        private ChoiceListItem filterListItem;
        private ChoiceListItem sortListItem;
        MessageListBox subFilterPopup;
        MasterBillFilterCriteria filterCriteria;
        CargoTruckWebService.MasterBillSortCriteria sortCriteria;


        public MasterBillFilterCriteria FilterCriteria
        {
            get 
            {
                return filterCriteria; 
            }
            set 
            {
                filterCriteria = value;
                filterListItem.LabelLine2.Text = value.ToString(); 
            }
        }
        public CargoTruckWebService.MasterBillSortCriteria SortCriteria
        {
            get 
            { 
                return sortCriteria; 
            }
            set 
            {
                sortCriteria = value;
                sortListItem.LabelLine2.Text = value.ToString(); 
            }
        }

        public FilterPopup(MasterBillFilterCriteria filterCriteria, CargoTruckWebService.MasterBillSortCriteria sortCriteria)
        {
            InitializeComponent();

            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            TopPanel = false;

            this.filterCriteria = filterCriteria;
            this.sortCriteria = sortCriteria;

            this.filterListItem = new CustomListItems.ChoiceListItem("Filter", CargoMatrix.Resources.Skin.Sort, filterCriteria.ToString());
            this.sortListItem = new CustomListItems.ChoiceListItem("Sort", CargoMatrix.Resources.Skin.Filter, sortCriteria.ToString());

            this.AddItem(filterListItem);
            this.AddItem(sortListItem);

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FilterPopup_ListItemClicked);
        }

        void FilterPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (this.subFilterPopup == null)
            {
                this.subFilterPopup = new MessageListBox();
                subFilterPopup.MultiSelectListEnabled = false;
                subFilterPopup.OneTouchSelection = true;
                subFilterPopup.TopPanel = false;
            }

            this.subFilterPopup.RemoveAllItems();

            if (listItem == this.filterListItem) // filtering ...
            {
                this.subFilterPopup.HeaderText = "Filter by";

                this.subFilterPopup.AddItems(this.GetFilters());

                this.SelectItem(this.subFilterPopup, (int)filterCriteria.Value);
                
                if (this.subFilterPopup.ShowDialog() == DialogResult.OK)
                {
                    this.filterCriteria.Value = (FilteringFieldsEnum)((StandardListItem)this.subFilterPopup.SelectedItems[0]).ID;
                    filterListItem.LabelLine2.Text = this.filterCriteria.ToString();
                }
            }
            else
            {
                if (listItem == this.sortListItem) // sorting ...
                {
                    this.subFilterPopup.HeaderText = "Sort by";

                    this.subFilterPopup.AddItems(this.GetSorts());

                    this.SelectItem(this.subFilterPopup, (int)sortCriteria.Value);

                    if (this.subFilterPopup.ShowDialog() == DialogResult.OK)
                    {
                        this.sortCriteria.Value = (CargoTruckWebService.SortingFieldsEnum)((StandardListItem)this.subFilterPopup.SelectedItems[0]).ID;
                        sortListItem.LabelLine2.Text = sortCriteria.ToString();
                    }
                }
            }
        }

        void SelectItem(MessageListBox listBox, int itemID)
        {
            var selectedItem = (
                                   from item in listBox.Items.OfType<StandardListItem>()
                                   where item.ID == itemID
                                   select item
                               ).FirstOrDefault();

            if (selectedItem != null)
            {
                listBox.SelectControl(selectedItem);
            }
        }


        Control[] GetFilters()
        {
            return new Control[] 
                {
                    new StandardListItem(MasterBillFilterCriteria.ToString(FilteringFieldsEnum.ALL), null, (int)FilteringFieldsEnum.ALL),
                    new StandardListItem(MasterBillFilterCriteria.ToString(FilteringFieldsEnum.NOT_STARTED), null, (int)FilteringFieldsEnum.NOT_STARTED),
                    new StandardListItem(MasterBillFilterCriteria.ToString(FilteringFieldsEnum.IN_PROGRESS), null, (int)FilteringFieldsEnum.IN_PROGRESS),
                    new StandardListItem(MasterBillFilterCriteria.ToString(FilteringFieldsEnum.NOT_COMPLETED), null, (int)FilteringFieldsEnum.NOT_COMPLETED),
                    new StandardListItem(MasterBillFilterCriteria.ToString(FilteringFieldsEnum.COMPLETED), null, (int)FilteringFieldsEnum.COMPLETED)
                };
        }

        Control[] GetSorts()
        {
            return new Control[] 
                {
                    new StandardListItem(CargoTruckWebService.MasterBillSortCriteria.ToString(CargoTruckWebService.SortingFieldsEnum.NA), null, (int)CargoTruckWebService.SortingFieldsEnum.NA),
                    new StandardListItem(CargoTruckWebService.MasterBillSortCriteria.ToString(CargoTruckWebService.SortingFieldsEnum.CarrierNumber), null, (int)CargoTruckWebService.SortingFieldsEnum.CarrierNumber),
                    new StandardListItem(CargoTruckWebService.MasterBillSortCriteria.ToString(CargoTruckWebService.SortingFieldsEnum.MasterBillNumber), null, (int)CargoTruckWebService.SortingFieldsEnum.MasterBillNumber),
                    new StandardListItem(CargoTruckWebService.MasterBillSortCriteria.ToString(CargoTruckWebService.SortingFieldsEnum.Weight), null, (int)CargoTruckWebService.SortingFieldsEnum.Weight),
                    new StandardListItem(CargoTruckWebService.MasterBillSortCriteria.ToString(CargoTruckWebService.SortingFieldsEnum.Origin), null, (int)CargoTruckWebService.SortingFieldsEnum.Origin),
                    new StandardListItem(CargoTruckWebService.MasterBillSortCriteria.ToString(CargoTruckWebService.SortingFieldsEnum.CutOff), null, (int)CargoTruckWebService.SortingFieldsEnum.CutOff),

                };
        }

    }
}