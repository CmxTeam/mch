﻿namespace CargoMatrix.CargoTruckLoad
{
    partial class ULDList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.labelReference = new System.Windows.Forms.Label();
            this.labelBlink = new CustomUtilities.CMXBlinkingLabel();
            this.btnForkliftScanned = new CargoMatrix.UI.CMXCounterIcon();
            this.SuspendLayout();
            //
            //pcxLogo
            //
            this.pcxLogo = new System.Windows.Forms.PictureBox();
            this.pcxLogo.Name = "pcxLogo";
            this.pcxLogo.Size = new System.Drawing.Size(32, 32);
            this.pcxLogo.Location = new System.Drawing.Point(3, 3);
            this.pcxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            
            // 
            // labelReference
            // 
            this.labelReference.Location = new System.Drawing.Point(39, 3);
            this.labelReference.Name = "labelReference";
            this.labelReference.Size = new System.Drawing.Size(160, 13);
            this.labelReference.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            
            //
            //lblLine2
            //
            this.lblLine2 = new System.Windows.Forms.Label();
            this.lblLine2.Name = "lblLine2";
            this.lblLine2.Location = new System.Drawing.Point(39, 20);
            this.lblLine2.Size = new System.Drawing.Size(160, 13);
            
            // 
            // labelBlink
            // 
            this.labelBlink.Location = new System.Drawing.Point(39, 37);
            this.labelBlink.Name = "labelBlink";
            this.labelBlink.ForeColor = System.Drawing.Color.Red;
            this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelBlink.Size = new System.Drawing.Size(160, 13);

            //
            // buttonScannedList
            //
            this.btnForkliftScanned.Location = new System.Drawing.Point(200, 3);
            this.btnForkliftScanned.Name = "buttonScannedList";
            this.btnForkliftScanned.Image = CargoMatrix.Resources.Skin.Forklift_btn_34x34;
            this.btnForkliftScanned.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_34x34_over;
            this.btnForkliftScanned.Size = new System.Drawing.Size(36, 36);
            this.btnForkliftScanned.TabIndex = 1;
            this.btnForkliftScanned.Click += new System.EventHandler(btnForkliftScanned_Click);
            this.btnForkliftScanned.BringToFront();

            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            this.splitter1.BackColor = System.Drawing.Color.DarkGray;
            
            
            // 
            // ULDList
            // 
            panelHeader2.Height = 53;

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.Controls.Add(this.pcxLogo);
            this.panelHeader2.Controls.Add(this.labelReference);
            this.panelHeader2.Controls.Add(this.lblLine2);
            this.panelHeader2.Controls.Add(this.labelBlink);
            this.panelHeader2.Controls.Add(this.btnForkliftScanned);
            this.panelHeader2.Controls.Add(this.splitter1);
            this.Name = "ULDList";
            this.Size = new System.Drawing.Size(240, 292);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label labelReference;
        private System.Windows.Forms.Label lblLine2;
        private System.Windows.Forms.PictureBox pcxLogo;
        private CustomUtilities.CMXBlinkingLabel labelBlink;
        private CargoMatrix.UI.CMXCounterIcon btnForkliftScanned;
    }
}
