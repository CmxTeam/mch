﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrixScanner
{
    public partial class CMXPasswordBox : UserControl
    {
        public new event EventHandler TextChanged;
        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        public CMXPasswordBox()
        {
            InitializeComponent();
            m_originalColor = BackColor;
            timer1.Interval = 800;
            timer1.Tick += new EventHandler(timer1_Tick);
        }
        Color m_originalColor;
        public new Font Font
        {
            get { return textBox.Font; }
            set
            {

            }
        }
        private void CMXTextBox_GotFocus(object sender, EventArgs e)
        {
            //CMXAnimationmanager.GetParent().KeyPreview = false;
            textBox.SelectionStart = textBox.Text.Length;
            textBox.Focus();
            textBox_GotFocus(null, null);

        }
        public new void Focus()
        {
            textBox.Focus();
        }
        private void textBox_GotFocus(object sender, EventArgs e)
        {
            //CMXAnimationmanager.GetParent().KeyPreview = false;
            textBox.SelectionStart = textBox.Text.Length;
            this.BackColor = Color.Red;
            SelectAll();
            textBox.ForeColor = Color.Black;
        }

        public override string Text
        {
            get
            {
                return textBox.Text;
            }
            set
            {
                textBox.Text = value;
            }
        }
        public new bool Focused
        {
            get
            {
                return textBox.Focused;
            }
        }

        public void SelectAll()
        {
            textBox.SelectAll();
        }

        private void CMXTextBox_LostFocus(object sender, EventArgs e)
        {
            this.BackColor = m_originalColor;
            //CMXAnimationmanager.GetParent().KeyPreview = true;


            textBox_LostFocus(null, null);
        }

        private void textBox_LostFocus(object sender, EventArgs e)
        {
            this.BackColor = m_originalColor;
            //CMXAnimationmanager.GetParent().KeyPreview = true;
        }
        private int scaleRatio
        {
            get
            { return (int)(CargoMatrix.UI.CMXAnimationmanager.GetParent().CurrentAutoScaleDimensions.Height / 96F); }
        }
        private void CMXTextBox_Resize(object sender, EventArgs e)
        {
            base.Height =  textBox.Height + 2 * scaleRatio;
            this.textBox.Width = Width - (2 * scaleRatio);
        }
        public int MaxLength
        {
            get { return textBox.MaxLength; }
            set { textBox.MaxLength = value; }
        }

        public int SelectionStart
        {
            get { return textBox.SelectionStart; }
            set { textBox.SelectionStart = value; }
        }
        public int SelectionLength
        {
            get { return textBox.SelectionLength; }
            set { textBox.SelectionLength = value; }
        }
        public string SelectedText
        {
            get { return textBox.SelectedText; }
            set { textBox.SelectedText = value; }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (this.TextChanged != null)
                this.TextChanged(this, e);
        }
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {

                // display hidden chars in the left
                int oldstart = textBox.SelectionStart;
                textBox.SelectionStart = 0;
                textBox.SelectionStart = oldstart;
                //

                if (textBox.SelectionStart == textBox.Text.Length)//(textBox1.TextLength < 10)
                {
                    int left = (2 + (textBox.SelectionStart) * 10) * scaleRatio;
                    labelMask.Text = (e.KeyChar).ToString();
                    labelMask.Left = textBox.Left + left;
                    if (labelMask.Right > textBox.Right - 1)
                        labelMask.Left = textBox.Right - labelMask.Width - 4;
                    splash();
                }
            }
        }


        void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            labelMask.Visible = false;
        }
        private void splash()
        {
            timer1.Enabled = false;
            timer1.Enabled = true;
            labelMask.Visible = true;

        }
    }
}
