﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Collections.Generic;
namespace CargoMatrix.CargoReceiver
{
    public partial class FlightShipmentsList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

        
        private FlightItem flightDetailsItem;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        //CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;
        //private long manifestId;

        //private string selectedUld = string.Empty;
        public FlightShipmentsList(FlightItem flightDetailsItem)
        {
            //this.selectedUld = selectedUld;
            this.flightDetailsItem = flightDetailsItem;
      
             
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_ListItemClicked);
        }


        void ValidateShipment(string shipment)
        {
            CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo[] obj = CargoMatrix.Communication.WebServiceManager.Instance().ScanShipmentMCH(flightDetailsItem.TaskId, shipment);

            List<string> ulds = new List<string>();
            try
            {
                foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo o in obj)
                {
                    if (o.Uld.UldType.ToUpper() == "LOOSE")
                    {
                        ulds.Add(o.Uld.UldType);
                    }
                    else
                    {
                        ulds.Add(o.Uld.UldType + o.Uld.UldSerialNo);
                    }

                }
            }
            catch { }

            string uld = string.Empty;
            if (ulds.Count == 1)
            {
                uld = ulds[0];
            }
            else if (ulds.Count > 1)
            {
                uld = SelectUld(ulds);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Unable to locate details for this shipment. Check if this shipment exists in system.", "Shipment details not found!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }


            if (uld != string.Empty)
            {
                FlightReceiver.scannedUld = uld;
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
            }

        }
        protected virtual void UldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
        
            ShipmentCargoItem tempShipmentCargoItem = (ShipmentCargoItem)listItem;
            ShipmentItem tempShipmetItem = (ShipmentItem)tempShipmentCargoItem.ItemData;
            //AddShipmentIntoForkLift(tempShipmetItem);


            string shipment = string.Empty;
            if (tempShipmetItem.Hwb != null)
            {
                shipment = tempShipmetItem.Hwb;
            }
            if (shipment == string.Empty)
            {
                shipment = tempShipmetItem.AWB;
            }

            ValidateShipment(shipment);

      }



        string SelectUld(List<string> ulds)
        {

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Select Ulds";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";


            actPopup.RemoveAllItems();
            for (int i = 0; i < ulds.Count ; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(ulds[i], null, i + 1));
            }

            
             


            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }



        }
 
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("buttonFilter_Click");
        }

        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

 

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
 
            //this.label1.Text = string.Format("{0}", filter);
 

            this.label1.Text =string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            this.label2.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.RecoveredULDs, this.flightDetailsItem.ULDCount);
            this.label3.Text = string.Format("PCS: {0} of {1}", this.flightDetailsItem.ReceivedPieces, this.flightDetailsItem.TotalPieces);

            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;


            CargoMatrix.Communication.DTO.ShipmentItem[] shipmentItemsArray = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightViewMCH(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId);


                     if (shipmentItemsArray!=null)
                     {
                         var rItems = from i in shipmentItemsArray
                                      select InitializeItem(i);
                         smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                     }


                     string flight = string.Format("{0}  {1}  {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);

                     this.TitleText = string.Format("CargoReceiver - {0} ({1})", flight, shipmentItemsArray.Length);
         
            Cursor.Current = Cursors.Default;
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);


            string flight = string.Format("{0}  {1}  {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);


            this.TitleText = string.Format("CargoReceiver - {0} ({1})", flight, smoothListBoxMainList.VisibleItemsCount);
      
        }

        protected ICustomRenderItem InitializeItem(ShipmentItem item)
        {
            ShipmentCargoItem tempShipment = new ShipmentCargoItem(item);
            tempShipment.OnEnterClick += new EventHandler(ULD_Enter_Click);
            tempShipment.ButtonClick += new EventHandler(ULD_OnMoreClick);
            listItemHeight = tempShipment.Height;
            return tempShipment;
        }

        void ULD_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
             
            Cursor.Current = Cursors.Default;
            MessageBox.Show("OnMoreClick" + (sender as ShipmentCargoItem).ItemData.AWB);
        }
 
  
        protected virtual void ULD_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ShipmentItem shipment = (sender as ShipmentCargoItem).ItemData;
            ProceedWithUld(shipment);
            Cursor.Current = Cursors.Default;
        }


   
 
 
        private bool ProceedWithUld(ShipmentItem tempShipment)
        {
            //if (tempShipment == null)
            //    return false;

            //MessageBox.Show("ProceedWithUld" + tempShipment.AWB);
            ////CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
            //Cursor.Current = Cursors.Default;
            return true;
        }



        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            ValidateShipment(barcodeData);
          
            Cursor.Current = Cursors.Default;
         
            //if (!BarcodeEnabled)
            //{
                BarcodeEnabled = true;
            //}


        }
    }

}
