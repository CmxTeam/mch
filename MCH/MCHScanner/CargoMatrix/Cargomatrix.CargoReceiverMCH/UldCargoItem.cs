﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;

namespace CargoMatrix.CargoReceiver
{
    public partial class UldCargoItem : CustomListItems.ExpandableRenderListitem<UldItem>, ISmartListItem
    {

        public event EventHandler ButtonClick;

        public UldCargoItem(UldItem uld)
            : base(uld)
        {
            InitializeComponent();
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (ButtonClick != null)
            {

                //ButtonClick(this, EventArgs.Empty);


                UldItem tempData = (UldItem)this.ItemData;

                if (tempData.Location == string.Empty || tempData.Location == "N/A")
                {
                    if (tempData.IsBUP)
                    {
                        string msg = "Are you sure you want to change from BUP to NOT BUP?";
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            bool result = CargoMatrix.Communication.WebServiceManager.Instance().BUPChangeMCH(tempData.FlightManifestId, tempData.UldId);
                            if (result)
                            {
                                tempData.IsBUP = false;
                                this.buttonBrowse.Image = CargoMatrix.Resources.Skin.button_uld;
                                this.buttonBrowse.PressedImage = CargoMatrix.Resources.Skin.button_uld_over;
                            }
                            else
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Unable to change mode.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            }

                        }
                    }
                    else
                    {
                        string msg = "Are you sure you want to change from NOT BUP to BUP?";
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            bool result = CargoMatrix.Communication.WebServiceManager.Instance().BUPChangeMCH(tempData.FlightManifestId, tempData.UldId);
                            if (result)
                            {
                                tempData.IsBUP = true;
                                this.buttonBrowse.Image = CargoMatrix.Resources.Skin.button_uldlocked;
                                this.buttonBrowse.PressedImage = CargoMatrix.Resources.Skin.button_uldlocked_over;
                            }
                            else
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Unable to change mode.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            }
                        }
                    }
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unable to change mode. This ULD was already recovered.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }






            }
        }

        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBrowse.Location = new System.Drawing.Point(202, 6);
            this.buttonBrowse.Name = "btnMore";
            this.buttonBrowse.Image = Resources.Skin._3dots_btn;
            this.buttonBrowse.PressedImage = Resources.Skin._3dots_btn_over;
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);
            this.Controls.Add(this.buttonBrowse);
            this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.ResumeLayout(false);
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Empty;
                    if (ItemData.UldType.ToUpper() == "LOOSE")
                    {
                        line1 = string.Format("{0}", ItemData.UldType);
                    }
                    else
                    {
                        line1 = string.Format("{0}{1}", ItemData.UldType, ItemData.UldSerialNo);
                    }
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {

                    string line2 = string.Format("PCS: {0} of {1}", ItemData.ReceivedPieces, ItemData.TotalPieces);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 16 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {

                    string line3 = string.Format("LOC: {0}", ItemData.Location);
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 30 * vScale, 170 * hScale, 13 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.Clipboard;
                switch (ItemData.Status)
                {
                    case RecoverStatuses.NotCompleted:
                    case RecoverStatuses.Pending:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case RecoverStatuses.InProgress:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case RecoverStatuses.Complete:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }


                if (ItemData.UldType.ToUpper() == "LOOSE")
                {
                    this.buttonBrowse.Visible = false;
                }
                else
                {
                    this.buttonBrowse.Visible = true;
                    if (ItemData.IsBUP)
                    {
                        this.buttonBrowse.Image = CargoMatrix.Resources.Skin.button_uldlocked;
                        this.buttonBrowse.PressedImage = CargoMatrix.Resources.Skin.button_uldlocked_over;
                    }
                    else
                    {
                        this.buttonBrowse.Image = CargoMatrix.Resources.Skin.button_uld;
                        this.buttonBrowse.PressedImage = CargoMatrix.Resources.Skin.button_uld_over;
                    }
                }

               
                 

                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
 

            string content = string.Empty;
            if (ItemData.UldType.ToUpper() == "LOOSE")
            {
                content = string.Format("{0}{1}", ItemData.UldType, ItemData.References);
            }
            else
            {
                content = string.Format("{0}{1}{2}", ItemData.UldType, ItemData.UldSerialNo, ItemData.References);
            }
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
