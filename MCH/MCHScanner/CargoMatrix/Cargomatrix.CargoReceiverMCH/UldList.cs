﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;

namespace CargoMatrix.CargoReceiver
{
    public partial class UldList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

        private UldItem selctedUldItem = null;
        private string selectedUld=string.Empty;
        private FlightItem flightDetailsItem;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;
        //private long manifestId;
        public UldList(FlightItem flightDetailsItem ,string selectedUld)
        {
            this.selectedUld = selectedUld;
            this.flightDetailsItem = flightDetailsItem;
        
             
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_ListItemClicked);
        }

        private void ShowUldAlerts(string reference, long taskId, long uldId)
        {
            CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] alerts = CargoMatrix.Communication.WebServiceManager.Instance().GetUldAlertsMCH(taskId, uldId);
               foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.Alert item in alerts)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        }
        }



        private void SelectUld(UldItem tempUldItem)
        {
            string reference = string.Empty;
 

            int remainingPieces = tempUldItem.TotalPieces - tempUldItem.ReceivedPieces;
            if (remainingPieces == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("You have already recoverd this ULD.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                if (tempUldItem.UldType.ToUpper() == "LOOSE")
                {

                    if (tempUldItem.ReceivedPieces == 0 && (tempUldItem.Location == "N/A" || tempUldItem.Location == ""))
                    {



                        reference = string.Format("{0}{1}{2}", flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber, tempUldItem.UldType.ToUpper());

                        ShowUldAlerts(reference, flightDetailsItem.TaskId, tempUldItem.UldId);



                        string enteredLocation;
                        BarcodeEnabled = false;
                        bool isFPC = false;
                        if (DoLocation(out enteredLocation, out isFPC, reference))
                        {
                            long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                            if (locationId == 0)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                BarcodeEnabled = true;
                                return;
                            }
                            
                            CargoMatrix.Communication.WebServiceManager.Instance().RecoverUldMCH(tempUldItem.UldId, locationId, tempUldItem.FlightManifestId);
                            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
                            if (isFPC)
                            {
                                ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CargoReceiver");
                            }
                        }
                        BarcodeEnabled = true;



                    }
                    else
                    {
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
                        return;
                    }






                }
                else
                {
                    reference = string.Format("{0}{1}{2}{3}", flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber, tempUldItem.UldType.ToUpper(), tempUldItem.UldSerialNo);


                    if (tempUldItem.IsBUP)
                    {

                        if (tempUldItem.ReceivedPieces == 0 && (tempUldItem.Location == "N/A" || tempUldItem.Location == ""))
                        {
                            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to recover this ULD?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                            {

                                ShowUldAlerts(reference, flightDetailsItem.TaskId, tempUldItem.UldId);

                                string enteredLocation;
                                BarcodeEnabled = false;
                                bool isFPC = false;
                                if (DoLocation(out enteredLocation, out isFPC, reference))
                                {
                                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                                    if (locationId == 0)
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                        BarcodeEnabled = true;
                                        return;
                                    }
                                    
                                    CargoMatrix.Communication.WebServiceManager.Instance().RecoverUldMCH(tempUldItem.UldId, locationId, tempUldItem.FlightManifestId);
                                    LoadControl();
                                    if (isFPC)
                                    {
                                        ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CargoReceiver");
                                    }
                                }
                            }
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("This ULD has been already recovered.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }



                       
                        if (!BarcodeEnabled)
                        {
                            BarcodeEnabled = true;
                        }
                        return;
                    }
                    else
                    {

                        ShowUldAlerts(reference, flightDetailsItem.TaskId, tempUldItem.UldId);

                        if (tempUldItem.ReceivedPieces == 0 && (tempUldItem.Location == "N/A" || tempUldItem.Location == ""))
                        {
 
                            string enteredLocation;
                            BarcodeEnabled = false;
                            bool isFPC = false;
                            if (DoLocation(out enteredLocation, out isFPC, reference))
                            {
                                long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                                if (locationId == 0)
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    BarcodeEnabled = true;
                                    return;
                                }
                                
                                CargoMatrix.Communication.WebServiceManager.Instance().RecoverUldMCH(tempUldItem.UldId, locationId, tempUldItem.FlightManifestId);
                                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
                                if (isFPC)
                                {
                                    ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CargoReceiver");
                                }
                            }

                        }
                        else
                        {
                            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
                            return;
                        }








                    }
                }
            }

   
         
        }


        protected virtual void UldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            string reference = string.Empty;
            UldCargoItem tempUldCargoItem = (UldCargoItem)listItem;
            UldItem tempUldItem = (UldItem)tempUldCargoItem.ItemData;
            SelectUld(tempUldItem);
        }
        //protected virtual void UldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        //{
        //    string reference = string.Empty;
        //    UldCargoItem tempUldCargoItem = (UldCargoItem)listItem;
        //    UldItem tempUldItem = (UldItem)tempUldCargoItem.ItemData;

        //    int remainingPieces = tempUldItem.TotalPieces - tempUldItem.ReceivedPieces;
        //    if (remainingPieces == 0)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("You have already recoverd this ULD.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //        return;
        //    }
        //    else
        //    {
        //        if (tempUldItem.UldType.ToUpper() == "LOOSE")
        //        {

        //            if (tempUldItem.ReceivedPieces == 0 && (tempUldItem.Location == "N/A" || tempUldItem.Location == ""))
        //            {



        //                reference = string.Format("{0}{1}{2}", flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber, tempUldItem.UldType.ToUpper());
                        
        //                ShowUldAlerts(reference, flightDetailsItem.TaskId, tempUldItem.UldId);
                        
                        
                   
        //                string enteredLocation;
        //                BarcodeEnabled = false;
        //                bool isFPC = false;
        //                if (DoLocation(out enteredLocation, out isFPC, reference))
        //                {
        //                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
        //                    CargoMatrix.Communication.WebServiceManager.Instance().RecoverUldMCH(tempUldItem.UldId, locationId, tempUldItem.FlightManifestId);
        //                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
        //                    if (isFPC)
        //                    {
        //                        ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CargoReceiver");
        //                    }
        //                }




        //            }
        //            else
        //            {
        //                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
        //                return;
        //            }






        //        }
        //        else
        //        {
        //            reference = string.Format("{0}{1}{2}{3}", flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber, tempUldItem.UldType.ToUpper(), tempUldItem.UldSerialNo);
                    
                    
        //            if (tempUldItem.IsBUP)
        //            {
        //                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to recover this ULD?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
        //                {

        //                    ShowUldAlerts(reference, flightDetailsItem.TaskId, tempUldItem.UldId);

        //                    string enteredLocation;
        //                    BarcodeEnabled = false;
        //                    bool isFPC = false;
        //                    if (DoLocation(out enteredLocation, out isFPC, reference))
        //                    {
        //                        long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
        //                        CargoMatrix.Communication.WebServiceManager.Instance().RecoverUldMCH(tempUldItem.UldId, locationId, tempUldItem.FlightManifestId);
        //                        LoadControl();
        //                        if (isFPC)
        //                        {
        //                            ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CargoReceiver");
        //                        }
        //                    }
        //                }
        //                if (!BarcodeEnabled)
        //                {
        //                    BarcodeEnabled = true;
        //                }
        //                return;
        //            }
        //            else
        //            {

        //                ShowUldAlerts(reference, flightDetailsItem.TaskId, tempUldItem.UldId);

        //                if (tempUldItem.ReceivedPieces == 0 && (tempUldItem.Location == "N/A" || tempUldItem.Location == ""))
        //                {

        //                    //BarcodeEnabled = false;
        //                    //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(tempUldItem.UldId, flightDetailsItem));
        //                    //BarcodeEnabled = true;
        //                    //return;
        //                    string enteredLocation;
        //                    BarcodeEnabled = false;
        //                    bool isFPC = false;
        //                    if (DoLocation(out enteredLocation, out isFPC, reference))
        //                    {
        //                        long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
        //                        CargoMatrix.Communication.WebServiceManager.Instance().RecoverUldMCH(tempUldItem.UldId, locationId, tempUldItem.FlightManifestId);
        //                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
        //                        if (isFPC)
        //                        {
        //                            ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CargoReceiver");
        //                        }
        //                    }

        //                }
        //                else
        //                {
        //                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldReceiver(flightDetailsItem, tempUldItem));
        //                    return;
        //                }








        //            }
        //        }
        //    }


        //   // return;

        //   // ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempUldItem.TotalPieces, "CARGO RECEIVER");

        //   // return;
        //   // //END HERE


        //   // int enteredPieces= 0;
        //   // if (DoPieces(reference, remainingPieces, out enteredPieces))
        //   // {
        //   //     MessageBox.Show(enteredPieces.ToString());
        //   // }
        //   // else
        //   // {
        //   //     return;
        //   // }


        //   // //string enteredLocation;
        //   // //BarcodeEnabled = false;
        //   // //if (DoLocation(out enteredLocation, reference))
        //   // //{
        //   // //    MessageBox.Show(enteredLocation);
        //   // //}
        //   // //BarcodeEnabled = true;





        //   ////ShowFreightPhotoCapture("12345", "abc", "cba", "test");

        //   // //if (CargoMatrix.Communication.Utilities.IsAdmin)
        //   // //{
        //   // //    ULD_Enter_Click(listItem, null);
        //   // //}
        //   // //else
        //   // //{
        //   // //    smoothListBoxMainList.MoveControlToTop(listItem);
        //   // //    smoothListBoxMainList.LayoutItems();
        //   // //}
        //}

        bool DoPieces(string reference, int remaningPieces, out int enteredPieces )
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
             
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination,  int pieces,string title)
        {
             

            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + hawbNo + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination,true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                      //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }
    
        bool DoLocation(out string enterLocation,out bool isFPC, string title)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = true;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    exitflag = true;
                }
             

            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;

            if (ShowFilterPopup() == false)
                BarcodeEnabled = true;
        }

        string GetOverageReason()
        {

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Overage Reasons";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";


            actPopup.RemoveAllItems();

            actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem("SPLIT", null, 1));
            actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem("SHORTAGE", null, 2));
            actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem("SPLIT/SHORTAGE", null, 3));


            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }



        }



        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK:
                        if (this.flightDetailsItem.Status != FlightStatus.Completed)
                        {
                            if (this.flightDetailsItem.ReceivedPieces < this.flightDetailsItem.TotalPieces)
                            {
                                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Not all pieces were scanned. Are you sure you want report a shortage?", "Finalize", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                                {
                                    string overageReason = GetOverageReason();
                                    FinalizeTask();
                                    if (overageReason != string.Empty)
                                    {

                                    }
                                    return;

                                }
                            }
                            else
                            {
                                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to finalize this task?", "Finalize", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                                {
                                    FinalizeTask();
                                }
                            }
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("This flight was already finalized.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Uld);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter;
                LoadControl();
                return true;
            }
            return false;
        }

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter  };
            this.AddOptionsListItem(filterOption);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
 
            //this.label1.Text = string.Format("{0}", filter);
 

            this.label1.Text =string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            this.label2.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.RecoveredULDs, this.flightDetailsItem.ULDCount);
            this.label3.Text = string.Format("PCS: {0} of {1}", this.flightDetailsItem.ReceivedPieces, this.flightDetailsItem.TotalPieces);

            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;
  

                    RecoverStatuses status = RecoverStatuses.All;
                     switch (filter)
                     {
                         case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                             status = RecoverStatuses.Pending;
                             break;
                         case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                             status = RecoverStatuses.InProgress;
                             break;
                         case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                             status = RecoverStatuses.Complete;
                             break;
                         case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                             status = RecoverStatuses.NotCompleted;
                             break;
                         default:
                             status = RecoverStatuses.All;
                             break;
                     }


                     CargoMatrix.Communication.DTO.UldItem[] uldItemsArray = null;
                     if (CargoMatrix.Communication.WebServiceManager.Instance().GetFlightULDsMCH(out uldItemsArray, status, this.flightDetailsItem.FlightManifestId))
                     {
                         var rItems = from i in uldItemsArray
                                      select InitializeItem(i);
                         smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                     }





            this.TitleText = string.Format("CargoReceiver - {0} ({1})",filter, uldItemsArray.Length);
         
            Cursor.Current = Cursors.Default;



            if (this.flightDetailsItem.Status != FlightStatus.Completed)
            {
                if (this.flightDetailsItem.ReceivedPieces >= this.flightDetailsItem.TotalPieces)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("All pieces were scanned. Do you want to finalize this task?", "Finalize", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        FinalizeTask();
                        return;
                    }
                }
            }

            if (this.selectedUld != string.Empty)
            {
                if (this.selctedUldItem != null)
                {
                    SelectUld(this.selctedUldItem);
                    //this.selectedUld = string.Empty;
                }
                this.selectedUld = string.Empty;
            }
           

        }


        void FinalizeTask()
        {
            if (!CargoMatrix.Communication.WebServiceManager.Instance().FinalizeReceiverMCH(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId))
            {
                if (!BarcodeEnabled)
                {
                    BarcodeEnabled = true;
                }
                CargoMatrix.UI.CMXMessageBox.Show("Unable to finalize this task.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            //this.TitleText = string.Format("CargoReceiver - {0}",  smoothListBoxMainList.VisibleItemsCount);
            this.TitleText = string.Format("CargoReceiver - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
        }

        protected ICustomRenderItem InitializeItem(UldItem item)
        {
        
            string reference = string.Empty;
            if(item.UldType.ToUpper()=="LOOSE")
            { 
                reference=item.UldType;
            }
            else
            {
                reference=item.UldType + item.UldSerialNo;
            }

            if (this.selectedUld == reference)
            {
                selctedUldItem = item;
            }



            UldCargoItem tempUld = new UldCargoItem(item);
            tempUld.OnEnterClick += new EventHandler(ULD_Enter_Click);
            tempUld.ButtonClick += new EventHandler(ULD_OnMoreClick);
            listItemHeight = tempUld.Height;
            return tempUld;
        }

        void ULD_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            var uld = (sender as UldCargoItem).ItemData;
            Cursor.Current = Cursors.Default;
            MessageBox.Show((sender as UldCargoItem).ItemData.UldSerialNo);
        }

        //private void MAWB_PrintLabels(IMasterBillItem item)
        //{
        //    BarcodeEnabled = false;
        //    CustomUtilities.HawbPiecesLabelPrinter.Show(item.MasterBillNumber, item.CarrierNumber);
        //    BarcodeEnabled = true;
        //    Cursor.Current = Cursors.Default;
        //}

  
        protected virtual void ULD_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            FlightItem flight = (sender as FlightCargoItem).ItemData;
            ProceedWithUld(flight);
            Cursor.Current = Cursors.Default;
        }

        private bool ProceedWithUld(FlightItem tempFlight)
        {
            if (tempFlight == null)
                return false;

          

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
            Cursor.Current = Cursors.Default;
            return true;
        }

        void MawbList_BarcodeReadNotify(string barcodeData)
        {

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Uld)
            {
               
 
                 for (int i = 0; i < smoothListBoxMainList.Items.Count; i++)
            {

                UldCargoItem ctr = (UldCargoItem)smoothListBoxMainList.Items[i];
                UldItem itm = (UldItem)ctr.ItemData;
                if (itm.UldType + itm.UldSerialNo == scanItem.UldNumber)
                {
                    SelectUld(itm);
                    return;
                }
            }


            }
          
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }
    }

}
