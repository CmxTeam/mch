﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.GridListBox
{
    

    public partial class GridListItem : SmoothListBox.UI.ListItems.ListItem// UserControl
    {
        public delegate bool ValidateQuantity(GridListItem selectedItem, int prevQuantity, int newQuantity);
        public delegate void PeicesLoaded(int peices);
        public PeicesLoaded Load_Pieces; 
        public CargoMatrix.Communication.WSScanner.ScanTypes ScanType;
        public EventHandler PhotoCapture_Click;
        public EventHandler Delete_Click;
        public ValidateQuantity Validate_Quantity;
        public EventHandler QuantityChanged;
        //public EventHandler Quantity_Changed;

        private bool editQuantityButtonDown = false;
        private bool editPiecesPressed = false;
        private CargoMatrix.Communication.WSScanner.Modes m_mode;
        int rowRecID = -1;
        public CargoMatrix.UI.CMXTextBox countTextBox;
        public GridListItem(CargoMatrix.Communication.WSScanner.Modes mode, int pieceNo, bool EditMode)
        {
            InitializeComponent();
            m_mode = mode;
            //labelPieceNo.Text = Convert.ToString(pieceNo);
            Init();
            editPiecesPressed = EditMode;
            //if (EditMode)
            //{
            //    if (countTextBox == null)
            //    {
            //        countTextBox = new CargoMatrix.UI.CMXTextBox();

            //        countTextBox.KeyDown += new KeyEventHandler(countTextBox_KeyDown);
            //        countTextBox.Location = new Point(1, 1);
            //        countTextBox.Width = panelPiece.Width - 2;
            //        countTextBox.Height = panelPiece.Height - 2;
            //        panelPiece.Controls.Add(countTextBox);
            //        countTextBox.BringToFront();
            //    }
            //    countTextBox.Text = Convert.ToString(pieceNo);
            //    pictureBox1.Visible = false;
            //    countTextBox.Visible = true;
            //    countTextBox.SelectAll();
            //    countTextBox.Focus();
            
            //}
        
        }
        public GridListItem(CargoMatrix.Communication.WSScanner.Modes mode, int pieceNo)
        {
            InitializeComponent();
            m_mode = mode;
            labelPieceNo.Text = Convert.ToString(pieceNo);
            Init();
           
        }
        private void Init()
        {
            Height = Height - panel2.Height;                
            panel2.Height = 0;

            buttonDelete.Image = GridListBoxResources.pcs_loc_del;
            buttonDelete.PressedImage = GridListBoxResources.pcs_loc_del_over;

            buttonPhotoCapture.Image = GridListBoxResources.conditions_btn;
            buttonPhotoCapture.PressedImage = GridListBoxResources.conditions_btn_over;

            if (m_mode == CargoMatrix.Communication.WSScanner.Modes.Count) // only allow edit piece in count mode
            {
                (labelPieceNo as Control).MouseDown += new MouseEventHandler(EditQuantity_MouseDown);
                panelPiece.MouseDown += new MouseEventHandler(EditQuantity_MouseDown);

                (labelPieceNo as Control).MouseMove += new MouseEventHandler(EditQuantity_MouseMove);
                panelPiece.MouseMove += new MouseEventHandler(EditQuantity_MouseMove);

                (labelPieceNo as Control).MouseUp += new MouseEventHandler(EditQuantity_MouseUp);
                panelPiece.MouseUp += new MouseEventHandler(EditQuantity_MouseUp);

                (labelPieceNo as Control).Click += new EventHandler(EditQuantity_MouseClick);
                panelPiece.Click += new EventHandler(EditQuantity_MouseClick);
            }

            panelBaseLine.SendToBack();
            pictureBox1.Visible = false;

            cmxPictureButtonOk.Image = GridListBoxResources.nav_ok;
            cmxPictureButtonOk.PressedImage = GridListBoxResources.nav_ok_over;

            cmxPictureButtonCancel.Image = GridListBoxResources.nav_cancel;
            cmxPictureButtonCancel.PressedImage = GridListBoxResources.nav_cancel_over;

            cmxPictureButtonAdd.Image = GridListBoxResources.nav_down;
            cmxPictureButtonAdd.PressedImage = GridListBoxResources.nav_down_over;

            cmxPictureButtonSubtract.Image = GridListBoxResources.nav_up;
            cmxPictureButtonSubtract.PressedImage = GridListBoxResources.nav_up_over;
        
        }
        void EditQuantity_MouseClick(object sender, EventArgs e)
        {
            editPiecesPressed = true;

        }
        

        void EditQuantity_MouseMove(object sender, MouseEventArgs e)
        {
            if (editQuantityButtonDown)
            {
                if ((e.X < panelPiece.Left || e.X > panelPiece.Right) || (e.Y < panelPiece.Top || e.Y > panelPiece.Bottom))
                {
                    editQuantityButtonDown = false;
                
                }
            }
        }
              
        void EditQuantity_MouseDown(object sender, MouseEventArgs e)
        {
            editQuantityButtonDown = true;
        }
        void EditQuantity_MouseUp(object sender, MouseEventArgs e)
        {
            

            //if (editQuantityButtonDown == true)
            //{
            //    editQuantityButtonDown = false;

                
              
                

            //    if (EditQuantity_Click != null)
            //        EditQuantity_Click(this, e);

            
            //}
            
            
        }

        void countTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Tab:
                case Keys.Enter:
                    if (Validate_Quantity != null)
                    {
                        if (Validate_Quantity(this, Convert.ToInt32(labelPieceNo.Text), Convert.ToInt32(countTextBox.Text)))
                        {
                            labelPieceNo.Text = countTextBox.Text;
                            countTextBox.Visible = false;
                            if (QuantityChanged != null)
                                QuantityChanged(this, EventArgs.Empty);
                        
                        }
                    }
                    
                    e.Handled = true;
                    
                    break;
            }

            if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                e.Handled = true;
        }

       


        public override void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;

            if (isSelected)
            {
                BackColor = focusedColor;
                panel2.Height = Height;
                Height = Height + panel2.Height;
                labelAdded.Text = labelPieceNo.Text;

            }
            else
            {
                BackColor = backColor;
                Height = Height - panel2.Height;
                panel2.Height = 0;
            }

            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }

            if (isSelected)
                pictureBox1.Visible = true;
            else
                pictureBox1.Visible = false;
           
            //Focus(isSelected);
            if (editPiecesPressed == true)
            {
                editPiecesPressed = false;

                if (countTextBox == null)
                {
                    countTextBox = new CargoMatrix.UI.CMXTextBox();

                    countTextBox.KeyDown += new KeyEventHandler(countTextBox_KeyDown);
                    countTextBox.Location = new Point(1, 1);
                    countTextBox.Width = panelPiece.Width - 2;
                    countTextBox.Height = panelPiece.Height - 2;
                    panelPiece.Controls.Add(countTextBox);
                    countTextBox.BringToFront();
                }
                countTextBox.Text = labelPieceNo.Text;
                pictureBox1.Visible = false;
                countTextBox.Visible = true;
                countTextBox.SelectAll();
                countTextBox.Focus();

            }
            else
            {
                if (countTextBox != null)
                {
                    if (countTextBox.Visible)
                    {
                        if (Validate_Quantity != null)
                        {
                            if (Validate_Quantity(this, Convert.ToInt32(labelPieceNo.Text), Convert.ToInt32(countTextBox.Text)))
                            {
                                labelPieceNo.Text = countTextBox.Text;
                                countTextBox.Visible = false;
                                if (QuantityChanged != null)
                                    QuantityChanged(this, EventArgs.Empty);

                            }
                        }
                        
                        
                        
                    }
                }

            }
        }
        public override void Focus(bool focused)
        {
            if (focused || m_selected)
                BackColor = focusedColor;
            else 
                BackColor = backColor;

            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }
            //this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;

        }

        private void buttonPhotoCapture_Click(object sender, EventArgs e)
        {
            //if (PhotoCapture_Click != null)
            //    PhotoCapture_Click(this, e);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (Delete_Click != null)
            {
                string pcs = this.labelPieceNo.Text;
                string loc = this.labelLocation.Text;
                if(CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to delete Piece(s): " + pcs + " from Location: " +loc, "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
                    Delete_Click(this, e);
                
            }
        }

        public int PieceCount
        {
            get
            {

                if (countTextBox != null && countTextBox.Visible)
                    return Convert.ToInt32(countTextBox.Text);
                else
                    return Convert.ToInt32(labelPieceNo.Text);
            }
        }
        public string TextLocation
        {
            set
            {
                labelLocation.Text = value;

                if (countTextBox != null)
                {
                    if (countTextBox.Visible)
                    {
                        if (Validate_Quantity != null)
                        {
                            if (Validate_Quantity(this, Convert.ToInt32(labelPieceNo.Text), Convert.ToInt32(countTextBox.Text)))
                            {
                                labelPieceNo.Text = countTextBox.Text;
                                countTextBox.Visible = false;
                                if (QuantityChanged != null)
                                    QuantityChanged(this, EventArgs.Empty);

                            }
                        }



                    }
                }

            }
            get
            {
                return labelLocation.Text;
            }
        }
        public int LocationID
        {
            get;
            set;
        }
        public int RowRecID
        {
            get
            {
                return rowRecID;
            }
            set
            {
                rowRecID = value;
            }
        }

        private void cmxPictureButtonAdd_Click(object sender, EventArgs e)
        {
            int addedCount = Convert.ToInt32(labelAdded.Text);

            if (addedCount < PieceCount)
                addedCount++;
            labelAdded.Text = Convert.ToString(addedCount);

        }

        private void cmxPictureButtonSubtract_Click(object sender, EventArgs e)
        {
            int addedCount = Convert.ToInt32(labelAdded.Text);

            if (addedCount > 0)
                addedCount--;
            labelAdded.Text = Convert.ToString(addedCount);

        }

        private void cmxPictureButtonOk_Click(object sender, EventArgs e)
        {
            int added = Convert.ToInt32(labelAdded.Text);


            if (Load_Pieces != null)
            {
                Load_Pieces(added);
                int newAmount = PieceCount - added;

                labelPieceNo.Text = Convert.ToString(newAmount);
                labelAdded.Text = "0";

                cmxPictureButtonCancel_Click(sender, e);

            }
 

        }

        private void cmxPictureButtonCancel_Click(object sender, EventArgs e)
        {
            for (Control control = this; control != null; control = control.Parent)
            {
                if (control is SmoothListBox.UI.SmoothListBoxBase)
                {
                    (control as SmoothListBox.UI.SmoothListBoxBase).Reset();
                    break;
                    
                }
            }
            

        }
    }
}
