﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.GridListBox
{
    public interface IGridListItem
    {
        
        int HousebillRecID
        {
            set;
            get;

        }

        int ActionID
        {
            set;
            get;
        }

        int Moving
        {
            set;
            //get;
        }
        int PieceCount
        {
            set;
            get;
        }
        string TextLocation
        {
            set;
            get;
        }
        Nullable<int> RowRecID
        {
            get;
            set;

        }
        int LocationID
        {
            get;
            set;
        }
        CargoMatrix.Communication.WSPieceScan.StorageTypes LocationType
        {
            get;
            set;
        }

        CargoMatrix.Communication.WSPieceScan.ScanTypes ScanType
        {
            get;
            set;
        }
    }
}
