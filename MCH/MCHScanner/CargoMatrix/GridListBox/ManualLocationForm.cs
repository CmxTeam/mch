﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.GridListBox
{
    public partial class ManualLocationForm : Form
    {
        bool bFirstTimeLoad = false;
       
        public string HeaderText;
        //SmoothListbox.ListItems.ComboBox m_typeComboBox;
        //SmoothListbox.ListItems.ComboBox m_directionComboBox;
        CargoMatrix.Communication.WSPieceScan.GatewayLocation _location = null;
        //CargoMatrix.Communication.WSPieceScan.Types _locationType;
        public ManualLocationForm()
        {
            InitializeComponent();

            //smoothListBoxBase1.AutoScroll += new SmoothListbox.AutoScrollHandler(smoothListBoxReasons_AutoScroll);
            //panel1.BackColor = FreightHandlerResources.thumbnailBackColor.GetPixel(0, 0);
            pictureBoxMenuBack.Image = GridListBoxResources.popup_Main_nav;
            pictureBoxHeader.Image = GridListBoxResources.popup_header;
            pictureBoxOk.Image = GridListBoxResources.nav_ok;
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel;
            //pictureBoxUp.Image = FreightHandlerResources.nav_up;
            //pictureBoxDown.Image = FreightHandlerResources.nav_down;

            //List<SmoothListbox.ListItems.ComboBoxItemData> tempList = new List<SmoothListbox.ListItems.ComboBoxItemData>();

            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Warehouse area", FreightHandlerResources.Warehouse));
            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Door", FreightHandlerResources.Door));
            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Truck", FreightHandlerResources.Truck));
            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Other", FreightHandlerResources.Symbol_New));
            //m_typeComboBox = new SmoothListbox.ListItems.ComboBox(null, FreightHandlerResources.Warehouse, tempList);
            ////smoothListBoxBase1.AddItem(m_typeComboBox);

            //List<SmoothListbox.ListItems.ComboBoxItemData> tempList2 = new List<SmoothListbox.ListItems.ComboBoxItemData>();
            //tempList2.Add(new SmoothListbox.ListItems.ComboBoxItemData("Import", FreightHandlerResources.Import24));
            //tempList2.Add(new SmoothListbox.ListItems.ComboBoxItemData("Export", FreightHandlerResources.Export24));
            //m_directionComboBox = new SmoothListbox.ListItems.ComboBox(null, FreightHandlerResources.Shipping_Routes, tempList2);
            //smoothListBoxBase1.AddItem(m_directionComboBox);

            //m_typeComboBox.SelectedID = 0;
            //m_directionComboBox.SelectedID = 0;
           
            //AirlineNumber = airlneno;
        }

        //void smoothListBoxReasons_AutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        //{
        //    switch (direction)
        //    {
        //        case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
        //            pictureBoxUp.Enabled = enable;
        //            break;
        //        case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
        //            pictureBoxDown.Enabled = enable;
        //            break;
        //    }
        //    //throw new NotImplementedException();
        //}
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {



                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));
                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }


            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (bFirstTimeLoad == false)
            {
                bFirstTimeLoad = true;
                HeaderText = "Manual Location for selected items" ;
                
                
            }
        }
       

        private void MicroPhotoCaptureForm_Load(object sender, EventArgs e)
        {
            textLocationCode.Focus();
            textLocationCode.SelectAll();

        }

     
        Font HeaderFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Bold);
        SolidBrush HeaderBrush = new SolidBrush(Color.White);
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawString(HeaderText, HeaderFont, HeaderBrush,3, 4);
        }

        private void pictureBoxOk_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = GridListBoxResources.nav_ok_over;
        }

        private void pictureBoxOk_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = GridListBoxResources.nav_ok;
        }

        private void pictureBoxOk_Click(object sender, EventArgs e)
        {
            pictureBoxOk.Image = GridListBoxResources.nav_ok_over;
            pictureBoxOk.Refresh();

            if (VerifyLocation())
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
            

        }

        private void pictureBoxCancel_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel_over;

        }

        private void pictureBoxCancel_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        {
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel_over;
            pictureBoxCancel.Refresh();

            DialogResult = DialogResult.Cancel;
           
        }

        //private void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        //{
        //    pictureBoxUp.Image = FreightHandlerResources.nav_up_over;
        //    smoothListBoxBase1.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);

        //}

        //private void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        //{
        //    pictureBoxUp.Image = FreightHandlerResources.nav_up;
        //    smoothListBoxBase1.UpButtonPressed = false;
        //}

        //private void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        //{
        //    pictureBoxDown.Image = FreightHandlerResources.nav_down_over;
        //    smoothListBoxBase1.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

        //}

        //private void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        //{
        //    pictureBoxDown.Image = FreightHandlerResources.nav_down;
        //    smoothListBoxBase1.DownButtonPressed = false;
        //}
               
        //private void pictureBoxUp_EnabledChanged(object sender, EventArgs e)
        //{
        //    if (pictureBoxUp.Enabled)
        //        pictureBoxUp.Image = FreightHandlerResources.nav_up;
        //    else
        //    {
        //        pictureBoxUp.Image = FreightHandlerResources.nav_up_dis;
        //        smoothListBoxBase1.UpButtonPressed = false;
        //    }

        //}

        //private void pictureBoxDown_EnabledChanged(object sender, EventArgs e)
        //{
        //    if (pictureBoxDown.Enabled)
        //        pictureBoxDown.Image = FreightHandlerResources.nav_down;
        //    else
        //    {
        //        pictureBoxDown.Image = FreightHandlerResources.nav_down_dis;
        //        smoothListBoxBase1.DownButtonPressed = false;
        //    }

        //}

        public CargoMatrix.Communication.WSPieceScan.StorageTypes LocationType
        {
            //set
            //{
            //    m_typeComboBox.SelectedID = value;
                
            //}
            get
            {
                return _location.Type;// m_typeComboBox.SelectedID;
            }
        }
        public int Direction
        {
            //set
            //{
            //    m_directionComboBox.SelectedID = value;
            //}
            get
            {
                return 0;// m_directionComboBox.SelectedID;
            }
        }
        public int LocationID
        {
            get
            {
                if (_location != null)
                    return _location.RecID;
                else
                    return -1;
            
            }
            
        }
       
        public string TextLocation
        {
            get
            {
                if (_location != null)
                    return _location.Lane;
                else
                    return string.Empty;
            }
            //set
            //{
            //    textLocationCode.Text = value;
            //    textLocationCode.Focus();
            //    textLocationCode.SelectAll();
            //}
        }
        //public string LocationTypeName
        //{
        //    get
        //    {
        //        return m_typeComboBox.SelectedItemName;
        //    }
        //}

        private void textLocationCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = GridListBoxResources.nav_ok_over;
                pictureBoxOk.Refresh();
                VerifyLocation();
                DialogResult = DialogResult.OK;
                pictureBoxOk.Image = GridListBoxResources.nav_ok;

            }
        }

        private bool VerifyLocation()
        {
            _location = CargoMatrix.Communication.HostPlusIncomming.Instance.GatewayLocationReadByLane(textLocationCode.Text);

            bool result = false;

            if (_location !=null && _location.RecID <= 0)
            {
                if (CargoMatrix.UI.CMXMessageBox.Show("Location: " + textLocationCode.Text + " does not exist in the system. Would you like to add this location?", "Add Location", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
                {
                    NewLocationForm newLocation = new NewLocationForm();
                    newLocation.TextLocation = textLocationCode.Text;

                    if (newLocation.ShowDialog() == DialogResult.OK)
                    {
                        _location = newLocation.GatewayLocation;
                    
                    }
                    newLocation.Dispose();
                    
                    //m_locationID = CargoMatrix.Communication.HostPlusIncomming.Instance.AddNewLocation(TextLocation, LocationTypeName);


                }
            }
            if (_location != null && _location.RecID > 0)
            {
                result = true;
            }
            return result;
        }

        private void textLocationCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = GridListBoxResources.nav_ok;                

            }
        }
        public bool EnableManualLocation
        {
            
            set 
            {
                textLocationCode.Enabled = value;
            }
        }
       
    }
}