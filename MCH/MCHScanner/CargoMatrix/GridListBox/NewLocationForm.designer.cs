﻿namespace CargoMatrix.GridListBox
{
    partial class NewLocationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.textLocationCode = new CargoMatrix.UI.CMXTextBox();
            this.pictureBoxCancel = new System.Windows.Forms.PictureBox();
            this.pictureBoxOk = new System.Windows.Forms.PictureBox();
            this.pictureBoxMenuBack = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.pictureBoxDown = new System.Windows.Forms.PictureBox();
            this.pictureBoxUp = new System.Windows.Forms.PictureBox();
            this.smoothListBoxBase1 = new SmoothListbox.SmoothListBoxBase();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.smoothListBoxBase1);
            this.panel1.Controls.Add(this.pictureBoxDown);
            this.panel1.Controls.Add(this.pictureBoxUp);
            this.panel1.Controls.Add(this.pictureBoxCancel);
            this.panel1.Controls.Add(this.pictureBoxOk);
            this.panel1.Controls.Add(this.pictureBoxMenuBack);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Location = new System.Drawing.Point(10, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(219, 266);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textLocationCode);
            this.panel2.Location = new System.Drawing.Point(3, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(213, 49);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(1, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 15);
            this.label1.Text = "Enter location code:";
            // 
            // textLocationCode
            // 
            this.textLocationCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textLocationCode.Location = new System.Drawing.Point(2, 19);
            this.textLocationCode.Name = "textLocationCode";
            this.textLocationCode.Size = new System.Drawing.Size(209, 28);
            this.textLocationCode.TabIndex = 13;
            this.textLocationCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textLocationCode_KeyDown);
            this.textLocationCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textLocationCode_KeyUp);
            // 
            // pictureBoxCancel
            // 
            this.pictureBoxCancel.Location = new System.Drawing.Point(57, 226);
            this.pictureBoxCancel.Name = "pictureBoxCancel";
            this.pictureBoxCancel.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCancel.Click += new System.EventHandler(this.pictureBoxCancel_Click);
            this.pictureBoxCancel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCancel_MouseDown);
            this.pictureBoxCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCancel_MouseUp);
            // 
            // pictureBoxOk
            // 
            this.pictureBoxOk.Location = new System.Drawing.Point(3, 226);
            this.pictureBoxOk.Name = "pictureBoxOk";
            this.pictureBoxOk.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOk.Click += new System.EventHandler(this.pictureBoxOk_Click);
            this.pictureBoxOk.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxOk_MouseDown);
            this.pictureBoxOk.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxOk_MouseUp);
            // 
            // pictureBoxMenuBack
            // 
            this.pictureBoxMenuBack.Location = new System.Drawing.Point(0, 224);
            this.pictureBoxMenuBack.Name = "pictureBoxMenuBack";
            this.pictureBoxMenuBack.Size = new System.Drawing.Size(219, 42);
            this.pictureBoxMenuBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(219, 22);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // pictureBoxDown
            // 
            this.pictureBoxDown.Location = new System.Drawing.Point(165, 226);
            this.pictureBoxDown.Name = "pictureBoxDown";
            this.pictureBoxDown.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDown_MouseDown);
            this.pictureBoxDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDown_MouseUp);
            this.pictureBoxDown.EnabledChanged += new System.EventHandler(this.pictureBoxDown_EnabledChanged);
            // 
            // pictureBoxUp
            // 
            this.pictureBoxUp.Location = new System.Drawing.Point(111, 226);
            this.pictureBoxUp.Name = "pictureBoxUp";
            this.pictureBoxUp.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxUp_MouseDown);
            this.pictureBoxUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxUp_MouseUp);
            this.pictureBoxUp.EnabledChanged += new System.EventHandler(this.pictureBoxUp_EnabledChanged);
            // 
            // smoothListBoxBase1
            // 
            this.smoothListBoxBase1.BackColor = System.Drawing.Color.White;
            this.smoothListBoxBase1.Location = new System.Drawing.Point(3, 72);
            this.smoothListBoxBase1.Name = "smoothListBoxBase1";
            this.smoothListBoxBase1.Size = new System.Drawing.Size(213, 150);
            this.smoothListBoxBase1.TabIndex = 6;
            // 
            // ManualLocationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "ManualLocationForm";
            this.Text = "MicroPhotoCaptureForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MicroPhotoCaptureForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxMenuBack;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.PictureBox pictureBoxCancel;
        private System.Windows.Forms.PictureBox pictureBoxOk;
        private System.Windows.Forms.Label label1;
        private CargoMatrix.UI.CMXTextBox textLocationCode;
        private System.Windows.Forms.Panel panel2;
        private SmoothListbox.SmoothListBoxBase smoothListBoxBase1;
        private System.Windows.Forms.PictureBox pictureBoxDown;
        private System.Windows.Forms.PictureBox pictureBoxUp;
    }
}