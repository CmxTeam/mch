﻿namespace CargoMatrix.GridListBox
{
    partial class GridListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridListItem));
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.buttonPhotoCapture = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDelete = new CargoMatrix.UI.CMXPictureButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelLocation = new System.Windows.Forms.Label();
            this.panelPiece = new System.Windows.Forms.Panel();
            this.pictureBox1 = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelPieceNo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmxPictureButtonOk = new CargoMatrix.UI.CMXPictureButton();
            this.cmxPictureButtonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.cmxPictureButtonSubtract = new CargoMatrix.UI.CMXPictureButton();
            this.cmxPictureButtonAdd = new CargoMatrix.UI.CMXPictureButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelAdded = new System.Windows.Forms.Label();
            //((System.ComponentModel.ISupportInitialize)(this.buttonPhotoCapture)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelPiece.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonOk)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonCancel)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonSubtract)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonAdd)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.Color.Silver;
            this.splitter4.Location = new System.Drawing.Point(188, 0);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(1, 32);
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Silver;
            this.splitter3.Location = new System.Drawing.Point(139, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1, 32);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Silver;
            this.splitter2.Location = new System.Drawing.Point(40, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1, 32);
            // 
            // buttonPhotoCapture
            // 
            this.buttonPhotoCapture.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPhotoCapture.Location = new System.Drawing.Point(140, 0);
            this.buttonPhotoCapture.Name = "buttonPhotoCapture";
            this.buttonPhotoCapture.Size = new System.Drawing.Size(48, 32);
            this.buttonPhotoCapture.Click += new System.EventHandler(this.buttonPhotoCapture_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDelete.Location = new System.Drawing.Point(189, 0);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(48, 32);
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelLocation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(41, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(98, 32);
            // 
            // labelLocation
            // 
            this.labelLocation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLocation.Location = new System.Drawing.Point(0, 10);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(97, 12);
            // 
            // panelPiece
            // 
            this.panelPiece.Controls.Add(this.pictureBox1);
            this.panelPiece.Controls.Add(this.labelPieceNo);
            this.panelPiece.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPiece.Location = new System.Drawing.Point(0, 0);
            this.panelPiece.Name = "panelPiece";
            this.panelPiece.Size = new System.Drawing.Size(40, 32);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(8, 8);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TransparentColor = System.Drawing.Color.White;
            // 
            // labelPieceNo
            // 
            this.labelPieceNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPieceNo.Location = new System.Drawing.Point(10, 10);
            this.labelPieceNo.Name = "labelPieceNo";
            this.labelPieceNo.Size = new System.Drawing.Size(28, 12);
            this.labelPieceNo.Text = "0";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Lime;
            this.panel2.Controls.Add(this.cmxPictureButtonOk);
            this.panel2.Controls.Add(this.cmxPictureButtonCancel);
            this.panel2.Controls.Add(this.cmxPictureButtonSubtract);
            this.panel2.Controls.Add(this.cmxPictureButtonAdd);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 32);
            // 
            // cmxPictureButtonOk
            // 
            this.cmxPictureButtonOk.Location = new System.Drawing.Point(139, 0);
            this.cmxPictureButtonOk.Name = "cmxPictureButtonOk";
            this.cmxPictureButtonOk.Size = new System.Drawing.Size(48, 32);
            this.cmxPictureButtonOk.Click += new System.EventHandler(this.cmxPictureButtonOk_Click);
            // 
            // cmxPictureButtonCancel
            // 
            this.cmxPictureButtonCancel.Location = new System.Drawing.Point(189, 0);
            this.cmxPictureButtonCancel.Name = "cmxPictureButtonCancel";
            this.cmxPictureButtonCancel.Size = new System.Drawing.Size(48, 32);
            this.cmxPictureButtonCancel.Click += new System.EventHandler(this.cmxPictureButtonCancel_Click);
            // 
            // cmxPictureButtonSubtract
            // 
            this.cmxPictureButtonSubtract.Location = new System.Drawing.Point(79, 0);
            this.cmxPictureButtonSubtract.Name = "cmxPictureButtonSubtract";
            this.cmxPictureButtonSubtract.Size = new System.Drawing.Size(32, 32);
            this.cmxPictureButtonSubtract.Click += new System.EventHandler(this.cmxPictureButtonSubtract_Click);
            // 
            // cmxPictureButtonAdd
            // 
            this.cmxPictureButtonAdd.Location = new System.Drawing.Point(41, 0);
            this.cmxPictureButtonAdd.Name = "cmxPictureButtonAdd";
            this.cmxPictureButtonAdd.Size = new System.Drawing.Size(32, 32);
            this.cmxPictureButtonAdd.Click += new System.EventHandler(this.cmxPictureButtonAdd_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Silver;
            this.splitter1.Location = new System.Drawing.Point(40, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1, 32);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Lime;
            this.panel3.Controls.Add(this.labelAdded);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(40, 32);
            // 
            // labelAdded
            // 
            this.labelAdded.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelAdded.Location = new System.Drawing.Point(10, 10);
            this.labelAdded.Name = "labelAdded";
            this.labelAdded.Size = new System.Drawing.Size(28, 12);
            this.labelAdded.Text = "0";
            // 
            // GridListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.splitter4);
            this.Controls.Add(this.buttonPhotoCapture);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelPiece);
            this.Controls.Add(this.panel2);
            this.Name = "GridListItem";
            this.Size = new System.Drawing.Size(238, 64);
            //((System.ComponentModel.ISupportInitialize)(this.buttonPhotoCapture)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelPiece.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonOk)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonCancel)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonSubtract)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmxPictureButtonAdd)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private CargoMatrix.UI.CMXPictureButton buttonPhotoCapture;
        private CargoMatrix.UI.CMXPictureButton buttonDelete;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelPiece;
        public System.Windows.Forms.Label labelPieceNo;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBox1;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Panel panel2;
        private CargoMatrix.UI.CMXPictureButton cmxPictureButtonOk;
        private CargoMatrix.UI.CMXPictureButton cmxPictureButtonCancel;
        private CargoMatrix.UI.CMXPictureButton cmxPictureButtonSubtract;
        private CargoMatrix.UI.CMXPictureButton cmxPictureButtonAdd;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label labelAdded;
    }
}
