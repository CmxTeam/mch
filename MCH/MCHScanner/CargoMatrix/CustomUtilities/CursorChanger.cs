﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public class CursorChanger : IDisposable
    {
        public Cursor StartCursor { get; set; }
        Cursor EndCursor { get; set; }

        public static CursorChanger WaitChanger
        {
            get
            {
                return new CursorChanger(Cursors.WaitCursor, Cursors.Default);
            }
        }

        public static void ShowCursor(bool show)
        {
            if(show == true)
            {
                Cursor.Show();
            }
            else
            {
                Cursor.Hide();
            }
        }

        public CursorChanger(Cursor startCursor, Cursor endCursor)
        {
            this.StartCursor = startCursor;
            this.EndCursor = endCursor;

            Cursor.Current = this.StartCursor;
        }

        #region IDisposable Members

        public void Dispose()
        {
            Cursor.Current = this.EndCursor;
        }

        #endregion
    }
}
