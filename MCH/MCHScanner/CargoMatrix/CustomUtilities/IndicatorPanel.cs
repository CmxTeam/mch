﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class IndicatorPanel : UserControl
    {
        public IndicatorPanel()
        {
            InitializeComponent();
        }
        private int flags;
        public string PopupTitle = "Legend";
        public string PopupHeader = string.Empty;
        public int Flags
        {
            set
            {
                this.flags = value;
                this.SuspendLayout();
                this.Controls.Clear();
                foreach (var ind in GetIndicators(flags))
                {
                    AddIndicator(ind);
                }
                this.ResumeLayout();
                
            }
        }
        private void AddIndicator(Image img)
        {

            OpenNETCF.Windows.Forms.PictureBox2 box = new OpenNETCF.Windows.Forms.PictureBox2();
            box.Image = img;
            box.Width = 20;
            box.Height =16;
            //int x = this.Width - index *(box.Width -  gap);
            //box.Location = new Point(x,0);
            box.SizeMode = PictureBoxSizeMode.StretchImage;
            box.TransparentColor = Color.White;
            box.Dock = DockStyle.Right;
            box.Click += new EventHandler(box_Click);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.Controls.Add(box);
        }

        void box_Click(object sender, EventArgs e)
        {
            CargoMatrix.Utilities.MessageListBox legendPopup = new CargoMatrix.Utilities.MessageListBox();
            legendPopup.HeaderText = PopupTitle;
            legendPopup.IsSelectable = false;
            legendPopup.HeaderText2 = PopupHeader;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CargoMatrix.Resources.Indicators));
            int power = 0;
            int testflag = 1;
            while (testflag <= flags)
            {
                if ((flags & testflag) == testflag)
                {
                    Image img = resources.GetObject("Indicator_big_" + testflag) as Image;
                    string descr = resources.GetString("Text_Indicator_" + testflag);
                    legendPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(descr, img as Image));

                }
                power++;
                testflag = (int)Math.Pow(2, power);
            }
            legendPopup.ShowDialog();
        }

        private List<Image> GetIndicators(int flags)
        {
            List<Image> retObj = new List<Image>();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CargoMatrix.Resources.Indicators));

            int power = 0;
            int testflag = 1;
            while (testflag <= flags)
            {
                if ((flags & testflag) == testflag)
                {
                    object img = resources.GetObject("Indicator_" + testflag);
                    if (img != null && img is Image)
                        retObj.Add(img as Image);
                }
                power++;
                testflag = (int)Math.Pow(2, power);
            }
            return retObj;
        }
    }
}
