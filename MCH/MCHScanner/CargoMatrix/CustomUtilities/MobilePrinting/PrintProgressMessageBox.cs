﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Symbol.WPAN.Bluetooth;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace CustomUtilities.MobilePrinting
{
    public partial class PrintProgressMessageBox : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        static readonly object stopLock = new object();
        static readonly object blueToothReadLock = new object();

        private int progress = 0;
        private string printerMacAddress;
        private string printerIpAddress;
        private int printerPort;
        private string pin;
        private string[] barcodes;
        private string[] humanReadableTexts;
        private int numberOfCopies = 1;
        private PrinterTypeEnum printerType;
        private PrintActionType actionType;
        private int numberOfCopiesPrinted;

        private System.Windows.Forms.Timer timerPrint;

        PrinterCommandsParser commandsParser;
        PrinterStatusHolder printerStatus;

        TcpClient client;
        NetworkStream stream;

        Bluetooth bluetooth;
        RemoteDevice remoteDevice;

        #region Properties
        public string PrinterMacAddress
        {
            get { return printerMacAddress; }
            set { printerMacAddress = value; }
        }

        public string Pin
        {
            get { return pin; }
            set { pin = value; }
        }

        public string PrinterIpAddress
        {
            get { return printerIpAddress; }
            set { printerIpAddress = value; }
        }

        public int PrinterPort
        {
            get { return printerPort; }
            set { printerPort = value; }
        }

        public int NumberOfCopies
        {
            get { return numberOfCopies; }
            set { numberOfCopies = value; }
        }

        public string[] Barcodes
        {
            get { return barcodes; }
            set { barcodes = value; }
        }

        public string[] HumanReadableTexts
        {
            get { return humanReadableTexts; }
            set { humanReadableTexts = value; }
        }
        
        #endregion

        #region Constructors
        public PrintProgressMessageBox(PrinterTypeEnum printerType, PrintActionType actionType)
        {
            InitializeComponent();
            Title = "Printing progress...";
            this.buttonOk.Enabled = false;
            this.buttonCancel.Enabled = false;

            this.printerType = printerType;
            this.actionType = actionType;
            this.numberOfCopiesPrinted = 0;
            commandsParser = new PrinterCommandsParser();

            this.timerPrint = new System.Windows.Forms.Timer();
            this.timerPrint.Interval = 1;
            this.timerPrint.Tick += new EventHandler(timerPrint_Tick);

            this.Load += new EventHandler(PrintProgressMessageBox_Load);
        } 
        #endregion

        #region Wireless
        private void UpdateWirelessPrinterStatus(NetworkStream stream, out PrinterStatusHolder printerStatus)
        {
            printerStatus = null;

            try
            {
                var message = commandsParser.CreatePrinterStatusMessage();
                var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(message);
                stream.Write(bytes, 0, bytes.Length);

                bytes = new byte[20];

                stream.Read(bytes, 0, bytes.Length);

                printerStatus = commandsParser.ParsePrinterStatus(bytes);

                if (printerStatus.StatusType == PrinterStatusEnum.Error ||
                    printerStatus.StatusType == PrinterStatusEnum.NA)
                {
                    message = commandsParser.CreatePrinterStatusMessage();
                    bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(message);
                    stream.Write(bytes, 0, bytes.Length);

                    bytes = new byte[20];

                    stream.Read(bytes, 0, bytes.Length);

                    printerStatus = commandsParser.ParsePrinterStatus(bytes);
                }

                this.lblPrinterStatus.Text = string.Format("Printer status: {0}", printerStatus.Message);

            }
            catch
            {
                this.lblPrinterStatus.Text = "Could not read printer status";
            }
            finally
            {
                this.lblPrinterStatus.Refresh();
            }
        }

        private void PrintWireless(Func<NetworkStream, bool> printAction)
        {
            try
            {
                this.SetMessage("Establishing connection to printer ...", false);

                this.ClearPrinterStatus();

                var ipAddress = IPAddress.Parse(this.printerIpAddress);

                client = new TcpClient(this.printerIpAddress, this.printerPort);

                if (client.Client.Connected == false)
                {
                    this.SetMessage("Unable to connect to printer. Retry?", true);
                    return;
                }

                this.SetMessage("Connected to printer ...", false);

                stream = client.GetStream();

                if (stream.CanWrite == false)
                {
                    this.SetMessage("Unable to send data to printer. Retry?", true);
                    return;
                }

                var result = printAction(stream);

                if (result == false)
                {
                    this.SetMessage("Unable to send data to printer. Retry?", true);
                    return;
                }
                else
                {
                    if (printerStatus != null &&
                        printerStatus.IsWarning == true)
                    {
                        return;
                    }
                }

                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception ex)
            {
                string message = string.Format("Unable to send data to printer. {0}. Retry?", ex.Message);

                this.SetMessage(message, true);
            }
            finally
            {
                this.StopCommunication();
            }
        }

        private void PrintWireless()
        {
            Func<NetworkStream, bool> printAction = stream => this.PrintWireless(stream, barcodes, humanReadableTexts, numberOfCopies);

            this.PrintWireless(printAction);
        }

        private void CalibrateWireless()
        {
            Func<NetworkStream, bool> printAction = stream =>
            {
                try
                {
                    string gapMessage = commandsParser.CreateCalibrateGAPMessage();
                    var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(gapMessage);
                    stream.Write(bytes, 0, bytes.Length);

                    return true;
                }
                catch
                {
                    return false;
                }
            };

            this.PrintWireless(printAction);
        }

        private bool PrintWireless(NetworkStream stream, string[] barcodes, string[] humanReadableTexts, int numberOfCopies)
        {
            this.EnebleOkButton(false);

            this.UpdateWirelessPrinterStatus(stream, out printerStatus);

            if (printerStatus == null || printerStatus.IsError == true) return false;
            
            string str = commandsParser.CreateLabelPositionMessage();
            var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(str);

            try
            {
                stream.Write(bytes, 0, bytes.Length);
            }
            catch { return false; }


            barcodes = barcodes.Skip(progress).ToArray();

            int j = 0;
            foreach (string barcode in barcodes)
            {
                string humanReadableText = humanReadableTexts.Length > 0 && j < humanReadableTexts.Length ? humanReadableTexts[j] : string.Empty;
                j++;

                int brcodeOffset = GetBarcodePositionOffset(barcode.Length);

                for (int i = numberOfCopiesPrinted; i < numberOfCopies; i++)
                {
                    try
                    {
                        str = commandsParser.CreatePrintBarcodeMessage(brcodeOffset, barcode, humanReadableText);
                        bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(str);
                        stream.Write(bytes, 0, bytes.Length);

                        this.lblCommunicationStatus.Text = string.Format("Sent to print :\n{0} of {1} / {2}", j * (i + 1), barcodes.Count() * numberOfCopies, barcode);
                        this.Refresh();
                    }
                    catch
                    {
                        numberOfCopiesPrinted = i;

                        this.UpdateWirelessPrinterStatus(stream, out printerStatus);

                        return false;
                    }
                }

                progress++;
                numberOfCopiesPrinted = 0;
            }

            this.UpdateWirelessPrinterStatus(stream, out printerStatus);

            if (printerStatus == null || printerStatus.IsError == true) return false;
           
            return true;
        }

        #endregion

        #region Bluetooth

        private void UpdateBluetoothPrinterStatus(RemoteDevice remoteDevice, out PrinterStatusHolder printerStatus)
        {
            printerStatus = null;

            try
            {
                var message = commandsParser.CreatePrinterStatusMessage();
                var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(message);
                remoteDevice.Write(bytes);

                bytes = remoteDevice.Read(20);

                printerStatus = commandsParser.ParsePrinterStatus(bytes);

                if (printerStatus.StatusType == PrinterStatusEnum.Error ||
                    printerStatus.StatusType == PrinterStatusEnum.NA)
                {
                    message = commandsParser.CreatePrinterStatusMessage();
                    bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(message);
                    remoteDevice.Write(bytes);

                    remoteDevice.Read(20);

                    printerStatus = commandsParser.ParsePrinterStatus(bytes);
                }

                this.lblPrinterStatus.Text = string.Format("Printer status: {0}", printerStatus.Message);

            }
            catch
            {
                this.lblPrinterStatus.Text = "Could not read printer status";
            }
            finally
            {
                this.lblPrinterStatus.Refresh();
            }
        }
        
        private void PrintBluetooth()
        {
            Func<RemoteDevice, bool> printAction = device => this.PrintBluetooth(device, this.barcodes, this.humanReadableTexts, this.numberOfCopies);

            PrintBluetooth(printAction);
        }

        private void PrintBluetooth(Func<RemoteDevice, bool> printAction)
        {
            Cursor.Current = Cursors.Default;

            this.SetMessage("Establishing connection to printer ...", false);

            this.ClearPrinterStatus();

            bool result = this.TryEnableBluetooth(out bluetooth);

            Cursor.Current = Cursors.Default;

            if (result == false) return;

            string printerName = string.Format("SATO MOBILE PRINTER {0}", printerMacAddress);

            result = this.TryCreateRemoteDevice(printerName, bluetooth, out remoteDevice);

            if (result == false)
            {
                result = this.TrySearchOrCreateRemoteDevice(printerName, bluetooth, out remoteDevice);

                if (result == false)
                {
                    try
                    {
                        bluetooth.Disable();
                    }
                    catch { }

                    return;
                }
            }

            result = this.TryPairDevice(remoteDevice, false);

            if (result == false)
            {
                result = this.TrySearchOrCreateRemoteDevice(printerName, bluetooth, out remoteDevice);

                if (result == false)
                {
                    try
                    {
                        bluetooth.Disable();
                    }
                    catch { }

                    return;
                }

                result = this.TryPairDevice(remoteDevice, true);

                if (result == false)
                {
                    try
                    {
                        remoteDevice.UnPair();
                    }
                    catch { }

                    try
                    {
                        bluetooth.Disable();
                    }
                    catch { }

                    return;
                }
            }

            result = this.TryOpenCommunicationPort(remoteDevice);

            this.Refresh();

            if (result == false)
            {
                try
                {
                    remoteDevice.ClosePort();
                }
                catch { }

                try
                {
                    remoteDevice.UnPair();
                }
                catch { }

                try
                {
                    bluetooth.Disable();
                }
                catch { }

                return;
            }

            this.SetMessage("Connected to printer ...", false);

            result = printAction(remoteDevice);

            if (result == false)
            {
                this.SetMessage("Unable to send data to printer. Retry?", true);
                return;
            }
            else
            {
                if (printerStatus != null &&
                    printerStatus.IsWarning == true)
                {
                    return;
                }
            }
    
           this.DialogResult = DialogResult.OK;
        }

        private void CalibrateBluetooth()
        {
            Func<RemoteDevice, bool> printAction = device =>
            {
                try
                {
                    string gapMessage = commandsParser.CreateCalibrateGAPMessage();
                    var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(gapMessage);

                    lock (blueToothReadLock)
                    {
                        device.Write(bytes);
                    }

                    return true;
                }
                catch
                {
                    this.SetMessage("Unable to send information to printer. Retry?", true);
                    return false;
                }
            };

            this.PrintBluetooth(printAction);
        }

        private bool PrintBluetooth(RemoteDevice remoteDevice, string[] barcodes, string[] humanReadableTexts, int numberOfCopies)
        {
            this.EnebleOkButton(false);
            
            this.UpdateBluetoothPrinterStatus(remoteDevice, out printerStatus);

            if (printerStatus == null || printerStatus.IsError == true) return false;

            string str = commandsParser.CreateLabelPositionMessage();
            var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(str);

            try
            {
                remoteDevice.Write(bytes);
            }
            catch
            {
                return false;
            }

            barcodes = barcodes.Skip(progress).ToArray();

            int j = 0;
            foreach (string barcode in barcodes)
            {
                string humanReadableText = humanReadableTexts.Length > 0 && j < humanReadableTexts.Length ? humanReadableTexts[j] : string.Empty;
                j++;

                int brcodeOffset = GetBarcodePositionOffset(barcode.Length);

                for (int i = numberOfCopiesPrinted; i < numberOfCopies; i++)
                {
                    try
                    {
                        str = commandsParser.CreatePrintBarcodeMessage(brcodeOffset, barcode, humanReadableText);
                        bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(str);
                        remoteDevice.Write(bytes);

                        this.lblCommunicationStatus.Text = string.Format("Sent to print :\n{0} of {1} / {2}", j * (i + 1), barcodes.Count() * numberOfCopies, barcode);
                        this.Refresh();
                    }
                    catch
                    {
                        numberOfCopiesPrinted = i;

                        return false;
                    }
                }

                progress++;
                numberOfCopiesPrinted = 0;
            }

            this.UpdateBluetoothPrinterStatus(remoteDevice, out printerStatus);

            if (printerStatus == null || printerStatus.IsError == true) return false;

            return true;
        }

        private bool TryEnableBluetooth(out Bluetooth bluetooth)
        {
            try
            {
                bluetooth = new Bluetooth();
                bluetooth.RadioMode = BTH_RADIO_MODE.BTH_DISCOVERABLE_AND_CONNECTABLE;
                bluetooth.Enable();

                return true;
            }
            catch
            {
                this.SetMessage("Unable to initialize Bluetooth. Retry?", true);
                bluetooth = null;
                return false;
            }
        }

        private bool TryCreateRemoteDevice(string printerName, Bluetooth bluetooth, out RemoteDevice remoteDevice)
        {
            try
            {
                remoteDevice = new RemoteDevice(printerName, printerMacAddress, printerName);
                remoteDevice.LocalComPort = 9;
                bluetooth.RemoteDevices.Add(remoteDevice);
                return true;
            }
            catch
            {
                remoteDevice = null;
                return false;
            }
        }

        private bool TrySearchOrCreateRemoteDevice(string printerName, Bluetooth bluetooth, out RemoteDevice remoteDevice)
        {
            remoteDevice = GetDevice(bluetooth, printerMacAddress);

            if (remoteDevice == null)
            {
                bool result = this.TryCreateRemoteDevice(printerName, bluetooth, out remoteDevice);

                if (result == false)
                {
                    this.SetMessage("Unable to initialize remote device. Retry?", true);
                    remoteDevice = null;
                    return false;
                }
            }

            return true;
        }

        private bool TryPairDevice(RemoteDevice remoteDevice, bool setMessage)
        {
            try
            {
                if (remoteDevice.IsPaired == true) return true;

                remoteDevice.Pair(pin);
                return true;
            }
            catch
            {
                if (setMessage == true)
                {
                    this.SetMessage("Unable to pair remote device. Retry?", true);
                }
                return false;
            }
        }

        private bool TryOpenCommunicationPort(RemoteDevice remoteDevice)
        {
            try
            {
                if (remoteDevice.IsComPortOpened == true) return true;

                remoteDevice.LocalComPort = 9;
                remoteDevice.OpenPort();

                return true;
            }
            catch
            {
                this.SetMessage("Unable to open communication port. Retry?", true);
                return false;
            }
        }

        #endregion

        #region Handlers
        void timerPrint_Tick(object sender, EventArgs e)
        {
            switch (this.printerType)
            {
                case PrinterTypeEnum.Bluetooth:

                    if (actionType == PrintActionType.Print)
                    {
                        this.PrintBluetooth();
                        break;
                    }

                    if (actionType == PrintActionType.Calibrate)
                    {
                        this.CalibrateBluetooth();
                    }
                    break;


                case PrinterTypeEnum.Wireless:

                    if (actionType == PrintActionType.Print)
                    {
                        this.PrintWireless();
                        break;
                    }

                    if (actionType == PrintActionType.Calibrate)
                    {
                        this.CalibrateWireless();
                    }
                    break;
            }

            this.timerPrint.Enabled = false;
        }

        void PrintProgressMessageBox_Load(object sender, EventArgs e)
        {
            this.timerPrint.Enabled = true;
        }

        #endregion

        #region Overrides
        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            this.timerPrint.Enabled = true;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.StopCommunication();
            
            base.OnClosing(e);
        }

        #endregion

        #region Additional Methods
        private void ClearPrinterStatus()
        {
            this.lblPrinterStatus.Text = string.Empty;
            this.lblPrinterStatus.Refresh();
        }

        private static bool HasDevice(Bluetooth bluetooth, string printerMacAddress)
        {
            RemoteDevice device = null;
            for (int i = 0; i < bluetooth.RemoteDevices.Length; i++)
            {
                if (bluetooth.RemoteDevices[i].Address.Equals(printerMacAddress))
                {
                    device = bluetooth.RemoteDevices[i];
                    break;
                }
            }

            return device != null;
        }

        private static RemoteDevice GetDevice(Bluetooth bluetooth, string printerMacAddress)
        {
            RemoteDevice device = null;
            for (int i = 0; i < bluetooth.RemoteDevices.Length; i++)
            {
                if (bluetooth.RemoteDevices[i].Address.Equals(printerMacAddress))
                {
                    device = bluetooth.RemoteDevices[i];
                    break;
                }
            }

            return device;
        }

        private void SetMessage(string message, bool buttonsEnebled)
        {
            this.lblCommunicationStatus.Text = message;
            this.buttonOk.Enabled = buttonsEnebled;
            this.buttonCancel.Enabled = true;

            this.Refresh();
        }

        private static int GetBarcodePositionOffset(int barcodeLength)
        {
            if (barcodeLength < 2) return 300;

            if (barcodeLength > 19) return 5;

            switch (barcodeLength)
            {
                case 19: return 10;

                case 18: return 20;

                case 17: return 30;

                case 16: return 40;

                case 15: return 50;

                case 14: return 60;

                case 13: return 80;

                case 12: return 100;

                case 11: return 120;

                case 10: return 140;

                case 9: return 160;

                case 8: return 180;

                case 7: return 200;

                case 6: return 220;

                case 5: return 240;

                case 4: return 260;

                case 3: return 280;

                case 2: return 290;

                default: return 5;
            }

        }

        private void StopCommunication()
        {
            this.timerPrint.Enabled = false;
            
            if (stream != null)
            {
                try
                {
                    stream.Close();
                }
                catch { }
            }

            if (client != null)
            {
                try
                {
                    client.Close();
                }
                catch { }
            }

            if (this.remoteDevice != null)
            {
                try
                {
                    this.remoteDevice.ClosePort();
                }
                catch { }
            }

            if (this.bluetooth != null)
            {
                try
                {
                    bluetooth.Disable();
                }
                catch { }
            }
        }

        private void EnebleOkButton(bool enable)
        {
            this.buttonOk.Enabled = enable;
            this.buttonOk.Refresh();
        }

        #endregion

        public enum PrinterTypeEnum
        {
            Bluetooth,
            Wireless
        }

        public enum PrintActionType
        {
            Print,
            Calibrate
        }
    }
}
