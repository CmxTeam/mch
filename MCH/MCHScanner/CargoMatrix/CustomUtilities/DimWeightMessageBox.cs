﻿using System;

//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class DimWeightMessageBox : CargoMatrix.UI.MessageBoxBase//Form
    {
        
        private string m_caption;

        private static DimWeightMessageBox instance = null;
        public DimWeightMessageBox()
        {
            InitializeComponent();

       

        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonPlus.PressedImage = Resources.Graphics.Skin.add_btn_over;
            this.buttonPlus.Image = Resources.Graphics.Skin.add_btn;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Packaging Type";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";


            actPopup.RemoveAllItems();
            foreach (var packageType in CargoMatrix.Communication.ScannerUtility.Instance.GetPackageTypeList())
            {
            actPopup.AddItem(  new SmoothListbox.ListItems.StandardListItem(packageType.name, null, packageType.Id));
            
            
            }


            //Control[] actions = new Control[]
            //    {   
            //        new SmoothListbox.ListItems.StandardListItem("SKID",null,1 ),
            //        new SmoothListbox.ListItems.StandardListItem("CTN", null, 2),
            //        new SmoothListbox.ListItems.StandardListItem("BOX", null, 2),


            //    };
            //actPopup.RemoveAllItems();
            //actPopup.AddItems(actions);
            

            if (DialogResult.OK == actPopup.ShowDialog())
            {
                instance.cmxTextBoxType.Text = actPopup.SelectedItems[0].Name;
            }
      
 
         
        }

        private int ParseStringToInt(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch { return 0; }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {

            if (cmxTextBoxPiece.Enabled && ParseStringToInt(cmxTextBoxPiece.Text) < 1)
            {
                CargoMatrix.UI.CMXMessageBox.Show("'Pieces' is not in a valid format. Please provide the number of pieces.", "Error", CMXMessageBoxIcon.Exclamation);
                return;
            }


            string msg = string.Format("Are you sure you want to apply this dimentions to Housebill# {0}?", m_caption);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Dimensions", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }
        //public static DialogResult Show(string title, out double wt, out int l, out int w, out int h)
        //{
        //    if (instance == null)
        //        instance = new DimWeightMessageBox();


        //    instance.cmxTextBoxWeight.Enabled = true;
        //    instance.cmxTextBoxLength.Enabled = true;
        //    instance.cmxTextBoxWidth.Enabled = true;
        //    instance.cmxTextBoxHeight.Enabled = true;

        //    instance.cmxTextBoxWeight.Text = string.Empty;
        //    instance.cmxTextBoxLength.Text = string.Empty;
        //    instance.cmxTextBoxWidth.Text = string.Empty;
        //    instance.cmxTextBoxHeight.Text = string.Empty;
        //    instance.cmxTextBoxWeight.Focus();

        //    instance.m_caption = title;

        //    return instance.CMXShowDialog(out wt, out l, out w, out h);
        //}

        public static DialogResult ShowCountMode(string title, ref double wt, ref int l, ref int w, ref int h, ref int piece, int totalPieces, ref string type, ref bool isManual)
        {
            if (instance == null)
                instance = new DimWeightMessageBox();

            instance.cmxTextBoxWeight.Enabled = false;
            instance.cmxTextBoxLength.Enabled = false;
            instance.cmxTextBoxWidth.Enabled = false;
            instance.cmxTextBoxHeight.Enabled = false;
            instance.cmxTextBoxPiece.Enabled = true;
            instance.cmxTextBoxPieces.Enabled = false;
            instance.cmxTextBoxType.Enabled = false;

            instance.cmxTextBoxWeight.Text = wt.ToString();
            instance.cmxTextBoxLength.Text = l.ToString();
            instance.cmxTextBoxWidth.Text = w.ToString();
            instance.cmxTextBoxHeight.Text = h.ToString();

            instance.label8.Text = "Pieces:";
            instance.cmxTextBoxPiece.Text = "";
            instance.cmxTextBoxPieces.Text = totalPieces.ToString();
            instance.cmxTextBoxType.Text = type;

            instance.cmxTextBoxPiece.Focus();

            instance.m_caption = title;

            //This is for manual dim entry
            isManual = false;
            return instance.CMXShowDialog(out wt, out l, out w, out h, out type, out piece);

        }

        public static DialogResult ShowPieceMode(string title, ref double wt, ref int l, ref int w, ref int h, ref int piece, int totalPieces,ref string type,ref bool isManual)
        {
            if (instance == null)
                instance = new DimWeightMessageBox();

            instance.cmxTextBoxWeight.Enabled = false;
            instance.cmxTextBoxLength.Enabled = false;
            instance.cmxTextBoxWidth.Enabled = false;
            instance.cmxTextBoxHeight.Enabled = false;
            instance.cmxTextBoxPiece.Enabled = false;
            instance.cmxTextBoxPieces.Enabled = false;
            instance.cmxTextBoxType.Enabled = false;

            instance.cmxTextBoxWeight.Text = wt.ToString();
            instance.cmxTextBoxLength.Text = l.ToString();
            instance.cmxTextBoxWidth.Text = w.ToString();
            instance.cmxTextBoxHeight.Text = h.ToString();

            instance.label8.Text = "Piece#:";
            instance.cmxTextBoxPiece.Text = piece.ToString();
            instance.cmxTextBoxPieces.Text = totalPieces.ToString();
            instance.cmxTextBoxType.Text = type;

            //instance.cmxTextBoxWeight.Focus();

            instance.m_caption = title;

            //This is for manual dim entry
            isManual = false;
            return instance.CMXShowDialog(out wt, out l, out w, out h, out type, out piece);
             
        }
        private DialogResult CMXShowDialog(out double wt, out int l, out int w, out int h, out string t,out int p)
        {
            DialogResult result = ShowDialog();
     
            try
            {
                wt = Convert.ToDouble(cmxTextBoxWeight.Text);
            }
            catch (Exception)
            {
                wt = 0;
            }
            try
            {
                l = Convert.ToInt32(cmxTextBoxLength.Text);
            }
            catch (Exception)
            {
                l = 0;
            }
            try
            {
                w = Convert.ToInt32(cmxTextBoxWidth.Text);
            }
            catch (Exception)
            {
                w = 0;
            }


            try
            {
                h = Convert.ToInt32(cmxTextBoxHeight.Text);
            }
            catch (Exception)
            {
                h = 0;
            }

            try
            {
                p = Convert.ToInt32(cmxTextBoxPiece.Text);
            }
            catch (Exception)
            {
                p = 0;
            }

            t = instance.cmxTextBoxType.Text;

            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}