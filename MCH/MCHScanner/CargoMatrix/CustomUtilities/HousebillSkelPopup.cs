﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class HousebillSkelPopup : CargoMatrix.UI.MessageBoxBase//Form
    {
        private string m_caption;

        private static HousebillSkelPopup instance = null;
        public HousebillSkelPopup()
        {
            InitializeComponent();
            string optional = "Optional";
            TextBoxCarrier.WatermarkText = optional;
            TextBoxWeight.WatermarkText = optional;
            TextBoxConsol.WatermarkText = optional;
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
        }


        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            TextBoxOrigin.Text = TextBoxOrigin.Text.ToUpper();
            TextBoxDestination.Text = TextBoxDestination.Text.ToUpper();
            int pieces, slac;
            double weight;
            TextBoxOrigin.Text = TextBoxOrigin.Text.ToUpper();
            TextBoxDestination.Text = TextBoxDestination.Text.ToUpper();
            TextBoxCarrier.Text = TextBoxCarrier.Text.ToUpper();

            if (TextBoxPieces.Text == string.Empty || textBoxSlac.Text == string.Empty)
            {
                CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_PieceSlacEmpty, "Error", CMXMessageBoxIcon.Exclamation);
                if (string.IsNullOrEmpty(TextBoxPieces.Text))
                    TextBoxPieces.Focus();
                else
                    textBoxSlac.Focus();
                return;
            }
            if (TextBoxOrigin.Text.Length != 3 || TextBoxDestination.Text.Length !=3 )
            {
                CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_OrgDestInvalid, "Error", CMXMessageBoxIcon.Exclamation);
                if (TextBoxOrigin.Text.Length != 3)
                    TextBoxOrigin.Focus();
                else
                    TextBoxDestination.Focus();
                return;
            }

            /// validate pieces
            try
            {
                pieces = Convert.ToInt32(TextBoxPieces.Text);
            }
            catch (Exception)
            {
                pieces = 0;
            }
            /// validate slac
            try
            {
                slac = Convert.ToInt32(textBoxSlac.Text);
            }
            catch (Exception)
            {
                slac = pieces;
            }

            /// validate Weight
            try
            {
                weight = (double)Convert.ToDecimal(TextBoxWeight.Text);
            }
            catch (Exception)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(, "Error", CMXMessageBoxIcon.Exclamation);
                //TextBoxWeight.Text.
                weight = 0.0;
            }

            /// Consol and Carrier must be set in pair
            if ((string.IsNullOrEmpty(TextBoxCarrier.Text) && !string.IsNullOrEmpty(TextBoxConsol.Text)) ||
               (!string.IsNullOrEmpty(TextBoxCarrier.Text) && string.IsNullOrEmpty(TextBoxConsol.Text)))
            {
                CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_CarrierConsolEmpty, "Error", CMXMessageBoxIcon.Exclamation);
                if (string.IsNullOrEmpty(TextBoxCarrier.Text))
                    TextBoxCarrier.Focus();
                else 
                    TextBoxConsol.Focus();
                return;
            }

            /// consol No 8 chars
            if (!string.IsNullOrEmpty(TextBoxConsol.Text) && TextBoxConsol.Text.Length != 8)
            {
                CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_InvalidConsolNumber, "Error", CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (slac < pieces)
            {
                CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_PieceSlacCheck, "Error", CMXMessageBoxIcon.Exclamation);
                textBoxSlac.Focus();
            }
            else
                DialogResult = DialogResult.OK;

        }
        
        public static DialogResult Show(string title, out HousebillSkel skel)
        {
            if (instance == null)
                instance = new HousebillSkelPopup();

            instance.TextBoxOrigin.Text = string.Empty;
            instance.TextBoxDestination.Text = string.Empty;
            instance.TextBoxPieces.Text = string.Empty;
            instance.textBoxSlac.Text = string.Empty;
            instance.TextBoxWeight.Text = string.Empty;
            instance.TextBoxConsol.Text = string.Empty;
            instance.TextBoxCarrier.Text = string.Empty;
            instance.m_caption = title;

            return instance.CMXShowDialog(out skel);
        }

        private DialogResult CMXShowDialog(out HousebillSkel skel)
        {
            DialogResult result = ShowDialog();
            skel = new HousebillSkel();
            skel.origin = TextBoxOrigin.Text;
            skel.destination = TextBoxDestination.Text;
            skel.carrier = TextBoxCarrier.Text;
            skel.mawbNo = TextBoxConsol.Text;

            try
            {
                skel.pieces = Convert.ToInt32(TextBoxPieces.Text);
            }
            catch (Exception)
            {
                skel.pieces = 0;
            }

            try
            {
                skel.slac = Convert.ToInt32(textBoxSlac.Text);
            }
            catch (Exception)
            {
                skel.slac = skel.pieces;
            }
            try
            {
                skel.weight = Convert.ToDouble(TextBoxWeight.Text);
            }
            catch (Exception)
            {
                skel.weight = 0;
            }

            return result;
        }


        void cmxTextBoxPieces_TextChanged(object sender, System.EventArgs e)
        {
            textBoxSlac.Text = TextBoxPieces.Text;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

    }
    public class HousebillSkel
    {
        public int pieces, slac;
        public double weight;
        public string origin, destination, carrier, mawbNo;
    }
}