﻿namespace CustomUtilities
{
    partial class HousebillSkelPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDest = new System.Windows.Forms.Label();
            this.textBoxSlac = new CargoMatrix.UI.CMXTextBox();
            this.TextBoxPieces = new CargoMatrix.UI.CMXTextBox();
            this.TextBoxWeight = new CargoMatrix.UI.CMXTextBox();
            this.TextBoxDestination = new CargoMatrix.UI.CMXTextBox();
            this.labelPcs = new System.Windows.Forms.Label();
            this.labelSlac = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelOrigin = new System.Windows.Forms.Label();
            this.TextBoxConsol = new CargoMatrix.UI.CMXTextBox();
            this.TextBoxCarrier = new CargoMatrix.UI.CMXTextBox();
            this.message = new System.Windows.Forms.Label();
            this.TextBoxOrigin = new CargoMatrix.UI.CMXTextBox();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.labelDest);
            this.panel1.Controls.Add(this.textBoxSlac);
            this.panel1.Controls.Add(this.TextBoxPieces);
            this.panel1.Controls.Add(this.TextBoxWeight);
            this.panel1.Controls.Add(this.TextBoxDestination);
            this.panel1.Controls.Add(this.labelPcs);
            this.panel1.Controls.Add(this.labelSlac);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labelOrigin);
            this.panel1.Controls.Add(this.TextBoxConsol);
            this.panel1.Controls.Add(this.TextBoxCarrier);
            this.panel1.Controls.Add(this.message);
            this.panel1.Controls.Add(this.TextBoxOrigin);
            this.panel1.Location = new System.Drawing.Point(3, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 251);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 207);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(116, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 15);
            this.label2.Text = "Wgt-kg:";
            // 
            // labelDest
            // 
            this.labelDest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDest.BackColor = System.Drawing.Color.White;
            this.labelDest.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelDest.ForeColor = System.Drawing.Color.Black;
            this.labelDest.Location = new System.Drawing.Point(114, 96);
            this.labelDest.Name = "labelDest";
            this.labelDest.Size = new System.Drawing.Size(50, 15);
            this.labelDest.Text = "Dest:";
            // 
            // textBoxSlac
            // 
            this.textBoxSlac.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSlac.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxSlac.Location = new System.Drawing.Point(171, 55);
            this.textBoxSlac.Name = "textBoxSlac";
            this.textBoxSlac.Size = new System.Drawing.Size(45, 26);
            this.textBoxSlac.TabIndex = 2;
            this.textBoxSlac.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            // 
            // TextBoxPieces
            // 
            this.TextBoxPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxPieces.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TextBoxPieces.Location = new System.Drawing.Point(53, 55);
            this.TextBoxPieces.Name = "TextBoxPieces";
            this.TextBoxPieces.Size = new System.Drawing.Size(45, 26);
            this.TextBoxPieces.TabIndex = 1;
            this.TextBoxPieces.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.TextBoxPieces.TextChanged += new System.EventHandler(this.cmxTextBoxPieces_TextChanged);
            // 
            // TextBoxWeight
            // 
            this.TextBoxWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxWeight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TextBoxWeight.Location = new System.Drawing.Point(171, 126);
            this.TextBoxWeight.Name = "TextBoxWeight";
            this.TextBoxWeight.Size = new System.Drawing.Size(45, 26);
            this.TextBoxWeight.TabIndex = 6;
            this.TextBoxWeight.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
            this.TextBoxWeight.WatermarkText = "Optional";
            // 
            // TextBoxDestination
            // 
            this.TextBoxDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxDestination.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TextBoxDestination.Location = new System.Drawing.Point(171, 90);
            this.TextBoxDestination.MaxLength = 3;
            this.TextBoxDestination.Name = "TextBoxDestination";
            this.TextBoxDestination.Size = new System.Drawing.Size(45, 26);
            this.TextBoxDestination.TabIndex = 4;
            this.TextBoxDestination.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Alphabetic;
            // 
            // labelPcs
            // 
            this.labelPcs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPcs.BackColor = System.Drawing.Color.White;
            this.labelPcs.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPcs.ForeColor = System.Drawing.Color.Black;
            this.labelPcs.Location = new System.Drawing.Point(1, 61);
            this.labelPcs.Name = "labelPcs";
            this.labelPcs.Size = new System.Drawing.Size(50, 15);
            this.labelPcs.Text = "Pieces:";
            // 
            // labelSlac
            // 
            this.labelSlac.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSlac.BackColor = System.Drawing.Color.White;
            this.labelSlac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelSlac.ForeColor = System.Drawing.Color.Black;
            this.labelSlac.Location = new System.Drawing.Point(114, 61);
            this.labelSlac.Name = "labelSlac";
            this.labelSlac.Size = new System.Drawing.Size(50, 15);
            this.labelSlac.Text = "SLAC:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(1, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 15);
            this.label3.Text = "MAWB#:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.Text = "Carrier:";
            // 
            // labelOrigin
            // 
            this.labelOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOrigin.BackColor = System.Drawing.Color.White;
            this.labelOrigin.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelOrigin.ForeColor = System.Drawing.Color.Black;
            this.labelOrigin.Location = new System.Drawing.Point(1, 96);
            this.labelOrigin.Name = "labelOrigin";
            this.labelOrigin.Size = new System.Drawing.Size(50, 15);
            this.labelOrigin.Text = "Origin:";
            // 
            // TextBoxConsol
            // 
            this.TextBoxConsol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxConsol.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TextBoxConsol.Location = new System.Drawing.Point(53, 168);
            this.TextBoxConsol.Name = "TextBoxConsol";
            this.TextBoxConsol.Size = new System.Drawing.Size(163, 26);
            this.TextBoxConsol.MaxLength = 8;
            this.TextBoxConsol.TabIndex = 7;
            this.TextBoxConsol.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            // 
            // TextBoxCarrier
            // 
            this.TextBoxCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxCarrier.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TextBoxCarrier.Location = new System.Drawing.Point(53, 126);
            this.TextBoxCarrier.MaxLength = 4;
            this.TextBoxCarrier.Name = "TextBoxCarrier";
            this.TextBoxCarrier.Size = new System.Drawing.Size(45, 26);
            this.TextBoxCarrier.TabIndex = 5;
            this.TextBoxCarrier.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            // 
            // message
            // 
            this.message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.message.BackColor = System.Drawing.Color.White;
            this.message.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.message.Location = new System.Drawing.Point(13, 30);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(209, 20);
            this.message.Text = "Provide the following details:";
            // 
            // TextBoxOrigin
            // 
            this.TextBoxOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxOrigin.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TextBoxOrigin.Location = new System.Drawing.Point(53, 90);
            this.TextBoxOrigin.MaxLength = 3;
            this.TextBoxOrigin.Name = "TextBoxOrigin";
            this.TextBoxOrigin.Size = new System.Drawing.Size(45, 26);
            this.TextBoxOrigin.TabIndex = 3;
            this.TextBoxOrigin.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Alphabetic;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(56, 211);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.TabIndex = 11;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 211);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.TabIndex = 12;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            InitializeImages();
            // 
            // HousebillSkelPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "HousebillSkelPopup";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;//CargoMatrix.UI.CMXPictureButton
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label labelDest;
        private System.Windows.Forms.Label labelOrigin;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.Label labelPcs;
        private System.Windows.Forms.PictureBox pictureBox1;
        public CargoMatrix.UI.CMXTextBox TextBoxPieces;//CargoMatrix.UI.CMXTextBox
        public CargoMatrix.UI.CMXTextBox TextBoxOrigin;
        public CargoMatrix.UI.CMXTextBox TextBoxDestination;
        public CargoMatrix.UI.CMXTextBox textBoxSlac;
        private System.Windows.Forms.Label labelSlac;
        private System.Windows.Forms.Label label2;
        public CargoMatrix.UI.CMXTextBox TextBoxWeight;
        private System.Windows.Forms.Label label1;
        public CargoMatrix.UI.CMXTextBox TextBoxCarrier;
        private System.Windows.Forms.Label label3;
        public CargoMatrix.UI.CMXTextBox TextBoxConsol;

    }
}