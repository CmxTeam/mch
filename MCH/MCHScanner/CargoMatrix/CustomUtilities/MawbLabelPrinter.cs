﻿using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication;
using SmoothListbox.ListItems;
using System;
using CMXExtensions;
using CargoMatrix.Communication.DTO;
using System.Drawing;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CustomUtilities
{
    public class MawbLabelPrinter : HawbPiecesLabelPrinter
    {
        private IMasterBillItem mawb;
        protected Label labelCopes;
        protected CargoMatrix.UI.CMXTextBox textBoxCopies;
        private static MawbLabelPrinter instance;
        private MawbLabelPrinter(IMasterBillItem mawb)
            : base(mawb.MasterBillNumber, mawb.CarrierNumber)
        {
            this.mawb = mawb;
        }
        private void LoadControl(IMasterBillItem mawb)
        {
            base.LoadControl();
            textBoxCopies.Text = mawb.Pieces.ToString();
            this.Title = string.Format("{0}-{1}", mawb.CarrierNumber, mawb.MasterBillNumber);
        }
        public static DialogResult Show(CargoMatrix.Communication.DTO.IMasterBillItem mawb)
        {
            if (instance == null)
                instance = new MawbLabelPrinter(mawb);
            else
                instance.mawb = mawb;
            Cursor.Current = Cursors.Default;
            string rsn;
            if (PrinterUtilities.PopulatePrintReasons(true, out rsn))
            {
                instance.reason = rsn;
                instance.LoadControl(mawb);
                return instance.ShowDialog();
            }
            else return DialogResult.Cancel;
        }

        protected override void InitializeComponent()
        {
            base.InitializeComponent();
            this.SuspendLayout();
            this.labelCopes = new System.Windows.Forms.Label();
            this.textBoxCopies = new CargoMatrix.UI.CMXTextBox();
            // 
            // labelCopes
            // 
            this.labelCopes.Location = new System.Drawing.Point(20, 102);
            this.labelCopes.Name = "labelCopes";
            this.labelCopes.Size = new System.Drawing.Size(100, 20);
            this.labelCopes.Font = new System.Drawing.Font("Tahoma", 9, System.Drawing.FontStyle.Bold);
            this.labelCopes.Text = "No of Copies:";
            // 
            // textBoxCopies
            // 
            this.textBoxCopies.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxCopies.Location = new System.Drawing.Point(173, 102);
            this.textBoxCopies.Name = "textBoxCopies";
            this.textBoxCopies.Size = new System.Drawing.Size(45, 28);
            this.textBoxCopies.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textBoxCopies.MaxLength = 3;
            this.textBoxCopies.TabIndex = 1;
            this.panelContent.Controls.Add(this.textBoxCopies);
            this.panelContent.Controls.Add(this.labelCopes);
            this.labelHeader.Text = "Print Masterbill Labels";
            this.AutoScaleDimensions = new SizeF(96F, 96F);
            this.ResumeLayout(false);
        }

        protected override void buttonPrinterList_Click(object sender, System.EventArgs e)
        {
            if (PrinterUtilities.PopulateAvailablePrinters(LabelTypes.MB, ref selectedPrinter))
            {
                DisplayPrinter();
            }
        }

        protected override void buttonOk_Click(object sender, System.EventArgs e)
        {
            int copies = 1;
            try
            {
                copies = Convert.ToInt32(textBoxCopies.Text);
            }
            catch
            {
                copies = 1;
            }

            if (selectedPrinter.PrinterType == PrinterType.MobileLabel ||
                selectedPrinter.PrinterType == PrinterType.MobileWireless)
            {
                /// capture printing reason before printing from mobile printer
                CargoMatrix.Communication.ScannerUtility.Instance.CapturePrintReason(mawb.MasterBillNumber, mawb.CarrierNumber, reason, true,copies,selectedPrinter.PrinterId);

                var mbilePrinter = new MobilePrinting.MobilePrinterUtility(selectedPrinter);
                mbilePrinter.NumberOfCopies = copies;
                mbilePrinter.Print(mawb.CarrierNumber + mawb.MasterBillNumber);
            }
            else
            {

                bool response = ScannerUtility.Instance.PrintMasterBillLabel(mawb.CarrierNumber, mawb.MasterBillNumber, selectedPrinter.PrinterId, copies, reason);
                if (response)
                    CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_PrintQueued + labelPrinter.Text, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                else
                    CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_PrintFailed, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
            DialogResult = DialogResult.OK;
        }
    }
}
