﻿using System;

//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class PassFailCompleteMessageBox : CargoMatrix.UI.MessageBoxBase//Form
    {

        private bool RequiresPhotos=false;
        private string m_caption=string.Empty;

        private static PassFailCompleteMessageBox instance = null;
        public PassFailCompleteMessageBox()
        {
            InitializeComponent();
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonPlus.PressedImage = Resources.Graphics.Skin._3dots_over;
            this.buttonPlus.Image = Resources.Graphics.Skin._3dots;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;




            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                this.buttonCamera.Enabled = true;
                this.buttonCamera.PressedImage = CustomUtilitiesResource.button_camera_over;
                this.buttonCamera.Image = CustomUtilitiesResource.button_camera;
            }
            else
            {
                this.buttonCamera.Enabled = false;
                this.buttonCamera.PressedImage = CustomUtilitiesResource.button_camera_dis;
                this.buttonCamera.Image = CustomUtilitiesResource.button_camera_dis;
            }


        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Select a remark";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "Remarks";


            actPopup.RemoveAllItems();

            int id = 0;
            foreach (var remark in CargoMatrix.Communication.ScannerUtility.Instance.GetGatewayReference("Screening Remarks"))
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(remark, null, id++));
            }

 

            if (DialogResult.OK == actPopup.ShowDialog())
            {
                if (instance.txtRemark.Text == string.Empty)
                {
                    instance.txtRemark.Text = actPopup.SelectedItems[0].Name;
                }
                else
                {
                    instance.txtRemark.Text += " " + actPopup.SelectedItems[0].Name;
                }
            }
     
         
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            instance.RequiresPhotos = false;
            DialogResult = DialogResult.Cancel;
        }

   

        private void buttonOk_Click(object sender, EventArgs e)
        {
            instance.RequiresPhotos = false;
            string msg = string.Format("Are you sure you want to apply this result to Housebill# {0}?", m_caption);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Screening", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                return;
            }
        }
   
        public static DialogResult Show(string reference, string sampleNumber, ref string remark,string result,int count,int total,ref bool requiresPhotos)
        {
            if (instance == null)
                instance = new PassFailCompleteMessageBox();
 
 
            instance.txtRemark.Focus();
            instance.lblSample.Text = "Sample#: " + sampleNumber;
            instance.lblCount.Text = "Slac: " + count + " of " + total;
            instance.lblResult.Text = "Result: " + result;
            instance.txtRemark.Text = remark;
            instance.m_caption = reference;

            return instance.CMXShowDialog(out remark, out requiresPhotos);

        }




        private DialogResult CMXShowDialog(out string remark, out bool requiresPhotos)
        {
            
            DialogResult result = ShowDialog();
 
            remark = instance.txtRemark.Text;
            requiresPhotos = instance.RequiresPhotos;

            return result;
        }

    
        private void buttonCamera_Click(object sender, EventArgs e)
        {
            instance.RequiresPhotos = true;
            string msg = string.Format("Are you sure you want to apply this result to Housebill# {0} and go to 'FreightPhotoCapture'?", m_caption);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Screening", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                return;
            }
        
        }
     
       
    
 


     
    }
}