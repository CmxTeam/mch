﻿using System;

 
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class EtaEditMessageBox : CargoMatrix.UI.MessageBoxBase 
    {
        
        private string m_caption;

        private static EtaEditMessageBox instance = null;
        public EtaEditMessageBox()
        {
            InitializeComponent();

       

        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }



        private void buttonOk_Click(object sender, EventArgs e)
        {
            string msg = string.Format("Are you sure you want to update ETA?", m_caption);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "ETA", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }
 
 
        public static DialogResult Show(string title, ref DateTime eta)
        {
            if (instance == null)
                instance = new EtaEditMessageBox();
 
 
            instance.cmbDay.SelectedItem = string.Format("{0:00}", eta.Day);
            instance.cmbMonth.SelectedItem = string.Format("{0:00}", eta.Month);
            instance.cmbYear.SelectedItem = string.Format("{0:yy}", eta);
            instance.cmbTime.SelectedItem = string.Format("{0:00}", eta.Hour);
            instance.cmbMin.SelectedItem = string.Format("{0:00}", eta.Minute);
      

            instance.m_caption = title;

      
   
            return instance.CMXShowDialog(ref eta);
             
        }
        private DialogResult CMXShowDialog(ref DateTime neweta)
        {
            DialogResult result = ShowDialog();


            DateTime dateValue;
            string dateString = string.Format("{0}/{1}/{2} {3}:{4}", cmbMonth.SelectedItem, cmbDay.SelectedItem, cmbYear.SelectedItem, cmbTime.SelectedItem, cmbMin.SelectedItem);
            try
            {
                dateValue = DateTime.Parse(dateString);
            }
            catch
            {
                dateValue = neweta;
            }

            neweta = dateValue;

            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}