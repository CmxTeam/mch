﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.DamageCapture
{
    public partial class DamageCapture : SmoothListbox.SmoothListBoxAsync2<IShipmentConditionType>
    {
        CargoMatrix.Utilities.MessageListBox printModePopup;

        IHouseBillItem houseBillItem;
        IShipmentConditionType[] conditionTypes;
        List<ShipmentCondition> shipmentConditions;
        DamageCaptureApplyTypeEnum damageCaptureApplyType;

        List<int> checkedItemsIDS;


        public event EventHandler<ShipmentConditionsEventArgs> OnLoadShipmentConditions;
        public event EventHandler<ShipmentConditionsSummaryEventArgs> OnLoadShipmentConditionsSummary;
        public event EventHandler<UpdateShipmentConditionsEventArgs> OnUpdateShipmentConditions;
        public Func<int[]> GetHouseBillPiecesIDs;

        public DamageCapture(IHouseBillItem houseBillItem)
        {
            InitializeComponent();

            this.checkedItemsIDS = new List<int>();

            this.damageCaptureApplyType = DamageCaptureApplyTypeEnum.HouseBill;

            this.houseBillItem = houseBillItem;
            this.TitleText = "Damage Capture";
            panelHeader2.Height = headerPanel.Height;
            headerPanel.Location = panelHeader2.Location;
            panelHeader2.Visible = false;
            ButtonFinalizeText = "Apply";
            this.blinkLabel.Text = DamageCaptureResources.Text_Blink;
            this.label1.Text = string.Format("{0}-{1}-{2}", this.houseBillItem.Origin, this.houseBillItem.HousebillNumber, this.houseBillItem.Destination);

            this.label2.Text = this.GetHeaderLine2Text();

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DamageCapture_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(DamageCapture_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(DamageCapture_MenuItemClicked);
            this.smoothListBoxMainList.MultiSelectEnabled = true;
            ShowContinueButton(true);
            ShowContinueButton(false);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.LoadComplete += new EventHandler(DamageCapture_LoadComplete);
        }

        public DamageCapture(IHouseBillItem houseBillItem, DamageCaptureApplyTypeEnum damageCaptureApplyType)
            : this(houseBillItem)
        {
            this.damageCaptureApplyType = damageCaptureApplyType;
            this.label2.Text = this.GetHeaderLine2Text();
        }

        void DamageCapture_LoadComplete(object sender, EventArgs e)
        {
            DisplayConditions();
        }

        void DamageCapture_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.REFRESH)
                {
                    LoadControl();
                }
            }
        }

        void FireLoadShipmentConditions(ShipmentConditionsEventArgs args)
        {
            var loadConditions = this.OnLoadShipmentConditions;

            if (loadConditions != null)
            {
                loadConditions(this, args);
            }
        }

        void FireLoadShipmentConditionsSummary(ShipmentConditionsSummaryEventArgs args)
        {
            var conditionsSummary = this.OnLoadShipmentConditionsSummary;

            if (conditionsSummary != null)
            {
                conditionsSummary(this, args);
            }
        }

        void FireUpdateShiomentConditions(UpdateShipmentConditionsEventArgs args)
        {
            var updConditions = this.OnUpdateShipmentConditions;

            if (updConditions != null)
            {
                updConditions(this, args);
            }
        }


        public override void LoadControl()
        {
            var args = new ShipmentConditionsEventArgs();

            this.FireLoadShipmentConditions(args);

            if (args.ShipmentConditions != null)
            {
                this.ReloadItemsAsync(args.ShipmentConditions);

                this.conditionTypes = args.ShipmentConditions;
            }
        }

        void DamageCapture_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        void DamageCapture_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (smoothListBoxMainList.SelectedItems.Count > 0)
                ShowContinueButton(true);
            else
                ShowContinueButton(false);
        }

        private delegate void DisplayConditionsDelegate();


        private List<ShipmentCondition> CreateEmptyShipmentConditions(IShipmentConditionType[] conditionTypes)
        {
            List<ShipmentCondition> result = new List<ShipmentCondition>();

            foreach (var item in conditionTypes)
            {
                ShipmentCondition shpCondition = new ShipmentCondition();
                shpCondition.Conditions = new IShipmentConditionType[] { item };

                result.Add(shpCondition);
            }

            return result;
        }

        private List<ShipmentCondition> CreateConditionsFromSummaryes(IShipmentConditionSummary[] summaries, IShipmentConditionType[] conditionTypes)
        {
            var result = this.CreateEmptyShipmentConditions(conditionTypes);

            foreach (SmoothListbox.ListItems.TwoLineListItem item in smoothListBoxMainList.Items)
            {
                foreach (var cond in summaries)
                {
                    if (item.ID == cond.ConditionTypeId)
                    {
                        this.checkedItemsIDS.Add(item.ID);

                        item.LabelLine2.Text = this.GetConditionItemText(cond.Pieces, this.houseBillItem.TotalPieces);
                        item.itemPicture.Image = cond.ContainsAqm == false ? CargoMatrix.Resources.Skin.Notepad_Check : CargoMatrix.Resources.Icons.Notepad_Security;
                        smoothListBoxMainList.MoveItemToTop(item as Control);

                        ShipmentCondition shpCondition = (
                                                            from resultItem in result
                                                            where resultItem.Conditions[0].ConditionTypeId == cond.ConditionTypeId
                                                            select resultItem
                                                         ).First();

                        shpCondition.Pieces = cond.Pieces;

                        break;
                    }
                }
            }

            return result;
        }

        private void DisplayConditions()
        {
            Cursor.Current = Cursors.WaitCursor;
            //smoothListBoxMainList.Reset();
            if (InvokeRequired)
            {
                this.Invoke(new DisplayConditionsDelegate(DisplayConditions));
                return;
            }

            smoothListBoxMainList.SelectNone();
            ShowContinueButton(false);

            ShipmentConditionsSummaryEventArgs args = new ShipmentConditionsSummaryEventArgs();
            args.HouseBillID = this.houseBillItem.HousebillId;
            this.FireLoadShipmentConditionsSummary(args);

            if (args.ConditionsSummaries == null) return;

            var conditions = args.ConditionsSummaries;

            this.checkedItemsIDS.Clear();

            this.shipmentConditions = conditions.Length <= 0 ? this.CreateEmptyShipmentConditions(this.conditionTypes) :
                                                               this.CreateConditionsFromSummaryes(conditions, this.conditionTypes);
            smoothListBoxMainList.LayoutItems();
            Cursor.Current = Cursors.Default;

        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            if (this.damageCaptureApplyType == DamageCaptureApplyTypeEnum.MasterBill)
            {
                this.ApplyForMasterBill();
                return;
            }
            if (this.houseBillItem.ScanMode == ScanModes.Count)
            {
                ApplyDamageInCountMode();
            }
            else
            {
                ApplyDamageInPieceMode();
            }
        }

        private void ApplyForMasterBill()
        {
            UpdateShipmentConditionsEventArgs args = new UpdateShipmentConditionsEventArgs();
            args.HouseBillId = this.houseBillItem.HousebillId;
            args.ScanModes = ScanModes.Count;

            var shpConditions = from item in this.shipmentConditions
                                where (from selectedItem in smoothListBoxMainList.SelectedItems.OfType<SmoothListbox.ListItems.StandardListItem>()
                                       select selectedItem.ID).Contains(item.Conditions[0].ConditionTypeId)
                                      ||
                                     (from id in this.checkedItemsIDS
                                      select id).Contains(item.Conditions[0].ConditionTypeId)
                                select item;

            args.ShipmentConditions = shpConditions.ToArray();

            this.FireUpdateShiomentConditions(args);

            if (args.Result == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Could not update shipment conditions", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }

            DisplayConditions();
        }

        private void ApplyDamageInCountMode()
        {
            int count = 1;

            if (this.houseBillItem.TotalPieces > 1)
            {
                CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
                CntMsg.PieceCount = this.houseBillItem.TotalPieces;
                CntMsg.HeaderText = DamageCaptureResources.Text_CountHeader;
                CntMsg.LabelDescription = DamageCaptureResources.Text_CountBoxText;
                CntMsg.LabelReference = label1.Text;

                if (CntMsg.ShowDialog() != DialogResult.OK)
                    return;
                else count = CntMsg.PieceCount;
            }

            int? slac = null;
            if(houseBillItem.Slac != houseBillItem.TotalPieces)
            {
                slac = this.GetSlacFromUser(count, this.houseBillItem.Slac, false);

                if (slac.HasValue == false) return;
            }

            UpdateShipmentConditionsEventArgs args = new UpdateShipmentConditionsEventArgs();
            args.HouseBillId = this.houseBillItem.HousebillId;
            args.ScanModes = ScanModes.Count;
            
            if (slac.HasValue == true)
            {
                args.Slac = slac.Value;
            }
           
            var shpConditions = from selectedItem in smoothListBoxMainList.SelectedItems.OfType<SmoothListbox.ListItems.StandardListItem>()
                                from item in this.shipmentConditions
                                where item.Conditions[0].ConditionTypeId == selectedItem.ID
                                select item;


            foreach (var item in shpConditions)
            {
                item.Pieces = count;
            }

            args.ShipmentConditions = this.shipmentConditions.ToArray();

            this.FireUpdateShiomentConditions(args);

            if (args.Result == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Could not update shipment conditions", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }

            DisplayConditions();
        }

        private void ApplyDamageInPieceMode()
        {
            if (houseBillItem.TotalPieces == 1)
            {
                UpdateShipmentConditionsEventArgs args = new UpdateShipmentConditionsEventArgs();
                args.HouseBillId = this.houseBillItem.HousebillId;
                args.ScanModes = ScanModes.Piece;

                int[] hawbPieceIds = this.GetHouseBillPiecesIDs();

                args.ShipmentConditions = this.GetUpdateConditionsPieces(hawbPieceIds).ToArray();
                this.FireUpdateShiomentConditions(args);
                DisplayConditions();
                return;
            }


            printModePopup = new CargoMatrix.Utilities.MessageListBox();
            printModePopup.HeaderText = label1.Text;
            printModePopup.HeaderText2 = DamageCaptureResources.Text_PrintMode;
            printModePopup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(printModePopup_ListItemClicked);

            printModePopup.CancelClick += (sender, e) =>
                {
                    printModePopup.Hide();
                };

            printModePopup.AddItem(new SmoothListbox.ListItems.StandardListItem("SELECT ALL", null, 1));
            printModePopup.AddItem(new SmoothListbox.ListItems.StandardListItem("SELECT RANGE", null, 2));
            printModePopup.AddItem(new SmoothListbox.ListItems.StandardListItem("SELECT PIECES", null, 3));

            printModePopup.Show();

        }

        List<ShipmentCondition> GetUpdateConditionsPieces(int[] piecesIDs)
        {
            var shpConditions = (
                                    from item in this.shipmentConditions
                                    where (from selectedItem in smoothListBoxMainList.SelectedItems.OfType<SmoothListbox.ListItems.StandardListItem>()
                                           select selectedItem.ID).Contains(item.Conditions[0].ConditionTypeId) ||
                                         (from id in this.checkedItemsIDS
                                          select id).Contains(item.Conditions[0].ConditionTypeId)
                                    select item.Conditions[0]

                                ).ToArray();

            List<ShipmentCondition> conditionsToSend = new List<ShipmentCondition>();

            foreach (var item in piecesIDs)
            {
                ShipmentCondition shpCond = new ShipmentCondition();
                shpCond.Conditions = shpConditions;
                shpCond.Pieces = 1;
                shpCond.PieceId = item;

                conditionsToSend.Add(shpCond);
            }

            return conditionsToSend;
        }

        void printModePopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var actPopup = listItem as SmoothListbox.ListItems.StandardListItem;
            CustomUtilities.NumericPad numPad;

            UpdateShipmentConditionsEventArgs args = new UpdateShipmentConditionsEventArgs();
            args.HouseBillId = this.houseBillItem.HousebillId;
            args.ScanModes = ScanModes.Piece;

            int[] hawbPieceIds = this.GetHouseBillPiecesIDs();

            switch (actPopup.ID)
            {
                case 1:// select all
                    args.ShipmentConditions = this.GetUpdateConditionsPieces(hawbPieceIds).ToArray();

                    this.FireUpdateShiomentConditions(args);

                    sender.Reset();
                    printModePopup.Hide();
                    DisplayConditions();

                    break;

                case 2: // SELECT RANGE

                    numPad = new CustomUtilities.NumericPad(hawbPieceIds, true);
                    numPad.Title = "Select range of piece numbers to apply damage";


                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        hawbPieceIds = numPad.SelectedIDs.ToArray();

                        args.ShipmentConditions = this.GetUpdateConditionsPieces(hawbPieceIds).ToArray();

                        this.FireUpdateShiomentConditions(args);

                        printModePopup.Hide();
                        DisplayConditions();
                    }
                    break;

                case 3:// select random
                    numPad = new CustomUtilities.NumericPad(hawbPieceIds);
                    numPad.Title = "Select pieces to apply damage";

                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        hawbPieceIds = numPad.SelectedIDs.ToArray();

                        args.ShipmentConditions = this.GetUpdateConditionsPieces(hawbPieceIds).ToArray();

                        this.FireUpdateShiomentConditions(args);

                        printModePopup.Hide();
                        DisplayConditions();
                    }
                    break;

            }
        }

        protected override Control InitializeItem(IShipmentConditionType item)
        {
            return new SmoothListbox.ListItems.TwoLineListItem(item.ConditionTypeName, CargoMatrix.Resources.Skin.Notepad, item.ConditionTypeId, this.GetConditionItemText(0, this.houseBillItem.TotalPieces));
        }

        string GetHeaderLine2Text()
        {
            switch (this.damageCaptureApplyType)
            {
                case DamageCaptureApplyTypeEnum.HouseBill:
                    return string.Format("WT: {0} KGS  PCS: {1} of {2}", this.houseBillItem.Weight, this.houseBillItem.ScannedPieces, this.houseBillItem.TotalPieces);

                case DamageCaptureApplyTypeEnum.MasterBill:
                    return string.Format("WT: {0} KGS  PCS: {1}", this.houseBillItem.Weight, this.houseBillItem.TotalPieces);

                default: return string.Empty;
            }
        }

        string GetConditionItemText(int conditionPieces, int totalPieces)
        {
            switch (this.damageCaptureApplyType)
            {
                case DamageCaptureApplyTypeEnum.HouseBill:
                    return string.Format("Pieces {0} of {1}", conditionPieces, totalPieces);

                default: return string.Empty;
            }



        }

        public int? GetSlacFromUser(int piecesCount, int maxReleseSlac, bool isReadonly)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = "Confirm slac";
            cntMsg.LabelDescription = "Enter the slac";
            cntMsg.LabelReference = string.Format("Number of pieces: {0}", piecesCount);
            cntMsg.PieceCount = maxReleseSlac;
            cntMsg.MinPiecesCount = 1;
            cntMsg.Readonly = isReadonly;
            cntMsg.MinPiecesCount = piecesCount;

            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }
    }
}
