﻿namespace SmartDeviceTest
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();




            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(88, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 20);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.Click += new System.EventHandler(this.button1_Click);

            this.Controls.Add(this.button1);

            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Text = "TestForm";
            this.AutoScroll = true;
        }

        private System.Windows.Forms.Button button1;

        #endregion
    }
}