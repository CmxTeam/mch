﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner
{
    public enum BarcodeTypesEnum
    {
        NA,
        HouseBill,
        MasterBill,
        Uld,
        Door,
        Area,
        Truck,
        User,
        ScreeningArea
    }
}
