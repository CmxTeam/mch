﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using SmartDeviceTest.Scanner.ScannerExtended.Results;



namespace SmartDeviceTest.Scanner.ScannerExtended.Data
{
    public static class DataSource
    {

        static string xmlData = @"<?xml version='1.0' encoding='utf-8' ?>
<barcodes>
  <types>
    <type name='MasterBill' islocation='false'>
        <expression regex='^[0-9]{11}$'>
          <partexpressions>
            <expression name='Carier' regex='\w{3}' order='1' />
            <expression name='ID' regex='\w{8}' order='2' />
          </partexpressions>
        </expression>

        <expression regex='^T999\w+$'>
          <partexpressions>
            <expression name='Carier' regex='T999' order='1' />
            <expression name='ID' regex='\w{8}' order='2' />
          </partexpressions>
        </expression>

        <expression regex='^\w{3}-[0-9]{3}-[0-9]{8}-\w{3}$'>
          <partexpressions>
            <expression name='H' regex='\w{3}-' order='1' />
            <expression name='Carier' regex='[0-9]{3}' order='2' />
            <expression name='-' regex='-' order='3' />
            <expression name='ID' regex='[0-9]{8}' order='4' />
          </partexpressions>
        </expression>
        
      
      
    </type>

    <type name='HouseBill' islocation='false'>
   
      <expression regex='^H?\w{7}\+Y[0-9]{4}\+$'>
        <partexpressions>
          <expression name='H' regex='H' order='1' />
          <expression name='ID' regex='\w{7}' order='2' />
          <expression name='Y' regex='\+Y' order='3' />
          <expression name='PieceID' regex='[0-9]{4}' order='4' />
        </partexpressions>
      </expression>

      <expression regex='^H?[^PMC]\w{7}(\+)*'>
        <partexpressions>
          <expression name='H' regex='H' order='1' />
          <expression name='ID' regex='\w{7}' order='2' />
        </partexpressions>
      </expression>

  

    </type>

    <type name='Area' islocation='true'>
      
        <expression regex='^B[-|*].+'>
          <partexpressions>
            <expression name='prefix' regex='B' order='1' />
            <expression name='delimiter' regex='[-|*]' order='2' />
            <expression name='ID' regex='.+' order='3' />
          </partexpressions>
        </expression>

    </type>

    <type name='Door' islocation='true'>

      <expression regex='^D[-|*].+' prefix='D'>
        <partexpressions>
          <expression name='prefix' regex='D' order='1' />
          <expression name='delimiter' regex='[-|*]' order='2' />
          <expression name='ID' regex='.+' order='3' />
        </partexpressions>
      </expression>

    </type>

    <type name='ScreeningArea' islocation='true'>

      <expression regex='^S[-|*].+'>
        <partexpressions>
          <expression name='prefix' regex='S' order='1' />
          <expression name='delimiter' regex='[-|*]' order='2' />
          <expression name='ID' regex='.+' order='3' />
        </partexpressions>
      </expression>

    </type>


    <type name='Truck' islocation='false'>

      <expression regex='^T[-|*].+'>
        <partexpressions>
          <expression name='prefix' regex='T' order='1' />
          <expression name='delimiter' regex='[-|*]' order='2' />
          <expression name='ID' regex='.+' order='3' />
        </partexpressions>
      </expression>

    </type>

    <type name='Uld' islocation='false'>

      <expression regex='^L[-|*].+'>
        <partexpressions>
          <expression name='prefix' regex='L' order='1' />
          <expression name='delimiter' regex='[-|*]' order='2' />
          <expression name='ID' regex='.+' order='3' />
        </partexpressions>
      </expression>

     <expression regex='^PM\w{8}'>
          <partexpressions>
            <expression name='prefix' regex='PM' order='1' />
            <expression name='ID' regex='\w{8}' order='2' />
          </partexpressions>
        </expression>

    </type>

    <type name='User' islocation='false'>

      <expression regex='^U[-|*].+'>
        <partexpressions>
          <expression name='prefix' regex='U' order='1' />
          <expression name='delimiter' regex='[-|*]' order='2' />
          <expression name='ID' regex='.+' order='3' />
        </partexpressions>
      </expression>

</type>
   </types>
</barcodes>


";

        public static List<ParsingPattern> GetBarcodePatterns()
        {
            var xDocument = XDocument.Parse(xmlData);

            var patterns = from node in xDocument.Elements("barcodes").Elements("types").Elements("type")
                           select node;

            List<ParsingPattern> result = new List<ParsingPattern>();

            foreach (var dataPattern in patterns)
            {
                string patternName = dataPattern.Attribute("name").Value;
                ResultTypeEnum patternType = (ResultTypeEnum)Enum.Parse(typeof(ResultTypeEnum), patternName, true);
                bool isLocation = bool.Parse(dataPattern.Attribute("islocation").Value);

                List<ParsingPattern.PatternValue> values = new List<ParsingPattern.PatternValue>();

                foreach (var mainExpression in dataPattern.Elements("expression"))
                {
                    var elements = from element in mainExpression.Elements("partexpressions").Elements("expression")
                                   select new ParsingElement()
                                   {
                                       Name = element.Attribute("name").Value,
                                       Regex = element.Attribute("regex").Value,
                                       Order = int.Parse(element.Attribute("order").Value)
                                   };

                    string patternRegex = mainExpression.Attribute("regex").Value;
                    var patternValue = new ParsingPattern.PatternValue(patternRegex, elements.ToArray());
                    
                    values.Add(patternValue);
                }

                result.Add(new ParsingPattern(patternType, isLocation, values));
            }

            return result;
        }
    }
}
