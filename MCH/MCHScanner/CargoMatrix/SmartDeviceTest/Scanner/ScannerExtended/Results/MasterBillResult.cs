﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner.ScannerExtended.Results
{
    public class MasterBillResult : ParseResult
    {
        string carier;

        public string Carier
        {
            get { return carier; }
        }

        public MasterBillResult(string barcode, ParsingPattern.PatternValue parsingValue)
            : base(barcode, ResultTypeEnum.MasterBill, parsingValue)
        {

        }

        protected override void EmbedValue(string value, string name)
        {
            switch (name.ToLower())
            {
                case "carier":
                    this.carier = value;
                    break;

                case "id":
                    this.iD = value;
                    break;
            }
        }
    }
}
