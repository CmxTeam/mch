﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SmartDeviceTest.Scanner
{
    public static class BarcodeParser
    {
        public static ScanObject Parse(string barcodeData)
        {
            try
            {
                var match = Regex.Match(barcodeData, @"(\w{7}|H\w{7})(\s)*\+[Y][0-9][0-9][0-9][0-9](\+)*", RegexOptions.IgnoreCase);

                if(match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseHouseBill(trimMatch, barcodeData);
                }
                
                match = Regex.Match(barcodeData, "^B[-|*](.?)*", RegexOptions.IgnoreCase);
            
                if(match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseArea(trimMatch, barcodeData);
                }

                match = Regex.Match(barcodeData, "^D[-|*](.?)*", RegexOptions.IgnoreCase);

                if(match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseDoor(trimMatch, barcodeData);
                }
                
                match = Regex.Match(barcodeData, "^S[-|*](.?)*", RegexOptions.IgnoreCase);

                if (match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseScreeningArea(trimMatch, barcodeData);
                }

                match = Regex.Match(barcodeData, "^T[-|*](.?)*", RegexOptions.IgnoreCase);

                if (match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseTruck(trimMatch, barcodeData);
                }

                match = Regex.Match(barcodeData, "^L[-|*](.?)*", RegexOptions.IgnoreCase);

                if (match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseUld(trimMatch, barcodeData);
                }

                match = Regex.Match(barcodeData, @"^U[-|*]\d+", RegexOptions.IgnoreCase);

                if (match.Success == true)
                {
                    string trimMatch = match.Value.Trim().ToUpper();

                    return BarcodeParser.ParseUser(trimMatch, barcodeData);
                }

                ScanObject result;
                bool masterBillparseResult = BarcodeParser.TryParseMasterBill(barcodeData, out result);

                if (masterBillparseResult == true) return result;

                return new ScanObject { BarcodeType = BarcodeTypesEnum.NA };
            }
            catch
            {
                return new ScanObject { BarcodeType = BarcodeTypesEnum.NA };
            }
        }

        private static ScanObject ParseHouseBill(string housebill, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.HouseBill };

            int yIndex = barcodeData.ToUpper().IndexOf("+Y");

            result.HouseBillNumber = barcodeData.Substring(0, yIndex).Trim();
            result.BarcodeData = barcodeData;

            if (result.HouseBillNumber.Length > 7 &&
               result.HouseBillNumber[0] == 'H')
            {
                result.HouseBillNumber = result.HouseBillNumber.Substring(1);
            }

            try
            {
                result.PieceNumber = int.Parse(housebill.Substring(yIndex + 2, housebill.Length - yIndex - 2 - 1));
            }
            catch
            {

            }

            return result;
        }
    
        private static ScanObject ParseArea(string area, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.Area };
            
            result.Location = area.Substring(2);
            result.BarcodeData = barcodeData;


            return result;
        }

        private static ScanObject ParseDoor(string door, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.Door };
            result.Location = door.Substring(2);
            result.BarcodeData = barcodeData;
            return result;
        }

        private static ScanObject ParseScreeningArea(string screeningArea, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.ScreeningArea };
            result.Location = screeningArea.Substring(2);
            result.BarcodeData = barcodeData;
            return result;
        }

        private static ScanObject ParseTruck(string truck, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.Truck };
            result.Location = truck.Substring(2);
            result.BarcodeData = barcodeData;
            return result;
        }

        private static ScanObject ParseUld(string uld, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.Uld };
            result.Location = uld.Substring(2);
            result.BarcodeData = barcodeData;
            return result;
        }

        private static ScanObject ParseUser(string user, string barcodeData)
        {
            var result = new ScanObject { BarcodeType = BarcodeTypesEnum.User };
            result.Location = user.Substring(2);
            result.BarcodeData = barcodeData;
            return result; 
        }

        private static bool TryParseMasterBill(string barcodeData, out ScanObject scanObject)
        {
            scanObject = null;

            var match = Regex.Match(barcodeData, @"^(T999)\w+", RegexOptions.IgnoreCase);

            if (match.Success == true)
            {
                string trimMatch = match.Value.Trim().ToUpper();
                scanObject = new ScanObject { BarcodeType = BarcodeTypesEnum.MasterBill };
                scanObject.BarcodeData = barcodeData;
                
                if (trimMatch.Length <= 11)
                {
                    scanObject.CarrierNumber = "T999";
                    scanObject.MasterBillNumber = trimMatch.Substring(5);
                    return true;
                }
            }

            match = Regex.Match(barcodeData, "^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])$", RegexOptions.IgnoreCase);

            if (match.Success == true)
            {
                string trimMatch = match.Value.Trim().ToUpper();
                scanObject = new ScanObject { BarcodeType = BarcodeTypesEnum.MasterBill };
                scanObject.BarcodeData = barcodeData;

                if (trimMatch.Length <= 11)
                {
                    scanObject.CarrierNumber = trimMatch.Substring(0, 3);
                    scanObject.MasterBillNumber = trimMatch.Substring(3, trimMatch.Length - 3);
                    return true;
                }
            }


            match = Regex.Match(barcodeData, @"^(\w\w\w-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-\w\w\w)$", RegexOptions.IgnoreCase);
            if (match.Success == true)
            {
                string trimMatch = match.Value.Trim().ToUpper();
                scanObject = new ScanObject { BarcodeType = BarcodeTypesEnum.MasterBill };
                scanObject.BarcodeData = barcodeData;

                if (trimMatch.Length <= 11)
                {
                    scanObject.CarrierNumber = trimMatch.Substring(3, 3);
                    scanObject.MasterBillNumber = trimMatch.Substring(8, 11);
                    return true;
                }
            }

            return false;
        }
                
    }
}
