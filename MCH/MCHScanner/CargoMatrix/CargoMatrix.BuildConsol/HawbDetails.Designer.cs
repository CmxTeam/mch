﻿namespace CargoMatrix.LoadConsol
{
    partial class HawbDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelPieces = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDamage = new CargoMatrix.UI.CMXPictureButton();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelHousebillInfo = new System.Windows.Forms.Panel();
            this.buttonLeft = new CargoMatrix.UI.CMXPictureButton();
            this.buttonRight = new CargoMatrix.UI.CMXPictureButton();
            this.buttonHeader = new System.Windows.Forms.PictureBox();
            this.labelEmpty = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelSlac = new System.Windows.Forms.Label();
            this.panelHousebillInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelWeight
            // 
            this.labelWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelWeight.Location = new System.Drawing.Point(107, 24);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(90, 13);
            // 
            // labelPieces
            // 
            this.labelPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPieces.Location = new System.Drawing.Point(107, 41);
            this.labelPieces.Name = "labelPieces";
            this.labelPieces.Size = new System.Drawing.Size(90, 13);
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(48, 6);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(152, 14);
            this.title.Text = " ";
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(5, 3);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(28, 28);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDetails.Location = new System.Drawing.Point(5, 41);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.BackColor = System.Drawing.SystemColors.Control;
            this.buttonBrowse.Location = new System.Drawing.Point(202, 41);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDamage
            // 
            this.buttonDamage.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDamage.Location = new System.Drawing.Point(202, 3);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.Size = new System.Drawing.Size(32, 32);
            this.buttonDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDamage.TransparentColor = System.Drawing.Color.White;
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(3, 78);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(48, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.Text = "Weight :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(48, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.Text = "Pieces :";
            // 
            // panelHousebillInfo
            // 
            this.panelHousebillInfo.Controls.Add(this.itemPicture);
            this.panelHousebillInfo.Controls.Add(this.panelIndicators);
            this.panelHousebillInfo.Controls.Add(this.label3);
            this.panelHousebillInfo.Controls.Add(this.label2);
            this.panelHousebillInfo.Controls.Add(this.buttonDetails);
            this.panelHousebillInfo.Controls.Add(this.label1);
            this.panelHousebillInfo.Controls.Add(this.buttonBrowse);
            this.panelHousebillInfo.Controls.Add(this.labelWeight);
            this.panelHousebillInfo.Controls.Add(this.buttonDamage);
            this.panelHousebillInfo.Controls.Add(this.labelSlac);
            this.panelHousebillInfo.Controls.Add(this.labelPieces);
            this.panelHousebillInfo.Controls.Add(this.title);
            this.panelHousebillInfo.Location = new System.Drawing.Point(0, 42);
            this.panelHousebillInfo.Name = "panelHousebillInfo";
            this.panelHousebillInfo.Size = new System.Drawing.Size(240, 97);
            // 
            // buttonLeft
            // 
            this.buttonLeft.BackColor = System.Drawing.SystemColors.Control;
            this.buttonLeft.Location = new System.Drawing.Point(0, 0);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(32, 32);
            this.buttonLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonLeft.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonRight
            // 
            this.buttonRight.BackColor = System.Drawing.SystemColors.Control;
            this.buttonRight.Location = new System.Drawing.Point(208, 0);
            this.buttonRight.Name = "buttonRight";
            this.buttonRight.Size = new System.Drawing.Size(32, 32);
            this.buttonRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonRight.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonHeader
            // 
            this.buttonHeader.BackColor = System.Drawing.SystemColors.Control;
            this.buttonHeader.Location = new System.Drawing.Point(32, 0);
            this.buttonHeader.Name = "buttonHeader";
            this.buttonHeader.Size = new System.Drawing.Size(176, 32);
            this.buttonHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonHeader_MouseDown);
            // 
            // labelEmpty
            // 
            this.labelEmpty.Location = new System.Drawing.Point(7, 70);
            this.labelEmpty.Name = "labelEmpty";
            this.labelEmpty.Size = new System.Drawing.Size(227, 60);
            this.labelEmpty.Text = LoadConsolResources.Text_LabelEmpty;
            this.labelEmpty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(48, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.Text = "Slac :";
            // 
            // labelSlac
            // 
            this.labelSlac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelSlac.Location = new System.Drawing.Point(107, 59);
            this.labelSlac.Name = "labelSlac";
            this.labelSlac.Size = new System.Drawing.Size(90, 13);
            InitializeImages();
            // 
            // HawbDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelHousebillInfo);
            this.Controls.Add(this.buttonRight);
            this.Controls.Add(this.buttonHeader);
            this.Controls.Add(this.buttonLeft);
            this.Controls.Add(this.labelEmpty);
            this.Name = "HawbDetails";
            this.Size = new System.Drawing.Size(240, 140);
            this.panelHousebillInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelPieces;
        private CustomUtilities.IndicatorPanel panelIndicators;
        internal CargoMatrix.UI.CMXPictureButton buttonDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        internal CargoMatrix.UI.CMXPictureButton buttonBrowse;
        internal CargoMatrix.UI.CMXPictureButton buttonDamage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelHousebillInfo;
        internal CargoMatrix.UI.CMXPictureButton buttonLeft;
        internal CargoMatrix.UI.CMXPictureButton buttonRight;
        internal System.Windows.Forms.PictureBox buttonHeader;
        private System.Windows.Forms.Label labelEmpty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelSlac;
    }
}
