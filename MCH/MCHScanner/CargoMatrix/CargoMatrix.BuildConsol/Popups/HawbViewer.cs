﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;
using SmoothListbox.ListItems;
using CMXBarcode;

namespace CargoMatrix.LoadConsol
{
    public partial class HawbViewer : CargoMatrix.Utilities.MessageListBox
    {
        //private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.Communication.DTO.IHouseBillItem hawbItem;
        private string filter;
        private string sort;
        private string location;

        public HawbViewer(CargoMatrix.Communication.DTO.IHouseBillItem hawb, string filter, string sort, string location)
        {
            InitializeComponent();
            this.hawbItem = hawb;
            this.filter = filter;
            this.sort = sort;
            this.location = location;
            this.HeaderText = hawb.Reference();
            this.HeaderText2 = LoadConsolResources.Text_HawbViewTitle;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(HawbViewer_ListItemClicked);
            {
                LoadControl();
                this.ShowDialog();
            }
        }

        void HawbViewer_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>)
            {
                if (hawbItem.RemoveMode != CargoMatrix.Communication.DTO.RemoveModes.NA)
                    if (!ShowReturnRemoveDialog())
                        return;

                TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem> lItem = (listItem as TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>);
                CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
                cntMsg.PieceCount = lItem.ItemData.Piece.Length;
                cntMsg.HeaderText = "Confirm count";
                cntMsg.LabelDescription = "Enter number of pieces";
                cntMsg.LabelReference = lItem.ItemData.Location;
                if (DialogResult.OK == cntMsg.ShowDialog())
                {
                    var response = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftCount(hawbItem.HousebillId,
                        ForkLiftViewer.Instance.Mawb.MasterBillId, cntMsg.PieceCount, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Manual, string.Empty, string.Empty, string.Empty, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
                    if (!response.Status.TransactionStatus1)
                        CargoMatrix.UI.CMXMessageBox.Show(response.Status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    else
                        ForkLiftViewer.Instance.ItemsCount = response.Status.TransactionRecords;

                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    sender.Reset();
                    OkEnabled = false;
                }
            }
        }
        private bool ShowReturnRemoveDialog()
        {
            CargoMatrix.Utilities.MessageListBox actPopup;
            Control[] actions = new Control[]
                {   
                    new SmoothListbox.ListItems.StandardListItem("RETURN TO CONSOL",null, 2),
                    new SmoothListbox.ListItems.StandardListItem("REMOVE FROM CONSOL",null, 1) };

            actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = "Select Action To Continue";
            actPopup.HeaderText2 = "Hawb was removed";
            Cursor.Current = Cursors.Default;
            LoadControl();

            if (DialogResult.OK == actPopup.ShowDialog())
            {

                int actionId = (actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID;
                if (actionId == 1)// Remove
                {
                    HousebillLoader.RemoveHawbFromConsol(hawbItem.HousebillId, hawbItem.HousebillNumber, ScanTypes.Manual);
                    this.DialogResult = DialogResult.Cancel;
                    return false;
                }
                if (actionId == 2) // return to Consol
                {
                    bool status = HousebillLoader.ReturnHawbIntoConsol(hawbItem.HousebillId, hawbItem.HousebillNumber);
                    if (status == false)
                        return false;
                }

                actPopup.OkEnabled = false;
                actPopup.ResetSelection();
                return true;
            }
            return false;
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            if (hawbItem.RemoveMode != CargoMatrix.Communication.DTO.RemoveModes.NA)
            {
                if (!ShowReturnRemoveDialog())
                    return;
            }

            List<PieceItem> pieceItems = new List<PieceItem>();
            foreach (var item in smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox>())
            {
                foreach (var pieceId in item.SelectedIds)
                    pieceItems.Add(new PieceItem() { HouseBillId = item.ID, PieceId = pieceId });
            }

            var response = CargoMatrix.Communication.LoadConsol.Instance.ScanPiecesIntoForklift(pieceItems.ToArray<PieceItem>(),
            ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, filter, sort, location, ForkLiftViewer.Instance.Mode == ConsolMode.Load, MOT.Air);
            if (!response.Status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(response.Status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            else
                ForkLiftViewer.Instance.ItemsCount = response.Status.TransactionRecords;

            this.DialogResult = DialogResult.OK;
            Cursor.Current = Cursors.Default;
        }
        //private void removeHousebill()
        //{
        //    //BarcodeEnabled = false;
        //    string msg;
        //    TransactionStatus status = new TransactionStatus() { TransactionStatus1 = true };
        //    var mawb = ForkLiftViewer.Instance.Mawb;
        //    msg = string.Format("This will remove the HB {0} from consol {1}. Do you want to continue?", hawbItem.HousebillNumber, mawb.MasterBillNumber);
        //    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Remove Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
        //    {
        //        CustomUtilities.ScanEnterPopup dropLoc = new CustomUtilities.ScanEnterPopup();
        //        dropLoc.Title = "Scan Drop Location";
        //        dropLoc.TextLabel = "Return Location to HB " + hawbItem.HousebillNumber;
        //        if (DialogResult.OK == dropLoc.ShowDialog())
        //        {
        //            ScanObject scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(dropLoc.ScannedText);
        //            /// get scan Ids from backend 
        //            var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(ForkLiftViewer.getScanItem(scanObj), mawb.TaskId);
        //            if (scanItem.Transaction.TransactionStatus1 == false)
        //            {
        //                scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
        //            }

        //            if (CMXBarcode.BarcodeTypes.Area == scanObj.BarcodeType || CMXBarcode.BarcodeTypes.Door == scanObj.BarcodeType)
        //            {
        //                {
        //                    status = CargoMatrix.Communication.LoadConsol.Instance.ReturnAllPiecesIntoLocation(mawb.TaskId, scanItem.LocationId);
        //                    //if (!status.TransactionStatus1)
        //                    //    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //                }
        //            }
        //            else
        //                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidLocationBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //        }
        //    }

        //    if (!status.TransactionStatus1)
        //        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

        //}

        /*
        private void HandleRemovedHousebill()
        {
            //BarcodeEnabled = false;
            var mawb = ForkLiftViewer.Instance.Mawb;
            TransactionStatus status = new TransactionStatus() { TransactionStatus1 = true };
            string msg;
            CargoMatrix.Utilities.MessageListBox actPopup;
            List<Control> actions = new List<Control>
                {   
                    new SmoothListbox.ListItems.StandardListItem("RETURN",null, 2),
                    new SmoothListbox.ListItems.StandardListItem("REMOVE",null, 1) };

            actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = "Select Action To Continue";
            actPopup.HeaderText2 = "Hawb was removed";
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 2: // RETURN
                        msg = string.Format("This will return the HB {0} into consol {1}. Do you want to continue?", hawbItem.HousebillNumber, mawb.MasterBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Return Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            foreach (var item in smoothListBoxBase1.Items)
                                foreach (int pieceid in (item as CustomListItems.ComboBox).SelectedIds)
                                {
                                    var tempStatus = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawbItem.HousebillId, pieceid, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece, ScanTypes.Manual);
                                    if (tempStatus.TransactionStatus1 == false)
                                        status = tempStatus;

                                }
                            
                            if (!status.TransactionStatus1)
                                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        }

                        break; // REMOVE
                    case 1:
                        msg = string.Format("This will remove the HB {0} from consol {1}. Do you want to continue?", hawbItem.HousebillNumber, mawb.MasterBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Remove Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            CustomUtilities.ScanEnterPopup dropLoc = new CustomUtilities.ScanEnterPopup();
                            dropLoc.Title = "Scan Drop Location";
                            dropLoc.TextLabel = "Return Location to HB " + hawbItem.HousebillNumber;
                            if (DialogResult.OK == dropLoc.ShowDialog())
                            {
                                ScanItem scitem = CargoMatrix.Communication.LoadConsol.Instance.ParseBarcode(dropLoc.ScannedText, mawb.TaskId);
                                if (BarcodeTypes.Area == scitem.BarcodeType || BarcodeTypes.Door == scitem.BarcodeType)
                                {
                                    //status = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawb.HouseBillId, hawb.TotalPieces, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Count, scanType);
                                    //if (!status.TransactionStatus1)
                                    //    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                                    //else
                                    {
                                        status = CargoMatrix.Communication.LoadConsol.Instance.ReturnPieceIntoLocation(hawbItem.HousebillId, mawb.TaskId, hawbItem.TotalPieces, scitem.LocationId, CargoMatrix.Communication.WSLoadConsol.ScanModes.Count, ScanTypes.Manual);
                                        if (!status.TransactionStatus1)
                                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                                    }
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidLocationBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            }
                        }
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
            if (!status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        }
        */

        public void LoadControl()
        {
            //this.headerItem.Counter = ForkLiftViewer.Instance.ItemsCount;
            Cursor.Current = Cursors.WaitCursor;

            smoothListBoxBase1.RemoveAll();
            //List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
            if (hawbItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)//? CargoMatrix.Resources.Skin.countMode : CargoMatrix.Resources.Skin.PieceMode)
            {
                foreach (var loc in CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hawbItem.HousebillId,ForkLiftViewer.Instance.Mawb.MasterBillId, CargoMatrix.Communication.ScannerUtilityWS.MOT.Air))
                {

                    List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                    foreach (var pc in loc.Piece)

                        items.Add(new CustomListItems.ComboBoxItemData("Piece " + pc.PieceNumber, pc.Location, pc.PieceId, true));

                    CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawbItem.HousebillId, loc.Location, string.Format("Pieces: {0} of {1}", loc.Piece.Length, hawbItem.TotalPieces), CargoMatrix.Resources.Skin.PieceMode, items);
                    combo.IsSelectable = true;
                    combo.ReadOnly = true;
                    combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                    smoothListBoxBase1.AddItem(combo);
                }
            }
            else
            {
                foreach (var loc in CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hawbItem.HousebillId,ForkLiftViewer.Instance.Mawb.MasterBillId, CargoMatrix.Communication.ScannerUtilityWS.MOT.Air))
                {
                    TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem> item = new TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>(loc.Location, CargoMatrix.Resources.Skin.countMode, loc.LocationId, string.Format("Pieces: {0} of {1}", loc.Piece.Length, hawbItem.TotalPieces), loc);
                    smoothListBoxBase1.AddItem(item);
                }

            }
            Cursor.Current = Cursors.Default;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if ((sender as CustomListItems.ComboBox).SubItemSelected)
                OkEnabled = true;
            else
                OkEnabled = false;
        }
    }
}
