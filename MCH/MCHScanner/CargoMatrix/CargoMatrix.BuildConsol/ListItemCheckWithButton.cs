﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.LoadConsol
{
    public class ListItemCheckWithButton<T> : SmoothListbox.ListItems.ListItemWithButton
    {
        public T ItemData { get; set; }
        public ListItemCheckWithButton(T data, string title, string line2)
            : base(title, line2, CargoMatrix.Resources.Icons.unselected, Resources.Skin.magnify_btn, Resources.Skin.magnify_btn_over)
        {
            ItemData = data;
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.Location = new System.Drawing.Point(8, 8);
            this.pictureBoxDelete.Left = 200;
            this.Scale(new System.Drawing.SizeF(this.CurrentAutoScaleDimensions.Width / 96F, this.CurrentAutoScaleDimensions.Height /96F));
            this.pictureBoxDelete.BringToFront();
        }
        public override void SelectedChanged(bool isSelected)
        {
            if (isSelected)
                itemPicture.Image = CargoMatrix.Resources.Icons.selected;
            else
                itemPicture.Image = CargoMatrix.Resources.Icons.unselected;
            this.Focus(isSelected);
            m_selected = isSelected;
        }
    }
}
