﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;

namespace CargoMatrix.Inventory
{
    public partial class LocationViewer : CustomListItems.MessageListBoxAsync<CargoMatrix.Communication.WSPieceScan.InventoryLocationGroup>
    {
        public LocationViewer(string location)
        {
            Cursor.Current = Cursors.WaitCursor;
            InitializeComponent();
            this.HeaderText = "Inventory Report";
            this.HeaderText2 = string.IsNullOrEmpty(location) ? "All Locations" : "Location " + location;
            this.smoothListBoxBase1.IsSelectable = false;
            ReloadItemsAsync(CargoMatrix.Communication.HostPlusIncomming.Instance.GetInventoryLocationReport(location));
            Cursor.Current = Cursors.Default;
        }
        public LocationViewer()
        {
            InitializeComponent();
            this.HeaderText = "Current Inventory Report";
            this.HeaderText2 = string.Format("Inventory for {0} at {1}", CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserName, DateTime.Today.ToString("MM/dd/yy"));
            this.smoothListBoxBase1.IsSelectable = false;
            ReloadItemsAsync(CargoMatrix.Communication.HostPlusIncomming.Instance.GetCurrentInventoryReport(Forklift.Instance.TaskID));
        }
        public LocationViewer(bool forklift)
        {
            InitializeComponent();
            this.HeaderText = "Current Inventory Report";
            this.HeaderText2 = string.Format("Inventory for {0} at {1}", CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserName, DateTime.Today.ToString("MM/dd/yy"));
            this.smoothListBoxBase1.IsSelectable = false;
            var item = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkliftInventoryReport(Forklift.Instance.TaskID);
            if (item.Length < 1)
                return;
            
            var hawbs = from hb in item[0].InventoryLocationGroupItems
                        let time = hb.LastScanTimestamp.HasValue ? hb.LastScanTimestamp.Value.ToString("MMM/dd HH:mm") : "N/A"
                        select new CustomListItems.ComboBoxItemData(string.Format("{0}-{1}-{2} ", hb.HouseBillOrigin, hb.HouseBillNo, hb.HouseBillDestination),
                                                                    string.Format("Pcs:{0} of {1} On {2}", hb.ScannedPieces, hb.TotalPieces, time), -1, true);

            int pieces = item[0].InventoryLocationGroupItems.Sum<CargoMatrix.Communication.WSPieceScan.InventoryLocationGroupItem>(i => int.Parse(i.ScannedPieces));

            CustomListItems.ComboBox combo = new CustomListItems.ComboBox(-1, Forklift.Instance.ScannedLocation, string.Format("HBs:{0} Pcs:{1}", hawbs.Count(), pieces), CargoMatrix.Resources.Skin.PieceMode, hawbs.ToList<CustomListItems.ComboBoxItemData>());
            combo.IsSelectable = false;
            combo.ReadOnly = true;
            this.AddItem(combo);
        }
        protected override Control InitializeItem(CargoMatrix.Communication.WSPieceScan.InventoryLocationGroup item)
        {
            var hawbs = from hb in item.InventoryLocationGroupItems
                        let time = hb.LastScanTimestamp.HasValue ? hb.LastScanTimestamp.Value.ToString("MMM/dd HH:mm") : "N/A"
                        select new CustomListItems.ComboBoxItemData(string.Format("{0}-{1}-{2} ", hb.HouseBillOrigin, hb.HouseBillNo, hb.HouseBillDestination),
                                                                    string.Format("Pcs:{0} of {1} On {2}", hb.ScannedPieces, hb.TotalPieces, time), -1, true);

            int pieces = item.InventoryLocationGroupItems.Sum<CargoMatrix.Communication.WSPieceScan.InventoryLocationGroupItem>(i => int.Parse(i.ScannedPieces));

            CustomListItems.ComboBox combo = new CustomListItems.ComboBox(-1, item.LocationName, string.Format("HBs:{0} Pcs:{1}", hawbs.Count(), pieces), CargoMatrix.Resources.Skin.PieceMode, hawbs.ToList<CustomListItems.ComboBoxItemData>());
            combo.IsSelectable = false;
            combo.ReadOnly = true;
            return combo;
        }
    }
}
