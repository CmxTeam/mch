﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using System.Windows.Forms;
using CargoMatrix.UI;
using CMXBarcode;
namespace CargoMatrix.Inventory
{
    class InventoryTasks : CargoUtilities.SmoothListBoxOptions
    {
        public InventoryTasks()
            : base()
        {
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(TaskList_ListItemClicked);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(TaskList_MenuItemClicked);
            this.LoadOptionsMenu += new EventHandler(TaskList_LoadOptionsMenu);
            this.smoothListBoxMainList.labelEmpty.Text = string.Empty;
            BarcodeReadNotify += new BarcodeReadNotifyHandler(TaskList_BarcodeReadNotify);
        }

        void TaskList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.REFRESH)
                {
                    LoadControl();
                }
            }
        }

        void TaskList_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        void TaskList_BarcodeReadNotify(string barcodeData)
        {
            var scan = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scan.BarcodeType)
            {
                case BarcodeTypes.Area:
                case BarcodeTypes.Door:
                    bool taskFoundFlag = false;
                    foreach (var item in smoothListBoxMainList.Items.OfType<StandardListItem>())
                        if (item.title.Text.Equals(scan.Location, StringComparison.OrdinalIgnoreCase))
                        {
                            Forklift.Instance.SetPreAssignedLocation(item.title.Text, item.ID);
                            CMXAnimationmanager.DisplayForm(new HawbInventory());
                            taskFoundFlag = true;
                            break;
                        }

                    if (!taskFoundFlag)
                    {
                        CMXMessageBox.Show("There is no assigned task for scanned location", "Task Not Assigned", CMXMessageBoxIcon.Hand);
                        BarcodeEnabled = true;
                    }
                    break;
                default:
                    CMXMessageBox.Show("Invalid Barcode Scan", "Invalid Barcode", CMXMessageBoxIcon.Hand);
                    BarcodeEnabled = true;
                    break;
            }
        }

        public override void LoadControl()
        {
            if (!Forklift.Isloaded)
            {
                this.TitleText = "CargoInventory";
                this.smoothListBoxMainList.labelEmpty.Text = "Unable to Load\r\nTry to Refresh ...";
                return;
            }
            else
            {
                this.smoothListBoxMainList.RemoveAll();
                this.smoothListBoxMainList.AddItem(new SmoothListbox.ListItems.StandardListItem("New Inventory", Resources.Icons.Notepad_Add, 1));
                var tasks = CargoMatrix.Communication.Inventory.Instance.GetInventoryTasks();
                foreach (var item in tasks)
                {
                    this.smoothListBoxMainList.AddItem(new SmoothListbox.ListItems.StandardListItem(item.Location, null, item.TaskId));
                }
                this.TitleText = string.Format("CargoInventory - ({0})", this.smoothListBoxMainList.ItemsCount - 1);
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        void TaskList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {
            StandardListItem taskItem = listItem as StandardListItem;
            if (listItem == null)
            {
                smoothListBoxMainList.Reset();
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            if (taskItem.ID == 1)
            {
                Forklift.Instance.ResetPreAssignedLocation();
            }
            else
            {
                Forklift.Instance.SetPreAssignedLocation(taskItem.title.Text, taskItem.ID);
            }
            CMXAnimationmanager.DisplayForm(new HawbInventory());
            smoothListBoxMainList.Reset();
        }
    }
}
