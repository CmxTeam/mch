﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.InventoryWS;
using CustomListItems.RenderItems;
using CustomUtilities;
using System.Windows.Forms;

namespace CargoMatrix.Inventory
{
    class ULDScannedList : CargoMatrix.Utilities.MessageListBox
    {
        public ULDScannedList()
        {
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(ULDScannedList_LoadListEvent);
            this.HeaderText = "Scanned ULD's";
            this.HeaderText2 = "Select Ulds to confirm Location";
        }

        void ULDScannedList_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.RemoveAllItems();
            var ulds = Communication.Inventory.Instance.GetForkliftULds(Forklift.Instance.ForkliftID, Forklift.Instance.TaskID);
            Forklift.Instance.ItemsCount = ulds.Length;

            foreach (var uld in ulds)
            {
                var listItem = new ListItemWithButton<UldObject>(uld.UldNo, uld.UldType, CargoMatrix.Resources.Icons.ULD, CargoMatrix.Resources.Skin.cc_trash, CargoMatrix.Resources.Skin.cc_trash_over, uld);
                listItem.OnButtonClick += new EventHandler(listItem_OnButtonClick);
                this.AddItem(listItem);
            }
            Cursor.Current = Cursors.Default;
            if (ulds.Length == 0)
                DialogResult = DialogResult.Cancel;
        }

        void listItem_OnButtonClick(object sender, EventArgs e)
        {
            var uld = sender as ListItemWithButton<UldObject>;
            if (uld == null)
                return;
            string message = string.Format("Are you sure you want to reomve ULD {0}?", uld.ItemData.UldNo);
            if (DialogResult.OK == UI.CMXMessageBox.Show(message, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                var status = Communication.Inventory.Instance.RemoveUldFromForklift(uld.ItemData.Id, Forklift.Instance.ForkliftID, Forklift.Instance.TaskID);
                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                else
                    Forklift.Instance.ItemsCount = status.TransactionRecords;

                ULDScannedList_LoadListEvent(null);
            }
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            var ids = from item in this.SelectedItems.OfType<ListItemWithButton<UldObject>>()
                      select item.ItemData.Id;
            ScanEnterPopup locPopup = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            locPopup.Title = "Scan/Enter Drop Location";
            if (locPopup.ShowDialog() == DialogResult.OK)
            {
                var status = Communication.Inventory.Instance.DropUldsIntoLocation(ids.ToArray<int>(), Forklift.Instance.ForkliftID, locPopup.Text, Forklift.Instance.TaskID);
                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                Forklift.Instance.ItemsCount = status.TransactionRecords;
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}
