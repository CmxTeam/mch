﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CargoMatrix.CargoTender
{
    public partial class SignatureCaptureForm : Form
    {
        private CargoMatrix.UI.CMXPictureButton btnCancel;
        private CargoMatrix.UI.CMXPictureButton btnClear;
        private CargoMatrix.UI.CMXPictureButton btnOK;
       
        string header = "Some Text";

        public string Header
        {
            get { return header; }
            set { header = value; }
        }

        /// <summary>
        /// Gets the signature as a bitmap
        /// </summary>
        public Bitmap SignatureAsBitmap
        {
            get
            {
                return this.signature.ToBitmap();
            }
        }

        /// <summary>
        /// Gets signature as bitmap byte array 
        /// </summary>
        public byte[] SignatureAsBytes
        {
            get
            {
                Bitmap bmp = this.SignatureAsBitmap;

                byte[] signature;
                using (MemoryStream ms = new MemoryStream())
                {
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    signature = ms.ToArray();
                    ms.Flush();
                }

                return signature;
            }
        }
        
        public SignatureCaptureForm()
        {
            InitializeComponent();
        }

        private void InitializeFields()
        {
            this.btnCancel = new CargoMatrix.UI.CMXPictureButton();
            this.btnClear = new CargoMatrix.UI.CMXPictureButton();
            this.btnOK = new CargoMatrix.UI.CMXPictureButton();

            this.headerItem.ButtonImage = CargoMatrix.Resources.Skin.Airplane24x24;
            this.headerItem.BringToFront();
            this.headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            this.pcxHeader.SendToBack();


            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = global::Resources.Graphics.Skin.nav_cancel;  //CargoMatrix.Resources.Skin.button_default;
            this.btnCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over;
            this.btnCancel.Location = new System.Drawing.Point(166, 195);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(52, 37);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
           
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Image = CargoMatrix.Resources.Skin.eraser_btn; //GraphicsResources.Skin.nav_cancel; //CargoMatrix.Resources.Skin.button_default; /;
            this.btnClear.PressedImage = CargoMatrix.Resources.Skin.eraser_button_over;
            this.btnClear.Location = new System.Drawing.Point(90, 195);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(52, 37);
            this.btnClear.TabIndex = 4;
            this.btnClear.Click += new EventHandler(btnClear_Click);
           
            // 
            // btnOK
            //
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Image = global::Resources.Graphics.Skin.nav_ok; //CargoMatrix.Resources.Skin.button_default; 
            this.btnOK.PressedImage = global::Resources.Graphics.Skin.nav_ok_over;
            this.btnOK.Location = new System.Drawing.Point(17, 195);
            this.btnOK.Name = "buttonOk";
            this.btnOK.Size = new System.Drawing.Size(52, 37);
            this.btnOK.TabIndex = 6;
            this.btnOK.Click += new EventHandler(btnOK_Click);

            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnClear);
            this.pnlMain.Controls.Add(this.btnOK);

            this.btnCancel.BringToFront();
            this.btnClear.BringToFront();
            this.btnOK.BringToFront();
            this.pcxHeader.BringToFront();
        }

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            if (this.headerItem.Counter <= 0) return;

            CommonMethods.ShowPlane();
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            byte[] signature = this.signature.GetSignatureEx();

            if (signature.Length <= 4)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Signature is required.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Message, MessageBoxButtons.OK, DialogResult.OK);

                return;
            }

            DialogResult result = CargoMatrix.UI.CMXMessageBox.Show("Confirm the signature is valid.", "Signature confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, DialogResult.Yes);

            if (result == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        void btnClear_Click(object sender, EventArgs e)
        {
            this.signature.Clear();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void pcxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.Header,
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(this.pnlMain.Left - 1, this.pnlMain.Top - 1, this.pnlMain.Width + 1, this.pnlMain.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }

        }

    }
}