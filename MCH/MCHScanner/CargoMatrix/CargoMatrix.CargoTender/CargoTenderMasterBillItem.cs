﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTender;

namespace CargoMatrix.CargoTender
{
    public partial class CargoTenderMasterBillItem : CustomListItems.ExpandableListItem
    {
        ICargTenderMasterBillItem masterBill;
        public event EventHandler DamageCaptureClick;

        public ICargTenderMasterBillItem MasterBill
        {
            get { return masterBill; }
            set 
            { 
                masterBill = value;
                this.Display();
            }
        }

        public CargoTenderMasterBillItem()
        {
            InitializeComponent();
        }

        private void Display()
        {
            this.Logo = CargoMatrix.Resources.Skin.Airplane24x24;

            this.title.Text = "001-123456789-001";
            this.labelLine2.Text = "24";
            this.labelLine3.Text = "2";
            this.labelLine4.Text = "24";
            this.labelLine5.Text = "157 KGS";
            this.labelLine6.Text = "0632";

            this.panelIndicators.Flags = 3;
        }


        protected override bool ButtonEnterValidation()
        {
            return false;
        }

        void btnDamageCapture_Click(object sender, System.EventArgs e)
        {
            var damageCapture = this.DamageCaptureClick;

            if (damageCapture != null)
            {
                damageCapture(this, EventArgs.Empty);
            }
        }
    }
}
