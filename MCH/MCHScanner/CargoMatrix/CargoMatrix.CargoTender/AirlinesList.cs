﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTender;
using CustomListItems;
using CargoMatrix.UI;

namespace CargoMatrix.CargoTender
{
    public partial class AirlinesList : SmoothListbox.SmoothListBoxAsync<IAirline>
    {
        int listItemHeight;

        #region Constructors
        public AirlinesList()
        {
            InitializeComponent();

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(AirlinesList_ListItemClicked);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(AirlinesList_MenuItemClicked);
            this.LoadOptionsMenu += new EventHandler(AirlinesList_LoadOptionsMenu);

            this.SetMainListHeaderText(0);
        } 
        #endregion

        #region Handlers
        void AirlinesList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        void AirlinesList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;

                default: return;
            }
        }

        void AirlinesList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var item = (SmoothListbox.ListItems.TwoLineListItem<IAirline>)listItem;

            if (item == null) return;

            Cursor.Current = Cursors.WaitCursor;

            //CMXAnimationmanager.DisplayForm(new MasterbillList(item.ItemData));
        }

        void txtFilter_TextChanged(object sender, System.EventArgs e)
        {
            string filterText = this.txtFilter.Text.TrimEnd(new char[] { ' ' });

            foreach (SmoothListbox.ListItems.TwoLineListItem<IAirline> item in smoothListBoxMainList.Items)
            {
                if (item.ItemData.Name.Contains(filterText))
                {
                    if (item.Height == 0)
                    {
                        item.Height = listItemHeight;
                    }
                }
                else
                {
                    if (item.IsSelected == true)
                    {
                        smoothListBoxMainList.Reset();
                    }

                    smoothListBoxMainList.Reset();
                    item.Height = 0;
                }
            }
            this.smoothListBoxMainList.itemsPanel.Top = 0;
            LayoutItems();
        }

        #endregion
        
        #region Overrides
        protected override Control InitializeItem(IAirline airline)
        {
            var listItem = new SmoothListbox.ListItems.TwoLineListItem<IAirline>(airline.Name, null, string.Empty, airline);
            listItemHeight = listItem.Height;

            return listItem;
        }

        public override void LoadControl()
        {

        } 
        #endregion

        #region Additional Methods
        private void SetMainListHeaderText(int listItemsCount)
        {
            this.TitleText = string.Format("CargoTender ({0})", listItemsCount);
        } 
        #endregion
    }
}
