﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.CargoTender
{
    public partial class UldContent : CargoMatrix.Utilities.MessageListBox
    {
        IULD uld;

        public UldContent(IULD uld)
        {
            this.uld = uld;

            InitializeComponent();

            this.HeaderText = "ULD Content";
            this.HeaderText2 = uld.ULDNo;

            this.MultiSelectListEnabled = true;
            this.OneTouchSelection = false;
            this.OkEnabled = false;

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(UldContent_LoadListEvent);
        }

        void UldContent_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent();
            this.OkEnabled = false;
        }

        private void PopulateContent()
        {

        }
    }
}