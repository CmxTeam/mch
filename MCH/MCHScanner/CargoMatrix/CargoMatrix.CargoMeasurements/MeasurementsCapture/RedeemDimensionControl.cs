﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoMeasurements;
using CargoMatrix.Communication.ScannerUtilityWS;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.DTO;


namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    public partial class RedeemDimensionControl : UserControl
    {
        MetricSystemTypeEnum metricSystemType;
        PackageType packageType;
        DimensioningMeasurementDetails measurementDetails;

        public event EventHandler DimensionsChanged;
        public event EventHandler<PackageEventAgrs> PackageScanned;

        #region Properties
        public PackageType PackageType
        {
            get { return packageType; }
            set
            {
                packageType = value;
                this.lblPkgType.Text = packageType.name;
            }
        }

        public string PackageRefernce
        {
            get
            {
                return this.txtPkgRef.Text;
            }
            set
            {
                this.txtPkgRef.Text = value;
            }
        }

        public bool HasCompleteMeasurements
        {
            get
            {
                if (this.measurementDetails == null) return false;

                if (this.measurementDetails.Measurement == null) return false;

                if (string.IsNullOrEmpty(this.txtPkgRef.Text) == true) return false;

                if (this.packageType == null) return false;

                try
                {
                    if (decimal.Parse(this.txtHeight.Text) <= 0) return false;

                    if (decimal.Parse(this.txtLength.Text) <= 0) return false;

                    if (decimal.Parse(this.txtWeight.Text) <= 0) return false;

                    if (decimal.Parse(this.txtWidth.Text) <= 0) return false;
                }
                catch
                {
                    return false;
                }

                return true;
            }
        } 
        #endregion

        #region Constructors
        public RedeemDimensionControl()
        {
            InitializeComponent();
            this.metricSystemType = MettlerMeasurements.DefaultMetricSystem;

            this.AttachEvents();
        } 
        #endregion

        #region Events
        void txtHeight_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.measurementDetails == null)
                {
                    this.measurementDetails = new DimensioningMeasurementDetails();
                }

                if (this.measurementDetails.Measurement == null)
                {
                    this.measurementDetails.Measurement = MettlerMeasurements.CreateFromSystemType(this.metricSystemType, MeasurementsOriginEnum.Manual);
                }
                else
                {
                    this.measurementDetails.Measurement.Origin = MeasurementsOriginEnum.Manual;
                }
            }
            catch { }
            

            this.CollectTextDimensions();

            this.FireDimansionsChanged();
        }
        
        #endregion

        public void UpdateDeviceInformation(string status)
        {
            this.lblDevice.Text = CommonMethods.CurrentDevice != null ? CommonMethods.CurrentDevice.Name : string.Empty;

            this.lblStatus.Text = status;
        }

        public void UpdateDeviceInformation()
        {
            this.UpdateDeviceInformation(string.Empty);
        }

        public void SetStatusMessage(string text, Color color)
        {
            this.lblStatus.Text = text;
            this.lblStatus.ForeColor = color;
        }

        public void SetErrorStatusMessage(string text)
        {
            this.SetStatusMessage(text, Color.Red);
        }

        public void SetInfoStatusMessage(string text)
        {
            this.SetStatusMessage(text, Color.Green);
        }

        public void DisplayMesurement(MettlerMeasurements measurement, bool overridePackageID)
        {
            this.DetachEvents();

            this.DisplayMesurementRaw(measurement, overridePackageID);

            this.AttachEvents();
        }

        public void ChangeMetricSystem(MetricSystemTypeEnum metricSystemType)
        {
            this.DetachEvents();

            this.metricSystemType = metricSystemType;

            if (this.measurementDetails == null)
            {
                var weightUnit = MettlerMeasurements.GetWeightMeasurementUnit(metricSystemType);
                var lengthUnit = MettlerMeasurements.GetLengthMeasurementUnit(metricSystemType);

                this.DisplayUnits(weightUnit, lengthUnit);

                this.AttachEvents();

                return;
            }

            if (this.measurementDetails.Measurement != null &&
                this.measurementDetails.Measurement.MetricSystemType != metricSystemType)
            {
                this.measurementDetails.Measurement = this.measurementDetails.Measurement.ConvertToMetricSystem(metricSystemType);
            }
            else
            {
                if (this.measurementDetails.Measurement == null)
                {
                    this.measurementDetails.Measurement = MettlerMeasurements.CreateFromSystemType(this.metricSystemType, MeasurementsOriginEnum.Manual);
                }
            }

            this.DisplayMesurementRaw(this.measurementDetails.Measurement, false);

            this.AttachEvents();
        }

        public DimensioningMeasurementDetails GetCompleteMeasurements()
        {
            if (this.HasCompleteMeasurements == false) return null;

            this.CollectTextDimensions();

            this.measurementDetails.PackageType = this.packageType;

            return this.measurementDetails;
        }

        private void CollectTextDimensions()
        {
            this.measurementDetails.Measurement.Width = string.IsNullOrEmpty(this.txtWidth.Text) == true ? 0.0d : double.Parse(this.txtWidth.Text);
            this.measurementDetails.Measurement.Length = string.IsNullOrEmpty(this.txtLength.Text) == true ? 0.0d : double.Parse(this.txtLength.Text);
            this.measurementDetails.Measurement.Height = string.IsNullOrEmpty(this.txtHeight.Text) == true ? 0.0d : double.Parse(this.txtHeight.Text);
            this.measurementDetails.Measurement.Weight = string.IsNullOrEmpty(this.txtWeight.Text) == true ? 0.0d : double.Parse(this.txtWeight.Text);
        }

        public bool ParsePackegeReference(string data)
        {
            DimensioningMeasurementDetails details;

            bool result = CommonMethods.ParsePackegeReference(data, out details);

            if (result == false)
            {
                this.FirePackageScanned();
                return false;
            }

            this.PackageRefernce = details.PackageNumber;

            this.SetHeaderText(details);

            MettlerMeasurements measurement = null;
            if (this.measurementDetails != null)
            {
                measurement = this.measurementDetails.Measurement;

                if (measurement == null)
                {
                    measurement = MettlerMeasurements.CreateFromSystemType(this.metricSystemType,MeasurementsOriginEnum.Manual);
                }
            }

            this.measurementDetails = details;
            this.measurementDetails.Measurement = measurement;
            this.measurementDetails.PackageType = this.PackageType;

            this.FirePackageScanned(details);
            return true;
        }

        #region Additional Methods
        private void DisplayMesurementRaw(MettlerMeasurements measurement, bool overridePackageID)
        {
            if (this.measurementDetails == null)
            {
                this.measurementDetails = new DimensioningMeasurementDetails();
                this.measurementDetails.Measurement = measurement;
            }
            else
            {
                this.measurementDetails.Measurement = measurement;
            }

            if (overridePackageID == true)
            {
                this.txtPkgRef.Text = measurement.PackageID;
            }

            if (measurement.Width != 0.0)
            {
                this.txtWidth.Text = string.Format("{0:N2}", measurement.Width);
            }
            else
            {
                this.txtWidth.Text = string.Empty;
            }

            if (measurement.Length != 0.0)
            {
                this.txtLength.Text = string.Format("{0:N2}", measurement.Length);
            }
            else
            {
                this.txtLength.Text = string.Empty;
            }

            if (measurement.Height != 0.0)
            {
                this.txtHeight.Text = string.Format("{0:N2}", measurement.Height);
            }
            else
            {
                this.txtHeight.Text = string.Empty;
            }

            if (measurement.Weight != 0.0)
            {
                this.txtWeight.Text = string.Format("{0:N2}", measurement.Weight);
            }
            else
            {
                this.txtWeight.Text = string.Empty;
            }

            this.DisplayUnits(measurement);
        }

        private void DisplayUnits(MesurementsUnitEnum weghtUnit, MesurementsUnitEnum lengthUnit)
        {
            string mesuresWeightSign = weghtUnit.ToString().ToLower();
            string mesuresLengthSign = lengthUnit.ToString().ToLower();

            this.lblHeightSystem.Text = mesuresLengthSign;
            this.lblWeightSystem.Text = mesuresWeightSign;
        }

        private void DisplayUnits(MettlerMeasurements measurement)
        {
            this.DisplayUnits(measurement.WeightMesurementsUnit, measurement.LengthMesurementsUnit);
        }

        private void AttachEvents()
        {
            this.txtHeight.TextChanged += new EventHandler(txtHeight_TextChanged);
            this.txtLength.TextChanged += new EventHandler(txtHeight_TextChanged);
            this.txtWeight.TextChanged += new EventHandler(txtHeight_TextChanged);
            this.txtWidth.TextChanged += new EventHandler(txtHeight_TextChanged);
        }

        private void DetachEvents()
        {
            this.txtHeight.TextChanged -= new EventHandler(txtHeight_TextChanged);
            this.txtLength.TextChanged -= new EventHandler(txtHeight_TextChanged);
            this.txtWeight.TextChanged -= new EventHandler(txtHeight_TextChanged);
            this.txtWidth.TextChanged -= new EventHandler(txtHeight_TextChanged);
        }

        private void SetHeaderText(DimensioningMeasurementDetails details)
        {
            this.label1.Text = CommonMethods.GetHeaderText(details);

            this.label1.Refresh();
        }

        private void FireDimansionsChanged()
        {
            var dimensionsChanged = System.Threading.Interlocked.CompareExchange<EventHandler>(ref this.DimensionsChanged, null, null);

            if (dimensionsChanged != null)
            {
                dimensionsChanged(this, EventArgs.Empty);
            }
        }


        private void FirePackageScanned(PackageEventAgrs args)
        {
            var packageScanned = System.Threading.Interlocked.CompareExchange<EventHandler<PackageEventAgrs>>(ref this.PackageScanned, null, null);

            if (packageScanned != null)
            {
                packageScanned(this, args);
            }
        }

        private void FirePackageScanned(DimensioningMeasurementDetails details)
        {
            FirePackageScanned(new PackageEventAgrs() { DimensioningDetails = details, Success = true });
        }

        private void FirePackageScanned()
        {
            FirePackageScanned(new PackageEventAgrs() { DimensioningDetails = null, Success = false });
        }
        #endregion

    }
}

