﻿using System.Windows.Forms;
using CargoMatrix.Communication.CargoDischarge;
using CMXExtensions;

namespace CargoMatrix.FreightDischarge
{
    public partial class DischargeItemDisplayControl : UserControl
    {
        private IDischargeHouseBillItem houseBillItem;

        public IDischargeHouseBillItem HouseBillItem
        {
            get { return houseBillItem; }
            set { houseBillItem = value; }
        }
       
        public DischargeItemDisplayControl()
        {
            InitializeComponent();
        }
        
        public void DisplayHousebill(IDischargeHouseBillItem houseBillItem)
        {
            this.houseBillItem = houseBillItem;
            title.Text = string.Format("{0} ({1:HH:mm})", houseBillItem.Reference(), houseBillItem.ReleaseTime);
            labelPieces.Text = string.Format("{0} of {1}", this.houseBillItem.ScannedPieces, this.houseBillItem.TotalPieces);
            this.lblSlac.Text = string.Format("{0} of {1}", this.houseBillItem.ReleseSlac, this.houseBillItem.Slac);
            labelLoc.Text = this.houseBillItem.Location;
            labelSender.Text = houseBillItem.SenderName;
            labelReceiver.Text = houseBillItem.ReceiverName;
            labelDescr.Text = houseBillItem.Description;
            lblAliasNumber.Text = houseBillItem.AliasNumber;

            switch (this.houseBillItem.status)
            {
                case CargoMatrix.Communication.DTO.HouseBillStatuses.Open: 
                case CargoMatrix.Communication.DTO.HouseBillStatuses.Pending:   
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case CargoMatrix.Communication.DTO.HouseBillStatuses.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;

            }
            panelIndicators.Flags = this.houseBillItem.Flags;
            panelIndicators.PopupHeader = houseBillItem.Reference();
        }


        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            DischargeItemFunctionallity.DisplayBillViewer(this.houseBillItem.HousebillNumber, this.Left, this.Width);
        }

        void buttonDamage_Click(object sender, System.EventArgs e)
        {
            DischargeItemFunctionallity.DisplayDamage(this.houseBillItem);    
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            //DischargeItemFunctionallity.DisplayBrowse(title.Text, this.houseBillItem, out item);

            //if (item != null)
            //{
            //    this.DisplayHousebill(item);
            //}
        }
    }
}