﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.FreightDischarge
{
    public enum OptionsActionPerformedEnum
    {
        SwitchMode,
        PrintLabels,
        ShowLocations,
        PiecesMovedToForklift,
        CancelTask,
        Close
    }
}
