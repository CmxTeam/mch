﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WarehouseManager;

namespace CargoMatrix.CargoUtilities
{
    public partial class LocationList : SmoothListbox.SmoothListbox2
    {
        IRegion region;
        List<ILocation> locations;
        
        
        public LocationList(IRegion region)
        {
            this.region = region;
            this.locations = new List<ILocation>();

            InitializeComponent();

            this.lblLine1.Text = string.Format("Warehouse : {0}", region.WarehouseName);
            this.lblLine2.Text = string.Format("Region : {0}", region.Name);

            this.TitleText = "LocationManager";

            this.LoadOptionsMenu += new EventHandler(LocationList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(LocationList_MenuItemClicked);

            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LocationList_BarcodeReadNotify);
            this.BarcodeEnabled = true;

            this.smoothListBoxMainList.AddItem(lastScanItem);
        }

        void LocationList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;
            
            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.VIEW_ALL_LOCATIONS:

                    LocationTypeSelector lTypeSelector = new LocationTypeSelector();
                    var result = lTypeSelector.ShowDialog();

                    if (result != DialogResult.OK) break;
                    
                    Cursor.Current = Cursors.WaitCursor;

                    var locations = WarehouseManagerProvider.Instance.GetLocationList(this.region, lTypeSelector.LocationType);

                    LocationsListView lLocationsView = new LocationsListView(locations);
                    lLocationsView.HeaderText = "Locations View";
                    lLocationsView.HeaderText2 = string.Format("Total : {0}", locations.Count());

                    lLocationsView.ShowDialog();


                    Cursor.Current = Cursors.Default;

                    break;
            }

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
        }

        void LocationList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.VIEW_ALL_LOCATIONS));
        }

        void LocationList_BarcodeReadNotify(string barcodeData)
        {
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
               
                    Cursor.Current = Cursors.WaitCursor;

                    ILocation location;
                    
                    bool result = CommonMethods.CreateLocation(this.region, scanItem,  out location);

                    Cursor.Current = Cursors.Default;

                    if (result == false) break;

                    this.lastScanItem.CaptionLine1 = string.Format("Last scan : {0}", location.Code);
                    this.lastScanItem.CaptionLine2 = string.Format("Location type : {0}", CommonMethods.GetLocationDescription((LocationTypeEnum)location.TypeID));
                  
                    this.locations.Add(location);
                    this.btnScanned.Value = this.locations.Count;
            
                    break;

                default:
                    
                    CargoMatrix.UI.CMXMessageBox.Show("Scanned barcode is not a valid warehouse location", "Scan error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    
                    break;

            }

            this.BarcodeEnabled = true;
        }

        void btnScanned_Click(object sender, System.EventArgs e)
        {
            if (this.btnScanned.Value <= 0) return;

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            LocationsListView lListView = new LocationsListView(this.locations.ToArray());
            lListView.HeaderText = "Scanned Locations View";
            lListView.HeaderText2 = string.Format("Total scanned : {0}", this.locations.Count());

            lListView.ShowDialog();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
        }
    }
}
