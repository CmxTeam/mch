﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class TaskList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (billViewer != null)
            {
                billViewer.Dispose();
                billViewer = null;
            }
            if (m_thread != null)
            {
                m_thread.Abort();
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TaskList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.BarcodeEnabled = true;
            this.Name = "TaskList";
            this.LoadOptionsMenu += new System.EventHandler(this.TaskList_LoadOptionsMenu);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(this.TaskList_BarcodeReadNotify);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(this.TaskList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(TaskList_ListItemClicked);
            //this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(TaskList_ListItemClicked);// SmoothListbox.ListItemClickedHandler(this.TaskList_ListItemClicked);
            this.EnabledChanged += new System.EventHandler(this.TaskList_EnabledChanged);
            this.ResumeLayout(false);

        }

        //void TaskList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        //{
        //    throw new System.NotImplementedException();
        //}

        

        #endregion

    }
}
