﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class MicroPhotoCaptureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxDown = new System.Windows.Forms.PictureBox();
            this.pictureBoxUp = new System.Windows.Forms.PictureBox();
            this.pictureBoxCancel = new System.Windows.Forms.PictureBox();
            this.pictureBoxOk = new System.Windows.Forms.PictureBox();
            this.pictureBoxMenuBack = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.microReasons1 = new CargoMatrix.FreightPhotoCapture.MicroReasons();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Controls.Add(this.pictureBoxDown);
            this.panel1.Controls.Add(this.pictureBoxUp);
            this.panel1.Controls.Add(this.pictureBoxCancel);
            this.panel1.Controls.Add(this.pictureBoxOk);
            this.panel1.Controls.Add(this.microReasons1);
            this.panel1.Controls.Add(this.pictureBoxMenuBack);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Location = new System.Drawing.Point(10, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(219, 266);
            // 
            // pictureBoxDown
            // 
            this.pictureBoxDown.Location = new System.Drawing.Point(165, 226);
            this.pictureBoxDown.Name = "pictureBoxDown";
            this.pictureBoxDown.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDown_MouseDown);
            this.pictureBoxDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDown_MouseUp);
            this.pictureBoxDown.EnabledChanged += new System.EventHandler(this.pictureBoxDown_EnabledChanged);
            // 
            // pictureBoxUp
            // 
            this.pictureBoxUp.Location = new System.Drawing.Point(111, 226);
            this.pictureBoxUp.Name = "pictureBoxUp";
            this.pictureBoxUp.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxUp_MouseDown);
            this.pictureBoxUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxUp_MouseUp);
            this.pictureBoxUp.EnabledChanged += new System.EventHandler(this.pictureBoxUp_EnabledChanged);
            // 
            // pictureBoxCancel
            // 
            this.pictureBoxCancel.Location = new System.Drawing.Point(57, 226);
            this.pictureBoxCancel.Name = "pictureBoxCancel";
            this.pictureBoxCancel.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCancel.Click += new System.EventHandler(this.pictureBoxCancel_Click);
            this.pictureBoxCancel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCancel_MouseDown);
            this.pictureBoxCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCancel_MouseUp);
            // 
            // pictureBoxOk
            // 
            this.pictureBoxOk.Location = new System.Drawing.Point(3, 226);
            this.pictureBoxOk.Name = "pictureBoxOk";
            this.pictureBoxOk.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOk.Click += new System.EventHandler(this.pictureBoxOk_Click);
            this.pictureBoxOk.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxOk_MouseDown);
            this.pictureBoxOk.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxOk_MouseUp);
            // 
            // pictureBoxMenuBack
            // 
            this.pictureBoxMenuBack.Location = new System.Drawing.Point(0, 224);
            this.pictureBoxMenuBack.Name = "pictureBoxMenuBack";
            this.pictureBoxMenuBack.Size = new System.Drawing.Size(219, 42);
            this.pictureBoxMenuBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(219, 22);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // microReasons1
            // 
            this.microReasons1.Location = new System.Drawing.Point(2, 22);
            this.microReasons1.Name = "microReasons1";
            this.microReasons1.Size = new System.Drawing.Size(215, 201);
            this.microReasons1.TabIndex = 0;
            // 
            // MicroPhotoCaptureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "MicroPhotoCaptureForm";
            this.Text = "MicroPhotoCaptureForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MicroPhotoCaptureForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MicroReasons microReasons1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxMenuBack;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.PictureBox pictureBoxDown;
        private System.Windows.Forms.PictureBox pictureBoxUp;
        private System.Windows.Forms.PictureBox pictureBoxCancel;
        private System.Windows.Forms.PictureBox pictureBoxOk;
    }
}