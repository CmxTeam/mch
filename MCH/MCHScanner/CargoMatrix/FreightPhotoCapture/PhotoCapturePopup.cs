﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CargoMatrix.FreightPhotoCapture
{
    public class PhotoCapturePopup : CargoMatrix.Utilities.MessageListBox
    {
        CargoMatrix.FreightPhotoCapture.PhotoCaptureControl ctrl;
        public PhotoCapturePopup()
        {
            this.SuspendLayout();
            this.Height = 246;
            this.panel1.BackColor = Color.White;
            this.AutoScaleDimensions = new SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi; 
            this.ResumeLayout(false);
            ctrl = new PhotoCaptureControl("123412", "Proof of Condition");
            //ctrl.Size = new System.Drawing.Size(464, 372);
            ctrl.Location = new System.Drawing.Point(0, 0);
            this.panel1.Controls.Remove(this.smoothListBoxBase1);
            this.panel1.Controls.Add(ctrl);
            ctrl.BringToFront();
            ctrl.Enabled = false;
            ctrl.Enabled = true;
        }
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            ctrl.StopCamera();
            ctrl.Dispose();
        }
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            ctrl.StopCamera();
            base.pictureBoxOk_Click(sender, e);
        }
    }
}
