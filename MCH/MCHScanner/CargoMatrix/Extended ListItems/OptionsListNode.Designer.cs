﻿namespace SmoothListBox.UI.ListItems
{
    partial class OptionsListNode
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxExpand = new OpenNETCF.Windows.Forms.PictureBox2();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxExpand)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxExpand
            // 
            this.pictureBoxExpand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxExpand.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBoxExpand.Location = new System.Drawing.Point(219, 12);
            this.pictureBoxExpand.Name = "pictureBoxExpand";
            this.pictureBoxExpand.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxExpand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // OptionsListNode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.pictureBoxExpand);
            this.Name = "OptionsListNode";
            this.Size = new System.Drawing.Size(240, 40);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxExpand)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        //private System.Windows.Forms.PictureBox pictureBoxExpand;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxExpand;

    }
}
