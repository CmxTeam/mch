﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class TwoLineListItem : StandardListItem
    {
        
        
        //public StandardListItem()
        //{
        //    InitializeComponent();
        //    itemPicture.TransparentColor = Color.White;

        //}
        public TwoLineListItem(string title, Image picture, string line2)
        {
            InitializeComponent();
            this.title.Text = title;
            this.Name = title;
            this.itemPicture.Image = picture;
            
            itemPicture.TransparentColor = Color.White;
        }

        public TwoLineListItem(string title, Image picture, int id, string line2)
        {
            InitializeComponent();
            ID = id;
            this.title.Text = title;
            this.Name = title;
            this.itemPicture.Image = picture;
            LabelLine2.Text = line2;
            itemPicture.TransparentColor = Color.White;
        }
        public override void SelectedChanged(bool isSelected)
        {
            base.SelectedChanged(isSelected);

            base.Focus(isSelected);

        }
        
        
       
    }
}
