﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXExtensions;
namespace CargoMatrix.CargoReceiver
{
    class HawbViewer : CargoMatrix.Utilities.MessageListBox
    {
        private CargoMatrix.Communication.WSCargoReceiver.HouseBillItem hawb;
        public HawbViewer(CargoMatrix.Communication.WSCargoReceiver.HouseBillItem hawb)
        {
            InitializeComponent();
            this.hawb = hawb;
            this.HeaderText = hawb.Reference();
            this.HeaderText2 = "Select Pieces Add to Forklift";
            LoadControl();
        }

        private void InitializeComponent()
        {
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            //check scanned housebill against current housebill on FL
            if (!Forklift.Instance.AllowMultipleHawbOnForkLift)
                if (!string.IsNullOrEmpty(Forklift.Instance.CurrentHousebill.hawbNo) && Forklift.Instance.ItemCount != 0)
                {
                    if (!string.Equals(Forklift.Instance.CurrentHousebill.hawbNo, hawb.HousebillNumber, StringComparison.OrdinalIgnoreCase))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_SingleHousebillScan, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        return;
                    }
                }


            List<int> pieceItems = new List<int>();
            foreach (var item in smoothListBoxBase1.Items)
            {
                if (item is CustomListItems.ComboBox)
                {
                    pieceItems.AddRange((item as CustomListItems.ComboBox).SelectedIds);
                }
            }

            HouseBillItem hawbdetails;
            if (hawb.ScanMode == ScanModes.Piece)
                hawbdetails = CargoMatrix.Communication.CargoReceiver.Instance.ScanHawbPiecesIntoForklift(hawb.HousebillNumber, pieceItems.ToArray<int>(), Forklift.Instance.Mawb.TaskId, Forklift.Instance.ForkliftID);
            else
                hawbdetails = CargoMatrix.Communication.CargoReceiver.Instance.ScanHawbCountIntoForklift(hawb.HousebillId, pieceItems.Count, Forklift.Instance.Mawb.TaskId, Forklift.Instance.ForkliftID, ScanTypes.Manual);

            if (!hawbdetails.Transaction.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(hawbdetails.Transaction.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            else
            {
                Forklift.Instance.CurrentHousebill.SetHawb(hawb.HousebillNumber, hawb.DropLocation);
                Forklift.Instance.Status = hawbdetails.Transaction;
            }
            this.DialogResult = DialogResult.OK;//LoadControl();

        }

        public void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            smoothListBoxBase1.RemoveAll();
            foreach (var loc in CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hawb.HousebillId,Forklift.Instance.Mawb.MasterBillId, CargoMatrix.Communication.ScannerUtilityWS.MOT.Air))
            {

                List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                foreach (var pc in loc.Piece)
#warning Id needs to be sat
                    items.Add(new CustomListItems.ComboBoxItemData("Piece " + pc.PieceNumber, pc.Location, /*pc.PieceId*/pc.PieceNumber, true));

                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, loc.Location, string.Format("Pieces: {0} of {1}", loc.Piece.Length, hawb.TotalPieces), hawb.ScanMode == ScanModes.Count ? CargoMatrix.Resources.Skin.countMode : CargoMatrix.Resources.Skin.PieceMode, items);
                combo.IsSelectable = true;
                combo.ReadOnly = true;
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                smoothListBoxBase1.AddItem(combo);
            }

            this.smoothListBoxBase1.SelectAll();
            Cursor.Current = Cursors.Default;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if ((sender as CustomListItems.ComboBox).SubItemSelected)
                OkEnabled = true;
            else
                OkEnabled = false;
        }
    }
}
