﻿namespace CargoMatrix.CargoReceiver
{
    partial class HawbScan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonScannedList = new CargoMatrix.UI.CMXCounterIcon();
            this.labelBlink = new CustomUtilities.CMXBlinkingLabel();
            //this.labelLocation = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.topPanel = new System.Windows.Forms.Panel();
            this.labelLine1 = new System.Windows.Forms.Label();
            this.labelLine2 = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();

            this.topPanel.SuspendLayout();

            this.SuspendLayout();
            // 
            // buttonScannedList
            // 
            this.buttonScannedList.Location = new System.Drawing.Point(198, 4);
            this.buttonScannedList.Name = "buttonScannedList";
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            this.buttonScannedList.Size = new System.Drawing.Size(36, 36);
            this.buttonScannedList.TabIndex = 1;
            this.buttonScannedList.Click += new System.EventHandler(buttonScannedList_Click);
            // 
            // labelLine1
            // 
            this.labelLine1.BackColor = System.Drawing.Color.White;
            this.labelLine1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine1.Location = new System.Drawing.Point(38, 2);
            this.labelLine1.Name = "labelLine1";
            this.labelLine1.Size = new System.Drawing.Size(160, 15);
            // 
            // labelLine2
            // 
            this.labelLine2.BackColor = System.Drawing.Color.White;
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine2.Location = new System.Drawing.Point(38, 18);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(160, 15);
            // 
            // labelBlink
            // 
            this.labelBlink.BackColor = System.Drawing.Color.White;
            this.labelBlink.Blink = true;
            this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelBlink.ForeColor = System.Drawing.Color.Red;
            this.labelBlink.Location = new System.Drawing.Point(3, 34);
            this.labelBlink.Name = "labelBlink";
            this.labelBlink.Size = new System.Drawing.Size(190, 15);
            this.labelBlink.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelLocation
            // 
            //this.labelLocation.BackColor = System.Drawing.Color.White;
            //this.labelLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            //this.labelLocation.ForeColor = System.Drawing.Color.Black;
            //this.labelLocation.Location = new System.Drawing.Point(3, 2);
            //this.labelLocation.Name = "labelLocation";
            //this.labelLocation.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //this.labelLocation.Size = new System.Drawing.Size(190, 18);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 43);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            // 
            // topPanel
            // 
            this.topPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.topPanel.Controls.Add(this.labelBlink);
            this.topPanel.Controls.Add(this.labelLine1);
            this.topPanel.Controls.Add(this.labelLine2);
            this.topPanel.Controls.Add(this.buttonScannedList);
            this.topPanel.Controls.Add(this.splitter1);
            this.topPanel.Controls.Add(this.pictureBoxLogo);
            //this.topPanel.Controls.Add(this.labelLocation);
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.Location = new System.Drawing.Point(2, 2);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(30, 30);
            // 
            // HawbInventory
            // 
            this.panelHeader2.Controls.Add(this.topPanel);
            this.Name = "HawbInventory";
            InitializeComponentHelper();

            this.topPanel.Size = new System.Drawing.Size(240, 50);
            this.Size = new System.Drawing.Size(240, 292);  
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.topPanel.ResumeLayout(false);
            this.ResumeLayout(false);
        }




        private System.Windows.Forms.Splitter splitter1;
        private CustomUtilities.CMXBlinkingLabel labelBlink;
        private System.Windows.Forms.Label labelLine1;
        private System.Windows.Forms.Label labelLine2;
        private CargoMatrix.UI.CMXCounterIcon buttonScannedList;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        //private System.Windows.Forms.Label labelLocation;

        #endregion
    }
}
