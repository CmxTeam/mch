﻿using MCH.BLL.DAL;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services;

namespace ScannerWebServices
{
    public class ScannerMCHService : ServiceBase
    {
        [WebMethod]
        public UserSession LoginByPin(string station, string pin)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork()) 
            {
                return scannerUnit.LoginByPin(station, pin);
            }
        }

        [WebMethod]
        public UserSession LoginByUserName(string station, string userName, string password, Platforms platformID)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.LoginByUserName(station, userName, password, platformID);
            }
        }

        [WebMethod]
        public LoginTypes LoginPreference(string gatewayName)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.LoginPreference(gatewayName);
            }
        }

        [WebMethod]
        public WarehouseLocationModel[] GetWarehouseLocations(string station, LocationType location)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetWarehouseLocations(station, location);
            }
        }

        [WebMethod]
        public MainMenuItem[] GetMainMenu(string guid, long userID, string gateway, string station, out int latestVersion)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetMainMenu(guid, userID, gateway, station, out latestVersion);
            }
        }

        [WebMethod]
        public long GetLocationIdByLocationBarcode(string station, string barcode)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetLocationIdByLocationBarcode(station, barcode);
            }
        }

        [WebMethod]
        public void AddTaskSnapshotReference(long taskId, string reference)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                scannerUnit.AddTaskSnapshotReference(taskId, reference);
            }
        }

        [WebMethod]
        public bool ExecuteNonQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteNonQuery(station, query);
            }
        }

        [WebMethod]
        public DataTable ExecuteQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteQuery(station, query);
            }
        }

        [WebMethod]
        public AwbInfo GetAwbInfo(string station, string shipment)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetAwbInfo(station, shipment);
            }
        }
    }
}
