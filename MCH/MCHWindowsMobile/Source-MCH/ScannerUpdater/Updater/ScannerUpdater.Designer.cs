﻿namespace Updater
{
    partial class ScannerUpdater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListBoxGateway = new System.Windows.Forms.CheckedListBox();
            this.hostPlusLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hostPlusDBDataSet = new Updater.HostPlusDBDataSet();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.scannerUpdateGatewayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.scannerUpdateGateway = new Updater.ScannerUpdateGateway();
            this.scannerUpdateGatewayTableAdapter = new Updater.ScannerUpdateGatewayTableAdapters.ScannerUpdateGatewayTableAdapter();
            this.hostPlus_LocationsTableAdapter = new Updater.HostPlusDBDataSetTableAdapters.HostPlus_LocationsTableAdapter();
            this.checkBoxCheckAll = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdateGatewayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdateGateway)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(465, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 27);
            this.button1.TabIndex = 2;
            this.button1.Text = "Select Folder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(103, 56);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(452, 244);
            this.checkedListBox1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 317);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 9;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkedListBoxGateway
            // 
            this.checkedListBoxGateway.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxGateway.CheckOnClick = true;
            this.checkedListBoxGateway.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.hostPlusLocationsBindingSource, "Location", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.checkedListBoxGateway.FormattingEnabled = true;
            this.checkedListBoxGateway.Location = new System.Drawing.Point(4, 56);
            this.checkedListBoxGateway.Name = "checkedListBoxGateway";
            this.checkedListBoxGateway.Size = new System.Drawing.Size(93, 244);
            this.checkedListBoxGateway.TabIndex = 11;
            // 
            // hostPlusLocationsBindingSource
            // 
            this.hostPlusLocationsBindingSource.DataMember = "HostPlus_Locations";
            this.hostPlusLocationsBindingSource.DataSource = this.hostPlusDBDataSet;
            // 
            // hostPlusDBDataSet
            // 
            this.hostPlusDBDataSet.DataSetName = "HostPlusDBDataSet";
            this.hostPlusDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(334, 20);
            this.textBox1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Version Label";
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(481, 306);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(74, 27);
            this.buttonOk.TabIndex = 14;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // scannerUpdateGatewayBindingSource
            // 
            this.scannerUpdateGatewayBindingSource.DataMember = "ScannerUpdateGateway";
            this.scannerUpdateGatewayBindingSource.DataSource = this.scannerUpdateGateway;
            // 
            // scannerUpdateGateway
            // 
            this.scannerUpdateGateway.DataSetName = "ScannerUpdateGateway";
            this.scannerUpdateGateway.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // scannerUpdateGatewayTableAdapter
            // 
            this.scannerUpdateGatewayTableAdapter.ClearBeforeFill = true;
            // 
            // hostPlus_LocationsTableAdapter
            // 
            this.hostPlus_LocationsTableAdapter.ClearBeforeFill = true;
            // 
            // checkBoxCheckAll
            // 
            this.checkBoxCheckAll.AutoSize = true;
            this.checkBoxCheckAll.Checked = true;
            this.checkBoxCheckAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCheckAll.Location = new System.Drawing.Point(4, 306);
            this.checkBoxCheckAll.Name = "checkBoxCheckAll";
            this.checkBoxCheckAll.Size = new System.Drawing.Size(90, 17);
            this.checkBoxCheckAll.TabIndex = 15;
            this.checkBoxCheckAll.Text = "Un/Check All";
            this.checkBoxCheckAll.UseVisualStyleBackColor = true;
            this.checkBoxCheckAll.CheckedChanged += new System.EventHandler(this.checkBoxCheckAll_CheckedChanged);
            // 
            // ScannerUpdater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 347);
            this.Controls.Add(this.checkBoxCheckAll);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.checkedListBoxGateway);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.button1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.scannerUpdateGatewayBindingSource, "ScannerUpdateID", true));
            this.Name = "ScannerUpdater";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ScannerUpdater_Load);
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdateGatewayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdateGateway)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox checkedListBoxGateway;
        private HostPlusDBDataSet hostPlusDBDataSet;
        private System.Windows.Forms.BindingSource hostPlusLocationsBindingSource;
        private Updater.HostPlusDBDataSetTableAdapters.HostPlus_LocationsTableAdapter hostPlus_LocationsTableAdapter;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonOk;
        private ScannerUpdateGateway scannerUpdateGateway;
        private System.Windows.Forms.BindingSource scannerUpdateGatewayBindingSource;
        private Updater.ScannerUpdateGatewayTableAdapters.ScannerUpdateGatewayTableAdapter scannerUpdateGatewayTableAdapter;
        private System.Windows.Forms.CheckBox checkBoxCheckAll;
    }
}

