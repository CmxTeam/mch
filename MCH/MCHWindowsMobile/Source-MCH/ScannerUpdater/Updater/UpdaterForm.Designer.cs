﻿namespace Updater
{
    partial class UpdaterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.scannerUpdate = new Updater.ScannerUpdate();
            this.scannerUpdateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.scannerUpdateTableAdapter = new Updater.ScannerUpdateTableAdapters.ScannerUpdateTableAdapter();
            this.buttonDetails = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.hostPlusLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hostPlusDBDataSet = new Updater.HostPlusDBDataSet();
            this.hostPlus_LocationsTableAdapter = new Updater.HostPlusDBDataSetTableAdapters.HostPlus_LocationsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusDBDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.hostPlusLocationsBindingSource, "Location", true, System.Windows.Forms.DataSourceUpdateMode.Never, null, "E2"));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(26, 51);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(424, 244);
            this.checkedListBox1.TabIndex = 6;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(355, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 27);
            this.button1.TabIndex = 7;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // scannerUpdate
            // 
            this.scannerUpdate.DataSetName = "ScannerUpdate";
            this.scannerUpdate.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // scannerUpdateBindingSource
            // 
            this.scannerUpdateBindingSource.DataMember = "ScannerUpdate";
            this.scannerUpdateBindingSource.DataSource = this.scannerUpdate;
            // 
            // scannerUpdateTableAdapter
            // 
            this.scannerUpdateTableAdapter.ClearBeforeFill = true;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Enabled = false;
            this.buttonDetails.Location = new System.Drawing.Point(340, 308);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(108, 25);
            this.buttonDetails.TabIndex = 8;
            this.buttonDetails.Text = "Details";
            this.buttonDetails.UseVisualStyleBackColor = true;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Enabled = false;
            this.buttonApply.Location = new System.Drawing.Point(34, 307);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(94, 25);
            this.buttonApply.TabIndex = 9;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // hostPlusLocationsBindingSource
            // 
            this.hostPlusLocationsBindingSource.DataMember = "HostPlus_Locations";
            this.hostPlusLocationsBindingSource.DataSource = this.hostPlusDBDataSet;
            // 
            // hostPlusDBDataSet
            // 
            this.hostPlusDBDataSet.DataSetName = "HostPlusDBDataSet";
            this.hostPlusDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // hostPlus_LocationsTableAdapter
            // 
            this.hostPlus_LocationsTableAdapter.ClearBeforeFill = true;
            // 
            // UpdaterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 343);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkedListBox1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.scannerUpdateBindingSource, "RecID", true));
            this.Name = "UpdaterForm";
            this.Text = "Updater";
            this.Load += new System.EventHandler(this.Updater_Load);
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scannerUpdateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hostPlusDBDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private HostPlusDBDataSet hostPlusDBDataSet;
        private System.Windows.Forms.BindingSource hostPlusLocationsBindingSource;
        private Updater.HostPlusDBDataSetTableAdapters.HostPlus_LocationsTableAdapter hostPlus_LocationsTableAdapter;
        private System.Windows.Forms.Button button1;
        private ScannerUpdate scannerUpdate;
        private System.Windows.Forms.BindingSource scannerUpdateBindingSource;
        private Updater.ScannerUpdateTableAdapters.ScannerUpdateTableAdapter scannerUpdateTableAdapter;
        private System.Windows.Forms.Button buttonDetails;
        private System.Windows.Forms.Button buttonApply;

    }
}