﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class MicroReasonsListItem : SmoothListbox.ListItems.StandardListItem// SmoothListBox.UI.ListItems.CheckBox//.StandardListItem// UserControl
    {
        int m_reasonType;
        MicroReasons m_parent;
        int m_previousHeight;
        public event ExitEventHandler CameraExitNotify;
        CargoMatrix.Communication.DTO.FreightPhotoCaptureItem photoCaptureData;
        public MicroPhotoCaptureControl microPhotoCaptureControl;
        bool m_bHasCamera = false;

        //public Control threadSyncControl;
        
        //public ReasonsListItem()
        //{
        //    InitReasonsListItem();
            
        //}

        public MicroReasonsListItem(string title, MicroReasons parent, int reasonType, bool HasCamera)
        {
            m_reasonType = reasonType;
            InitReasonsListItem();
            m_parent = parent;
            this.title.Text = title;
            photoCaptureData.reason = this.title.Text;
            m_bHasCamera = HasCamera;
         

        }

        private void InitReasonsListItem()
        {
            InitializeComponent();
            m_previousHeight = Height;
            
            if (m_reasonType == 1)
                itemPicture.Image = PhotoCaptureResource.Notepad_Information;//.Notepad_Edit;
            else
                itemPicture.Image = PhotoCaptureResource.Notepad;

            photoCaptureData = new CargoMatrix.Communication.DTO.FreightPhotoCaptureItem();
            //this.FocusedColor = Color.WhiteSmoke;//Color.Khaki;//.LightYellow;

            title.Width = pictureBoxCheck.Left - title.Left;

        }
        public void InsertOuterImage(Image image)
        {
            itemPicture.Image = PhotoCaptureResource.Notepad_Check;
            if (pictureBoxCheck.Image != null)
            {
                pictureBoxCheck.Image.Dispose();
                pictureBoxCheck.Image = null;
            }
            pictureBoxCheck.Image = image;
            pictureBoxCheck.Visible = true;
        }

        public override void SelectedChanged(bool isSelected)
        {
            base.SelectedChanged(isSelected);
            
            
            if (isSelected)
            {
                if (m_bHasCamera == false)
                {
                    itemPicture.Image = PhotoCaptureResource.Notepad_Information;//.Notepad_Edit;
                }
                
                
                
            }
            else
            {
                if (m_bHasCamera == false)
                {
                    itemPicture.Image = PhotoCaptureResource.Notepad;//.Notepad_Edit;
                }
            }

            Refresh();
        }

        void microPhotoCaptureControl_ExitNotify(Control sender)
        {
            try
            {
                if (sender is MicroPhotoCaptureControl)
                {
                    List<CargoMatrix.Communication.DTO.ImageObject> imgList = (sender as MicroPhotoCaptureControl).ImageList;
                    if (imgList == null)
                    {
                        CloseCamera();
                        if (microPhotoCaptureControl != null)
                        {
                            Controls.Remove(microPhotoCaptureControl);
                            microPhotoCaptureControl.Dispose();
                            microPhotoCaptureControl = null;
                        }

                        if (pictureBoxCheck.Image != null)
                        {
                            this.pictureBoxCheck.Image.Dispose();
                            this.pictureBoxCheck.Image = null;
                        }
                        return;
                    }
                    else
                    {
                        if (imgList.Count == 0)
                        {
                            CloseCamera();
                            if (microPhotoCaptureControl != null)
                            {
                                Controls.Remove(microPhotoCaptureControl);
                                microPhotoCaptureControl.Dispose();
                                microPhotoCaptureControl = null;
                                
                            }

                            if (pictureBoxCheck.Image != null)
                            {
                                this.pictureBoxCheck.Image.Dispose();
                                this.pictureBoxCheck.Image = null;
                            }
                            return;
                        }
                    }

                    if (pictureBoxCheck.Image != null)
                    {
                        this.pictureBoxCheck.Image.Dispose();
                        this.pictureBoxCheck.Image = null;
                    }
                    base.SelectedChanged(false);
                    
                    if(m_reasonType == 1)
                        itemPicture.Image = PhotoCaptureResource.Notepad_Information;
                    else
                        itemPicture.Image = PhotoCaptureResource.Notepad;

                    if (imgList != null)
                    {
                        if (imgList.Count > 0)
                        {
                            GC.Collect();

                            for (int i = 0; i < imgList.Count; i++)
                            {
                                if (imgList[i].m_rawImage != null)
                                {
                                    InsertOuterImage(new Bitmap(new System.IO.MemoryStream(imgList[i].m_rawImage)));
                                    break;
                                }
                            }
                                
                            
                        }
                        
                    }
                    
                    CloseCamera();
                    
                }

            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30007);

            }
            
        }
        public void CloseCamera()
        {
            Height = m_previousHeight;
            microPhotoCaptureControl.Enabled = false;
            microPhotoCaptureControl.Visible = false;
            title.Visible = true;
            itemPicture.Visible = true;
            pictureBoxCheck.Visible = true;

            if (CameraExitNotify != null)
            {

                CameraExitNotify(this);
            }
        
        }

        public void ShowCamera()
        {
            SuspendLayout();
            if (microPhotoCaptureControl == null)
            {

                microPhotoCaptureControl = new  MicroPhotoCaptureControl();
                //microPhotoCaptureControl.FilmBackColor = m_parent.thumbnailBackColor;
                microPhotoCaptureControl.m_parent = m_parent;

                microPhotoCaptureControl.ExitNotify += new ExitEventHandler(microPhotoCaptureControl_ExitNotify);
                microPhotoCaptureControl.Location = new Point(0, 0);
                this.Controls.Add(microPhotoCaptureControl);
                int index = m_parent.MainControlIndex(this);
                
                if (index >= 0)
                    microPhotoCaptureControl.ImageList = m_parent.PhotoCaptureData.itemsList[index].imagelist;
                    
                
                
            }


            microPhotoCaptureControl.Visible = true;

            title.Visible = false;
            itemPicture.Visible = false;
            pictureBoxCheck.Visible = false;

            this.Height = microPhotoCaptureControl.Height + m_previousHeight;
            microPhotoCaptureControl.Title = title.Text;
            m_parent.smoothListBoxReasons.LayoutItems();
            m_parent.smoothListBoxReasons.MoveControlToTop(this);

            ResumeLayout(true);
            Refresh();

            microPhotoCaptureControl.Enabled = false;
            microPhotoCaptureControl.Enabled = true;
            
                
        }
        public List<CargoMatrix.Communication.DTO.ImageObject> GetImageList()
        {
            if (microPhotoCaptureControl == null)
                return null;
            return microPhotoCaptureControl.ImageList;
            
        }

        public List<CargoMatrix.Communication.DTO.ImageObject> GetUploadImageList()
        {
            if (microPhotoCaptureControl == null)
                return null;
            return microPhotoCaptureControl.UploadImageList;

        }
        public void GoBack()
        {
            if (microPhotoCaptureControl != null)
                microPhotoCaptureControl.cameraTimer.Enabled = false;

            microPhotoCaptureControl_ExitNotify(microPhotoCaptureControl);
        }
        public void StopCameraTimer()
        {
            if (microPhotoCaptureControl != null)
                microPhotoCaptureControl.cameraTimer.Enabled = false;
        }
    }
}
