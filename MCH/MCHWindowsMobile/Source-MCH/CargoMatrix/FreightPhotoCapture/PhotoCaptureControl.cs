﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class PhotoCaptureControl : CargoMatrix.UI.CMXUserControl
    {
        public event ExitEventHandler ExitNotify;
        List<SmoothListBox.UI.ListItems.Thumbnail> imageList;

        public PhotoCaptureControl()
        {
            InitPhotoCapture();

        }
        private void InitPhotoCapture()
        {
            InitializeComponent();
            buttonDelete.Enabled = false;
            buttonZoom.Enabled = false;
            buttonNote.Image = FreightPhotoCapture.PhotoCaptureResource.Notepad_disable;
            buttonNote.Enabled = false;
            buttonCameraMode.Image = FreightPhotoCapture.PhotoCaptureResource.Digital_Camera_disable;
            buttonCameraMode.Enabled = false;
            buttonExit.Image = FreightPhotoCapture.PhotoCaptureResource.Exit;
            imageList = new List<SmoothListBox.UI.ListItems.Thumbnail>();
        
        }
        
        SmoothListBox.UI.ListItems.Thumbnail selectedThumbnail;
        private void cmxCameraControl1_ImageReadNotify(System.IO.MemoryStream stream)
        {
            if (stream == null)
            {
                if (selectedThumbnail != null)
                {
                    horizontalSmoothListbox1.Reset();
                }
                EnableButtons(false);
                
            }
            else
            {
                selectedThumbnail = new SmoothListBox.UI.ListItems.Thumbnail();
                selectedThumbnail.image = new Bitmap(stream);
                horizontalSmoothListbox1.AddItem(selectedThumbnail);
                selectedThumbnail.SelectedChanged(true);
                horizontalSmoothListbox1.MoveListToEnd();

                EnableButtons(true);
                
            }
        }

        private void buttonCaptureSave_Click(object sender, EventArgs e)
        {
            //if (tempImage != null)
            {
                

                    
                
            }
        }

        
        private void horizontalSmoothListbox1_ListItemClicked(SmoothListBox.UI.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            selectedThumbnail = listItem as SmoothListBox.UI.ListItems.Thumbnail;
            cmxCameraControl1.StopAcquisition();
            cmxCameraControl1.image = selectedThumbnail.image;

            EnableButtons(true);
        }
        public bool LeftButtonPressed
        { 
            set
            {
                horizontalSmoothListbox1.leftButtonPressed = value;
            }

        }

        public bool RightButtonPressed
        {
            set
            {
                horizontalSmoothListbox1.rightButtonPressed = value;
            }

        }

       
        private void buttonExit_Click_1(object sender, EventArgs e)
        {
            if (ExitNotify != null)
                ExitNotify(this);
        }
        private void EnableButtons(bool enable)
        {
            if (enable == true)
            {
                buttonDelete.Enabled = true;
                buttonZoom.Enabled = true;
                buttonNote.Image = FreightPhotoCapture.PhotoCaptureResource.Notepad;
                buttonNote.Enabled = true;
                buttonCameraMode.Image = FreightPhotoCapture.PhotoCaptureResource.Digital_Camera;
                buttonCameraMode.Enabled = true;
            }
            else
            {
                buttonDelete.Enabled = false;
                buttonZoom.Enabled = false;
                buttonNote.Image = FreightPhotoCapture.PhotoCaptureResource.Notepad_disable;
                buttonNote.Enabled = false;
                buttonCameraMode.Image = FreightPhotoCapture.PhotoCaptureResource.Digital_Camera_disable;
                buttonCameraMode.Enabled = false;
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to delete this picture?", "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Cancel))
                return;

            //if (selectedIndex >= 0)
            {
                if (selectedThumbnail != null)
                {
                    horizontalSmoothListbox1.RemoveItem(selectedThumbnail);
                    selectedThumbnail.image.Dispose();
                    selectedThumbnail.Dispose();
                    selectedThumbnail = null;
                    horizontalSmoothListbox1.MoveListToEnd();

                    
                }

                
            }

        }

        private void buttonCameraMode_Click(object sender, EventArgs e)
        {
            cmxCameraControl1.StartAcquisition();
        }

        private void buttonNote_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        private void buttonZoom_Click(object sender, EventArgs e)
        {
            cmxCameraControl1.FullScreen(true);

        }
    }
}
