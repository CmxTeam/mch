﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class FreightPhotoCaptureTaskItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {            
            if (pictureBoxLogo.Image != null)
            {
                pictureBoxLogo.Image.Dispose();
                pictureBoxLogo.Image = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLine2 = new System.Windows.Forms.Label();
            this.labelLine3 = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new OpenNETCF.Windows.Forms.PictureBox2();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // labelLine2
            // 
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelLine2.Location = new System.Drawing.Point(41, 23);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(164, 12);
            this.labelLine2.Text = "<labelDescription>";
            this.labelLine2.ParentChanged += new System.EventHandler(this.labelDescription_ParentChanged);
            // 
            // labelLine3
            // 
            this.labelLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelLine3.Location = new System.Drawing.Point(41, 38);
            this.labelLine3.Name = "labelLine3";
            this.labelLine3.Size = new System.Drawing.Size(164, 12);
            this.labelLine3.Text = "<labelHouseBill>";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Location = new System.Drawing.Point(213, 26);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Location = new System.Drawing.Point(3, 29);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(24, 24);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            this.buttonDetails.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonDetails_MouseDown);
            this.buttonDetails.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonDetails_MouseUp);
            // 
            // FreightPhotoCaptureTaskItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.labelLine2);
            this.Controls.Add(this.labelLine3);
            this.Name = "FreightPhotoCaptureTaskItem";
            this.Size = new System.Drawing.Size(240, 56);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelLine2; 
        private System.Windows.Forms.Label labelLine3;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxLogo;
        private OpenNETCF.Windows.Forms.PictureBox2 buttonDetails;
        
    }
}
