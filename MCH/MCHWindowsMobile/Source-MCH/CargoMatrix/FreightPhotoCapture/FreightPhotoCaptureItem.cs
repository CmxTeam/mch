﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
//using SmoothListBox.UI;
//using SmoothListBox.UI.ListItems;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class FreightPhotoCaptureTaskItem : SmoothListbox.ListItems.StandardListItem
    {
        TaskList m_parent;
        public string taskID;
        CargoMatrix.Communication.DTO.FreightPhotoCapture m_photoCapture;
        //CargoMatrix.Viewer.BillViewerShell masterbillViewer;

        private CargoMatrix.UI.CMXTextBox cmxTextBox1;
        private OpenNETCF.Windows.Forms.PictureBox2 buttonEnter;
        private OpenNETCF.Windows.Forms.PictureBox2 buttonCancel;
        //private OpenNETCF.Windows.Forms.PictureBox2 buttonDetails;
        private System.Windows.Forms.Label labelConfirm;

        bool m_firstTimeSelection = false;

        public FreightPhotoCaptureTaskItem()
        {
            itemPicture.Size = new Size(24, 24);
            InitializeComponent();
            previousHeight = this.Height;
            
        }
        public FreightPhotoCaptureTaskItem(string taskID, string title, string line2, string line3, Image picture)
        {
            itemPicture.Size = new Size(24, 24);
            InitializeComponent();
            
            previousHeight = this.Height;
            //this.m_houseBill = actualHouseBill;
            this.taskID = taskID;
            this.title.Text = title;
            labelLine2.Text = line2;
            labelLine3.Text = line3;
            itemPicture.Image = picture;
            //this.title.Top = 5;
            itemPicture.Top = (Height - itemPicture.Height) / 2;
            //Color col;
           
            //this.FocusedColor = Color.WhiteSmoke; // Color.Khaki;//.LightYellow;
            //if (picture != null)
            {
                //this.itemPicture.Image = picture;
                //Bitmap bmp = new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = Color.White; ;// bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }

            
        }
      
        public FreightPhotoCaptureTaskItem(CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture, TaskList parent)
        {
            itemPicture.Size = new Size(24, 24);
            InitializeComponent();
            buttonDetails.Image = PhotoCaptureResource.manual_entry_zoom;
            previousHeight = this.Height;
            m_parent = parent;
            m_photoCapture = photoCapture;
            //this.taskID = taskID;
            this.title.Text = m_photoCapture.taskPrefix + " " + m_photoCapture.reference;
            labelLine2.Text = m_photoCapture.line2;
            labelLine3.Text = m_photoCapture.line3;
            if (photoCapture.logo != null)
            {
                System.IO.MemoryStream stream = null;
                try
                {
                    stream = new System.IO.MemoryStream(photoCapture.logo);
                    pictureBoxLogo.Image = new Bitmap(stream);
                    stream.Close();
                }
                catch(Exception ex)
                {
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex,30033);//("Airline logo is not in a valid format. System will not be able to display this logo. Press OK to continue." "Error!", )
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                    }
                    
                }
            }
            
            //this.m_houseBill = m_photoCapture.actualHouseBill;
            //itemPicture.Image = SmoothListbox.ListItems.ListItemsResource.PHOTO_CAPTURE;
            //this.title.Top = 5;
            //itemPicture.Top = (Height - itemPicture.Height) / 2;

            switch (photoCapture.statusCode)
            { 
                case 'N':
                    itemPicture.Image = PhotoCaptureResource.Clipboard;
                    break;
                case 'I':
                    itemPicture.Image = PhotoCaptureResource.History;
                    break;
                case 'C':
                    itemPicture.Image = PhotoCaptureResource.Clipboard_Check;
                    break;

            }

            //this.FocusedColor = Color.WhiteSmoke;//.LightYellow;
            itemPicture.Click += new EventHandler(itemPicture_Click);
            itemPicture.MouseDown += new MouseEventHandler(itemPicture_MouseDown);
            itemPicture.MouseUp += new MouseEventHandler(itemPicture_MouseUp);
            
        }

        void itemPicture_MouseUp(object sender, MouseEventArgs e)
        {
            buttonDetails_MouseUp(sender, e);
        }

        void itemPicture_MouseDown(object sender, MouseEventArgs e)
        {
            buttonDetails_MouseDown(sender, e);
        }

        void itemPicture_Click(object sender, EventArgs e)
        {
            buttonDetails_Click(sender, e);
            //throw new NotImplementedException();
        }
        
        public CargoMatrix.Communication.DTO.FreightPhotoCapture PhotoCaptureData
        {
            get { return m_photoCapture; }
        }

        private void labelDescription_ParentChanged(object sender, EventArgs e)
        {

        }
        int previousHeight;
        public override void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;

            base.SelectedChanged(isSelected);

            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
            {


                if (isSelected)
                {
                    ////////////////////////////////
                    if (m_firstTimeSelection == false)
                    {
                        m_firstTimeSelection = true;

                        this.SuspendLayout();

                        this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
                        this.buttonEnter = new OpenNETCF.Windows.Forms.PictureBox2();
                        this.buttonCancel = new OpenNETCF.Windows.Forms.PictureBox2();

                        this.labelConfirm = new System.Windows.Forms.Label();
                        // cmxTextBox1
                        // 
                        this.cmxTextBox1.Location = new System.Drawing.Point(3, 69);
                        //this.cmxTextBox1.Name = "cmxTextBox1";
                        this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
                        this.cmxTextBox1.Size = new System.Drawing.Size(147, 28);
                        this.cmxTextBox1.TabIndex = 2;
                        this.cmxTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
                        // 
                        // buttonEnter
                        // 
                        this.buttonEnter.Location = new System.Drawing.Point(152, 69);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonEnter.Size = new System.Drawing.Size(40, 28);
                        this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
                        this.buttonEnter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonEnter_MouseDown);
                        this.buttonEnter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonEnter_MouseUp);
                        this.buttonEnter.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.buttonEnter.Image = global::Resources.Graphics.Skin.manual_entry_ok;
                        this.buttonEnter.TransparentColor = Color.White;
                        // 
                        // buttonCancel
                        // 
                        this.buttonCancel.Location = new System.Drawing.Point(195, 69);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonCancel.Size = new System.Drawing.Size(40, 28);
                        this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
                        this.buttonCancel.MouseDown += new MouseEventHandler(buttonCancel_MouseDown);
                        this.buttonCancel.MouseUp += new MouseEventHandler(buttonCancel_MouseUp);
                        this.buttonCancel.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.buttonCancel.Image = global::Resources.Graphics.Skin.manual_entry_cancel;
                        this.buttonCancel.TransparentColor = Color.White;


                        // 
                        // labelConfirm
                        // 
                        this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
                        this.labelConfirm.ForeColor = System.Drawing.Color.Black;
                        this.labelConfirm.Location = new System.Drawing.Point(3, 54);
                        //this.labelConfirm.Name = "labelConfirm";
                        this.labelConfirm.Size = new System.Drawing.Size(234, 12);
                        if (m_photoCapture != null)
                        {
                            switch (m_photoCapture.taskType)
                            {
                                case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
                                    this.labelConfirm.Text = PhotoCaptureResource.Text_HousebillConfirm;
                                    break;
                                case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
                                    this.labelConfirm.Text = PhotoCaptureResource.Text_MasterbillConfirm;
                                    break;
                            }
                        }


                        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

                        this.Controls.Add(this.labelConfirm);
                        this.Controls.Add(this.cmxTextBox1);
                        this.Controls.Add(this.buttonEnter);
                        this.Controls.Add(this.buttonCancel);



                        this.ResumeLayout();
                    }
                    ////////////////////////////////
                    labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                    Cursor.Current = Cursors.Default;

                    this.Height = buttonEnter.Bottom + 4;
                    cmxTextBox1.SelectAll();
                    cmxTextBox1.Focus();
                    //buttonEnterClicked = false;
                }
                else
                {
                    //if (this.ClientRectangle.Width == 480)
                    //    this.Height = 112;// previousHeight;
                    //else
                    //    this.Height = 56;
                    Height = previousHeight;

                    if (m_firstTimeSelection)
                        labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;
                }
            }
            //else
            //{
            //    Cursor.Current = Cursors.WaitCursor;
            //    if (OnEnterClick != null)
            //        OnEnterClick(this, EventArgs.Empty);


            //}
        }

        void buttonCancel_MouseUp(object sender, MouseEventArgs e)
        {
            buttonCancel.Image = global::Resources.Graphics.Skin.manual_entry_cancel; 
        }

        void buttonCancel_MouseDown(object sender, MouseEventArgs e)
        {
            buttonCancel.Image = global::Resources.Graphics.Skin.manual_entry_cancel_over;
        }

        void buttonCancel_Click(object sender, EventArgs e)
        {
            cmxTextBox1.Text = "";
            //SelectedChanged(false);
        }
        bool buttonEnterClicked = false;
        private void buttonEnter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            buttonEnter.Image = global::Resources.Graphics.Skin.manual_entry_ok_over;
            buttonEnter.Refresh();

            if (m_photoCapture.actualBill.ToLower() == cmxTextBox1.Text.ToLower())
            {
                if (buttonEnterClicked == false)
                {
                    buttonEnterClicked = true;
                    if (m_parent.DisplayReasons(m_photoCapture) == false)
                        buttonEnterClicked = false;
                   
                }
                //Cursor.Current = Cursors.Default;
            }
            else
            {
                
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Housebill does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                buttonEnter.Image = global::Resources.Graphics.Skin.manual_entry_ok;
                buttonEnter.Refresh();
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();
            }
            

        }
        private void buttonEnter_MouseDown(object sender, MouseEventArgs e)
        {
            buttonEnter.Image = global::Resources.Graphics.Skin.manual_entry_ok_over;
        }

        private void buttonEnter_MouseUp(object sender, MouseEventArgs e)
        {
            buttonEnter.Image = global::Resources.Graphics.Skin.manual_entry_ok;
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            //if (masterbillViewer == null)
            //{
            //    masterbillViewer = new CargoMatrix.Viewer.HousebillViewer(m_photoCapture.carrier, m_photoCapture.actualBill);
                
                
            //}
            //masterbillViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);// CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            //masterbillViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height -  CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(masterbillViewer);
            m_parent.DisplayBillViewer(m_photoCapture.carrier, m_photoCapture.actualBill);
            


        }
        private void buttonDetails_MouseDown(object sender, MouseEventArgs e)
        {
            buttonDetails.Image = PhotoCaptureResource.manual_entry_zoom_over;
        }

        private void buttonDetails_MouseUp(object sender, MouseEventArgs e)
        {
            buttonDetails.Image = PhotoCaptureResource.manual_entry_zoom;
        }
        public string ActualBill
        {
            get { return m_photoCapture.actualBill; }
        }

        private void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (this.m_selected)
                        buttonEnter_Click(sender, EventArgs.Empty);
                    break;
            }


        }

        private void InitComponent2()
        {
            this.title.Top = 5;
            itemPicture.Top = (Height - itemPicture.Height) / 2;
        }

        //private void buttonDetails_Click_1(object sender, EventArgs e)
        //{

        //}

        //private void buttonDetails_MouseDown_1(object sender, MouseEventArgs e)
        //{

        //}

        //private void buttonDetails_MouseUp_1(object sender, MouseEventArgs e)
        //{

        //}

    }
}












// 
            