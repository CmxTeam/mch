﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class CMXCameraControl 
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            StopAcquisition();
            ///////////////////
            if (triggerList != null)
            {
                foreach (Symbol.ResourceCoordination.Trigger trigger
                                 in triggerList)
                {
                    if (trigger != null)
                        trigger.Dispose();
                }
                triggerList.Clear();
                triggerList = null;
            }
            if (imager != null)
            {
                imager.Dispose();
                imager = null;
            }

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cameraPictureBox = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // cameraPictureBox
            // 
            this.cameraPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraPictureBox.Location = new System.Drawing.Point(0, 0);
            this.cameraPictureBox.Name = "cameraPictureBox";
            this.cameraPictureBox.Size = new System.Drawing.Size(240, 180);
            this.cameraPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cameraPictureBox.Click += new System.EventHandler(this.pictureBox1_Click);
            this.cameraPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // CMXCameraControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.cameraPictureBox);
            this.Name = "CMXCameraControl";
            this.Size = new System.Drawing.Size(240, 180);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CMXCameraControl_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox cameraPictureBox;
    }
}
