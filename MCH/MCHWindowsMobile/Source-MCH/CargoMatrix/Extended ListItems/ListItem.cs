﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class ListItem : UserControl, IExtendedListItem
    {
        private int m_ID = -1;
        protected Color selectionColor = Color.Maroon;
        protected Color focusedColor = Color.Gainsboro;
        protected Color m_backColor = Color.White;
        protected Color backColor;
        protected bool m_selected = false;
        private bool m_firstTimeFocus = false;

        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelBottom;


        public ListItem()
        {
            InitializeComponent();
            //panelBottom.BackColor = panelTop.BackColor = panelLeft.BackColor = panelRight.BackColor = selectionColor;
            //panelRight.Visible = panelLeft.Visible = panelTop.Visible = panelBottom.Visible = false;
            backColor = BackColor;
        }

        #region IExtendedListItem Members
        public virtual void SetBackColor(Color color)
        {
            m_backColor = color;
            this.backColor = color;

        }
        public virtual void SetID(int id)
        {
            m_ID = id;
        }
        public virtual int GetID()
        {
            return m_ID;
        }

        public virtual void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;
            

            if (isSelected)
            {
                if (m_firstTimeFocus == false)
                {
                    m_firstTimeFocus = true;

                    this.panelLeft = new System.Windows.Forms.Panel();
                    this.panelRight = new System.Windows.Forms.Panel();
                    this.panelTop = new System.Windows.Forms.Panel();
                    this.panelBottom = new System.Windows.Forms.Panel();

                    this.SuspendLayout();

                    // 
                    // panelLeft
                    // 
                    this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
                    this.panelLeft.Location = new System.Drawing.Point(0, 0);
                    this.panelLeft.Name = "panelLeft";
                    this.panelLeft.Size = new System.Drawing.Size(1, 1);
                    // 
                    // panelRight
                    // 
                    this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
                    this.panelRight.Location = new System.Drawing.Point(239, 0);
                    this.panelRight.Name = "panelRight";
                    this.panelRight.Size = new System.Drawing.Size(1, 1);
                    // 
                    // panelTop
                    // 
                    this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
                    this.panelTop.Location = new System.Drawing.Point(1, 0);
                    this.panelTop.Name = "panelTop";
                    this.panelTop.Size = new System.Drawing.Size(1, 1);
                    // 
                    // panelBottom
                    // 
                    this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
                    this.panelBottom.Location = new System.Drawing.Point(1, 74);
                    this.panelBottom.Name = "panelBottom";
                    this.panelBottom.Size = new System.Drawing.Size(1, 1);

                    //this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                    //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

                    this.Controls.Add(this.panelBottom);
                    this.Controls.Add(this.panelTop);
                    this.Controls.Add(this.panelRight);
                    this.Controls.Add(this.panelLeft);
                    //panelBottom.BringToFront();
                    panelBottom.SendToBack();
                    panelTop.SendToBack();
                    panelRight.SendToBack();
                    panelLeft.SendToBack();
                    panelBaseLine.SendToBack();
                    this.ResumeLayout(false);

                }

                panelBottom.BackColor = panelTop.BackColor = panelLeft.BackColor = panelRight.BackColor = selectionColor;
                panelRight.Visible = panelLeft.Visible = panelTop.Visible = panelBottom.Visible =  true;
                //BackColor = System.Drawing.Color.Linen;// Color.AliceBlue;// onOddRow ? Color.AliceBlue : Color.LightBlue;
            }
            else
            {
                if(m_firstTimeFocus == true)
                    panelRight.Visible = panelLeft.Visible = panelTop.Visible = panelBottom.Visible = false;
                //BackColor = SystemColors.ControlLight;// onOddRow ? Color.White : Color.Silver;
            }
        }
        protected void SelectionWidth(int width)
        {
            SelectedChanged(true);
            panelTop.Height = panelBottom.Height = width;
            panelLeft.Width = panelRight.Width = width;
            
        }

        public virtual void PositionChanged(int index)
        {
            
        }
        public virtual void Focus(bool focused)
        {
            if (focused)
                BackColor = focusedColor;
            else
                BackColor = backColor;

            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }
            //this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;
            
        }
        public Color FocusedColor
        {
            set
            {
                
                focusedColor = value;
                
            }
        
        }
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

    }
}
