﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class CheckBox : StandardListItem // UserControl
    {
        public CheckBox()
        {
            InitializeComponent();
            itemPicture.Image = null;// ListItemsResource.Notepad_Information;
            
        }
        public CheckBox(string text)
        {
            InitializeComponent();
            this.title.Text = text;
            this.Name = text;
            this.itemPicture.Image = null;// ListItemsResource.Notepad_Information; 

            //this.FocusedColor = Color.WhiteSmoke; ;// Color.Silver;//.IndianRed;//.Khaki;//.LightYellow;
            this.backColor = Color.White;
            //if (itemPicture.Image != null)
            {
                //Bitmap bmp = ListItemsResource.Check_In;// new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = Color.White; ;// bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }
        }
        public override void SelectedChanged(bool isSelected)
        {
            if (isSelected)
            {
                //pictureBoxCheck.Image = ListItemsResource.Symbol_Check_2;
                itemPicture.Image = ListItemsResource.Check_In;
            }
            else
            {
                itemPicture.Image = null;// ListItemsResource.Notepad_Information;
                //pictureBoxCheck.Image = null;

            }

            //base.SelectedChanged(isSelected);
        }
    }
}
