﻿namespace SmoothListBox.UI.ListItems
{
    partial class CounterListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmxCounterIcon1 = new CargoMatrix.UI.CMXCounterIcon();
            this.labelTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmxCounterIcon1
            // 
            this.cmxCounterIcon1.BackColor = System.Drawing.Color.White;
            this.cmxCounterIcon1.Location = new System.Drawing.Point(2, 2);
            this.cmxCounterIcon1.Name = "cmxCounterIcon1";
            this.cmxCounterIcon1.Size = new System.Drawing.Size(36, 36);
            this.cmxCounterIcon1.TabIndex = 0;
            this.cmxCounterIcon1.Value = 0;
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelTitle.Location = new System.Drawing.Point(44, 7);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(186, 15);
            this.labelTitle.Text = "label1";
            // 
            // CounterListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.cmxCounterIcon1);
            this.Name = "CounterListItem";
            this.Size = new System.Drawing.Size(240, 40);
            this.ResumeLayout(false);

        }

        #endregion

        private CargoMatrix.UI.CMXCounterIcon cmxCounterIcon1;
        private System.Windows.Forms.Label labelTitle;
    }
}
