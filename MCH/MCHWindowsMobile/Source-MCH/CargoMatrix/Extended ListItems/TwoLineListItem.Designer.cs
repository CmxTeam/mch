﻿namespace SmoothListBox.UI.ListItems
{
    partial class TwoLineListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelLine2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelLine2
            // 
            this.LabelLine2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.LabelLine2.ForeColor = System.Drawing.Color.Gray;
            this.LabelLine2.Location = new System.Drawing.Point(41, 26);
            this.LabelLine2.Name = "LabelLine2";
            this.LabelLine2.Size = new System.Drawing.Size(196, 12);
            // 
            // TwoLineListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.LabelLine2);
            this.Name = "TwoLineListItem";
            this.Size = new System.Drawing.Size(240, 40);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label LabelLine2;

        //protected System.Windows.Forms.PictureBox itemPicture;
    }
}
