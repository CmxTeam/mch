﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class MainMenuItem : StandardListItem
    {
        int m_actionID;
        string originalText = null;
        
        public MainMenuItem()
        {
            InitializeComponent();
            
        }

        //public MainMenuItem(string label, int count, int actionID, string image)
        public MainMenuItem(CargoMatrix.Communication.DTO.MainMenuItem menuItem)
        {
            InitializeComponent();
            this.title.Text = menuItem.Label;
            m_actionID = menuItem.ActionID;
            Count = Convert.ToInt32(menuItem.NotCompleted);
            labelDescription.Text = "Not Assigned: " + menuItem.NotAssigned;// +", Completed: " + menuItem.Completed + "/" + menuItem.TotalCount;
            //this.FocusedColor = Color.WhiteSmoke; //Color.Khaki;//.LightYellow;
            if (menuItem.Icon != null)
            {
                this.itemPicture.Image = (Image)ListItemsResource.ResourceManager.GetObject(menuItem.Icon);
                //Bitmap bmp = new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = Color.White; ;// bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }
        }
        public int ActionID
        {
            set { m_actionID = value; }
            get { return m_actionID; }
        }

        public int Count
        {
            set 
            {
                int count = value;

                if (originalText == null)
                    originalText = title.Text;
                title.Text = originalText;
                if (count > 0)
                {
                    title.Text += " (" + Convert.ToString(count) + ")";
                    title.Font = new Font(FontFamily.GenericSansSerif, title.Font.Size, FontStyle.Bold);
                }
                else
                {
                    title.Text += " (" + Convert.ToString(count) + ")";
                    title.Font = new Font(FontFamily.GenericSansSerif, title.Font.Size, FontStyle.Regular);
                }
            }
            
        }
    }
}
