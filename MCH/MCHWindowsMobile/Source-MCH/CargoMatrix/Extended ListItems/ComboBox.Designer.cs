﻿namespace SmoothListBox.UI.ListItems
{
    partial class ComboBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxCombo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.title = new System.Windows.Forms.Label();
            this.pictureBoxDottedLine = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelHeading = new System.Windows.Forms.Label();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxCombo)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.itemPicture)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxDottedLine)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCombo
            // 
            this.pictureBoxCombo.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBoxCombo.Location = new System.Drawing.Point(3, 10);
            this.pictureBoxCombo.Name = "pictureBoxCombo";
            this.pictureBoxCombo.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxCombo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCombo.TransparentColor = System.Drawing.Color.White;
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(21, 7);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(48, 19);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(187, 15);
            this.title.Text = "<title>";
            // 
            // pictureBoxDottedLine
            // 
            this.pictureBoxDottedLine.BackColor = System.Drawing.Color.White;
            this.pictureBoxDottedLine.Location = new System.Drawing.Point(3, 26);
            this.pictureBoxDottedLine.Name = "pictureBoxDottedLine";
            this.pictureBoxDottedLine.Size = new System.Drawing.Size(16, 12);
            this.pictureBoxDottedLine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDottedLine.TransparentColor = System.Drawing.Color.White;
            this.pictureBoxDottedLine.Visible = false;
            // 
            // labelHeading
            // 
            this.labelHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHeading.ForeColor = System.Drawing.Color.Gray;
            this.labelHeading.Location = new System.Drawing.Point(48, 2);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(187, 15);
            this.labelHeading.Text = "<title>";
            // 
            // ComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelHeading);
            this.Controls.Add(this.pictureBoxDottedLine);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.pictureBoxCombo);
            this.Name = "ComboBox";
            this.Size = new System.Drawing.Size(238, 38);
            this.Resize += new System.EventHandler(this.ComboBox_Resize);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxCombo)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.itemPicture)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxDottedLine)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected OpenNETCF.Windows.Forms.PictureBox2 pictureBoxCombo;
        protected OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        public System.Windows.Forms.Label title;
        protected OpenNETCF.Windows.Forms.PictureBox2 pictureBoxDottedLine;
        public System.Windows.Forms.Label labelHeading;
    }
}
