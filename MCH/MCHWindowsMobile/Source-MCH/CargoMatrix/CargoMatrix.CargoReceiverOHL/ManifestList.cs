﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoReceiver;
using SmoothListbox;
using SmoothListbox.ListItems;
using CargoMatrix.Utilities;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class ManifestList : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        protected string filter = MawbFilter.NOT_COMPLETED;
        protected string sort = MawbSort.CARRIER;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        private CustomListItems.OptionsListITem filterOption;
        protected System.Windows.Forms.Label label1;
        protected CargoMatrix.UI.CMXPictureButton buttonFilter;

        public ManifestList()
        {

            InitializeComponent();
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_MenuItemClicked);
            this.ListItemClicked += new ListItemClickedHandler(ManifestList_ListItemClicked);
        }


        void ManifestList_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is ListItemWithButton)
                DataProvider.Title = (listItem as ListItemWithButton).title.Text;
            else
                DataProvider.Title = "ZHR-724-83137390-JFK";
            UI.CMXAnimationmanager.DisplayForm(new OnHandDetails());
        }


        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();
            {
                Cursor.Current = Cursors.WaitCursor;
                this.label1.Text = string.Format("{0}/{1}", filter, sort);
                this.searchBox.Text = string.Empty;
                this.Refresh();
                Cursor.Current = Cursors.WaitCursor;


                smoothListBoxMainList.AddItemToFront(new StaticListItem("Receive New", Resources.Icons.Notepad_Add));

                var items = DataProvider.GetManifestList();
                this.TitleText = string.Format("CargoReceiver - ({0})", items.Length);


                foreach (var item in items)
                {
                    var listItem = new ListItemWithButton(item.ManifestNo, item.Line2, item.Logo, CargoMatrix.Resources.Skin.printerButton, CargoMatrix.Resources.Skin.printerButton_over);
                    smoothListBoxMainList.AddItem2(listItem);
                }

                smoothListBoxMainList.LayoutItems();
                smoothListBoxMainList.RefreshScroll();
                Cursor.Current = Cursors.Default;
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private void InitializeComponent()
        {
            this.buttonFilter = new CargoMatrix.UI.CMXPictureButton();
            this.panelHeader2.SuspendLayout();
            //
            // searchbox
            //
            this.searchBox = new CargoMatrix.UI.CMXTextBox();
            this.searchBox.Location = new System.Drawing.Point(4, 21);
            this.searchBox.Size = new System.Drawing.Size(196, 23);
            this.searchBox.TextChanged += new System.EventHandler(searchBox_TextChanged);
            //
            //label1
            //
            this.label1 = new System.Windows.Forms.Label();
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Size = new System.Drawing.Size(198, 15);
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //
            //
            this.buttonFilter.Location = new System.Drawing.Point(202, 10);
            this.buttonFilter.Size = new System.Drawing.Size(32, 32);
            this.buttonFilter.Image = CargoMatrix.Resources.Skin.filter_btn;
            this.buttonFilter.PressedImage = CargoMatrix.Resources.Skin.filter_btn_over;
            this.buttonFilter.Click += new System.EventHandler(buttonFilter_Click);
            //
            //
            //
            this.panelHeader2.Controls.Add(searchBox);
            this.panelHeader2.Controls.Add(buttonFilter);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            this.panelHeader2.Controls.Add(label1);
            panelHeader2.Height = 49;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);

        }

        private void buttonFilter_Click(object sender, System.EventArgs e)
        {
            ShowFilterPopup();
        }


        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Mawb);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter + " / " + sort;
                LoadControl();
                return true;
            }
            return false;
        }

        private void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void MawbList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        protected void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
        }
    }
}
