﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class PackageItem<T> : SmoothListbox.ListItems.ListItem<T>
    {
        public event EventHandler ButtonEditClick;
        public event EventHandler ButtonDeleteClick;

        public Image Logo
        {
            get { return pictureBox1.Image; }
            set { pictureBox1.Image = value; }
        }
        public string Line1
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }
        public string Line2
        {
            get { return label2.Text; }
            set { label2.Text = value; }
        }

        public PackageItem(T itemData)
            : base(itemData)
        {
            InitializeComponent();
            Logo = CargoMatrix.Resources.Skin.Freight_Car;
            ItemData = itemData;
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (ButtonEditClick != null)
                ButtonEditClick(this, e);
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ButtonDeleteClick != null)
                ButtonDeleteClick(this, e);
        }

    }

}
