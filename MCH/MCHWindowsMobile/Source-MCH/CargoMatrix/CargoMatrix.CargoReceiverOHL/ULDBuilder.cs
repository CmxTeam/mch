﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomUtilities;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using CargoMatrix.UI;
using System.Linq;
using CargoMatrix.FreightPhotoCapture;
namespace CargoMatrix.CargoReceiverOHL
{
    public partial class DEmoULDBuilder : CargoUtilities.SmoothListBox2Options
    {

        private CustomListItems.HeaderItem headerItem;
        private bool photoCaptured = false;


        public DEmoULDBuilder()
        {

            InitializeComponent();
            photoCaptured = false;
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label3 = "ADD/EDIT PIECES";
            headerItem.Label2 = DataProvider.Title;
            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            headerItem.Location = panel1.Location;
            panel1.Visible = true;
            this.panel1.Controls.Add(headerItem);
            headerItem.BringToFront();
            panel1.Height = headerItem.Height;
            panelHeader2.Height = headerItem.Height;

            this.TitleText = "CargoLoader ULD Setup";

            LoadOptionsMenu += new EventHandler(ULDBuilder_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ULDBuilder_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DEmoULDBuilder_ListItemClicked);
            this.ButtonFinalizeText = "Next";
        }

        void DEmoULDBuilder_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //if (listItem is PackageItem<DumbULD>)
            //{
            //    if ((listItem as PackageItem<DumbULD>).ItemData.IsLoose)
            //    {
            //        var popup = new Popups.LoosePiecesPopup();
            //        if (DialogResult.OK == popup.ShowDialog())
            //        {
            //            (listItem as PackageItem<DumbULD>).ItemData.Pieces = popup.NoOfPieces;
            //            LoadControl();
            //        }
            //    }
            //}
        }

        void ULDBuilder_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }
            }
        }

        private void DisplayExistingULDS()
        {
            smoothListBoxMainList.RemoveAll();
            foreach (var item in DataProvider.Packages)
            {
                PackageItem<DumbPackage> pckg = new PackageItem<DumbPackage>(item);
                pckg.Line1 = item.Package.Name;
                pckg.Line2 = "Pieces : " + item.Pieces.ToString();
                pckg.ButtonDeleteClick += new EventHandler(Ulditem_ButtonDeleteClick);
                pckg.ButtonEditClick += new EventHandler(Ulditem_ButtonEditClick);
                smoothListBoxMainList.AddItem(pckg);
            }
        }

        void ULDBuilder_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL));
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            photoCaptured = true;
            Reasons reasonsControl = null;
            ReasonsLogic.DisplayReasons(string.Empty, "2dq4125", CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL, this, ref reasonsControl);

        }


        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            PackageEditor.Instance.AddPackage();
            LoadControl();
        }

        public override void LoadControl()
        {
            if (!photoCaptured)
            {
                BarcodeEnabled = false;
                DisplayExistingULDS();

                Cursor.Current = Cursors.Default;
                if (smoothListBoxMainList.ItemsCount == 0)
                {
                    ShowContinueButton(false);

                }
                else
                {
                    ShowContinueButton(false);
                    ShowContinueButton(true);
                }
            }
            else
            {
                photoCaptured = false;
                ShowContinueButton(true);
                Application.DoEvents();
                if (DisplayPrinterPopups() == false)
                    return;
                if (DialogResult.OK == UI.CMXMessageBox.Show("Would you like to continue with OnhandProcess?", "Continue OnHand", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    StageManifest();
                }
            }
        }
        private void StageManifest()
        {
            CustomUtilities.ScanEnterPopup_old consolStagePop = new CustomUtilities.ScanEnterPopup_old(BarcodeType.All);
            consolStagePop.Title = DataProvider.Title;
            consolStagePop.TextLabel = "Scan or Enter Drop Location";
            if (DialogResult.OK == consolStagePop.ShowDialog())
            {
                UI.CMXAnimationmanager.DisplayDefaultForm();
            }
            else
                LoadControl();
        }
        private bool DisplayPrinterPopups()
        {
            //Cursor.Current = Cursors.Default;
            //CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            //CntMsg.HeaderText = "Confirm count";
            //CntMsg.LabelDescription = "Confirm Print Count";
            //CntMsg.LabelReference = string.Format("{0} has been received successfully", DataProvider.Title);
            //CntMsg.MinPiecesCount = 1;
            //CntMsg.PieceCount = 10;

            return DialogResult.OK == new PrintLabelsPopup().ShowDialog();
        }


        void Ulditem_ButtonEditClick(object sender, EventArgs e)
        {
            if (sender is PackageItem<DumbPackage>)
            {
                PackageItem<DumbPackage> item = sender as PackageItem<DumbPackage>;
                PackageEditor.Instance.EditPackage(item.ItemData);
                LoadControl();
            }
        }



        void Ulditem_ButtonDeleteClick(object sender, EventArgs e)
        {
            if (PackageEditor.Instance.DeletePackage((sender as PackageItem<DumbPackage>).ItemData))
                LoadControl(); // if delete confirmed reload the uld items

            if (smoothListBoxMainList.ItemsCount == 0)
                ShowContinueButton(false);
        }

    }
}
