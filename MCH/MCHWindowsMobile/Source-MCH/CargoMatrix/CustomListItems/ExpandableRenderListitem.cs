﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
namespace CustomListItems
{
    public abstract partial class ExpandableRenderListitem<T> : RenderItemBase<T>
    {
        public event EventHandler OnEnterClick;
        protected Panel panelManualEntry;
        protected CargoMatrix.UI.CMXTextBox TextBox1;
        protected CargoMatrix.UI.CMXPictureButton buttonEnter;
        protected CargoMatrix.UI.CMXPictureButton buttonCancel;
        protected bool m_selected = false;
        protected bool m_firstTimeSelection = false;
        protected int previousHeight;
        protected System.Windows.Forms.Label labelConfirm;
        protected bool isExpanded = false;

        protected virtual bool ExpandCondition()
        {
            return !CargoMatrix.Communication.Utilities.IsAdmin;
        }
        protected abstract bool ButtonEnterValidation();
        public string LabelConfirmation { get; set; }
        public ExpandableRenderListitem(T item):base(item)
        {
            InitializeComponent();
            previousHeight = this.Height;
        }

        public override void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;

            base.SelectedChanged(isSelected);

            if (ExpandCondition())
            {
                if (isSelected)
                {
                    if (m_firstTimeSelection == false)
                    {
                        m_firstTimeSelection = true;

                        this.SuspendLayout();
                        this.panelManualEntry = new Panel();
                        this.panelManualEntry.Size = new Size(this.Width, 43);
                        int newTop = (int)((this.Height - 1) / (this.CurrentAutoScaleDimensions.Height / 96F));
                        this.panelManualEntry.Location = new Point(0, newTop);
                        panelManualEntry.SuspendLayout();

                        this.TextBox1 = new CargoMatrix.UI.CMXTextBox();
                        this.buttonEnter = new CargoMatrix.UI.CMXPictureButton();
                        this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
                        this.labelConfirm = new System.Windows.Forms.Label();
                        // cmxTextBox1
                        // 
                        this.TextBox1.Location = new System.Drawing.Point(3, 14);
                        this.TextBox1.Size = new System.Drawing.Size(147, 28);
                        this.TextBox1.TabIndex = 2;
                        this.TextBox1.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
                        this.TextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
                        this.TextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
                        // 
                        // buttonEnter
                        // 
                        this.buttonEnter.Location = new System.Drawing.Point(152, 14);
                        this.buttonEnter.Size = new System.Drawing.Size(40, 28);
                        this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
                        this.buttonEnter.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                        this.buttonEnter.PressedImage = Resources.Graphics.Skin.manual_entry_ok_over;
                        this.buttonEnter.TransparentColor = Color.White;
                        // 
                        // buttonCancel
                        // 
                        this.buttonCancel.Location = new System.Drawing.Point(195, 14);
                        this.buttonCancel.Size = new System.Drawing.Size(40, 28);
                        this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
                        this.buttonCancel.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.buttonCancel.Image = Resources.Graphics.Skin.manual_entry_cancel;
                        this.buttonCancel.PressedImage = Resources.Graphics.Skin.manual_entry_cancel_over;
                        this.buttonCancel.TransparentColor = Color.White;
                        // 
                        // labelConfirm
                        // 
                        this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
                        this.labelConfirm.ForeColor = System.Drawing.Color.Black;
                        this.labelConfirm.Location = new System.Drawing.Point(3, 0);
                        this.labelConfirm.Size = new System.Drawing.Size(234, 12);
                        this.labelConfirm.Text = LabelConfirmation;




                        this.panelManualEntry.Controls.Add(this.labelConfirm);
                        this.panelManualEntry.Controls.Add(this.TextBox1);
                        this.panelManualEntry.Controls.Add(this.buttonEnter);
                        this.panelManualEntry.Controls.Add(this.buttonCancel);

                        this.Controls.Add(panelManualEntry);
                        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                        this.panelManualEntry.ResumeLayout();
                        this.ResumeLayout();
                        isExpanded = true;
                    }

                    labelConfirm.Visible = TextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                    Cursor.Current = Cursors.Default;

                    this.Height = panelManualEntry.Bottom + 4;
                    this.Dirty = true;
                    this.panelManualEntry.BackColor = this.BackColor;
                    TextBox1.SelectAll();
                    TextBox1.Focus();
                }
                else
                {
                    this.Collapse(false);
                }

            }
            for (Control control = this.Parent; control != null; control = control.Parent)
            {
                if (control is SmoothListbox.SmoothListBoxBase)
                {
                    (control as SmoothListbox.SmoothListBoxBase).RefreshScroll();
                    break;
                }
            }
        }



        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
        }
        protected void buttonEnter_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            char[] whiteSpaces = " ".ToCharArray();
            TextBox1.Text = TextBox1.Text.TrimStart(whiteSpaces);
            TextBox1.Text = TextBox1.Text.TrimEnd(whiteSpaces);
            TextBox1.Text = TextBox1.Text.ToUpper();

            if (ButtonEnterValidation())
            {
                Cursor.Current = Cursors.Default;
                if (OnEnterClick != null)
                    OnEnterClick(this, e);

            }
            else
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Text entered does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                TextBox1.SelectAll();
                TextBox1.Focus();
            }
        }

        protected void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (this.m_selected)
                        buttonEnter_Click(sender, EventArgs.Empty);
                    break;
            }
        }

        public void Collapse(bool clearText)
        {
            Height = previousHeight;
            isExpanded = false;
            if (m_firstTimeSelection)
            {
                labelConfirm.Visible = TextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;

                if (clearText == true)
                {
                    this.TextBox1.Text = string.Empty;
                }
            }
        }

        protected abstract override void Draw(Graphics gOffScreen);

    }
}