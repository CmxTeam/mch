﻿using System;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.CargoDischarge;
using SmoothListbox;
using CMXExtensions;

namespace CustomListItems
{
    public partial class DischargeHAWBListItem : ExpandableListItem
    {
        IDischargeHouseBillItem houseBillItem;

        public IDischargeHouseBillItem HouseBillItem
        {
            get { return houseBillItem; }
            set { houseBillItem = value; }
        }

        public DischargeHAWBListItem(IDischargeHouseBillItem houseBillItem)
        {
            InitializeComponent();

            this.houseBillItem = houseBillItem;
            this.title.Text = houseBillItem.Reference();
            this.labelLine2.Text = houseBillItem.CompanyName;
            this.labelLine3.Text = houseBillItem.DriverFullName;
            this.labelLine4.Text = string.Format("Pieces: {0},  Loc: {1}",
                                                  houseBillItem.TotalPieces,
                                                  houseBillItem.Location);

            this.CutoffTime = houseBillItem.ReleaseTime;
         
            switch (this.houseBillItem.status)
            {
                case  HouseBillStatuses.Open:
                case  HouseBillStatuses.Pending:
                    Logo = CustomListItemsResource.Clipboard;
                    break;
                case  HouseBillStatuses.InProgress:
                    Logo = CustomListItemsResource.History;
                    break;
                case  HouseBillStatuses.Completed:
                    Logo = CustomListItemsResource.Clipboard_Check;
                    break;

            }
          
            this.LabelConfirmation = "Please enter housebill number";
            panelIndicators.Flags = houseBillItem.Flags;
            panelIndicators.PopupHeader = houseBillItem.Reference();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }

        public void FocusItem(bool isFocused)
        {
            ((IExtendedListItem)this).Focus(isFocused);
        }
        
        protected override bool ButtonEnterValidation()
        {
            return string.Equals(this.houseBillItem.HousebillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        protected override bool ExpandCondition()
        {
            return CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin;
        }
    
    }
}
