﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.CargoDischarge;
using CMXExtensions;

namespace CustomListItems
{
    public partial class DischargeListItem : ExpandableListItem
    {
        IDischargeHouseBillItem houseBillItem;
        
        public event EventHandler OnDetailsClick;
        public event EventHandler OnDamageCaptureClick;
        public event EventHandler OnMoreDetailsClick;


        public IDischargeHouseBillItem HouseBillItem
        {
            get { return houseBillItem; }
            set 
            { 
                this.houseBillItem = value;

                this.DispalyItem(this.houseBillItem);
            }
        }

        public DischargeListItem()
        {
            InitializeComponent();

            this.LabelConfirmation = "Confirm housebill number";

            this.panelCutoff.Visible = false;

            this.previousHeight = this.Height;

            this.btnDamageCapture.Click += new EventHandler(btnDamageCapture_Click);
            this.buttonDetails.Click += new EventHandler(btnDetails_Click);
            this.btnMoreDetails.Click += new EventHandler(btnMoreDetails_Click);
        }

        private void DispalyItem(IDischargeHouseBillItem houseBillItem)
        {
            this.title.Text = string.Format("{0} ({1:HH:mm})", houseBillItem.Reference(), houseBillItem.ReleaseTime);
            this.labelLine2.Text = houseBillItem.CompanyName;
            this.labelLine3.Text = houseBillItem.DriverFullName;
            this.lblSlac.Text = string.Format("{0} of {1}", houseBillItem.ScannedSlac, houseBillItem.ReleseSlac);

            if (houseBillItem.ScannedSlac == 0)
            {
                this.lblSlac.ForeColor = Color.Black;
            }
            else
            {
                this.lblSlac.ForeColor = houseBillItem.ScannedSlac != houseBillItem.ReleseSlac ? Color.Red : Color.Green;
            }


            this.lblPieces.Text = (houseBillItem.AvailablePieces + houseBillItem.ScannedPieces).ToString();
            
            this.lblReleaseSlac.Text = houseBillItem.ReleseSlac.ToString();
            this.lblLocation.Text = houseBillItem.Location;

            switch (this.houseBillItem.status)
            {
                case HouseBillStatuses.Open:
                case HouseBillStatuses.Pending:
                    this.pictureBox1.Image = CustomListItemsResource.Clipboard;
                    break;
                case HouseBillStatuses.InProgress:
                    this.pictureBox1.Image = CustomListItemsResource.History;
                    break;
                case HouseBillStatuses.Completed:
                    this.pictureBox1.Image = CustomListItemsResource.Clipboard_Check;
                    break;

            }

            this.panelIndicators.Flags = houseBillItem.Flags;
            this.panelIndicators.PopupHeader = houseBillItem.Reference();
        }

        void btnMoreDetails_Click(object sender, EventArgs e)
        {
            var moreDetails = OnMoreDetailsClick;

            if (moreDetails != null)
            {
                moreDetails(this, EventArgs.Empty);
            }
        }

        void btnDetails_Click(object sender, EventArgs e)
        {
            var details = this.OnDetailsClick;

            if (details != null)
            {
                details(this, EventArgs.Empty);
            }
        }

        void btnDamageCapture_Click(object sender, EventArgs e)
        {
            var damageCapture = this.OnDamageCaptureClick;

            if (damageCapture != null)
            {
                damageCapture(this, EventArgs.Empty);
            }
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(this.houseBillItem.HousebillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }
      
    }
}
