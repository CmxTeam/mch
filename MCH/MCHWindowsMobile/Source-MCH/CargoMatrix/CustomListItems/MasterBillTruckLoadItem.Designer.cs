﻿namespace CustomListItems
{
    partial class MasterBillTruckLoadItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcxDamage = new CargoMatrix.UI.CMXPictureButton();
            this.btnMore = new CargoMatrix.UI.CMXPictureButton();
            this.line4 = new System.Windows.Forms.Label();
            this.SuspendLayout();

            ///
            ///line4
            ///
            this.line4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.line4.Size = new System.Drawing.Size(140, 13);
            this.line4.Location = new System.Drawing.Point(41, 47);
            this.line4.Text = "DOORs:";
            
            // 
            // pcxDamage
            // 
            this.pcxDamage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.pcxDamage.Location = new System.Drawing.Point(202, 5);
            this.pcxDamage.Name = "cmxPictureButton1";
            this.pcxDamage.Image = CargoMatrix.Resources.Skin.damage;
            this.pcxDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.pcxDamage.Size = new System.Drawing.Size(32, 32);
            this.pcxDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxDamage.TransparentColor = System.Drawing.Color.White;
            this.pcxDamage.Click += new System.EventHandler(pcxDamage_Click);
            this.pcxDamage.BringToFront();

            //
            // btnMore
            //
            this.btnMore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMore.Location = new System.Drawing.Point(202, 43);
            this.btnMore.Name = "cmxPictureButton1";
            this.btnMore.Image = global::Resources.Graphics.Skin._3dots;
            this.btnMore.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.btnMore.Size = new System.Drawing.Size(32, 32);
            this.btnMore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMore.TransparentColor = System.Drawing.Color.White;
            this.btnMore.Click += new System.EventHandler(btnMore_Click);

            // 
            // MawbCargoItem
            // 
            this.Controls.Add(this.line4);
            this.Controls.Add(this.pcxDamage);
            this.Controls.Add(this.btnMore);
            this.Name = "MawbCargoItem";
            this.buttonDetails.Location = new System.Drawing.Point(3, 43);
            this.pictureBox1.Location = new System.Drawing.Point(3, 5);
            this.panelIndicators.Location = new System.Drawing.Point(41, 79);
            this.Size = new System.Drawing.Size(240, 97);
            this.title.Size = new System.Drawing.Size(160, 14);

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        private CargoMatrix.UI.CMXPictureButton pcxDamage;
        private CargoMatrix.UI.CMXPictureButton btnMore;
        private System.Windows.Forms.Label line4;

        #endregion
    }
}
