﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using SmoothListbox;

namespace CustomListItems
{
    public partial class ThreeLineItem<T> : ListItem,ISmartListItem, IDataHolder<T>
    {
        public Image Logo { get { return pictureBoxLogo.Image; } set { pictureBoxLogo.Image = value; } }
        public string Line1 { get { return label1.Text; } set { label1.Text = value; } }
        public string Line2 { get { return label2.Text; } set { label2.Text = value; } }
        public string Line3 { get { return label3.Text; } set { label3.Text = value; } }
        public Image ButtonImage { get { return button1.Image; } set { button1.Image = value; } }
        public Image ButtonPressedImage { get { return button1.PressedImage; } set { button1.PressedImage = value; } }
        public event EventHandler ButtonClicked;
        public bool ButtonVisible { get { return button1.Visible; } set { button1.Visible = value; } }
        public ThreeLineItem(T item)
            : this(item, string.Empty, string.Empty, string.Empty, null)
        {
        }

        public ThreeLineItem(T item, string line1, string line2, string line3, Image logo)
        {
            InitializeComponent();
            InitializeFields(item);
            this.label1.Text = line1;
            this.label2.Text = line2;
            this.label3.Text = line3;
            pictureBoxLogo.Image = logo ?? global::Resources.Graphics.Skin.radio_button;
        }

        void button1_Click(object sender, System.EventArgs e)
        {
            if (ButtonClicked != null)
                ButtonClicked(this, e);
        }

        //public override bool FilterContains(string filter)
        //{
        //    /// checks for the mathc in lines 1 and 2
        //    return label1.Text.ToLower().Contains(filter.ToLower()) || label2.Text.ToLower().Contains(filter.ToLower());
        //}

        public override void SelectedChanged(bool isSelected)
        {
            base.SelectedChanged(isSelected);

            base.Focus(isSelected);
        }
        #region IDataHolder<T> Members
        private DataHolder<T> dataHolder;
        private void InitializeFields(T itemData)
        {
            dataHolder = new DataHolder<T>(itemData);

        }
        public T ItemData
        {
            get
            {
                return dataHolder.ItemData;
            }
            set
            {
                dataHolder.ItemData = value;
            }
        }

        public bool OnBeforeDataItemChanged(T item)
        {
            return true;
        }

        public void OnDataItemChanged()
        {

        }

        #endregion

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            return label1.Text.ToLower().Contains(text.ToLower()) || label2.Text.ToLower().Contains(text.ToLower());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
