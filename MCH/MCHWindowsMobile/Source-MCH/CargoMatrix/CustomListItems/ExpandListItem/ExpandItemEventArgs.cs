﻿using System;
namespace CustomListItems
{
    public class ExpandItemEventArgs : EventArgs
    {
        public ExpandItemEventArgs(int id, bool selected)
        {
            ItemID = id;
            IsSelected = selected;
        }
        public int ItemID;
        public bool IsSelected;
        public int? ParetntId = null;
    }
}