﻿namespace CustomListItems
{
    partial class ComboBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.title = new System.Windows.Forms.Label();
            this.pictureBoxDottedLine = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelHeading = new System.Windows.Forms.Label();
            this.buttonDelte = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCollapse = new CargoMatrix.UI.CMXPictureButton();
            this.SuspendLayout();
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.ForeColor = System.Drawing.SystemColors.ControlText;
            this.itemPicture.Location = new System.Drawing.Point(4, 4);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TabIndex = 0;
            this.itemPicture.TabStop = false;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.ForeColor = System.Drawing.Color.Gray;
            this.title.Location = new System.Drawing.Point(38, 19);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(118, 15);
            this.title.Text = "<title>";
            // 
            // pictureBoxDottedLine
            // 
            this.pictureBoxDottedLine.BackColor = System.Drawing.Color.White;
            this.pictureBoxDottedLine.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pictureBoxDottedLine.Location = new System.Drawing.Point(5, 26);
            this.pictureBoxDottedLine.Name = "pictureBoxDottedLine";
            this.pictureBoxDottedLine.Size = new System.Drawing.Size(16, 12);
            this.pictureBoxDottedLine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDottedLine.TabIndex = 4;
            this.pictureBoxDottedLine.TabStop = false;
            this.pictureBoxDottedLine.TransparentColor = System.Drawing.Color.White;
            this.pictureBoxDottedLine.Visible = false;
            // 
            // labelHeading
            // 
            this.labelHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHeading.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelHeading.ForeColor = System.Drawing.Color.Black;
            this.labelHeading.Location = new System.Drawing.Point(38, 2);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(118, 15);
            this.labelHeading.Text = "<title>";
            // 
            // buttonDelte
            // 
            this.buttonDelte.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDelte.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonDelte.Image = CargoMatrix.Resources.Skin.cc_trash;
            this.buttonDelte.Location = new System.Drawing.Point(198, 3);
            this.buttonDelte.Name = "buttonDelte";
            this.buttonDelte.PressedImage = CargoMatrix.Resources.Skin.cc_trash_over;
            this.buttonDelte.Size = new System.Drawing.Size(32, 32);
            this.buttonDelte.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelte.TabIndex = 2;
            this.buttonDelte.TabStop = false;
            this.buttonDelte.TransparentColor = System.Drawing.Color.White;
            this.buttonDelte.Click += new System.EventHandler(this.buttonDelte_Click);
            // 
            // buttonCollapse
            // 
            this.buttonCollapse.BackColor = System.Drawing.SystemColors.Control;
            this.buttonCollapse.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonCollapse.Image = global::CustomListItems.CustomListItemsResource.Collapse_Down;
            this.buttonCollapse.Location = new System.Drawing.Point(162, 3);
            this.buttonCollapse.Name = "buttonCollapse";
            this.buttonCollapse.PressedImage = global::CustomListItems.CustomListItemsResource.Collapse_Down;
            this.buttonCollapse.Size = new System.Drawing.Size(32, 32);
            this.buttonCollapse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonCollapse.TabIndex = 1;
            this.buttonCollapse.TabStop = false;
            this.buttonCollapse.TransparentColor = System.Drawing.Color.White;
            this.buttonCollapse.Click += new System.EventHandler(this.buttonCollapse_Click);
            // 
            // ComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonCollapse);
            this.Controls.Add(this.buttonDelte);
            this.Controls.Add(this.labelHeading);
            this.Controls.Add(this.pictureBoxDottedLine);
            this.Controls.Add(this.title);
            this.Name = "ComboBox";
            this.Size = new System.Drawing.Size(238, 38);
            this.Resize += new System.EventHandler(this.ComboBox_Resize);
            this.ResumeLayout(false);

         }

        #endregion

        protected OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        public System.Windows.Forms.Label title;
        protected OpenNETCF.Windows.Forms.PictureBox2 pictureBoxDottedLine;
        public System.Windows.Forms.Label labelHeading;
        private CargoMatrix.UI.CMXPictureButton buttonDelte;
        private CargoMatrix.UI.CMXPictureButton buttonCollapse;
        //protected System.Windows.Forms.PictureBox pictureBoxBarcodeError;
    }
}
