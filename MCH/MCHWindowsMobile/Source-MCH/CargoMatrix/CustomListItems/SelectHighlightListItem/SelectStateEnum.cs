﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomListItems.SelectHighlightListItem
{
    public enum SelectStateEnum
    {
        NotSelected,
        PartiallySelected,
        CompletelySelected
    }
}
