﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI
{
    public partial class BillViewerShell : SmoothListbox
    {
        private int pageCount = 0;
        private int currentPageIndex = 0;
        private List<SmoothListBoxBase> m_Pages;
        private bool m_bHeaderMenu = false;
        private string m_headerText;
        
        public BillViewerShell()
        {
            InitializeComponent();
            pictureBoxHeader.Image = SmoothListBoxResource.dropdown;
            pictureBoxBookMarkPrevious.Image = SmoothListBoxResource.leftArrow;
            pictureBoxBookMarkNext.Image = SmoothListBoxResource.rightArrow;
            
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            
            this.smoothListBoxMainList.AutoScroll -= smoothListBoxMainList_AutoScroll;
            //smoothListBoxMainList.Dispose();
            //smoothListBoxMainList = null;
            //labelTitle.Height = 0;
            //labelTitle.Visible = false;
            m_Pages = new List<SmoothListBoxBase>();
            //panelBookMarks.Top =  panelTitle.Top - panelBookMarks.Height;
            panelBookMarks.Top = panelShellHeader.Bottom;
            panelBookMarks.Left = 0;
            panelBookMarks.Width = Width - 4;
            panelBookMarks.Height = Height - panelTitle.Height - panelShellHeader.Height - panelSoftkeys.Height - 4;

            foreach (Control control in smoothListBoxBookMarks.Controls)
                control.BackColor = Color.AliceBlue;
           
            
        }
        public void InsertPage(Control header, Control []itemsList)
        { 
            SmoothListBoxBase newPage = new SmoothListBoxBase();
            newPage.BorderStyle = BorderStyle.FixedSingle;
            newPage.Name = header.Name;
            newPage.Top = panelShellHeader.Height + panelTitle.Height;
            newPage.Height = Height - panelTitle.Height - panelShellHeader.Height - panelSoftkeys.Height - 4;
            newPage.Left = 0;
            newPage.Width = Width;
            newPage.AddItems(itemsList);
            //newPage.LayoutItems();
            //foreach (Control control in newPage.Controls)
                //control.BackColor = Color.Maroon;
            //newPage.LayoutItems();
            m_Pages.Add(newPage);
            AddHeader(header);
            pageCount++;
            
            Controls.Add(newPage);
            OpenPage(0);
            //newPage.BringToFront();
            //panelBookMarks.BringToFront();
            
        }
        public void Reset()
        {
            smoothListBoxBookMarks.RemoveAll();
            this.smoothListBoxMainList.RemoveAll();
            for(int i=0;i<m_Pages.Count;i++)
            {
                m_Pages[i].RemoveAll();
                m_Pages[i].Parent = null;
                m_Pages[i].Dispose();
                
            }
            m_Pages.Clear();
            pageCount = 0;
        
        }
        private void OpenBookMarksMenu()
        {
            panelBookMarks.Top = panelShellHeader.Height /*+ panelTitle.Height*/ - panelBookMarks.Height;// panelShellHeader.Bottom;// CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight;
            panelBookMarks.Visible = true;
            panelTitle.Visible = false;
            m_bHeaderMenu = true;
            smoothListBoxBookMarks.AutoScroll += BookMarksAutoScroll;

            int bookmarksMenuAnimationStep = panelBookMarks.Height / 3;
            for (int i = 0; i < 3; i++)
            {
                panelBookMarks.Top += bookmarksMenuAnimationStep;
                panelBookMarks.Refresh();
            }
        }
        private void CloseBookMarksMenu()
        {
            m_bHeaderMenu = false;
            panelBookMarks.Visible = false;
            panelTitle.Visible = true;
            smoothListBoxBookMarks.AutoScroll -= BookMarksAutoScroll;

            pictureBoxHeader.Image = SmoothListBoxResource.dropdown;
            //if (m_bOptionsMenu == true)
            //    DisappearOptionsMenu();
        }
        private void OpenNextPage()
        {
            if (m_bHeaderMenu == true)
                CloseBookMarksMenu();
            if (pageCount <= 0)
                return;
            SuspendLayout();
            int prevPageIndex = currentPageIndex;
            currentPageIndex++;
            currentPageIndex = currentPageIndex % pageCount;

            m_Pages[currentPageIndex].Left = Width;


            m_Pages[currentPageIndex].BringToFront();
            panelBookMarks.BringToFront();
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            panelOptions.BringToFront();
            panelSoftkeys.BringToFront();
            SetPageAutoScroll(m_Pages[currentPageIndex]);

            ResumeLayout();

            int step = Width / 3;
            
            for (int i = 0; i < 3; i++)
            {
                m_Pages[currentPageIndex].Left -= step;
                m_Pages[prevPageIndex].Left -= step;
                m_Pages[currentPageIndex].Refresh();
                m_Pages[prevPageIndex].Refresh();
                
            }
            m_Pages[currentPageIndex].Left = 0;

            //labelHeader.Text = m_Pages[currentPageIndex].Name;
            HeaderText = m_Pages[currentPageIndex].Name;
            smoothListBoxBookMarks.Reset();
            (smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).SelectedChanged(true);
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).Focus(true);
            smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;



        }
        private void OpenPreviousPage()
        {
            if (m_bHeaderMenu == true)
                CloseBookMarksMenu();
            
            SuspendLayout();
            int prevPageIndex = currentPageIndex;
            currentPageIndex--;
            if (currentPageIndex < 0)
                currentPageIndex = pageCount - 1;

            //m_Pages[prevPageIndex].Left = (-Width);
            m_Pages[currentPageIndex].Left = (-Width);

            m_Pages[currentPageIndex].BringToFront();
            panelBookMarks.BringToFront();
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            panelOptions.BringToFront();
            
            panelSoftkeys.BringToFront();
            SetPageAutoScroll(m_Pages[currentPageIndex]);

            ResumeLayout();

            int step = Width / 3;

            for (int i = 0; i < 3; i++)
            {
                m_Pages[currentPageIndex].Left += step;
                m_Pages[prevPageIndex].Left += step;
                m_Pages[currentPageIndex].Refresh();
                m_Pages[prevPageIndex].Refresh();

            }
            m_Pages[currentPageIndex].Left = 0;
            //labelHeader.Text = m_Pages[currentPageIndex].Name;
            HeaderText = m_Pages[currentPageIndex].Name;

            smoothListBoxBookMarks.Reset();
            (smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).SelectedChanged(true);
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).Focus(true);
            smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;
        
        }
        private void OpenPage(int pageID)
        {
            if (m_bHeaderMenu == true)
                CloseBookMarksMenu();
            
            SuspendLayout();

            m_Pages[pageID].Left = 0;
            m_Pages[pageID].BringToFront();
            panelBookMarks.BringToFront();
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            panelOptions.BringToFront();
            
            panelSoftkeys.BringToFront();
            currentPageIndex = pageID;

            SetPageAutoScroll(m_Pages[pageID]);

            //labelHeader.Text = m_Pages[currentPageIndex].Name;
            HeaderText = m_Pages[currentPageIndex].Name;
            ResumeLayout();

             smoothListBoxBookMarks.Reset();
            (smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).SelectedChanged(true);
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).Focus(true);
            smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;

        
        }
        private void AddHeader(Control header)
        {
            if (header is IExtendedListItem)
            {
                (header as IExtendedListItem).SetID(pageCount);
                (header as IExtendedListItem).SetBackColor(Color.AliceBlue);
            }
                
            smoothListBoxBookMarks.AddItem(header);
            
        }

        void ResizeBookMarksMenu(SmoothListBoxBase menu)
        {
            menu.Left = 0;// panelOptionsList.Right;
            menu.Top = 0;// pictureBoxOptionsHeader.Height + 1;
            menu.Height = panelBookMarks.Height;// panelOptionsList.Height;// panelOptions.Height - pictureBoxOptionsHeader.Height;
            menu.Width = panelBookMarks.Width;// panelOptionsList.Width;// panelOptions.Width - panelOptionsBorder2.Width - panelOptionsBorder3.Width;
            //menuOptionsOuterPanel.Left = 2;
        }
        protected override void OnResize(EventArgs e)
        {

            //if (panelTitle != null && panelShellHeader != null)
              //  panelTitle.Height = panelShellHeader.Height;

            if (panelBookMarks != null)
                panelBookMarks.Height = Height /*- panelTitle.Height*/ - panelShellHeader.Height - panelSoftkeys.Height - 4;
            //if (panelBookMarks != null)
            //    panelBookMarks.Height = Height - labelTitle.Height - panelSoftkeys.Height;// -10;
            if (smoothListBoxBookMarks != null)
                ResizeBookMarksMenu(smoothListBoxBookMarks);

            base.OnResize(e);
        }

        protected override void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseDown(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.Scroll(SmoothListBoxBase.DIRECTION.DOWN);
                else
                {
                    m_Pages[currentPageIndex].Scroll(SmoothListBoxBase.DIRECTION.DOWN);
                }
                if (pictureBoxDown.Enabled)
                {
                    pictureBoxDown.Image = SmoothListBoxResource.btnDown_over;
                }
            
            }

            

        }
        protected override void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseUp(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.DownButtonPressed = false;
                else
                {
                    m_Pages[currentPageIndex].DownButtonPressed = false;
                }
                if (pictureBoxDown.Enabled)
                {
                    pictureBoxDown.Image = SmoothListBoxResource.btnDown;
                }

            }
        }

        protected override void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseDown(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.Scroll(SmoothListBoxBase.DIRECTION.UP);
                else
                {
                    m_Pages[currentPageIndex].Scroll(SmoothListBoxBase.DIRECTION.UP);
                }
                if (pictureBoxDown.Enabled)
                {
                    pictureBoxUp.Image = SmoothListBoxResource.btnUp_over;
                }

            }
        }

        protected override void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseUp(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.UpButtonPressed = false;
                else
                {
                    m_Pages[currentPageIndex].UpButtonPressed = false;
                }
                if (pictureBoxUp.Enabled)
                {
                    pictureBoxUp.Image = SmoothListBoxResource.btnUp;
                }

            }

        }

        private void optionsTimer_Tick(object sender, EventArgs e)
        {

        }

        private void bookMarksTimer_Tick(object sender, EventArgs e)
        {
            //Todo: Animation
        }

        private void pictureBoxHeader_Click(object sender, EventArgs e)
        {
            //panelBookMarks.BringToFront();
            
            if (m_bHeaderMenu == false)
            {
                pictureBoxHeader.Image = SmoothListBoxResource.dropdown_over;
                pictureBoxHeader.Refresh();
                OpenBookMarksMenu();
                
                
            }
            else
            {
                CloseBookMarksMenu();
                
                //panelBookMarks.Visible = false;
                //m_bHeaderMenu = false;
            }
        }

        
        private void RemovePageAutoScroll()
        {
            for (int i = 0; i < m_Pages.Count; i++)
            {
                m_Pages[i].AutoScroll -= PageAutoScroll;

            }
        }
        private void SetPageAutoScroll(SmoothListBoxBase menu)
        {
            for (int i = 0; i < m_Pages.Count; i++)
            {
                m_Pages[i].AutoScroll -= PageAutoScroll;

            }
            menu.AutoScroll += PageAutoScroll;

        }
        private void PageAutoScroll(SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bHeaderMenu == false && m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                            m_Pages[currentPageIndex].UpButtonPressed = false;
                        break;
                    case SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            m_Pages[currentPageIndex].DownButtonPressed = false;
                        break;
                }

            }

        }

        private void BookMarksAutoScroll(SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bHeaderMenu == true && m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                             smoothListBoxBookMarks.UpButtonPressed = false;
                        break;
                    case SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            smoothListBoxBookMarks.DownButtonPressed = false;
                        break;
                }

            }

        }

        private void pictureBoxBookMarkNext_Click(object sender, EventArgs e)
        {
            OpenNextPage();
        }

        private void pictureBoxBookMarkPrevious_Click(object sender, EventArgs e)
        {
            OpenPreviousPage();
        }

        private void smoothListBoxBookMarks_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is IExtendedListItem)
            {
                CloseBookMarksMenu();
                //listItem.Left = 0;
                OpenPage((listItem as IExtendedListItem).GetID());

            }
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            CloseBookMarksMenu();
            base.pictureBoxMenu_Click(sender, e);
        
        }

        private void pictureBoxBookMarkPrevious_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkPrevious.Image = SmoothListBoxResource.leftArrow_over;//.back_over;
            pictureBoxBookMarkPrevious.Refresh();
            
        }

        private void pictureBoxBookMarkPrevious_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkPrevious.Image = SmoothListBoxResource.leftArrow;//.back_reg;
        }

        private void pictureBoxBookMarkNext_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkNext.Image = SmoothListBoxResource.rightArrow_over;//.forward_over;
            pictureBoxBookMarkNext.Refresh();
            
        }

        private void pictureBoxBookMarkNext_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkNext.Image = SmoothListBoxResource.rightArrow;//.forward_reg;
        }

        public string HeaderText
        {
            set 
            {
                m_headerText = value;
                pictureBoxHeader.Invalidate();
            }
        }


        Font headerFont = new Font(FontFamily.GenericSansSerif,12,FontStyle.Bold);    
        
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            //SizeF textSize = e.Graphics.MeasureString(m_headerText, headerFont);
            //int x = ((pictureBoxHeader.Width - 32) - (int)textSize.Width) / 2;
            //int y = (pictureBoxHeader.Height - (int)textSize.Height) / 2;
            
            Rectangle strRect = new Rectangle(0,0, pictureBoxHeader.Width - 32, pictureBoxHeader.Height) ;
            
            StringFormat format = new StringFormat( StringFormatFlags.NoWrap);
            //format.FormatFlags = StringFormatFlags.NoWrap;
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            e.Graphics.DrawString(m_headerText, headerFont, new SolidBrush(Color.White),strRect, format);//, x, y);
            format.Dispose();

        }

       
        
    }
}
