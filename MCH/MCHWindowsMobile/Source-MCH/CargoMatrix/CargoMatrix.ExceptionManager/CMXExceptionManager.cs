﻿using System;

using System.Collections.Generic;
using System.Text;
using CargoMatrix.UI;

namespace CargoMatrix.ExceptionManager
{
    public class CMXExceptionManager
    {
        public static void DisplayException(Exception exception, int locationCode)
        {
            string details;
            if (exception is System.Web.Services.Protocols.SoapException)
            {
                if ((exception as System.Web.Services.Protocols.SoapException).Code.Name == "GUIDException")
                {

                    CMXMessageBox.Show(ExceptionResource.GUIDException, "User Session Expired", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                    return;
                }
            }
            //if (exception is System.Net.WebException)
            //{
            //    details = exception.Message + ExceptionResource.WebException;
            //}
            //else
            {
                details = (string)ExceptionResource.ResourceManager.GetObject(exception.GetType().Name);
                if (details == null || details == "")
                    details = exception.Message + ExceptionResource.ExceptionGenericMessage;
            }
            
            CMXMessageBox.Show(details, "Error!" + " (" + locationCode + ")", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

            Logger.Log(exception, locationCode);

            
        }
    }
}
