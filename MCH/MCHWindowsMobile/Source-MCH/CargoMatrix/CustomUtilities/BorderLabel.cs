﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class BorderLabel : System.Windows.Forms.Label
    {
        public int BorderWidth { get; set; }
        public Color BorderColor { get; set; }
        
        public BorderLabel()
        {
            this.BorderWidth = 1;
            this.BorderColor = Color.Black;
          
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            
        }

        

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            using (Pen pen = new Pen(this.BorderColor, this.BorderWidth))
            {
                var rectengle = new Rectangle(this.ClientRectangle.X + this.BorderWidth, this.ClientRectangle.Y + this.BorderWidth, this.ClientRectangle.Width - this.BorderWidth, this.ClientRectangle.Height - this.BorderWidth);

                e.Graphics.DrawRectangle(pen, rectengle);
            }
        }        
    }
}
