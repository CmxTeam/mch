﻿using System;

//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class OriginDestinationMessageBox : CargoMatrix.UI.MessageBoxBase//Form
    {
        private string m_caption;
        
        private static OriginDestinationMessageBox instance = null;
        public OriginDestinationMessageBox()
        {
            InitializeComponent();
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.add_btn_over;
            this.buttonOk.Image = Resources.Graphics.Skin.add_btn;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            cmxTextBoxOrigin.Text = cmxTextBoxOrigin.Text.ToUpper();
            cmxTextBoxDestination.Text = cmxTextBoxDestination.Text.ToUpper();
            if (cmxTextBoxOrigin.Text.Trim().Length == 3 && cmxTextBoxDestination.Text.Trim().Length == 3)
                DialogResult = DialogResult.OK;
            else
                CargoMatrix.UI.CMXMessageBox.Show("Origin/Destination code is not in a valid format. Please provide 3 character Origin and 3 character Destination code.", "Error", CMXMessageBoxIcon.Exclamation);
        }
        public static DialogResult Show(string title,out int pieces, out string origin, out string destination)
        {
            if (instance == null)
                instance = new OriginDestinationMessageBox();
            
            instance.cmxTextBoxPieces.Text = string.Empty;
            instance.cmxTextBoxOrigin.Text = string.Empty;
            instance.cmxTextBoxDestination.Text = string.Empty;
            instance.cmxTextBoxPieces.Focus();
            
            instance.m_caption = title;

            return instance.CMXShowDialog(out pieces,out origin, out destination);
        }
        public static DialogResult Show2(string title,ref int pieces, ref string origin, ref string destination)
        {
            if (instance == null)
                instance = new OriginDestinationMessageBox();

 
            instance.cmxTextBoxPieces.Text = pieces.ToString();
            instance.cmxTextBoxOrigin.Text = origin;
            instance.cmxTextBoxDestination.Text = destination;
            instance.cmxTextBoxPieces.Focus();
            
            instance.m_caption = title;

            return instance.CMXShowDialog(out pieces,out origin, out destination);
        }
        private DialogResult CMXShowDialog(out int pieces,out string origin, out string destination)
        {
            DialogResult result = ShowDialog();
            origin = cmxTextBoxOrigin.Text;
            destination = cmxTextBoxDestination.Text;
            try
            {
                pieces = Convert.ToInt32(cmxTextBoxPieces.Text);
            }
            catch (Exception)
            {
                pieces = 0;
            }
            return result;
        }
       
      
        private void cmxTextBoxOrigin_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Tab:
                case Keys.Enter:
                    cmxTextBoxDestination.Focus();
                    e.Handled = true;
                    break;
            }
            if ((e.KeyValue >= 'a' && e.KeyValue <= 'z') || (e.KeyValue >= 'A' && e.KeyValue <= 'Z') || (e.KeyValue >= '0' && e.KeyValue <= '9'))
            {
                Application.DoEvents();
                
                if (cmxTextBoxOrigin.Text.Length >= 3)
                {
                    cmxTextBoxOrigin.Text = cmxTextBoxOrigin.Text.ToUpper();
                    cmxTextBoxDestination.Focus();


                }

            }
        }

        private void cmxTextBoxDestination_KeyDown(object sender, KeyEventArgs e)
        {
            if (cmxTextBoxDestination.Text.Length == 0 && e.KeyCode == Keys.Back)
                cmxTextBoxOrigin.Focus();

            switch (e.KeyCode)
            {
                case Keys.Tab:
                case Keys.Enter:
                    buttonOk.Focus();
                    e.Handled = true;
                    break;
            }
            if ((e.KeyValue >= 'a' && e.KeyValue <= 'z') || (e.KeyValue >= 'A' && e.KeyValue <= 'Z') || (e.KeyValue >= '0' && e.KeyValue <= '9'))
            {
                Application.DoEvents();
                
                if (cmxTextBoxDestination.Text.Length >= 3)
                {
                    cmxTextBoxDestination.Text = cmxTextBoxDestination.Text.ToUpper();
                    buttonOk.Focus();


                }
                
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}