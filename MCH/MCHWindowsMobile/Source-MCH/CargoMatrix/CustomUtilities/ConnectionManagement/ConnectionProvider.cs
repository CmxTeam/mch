﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
using System.Threading;

namespace CustomUtilities.ConnectionManagement
{
    public static class ConnectionProvider
    {
        private static readonly int tryToReconnectTimes = 7;
        private static ConnectionManager connectionManager;
        private static MessageWindowForm messageWindowForm;
     
        private static MessageWindowForm MessageWindowForm
        {
            get
            {
                var mWF = messageWindowForm;

                if (mWF == null)
                {
                    messageWindowForm = new MessageWindowForm();
                    messageWindowForm.Header = "Message";
                    return messageWindowForm;
                }

                return mWF;
            }
        }
        
        static ConnectionProvider()
        {
            connectionManager = new ConnectionManager();
        }

        private static DialogResult ShowDialogMessage(string text)
        {
            var messageForm = MessageWindowForm;

            messageForm.Message = text;

            return messageWindowForm.ShowDialog();
        }

        private static void ShowMessage(string text)
        {
            var messageForm = MessageWindowForm;

            messageForm.Message = text;

            if (messageWindowForm.Visible == true) return;

            messageWindowForm.Show();
        }

        private static void HideMessage()
        {
            var messageForm = MessageWindowForm;

            if (messageForm.InvokeRequired == false)
            {
                messageForm.Hide();
            }
            else
            {
                messageForm.Invoke((Action)delegate
                {
                    messageForm.Hide();
                });
            }
        }


        private static MethodExecutedEventArgs<T> ExecuteMethod<T>(Func<T> func)
        {
            MethodExecutedEventArgs<T> args = null;
            
            try
            {
                var result = func();
                args = new MethodExecutedEventArgs<T>(ExecutionResultTypeEnum.Success);
                args.Result = result;
                return args;
            }
            catch (Exception ex)
            {
                args = new MethodExecutedEventArgs<T>(ExecutionResultTypeEnum.Error);
                args.Exception = ex;
                return args;
            }
        }


        public static void ExecuteMethodCall<T>(Func<T> func, Action<MethodExecutedEventArgs<T>> resultAction)
        {
            Cursor.Current = Cursors.Default;

            MethodExecutedEventArgs<T> args = null;

            if (connectionManager.IsConnectedToInternet == true)
            {
                args = ExecuteMethod(func);
                HideMessage();
                resultAction(args);
                return;
            }

            ShowMessage("Connecting to Internet");

            for (int i = 0; i < tryToReconnectTimes; i++)
            {
                connectionManager.ReConnect();

                if (connectionManager.IsConnectedToInternet == true)
                {
                    ShowMessage("Executing");

                    args = ExecuteMethod(func);
                    HideMessage();
                    resultAction(args);
                    return;
                }
            }

            HideMessage();
            args = new MethodExecutedEventArgs<T>(ExecutionResultTypeEnum.UnableToConnect);
            resultAction(args);
        }

    }
}
