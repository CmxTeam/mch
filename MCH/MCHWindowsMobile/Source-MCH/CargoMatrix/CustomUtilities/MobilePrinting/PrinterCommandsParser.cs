﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CustomUtilities.MobilePrinting
{
    public class PrinterCommandsParser
    {
        public PrinterStatusHolder ParsePrinterStatus(byte[] data)
        {
            PrinterStatusHolder result = new PrinterStatusHolder();
            result.StatusType = PrinterStatusEnum.NA;

            var printerStatusEnquiry = System.Text.ASCIIEncoding.ASCII.GetString(data, 0, data.Length);

            string responseSkeleton = string.Format("{0}.*{1}", ASCIIDirectives.STX.ToChar(), ASCIIDirectives.ETX.ToChar());

            var match = Regex.Match(printerStatusEnquiry, responseSkeleton);

            if (match.Success == false)
            {
                if (data.Length > 0)
                {
                    int first = (int)data[0];

                    if(first == (int)PrinterStatusEnum.Error ||
                       first == (int)PrinterStatusEnum.NoError)
                    {
                        result.StatusType = (PrinterStatusEnum)first;
                    }
                }
                
                return result;
            }

            result = this.ConvertToPrinterStatus(match.Value[3]);

            match = Regex.Match(match.Value, @"\d{4}" + ASCIIDirectives.ETX.ToChar().ToString());

            if (match.Success == false) return result;

            result.RemainingPrintNumber = Convert.ToInt32(match.Value.Substring(0, 4));

            return result;
        }

        private PrinterStatusHolder ConvertToPrinterStatus(char printerStatus)
        {
            PrinterStatusHolder result = new PrinterStatusHolder();
            
            int statusAsInt = (int)printerStatus;

            bool isDefined = Enum.IsDefined(typeof(PrinterStatusEnum), statusAsInt);

            if(isDefined == true)
            {
                result.StatusType = (PrinterStatusEnum)statusAsInt;
            }
            else
            {
                result.StatusType = PrinterStatusEnum.NA;
            }

            return result;
        }

        public virtual string CreateCalibrateGAPMessage()
        {
            return "APG33A1000105600383+000+000+00+00+00000001";
        }

        public virtual string CreatePrintBarcodeMessage(int horizontalAllignment, string barcodeData, string humanReadibleText)
        {
            // result AH{0}V0030BG03100{1}H0270V0140L0304XU{2}Z"

            var commandText = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}",
                CommandHHorizontalAllignment(horizontalAllignment),
                CommandVVerticalAllignmentV(30),
                CommandBGBarcode(barcodeData, 3, 100),
                barcodeData,
                CommandHHorizontalAllignment(270),
                CommandVVerticalAllignmentV(140),
                CommandLCharacterExpention(3, 4),
                CommadXUFont(),
                humanReadibleText);

            return PrinterCommandsParser.CreateMessageToPrinter(commandText);
        }

        public virtual string CreateLabelPositionMessage()
        {
            // result AA3H001V001CS2#E5A104060812Z

            var commandText = string.Format("{0}{1}{2}{3}",
                CommandA3BaseReferencePoint(1, 1),
                CommandCSPrintSpeed(2),
                CommandEPrintDarkness(5),
                CommandA1MediaSize(0406, 0812));

            return PrinterCommandsParser.CreateMessageToPrinter(commandText);
        }

        public virtual string CreatePrinterStatusMessage()
        {
            string enqCommand = CreatePrinterCommand(ASCIIDirectives.ENQ.ToChar().ToString());
            string iDCommand = CreatePrinterCommand("ID");

            string messageText = string.Format("{0}{1}", enqCommand, iDCommand);

            return CreateMessageToPrinter(messageText);
        }

        private static string CreateMessageToPrinter(string innerText)
        {
            return string.Format("{0}{1}{2}{3}{4}", ASCIIDirectives.STX.ToChar(), CommandA(), innerText, CommandZ(), ASCIIDirectives.ETX.ToChar());
        }

        private static string CreatePrinterCommand(string commandText)
        {
            return string.Format("{0}{1}", ASCIIDirectives.ESC.ToChar(), commandText);
        }

        public static string FormatValueToLength(int value, int stringLength)
        {
            string strValue = value.ToString();

            return strValue.Length < stringLength ? string.Format("{0}{1}", new string('0', stringLength - strValue.Length), strValue) : strValue;
        }

        public static string FormatValueToLength(int value)
        {
            return FormatValueToLength(value, 4);
        }

        public static string CommandZ()
        {
            return CreatePrinterCommand("Z");
        }

        public static string CommandA()
        {
            return CreatePrinterCommand("A");
        }

        public static string CommandHHorizontalAllignment(int parameter)
        {
            var formatedParameter = FormatValueToLength(parameter);

            return CreatePrinterCommand(string.Format("H{0}", formatedParameter));
        }

        public static string CommandVVerticalAllignmentV(int parameter)
        {
            var formatedParameter = FormatValueToLength(parameter);

            return CreatePrinterCommand(string.Format("V{0}", formatedParameter));
        }

        public static string CommandLCharacterExpention(int horizontal, int vertical)
        {
            var horizontalStr = FormatValueToLength(horizontal, 2);
            var verticalstr = FormatValueToLength(vertical, 2);
            return CreatePrinterCommand(string.Format("L{0}{1}", horizontalStr, verticalstr));
        }

        public static string CommandBGBarcode(string barcodeData, int narrowBarWidth, int barcodeHeight)
        {
            var barWidth = FormatValueToLength(narrowBarWidth, 2);
            var height = FormatValueToLength(barcodeHeight, 3);
            return CreatePrinterCommand(string.Format("BG{0}{1}", barWidth, height));
        }

        public static string CommadXUFont()
        {
            return CreatePrinterCommand("XU");
        }

        public static string CommandCSPrintSpeed(int parameter)
        {
            return CreatePrinterCommand(string.Format("CS{0}", parameter));
        }

        public static string CommandA3BaseReferencePoint(int horizontalOffset, int verticalOffset)
        {
            var horizontalStr = FormatValueToLength(horizontalOffset, 4);
            var verticalstr = FormatValueToLength(verticalOffset, 4);

            return CreatePrinterCommand(string.Format("A3H{0}V{1}", horizontalStr, verticalstr));
        }

        public static string CommandA1MediaSize(int labelHeight, int labelWidth)
        {
            var height = FormatValueToLength(labelHeight, 4);
            var width = FormatValueToLength(labelWidth, 4);

            return CreatePrinterCommand(string.Format("A1{0}{1}", height, width));
        }

        public static string CommandEPrintDarkness(int parameter)
        {
            return CreatePrinterCommand(string.Format("#E{0}", parameter));
        }

        public enum ASCIIDirectives
        {
            /// <summary>
            /// response begin, start of text
            /// </summary>
            STX = 0x02,

            /// <summary>
            /// response end, end of text
            /// </summary>
            ETX = 0x03,

            /// <summary>
            /// no error, acknowledge
            /// </summary>
            ACK = 0x06,

            /// <summary>
            /// error occurred, negative acknowledge
            /// </summary>
            NAK = 0x15,

            /// <summary>
            /// escape
            /// </summary>
            ESC = 0x1B,

            /// <summary>
            /// inquiry
            /// </summary>
            ENQ = 0x05
        }

        public struct EnquiryStastusResponse
        {
            public char Status;
            public int RemainingPrintNumber;
        }
    }

    public static class Extentions
    {
        public static char ToChar(this PrinterCommandsParser.ASCIIDirectives directive)
        {
            return (char)directive;
        }
    }
}
