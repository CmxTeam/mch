﻿namespace CargoMatrix.Viewer
{
    partial class ShellItemB
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.panelBaseLine = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDescription.BackColor = System.Drawing.Color.White;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescription.Location = new System.Drawing.Point(68, 2);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(170, 14);
            this.labelDescription.Text = "<desc>";
            // 
            // labelHeading
            // 
            this.labelHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHeading.BackColor = System.Drawing.Color.White;
            this.labelHeading.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelHeading.Location = new System.Drawing.Point(3, 2);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(63, 14);
            this.labelHeading.Text = "Heading";
            this.labelHeading.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelBaseLine
            // 
            this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;
            this.panelBaseLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBaseLine.Location = new System.Drawing.Point(0, 19);
            this.panelBaseLine.Name = "panelBaseLine";
            this.panelBaseLine.Size = new System.Drawing.Size(240, 1);
            // 
            // ShellItemB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelBaseLine);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelHeading);
            this.Name = "ShellItemB";
            this.Size = new System.Drawing.Size(240, 20);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelHeading;
        protected System.Windows.Forms.Panel panelBaseLine;
    }
}
