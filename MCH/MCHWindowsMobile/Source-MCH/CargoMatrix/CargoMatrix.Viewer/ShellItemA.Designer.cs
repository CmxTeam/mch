﻿

namespace CargoMatrix.Viewer
{
    partial class ShellItemA
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent()
        {
            this.labelDescription = new System.Windows.Forms.Label();
            this.Panel = new System.Windows.Forms.Panel();
            this.pictureBox21 = new OpenNETCF.Windows.Forms.PictureBox2();
            this.panelBaseLine = new System.Windows.Forms.Panel();
            this.Panel.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDescription.BackColor = System.Drawing.Color.White;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescription.Location = new System.Drawing.Point(34, 1);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(199, 32);
            this.labelDescription.Text = "<desc>";
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.Controls.Add(this.labelDescription);
            this.Panel.Controls.Add(this.pictureBox21);
            this.Panel.Location = new System.Drawing.Point(3, 3);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(234, 34);
            // 
            // pictureBox21
            // 
            this.pictureBox21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox21.BackColor = System.Drawing.Color.White;
            this.pictureBox21.Location = new System.Drawing.Point(1, 1);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(32, 32);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            // 
            // panelBaseLine
            // 
            this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;
            this.panelBaseLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBaseLine.Location = new System.Drawing.Point(0, 39);
            this.panelBaseLine.Name = "panelBaseLine";
            this.panelBaseLine.Size = new System.Drawing.Size(240, 1);
            // 
            // ShellItemA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelBaseLine);
            this.Controls.Add(this.Panel);
            this.Name = "ShellItemA";
            this.Size = new System.Drawing.Size(240, 40);
            this.Panel.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.ResumeLayout(false);

        }
        //private void InitializeComponent(string desc)
        //{
        //    this.labelDescription = new System.Windows.Forms.Label();
        //    this.SuspendLayout();
        //    // 
        //    // labelDescription
        //    // 
        //    this.labelDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
        //                | System.Windows.Forms.AnchorStyles.Left)
        //                | System.Windows.Forms.AnchorStyles.Right)));
        //    this.labelDescription.BackColor = System.Drawing.SystemColors.Control;
        //    this.labelDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
        //    this.labelDescription.Location = new System.Drawing.Point(3, 3);
        //    this.labelDescription.Name = "labelDescription";
        //    this.labelDescription.Size = new System.Drawing.Size(251, 32);
        //    this.labelDescription.Text = desc;
        //    // 
        //    // ShellItemA
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        //    this.BackColor = System.Drawing.Color.White;
        //    this.Controls.Add(this.labelDescription);
        //    this.Name = "ShellItemA";
        //    this.Size = new System.Drawing.Size(257, 38);
        //    this.ResumeLayout(false);

        //}
        //private void InitializeComponent(string desc, int height)
        //{
        //    this.labelDescription = new System.Windows.Forms.Label();
        //    this.SuspendLayout();
        //    // 
        //    // labelDescription
        //    // 
        //    this.labelDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
        //                | System.Windows.Forms.AnchorStyles.Left)
        //                | System.Windows.Forms.AnchorStyles.Right)));
        //    this.labelDescription.BackColor = System.Drawing.SystemColors.Control;
        //    this.labelDescription.Location = new System.Drawing.Point(3, 3);
        //    this.labelDescription.Name = "labelDescription";
        //    this.labelDescription.Size = new System.Drawing.Size(251, 32);
        //    this.labelDescription.Text = desc;
        //    // 
        //    // Reference
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        //    this.BackColor = System.Drawing.Color.White;
        //    this.Controls.Add(this.labelDescription);
        //    this.Name = "Reference";
        //    this.Size = new System.Drawing.Size(257, height);
        //    this.ResumeLayout(false);

        //}

        #endregion

        public System.Windows.Forms.Label labelDescription;
        public System.Windows.Forms.Panel Panel;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBox21;
        protected System.Windows.Forms.Panel panelBaseLine;
    }
}
