﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.CargoReceiver
{
    class OnHandTasksList : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        public OnHandTasksList()
        {

        }

        public override void LoadControl()
        {
            this.TitleText = string.Format("OnHand Tasks ({0})", smoothListBoxMainList.ItemsCount);
        }
    }
}
