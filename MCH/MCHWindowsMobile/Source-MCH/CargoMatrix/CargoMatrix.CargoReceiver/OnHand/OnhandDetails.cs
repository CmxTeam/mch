﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.CargoReceiver
{
    public class OnhandDetails
    {
        public string onHandNo;
        public CargoMatrix.Communication.CargoMeasurements.MettlerMeasurements[] measurements;
        public string carrier;
        public string customerAccnt;
        public string destination;
        public int noOfPcs;
        public string[] attributes;
    }
}
