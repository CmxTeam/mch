﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Utilities;
using CargoMatrix.Communication;
using CMXExtensions;
using System.Linq;
namespace CargoMatrix.CargoReceiver
{
    class ULDEditor
    {
        private static ULDEditor instance;
        private SmoothListbox.ListItems.StandardListItem looseItem;
        private CargoMatrix.Utilities.MessageListBox uldTypeBox;
        private CargoMatrix.Utilities.MessageListBox editPrePopup;
        private ULDListItem_new selectedItem;
        private bool showLoose;

        private void PopulatePrePopup()
        {
            if (editPrePopup == null)
            {
                Control[] actions = new Control[]
                { 
                    new SmoothListbox.ListItems.StandardListItem("EDIT ULD TYPE",null,1),
                    new SmoothListbox.ListItems.StandardListItem("EDIT ULD No",null,2),
                    new SmoothListbox.ListItems.StandardListItem("EDIT STAGING LOCATION",null,3),

                };
                editPrePopup = new CargoMatrix.Utilities.MessageListBox();
                editPrePopup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(editPrePopup_ListItemClicked);
                editPrePopup.AddItems(actions);
                editPrePopup.OneTouchSelection = true;
                editPrePopup.MultiSelectListEnabled = false;
                editPrePopup.HeaderText = Forklift.Instance.Mawb.Reference();

            }
        }

        void editPrePopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            bool result = false;
            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:
                    result = editULDType();
                    break;
                case 2:
                    result = editUldNumber();
                    break;
                case 3:
                    result = editUldDropLocation();
                    break;
            }
            sender.Reset();
            if (result)
            {
                Cursor.Current = Cursors.WaitCursor;
                CargoMatrix.Communication.CargoReceiver.Instance.UpdateULD(selectedItem.ItemData);
            }
        }

        private bool editUldDropLocation()
        {
            ScanEnterPopup scanUldDrop = new ScanEnterPopup();
            scanUldDrop.Title = selectedItem.ULDReference;
            scanUldDrop.TextLabel = "ULD Stage Location - " + selectedItem.ItemData.ULDName;
            scanUldDrop.ScannedText = selectedItem.ItemData.Location;
            if (DialogResult.OK == scanUldDrop.ShowDialog())
            {
                //var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(scanUldDrop.ScannedText);
                //if (scanItem.BarcodeType == CMXBarcode.BarcodeTypes.Area || scanItem.BarcodeType == CMXBarcode.BarcodeTypes.Door || scanItem.BarcodeType == CMXBarcode.BarcodeTypes.Truck)
                //{
                selectedItem.ItemData.Location = scanUldDrop.Text;
                selectedItem.RefreshView();
                return true;
                //}
                //else
                //    CargoMatrix.UI.CMXMessageBox.Show("Ivalid barcode for ULD", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }

            return false;
        }
        private bool editUldNumber()
        {
            ScanEnterPopup_old scanUldInfo = new ScanEnterPopup_old(BarcodeType.ULD);

            scanUldInfo.TextLabel = "ULD Identification# - " + selectedItem.ItemData.ULDName;
            scanUldInfo.ScannedText = selectedItem.ItemData.ULDNo;
            if (DialogResult.OK == scanUldInfo.ShowDialog())
            {
                var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(scanUldInfo.ScannedText);
                if (scanItem.BarcodeType == CMXBarcode.BarcodeTypes.Uld)
                {
                    var uldsNos = from uld in CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillULDs(Forklift.Instance.Mawb.MasterBillId)
                                  select uld.ULDNo;

                    if (uldsNos.Contains(scanItem.UldNumber))
                    {
                        string msg = string.Format(CargoReceiverResources.Text_UldNoExists, scanItem.UldNumber);
                        CargoMatrix.UI.CMXMessageBox.Show(msg, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                    else
                    {
                        selectedItem.ItemData.ULDNo = scanItem.UldNumber;
                        selectedItem.RefreshView();
                        return true;
                    }
                }
                else
                    CargoMatrix.UI.CMXMessageBox.Show("Ivalid barcode for ULD", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }

            return false;
        }
        private bool editULDType()
        {
            PopulateULDTypes(selectedItem.ItemData.TypeID, showLoose);
            uldTypeBox.HeaderText = Forklift.Instance.Mawb.Reference();
            bool changeFlag = false;
            if (DialogResult.OK == uldTypeBox.ShowDialog())
            {
                int oldtypeId = selectedItem.ItemData.TypeID;
                string oldtype = selectedItem.ItemData.ULDName;
                selectedItem.SetULDType((uldTypeBox.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID, (uldTypeBox.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).title.Text);
                changeFlag = true;

                // if changed to Loose - clear the ULD No
                if (selectedItem.ItemData.IsLoose)
                {
                    selectedItem.ItemData.ULDNo = string.Empty;
                }

                /// was Loose but now not loose should ask for a uld Number
                if (oldtype.Equals("loose", StringComparison.OrdinalIgnoreCase) && !oldtype.Equals(selectedItem.ItemData.ULDName, StringComparison.OrdinalIgnoreCase))
                {
                    // if user has canceled the scan uld No, type should be reset to previous
                    if (!editUldNumber())
                    {
                        selectedItem.SetULDType(oldtypeId, oldtype);
                        changeFlag = false;
                    }
                }
                return changeFlag;
            }
            return false;
        }
        private ULDEditor() { }

        private void PopulateULDTypes(int IdtoSelect, bool displayLoose)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (null == uldTypeBox)
            {
                uldTypeBox = new CargoMatrix.Utilities.MessageListBox();
                uldTypeBox.HeaderText = Forklift.Instance.Mawb.MasterBillNumber;
                uldTypeBox.HeaderText2 = "Select ULD Type from the list";
                foreach (var item in CargoMatrix.Communication.ScannerUtility.Instance.GetULDTypes(true, CargoMatrix.Communication.ScannerUtilityWS.MOT.Air))
                {
                    SmoothListbox.ListItems.StandardListItem uldItem = new SmoothListbox.ListItems.StandardListItem(item.ULDName, null, item.TypeID);
                    // keep the reference of loose
                    if (item.IsLoose)
                        looseItem = uldItem;
                    else // add oter types to list
                        uldTypeBox.AddItem(uldItem);
                }
                uldTypeBox.MultiSelectListEnabled = false;
                uldTypeBox.OneTouchSelection = true;
            }
            uldTypeBox.ResetSelection();

            if (looseItem != null)
                if (displayLoose)
                {
                    uldTypeBox.smoothListBoxBase1.RemoveItem(looseItem);
                    uldTypeBox.smoothListBoxBase1.AddItemToFront(looseItem);
                }
                else
                    uldTypeBox.RemoveItem(looseItem);

            if (IdtoSelect != 0)
                foreach (SmoothListbox.ListItems.StandardListItem item in uldTypeBox.Items)
                    if (item.ID == IdtoSelect)
                    {
                        uldTypeBox.SelectControl(item);
                        break;
                    }

            Cursor.Current = Cursors.Default;
        }

        internal void AddULDItem(int mawbid, string carrier, string mawb)
        {
            AddULDItem(mawbid, carrier, mawb, true);
        }
        internal bool AddULDItem(int mawbid, string carrier, string mawb, bool showLoose)
        {

            PopulateULDTypes(0, showLoose);
            uldTypeBox.HeaderText = Forklift.Instance.Mawb.Reference();

            bool flag = false;
            string uldType = string.Empty;
            string uldNumber = string.Empty;
            string uldDropLoc = string.Empty;
            int uldTypeId = 0;

            if (DialogResult.OK == uldTypeBox.ShowDialog())
            {
                uldType = (uldTypeBox.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).title.Text;
                uldTypeId = (uldTypeBox.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID;

                if (!string.Equals(uldType, "loose", StringComparison.OrdinalIgnoreCase))
                {
                    ScanEnterPopup_old scanUldInfo = new ScanEnterPopup_old(BarcodeType.ULD);
                    scanUldInfo.TextLabel = "ULD Identification# - " + uldType;
                    if (DialogResult.OK == scanUldInfo.ShowDialog())
                    {
                        var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(scanUldInfo.ScannedText);
                        if (scanItem.BarcodeType == CMXBarcode.BarcodeTypes.Uld)
                        {
                            var uldsNos = from uld in CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillULDs(Forklift.Instance.Mawb.MasterBillId)
                                          select uld.ULDNo;

                            if (uldsNos.Contains(scanItem.UldNumber))
                            {
                                string msg = string.Format(CargoReceiverResources.Text_UldNoExists, scanItem.UldNumber);
                                CargoMatrix.UI.CMXMessageBox.Show(msg, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            }
                            else
                            {
                                flag = true;
                                uldNumber = scanItem.UldNumber;
                            }
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Ivalid barcode for ULD", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        }
                    }
                }
                else
                {
                    uldNumber = string.Empty;
                    flag = true;
                }

                // Stage location dont need to capture
                if (flag && Forklift.Instance.ReceiveMode != CargoMatrix.Communication.WSCargoReceiver.MasterBillDirection.Import)
                {
                    ScanEnterPopup scanUldLocation = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
                    scanUldLocation.Title = Forklift.Instance.Mawb.Reference();
                    scanUldLocation.TextLabel = "ULD Stage Location - " + uldType;

                    if (DialogResult.OK == scanUldLocation.ShowDialog())
                    {
                        flag = true;
                        uldDropLoc = scanUldLocation.Text;
                    }
                    else flag = false;
                }
            }

            /// 3 steps are OK
            if (flag)
            {
                var status = CargoMatrix.Communication.CargoReceiver.Instance.AttachULD(mawbid, uldTypeId, uldNumber, uldDropLoc, false);

                if (status.TransactionStatus1 == false && status.NeedOverride)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Alert", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, DialogResult.OK))
                    {
                        status = CargoMatrix.Communication.CargoReceiver.Instance.AttachULD(mawbid, uldTypeId, uldNumber, uldDropLoc, true);
                        if (status.TransactionStatus1 == false)
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Alert", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                    }
                }
            }
            return flag;
        }
        internal void EditUldItem(ULDListItem_new Ulditem)
        {
            EditUldItem(Ulditem, true);
        }
        internal void EditUldItem(ULDListItem_new Ulditem, bool showLoose)
        {
            PopulatePrePopup();
            selectedItem = Ulditem;
            this.showLoose = showLoose;
            PopulateULDTypes(Ulditem.ItemData.TypeID, showLoose);
            uldTypeBox.HeaderText = Forklift.Instance.Mawb.Reference();

            string uldType = string.Empty;
            string uldNumber = string.Empty;
            editPrePopup.HeaderText2 = string.Format("Selected ULD - {0}", Ulditem.ItemData.IsLoose ? Ulditem.ItemData.ULDName : Ulditem.ItemData.ULDName + " - " + Ulditem.ItemData.ULDNo);
            editPrePopup.Items[1].Enabled = !Ulditem.ItemData.IsLoose;

            editPrePopup.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        internal bool DeleteULDItem(ULDListItem_new Ulditem)
        {
            if (CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_DeleteULD, "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
            {//CargoMatrix.Communication.GTWSettings.Instance.AllowULDEdit == false &&
                if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                    if (DialogResult.OK != CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                        return false;

                var status = CargoMatrix.Communication.CargoReceiver.Instance.DeleteULD(Ulditem.ItemData.ID);
                if (null != status && status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return true;
            }
            return false;
        }

        internal static ULDEditor Instance
        {
            get
            {
                if (instance == null)
                    instance = new ULDEditor();
                return instance;
            }
        }

        #region copy
        private MessageListBox uldList;
        public DialogResult ShowULdList()
        {
            uldList = new MessageListBox();
            uldList.WideWidth();
            uldList.HeaderText = Forklift.Instance.Mawb.Reference();
            uldList.HeaderText2 = "Select ULD to Recover";
            uldList.MultiSelectListEnabled = false;
            uldList.OneTouchSelection = false;
            uldList.OkEnabled = false;

            DisplayExistingULDs();
            return uldList.ShowDialog();
        }
        private void DisplayExistingULDs()
        {
            uldList.RemoveAllItems();
            uldList.AddItem(new SmoothListbox.ListItems.StandardListItem("Add New ULD", Resources.Skin.Freight_Car));
            uldList.ListItemClicked += new SmoothListbox.ListItemClickedHandler(uldList_ListItemClicked);

            foreach (var item in CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillULDs(Forklift.Instance.Mawb.MasterBillId))
            {
                ULDListItem_new uld = new CustomListItems.ULDListItem_new(item, true);
                uld.ButtonDeleteClick += new EventHandler(Ulditem_ButtonDeleteClick);
                uld.ButtonEditClick += new EventHandler(Ulditem_ButtonEditClick);
                uld.ButtonEnterClick += new EventHandler(uld_ButtonEnterClick);
                uld.ButtonBrowseClick += new EventHandler(Ulditem_ButtonBrowseClick);
                uldList.AddItem(uld);
            }
        }

        void uld_ButtonEnterClick(object sender, EventArgs e)
        {
            ScanEnterPopup dropLoc = new ScanEnterPopup();
            dropLoc.Title = "Scan Location";
            dropLoc.TextLabel = (sender as ULDListItem_new).ItemData.ULDNo;
            dropLoc.ShowDialog();
        }
        void uldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is SmoothListbox.ListItems.StandardListItem)
            {
                AddULDItem(-1, string.Empty, string.Empty, !HasLoose);
                DisplayExistingULDs();
                sender.Reset();
            }
            uldList.OkEnabled = false;
        }
        void Ulditem_ButtonBrowseClick(object sender, EventArgs e)
        {
            //PopulateUldContent(sender as ULDListItem);

        }
        private bool HasLoose
        {
            get
            {
                foreach (var item in uldList.Items)
                {
                    if (item is ULDListItem_new && (item as ULDListItem_new).ItemData.IsLoose)
                        return true;
                }
                return false;
            }
        }
        void Ulditem_ButtonEditClick(object sender, EventArgs e)
        {
            if (sender is ULDListItem_new)
            {
                ULDListItem_new item = sender as ULDListItem_new;
                EditUldItem(item, !HasLoose);
                if (item.ItemData.IsLoose)
                {
                    uldList.smoothListBoxBase1.MoveItemToTop(item);
                    uldList.smoothListBoxBase1.LayoutItems();
                }
            }
        }

        void Ulditem_ButtonDeleteClick(object sender, EventArgs e)
        {
            if (ULDEditor.Instance.DeleteULDItem((sender as ULDListItem_new)))
                DisplayExistingULDs(); // if delete was successful reload the uld items
        }
        private void ShowFailMessage(string message)
        {
            CargoMatrix.UI.CMXMessageBox.Show(message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }
        #endregion
    }


}
