﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXExtensions;
using CargoMatrix.UI;
using SmoothListbox.ListItems;
namespace CargoMatrix.CargoReceiver
{
    public partial class HawbDetailedItem : UserControl
    {
        private HouseBillItem hawb;
        private string summary;
        private CargoMatrix.Viewer.HousebillViewer billViewer;
        private Timer holdTimer;
        internal HouseBillItem Hawb { get { return hawb; } }
        public string Summary { get { return summary; } set { summary = value; pictureBoxSummary.Refresh(); } }
        public event EventHandler HawbDetailsHold;
        public event EventHandler DoScan;
        public HawbDetailedItem()
        {
            /// call InitializeHelper in InitializeComponent
            InitializeComponent();
            this.MouseDown += new MouseEventHandler(HawbDetailedItem_MouseDown);
            this.MouseUp += new MouseEventHandler(HawbDetailedItem_MouseUp);
        }



        private void InitializeHelper()
        {
            this.Top = 50;
            //this.buttonPrint.Image = global::Resources.Graphics.Skin._3dots;
            //this.buttonPrint.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.buttonDamage.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.pictureBoxSummary.Image = global::Resources.Graphics.Skin.popup_header;
            this.buttonPrint.Image = Resources.Skin.printerButton;
            this.buttonPrint.PressedImage = Resources.Skin.printerButton_over;
            this.buttonToggle.Image = Resources.Skin.btn_listView;
            this.buttonToggle.PressedImage = Resources.Skin.btn_listView_over;

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }
        public void DisplayHousebill(HouseBillItem hb)
        {
            this.hawb = hb;//CargoMatrix.Communication.CargoReceiver.Instance.GetHousebillInfo(hb.HousebillId);//hawb;
            title.Text = hawb.Reference();
            labelPieces.Text = string.Format("{0} of {1}", hawb.ScannedPieces, hawb.TotalPieces);
            
            if (hawb.ScannedPieces == 0 || hawb.ScannedPieces == hawb.TotalPieces)
                labelPieces.ForeColor = Color.Black;
            else
                labelPieces.ForeColor = Color.Red;

            labelWeight.Text = hawb.Weight + " KGS";
            labelAlias.Text = hawb.AliasNumber;
            labelSender.Text = hawb.ShipperName;
            labelReceiver.Text = hawb.ConsigneeName;
            labelDescr.Text = hawb.Description;
            labelDropAt.Text = hawb.DropLocation;
            labelSlac.Text = hawb.TotalSlac.ToString();
            labelDropAt.Blink = true;

            switch (hawb.Status)
            {
                case HouseBillStatuses.Open:
                case HouseBillStatuses.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case HouseBillStatuses.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case HouseBillStatuses.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;

            }
            panelIndicators.Flags = hawb.Flags;
            panelIndicators.PopupHeader = hawb.Reference();
            this.buttonPrint.Visible = true;
            this.buttonDamage.Visible = true;
            this.buttonDetails.Visible = true;
            //this.Summary = string.Format("Pcs: {0} of {1} | {2}%", 
            //    Hawb.Transaction.TotalProcessed,
            //    Hawb.Transaction.TotalCount,
            //    Hawb.Transaction.TaskProgress);

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }
        public void clearDetails()
        {
            this.hawb = null;
            title.Text = string.Empty;
            labelPieces.Text = string.Empty;
            labelWeight.Text = string.Empty;
            labelAlias.Text = string.Empty;
            labelSender.Text = string.Empty;
            labelReceiver.Text = string.Empty;
            labelDescr.Text = string.Empty;
            labelDropAt.Text = string.Empty;
            labelSlac.Text = string.Empty;
            labelDropAt.Blink = false;
            itemPicture.Image = null;
            panelIndicators.Flags = 0;
            this.buttonPrint.Visible = false;
            this.buttonDamage.Visible = false;
            this.buttonDetails.Visible = false;
        }
        public void RefreshView()
        {
            hawb = CargoMatrix.Communication.CargoReceiver.Instance.GetHousebillInfo(hawb.HousebillId, Forklift.Instance.Mawb.TaskId);
            DisplayHousebill(hawb);
        }

        void HawbDetailedItem_Click(object sender, EventArgs e)
        {
            if (hawb != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                new HawbViewer(Hawb).ShowDialog();
            }
        }
        void buttonToggle_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            HawbListMessageBox hawbList = new HawbListMessageBox(Forklift.Instance.Mawb.Reference());
            if (DialogResult.OK == hawbList.ShowDialog())
            {
                var selectedItem = hawbList.SelectedItems[0] as IDataHolder<HouseBillItem>;
                DisplayHousebill(selectedItem.ItemData);

                DoScan(selectedItem.ItemData, EventArgs.Empty);


            }
        }
        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            DisplayBillViewer(hawb.HousebillNumber);
        }

        void buttonDamage_Click(object senderObj, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(new ReceiverHousebill(this.Hawb));

            damageCapture.OnLoadShipmentConditions += (sender, args) =>
            {
                args.ShipmentConditions = ScannerUtility.Instance.GetHouseBillShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sender, args) =>
            {
                args.ConditionsSummaries = ScannerUtility.Instance.GetHouseBillShipmentConditionSummary(args.HouseBillID);

            };


            damageCapture.OnUpdateShipmentConditions += (sender, args) =>
            {
                args.Result = ScannerUtility.Instance.UpdateHouseBillShipmentConditions(args.HouseBillId, args.ScanModes, args.ShipmentConditions, Forklift.Instance.Mawb.TaskId, CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.CargoReceiver, string.Empty, args.Slac);
            };

            damageCapture.GetHouseBillPiecesIDs = () =>
            {
                return (
                            from piece in Hawb.Pieces
                            select piece.PieceId
                       ).ToArray();
            };
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }
        void buttonPrint_Click(object sender, System.EventArgs e)
        {
            CustomUtilities.HawbLabelPrinter.Show(hawb.HousebillId, hawb.HousebillNumber, hawb.TotalPieces);
        }

        private void DisplayBillViewer(string hawbNo)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);//"3TB7204" masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
            Cursor.Current = Cursors.Default;
        }

        private void pictureBoxSummary_Paint(object sender, PaintEventArgs e)
        {
            if (this.Enabled)
            {
                using (Font titleFont = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold))
                {
                    SizeF textSize = e.Graphics.MeasureString(summary, titleFont);
                    int x = (pictureBoxSummary.Width - (int)textSize.Width) / 2;
                    int y = (pictureBoxSummary.Height - (int)textSize.Height) / 2;

                    e.Graphics.DrawString(summary, titleFont, new SolidBrush(Color.White), x, y);
                }
            }
        }


        void HawbDetailedItem_MouseUp(object sender, MouseEventArgs e)
        {
            if (holdTimer != null)
                holdTimer.Enabled = false;
        }

        void holdTimer_Tick(object sender, EventArgs e)
        {
            holdTimer.Enabled = false;
            if (hawb != null && HawbDetailsHold != null)
            {
                HawbDetailsHold(this, EventArgs.Empty);
            }
        }

        void HawbDetailedItem_MouseDown(object sender, MouseEventArgs e)
        {
            if (hawb == null)
                return;

            if (holdTimer == null)
            {
                holdTimer = new Timer();
                holdTimer.Interval = 1500;
                holdTimer.Tick += new EventHandler(holdTimer_Tick);
            }
            holdTimer.Enabled = true;
        }

        #region Switch Mode

        internal bool SwitchMode()
        {
            bool retValue = false;
            /// piece Mode -> Count
            if (hawb.ScanMode == ScanModes.Piece)
            {
                string msg = string.Format("This will change Hawb {0} scan mode to COUNT. Do you want to continue?", hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (ChangeModeAllowed)
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, Forklift.Instance.Mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Count);
                        if (status.TransactionStatus1) ;
                        //CargoMatrix.UI.CMXMessageBox.Show(string.Format("Hawb {0} : Mode changed to COUNT", hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

                        retValue = status.TransactionStatus1;
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format("This will change HAWB {0} scan mode to PIECEID. Do you want to continue?", hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (ChangeModeAllowed)
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, Forklift.Instance.Mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Piece);
                        if (status.TransactionStatus1)
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format("Hawb {0} : Mode changed to PIECEID", hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

                        retValue = status.TransactionStatus1;
                    }
                }
            }
            return retValue;
        }

        private bool ChangeModeAllowed
        {
            get
            {
                if (Forklift.Instance.ChangeHawbModeAllowed)
                    return true;

                //CargoMatrix.Communication.GTWSettings.Instance.ChangeHawbScanModeAllowed||
                if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin ||
                    DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                    return true;
                else
                    return false;
            }
        }
        #endregion
    }
    internal class ReceiverHousebill : CargoMatrix.Communication.DTO.IHouseBillItem
    {

        private HouseBillItem hawb;
        public ReceiverHousebill(HouseBillItem hawb)
        {
            this.hawb = hawb;
        }
        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get { return hawb.HousebillNumber; }
        }

        public int HousebillId
        {
            get { return hawb.HousebillId; }
        }

        public string Origin
        {
            get { return hawb.Origin; }
        }

        public string Destination
        {
            get { return hawb.Destination; }
        }

        public int TotalPieces
        {
            get { return hawb.TotalPieces; }
        }

        public int ScannedPieces
        {
            get { return hawb.ScannedPieces; }
        }

        public int Slac
        {
            get { return hawb.TotalSlac; }
        }

        public int Weight
        {
            get { return (int)hawb.Weight; }
        }

        public string Location
        {
            get { return hawb.LastLocation; }
        }

        public int Flags
        {
            get { return hawb.Flags; }
        }

        public CargoMatrix.Communication.DTO.ScanModes ScanMode
        {
            get
            {
                //switch (hawb.ScanMode)
                //{
                //case ScanModes.Piece:
                //    return CargoMatrix.Communication.DTO.ScanModes.Piece;
                //case ScanModes.Count:
                return CargoMatrix.Communication.DTO.ScanModes.Count;
                //case ScanModes.Slac:
                //    return CargoMatrix.Communication.DTO.ScanModes.Slac;
                //case ScanModes.NA:
                //default:
                //    return CargoMatrix.Communication.DTO.ScanModes.NA;
                //} 
            }
        }

        public CargoMatrix.Communication.DTO.HouseBillStatuses status
        {
            get
            {
                switch (hawb.ScanStatus)
                {
                    case ScanStatuses.All:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
                    case ScanStatuses.NotScanned:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Pending;
                    case ScanStatuses.Scanned:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Completed;
                    default:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Open;
                }
            }
        }

        public CargoMatrix.Communication.DTO.RemoveModes RemoveMode
        {
            get
            {
                switch (hawb.RemoveMode)
                {
                    case RemoveModes.All:
                        return CargoMatrix.Communication.DTO.RemoveModes.All;
                    case RemoveModes.Shortage:
                        return CargoMatrix.Communication.DTO.RemoveModes.Shortage;
                    case RemoveModes.TakeOff:
                        return CargoMatrix.Communication.DTO.RemoveModes.TakeOff;
                    default:
                        return CargoMatrix.Communication.DTO.RemoveModes.NA;
                };
            }
        }

        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}", hawb.Origin, hawb.HousebillNumber, hawb.Destination); }
        }

        public string Line2
        {
            get { return string.Format("Weight: {0} KGS", hawb.Weight); }
        }

        public string Line3
        {
            get { return string.Format("Pieces: {0} of {1}  Slac: {2}", hawb.ScannedPieces, hawb.TotalPieces, hawb.TotalSlac); }
        }

        public string Line4
        {
            get { return string.Format("Location: {0}", hawb.LastLocation); }
        }

        #endregion
    }
}
