﻿using System;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSCargoReceiver;
using SmoothListbox.ListItems;
using CMXExtensions;
namespace CargoMatrix.CargoReceiver
{
    public partial class HawbScan : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        private int currentPieceNumber = 0;
        private HawbDetailedItem hawbDetails;
        private CustomListItems.OptionsListITem optionSwithcMode;
        private CustomListItems.OptionsListITem optionShowLocations;
        public HawbScan()
        {
            InitializeComponent();
            Cursor.Current = Cursors.Default;
            Forklift.Instance.StatusChanged += new EventHandler(StatusChanged);
            hawbDetails = new HawbDetailedItem();
            this.panelHeader2.Controls.Add(hawbDetails);
            hawbDetails.HawbDetailsHold += new EventHandler(hawbDetails_HawbDetailsHold);
            hawbDetails.DoScan += new EventHandler(hawbDetails_DoScan);
 
        }

        void StatusChanged(object sender, EventArgs e)
        {
            this.buttonScannedList.Value = Forklift.Instance.ItemCount;
            this.hawbDetails.Summary = Forklift.Instance.Summary;
        }

        private void InitializeComponentHelper()
        {
            panelHeader2.Height = 232;
            this.LoadOptionsMenu += new EventHandler(CargoReceiver_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(CargoReceiver_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeScanned);
            labelBlink.Text = CargoReceiverResources.Text_ScanHwbMawb;
            smoothListBoxMainList.Scrollable = false;
        }

        void CargoReceiver_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.VIEW_HAWB_LIST:
                        DisplayHawbList();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        if (Forklift.Instance.ItemCount == 0)
                        {
                            var status = CargoMatrix.Communication.CargoReceiver.Instance.FinalizeCargoReceiver(Forklift.Instance.Mawb.TaskId, false);
                            if (status.TransactionStatus1 == false)
                            {
                                if (status.NeedOverride)
                                {
                                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_FinalizeOverride, "Finlize Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                                    {
                                        status = CargoMatrix.Communication.CargoReceiver.Instance.FinalizeCargoReceiver(Forklift.Instance.Mawb.TaskId, true);
                                        if (status.TransactionStatus1 == true)
                                            CargoMatrix.UI.CMXAnimationmanager.GoToContol("TaskList");
                                        else
                                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                                    }
                                    break;
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                                break;
                            }

                            CargoMatrix.UI.CMXAnimationmanager.GoToContol("TaskList");
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_ClearForklift, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.SWITCH_MODE:
                        if (hawbDetails.Hawb != null)
                        {
                            hawbDetails.SwitchMode();
                            hawbDetails.RefreshView();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.SHOW_LOCATIONS:
                        if (hawbDetails.Hawb != null)
                        {
                            hawbDetails_HawbDetailsHold(null, EventArgs.Empty);
                        }
                        break;
                    default:
                        break;
                }
        }
        private void DisplayHawbList()
        {
            HawbListMessageBox hawbList = new HawbListMessageBox(Forklift.Instance.Mawb.Reference());
            if (DialogResult.OK == hawbList.ShowDialog())
            {
                var item = hawbList.SelectedItems[0] as IDataHolder<HouseBillItem>;
                DisplayHawb(item.ItemData);
            }
        }

        void CargoReceiver_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            //AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.VIEW_HAWB_LIST));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL));
            optionSwithcMode = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SWITCH_MODE);
            optionShowLocations = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SHOW_LOCATIONS) { Title = "Select Housebill Pcs" };
            AddOptionsListItem(optionSwithcMode);
            AddOptionsListItem(optionShowLocations);
            AddOptionsListItem(this.UtilitesOptionsListItem);
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            base.pictureBoxMenu_Click(sender, e);
            if (m_bOptionsMenu)
                /// enable disable options if hawb persisits
                optionShowLocations.Enabled = optionSwithcMode.Enabled = hawbDetails.Hawb != null;
        }

        public override void LoadControl()
        {
            this.Refresh();
            if (!string.IsNullOrEmpty(Forklift.Instance.Mawb.CarrierInfo.CarrierLogo))
                pictureBoxLogo.Image = CargoMatrix.Communication.Utilities.ConvertToImage(Forklift.Instance.Mawb.CarrierInfo.CarrierLogo);
            else
                pictureBoxLogo.Image = Resources.Icons.Truck;

            labelLine1.Text = Forklift.Instance.Mawb.Reference();
            labelLine2.Text = Forklift.Instance.Mawb.Line2();
            labelBlink.Text = CargoReceiverResources.Text_ScanHawb;
            this.TitleText = string.Format("CargoReceiver - {0}", Forklift.Instance.ReceiveMode.ToString());
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            Forklift.Instance.Status = CargoMatrix.Communication.CargoReceiver.Instance.GetForkLiftHouseBillsCount(Forklift.Instance.ForkliftID, Forklift.Instance.Mawb.TaskId);
            
            
             
            
            //hawbDetails.clearDetails();
            if (!string.IsNullOrEmpty(Forklift.Instance.FirstScannedHousebill))
            {
                this.Refresh();
                BarcodeScanned(Forklift.Instance.FirstScannedHousebill);
                Forklift.Instance.FirstScannedHousebill = string.Empty;
            }
            if (hawbDetails.Hawb != null)
                hawbDetails.RefreshView();
        }
        void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            bool finalized = false;
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            if (DialogResult.OK == Forklift.Instance.ShowDialog())
            {
                if (hawbDetails != null)
                    hawbDetails.clearDetails();
                finalized = FinalizeConsol();
            }
            if (!finalized)
                LoadControl();
        }


        private void DisplayHawb(CargoMatrix.Communication.WSCargoReceiver.HouseBillItem hawb)
        {
            if (hawbDetails == null)
            {
                hawbDetails = new HawbDetailedItem() { Top = 50 };
                this.panelHeader2.Controls.Add(hawbDetails);
                hawbDetails.HawbDetailsHold += new EventHandler(hawbDetails_HawbDetailsHold);
            }
            //hawbDetails.Visible = true;
            hawbDetails.DisplayHousebill(hawb);
           

        }

        void hawbDetails_HawbDetailsHold(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (DialogResult.OK == new HawbViewer(hawbDetails.Hawb).ShowDialog())
                LoadControl();
        }

        void hawbDetails_DoScan(object sender, EventArgs e)
        {
            var selectedItem = sender as HouseBillItem;

            if (selectedItem.TotalPieces > 1 && selectedItem.ScanMode == ScanModes.Piece)
            {
                hawbDetails_HawbDetailsHold(selectedItem.HousebillNumber, EventArgs.Empty);
            }
            else
            {
                Scan(selectedItem.HousebillNumber, 1);
            }
        
        }


        void BarcodeScanned(string barcodeData)
        {
            labelBlink.SplashText("Scanned " + barcodeData);
            Cursor.Current = Cursors.WaitCursor;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scanItem.BarcodeType)
            {
                 case  CMXBarcode.BarcodeTypes.Dimensions:
                   
                    string packingType = string.Empty;
                    bool isManual = false;
                    int pieceNumber = currentPieceNumber;
                    int totalPieces = 0;
                    int length = scanItem.Length;
                    int width = scanItem.Width;
                    int height = scanItem.Height;
                    double weight = scanItem.Weight;
                    string hawb = string.Empty;
                    try
                    {
                        hawb = hawbDetails.Hawb.HousebillNumber;
                        //isPieceCount  =( hawbDetails.Hawb.ScanMode == ScanModes.Piece) ? true : false;
                        totalPieces = hawbDetails.Hawb.TotalPieces;
                        packingType = hawbDetails.Hawb.PackingMethod;
                    }
                    catch { }

                    if (hawb != string.Empty)
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                        if (hawbDetails.Hawb.ScanMode == ScanModes.Piece)
                        {

                            if (DialogResult.OK == CustomUtilities.DimWeightMessageBox.ShowPieceMode(hawb, ref weight, ref length, ref width, ref height, ref pieceNumber, totalPieces, ref packingType, ref isManual))
                            {
                                var status = CargoMatrix.Communication.ScannerUtility.Instance.UpdateHouseBillDimensions(hawbDetails.Hawb.HousebillId, CargoMatrix.Communication.ScannerUtilityWS.ScanModes.Piece , pieceNumber, packingType, weight, length, width, height, isManual);
                            }
                        }
                        else
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show("This feature is only available for piece scanning mode.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            if (DialogResult.OK == CustomUtilities.DimWeightMessageBox.ShowCountMode(hawb, ref weight, ref length, ref width, ref height, ref pieceNumber, totalPieces, ref packingType, ref isManual))
                            {
                                var status = CargoMatrix.Communication.ScannerUtility.Instance.UpdateHouseBillDimensions(hawbDetails.Hawb.HousebillId, CargoMatrix.Communication.ScannerUtilityWS.ScanModes.Count, pieceNumber, packingType, weight, length, width, height, isManual);
                            }
                        }
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("You must scan a Housebill first in order to update dimensions.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                 
                
                       
                   
                    break;
                case CMXBarcode.BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    currentPieceNumber = scanItem.PieceNumber;
                    //if (ValidateHawb(scanItem.HouseBillNumber))
                    Scan(scanItem.HouseBillNumber, scanItem.PieceNumber);
                    //else
                    //    return;
                    break;
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    if (Forklift.Instance.ItemCount != 0)
                    {
                        if (Forklift.Instance.DropForklift(scanItem.Location))
                        {
                            if (FinalizeConsol())
                                return;
                            if (hawbDetails != null)
                                hawbDetails.clearDetails();
                        }
                    }
                    else
                        goto default;
                    break;
                case CMXBarcode.BarcodeTypes.MasterBill:
                    if (scanItem.MasterBillNumber != Forklift.Instance.Mawb.MasterBillNumber)
                        SwitchMasterbill(scanItem);
                    break;
                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    break;
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        // obsolete
        //private bool ValidateHawb(string hawbNo)
        //{
        //    MasterBillItem newMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(string.Empty, string.Empty, hawbNo);

        //    if (newMawb.MasterBillId == Forklift.Instance.Mawb.MasterBillId) // Valid
        //        return true;
        //    else // hawb from differen cosnol
        //    {
        //        /// froklift is not empty
        //        if (CargoMatrix.Communication.CargoReceiver.Instance.GetForkLiftHouseBillsCount(Forklift.Instance.ForkliftID) != 0)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_UnableToSwitch, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //            BarcodeEnabled = true;
        //        }
        //        else
        //        {
        //            string msg = string.Format(CargoReceiverResources.Text_ConfirmSwitch, Forklift.Instance.Mawb.MasterBillNumber, newMawb.MasterBillNumber);
        //            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Switch", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
        //            {
        //                Forklift.Instance.Mawb = newMawb;
        //                CargoMatrix.UI.CMXAnimationmanager.GoBack();
        //            }
        //        }
        //        return false;
        //    }
        //}

        private void SwitchMasterbill(CMXBarcode.ScanObject scanItem)
        {
            /// froklift is not empty
            if (CargoMatrix.Communication.CargoReceiver.Instance.GetForkLiftHouseBillsCount(Forklift.Instance.ForkliftID, Forklift.Instance.Mawb.TaskId).TransactionRecords != 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_UnableToSwitch, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else // Confirm Mawsterbill Switch
            {
                string msg = string.Format(CargoReceiverResources.Text_ConfirmSwitch, Forklift.Instance.Mawb.MasterBillNumber, scanItem.MasterBillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Switch", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    Forklift.Instance.Mawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(scanItem.CarrierNumber, scanItem.MasterBillNumber, string.Empty);
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                }
                else BarcodeEnabled = true;
            }
        }

        public bool FinalizeConsol()
        {
            if (Forklift.Instance.ItemCount == 0 && Forklift.Instance.Status.TotalProcessed == Forklift.Instance.Mawb.Pieces)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_Finalize, "Finalize Ready", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {

                    var status = CargoMatrix.Communication.CargoReceiver.Instance.FinalizeCargoReceiver(Forklift.Instance.Mawb.TaskId, false);

                    if (status.TransactionStatus1 == false)
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    else
                    {
                        Forklift.Instance.Mawb = null;
                        CargoMatrix.UI.CMXAnimationmanager.GoToContol("TaskList");
                        return true;
                    }
                }
            }
            return false;
        }
        private void Scan(string hawbNo, int pieceNo)
        {

            //check scanned housebill against current housebill on FL
            if (!Forklift.Instance.AllowMultipleHawbOnForkLift)
                if (!string.IsNullOrEmpty(Forklift.Instance.CurrentHousebill.hawbNo) && Forklift.Instance.ItemCount != 0)
                {
                    if (!string.Equals(Forklift.Instance.CurrentHousebill.hawbNo, hawbNo, StringComparison.OrdinalIgnoreCase))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_SingleHousebillScan, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        return;
                    }
                }



            var hawb = CargoMatrix.Communication.CargoReceiver.Instance.ScanHawbIntoForklift(hawbNo, pieceNo, Forklift.Instance.Mawb.TaskId, Forklift.Instance.ForkliftID);
             
            if (hawb.Transaction.TransactionStatus1 == true) // Piece Mode
            {
                Forklift.Instance.CurrentHousebill.SetHawb(hawb.HousebillNumber, hawb.DropLocation);
                DisplayHawb(hawb);
                Forklift.Instance.Status = hawb.Transaction;
            }
            else
            {
                if (hawbDetails != null)
                {
                    hawbDetails.clearDetails();
                    Refresh();
                }

                //validation 
                // hawb from differen cosnol
                if (!string.IsNullOrEmpty(hawb.MasterBillNumber) && hawb.MasterBillNumber != Forklift.Instance.Mawb.MasterBillNumber)
                {
                    /// froklift is not empty
                    if (Forklift.Instance.ItemCount != 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_UnableToSwitch, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }
                    else
                    {
                        string msg = string.Format(CargoReceiverResources.Text_ConfirmSwitch, Forklift.Instance.Mawb.MasterBillNumber, hawb.MasterBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Switch", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            var newMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(string.Empty, string.Empty, hawb.HousebillNumber);
                            Forklift.Instance.Mawb = newMawb;
                            CargoMatrix.UI.CMXAnimationmanager.GoBack();
                        }
                        return;
                    }
                }




                if (hawb.ScanMode == CargoMatrix.Communication.WSCargoReceiver.ScanModes.Count)
                {
                    Cursor.Current = Cursors.Default;
                    // display Alerts
                    foreach (var alert in hawb.ShipmentAlerts)
                        CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage + Environment.NewLine + alert.AlertSetBy + Environment.NewLine + alert.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + hawb.HousebillNumber, CargoMatrix.UI.CMXMessageBoxIcon.Hand);

                    ///display count confirm massebox
                    CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
                    CntMsg.HeaderText = "Confirm count";
                    CntMsg.LabelDescription = "Enter number of pieces";
                    CntMsg.PieceCount = hawb.TotalPieces - hawb.ScannedPieces;
                    CntMsg.LabelReference = hawb.Reference();

                    if (DialogResult.OK == CntMsg.ShowDialog())
                    {
                        var countHawb = CargoMatrix.Communication.CargoReceiver.Instance.ScanHawbCountIntoForklift(hawb.HousebillId, CntMsg.PieceCount, Forklift.Instance.Mawb.TaskId, Forklift.Instance.ForkliftID, ScanTypes.Automatic);
                        if (countHawb.Transaction.TransactionStatus1)
                        {
                            Forklift.Instance.Status = countHawb.Transaction;
                            Forklift.Instance.CurrentHousebill.SetHawb(hawb.HousebillNumber, hawb.DropLocation);

                            DisplayHawb(countHawb);
                            //hawbDetails.Visible = true;
                        }
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(countHawb.Transaction.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    }
                }
                else
                {
                  
                    //RDF
                    if (CargoReceiverResources.Text_HawbDoesNotExist.ToLower() == hawb.Transaction.TransactionError.ToLower())
                    {
                        string msg = string.Format(CargoReceiverResources.Text_AddNewHawbToMawb, hawbNo);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "New Housebill", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {


                            int pieces;
                            string origin, dest;


                   

                            if (DialogResult.OK == CustomUtilities.OriginDestinationMessageBox.Show(hawbNo, out pieces, out origin, out dest))
                            {
                                
                                // create new record and call scan again
                                //CargoMatrix.Communication.InventoryWS.TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.CreateHousebill(hawbNo, origin, dest, pieces, Forklift.Instance.Mawb.TaskId);
                                TransactionStatus status = CargoMatrix.Communication.CargoReceiver.Instance.CreateHousebill(hawbNo, Forklift.Instance.Mawb.MasterBillNumber, Forklift.Instance.Mawb.CarrierNumber, origin, dest, pieces, Forklift.Instance.Mawb.TaskId);
                                if (status.TransactionStatus1 == false)
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                                    // Withdraw manual scan flag to not get the reprint label message popup
                                    //manualScan = false;
                                }
                                else
                                {
                                    Scan(hawbNo, pieceNo);
                                }
                                    
                            }

                        }
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(hawb.Transaction.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    }

                }
            }
        }
    }
}

