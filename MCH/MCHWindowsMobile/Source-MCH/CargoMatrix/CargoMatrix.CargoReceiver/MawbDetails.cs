﻿using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.WSCargoReceiver;
using CargoMatrix.Communication;
using CargoMatrix.UI;
using CMXExtensions;
namespace CargoMatrix.CargoReceiver
{
    public partial class MawbDetails : UserControl
    {
        private CargoMatrix.Viewer.HousebillViewer billViewer;
        //public string ButtonText {get {return ProceedButton.te
        public void DisplayMasterbill()
        {
            this.SuspendLayout();
            title.Text = Forklift.Instance.Mawb.Reference();

            labelWeight.Text = Forklift.Instance.Mawb.Weight + " KGS";
            labelPieces.Text = Forklift.Instance.Mawb.Pieces.ToString();
            labelSlac.Text = Forklift.Instance.Mawb.Slac.ToString();
            labelUlds.Text = Forklift.Instance.Mawb.Ulds.ToString();
            labelHawbs.Text = Forklift.Instance.Mawb.HouseBills.ToString();
            labelFlightNo.Text = Forklift.Instance.Mawb.FlightNumber;
            labelLocation.Text = Forklift.Instance.Mawb.StageLocation;

            if (!string.IsNullOrEmpty(Forklift.Instance.Mawb.CarrierInfo.CarrierLogo))
                pictureBoxCarrierLogo.Image = CargoMatrix.Communication.Utilities.ConvertToImage(Forklift.Instance.Mawb.CarrierInfo.CarrierLogo);
            else
                pictureBoxCarrierLogo.Image = Resources.Icons.Truck;

            switch (Forklift.Instance.Mawb.Status)
            {
                case CargoMatrix.Communication.WSCargoReceiver.MasterBillStatuses.Open:
                case CargoMatrix.Communication.WSCargoReceiver.MasterBillStatuses.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case CargoMatrix.Communication.WSCargoReceiver.MasterBillStatuses.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case CargoMatrix.Communication.WSCargoReceiver.MasterBillStatuses.Completed:
                    //ProceedButton.Enabled = false;
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;

            }
            buttonDetails.Enabled = !(Forklift.Instance.Mawb.Direction == CargoMatrix.Communication.WSCargoReceiver.MasterBillDirection.Domestic);

            panelIndicators.Flags = Forklift.Instance.Mawb.Flags;
            panelIndicators.PopupHeader = title.Text;
            ProceedButton.BackgroundImage = Resources.Skin.button;
            ProceedButton.ActiveBackgroundImage = Resources.Skin.button_over;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.ResumeLayout(false);
            this.Visible = true;
        }

        public MawbDetails()
        {
            InitializeComponent();
            this.SuspendLayout();
            buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            buttonDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            buttonDamage.Image = CargoMatrix.Resources.Skin.damage;
            buttonDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            buttonPrint.Image = CargoMatrix.Resources.Skin.printerButton;
            buttonPrint.PressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            this.pictureBox2.Image = global::Resources.Graphics.Skin.nav_bg;
            this.ResumeLayout(false);

        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(Forklift.Instance.Mawb.CarrierNumber, Forklift.Instance.Mawb.MasterBillNumber);//"3TB7204" masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
            Cursor.Current = Cursors.Default;
        }

        void buttonDamage_Click(object sender, System.EventArgs e)
        {
            if (Forklift.Instance.Mawb == null)
                return;
            Cursor.Current = Cursors.WaitCursor;

            MawbItemAsHousebill masterAsHouseBill = new MawbItemAsHousebill(Forklift.Instance.Mawb);

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(masterAsHouseBill, DamageCapture.DamageCaptureApplyTypeEnum.MasterBill);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionsSummarys(Forklift.Instance.Mawb.TaskId);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.ScannerUtility.Instance.UpdateMasterBillConditions(Forklift.Instance.Mawb.TaskId, args.ShipmentConditions);
            };

            CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }


        void buttonPrint_Click(object sender, System.EventArgs e)
        {
            CustomUtilities.HawbPiecesLabelPrinter.Show(Forklift.Instance.Mawb.MasterBillNumber, Forklift.Instance.Mawb.CarrierNumber);
        }
    }

    public class MawbItemAsHousebill : CargoMatrix.Communication.DTO.IHouseBillItem
    {
        private MasterBillItem mawb;
        public MawbItemAsHousebill(MasterBillItem mawb)
        {
            this.mawb = mawb;
        }
        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get { return mawb.MasterBillNumber; }
        }

        public int HousebillId
        {
            get { return mawb.MasterBillId; }
        }

        public string Origin
        {
            get { return mawb.Origin; }
        }

        public string Destination
        {
            get { return mawb.Destination; }
        }

        public int TotalPieces
        {
            get { return mawb.Pieces; }
        }

        public int ScannedPieces
        {
            get { return mawb.ScannedPieces; }
        }

        public int Slac
        {
            get { return mawb.Slac; }
        }

        public int Weight
        {
            get { return (int)mawb.Weight; }
        }

        public string Location
        {
            get { return string.Empty; }
        }

        public int Flags
        {
            get { return mawb.Flags; }
        }

        public CargoMatrix.Communication.DTO.ScanModes ScanMode
        {
            get { return CargoMatrix.Communication.DTO.ScanModes.NA; }
        }

        public CargoMatrix.Communication.DTO.HouseBillStatuses status
        {
            get
            {
                switch (mawb.ScanStatus)
                {
                    case ScanStatuses.All:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
                    case ScanStatuses.NotScanned:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Pending;
                    case ScanStatuses.Scanned:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Completed;
                    case ScanStatuses.Shortage:
                    case ScanStatuses.TakeOff:
                    case ScanStatuses.Removed:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Cancelled;
                    default:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress;
                }
            }
        }

        public CargoMatrix.Communication.DTO.RemoveModes RemoveMode
        {
            get { return CargoMatrix.Communication.DTO.RemoveModes.NA; }
        }

        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}-{3}", mawb.Origin,mawb.CarrierNumber, mawb.MasterBillNumber, mawb.Destination); }
        }

        public string Line2
        {
            get { return string.Format("Weight: {0} KGS", mawb.Weight); }
        }

        public string Line3
        {
            get { return string.Format("Pieces: {0} of {1}  Slac: {2}", mawb.ScannedPieces, mawb.Pieces, mawb.Slac); }
        }

        public string Line4
        {
            get { return string.Empty; }
        }

        #endregion
    }

}
