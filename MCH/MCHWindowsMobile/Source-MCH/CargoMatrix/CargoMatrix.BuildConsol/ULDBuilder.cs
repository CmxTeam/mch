﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.UI;
using System.Linq;

namespace CargoMatrix.LoadConsol
{
    public partial class ULDBuilder : CargoUtilities.SmoothListBox2Options
    {

        private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.Utilities.MessageListBox uldContentList;
        public const string FORM_NAME = "ULDBuilder";
        private bool HasLoose
        {
            get
            {
                foreach (var item in smoothListBoxMainList.Items)
                {
                    if ((item as ULDListItem_new).ItemData.IsLoose)
                        return true;
                }
                return false;
            }
        }


        public ULDBuilder()
        {

            InitializeComponent();
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = ForkLiftViewer.Instance.Mawb.Reference();
            headerItem.Label2 = ForkLiftViewer.Instance.Mawb.Line2();
            headerItem.Label3 = "ADD/EDIT ULD";
            //headerItem.BlinkColor = Color.Red;
            //headerItem.Blink = true;

            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            headerItem.Location = panel1.Location;
            panel1.Visible = true;
            this.panel1.Controls.Add(headerItem);
            headerItem.BringToFront();
            panel1.Height = headerItem.Height;
            panelHeader2.Height = headerItem.Height;

            this.TitleText = "CargoLoader ULD Setup";

            smoothListBoxMainList.ListItemClicked += new SmoothListbox.ListItemClickedHandler(smoothListBoxMainList_ListItemClicked);
            LoadOptionsMenu += new EventHandler(ULDBuilder_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ULDBuilder_MenuItemClicked);
            this.ButtonFinalizeText = "Next";
            headerItem.Logo = ForkLiftViewer.Instance.CarrierLogo;

        }

        void ULDBuilder_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL:
                        populatePrintableUlds();
                        break;
                    case OptionsListITem.OptionItemID.PRINT_MAWB_LABEL:
                        BarcodeEnabled = false;
                        CustomUtilities.MawbLabelPrinter.Show(ForkLiftViewer.Instance.Mawb);
                        BarcodeEnabled = true;
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        if (ForkLiftViewer.Instance.FinalizeReady)
                            SharedMethods.FinalizeConsol();
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(ForkLiftViewer.Instance.FinalizeMessage, "Not Ready", CMXMessageBoxIcon.Hand);

                        break;
                }
            }
        }

        private void DisplayExistingULDS()
        {
            smoothListBoxMainList.RemoveAll();
            var existingUlds = CargoMatrix.Communication.LoadConsol.Instance.GetAttachedULDs(ForkLiftViewer.Instance.Mawb.MasterBillId, MOT.Air);
            foreach (var item in existingUlds)
            {

                ULDListItem_new uld = new CustomListItems.ULDListItem_new(item, false, false);
                uld.ButtonDeleteClick += new EventHandler(Ulditem_ButtonDeleteClick);
                uld.ButtonEditClick += new EventHandler(Ulditem_ButtonEditClick);

                if (item.IsLoose)
                    smoothListBoxMainList.AddItemToFront(uld);
                else
                    smoothListBoxMainList.AddItem(uld);
            }
        }

        void ULDBuilder_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_MAWB_LABEL));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL));
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            var ulds = smoothListBoxMainList.Items.OfType<ULDListItem_new>();

            if (ulds.Any(uld => (!uld.ItemData.IsLoose && !string.IsNullOrEmpty(uld.ItemData.ULDNo)))
                || ulds.All(uld => uld.ItemData.IsLoose))
            {

                if (ForkLiftViewer.Instance.FinalizeReady)
                    SharedMethods.FinalizeConsol();
                else
                {
                    if (ulds.Any(u => u.ItemData.IsLoose))
                        ProceedToDoorTruck();
                    else
                        ProceedToLoader();
                }
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_UldNoNumberAssigned, "ULD Number", CMXMessageBoxIcon.Hand);
            }
        }

        private void ProceedToLoader()
        {
            Cursor.Current = Cursors.WaitCursor;

            var loadHwabForm = new HousebillLoader();

            (loadHwabForm as UserControl).Location = new Point(Left, Top);
            (loadHwabForm as UserControl).Size = new Size(Width, Height);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(loadHwabForm as CargoMatrix.UI.CMXUserControl);
            smoothListBoxMainList.Reset();
        }

        private void ProceedToDoorTruck()
        {
            Cursor.Current = Cursors.WaitCursor;

            var doorTruckForm = new DoorTruckSetUp();

            (doorTruckForm as UserControl).Location = new Point(Left, Top);
            (doorTruckForm as UserControl).Size = new Size(Width, Height);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(doorTruckForm as CargoMatrix.UI.CMXUserControl);
            smoothListBoxMainList.Reset();
        }

        #region ULDLabelPrinting
        private void populatePrintableUlds()
        {
            CargoMatrix.Utilities.MessageListBox printableUlds = new CargoMatrix.Utilities.MessageListBox();
            printableUlds.ButtonVisible = true;
            printableUlds.ButtonImage = CargoMatrix.Resources.Skin.printerButton;
            printableUlds.ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            var defPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);
            printableUlds.HeaderText = defPrinter.PrinterName;
            printableUlds.HeaderText2 = "Select ULDs to print labels";

            printableUlds.ButtonClick += delegate(object sender, EventArgs e)
            {
                if (CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD, ref defPrinter))
                    printableUlds.HeaderText = defPrinter.PrinterName;

            };



            var uldsCheckItems = from uld in smoothListBoxMainList.Items.OfType<ULDListItem_new>()
                                 where !uld.ItemData.IsLoose
                                 select new CustomListItems.CkeckItem<IULDItem>(uld.ULDReference, uld.ID, uld.ItemData);

            if (uldsCheckItems.Count() > 1)
            {
                var selectAllItem = new CkeckItem<int>("Select All", 0, 0);
                printableUlds.AddItem(selectAllItem);
                printableUlds.ListItemClicked += (sender, item, isSelected) =>
                {
                    if (item is CkeckItem<int>)
                    {
                        if (isSelected)
                            printableUlds.smoothListBoxBase1.SelectAll();
                        else
                            printableUlds.smoothListBoxBase1.SelectNone();
                    }
                    else
                    {
                        if (isSelected && printableUlds.Items.OfType<CkeckItem<IULDItem>>().All(ci => ci.IsSelected))
                            selectAllItem.SelectedChanged(true);
                        else
                        {
                            selectAllItem.SelectedChanged(false);
                            printableUlds.smoothListBoxBase1.selectedItemsMap[selectAllItem] = false;
                        }
                    }
                };
            }

            printableUlds.AddItems(uldsCheckItems.OfType<Control>().ToArray());

            if (DialogResult.OK == printableUlds.ShowDialog())
            {
                var uldsToPrint = printableUlds.SelectedItems.OfType<CkeckItem<IULDItem>>().ToList<CkeckItem<IULDItem>>();
                SendPrintingJobToPrinter(uldsToPrint, defPrinter);
            }
        }

        private void SendPrintingJobToPrinter(IEnumerable<CkeckItem<IULDItem>> list, CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter selectedPrinter)
        {
            if (selectedPrinter.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel ||
    selectedPrinter.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless)
            {
                /// Capture printing reason if printed from mobile printer

                var mbilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(selectedPrinter);
                foreach (var item in list.OfType<CustomListItems.CkeckItem<IULDItem>>())
                    mbilePrinter.UldPrint(item.ItemData.ULDNo);
            }
            else            /// if network printer
            {

                foreach (var item in list.OfType<CustomListItems.CkeckItem<IULDItem>>())
                    CargoMatrix.Communication.ScannerUtility.Instance.PrintULDLabel(item.ItemData.ID, selectedPrinter.PrinterId, 1);
                string message = string.Format("Printing has been scheduled on {0}", selectedPrinter.PrinterName);
                CargoMatrix.UI.CMXMessageBox.Show(message, "Print", CargoMatrix.UI.CMXMessageBoxIcon.Success);

            }
        }

        #endregion


        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            BarcodeEnabled = false;
            ULDEditor.Instance.AddULDItem(!HasLoose);
            LoadControl();
        }

        public override void LoadControl()
        {
            BarcodeEnabled = false;
            DisplayExistingULDS();

            Cursor.Current = Cursors.Default;
            Application.DoEvents();
            if (smoothListBoxMainList.ItemsCount == 0)
            {
                ShowContinueButton(false);
                Application.DoEvents();
                BarcodeEnabled = false;
                if (ULDEditor.Instance.AddULDItem(!HasLoose))
                    LoadControl();
                else
                    BarcodeEnabled = true;
            }
            else
            {
                ShowContinueButton(false);
                ShowContinueButton(true);
            }
        }


        void Ulditem_ButtonEditClick(object sender, EventArgs e)
        {
            if (sender is ULDListItem_new)
            {
                BarcodeEnabled = false;
                ULDListItem_new item = sender as ULDListItem_new;
                ULDEditor.Instance.EditUldItem(item, !HasLoose);
                LoadControl();
            }
        }



        void Ulditem_ButtonDeleteClick(object sender, EventArgs e)
        {
            if (ULDEditor.Instance.DeleteULDItem((sender as ULDListItem_new)))
                LoadControl(); // if delete confirmed reload the uld items

            if (smoothListBoxMainList.ItemsCount == 0)
                ShowContinueButton(false);
        }

        private void PopulateUldContent(IULDItem uld)
        {
            Cursor.Current = Cursors.WaitCursor;

            uldContentList = new CargoMatrix.Utilities.MessageListBox();
            uldContentList.LabelEmpty = "ULD is Empty";
            uldContentList.HeaderText = string.Format("{0} - {1}", uld.ULDName, uld.ULDNo);
            uldContentList.HeaderText2 = "ULD Content";
            uldContentList.OkEnabled = false;
            uldContentList.IsSelectable = false;

            uldContentList.AddItems(GetULDContent(uld.ID));

            Cursor.Current = Cursors.Default;
            uldContentList.ShowDialog();
            DisplayExistingULDS();

        }


        int selecteduldId;
        void smoothListBoxMainList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            selecteduldId = (listItem as ULDListItem_new).ItemData.ID;
            PopulateUldContent((listItem as ULDListItem_new).ItemData);

        }

        private Control[] GetULDContent(int uldId)
        {
            List<Control> uldContnent = new List<Control>();
            foreach (var hawb in CargoMatrix.Communication.LoadConsol.Instance.GetULDContent(uldId, ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot))
            {
                List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                foreach (var piece in hawb.Pieces)
                    items.Add(new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", piece.PieceNumber), piece.Location, piece.PieceId));
                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, hawb.HousebillNumber, string.Format("PCS: {0} of {1}", hawb.ScannedPieces, hawb.TotalPieces), hawb.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode, items);
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.IsSelectable = false;
                combo.ContainerName = "ULD";
                uldContnent.Add(combo);
            }

            return uldContnent.ToArray();
        }
        void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            if (comboIsEmpty)
                uldContentList.RemoveItem(sender as Control);
            TransactionStatus status;
            if (subItemId == -1)
            {
                status = CargoMatrix.Communication.LoadConsol.Instance.RemoveHousebillFromUld(sender.ID, ForkLiftViewer.Instance.Mawb.TaskId, selecteduldId, ForkLiftViewer.Instance.Mawb.Mot);
            }
            else
            {
                status = CargoMatrix.Communication.LoadConsol.Instance.RemovePieceFromULD(sender.ID, subItemId, ForkLiftViewer.Instance.Mawb.TaskId, selecteduldId, ForkLiftViewer.Instance.Mawb.Mot);
            }

            if (status.TransactionStatus1 == false)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

            if (uldContentList.smoothListBoxBase1.ItemsCount == 0)
                uldContentList.DialogResult = DialogResult.Cancel;
        }

        //void combo_LayoutChanged(CustomListItems.ComboBox sender, bool comboIsEmpty)
        //{
        //    if (comboIsEmpty)
        //        uldContentList.RemoveItem(sender as Control);
        //    uldContentList.smoothListBoxBase1.LayoutItems();
        //    uldContentList.smoothListBoxBase1.RefreshScroll();

        //}
        void item_OnButtonClick(object sender, EventArgs e)
        {
            SmoothListbox.ListItems.ListItemWithButton item = sender as SmoothListbox.ListItems.ListItemWithButton;
            Control parent = item.Parent;
            parent.Controls.Remove(item);
            parent.Refresh();
        }
    }
}
