﻿namespace CargoMatrix.LoadContainer
{
    partial class OnhandLoader
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUp = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDown = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDrop = new OpenNETCF.Windows.Forms.Button2();
            this.pictureBoxBottomPane = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            //
            // buttonUp
            //
            this.buttonUp.BackColor = System.Drawing.SystemColors.Control;
            this.buttonUp.Location = new System.Drawing.Point(122, 251);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(52, 37);
            this.buttonUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonUp.TransparentColor = System.Drawing.Color.White;
            this.buttonUp.Visible = false;
            this.buttonUp.Image = global::Resources.Graphics.Skin.nav_up;
            this.buttonUp.PressedImage = global::Resources.Graphics.Skin.nav_up_over;
            this.buttonUp.Click += new System.EventHandler(buttonUp_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDown.Location = new System.Drawing.Point(179, 251);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(52, 37);
            this.buttonDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDown.TransparentColor = System.Drawing.Color.White;
            this.buttonDown.Visible = false;
            this.buttonDown.Image = global::Resources.Graphics.Skin.nav_down;
            this.buttonDown.PressedImage = global::Resources.Graphics.Skin.nav_down_over;
            this.buttonDown.Click += new System.EventHandler(buttonDown_Click);
            // 
            // buttonDrop
            // 
            this.buttonDrop.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDrop.Location = new System.Drawing.Point(80, 193);
            this.buttonDrop.Name = "buttonDown";
            this.buttonDrop.Text = "Drop";
            this.buttonDrop.Size = new System.Drawing.Size(80, 35);
            this.buttonDrop.Visible = false;
            this.buttonDrop.ForeColor = System.Drawing.Color.White;
            this.buttonDrop.BackgroundImage = global::Resources.Graphics.Skin.filter_apply;
            this.buttonDrop.ActiveBackgroundImage = global::Resources.Graphics.Skin.filter_apply_over;
            this.buttonDrop.Click += new System.EventHandler(buttonDrop_Click);
            // 
            // pictureBoxBottomPane
            // 
            this.pictureBoxBottomPane.Location = new System.Drawing.Point(0, 191);
            this.pictureBoxBottomPane.Name = "pictureBoxBottomPane";
            this.pictureBoxBottomPane.Size = new System.Drawing.Size(240, 40);
            this.pictureBoxBottomPane.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBottomPane.Image = global::Resources.Graphics.Skin.nav_bg;
            this.pictureBoxBottomPane.Paint += new System.Windows.Forms.PaintEventHandler(pictureBoxBottomPane_Paint);

            this.Controls.Add(this.buttonDown);
            this.Controls.Add(this.buttonUp);
            this.panelHeader2.Height = 231;
            this.panelHeader2.Controls.Add(this.buttonDrop);
            this.panelHeader2.Controls.Add(this.pictureBoxBottomPane);
            this.LoadOptionsMenu += new System.EventHandler(LoadHawbs_LoadOptionsMenu);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.ResumeLayout(false);
        }


        private CargoMatrix.UI.CMXPictureButton buttonUp;
        private CargoMatrix.UI.CMXPictureButton buttonDown;
        private OpenNETCF.Windows.Forms.Button2 buttonDrop;
        private System.Windows.Forms.PictureBox pictureBoxBottomPane;

        #endregion
    }
}
