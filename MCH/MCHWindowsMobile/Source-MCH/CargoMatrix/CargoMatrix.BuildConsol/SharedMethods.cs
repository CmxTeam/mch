﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSLoadConsol;
using CustomUtilities;
using CMXExtensions;
using CargoMatrix.LoadConsol;
using CargoMatrix.FreightPhotoCapture;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;

namespace CargoMatrix.LoadConsol
{
    public class SharedMethods
    {
        public static bool FinalizeAndTakePictures()
        {
            bool finalized = FinalizeLoadContainer();

            // Create Photo Capture task regardles of the device cam presence
            if (finalized)// && Communication.Utilities.CameraPresent)
            {
                Reasons reasonsControl = null;
                ReasonsLogic.CreateTask(ForkLiftViewer.Instance.Mawb.CarrierNumber, ForkLiftViewer.Instance.Mawb.MasterBillNumber, CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL, null, ref reasonsControl);
            }
            return finalized;
        }

        private static bool FinalizeLoadContainer()
        {
            bool result = false;
            bool emptiesRemoved = true;
            bool locationsConfirmed = true;
            foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetAttachedULDs(ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.Mot))
            {
                if (item.Pieces == 0)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_EmptyULD, item.ULDType, item.ULDNo), "Empty ULD", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                    {
                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DeleteULD(item.ID, ForkLiftViewer.Instance.Mawb.Mot, item.FirstHousebill.HousebillId);
                        if (!status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            emptiesRemoved = false;
                        }
                    }
                    else
                    {
                        emptiesRemoved = false;
                        if (!ConfirmULDLocation(item))
                            locationsConfirmed = false;
                    }

                }
                else
                {
                    if (!ConfirmULDLocation(item))
                        locationsConfirmed = false;
                }
            }

            if (emptiesRemoved && locationsConfirmed)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_Finalize, ForkLiftViewer.Instance.Mawb.Reference()), "Finalize Load", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    var status = CargoMatrix.Communication.LoadConsol.Instance.FinalizeConsol(ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot);
                    result = status.TransactionStatus1;
                    if (status.TransactionStatus1 == true)
                        CargoMatrix.UI.CMXAnimationmanager.GoToContol("ConsolList");

                    else
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }

            return result;
        }

        public static IULDType DisplayULDTypes(bool showLoose, CargoMatrix.Communication.ScannerUtilityWS.MOT mot)
        {
            var typeList = new CustomUtilities.SearchMessageListBox();
            typeList.HeaderText = "Select ULD Type";
            typeList.MultiSelectListEnabled = false;
            typeList.OneTouchSelection = true;
            foreach (var uldType in Communication.ScannerUtility.Instance.GetULDTypes(showLoose, mot))
            {
                if (showLoose && uldType.IsLoose)
                    typeList.smoothListBoxBase1.AddItemToFront(new StandardListItem<IULDType>(uldType.ULDName, null, uldType));
                else
                    typeList.smoothListBoxBase1.AddItem2(new StandardListItem<IULDType>(uldType.ULDName, null, uldType));
            }
            typeList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == typeList.ShowDialog())
            {
                return (typeList.SelectedItems[0] as StandardListItem<IULDType>).ItemData;
            }
            else return null;
        }

        private static bool ConfirmULDLocation(CargoMatrix.Communication.LoadConsol.ULD uld)
        {
            CustomUtilities.MultiScanPopup mscanPop = new CustomUtilities.MultiScanPopup();
            mscanPop.Header = uld.ULDNo;

            mscanPop.Caption = "Scan or enter number";
            mscanPop.Label1 = "Enter Seal Number";
            mscanPop.Label2 = "Scan ULD Drop Location (Optional)";
            mscanPop.Text2ISOptional = true;
            mscanPop.Prefix1 = CustomUtilities.BarcodeType.All;
            mscanPop.Prefix2 = CustomUtilities.BarcodeType.Location;

            if (DialogResult.OK == mscanPop.ShowDialog())
            {
                int? locId = null;
                if (!string.IsNullOrEmpty(mscanPop.Text2))
                {
                    locId = Communication.ScannerUtility.Instance.GetLocationId(mscanPop.Text2);

                }
                CargoMatrix.Communication.LoadConsol.Instance.DropULDIntoLocation(ForkLiftViewer.Instance.Mawb.TaskId, uld.ID, locId ?? 0, mscanPop.Text1, ForkLiftViewer.Instance.Mawb.Mot);
                return true;
            }
            return false;
        }

        private static bool ConfirmLoaderULDLocation(CargoMatrix.Communication.LoadConsol.ULD uld)
        {
            /// don't confirm for loose
            if (string.Equals(uld.ULDType, "loose", StringComparison.OrdinalIgnoreCase))
                return true;
            BarcodeReader.TermReader();
            ScanEnterPopup dropLoc = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            dropLoc.Title = "Scan ULD drop location";
            dropLoc.TextLabel = string.Concat("ULD ", uld.ULDType);
            if (uld.ULDNo != string.Empty)
                dropLoc.TextLabel += " - " + uld.ULDNo;
            if (DialogResult.OK == dropLoc.ShowDialog())
            {
                var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(dropLoc.ScannedText);
                /// get scan Ids from backend 
                var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(ForkLiftViewer.getScanItem(scanObj), ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot);
                if (scanItem.Transaction.TransactionStatus1 == false)
                {
                    scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
                }

                if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Area || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Door || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Truck)
                {
                    CargoMatrix.Communication.LoadConsol.Instance.DropULDIntoLocation(ForkLiftViewer.Instance.Mawb.TaskId, uld.ID, scanItem.LocationId, string.Empty, ForkLiftViewer.Instance.Mawb.Mot);
                    return true;
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidLocationBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }
            return false;
        }

        public static bool FinalizeConsol()
        {
            bool result = false;
            bool emptiesRemoved = true;
            bool locationsConfirmed = true;
            foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetAttachedULDs(ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.Mot).OrderBy(c => c.Pieces))
            {
                if (item.Pieces == 0)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_EmptyULD, item.ULDType, item.ULDNo), "Empty ULD", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                    {
                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DeleteULD(item.ID, ForkLiftViewer.Instance.Mawb.Mot, null);
                        if (!status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            emptiesRemoved = false;
                        }
                    }
                    else
                    {
                        emptiesRemoved = false;
                        if (!ConfirmLoaderULDLocation(item))
                            locationsConfirmed = false;
                    }

                }
                else
                {
                    if (!ConfirmLoaderULDLocation(item))
                        locationsConfirmed = false;
                }
            }

            if (emptiesRemoved && locationsConfirmed)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_Finalize, ForkLiftViewer.Instance.Mawb.Reference()), "Finalize Load", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    var status = CargoMatrix.Communication.LoadConsol.Instance.FinalizeConsol(ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot);
                    result = status.TransactionStatus1;
                    if (status.TransactionStatus1 == true)
                        CargoMatrix.UI.CMXAnimationmanager.GoToContol("ConsolList");

                    else
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }

            return result;
        }

    }
}
