﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;
using CMXExtensions;

namespace CargoMatrix.LoadConsol
{
    public class ShareTask
    {
        private static CargoMatrix.Utilities.MessageListBox sharedUsersList;
        private static int _taskId;
        public static void ShowDialog(CargoMatrix.Communication.DTO.IMasterBillItem mawb)
        {
            if (sharedUsersList == null)
            {
                sharedUsersList = new CargoMatrix.Utilities.MessageListBox();
                sharedUsersList.OkEnabled = true;
                sharedUsersList.ButtonVisible = true;
                sharedUsersList.ButtonImage = Resources.Skin.add_btn;
                sharedUsersList.ButtonPressedImage = Resources.Skin.add_btn_over;
                sharedUsersList.ButtonClick += new EventHandler(sharedUsersList_ButtonClick);
                sharedUsersList.IsSelectable = false;
            }
            sharedUsersList.HeaderText = "Users Sharing the Task";
            sharedUsersList.HeaderText2 = mawb.Reference();
            _taskId = mawb.TaskId;
            LoadUsers();
            sharedUsersList.ShowDialog();
        }

        private static void LoadUsers()
        {
            sharedUsersList.RemoveAllItems();
            foreach (var user in CargoMatrix.Communication.LoadConsol.Instance.GetTaskUsers(_taskId))
            {
                if (string.Equals(user.UserCode, CargoMatrix.Communication.WebServiceManager.GetUserID(), StringComparison.OrdinalIgnoreCase))
                {
                    sharedUsersList.AddItemToFront(new SmoothListbox.ListItems.StandardListItem(user.UserName, CargoMatrix.Resources.Skin.worker, user.UserId));
                }
                else
                {
                    var userItem = new SmoothListbox.ListItems.ListItemWithButton(user.UserName, string.Empty, CargoMatrix.Resources.Skin.worker, user.UserId,
                         CargoMatrix.Resources.Skin.cc_trash, CargoMatrix.Resources.Skin.cc_trash_over);
                    userItem.OnButtonClick += new EventHandler(deleteButton_Click);
                    sharedUsersList.AddItem(userItem);
                }
            }
        }

        static void sharedUsersList_ButtonClick(object sender, EventArgs e)
        {
            if (ShareList())
                LoadUsers();
        }

        static void deleteButton_Click(object sender, EventArgs e)
        {
            string user = (sender as SmoothListbox.ListItems.ListItemWithButton).title.Text;
            string msg = "Are you sure you want stop sharing task with {0}";
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(string.Format(msg, user), "Unshare", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                return;


            bool okFlag = false;
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
            {
                if (DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                    okFlag = true;
            }
            else
                okFlag = true;

            if (okFlag)
            {
                var tStatus = CargoMatrix.Communication.LoadConsol.Instance.UnShareLoadConsolTask(_taskId, (sender as SmoothListbox.ListItems.ListItemWithButton).ID);
                if (tStatus.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(tStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                LoadUsers();
            }

        }

        private static bool ShareList()
        {
            bool result = false;
            // user is not admin get supervisor override
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin ||
                DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
            {
                Cursor.Current = Cursors.WaitCursor;
                CustomUtilities.SearchMessageListBox usersList = new CustomUtilities.SearchMessageListBox();
                usersList.HeaderText = "Select users from list";
                usersList.MultiSelectListEnabled = true;

                foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetWarehouseUsers(_taskId))
                {
                    usersList.smoothListBoxBase1.AddItem2(new SmoothListbox.ListItems.StandardListItem<UserItem>(item.UserName, CargoMatrix.Resources.Skin.worker, item));
                }
                usersList.smoothListBoxBase1.LayoutItems();
                usersList.smoothListBoxBase1.RefreshScroll();

                Cursor.Current = Cursors.Default;
                if (DialogResult.OK == usersList.ShowDialog())
                {
                    foreach (SmoothListbox.ListItems.StandardListItem<UserItem> userItem in usersList.SelectedItems)
                    {
                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.ShareLoadConsolTask(_taskId, userItem.ItemData.UserId, MOT.Air);
                        if (status.TransactionStatus1)
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_TaskShared, userItem.ItemData.UserName), "Task Shared", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    }
                    result = true;
                }
            }
            return result;
        }

    }
}
