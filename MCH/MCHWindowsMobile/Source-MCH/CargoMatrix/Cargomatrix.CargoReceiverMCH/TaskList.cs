﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXBarcode;
using SmoothListbox;
using System.Linq;

namespace CargoMatrix.CargoReceiver
{
    public class TaskList : FlightList
    {
        public TaskList()
            : base()
        {
            this.Name = "TaskList";
 
            //this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
        }



        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();
      
                Cursor.Current = Cursors.WaitCursor;
                this.label1.Text = string.Format("{0}/{1}", filter, sort);
                this.searchBox.Text = string.Empty;
                this.Refresh();
                Cursor.Current = Cursors.WaitCursor;


                smoothListBoxMainList.AddItemToFront(new StaticListItem("Receive New", Resources.Icons.Notepad_Add));

                //var items = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillList(Forklift.Instance.ReceiveMode, sort, filter, true);
                //this.TitleText = string.Format("CargoReceiver - ({0})", items.Length);

               

                         CargoMatrix.Communication.DTO.FlightItem[] flightItems = null;

                         FlightStatus status = FlightStatus.All;
                         switch (filter)
                         {
                             case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                                 status = FlightStatus.Pending;
                                 break;
                             case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                                 status = FlightStatus.InProgress;
                                 break;
                             case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                                 status = FlightStatus.Completed;
                                 break;
                             case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                                 status = FlightStatus.Open;
                                 break;
                             default:
                                 status = FlightStatus.All;
                                 break;
                         }

                         FlightSort sortfield = FlightSort.Carrier;
                         switch (sort)
                         {
                             case CargoMatrix.Communication.DTO.FlightSorts.ORIGIN:
                                 sortfield = FlightSort.Origin;
                                 break;
                             case CargoMatrix.Communication.DTO.FlightSorts.CARRIER:
                                 sortfield = FlightSort.Carrier;
                                 break;
                             case CargoMatrix.Communication.DTO.FlightSorts.FLIGHTNO:
                                 sortfield = FlightSort.FlightNo;
                                 break;
                             default:
                                 sortfield = FlightSort.Carrier;
                                 break;
                         }

                         //if (CargoMatrix.Communication.WebServiceManager.Instance().GetFlightsMCH(out flightItems,FlightStatus.All,"","","", FlightSort.Carrier))

                         if (CargoMatrix.Communication.WebServiceManager.Instance().GetFlightsMCH(out flightItems, status, "", "", "", sortfield))
                         {
                             var rItems = from i in flightItems
                                          select InitializeItem(i);
                             smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                         }

                         this.TitleText = string.Format("CargoReceiver - ({0})", flightItems.Length);

                //var rItems = from i in items
                //             select InitializeItem(i);

                //smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());


                         Cursor.Current = Cursors.Default;

            BarcodeEnabled = false;
            BarcodeEnabled = true;



            GotoFlight();

        }

     

        protected override void MawbList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
                SearchFlight();
            else
                base.MawbList_ListItemClicked(sender, listItem, isSelected);
        }


        void SearchFlight()
        {

            this.selectedTaskId = 0;

            string carrierCode = string.Empty;
            string flightNumber = string.Empty;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
           
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "Flight Search";
            actPopup.HeaderText2 = "Select a carrier";
            actPopup.RemoveAllItems();


            CargoMatrix.Communication.WSCargoReceiverMCHService.AvailableItmes[] carrier = CargoMatrix.Communication.WebServiceManager.Instance().GetAvailableCarriers();
            if (carrier != null)
            {
                for (int i = 0; i < carrier.Length; i++)
                {
                    actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(carrier[i].Name, CargoMatrix.Resources.Airlines.GetImage(carrier[i].Name), i + 1));
                }
            }



            if (DialogResult.OK == actPopup.ShowDialog())
            {
                carrierCode = actPopup.SelectedItems[0].Name;
                long carrierId = 0;

                for (int i = 0; i < carrier.Length; i++)
                {
                    if (carrier[i].Name == actPopup.SelectedItems[0].Name)
                    {
                        carrierId = carrier[i].Id;
                    }
                }



                actPopup.HeaderText2 = "Select ETA";
                actPopup.RemoveAllItems();

             

                DateTime[] dates = CargoMatrix.Communication.WebServiceManager.Instance().GetAvailableEtas(carrierId);
                for (int i = 0; i < dates.Length; i++)
                {
                    actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(string.Format("{0:dd-MMM-yy HH:mm}", dates[i]), CargoMatrix.Resources.Skin.status_history, i + 1));
                }

                if (DialogResult.OK == actPopup.ShowDialog())
                {
 
                    string eta = actPopup.SelectedItems[0].Name;

                    actPopup.HeaderText2 = "Select a Flight";
                    actPopup.RemoveAllItems();


                    CargoMatrix.Communication.WSCargoReceiverMCHService.AvailableFlight[] flights = CargoMatrix.Communication.WebServiceManager.Instance().GetAvailableFlightsByEta(carrierId, DateTime.Parse(eta));
                    for (int i = 0; i < flights.Length; i++)
                    {
                        actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(flights[i].Name, CargoMatrix.Resources.Skin.Airplane24x24, i + 1));
                 
                    }

                    if (DialogResult.OK == actPopup.ShowDialog())
                    {

                        long taskId = 0;
                         for (int i = 0; i < flights.Length; i++)
                         {
                             if (flights[i].Name == actPopup.SelectedItems[0].Name)
                             {
                                 taskId = flights[i].TaskId;
                                 if (CargoMatrix.Communication.WebServiceManager.Instance().LinkTaskToUserId(taskId))
                                 {
                                     this.selectedTaskId = taskId;
                                     LoadControl();
                                 }
                                 else
                                 {
                                     CargoMatrix.UI.CMXMessageBox.Show("Unable to link flight.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                 }
                                
                             }
                         }
 
                    }

                }

            }

            // CargoMatrix.Communication.WebServiceManager.Instance()





            //if (DialogResult.OK == actPopup.ShowDialog())
            //{
            //    carrierCode = actPopup.SelectedItems[0].Name;

            //    actPopup.HeaderText2 = "Select a flight number";
            //    actPopup.RemoveAllItems();
            //    actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem("800", CargoMatrix.Resources.Skin.Airplane24x24, 1));
            //    actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem("750", CargoMatrix.Resources.Skin.Airplane24x24, 2));
            //    if (DialogResult.OK == actPopup.ShowDialog())
            //    {
            //        flightNumber = actPopup.SelectedItems[0].Name;
            //        MessageBox.Show(carrierCode + flightNumber);

            //    }

            //}
           

          

        }



        protected override void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            /// show count - 1  -1 is the Receive New button
            this.TitleText = string.Format("CargoReceiver - ({0})", smoothListBoxMainList.VisibleItemsCount - 1);
        }

    }
}
