﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication;
using CargoMatrix.Communication.ScannerUtilityWS;
using CustomListItems;

namespace CargoMatrix.CargoUtilities
{
    public class ReleasePieces
    {
        private static CargoMatrix.Utilities.MessageListBox piecesList;
        public static void ReleaseHawbPiece()
        {
            CustomUtilities.ScanEnterPopup_old hawbPopup = new CustomUtilities.ScanEnterPopup_old(CustomUtilities.BarcodeType.Housebill) { DisplayHousebillPiece = false };
            if (System.Windows.Forms.DialogResult.OK == hawbPopup.ShowDialog())
            {
                piecesList = new CargoMatrix.Utilities.MessageListBox();
                piecesList.MultiSelectListEnabled = true;
                piecesList.OneTouchSelection = false;
                piecesList.HeaderText = hawbPopup.ScannedText;
                piecesList.HeaderText2 = "Select Pieces to Release";


                foreach (var item in ScannerUtility.Instance.GetHawbPiecesInForklift(hawbPopup.ScannedText))
                {
                    var pcs = from pc in item.Pieces
                              select new ComboBoxItemData("Piece " + pc.TrackID.ToString(), null, pc.PieceID) { isReadonly = true };

                    CustomListItems.ComboBox UserItem = new CustomListItems.ComboBox(item.UserID, item.UserName, "Pcs: " + item.Pieces.Count(), CargoMatrix.Resources.Skin.PieceMode, pcs.ToList<ComboBoxItemData>());
                    UserItem.SelectionChanged += new EventHandler(UserItem_SelectionChanged);
                    UserItem.ReadOnly = true;
                    UserItem.IsSelectable = true;
                    piecesList.AddItem(UserItem);

                }
                if (System.Windows.Forms.DialogResult.OK == piecesList.ShowDialog())
                {
                    List<int> piecesToRelease = new List<int>();
                    foreach (var combo in piecesList.smoothListBoxBase1.Items.OfType<ComboBox>())
                    {
                        piecesToRelease.AddRange(combo.SelectedIds);
                    }

                    string piecesStr = string.Join(",", piecesToRelease.ConvertAll<string>(p => p.ToString()).ToArray());

                    if (ScannerUtility.Instance.ReleasePieces(piecesStr))
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);   
                        //string str = string.Format("{0} pieces were released from hawb {1}", piecesToRelease.Count, hawbPopup.ScannedText);
                        //CargoMatrix.UI.CMXMessageBox.Show(str, "Pieces Released", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                    }
                }
            }
        }

        static void UserItem_SelectionChanged(object sender, EventArgs e)
        {

            if ((sender is CustomListItems.ComboBox) == false)
                return;
            CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
            if (piecesList.smoothListBoxBase1.SelectedItems.Count == 0)
            {
                bool enableflag = false;
                if (item.SubItemSelected)
                {
                    enableflag = true;
                }
                piecesList.OkEnabled = enableflag;
            }
            else piecesList.OkEnabled = true;
        }
    }

}
