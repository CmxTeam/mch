﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.CargoUtilities
{
    public class EditHawbSkel
    {
        public static void Edit()
        {
            CustomUtilities.ScanEnterPopup_old hawbPopup = new CustomUtilities.ScanEnterPopup_old(CustomUtilities.BarcodeType.Housebill) { DisplayHousebillPiece = false };
            if (System.Windows.Forms.DialogResult.OK == hawbPopup.ShowDialog())
            {
                Cursor.Current = Cursors.WaitCursor;
                var hawb = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(hawbPopup.ScannedText);

                int pieces = hawb.TotalPieces;
                string origin = hawb.Origin;
                string dest = hawb.Destination;
                Cursor.Current = Cursors.Default;
                if (DialogResult.OK == CustomUtilities.OriginDestinationMessageBox.Show2(hawbPopup.ScannedText, ref pieces, ref origin, ref dest))
                {
                    if (CargoMatrix.Communication.ScannerUtility.Instance.UpdateHawbDetails(hawb.HousebillId, origin, dest, pieces))
                        CargoMatrix.UI.CMXMessageBox.Show("Hawb " + hawb.HousebillNumber + " was updated", "Updated", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                    else
                        CargoMatrix.UI.CMXMessageBox.Show("Operation Failed", "Fail", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }
        }
    }
}
