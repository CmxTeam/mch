﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CustomUtilities;
using CargoMatrix.UI;

namespace CargoMatrix.CargoUtilities
{
    public partial class EnterNumberControl : SmoothListbox.SmoothListbox2
    {
        ScanActionTypeEnum scanActionType;
        WorkItemTypeEnum workItemType;

        public EnterNumberControl(ScanActionTypeEnum scanActionType, WorkItemTypeEnum workItemType)
        {
            InitializeComponent();

            this.scanActionType = scanActionType;
            this.workItemType = workItemType;

            this.panel1.Height = 0;
            this.panelHeader2.Height = 0;

            this.TitleText = "CargoUtilities";
        }

        public override void LoadControl()
        {
            string scannedText;
            string scannedItemMainPart;
            CMXBarcode.ScanObject scanItem;

            bool result = CommonMethods.GetItemFromUser(workItemType, out scannedText, out scannedItemMainPart, out scanItem);

            if (result == false)
            {
                CMXAnimationmanager.GoBack();
                return;
            }

            switch (this.scanActionType)
            {
                case ScanActionTypeEnum.Browse:

                    CommonMethods.DisplayBillViewer(scannedItemMainPart, this.Left, this.Width);
                    break;

                case ScanActionTypeEnum.CalibratePrinter_Bluetooth:

                    CustomUtilities.MobilePrinting.MobilePrinterUtility.CalibrateBlueToothPrinter(scannedText);

                    Application.DoEvents();

                    this.LoadControl();

                    break;

                case ScanActionTypeEnum.CalibratePrinter_Wireless:

                    var port = CommonMethods.GetPortNumber();

                    if (port.HasValue == true)
                    {
                        CustomUtilities.MobilePrinting.MobilePrinterUtility.CalibrateWirelessPrinter(scannedText, port.Value);
                    }

                    Application.DoEvents();

                    this.LoadControl();


                    break;
            }

        }
    }
}
