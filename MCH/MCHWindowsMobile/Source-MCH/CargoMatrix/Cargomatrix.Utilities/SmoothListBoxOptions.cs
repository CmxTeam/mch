﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.UI;

namespace CargoMatrix.CargoUtilities
{
    public abstract class SmoothListBoxOptions : SmoothListbox.SmoothListbox
    {
        private CustomListItems.OptionsListITem utilitesOptionsListItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.UTILITIES);
        private CustomListItems.OptionsListITem messagesOptionsListItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MESSAGES);
        private CustomListItems.OptionsListITem mainMenuOptionsListItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU);
        private CustomListItems.OptionsListITem logOutOptionsListItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT);

        /// <summary>
        /// Gets utilities options list item
        /// </summary>
        public CustomListItems.OptionsListITem UtilitesOptionsListItem
        {
            get { return utilitesOptionsListItem; }
        }
        public CustomListItems.OptionsListITem MessagesOptionsListItem
        {
            get { return messagesOptionsListItem; }
        }
        public SmoothListBoxOptions()
        {
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(SmoothListBoxOptions_MenuItemClicked);
            this.LoadOptionsMenu += new EventHandler(SmoothListBoxOptions_LoadOptionsMenu);
        }

        void SmoothListBoxOptions_LoadOptionsMenu(object sender, EventArgs e)
        {
            ////this.AddOptionsListItem(messagesOptionsListItem);
            //this.AddOptionsListItem(mainMenuOptionsListItem);
            //this.AddOptionsListItem(logOutOptionsListItem);
        }

        void SmoothListBoxOptions_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.UTILITIES:

                    CMXAnimationmanager.DisplayForm(new UtilitiesList());

                    break;
                case CustomListItems.OptionsListITem.OptionItemID.MESSAGES:
                    //CargoMatrix.Messageing.MessageManager.Instance.DipslayMessages();
                    break;

                case CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU:
                    UI.CMXAnimationmanager.DisplayDefaultForm();
                    break;
                case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
                    if (UI.CMXAnimationmanager.ConfirmLogout())
                        UI.CMXAnimationmanager.DoLogout();
                    break;
            }
        }
    }
}
