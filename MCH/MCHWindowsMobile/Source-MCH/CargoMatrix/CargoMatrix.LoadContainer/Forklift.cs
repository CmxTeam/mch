﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Utilities;
using CustomListItems;
using CargoMatrix.Communication.WSLoadConsol;
using System.Windows.Forms;
using System.Drawing;
using CustomUtilities;
using CMXBarcode;
using CargoMatrix.LoadConsol;
using CMXExtensions;
#if inlcude
namespace CargoMatrix.LoadContainer
{
    class Forklift : CargoMatrix.Utilities.MessageListBox
    {
        private static Forklift instance;
        private bool isLoaded;
        private CargoMatrix.Communication.ScannerUtilityWS.TaskSettings settings;
        private MessageListBox uldContentList;
        private MessageListBox uldList;
        private CargoMatrix.Communication.DTO.IMasterBillItem _mawb;
        private int itemCount;
        private CargoMatrix.UI.BarcodeReader barcode;


        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                    instance = new Forklift();


                if (instance.isLoaded == false)
                    instance.loadAppSettings();

                return instance;
            }
        }

        public static bool Isloaded
        {
            get { return Instance.isLoaded; }
        }

        private void loadAppSettings()
        {
            var tempSettings = CargoMatrix.Communication.LoadConsol.Instance.GetTaskSettings();
            if (tempSettings.Transaction.TransactionStatus1 == true)
            {
                settings = tempSettings;
                this.isLoaded = true;
            }
            else
            {
                this.isLoaded = false;
                CargoMatrix.UI.CMXMessageBox.Show("Error has occured while downloading task settings. Refresh or restart the application", "Unable to connect", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }



        public event EventHandler ItemCountChanged;
        public ConsolMode Mode { get; set; }

        #region settings
        internal int ForkliftID
        { get { return settings.ForkliftId; } }
        public bool ScanHawbToULD
        {
            get { return settings.ScanHAWBtoULD; }
        }
        public bool AllowULDEdit
        {
            get { return settings.AllowULDEdit; }
        }
        public bool ChangeHawbScanModeAllowed
        { get { return settings.ChangeHawbScanModeAllowed; } }

        public bool ManualModeConfirmRequired { get { return settings.ManualModeConfirmRequired; } }
        public bool CaptureULDWeight
        {
            //UI Flow is not implemented
            get { return settings.CaptureULDWeight; } //
        }
        public bool OverWeightPalletCheck
        {
            get { return false; } //settings.OverWeightPalletCheck
        }
        public bool MatchULDNoToCarrier { get { return false; } }
        public bool DoubleScanEnabled { get { return settings.ScanningSteps == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.TwoScan; } }
        #endregion

        public string CarrierCode
        {
            get;
            private set;
        }
        public Image CarrierLogo
        {
            get;
            private set;
        }

        private bool HasLoose
        {
            get
            {
                var item = uldList.Items.OfType<ULDListItem_new>().FirstOrDefault<ULDListItem_new>(uld => uld.ItemData.IsLoose);
                return item != null;
            }
        }


        private void LoadIntoULD(int uldId)
        {
            TransactionStatus finalStatus = new TransactionStatus { TransactionStatus1 = true };

            List<PieceItem> pieceItems;
            GetPieceItems(out pieceItems);
            finalStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadPiecesIntoULDPiece(uldId, pieceItems.ToArray<PieceItem>(), Mawb.TaskId, ScanTypes.Manual, ForkliftID);

            if (finalStatus.TransactionStatus1 == false)
                CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

            if (FinalizeReady)
            {
                FinalizeConsol();
            }
            else
                if (smoothListBoxBase1.ItemsCount == 0)
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                else
                    smoothListBoxBase1.LayoutItems();
        }

        private void LoadIntoLocation(int locId, int uldId)
        {
            List<PieceItem> pieceItems;
            GetPieceItems(out pieceItems);

            var status = CargoMatrix.Communication.LoadConsol.Instance.LoadPiecesIntoLocation(uldId, locId, pieceItems.ToArray<PieceItem>(), Mawb.TaskId, ScanTypes.Manual, ForkliftID);
            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return;
            }

            if (FinalizeReady)
            {
                FinalizeConsol();
            }
            else
                if (smoothListBoxBase1.ItemsCount == 0)
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                else
                    smoothListBoxBase1.LayoutItems();
        }


        private void PullToLocation(int locId)
        {
            List<PieceItem> pieceItems;
            GetPieceItems(out pieceItems);

            TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DropPiecesIntoLocation(pieceItems.ToArray<PieceItem>(), Mawb.TaskId, locId, ForkliftID);
            if (status.TransactionStatus1 == false)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            else
                ItemsCount = status.TransactionRecords;

            if (smoothListBoxBase1.ItemsCount == 0)
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
            else
                smoothListBoxBase1.LayoutItems();
        }

        private void GetPieceItems(out List<PieceItem> pieceItems)
        {
            pieceItems = new List<PieceItem>();
            if (settings.ScanningSteps == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.TwoScan)
            {
                foreach (var item in smoothListBoxBase1.Items)
                {
                    if (item is CustomListItems.ComboBox)
                    {
                        foreach (var pieceId in (item as CustomListItems.ComboBox).CheckedIds)
                            pieceItems.Add(new PieceItem() { HouseBillId = (item as CustomListItems.ComboBox).ID, PieceId = pieceId });
                    }
                }
            }
            else
                foreach (var item in smoothListBoxBase1.Items)
                {
                    if (item is CustomListItems.ComboBox)
                    {
                        foreach (var pieceId in (item as CustomListItems.ComboBox).SelectedIds)
                            pieceItems.Add(new PieceItem() { HouseBillId = (item as CustomListItems.ComboBox).ID, PieceId = pieceId });
                    }
                }
        }

        public CargoMatrix.Communication.DTO.IMasterBillItem Mawb
        {
            get { return _mawb; }
            set
            {
                _mawb = value;
                var carrier = CargoMatrix.Communication.ScannerUtility.Instance.GetCarrierInfo(_mawb.CarrierNumber);
                CarrierCode = carrier.CarrierCode;
                CarrierLogo = CargoMatrix.Communication.Utilities.ConvertToImage(carrier.CarrierLogo);
                ItemsCount = CargoMatrix.Communication.LoadConsol.Instance.GetForkliftItemsCount(Mawb.TaskId, ForkliftID);
            }
        }
        public int ItemsCount
        {
            get
            {
                return itemCount;
            }
            set
            {
                itemCount = value;
                if (ItemCountChanged != null)
                    ItemCountChanged(null, null);
            }
        }
        private Forklift()
        {
            smoothListBoxBase1.MultiSelectEnabled = true;
            //panel1.Height = 0;
            //panelHeader2.Height = 0;
            this.HeaderText = "Forklift Content";
            this.HeaderText2 = "Select shipments to drop into consol";
            this.LoadListEvent += new LoadSmoothList(ForkLiftViewer_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ForkLiftViewer_ListItemClicked);
            barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(ForkliftScan);
        }


        void ForkLiftViewer_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var combo = listItem as CustomListItems.ComboBox<HouseBillItem>;
            if (combo.Data.ScanMode == ScanModes.Count)
            {
                combo.SelectedChanged(!isSelected);
                CountMessageBox cnt = new CountMessageBox();
                cnt.MinPiecesCount = 0;
                cnt.LabelReference = combo.Data.HousebillNumber;
                cnt.LabelDescription = "Enter Number of Pieces to Select";
                cnt.PieceCount = combo.Items.Count;
                if (DialogResult.OK == cnt.ShowDialog())
                {
                    combo.SelectSubitems(cnt.PieceCount);
                }
            }
            combo_SelectionChanged(combo, EventArgs.Empty);
            //UpdateDescriptionLine(combo);

        }

        void ForkLiftViewer_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxBase1.RemoveAll();
            int forkliftPieces = 0;
            OkEnabled = false;
            var hawbs = CargoMatrix.Communication.LoadConsol.Instance.GetForkliftContent(Mawb.MasterBillId, Mawb.TaskId, HouseBillFields.NA, ForkliftID);
            if (hawbs.Length == 0)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            foreach (var hawb in hawbs)
            {
                List<ComboBoxItemData> pieces = new List<ComboBoxItemData>();

                forkliftPieces += hawb.Pieces.Length;
                foreach (var hawbPiece in hawb.Pieces)
                {
                    pieces.Add(new ComboBoxItemData<PieceInfo>(new PieceInfo(hawbPiece.PieceNumber), string.Format("Piece {0}", hawbPiece.PieceNumber), "Loc: " + hawbPiece.Location, hawbPiece.PieceId));
                }
                Image icon;
                string line2;
                if (hawb.ScanMode == ScanModes.Piece)
                {
                    icon = CargoMatrix.Resources.Skin.PieceMode;
                }
                else
                {
                    icon = CargoMatrix.Resources.Skin.countMode;
                }
                line2 = string.Format("Pieces: {0} of {1}", 0, hawb.Pieces.Length);
                CustomListItems.ComboBox<HouseBillItem> combo = new CustomListItems.ComboBox<HouseBillItem>(hawb, hawb.HousebillId, hawb.HousebillNumber, line2, icon, pieces);

                combo.Expandable = hawb.ScanMode == ScanModes.Piece;

                if (settings.ScanningSteps == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.TwoScan)
                    combo.IsSelectable = false;

                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                smoothListBoxBase1.AddItem(combo);
            }
            Cursor.Current = Cursors.Default;
            ItemsCount = forkliftPieces;
        }

        void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            if (comboIsEmpty)
                smoothListBoxBase1.RemoveItem(sender as Control);
            combo_SelectionChanged(null, null);
            smoothListBoxBase1.LayoutItems();
            TransactionStatus finalStatus = new TransactionStatus() { TransactionStatus1 = true };
            if (subItemId == -1)
            {
                finalStatus = CargoMatrix.Communication.LoadConsol.Instance.RemoveHousebillFromForklift(sender.ID, Mawb.TaskId, ForkliftID);

            }
            else
            {
                finalStatus = CargoMatrix.Communication.LoadConsol.Instance.RemovePieceFromForklift(sender.ID, subItemId, Mawb.TaskId, CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece, ForkliftID);
                if (finalStatus.TransactionStatus1)
                {
                    UpdateDescriptionLine(sender as ComboBox<HouseBillItem>);
                }
            }
            if (!finalStatus.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            else
                ItemsCount = finalStatus.TransactionRecords;

            if (smoothListBoxBase1.ItemsCount == 0)
                this.DialogResult = DialogResult.Cancel;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if ((sender is CustomListItems.ComboBox) == false)
                return;
            CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
            UpdateDescriptionLine(item);
            if (smoothListBoxBase1.SelectedItems.Count == 0)
            {
                bool enableflag = false;
                if (item.SubItemSelected)
                {
                    enableflag = true;
                }
                OkEnabled = enableflag;
            }
            else OkEnabled = true;
        }

        private void UpdateDescriptionLine(CustomListItems.ComboBox sender)
        {
            if (sender is CustomListItems.ComboBox)
            {
                CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
                item.Line2 = string.Format("Pieces: {0} of {1}", item.SelectedIds.Count(), item.Items.Count);
            }
        }
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            if (Mode == ConsolMode.Load)
                DropinLoadMode();

            if (Mode == ConsolMode.Pull)
                DropinPullMode();
            LoadControl();
        }

        private void DropinPullMode()
        {
            ScanEnterPopup droploc = new ScanEnterPopup();
            droploc.TextLabel = "Scan Drop Location";
            if (DialogResult.OK == droploc.ShowDialog())
            {
                ScanObject scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(droploc.ScannedText);
                /// get scan Ids from backend 
                var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(Forklift.getScanItem(scanObj), Mawb.TaskId);
                if (scanItem.Transaction.TransactionStatus1 == false)
                {
                    scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
                }

                if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Area || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Door)
                {
                    PullToLocation(scanItem.LocationId);
                    if (smoothListBoxBase1.ItemsCount != 0)
                        smoothListBoxBase1.SelectAll();
                    else
                        OkEnabled = false;
                }
                else if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Uld || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Truck)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        Mode = ConsolMode.Load;
                        DropinLoadMode();
                    }
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }
        }

        private void DropinLoadMode()
        {
            uldList = new MessageListBox();
            uldList.ListItemClicked += new SmoothListbox.ListItemClickedHandler(uldList_ListItemClicked);
            uldList.WideWidth();
            uldList.HeaderText = Mawb.Reference();
            uldList.HeaderText2 = "Select ULD to drop:";
            uldList.MultiSelectListEnabled = false;
            uldList.OneTouchSelection = false;
            uldList.OkEnabled = false;
            DisplayExistingULDs();
            if (DialogResult.OK == uldList.ShowDialog())
            {
                if (smoothListBoxBase1.ItemsCount != 0)
                    smoothListBoxBase1.SelectAll();
                else
                {
                    this.DialogResult = DialogResult.Cancel;
                }
            }
        }

        private void DisplayExistingULDs()
        {
            uldList.RemoveAllItems();
            uldList.AddItem(new SmoothListbox.ListItems.StandardListItem("Add New ULD", LoadConsolResources.Freight_Car_Add));

            foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetAttachedULDs(Mawb.MasterBillId))
            {
                ULDListItem_new uld = new CustomListItems.ULDListItem_new(item, false, false);// { Logo = CargoMatrix.Resources.Skin.Freight_Car, Line2 = string.Format("PCS: {0} WT: {1}KG", item.Pieces, item.Weight) };
                uld.ButtonDeleteClick += new EventHandler(Ulditem_ButtonDeleteClick);
                uld.ButtonEditClick += new EventHandler(Ulditem_ButtonEditClick);
                uld.ButtonEnterClick += new EventHandler(Ulditem_ButtonEnterClick);
                uld.ButtonBrowseClick += new EventHandler(Ulditem_ButtonBrowseClick);
                uldList.AddItem(uld);
            }
        }

        void uldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is SmoothListbox.ListItems.StandardListItem)
            {
                ULDEditor.Instance.AddULDItem(!HasLoose);
                DisplayExistingULDs();
                sender.Reset();
            }
            else
            {
                if (listItem is CustomListItems.ULDListItem_new && !(listItem as CustomListItems.ULDListItem_new).Expandable)
                    Ulditem_ButtonEnterClick(listItem, EventArgs.Empty);
            }
            uldList.OkEnabled = false;
        }

        #region copy

        private void PopulateUldContent(CargoMatrix.Communication.DTO.IULDItem uld)
        {
            Cursor.Current = Cursors.WaitCursor;

            uldContentList = new CargoMatrix.Utilities.MessageListBox();
            uldContentList.LabelEmpty = "ULD is Empty";
            uldContentList.HeaderText = string.Format("{0} - {1}", uld.ULDName, uld.ULDNo);
            uldContentList.HeaderText2 = "ULD Content";
            uldContentList.OkEnabled = false;
            uldContentList.IsSelectable = false;

            uldContentList.AddItems(GetULDContent(uld.ID));

            Cursor.Current = Cursors.Default;

            uldContentList.ShowDialog();
        }

        private List<Control> GetULDContent(int uldId)
        {
            List<Control> forkliftcontnent = new List<Control>();
            foreach (var hawb in CargoMatrix.Communication.LoadConsol.Instance.GetULDContent(uldId, Mawb.MasterBillId, Mawb.TaskId))
            {
                List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                foreach (var piece in hawb.Pieces)
                    items.Add(new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", piece.PieceNumber), piece.Location, piece.PieceId));
                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, hawb.HousebillNumber, string.Format("PCS: {0} of {1}", hawb.ScannedPieces, hawb.TotalPieces), hawb.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode, items);
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.IsSelectable = false;
                combo.ContainerName = "ULD";
                forkliftcontnent.Add(combo);
            }

            return forkliftcontnent;
        }

        void Ulditem_ButtonBrowseClick(object sender, EventArgs e)
        {
            PopulateUldContent((sender as ULDListItem_new).ItemData);

        }

        void Ulditem_ButtonEditClick(object sender, EventArgs e)
        {
            if (sender is ULDListItem_new)
            {
                ULDListItem_new item = sender as ULDListItem_new;
                ULDEditor.Instance.EditUldItem(item, !HasLoose);
                if (item.ItemData.IsLoose)
                    uldList.smoothListBoxBase1.MoveItemToTop(item);
                uldList.smoothListBoxBase1.LayoutItems();
            }
        }

        void Ulditem_ButtonEnterClick(object sender, EventArgs e)
        {
            //if (ForkLiftViewer.Instance.ItemsCount > 0)
            if ((sender as CustomListItems.ULDListItem_new).ItemData.IsLoose)
            {
                ScanEnterPopup droploc = new ScanEnterPopup();
                droploc.TextLabel = "Scan Drop Location";
                if (DialogResult.OK == droploc.ShowDialog())
                {
                    var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(droploc.ScannedText);
                    /// get scan Ids from backend 
                    var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(Forklift.getScanItem(scanObj), Mawb.TaskId);
                    if (scanItem.Transaction.TransactionStatus1 == false)
                    {
                        scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
                    }

                    if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Area || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Door || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Truck)
                    {
                        Forklift.Instance.LoadIntoLocation(scanItem.LocationId, (sender as CustomListItems.ULDListItem_new).ItemData.ID);
                        this.smoothListBoxBase1.Reset();
                        uldList.DialogResult = DialogResult.OK;
                    }
                    else
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                Forklift.Instance.LoadIntoULD((sender as CustomListItems.ULDListItem_new).ItemData.ID);
                this.smoothListBoxBase1.Reset();
                uldList.DialogResult = DialogResult.OK;
                Cursor.Current = Cursors.Default;
            }
            DisplayExistingULDs();
        }



        void Ulditem_ButtonDeleteClick(object sender, EventArgs e)
        {
            if (ULDEditor.Instance.DeleteULDItem((sender as ULDListItem_new)))
                DisplayExistingULDs(); // if delete was successful reload the uld items
        }
        private void ShowFailMessage(string message)
        {
            CargoMatrix.UI.CMXMessageBox.Show(message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }
        #endregion


        #region Consolmembers
        internal string FinalizeMessage { get; private set; }
        public bool FinalizeReady
        {
            get
            {
                if (Mode == ConsolMode.Load)
                {
                    var transaction = CargoMatrix.Communication.LoadConsol.Instance.IsLoadConsolReady(Mawb.TaskId);
                    FinalizeMessage = transaction.TransactionError;
                    return transaction.TransactionStatus1;
                }
                else
                {
                    FinalizeMessage = string.Empty;
                    return false;
                }
            }
        }
        public bool FinalizeConsol()
        {
            bool result = false;
            bool emptiesRemoved = true;
            bool locationsConfirmed = true;
            foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetAttachedULDs(Mawb.MasterBillId).OrderBy(c => c.Pieces))
            {
                if (item.Pieces == 0)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_EmptyULD, item.ULDType, item.ULDNo), "Empty ULD", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                    {
                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DeleteULD(item.ID);
                        if (!status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            emptiesRemoved = false;
                        }
                    }
                    else
                    {
                        emptiesRemoved = false;
                        if (!ConfirmULDLocation(item))
                            locationsConfirmed = false;
                    }

                }
                else
                {
                    if (!ConfirmULDLocation(item))
                        locationsConfirmed = false;
                }
            }

            if (emptiesRemoved && locationsConfirmed)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_Finalize, Mawb.Reference()), "Finalize Load", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    var status = CargoMatrix.Communication.LoadConsol.Instance.FinalizeConsol(Mawb.TaskId);
                    result = status.TransactionStatus1;
                    if (status.TransactionStatus1 == true)
                        CargoMatrix.UI.CMXAnimationmanager.GoToContol("ConsolList");

                    else
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }

            return result;
        }

        private bool ConfirmULDLocation(CargoMatrix.Communication.LoadConsol.ULD uld)
        {
            /// don't confirm for loose
            if (string.Equals(uld.ULDType, "loose", StringComparison.OrdinalIgnoreCase))
                return true;

            ScanEnterPopup dropLoc = new ScanEnterPopup();
            dropLoc.Title = "Scan ULD drop location";
            dropLoc.TextLabel = string.Concat("ULD ", uld.ULDType);
            if (uld.ULDNo != string.Empty)
                dropLoc.TextLabel += " - " + uld.ULDNo;
            if (DialogResult.OK == dropLoc.ShowDialog())
            {
                var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(dropLoc.ScannedText);
                /// get scan Ids from backend 
                var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(Forklift.getScanItem(scanObj), Mawb.TaskId);
                if (scanItem.Transaction.TransactionStatus1 == false)
                {
                    scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
                }

                if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Area || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Door || scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Truck)
                {
                    CargoMatrix.Communication.LoadConsol.Instance.DropULDIntoLocation(Mawb.TaskId, uld.ID, scanItem.LocationId);
                    return true;
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidLocationBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }
            return false;
        }
        #endregion

        public new DialogResult ShowDialog()
        {
            if (settings.ScanningSteps == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.TwoScan)
            {
                smoothListBoxBase1.IsSelectable = false;
                barcode.StartRead();
            }
            else
            {
                smoothListBoxBase1.IsSelectable = true;

            }
            DialogResult dr = (instance as MessageListBox).ShowDialog();
            barcode.StopRead();
            return dr;

        }

        void ForkliftScan(string barcodeData)
        {
            global::CMXBarcode.ScanObject scanObject = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            if (scanObject.BarcodeType == CMXBarcode.BarcodeTypes.HouseBill)
            {

                var combo = (from item in this.Items.OfType<CustomListItems.ComboBox>()
                             where string.Equals(item.ComboText, scanObject.HouseBillNumber, StringComparison.OrdinalIgnoreCase)
                             select item).FirstOrDefault();

                if (combo == null)
                    CargoMatrix.UI.CMXMessageBox.Show("Scanned Housebill was not found in Forklift", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                else
                {
                    bool pieceFound = false;
                    foreach (ComboBoxItemData<PieceInfo> item in combo.Items)
                    {
                        if (item.Data.pieceNumber == scanObject.PieceNumber)
                        {
                            CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                            item.Data.scanned = !item.Data.scanned;
                            combo.UpdateSubItemState<ComboBoxItemData<PieceInfo>>(item.id, item.Data.scanned);
                            pieceFound = true;
                            setOkState();
                            break;
                        }
                    }

                    if (pieceFound == false)
                        CargoMatrix.UI.CMXMessageBox.Show("Scanned Piece was not found in Forklift", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

                }
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid Type of Barcode Has Been Scanned", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }

            barcode.StartRead();
        }

        private void setOkState()
        {
            bool enableflag = false;

            foreach (var item in smoothListBoxBase1.Items)
                if (item is CustomListItems.ComboBox && (item as CustomListItems.ComboBox).SubItemChecked)
                {
                    enableflag = true;
                    break;
                }
            OkEnabled = enableflag;
        }

        internal static ScanItem getScanItem(CMXBarcode.ScanObject scanObj)
        {
            ScanItem sc = new ScanItem();
            switch (scanObj.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.Area;
                    sc.Location = scanObj.Location;
                    break;
                case CMXBarcode.BarcodeTypes.Door:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.Door;
                    sc.Location = scanObj.Location;
                    break;
                case CMXBarcode.BarcodeTypes.HouseBill:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.HouseBill;
                    sc.HouseBillNumber = scanObj.HouseBillNumber;
                    sc.PieceNumber = scanObj.PieceNumber;
                    break;
                case CMXBarcode.BarcodeTypes.MasterBill:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.MasterBill;
                    sc.MasterBillNumber = scanObj.MasterBillNumber;
                    sc.CarrierNumber = scanObj.CarrierNumber;
                    break;
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.ScreeningArea;
                    sc.Location = scanObj.Location;
                    break;
                case CMXBarcode.BarcodeTypes.Truck:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.Truck;
                    sc.Location = scanObj.Location;
                    break;
                case CMXBarcode.BarcodeTypes.Uld:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.Uld;
                    sc.UldNumber = scanObj.UldNumber;
                    break;
                case CMXBarcode.BarcodeTypes.User:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.User;
                    sc.UserNumber = scanObj.UserNumber;
                    break;
                case CMXBarcode.BarcodeTypes.NA:
                default:
                    sc.BarcodeType = CargoMatrix.Communication.WSLoadConsol.BarcodeTypes.NA;
                    sc.BarcodeData = scanObj.BarcodeData;
                    break;
            }
            return sc;
        }

    }
    public class PieceInfo
    {
        public int pieceNumber;
        public bool scanned;
        public PieceInfo(int pcNumber, bool scaned)
        {
            pieceNumber = pcNumber;
            scanned = scaned;
        }
        public PieceInfo(int pcNumber)
            : this(pcNumber, false)
        { }
    }
    public enum ConsolMode
    {
        Pull, Load
    }
}

#endif