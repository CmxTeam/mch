﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.FreightPhotoCapture;

namespace CargoMatrix.LoadContainer
{
    public partial class LoadContainerTasks : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        protected string filter = "Not Completed";
        protected string sort = "Pickup Reference";

        public LoadContainerTasks()
        {
            InitializeComponent();
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(LoadContainerTasks_ListItemClicked);
        }

        void LoadContainerTasks_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            Reasons reasons = new CargoMatrix.FreightPhotoCapture.Reasons(false);

            ReasonsLogic.DisplayReasons("006", "95908212", CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL, new UserControl(), ref reasons);
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.RemoveAll();
            this.smoothListBoxMainList.AddItem(new SmoothListbox.ListItems.StandardListItem("Receive New", null, 1));
            this.labelFilterSort.Text = string.Format("{0}/{1}", filter, sort);
            this.TitleText = string.Format("OnHand Tasks ({0})", smoothListBoxMainList.ItemsCount - 1);
        }

        void searchBox_TextChanged(object sender, System.EventArgs e)
        {

        }
    }
}
