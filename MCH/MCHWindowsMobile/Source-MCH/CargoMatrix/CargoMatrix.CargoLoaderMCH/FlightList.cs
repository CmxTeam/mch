﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;

namespace CargoMatrix.CargoLoader
{
    public partial class FlightList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;

        public FlightList()
        {

            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(FlightList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(FlightList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(FlightList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FlightList_ListItemClicked);
        }

        protected virtual void FlightList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //if (CargoMatrix.Communication.Utilities.IsAdmin)
            //{
                Flight_Enter_Click(listItem, null);
            //}
            //else
            //{
            //    smoothListBoxMainList.MoveControlToTop(listItem);
            //    smoothListBoxMainList.LayoutItems();
            //}

        }
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;

            if (ShowFilterPopup() == false)
                BarcodeEnabled = true;
        }
        void FlightList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Flight);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter + " / " + sort;
                LoadControl();
                return true;
            }
            return false;
        }

        void FlightList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

            this.label1.Text = string.Format("{0}/{1}", filter, sort);
            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;


            TaskStatuses status = TaskStatuses.All;
            switch (filter)
            {
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                    status = TaskStatuses.Pending;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                    status = TaskStatuses.InProgress;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                    status = TaskStatuses.Completed;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                    status = TaskStatuses.Open;
                    break;
                default:
                    status = TaskStatuses.All;
                    break;
            }

            ReceiverSortFields sortfield = ReceiverSortFields.Carrier;
            switch (sort)
            {
                case CargoMatrix.Communication.DTO.FlightSorts.DESTINATION:
                    sortfield = ReceiverSortFields.Destination;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.CARRIER:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.FLIGHTNO:
                    sortfield = ReceiverSortFields.FlightNo;
                    break;
                default:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
            }

 

            CargoLoaderFlight[] flightItems = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlights(status, "", "", "", sortfield);
            if (flightItems != null)
            {
                var rItems = from i in flightItems
                             select InitializeItem(i);
                smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
            }



            this.TitleText = string.Format("CargoLoader - ({0})", smoothListBoxMainList.VisibleItemsCount);

            Cursor.Current = Cursors.Default;
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoLoader - {0}", smoothListBoxMainList.VisibleItemsCount);
        }

        protected ICustomRenderItem InitializeItem(CargoLoaderFlight item)
        {
            FlightCargoItem tempMAWB = new FlightCargoItem(item);
            tempMAWB.OnEnterClick += new EventHandler(Flight_Enter_Click);
            listItemHeight = tempMAWB.Height;
            return tempMAWB;
        }

        void Flight_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            var mawb = (sender as FlightCargoItem).ItemData;
            Cursor.Current = Cursors.Default;
        }

        private void ShowAlerts(long manifestId, string reference)
        {
            Alert[] alerts = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetFlightAlerts(manifestId);
            foreach (Alert item in alerts)
            {
                CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }

        protected virtual void Flight_Enter_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            CargoLoaderFlight flight = (sender as FlightCargoItem).ItemData;


            string reference = string.Format("{0}{1}", flight.CarrierCode, flight.FlightNumber);
            ShowAlerts(flight.FlightManifestId, reference);

            if (flight == null)
                return;

            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightLegList(flight));
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightLegUldList(flight));

            Cursor.Current = Cursors.Default;
        }

        //private bool ProceedWithFlight(CargoLoaderFlight tempFlight)
        //{
        //    if (tempFlight == null)
        //        return false;
 
        //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightLegList(tempFlight));
        //    Cursor.Current = Cursors.Default;
        //    return true;
        //}

        void FlightList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            CargoMatrix.Communication.WSCargoReceiver.MasterBillItem tempMawb = null;
            switch (barcode.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    break;
                case BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    break;
                default:
                    CargoMatrix.UI.CMXMessageBox.Show(CargoMatrix.CargoLoaderMCH.Resource.Text_ScanHwbMawb, "Invalid Barcode", CMXMessageBoxIcon.Hand);
                    break;
            }

        }
    }

}
