﻿namespace CargoMatrixScanner
{
    partial class CMXPasswordBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new CargoMatrix.UI.CMXAlphaNumTextBox();
            this.labelMask = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.AcceptsReturn = true;
            this.textBox.AcceptsTab = true;
            this.textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.textBox.Location = new System.Drawing.Point(1, 1);
            this.textBox.Name = "textBox";
            this.textBox.PasswordChar = '*';
            this.textBox.Size = new System.Drawing.Size(158, 26);
            this.textBox.TabIndex = 0;
            this.textBox.GotFocus += new System.EventHandler(this.textBox_GotFocus);
            this.textBox.LostFocus += new System.EventHandler(this.textBox_LostFocus);
            this.textBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // labelMask
            // 
            this.labelMask.BackColor = System.Drawing.Color.White;
            this.labelMask.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelMask.Location = new System.Drawing.Point(15, 4);
            this.labelMask.Name = "labelMask";
            this.labelMask.Size = new System.Drawing.Size(18, 20);
            // 
            // CMXPasswordBox
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.labelMask);
            this.Controls.Add(this.textBox);
            this.Name = "CMXPasswordBox";
            this.Size = new System.Drawing.Size(160, 28);
            this.GotFocus += new System.EventHandler(this.CMXTextBox_GotFocus);
            this.Resize += new System.EventHandler(this.CMXTextBox_Resize);
            this.LostFocus += new System.EventHandler(this.CMXTextBox_LostFocus);
            this.ResumeLayout(false);

        }

        #endregion

        protected CargoMatrix.UI.CMXAlphaNumTextBox textBox;
        private System.Windows.Forms.Label labelMask;

        
    }
}
