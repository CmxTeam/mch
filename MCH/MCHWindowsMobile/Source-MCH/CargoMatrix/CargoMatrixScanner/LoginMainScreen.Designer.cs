﻿namespace CargoMatrixScanner
{
    partial class LoginMainScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelTitleDescription = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button22 = new CargoMatrix.UI.CMXTextButton();
            this.labelScanCard = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button21 = new CargoMatrix.UI.CMXTextButton();
            this.labelVersion = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox21);
            this.panel3.Controls.Add(this.labelLogin);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 56);
            // 
            // pictureBox21
            // 
            this.pictureBox21.Location = new System.Drawing.Point(9, 5);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(48, 48);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // labelLogin
            // 
            this.labelLogin.Font = new System.Drawing.Font("Tahoma", 22F, System.Drawing.FontStyle.Regular);
            this.labelLogin.Location = new System.Drawing.Point(78, 9);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(159, 42);
            this.labelLogin.Text = "<labelLogin>";
            // 
            // labelTitleDescription
            // 
            this.labelTitleDescription.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelTitleDescription.Location = new System.Drawing.Point(0, 59);
            this.labelTitleDescription.Name = "labelTitleDescription";
            this.labelTitleDescription.Size = new System.Drawing.Size(240, 24);
            this.labelTitleDescription.Text = "<labelTitleDescription>";
            this.labelTitleDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button22);
            this.panel1.Controls.Add(this.labelScanCard);
            this.panel1.Location = new System.Drawing.Point(0, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 54);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.White;
            this.button22.Enabled = false;
            this.button22.Location = new System.Drawing.Point(13, 5);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(213, 44);
            // 
            // labelScanCard
            // 
            this.labelScanCard.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelScanCard.Location = new System.Drawing.Point(71, 21);
            this.labelScanCard.Name = "labelScanCard";
            this.labelScanCard.Size = new System.Drawing.Size(151, 20);
            this.labelScanCard.Text = "<scancard>";
            this.labelScanCard.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button21);
            this.panel2.Location = new System.Drawing.Point(0, 138);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 60);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.White;
            this.button21.Location = new System.Drawing.Point(13, 7);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(213, 44);
            this.button21.Click += new System.EventHandler(this.button21_Click);
            this.button21.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button21_MouseDown);
            // 
            // labelVersion
            // 
            this.labelVersion.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.labelVersion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.labelVersion.Location = new System.Drawing.Point(13, 228);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(213, 18);
            this.labelVersion.Text = CargoMatrixScannerResource.Text_Version;
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LoginMainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BarcodeEnabled = true;
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelTitleDescription);
            this.Name = "LoginMainScreen";
            this.Size = new System.Drawing.Size(240, 292);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(this.LoginMainScreen_BarcodeReadNotify);
            this.EnabledChanged += new System.EventHandler(this.LoginMainScreen_EnabledChanged);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelTitleDescription;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Label labelScanCard;
        //private OpenNETCF.Windows.Forms.Button2 button21;
        //private OpenNETCF.Windows.Forms.Button2 button22;
        CargoMatrix.UI.CMXTextButton button21;
        CargoMatrix.UI.CMXTextButton button22;
        private System.Windows.Forms.Label labelVersion;
    }
}
