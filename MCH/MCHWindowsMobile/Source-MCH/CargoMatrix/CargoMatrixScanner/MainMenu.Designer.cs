﻿namespace CargoMatrixScanner
{
    partial class MainMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Name = "MainMenu";
            this.Size = new System.Drawing.Size(240, 208);
            this.LoadOptionsMenu += new System.EventHandler(this.MainMenu_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(this.MainMenu_MenuItemClicked);
            //this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(this.MainMenu_ListItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MainMenu_ListItemClicked);
            this.EnabledChanged += new System.EventHandler(this.MainMenu_EnabledChanged);
            this.ResumeLayout(false);

        }


        #endregion

    }
}
