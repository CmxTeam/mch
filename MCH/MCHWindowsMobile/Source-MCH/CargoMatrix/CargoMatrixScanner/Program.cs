﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CargoMatrixScanner
{
    static class Program
    {
        //public static string ConfigFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\CMXScannerConfig.xml";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            if (System.Environment.OSVersion.Version.Major > 4)
            {
                ShowTaskBar(false);
            }
            try
            {
                OpenNETCF.Windows.Forms.Application2.Run(new MainForm());
                //System.Windows.Forms.Application.Run(new MainForm());
                try
                {
                    new Foo();
                }
                catch { } 
              

            }
            catch (Exception ex)
            {
              
                    System.Windows.Forms.MessageBox.Show(ex.Message);

                    //System.Windows.Forms.MessageBox.Show(ex.InnerException.StackTrace);

                    //System.Windows.Forms.MessageBox.Show(ex.InnerException.Message);
                
            }
            finally
            {
                if (System.Environment.OSVersion.Version.Major > 4)
                {
                    ShowTaskBar(true);
                }
            }
            if (System.Environment.OSVersion.Version.Major > 4)
            {
                ShowTaskBar(true);
            }

            //KillApp();
            Cursor.Current = Cursors.Default;
            return;
        }

        static void KillApp()
        {




            CargoMatrixScanner.Process[] processes = CargoMatrixScanner.Process.GetProcesses();

            if (processes != null)
            {
                for (int i = 0; i < processes.Length; i++)
                {

                    if (processes[i].ProcessName.ToLower() == "mobilecargohandler.exe")
                    {
                        processes[i].Kill();

                    }
                    //MessageBox.Show(processes[i].ProcessName);
                }
            }

        }

        [DllImport("CMXNativeUtilities.DLL")]
        private static extern void ShowTaskBar(bool bShow);
    }

    class Foo
    {
        //static Foo foo = new Foo(); // you are not allowed using this approach
        //static readonly Foo foo = new Foo(); // you are not allowed using this approach
        Foo foo = new Foo();
    }



    
}