﻿namespace CargoMatrix.OnHand
{
    partial class OnHandListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxItemStatus = new OpenNETCF.Windows.Forms.PictureBox2();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.labelLine1 = new System.Windows.Forms.Label();
            this.labelLine2 = new System.Windows.Forms.Label();
            this.labelLine3 = new System.Windows.Forms.Label();
            this.labelLine4 = new System.Windows.Forms.Label();
            //this.labelLine5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pictureBoxItemStatus
            // 
            this.pictureBoxItemStatus.Location = new System.Drawing.Point(4, 4);
            this.pictureBoxItemStatus.Name = "pictureBoxItemStatus";
            this.pictureBoxItemStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxItemStatus.Size = new System.Drawing.Size(24, 24);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(4, 54);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            // 
            // labelLine1
            // 
            this.labelLine1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelLine1.Location = new System.Drawing.Point(35, 4);
            this.labelLine1.Name = "labelLine1";
            this.labelLine1.Size = new System.Drawing.Size(202, 13);
            // 
            // labelLine2
            // 
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.Location = new System.Drawing.Point(35, 17);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(202, 13);
            // 
            // labelLine3
            // 
            this.labelLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine3.Location = new System.Drawing.Point(35, 30);
            this.labelLine3.Name = "labelLine3";
            this.labelLine3.Size = new System.Drawing.Size(202, 13);
            // 
            // labelLine4
            // 
            this.labelLine4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine4.Location = new System.Drawing.Point(35, 43);
            this.labelLine4.Name = "labelLine4";
            this.labelLine4.Size = new System.Drawing.Size(150, 13);
            // 
            // labelLine5
            // 
            //this.labelLine5.Location = new System.Drawing.Point(35, 56);
            //this.labelLine5.Name = "labelLine5";
            //this.labelLine5.Size = new System.Drawing.Size(181, 13);
            // 
            // OnHandListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            //this.Controls.Add(this.labelLine5);
            this.Controls.Add(this.labelLine4);
            this.Controls.Add(this.labelLine3);
            this.Controls.Add(this.labelLine2);
            this.Controls.Add(this.labelLine1);
            this.Controls.Add(this.panelIndicators);
            this.Controls.Add(this.pictureBoxItemStatus);
            this.Name = "OnHandListItem";
            this.Size = new System.Drawing.Size(240, 73);
            this.ResumeLayout(false);

        }

        #endregion

        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxItemStatus;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private System.Windows.Forms.Label labelLine1;
        private System.Windows.Forms.Label labelLine2;
        private System.Windows.Forms.Label labelLine3;
        private System.Windows.Forms.Label labelLine4;
        //private System.Windows.Forms.Label labelLine5;
    }
}
