﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CMXExtensions;
using CargoMatrix.Communication.WSOnHand;
using CargoMatrix.Communication.OnHand;
namespace CargoMatrix.OnHand
{
    public partial class OnhandDetails : UserControl
    {
        private PackageItem package;

        public event EventHandler ButtonPrintClick;
        public event EventHandler ButtonListClick;
        public event EventHandler ButtonDetailsClick;
        public event EventHandler ButtonDamageClick;
        public OnhandDetails()
        {
            InitializeComponent();
            this.Clear();
        }
        private void InitializeImages()
        {
            this.buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonList.Image = Resources.Skin.btn_listView;
            this.buttonList.PressedImage = Resources.Skin.btn_listView_over;
            this.buttonDamage.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonPrint.Image = Resources.Skin.printerButton;
            this.buttonPrint.PressedImage = Resources.Skin.printerButton_over;
            this.itemPicture.Image = Resources.Skin.PieceMode;

        }

        internal void DisplayOnhand(PackageItem pkg)
        {
            if (pkg != null && pkg.PackageID != 0)
            {
                this.package = pkg;
                title.Text = pkg.Customer;
                this.labelPiece.Text = pkg.PieceNumber.ToString();
                // Set Label Color;
                //if (Hawb.ScannedPieces > 0 && Hawb.ScannedPieces < Hawb.TotalPieces)
                //    labelPieces.ForeColor = Color.Red;
                //else labelPieces.ForeColor = Color.Black;

                labelWeight.Text = pkg.Weight + " " + pkg.WeightUOM;
                labelDims.Text = string.Format("{0} x {1} x {2} {3}", pkg.Width, pkg.Length, pkg.Height, pkg.DimUOM);
                labelPkg.Text = pkg.PackageType;
                labelVendor.Text = pkg.Vendor;
                labelDropAt.Text = pkg.DropAt;
                //panelIndicators.Flags = Hawb.Flags;
                //panelIndicators.PopupHeader = title.Text;
                //switch (pkg.status)
                //{
                //    case CargoMatrix.Communication.DTO.HouseBillStatuses.Open:
                //    case CargoMatrix.Communication.DTO.HouseBillStatuses.Pending:
                //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                //        break;
                //    case CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress:
                //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                //        break;
                //    case CargoMatrix.Communication.DTO.HouseBillStatuses.Completed:
                //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                //        break;
                //}
                ShowButtons(true);

            }
            else
            {
                this.package = null;
            }
        }

        private void ShowButtons(bool show)
        {
            buttonDamage.Visible = buttonDetails.Visible = buttonPrint.Visible = itemPicture.Visible = show;
        }



        void buttonPrint_Click(object sender, System.EventArgs e)
        {
            if (ButtonPrintClick != null)
                ButtonPrintClick(this, EventArgs.Empty);
        }

        void buttonDamage_Click(object sender, System.EventArgs e)
        {
            if (ButtonDamageClick != null)
                ButtonDamageClick(this, EventArgs.Empty);
        }

        void buttonList_Click(object sender, System.EventArgs e)
        {
            if (ButtonListClick != null)
                ButtonListClick(this, EventArgs.Empty);
        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonDetailsClick != null)
                ButtonDetailsClick(this, EventArgs.Empty);
        }

        internal void Clear()
        {
            title.Text = string.Empty;
            this.labelDims.Text = this.labelDropAt.Text = this.labelPiece.Text =
                this.labelPkg.Text = this.labelVendor.Text = this.labelWeight.Text = string.Empty;
            this.itemPicture.Image = null;
            ShowButtons(false);
        }
    }
}
