﻿using System;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using System.Windows.Forms;
using CargoMatrix.OnHand;
using CustomListItems;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    partial class OnHandTasksList : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        protected string filter = "Not Completed";
        protected string sort = "PO Number";
        internal const string FORM_NAME = "OnHandTaskList";
        private CustomListItems.OptionsListITem filterOption;

        public OnHandTasksList()
        {
            InitializeComponents();
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(OnHandTasksList_ListItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeScan);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(OnHandTasksList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(OnHandTasksList_MenuItemClicked);
        }

        void OnHandTasksList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        FilterSort();
                        break;
                }
        }

        private void FilterSort()
        {
            FilterSortPopup filterPopup = new FilterSortPopup(filter, sort);
            if (DialogResult.OK == filterPopup.ShowDialog())
            {
                filter = filterPopup.Filter;
                sort = filterPopup.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter + " / " + sort;
                LoadControl();
            }
        }

        void OnHandTasksList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };

            this.AddOptionsListItem(filterOption);
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO) { Title = "Manual Lookup" });
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void BarcodeScan(string barcodeData)
        {
            var scan = Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scan.BarcodeType == CMXBarcode.BarcodeTypes.OnHand)
            {
                var onhandDetails = Communication.OnHand.OnHand.Instance.GetOnHandDetails(scan.OnHandNumber);
                if (onhandDetails == null)
                {
                    SharedMethods.ShowFailMessage("OnHand was not found");
                    BarcodeEnabled = true;
                }
                else
                {
                    SharedMethods.CurrentPickup = onhandDetails;
                }
            }
            else
            {
                SharedMethods.ShowFailMessage("Invalid barcode has been scanned");
                BarcodeEnabled = true;
            }
        }


        void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
        }

        void OnHandTasksList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new DropToLocation());
            //return;

            if (listItem is StaticListItem)
            {
                SearchOnhandPopup onahndSearch = new SearchOnhandPopup();
                if (DialogResult.OK == onahndSearch.ShowDialog())
                {
                    SearchItem search = new SearchItem(onahndSearch.PickupRefNo, onahndSearch.Customer, onahndSearch.Vendor, onahndSearch.OrderReference);
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new OnHandSearchTasks(search));
                }
                else
                    sender.Reset();
            }
            else
            {
                SharedMethods.CurrentPickup = (listItem as OnHandListItem).ItemData;
                if (SharedMethods.CurrentPickup.Status != ShipmentStatus.Complete)
                {
                    if ((SharedMethods.CurrentPickup as OnHandTaskItem).ShipmentStatus == OnHandStatus.OnHandInProgress)
                    {
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new DropToLocation());
                    }
                    else
                    {
                        CargoMatrix.Communication.OnHand.OnHand.Instance.StartPickupRecovery(SharedMethods.CurrentPickup.ShipmentID);
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new OnHandDetails());
                    }
                }
                else
                {
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new PiecesList());
                }

            }
        }

        public override void LoadControl()
        {
            this.labelFilterSort.Text = string.Format("{0}/{1}", filter, sort);
            searchBox.Text = string.Empty;
            this.smoothListBoxMainList.RemoveAll();
            this.smoothListBoxMainList.AddItem(new StaticListItem("Receive New", Resources.Icons.Notepad_Add));
            foreach (var item in Communication.OnHand.OnHand.Instance.GetOnhandTasks(filter, sort))
            {
                smoothListBoxMainList.AddItem2(new OnHandListItem(item));
            }
            LayoutItems();
            this.TitleText = string.Format("OnHand Tasks ({0})", smoothListBoxMainList.ItemsCount - 1);
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }
    }
}
