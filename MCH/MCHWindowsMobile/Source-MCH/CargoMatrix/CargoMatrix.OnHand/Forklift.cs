﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.OnHand
{
    public class Forklift
    {

        private static Forklift instance;
        private bool isLoaded;

        private CargoMatrix.Communication.ScannerUtilityWS.TaskSettings settings;
        private int itemsCount;
        
        private Forklift()
        {
            this.isLoaded = false;
        }

        private void LoadAppSettings()
        {
            var tempSettings = Communication.OnHand.OnHand.Instance.GetTaskSettings();
            if (tempSettings.Transaction.TransactionStatus1 == true )
            {
                this.settings = tempSettings;
                this.isLoaded = true;
            }
            else
            {
                this.isLoaded = false;
                CargoMatrix.UI.CMXMessageBox.Show("Error has occured while downloading task settings. Refresh or restart the application", "Unable to connect", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }
        public int TaskID
        {
            get;
            set;
        }


        public event EventHandler ItemCountChanged;
        public static bool Isloaded
        {
            get { return Instance.isLoaded; }
        }
        public int ID { get { return settings.ForkliftId; } }
        public int ItemsCount
        {
            internal set
            {
                itemsCount = value;
                var handlers = this.ItemCountChanged;
                if (handlers != null)
                    handlers(this, EventArgs.Empty);
            }
            get { return itemsCount; }

        }
        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                    instance = new Forklift();


                if (instance.isLoaded == false)
                    instance.LoadAppSettings();

                return instance;
            }
        }


    }
}
