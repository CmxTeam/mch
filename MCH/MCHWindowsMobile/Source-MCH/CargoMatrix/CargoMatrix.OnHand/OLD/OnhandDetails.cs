﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.OnHand
{
    public class OnhandDetails
    {
        public string onHandNo;
        public CargoMatrix.Communication.CargoMeasurements.DimensioningMeasurementDetails[] measurements;
        public string carrier;
        public string customerAccnt;
        public string destination;
        public int noOfPcs;
        public string[] attributes;
    }
}
