﻿using CargoMatrix.UI;
namespace CargoMatrix.OnHand
{
    partial class SearchOnhandPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.labelPickupRef = new System.Windows.Forms.Label();
            this.textBoxPickupRef = new CMXTextBox();
            this.labelCustomer = new System.Windows.Forms.Label();
            this.textBoxCustomer = new CMXTextBox();
            this.labelVendor = new System.Windows.Forms.Label();
            this.textBoxVendor = new CMXTextBox();
            this.labelOrdRef = new System.Windows.Forms.Label();
            this.textBoxOrdRef = new CMXTextBox();
            this.buttonCustomer = new CMXPictureButton();
            this.buttonVendor = new CMXPictureButton();
            this.SuspendLayout();
            this.panelContent.SuspendLayout();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            // 
            // labelPickupRef
            // 
            this.labelPickupRef.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelPickupRef.Location = new System.Drawing.Point(4, 26);
            this.labelPickupRef.Name = "labelPickupRef";
            this.labelPickupRef.Size = new System.Drawing.Size(226, 13);
            this.labelPickupRef.Text = "Pickup Ref#";
            // 
            // textBoxPickupRef
            // 
            this.textBoxPickupRef.Location = new System.Drawing.Point(4, 44);
            this.textBoxPickupRef.Name = "textBoxPickupRef";
            this.textBoxPickupRef.Size = new System.Drawing.Size(226, 21);
            this.textBoxPickupRef.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxPickupRef.TabIndex = 1;
            // 
            // labelCustomer
            // 
            this.labelCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelCustomer.Location = new System.Drawing.Point(4, 72);
            this.labelCustomer.Name = "labelCustomer";
            this.labelCustomer.Size = new System.Drawing.Size(186, 13);
            this.labelCustomer.Text = "Customer";
            // 
            // textBoxCustomer
            // 
            this.textBoxCustomer.Location = new System.Drawing.Point(4, 90);
            this.textBoxCustomer.Name = "textBoxCustomer";
            this.textBoxCustomer.Size = new System.Drawing.Size(186, 21);
            this.textBoxCustomer.TabIndex = 2;
            this.textBoxCustomer.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // buttonCustomer
            // 
            this.buttonCustomer.BackColor = System.Drawing.Color.White;
            this.buttonCustomer.Location = new System.Drawing.Point(196, 80);
            this.buttonCustomer.Name = "buttonPrint";
            this.buttonCustomer.Size = new System.Drawing.Size(32, 32);
            this.buttonCustomer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonCustomer.TransparentColor = System.Drawing.Color.White;
            this.buttonCustomer.Click += new System.EventHandler(buttonCustomer_Click);
            this.buttonCustomer.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonCustomer.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            // 
            // labelVendor
            // 
            this.labelVendor.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelVendor.Location = new System.Drawing.Point(4, 120);
            this.labelVendor.Name = "labelVendor";
            this.labelVendor.Size = new System.Drawing.Size(186, 13);
            this.labelVendor.Text = "Vendor";
            // 
            // textBoxVendor
            // 
            this.textBoxVendor.Location = new System.Drawing.Point(4, 138);
            this.textBoxVendor.Name = "textBoxVendor";
            this.textBoxVendor.Size = new System.Drawing.Size(186, 21);
            this.textBoxVendor.TabIndex = 3;
            this.textBoxVendor.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxVendor.TextChanged += new System.EventHandler(textBoxVendor_TextChanged);
            // 
            // buttonVendor
            // 
            this.buttonVendor.BackColor = System.Drawing.Color.White;
            this.buttonVendor.Location = new System.Drawing.Point(196, 128);
            this.buttonVendor.Name = "buttonPrint";
            this.buttonVendor.Size = new System.Drawing.Size(32, 32);
            this.buttonVendor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonVendor.TransparentColor = System.Drawing.Color.White;
            this.buttonVendor.Click += new System.EventHandler(buttonVendor_Click);
            this.buttonVendor.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonVendor.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonVendor.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonVendor.Enabled = false;
            // 
            // labelOrdRef
            // 
            this.labelOrdRef.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelOrdRef.Location = new System.Drawing.Point(4, 170);
            this.labelOrdRef.Name = "labelOrdRef";
            this.labelOrdRef.Size = new System.Drawing.Size(226, 13);
            this.labelOrdRef.Text = "PO Reference";
            // 
            // textBoxOrdRef
            // 
            this.textBoxOrdRef.Location = new System.Drawing.Point(4, 188);
            this.textBoxOrdRef.Name = "textBoxOrdRef";
            this.textBoxOrdRef.Size = new System.Drawing.Size(226, 21);
            this.textBoxOrdRef.TabIndex = 4;
            this.textBoxOrdRef.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // Form1
            // 
            this.panelContent.Size = new System.Drawing.Size(234, 268);
            this.panelContent.Location = new System.Drawing.Point(3, 20);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.panelContent.Controls.Add(this.textBoxOrdRef);
            this.panelContent.Controls.Add(this.labelOrdRef);
            this.panelContent.Controls.Add(this.textBoxVendor);
            this.panelContent.Controls.Add(this.labelVendor);
            this.panelContent.Controls.Add(this.textBoxCustomer);
            this.panelContent.Controls.Add(this.labelCustomer);
            this.panelContent.Controls.Add(this.textBoxPickupRef);
            this.panelContent.Controls.Add(this.labelPickupRef);
            this.panelContent.Controls.Add(this.buttonCustomer);
            this.panelContent.Controls.Add(this.buttonVendor);
            this.panelContent.ResumeLayout(false);
            this.ResumeLayout(false);

        }




        #endregion

        private System.Windows.Forms.Label labelPickupRef;
        private System.Windows.Forms.Label labelCustomer;
        private System.Windows.Forms.Label labelVendor;
        private System.Windows.Forms.Label labelOrdRef;
        private CMXTextBox textBoxPickupRef;
        private CMXTextBox textBoxCustomer;
        private CMXTextBox textBoxVendor;
        private CMXTextBox textBoxOrdRef;
        private CargoMatrix.UI.CMXPictureButton buttonCustomer;
        private CargoMatrix.UI.CMXPictureButton buttonVendor;
    }
}
