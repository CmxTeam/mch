﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    public partial class FilterSortPopup : MessageListBox
    {
        private CustomListItems.ChoiceListItem filterItem;
        private CustomListItems.ChoiceListItem sortItem;
        private MessageListBox optionalChoiceList;
        private string filter;
        private string sort;

        public string Filter
        {
            get { return filter; }
            //set { filter = value; filterItem.LabelLine2.Text = value; }
        }

        public string Sort
        {
            get { return sort; }
            //set { sort = value; sortItem.LabelLine2.Text = value; }
        }


        public FilterSortPopup(string filter, string sort)
        {
            InitializeComponent();
            this.filter = filter;
            this.sort = sort;
            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FilterPopup_ListItemClicked);
            TopPanel = false;
            filterItem = new CustomListItems.ChoiceListItem("Filter", CargoMatrix.Resources.Skin.Filter, !string.IsNullOrEmpty(filter) ? filter : "ALL");
            AddItem(filterItem);

            sortItem = new CustomListItems.ChoiceListItem("Sort", CargoMatrix.Resources.Skin.Sort, !string.IsNullOrEmpty(sort) ? sort : "All");
            AddItem(sortItem);
        }


        void FilterPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            // first time initialization
            if (null == this.optionalChoiceList)
            {
                optionalChoiceList = new MessageListBox();
                optionalChoiceList.MultiSelectListEnabled = false;
                optionalChoiceList.OneTouchSelection = true;
                optionalChoiceList.TopPanel = false;
            }
            // if filter
            if (listItem == filterItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = "Filter By";



                optionalChoiceList.AddItems(GetFilterItems());

                // select current selected status
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == filter)
                    {
                        optionalChoiceList.SelectControl(item);
                        break;
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    filter = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    filterItem.LabelLine2.Text = filter;
                }
            }
            //if sort 
            if (listItem == sortItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = "Sort By";
                optionalChoiceList.AddItems(GetSorts());

                // select current selected direction
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == sort)
                    {
                        optionalChoiceList.SelectControl(item);
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    sort = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    sortItem.LabelLine2.Text = sort;
                }
            }
            sender.Reset();
        }

        private Control[] GetFilterItems()
        {
            return new Control[] 
                {
                    new StandardListItem("Not Started", null),
                    new StandardListItem("Not Completed", null),
                    new StandardListItem("In Progress", null),
                    new StandardListItem("Completed", null),
                    new StandardListItem("Assigned", null),
                    new StandardListItem("Not Assigned", null),
                };

        }

        private Control[] GetSorts()
        {
            return new Control[]
            {
                    new StandardListItem("Pickup Reference", null),
                    new StandardListItem("PO Number", null),
                    new StandardListItem("Vendor Name", null),
                    new StandardListItem("Customer Name", null),
                    new StandardListItem("Origin", null),
                    new StandardListItem("Destination", null),
                };
        }
    }
}
