﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;
using System.Net;
using System.IO;

namespace CargoMatrix.Communication.Json
{
    public class RestServiceProcessor
    {

        Uri baseUri;

        private RestServiceProcessor() { }

        public static RestServiceProcessor GetService()
        {
            RestServiceProcessor instance = new RestServiceProcessor();
            instance.baseUri = new Uri(Settings.Instance.RestURLPath);
            return instance;
        }

        private string GetFullUrl(string functionName)
        {
            Uri uri = new Uri(baseUri, functionName);
            return uri.ToString();
        }

        public CommunicationTransaction GetPostMethod(string url, string json)
        {

            url = GetFullUrl(url);

            CommunicationTransaction t = new CommunicationTransaction();
            t.Url = url;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "POST";
            byte[] postBytes = Encoding.ASCII.GetBytes(json);
            request.ContentLength = postBytes.Length;
            Stream postStream = request.GetRequestStream();
            postStream.Write(postBytes, 0, postBytes.Length);
            postStream.Close();

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Server returned status code: {0}", response.StatusCode));
                    }
                    else
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            var content = reader.ReadToEnd();
                            if (string.IsNullOrEmpty(content) || string.IsNullOrEmpty(content.Trim()))
                            {
                                throw new Exception("Response contained empty body.");
                            }
                            else
                            {
                                t.Status = true;
                                t.Content = content;
                                t.Error = string.Empty;
                                return t;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                t.Status = false;
                t.Content = string.Empty;
                t.Error = string.Format("Error fetching data. {0}", ex.Message);
                return t;
            }
        }

        public CommunicationTransaction GetMethod(string url, Dictionary<string, string> parameters)
        {

            url = GetFullUrl(url);

            CommunicationTransaction t = new CommunicationTransaction();
            t.Url = url;

            if (parameters != null)
            {
                StringBuilder paramList = new StringBuilder();
                foreach (KeyValuePair<string, string> kvp in parameters)
                {

                    if (paramList.ToString().Trim() == string.Empty)
                    {
                        paramList.Append(string.Format("{0}={1}", kvp.Key, kvp.Value));
                    }
                    else
                    {
                        paramList.Append(string.Format("&{0}={1}", kvp.Key, kvp.Value));
                    }

                }

                if (paramList.ToString().Trim() != string.Empty)
                {
                    url += string.Format("?{0}", paramList.ToString());
                }
            }

            var request = HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";


            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Server returned status code: {0}", response.StatusCode));
                    }
                    else
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            var content = reader.ReadToEnd();
                            if (string.IsNullOrEmpty(content) || string.IsNullOrEmpty(content.Trim()))
                            {
                                throw new Exception("Response contained empty body.");
                            }
                            else
                            {
                                t.Status = true;
                                t.Content = content;
                                t.Error = string.Empty;
                                return t;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                t.Status = false;
                t.Content = string.Empty;
                t.Error = string.Format("Error fetching data. {0}", ex.Message);
                return t;
            }


        }
    }
}
