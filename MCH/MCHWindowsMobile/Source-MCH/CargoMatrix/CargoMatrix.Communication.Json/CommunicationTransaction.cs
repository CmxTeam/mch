﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Json
{
    public class CommunicationTransaction
    {
        public bool Status { get; set; }
        public string Content { get; set; }
        public string Error { get; set; }
        public string Url { get; set; }
    }
}
