﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication;
using CargoMatrix.UI;
using SmoothListbox.ListItems;

namespace CargoMatrix.Inventory
{
    public partial class HawbInventory : CargoUtilities.SmoothListBoxOptions
    {
        private HawbDetailedItem hawbDetails;
        private HouseBillItem currentHawb;
        private bool manualScan;
        private List<HouseBillItem> manualScannedHawbs = new List<HouseBillItem>();

        public HawbInventory()
        {
            InitializeComponent();
            this.LoadOptionsMenu += new EventHandler(HawbInventory_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(HawbInventory_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeScanned);
            topLabel.Text = InventoryResources.Text_ScanLoc;
            topLabel.Top = 22;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            smoothListBoxMainList.Scrollable = false;
            this.BackButtonHold = false;
            Cursor.Current = Cursors.Default;
        }

        private void InitializeComponentHelper()
        {
            panelHeader2.Height = 231;
            this.TitleText = InventoryResources.Text_HawbInventoryTitle;
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            buttonScannedList.Value = Forklift.Instance.ItemsCount;
            if (!string.IsNullOrEmpty(Forklift.Instance.ScannedLocation))
            {
                labelLocation.Text = Forklift.Instance.ScannedLocation;
                topLabel.Text = InventoryResources.Text_ScanHawb;
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            // ask for finazlize if it was preassigned inventory
            if (!Forklift.Instance.EnableChangeLocation
            && DialogResult.OK != CMXMessageBox.Show(InventoryResources.Text_FinalizeConfirm, "Finalize Task", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                base.pictureBoxBack_Click(sender, e);
                return;
            }

            if (Forklift.Instance.ItemsCount != 0)
            {
                /// forklift is not empty
                CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_EmptyForklift, "Confirm Location", CMXMessageBoxIcon.Hand);//, MessageBoxButtons.OK, DialogResult.OK);
            }
            else
            {
                TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.FinalizeInventory(Forklift.Instance.TaskID, Forklift.Instance.ForkliftID);
                if (status.TransactionStatus1)
                {
                    if (status.TransactionError != string.Empty)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Finalized", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                    }
                    base.pictureBoxBack_Click(sender, e);
                }
                else
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError + InventoryResources.Text_ContinueOption, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                    {
                        // reset scanned list
                        base.pictureBoxBack_Click(sender, e);
                    }
                    else
                        new DiscrepancyReport().ShowDialog();
                }
            }
        }


        private void BarcodeScanned(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    /// no location has been scanned
                    if (string.IsNullOrEmpty(Forklift.Instance.ScannedLocation))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_ScanLocation, "Scan Location", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    Scan2(scanItem.HouseBillNumber, scanItem.PieceNumber, false);
                    // ANI - add the hawb info to queue to have the label reprinted later
                    if (manualScan)
                    {
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_ReprintLabelConfirm, "Print Label", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            manualScannedHawbs.Add(currentHawb);
                        }
                    }
                    break;


                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    if (!Forklift.Instance.EnableChangeLocation && !scanItem.Location.Equals(Forklift.Instance.ScannedLocation))
                    {
                        CMXMessageBox.Show(InventoryResources.Text_LocChangeDenied, "Not Allowed", CMXMessageBoxIcon.Hand);
                        return;
                    }



                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    if (string.IsNullOrEmpty(Forklift.Instance.ScannedLocation) || buttonScannedList.Value == 0)
                    {
                        if (!CargoMatrix.Communication.ScannerUtility.Instance.IsLocationValid(scanItem.Location))
                            goto default;
                        Forklift.Instance.ScannedLocation = scanItem.Location;
                        labelLocation.Text = scanItem.Location;
                        topLabel.Text = InventoryResources.Text_ScanHawb;
                        break;
                    }
                    if (!string.Equals(scanItem.Location, Forklift.Instance.ScannedLocation))
                    {
                        string msg = string.Format(InventoryResources.Text_ChangeLocation, Forklift.Instance.ScannedLocation, scanItem.Location);
                        if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Location", CargoMatrix.UI.CMXMessageBoxIcon.Hand, MessageBoxButtons.YesNo, DialogResult.Yes))
                            break;
                    }


                    string message = string.Format(InventoryResources.Text_DropConfirm, Forklift.Instance.ItemsCount, scanItem.Location);
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(message, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, DialogResult.OK))
                    {
                        TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.DropAllPiecesIntoLocation(Forklift.Instance.TaskID, scanItem.Location, Forklift.Instance.ForkliftID);
                        if (!status.TransactionStatus1)
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        else
                            buttonScannedList.Value = Forklift.Instance.ItemsCount = status.TransactionRecords;

                        if (hawbDetails != null)
                            hawbDetails.Visible = false;

                        if (Forklift.Instance.EnableChangeLocation)
                        {
                            Forklift.Instance.ScannedLocation = string.Empty;
                            topLabel.Text = InventoryResources.Text_ScanLoc;
                            labelLocation.Text = string.Empty;
                        }
                    }
                    break;
                default:
                    CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    break;
            }

            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        private void Scan2(string hawbNo, int pieceNo, bool force)
        {
            currentHawb = CargoMatrix.Communication.Inventory.Instance.ScanHousebillIntoForkliftLocation(hawbNo, pieceNo, Forklift.Instance.TaskID, Forklift.Instance.ForkliftID, ScanTypes.Automatic, Forklift.Instance.ScannedLocation, force);
            if (currentHawb.Transaction.TransactionStatus1 == true) // Piece Mode
            {
                DisplayHawb(currentHawb);
            }
            else
            {
                if (currentHawb.Transaction.NeedOverride)
                {
                    ReCheckDischargedHawb(currentHawb, pieceNo);
                    return;
                }
                /// create new housebill
                if (currentHawb.HousebillId == 0)
                {
                    CreateHousebill(hawbNo, pieceNo, force);
                    return;
                }

                /// if last scan is different than workling location
                if (Forklift.Instance.LocationDifferentfromCurrentCheck && !string.IsNullOrEmpty(currentHawb.LastLocation) && !string.Equals(currentHawb.LastLocation, Forklift.Instance.ScannedLocation, StringComparison.OrdinalIgnoreCase))
                {
                    string msg = string.Format(InventoryResources.Text_LocationOverrideConfirm, currentHawb.Reference(), currentHawb.LastLocation);
                    if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                        return;
                }

                if (currentHawb.ScanMode == ScanModes.Piece && force != true)
                {
                    Scan2(hawbNo, pieceNo, true);
                    return;
                }

                if (currentHawb.ScanMode == ScanModes.Count)
                {
                    ScanHawbInCountMode(currentHawb);
                }
                else if (!string.IsNullOrEmpty(currentHawb.Transaction.TransactionError))
                {
                    CargoMatrix.UI.CMXMessageBox.Show(currentHawb.Transaction.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    // Withdraw manual scan flag to not get the reprint label message popup
                    manualScan = false;
                }
            }
        }

        private void ReCheckDischargedHawb(HouseBillItem hawb, int pieceNo)
        {
            /// show yes/no confirmation meassage to retrun discharged shipment
            if (DialogResult.OK == UI.CMXMessageBox.Show(hawb.Transaction.TransactionError, "Return Shipment", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                var reason = DisplayReturnDischargedShipmentReasons(hawb.HousebillNumber);
                if (reason != null)
                {
                    var status = Communication.Inventory.Instance.ReturnDischargedShipment(hawb.HousebillId, reason.ReasonId);
                    if (status.TransactionStatus1)
                        Scan2(hawb.HousebillNumber, pieceNo, false);
                    else
                    {
                        UI.CMXMessageBox.Show(status.TransactionError, "Error", CMXMessageBoxIcon.Hand);
                        // Withdraw manual scan flag to not get the reprint label message popup
                        manualScan = false;
                    }
                }
            }
        }

        private ReturnReasonItem DisplayReturnDischargedShipmentReasons(string title)
        {
            CargoMatrix.Utilities.MessageListBox reasonsPopup = new CargoMatrix.Utilities.MessageListBox();
            reasonsPopup.OneTouchSelection = true;
            reasonsPopup.MultiSelectListEnabled = false;
            reasonsPopup.HeaderText = "Select return Reason";
            reasonsPopup.HeaderText2 = title;
            foreach (var item in Communication.Inventory.Instance.GetReturnReasons())
            {
                reasonsPopup.smoothListBoxBase1.AddItem2(new StandardListItem<ReturnReasonItem>(item.Reason, null, item));
            }
            reasonsPopup.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == reasonsPopup.ShowDialog())
            {
                return (reasonsPopup.SelectedItems[0] as StandardListItem<ReturnReasonItem>).ItemData;
            }
            else
                return null;
        }

        private void ScanHawbInCountMode(HouseBillItem hawb)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.LabelReference = hawb.Reference();
            ShowHawbAlerts(hawb);

            if (hawb.TotalPieces - hawb.ScannedPieces == 0)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_InventroyAlreadyDone, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    CntMsg.PieceCount = hawb.TotalPieces;
                else
                    return;
            }
            else
                CntMsg.PieceCount = hawb.TotalPieces - hawb.ScannedPieces;

            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                var countHawb = CargoMatrix.Communication.Inventory.Instance.ScanPiecesIntoForkliftLocation(hawb.HousebillId,
                    CntMsg.PieceCount, Forklift.Instance.TaskID,
                    Forklift.Instance.ForkliftID, ScanTypes.Automatic, Forklift.Instance.ScannedLocation, true);
                if (countHawb.Transaction.TransactionStatus1)
                    DisplayHawb(countHawb);
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show(countHawb.Transaction.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    // Withdraw manual scan flag to not get the reprint label message popup
                    manualScan = false;
                }
            }
        }

        private static void ShowHawbAlerts(HouseBillItem hawb)
        {
            foreach (var alert in hawb.ShipmentAlerts)
            {
                CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage + Environment.NewLine + alert.AlertSetBy + Environment.NewLine + alert.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + hawb.HousebillNumber, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }

        private void CreateHousebill(string hawbNo, int pieceNo, bool force)
        {
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_NewHousebill, "Create Housebill", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                int pieces;
                string origin, dest;
                if (DialogResult.OK == CustomUtilities.OriginDestinationMessageBox.Show(hawbNo, out pieces, out origin, out dest))
                {
                    // create new record and call scan again
                    TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.CreateHousebill(hawbNo, origin, dest, pieces, Forklift.Instance.TaskID);
                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        // Withdraw manual scan flag to not get the reprint label message popup
                        manualScan = false;
                    }
                    else
                        Scan2(hawbNo, pieceNo, force);
                }
            }
            else
            {
                manualScan = false;
            }
        }

        private void DisplayHawb(HouseBillItem hawb)
        {
            if (hawbDetails == null)
            {
                hawbDetails = new HawbDetailedItem(this) { Location = new Point(0, 45) };
                this.panelHeader2.Controls.Add(hawbDetails);
            }
            hawbDetails.DisplayHousebill(hawb);
            buttonScannedList.Value = Forklift.Instance.ItemsCount = hawb.Transaction.TransactionRecords;
            if (hawb.ScanMode == ScanModes.Piece)
                ShowHawbAlerts(hawb);
        }

        private void HawbInventory_LoadOptionsMenu(object sender, EventArgs e)
        {

            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO) { Title = "Housebill Lookup" });
            // Remove "Enter Barcode" from option, as it has been implemented through UI icon
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_BARCODE));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.INVENTORY_REPORT_CURRENT));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.INVENTORY_REPORT_LOC));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_INVENTORY));
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        private void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            Forklift.Instance.ShowDialog();

            //if (!string.IsNullOrEmpty(Forklift.Instance.Location))
            //    new LocationViewer(true).ShowDialog();

            if (hawbDetails != null)
                hawbDetails.Visible = false;
            Cursor.Current = Cursors.Default;
            LoadControl();
        }

        private void btnEnterBarcodeManually_Click(object sender, System.EventArgs e)
        {
            manualScan = true;
            EnterManualBarcode();
            manualScan = false;
        }

        private void HawbInventory_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                        {
                            BarcodeEnabled = false;
                            lookuphawb();
                            //HawbLookup.Show();
                            BarcodeEnabled = true;
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.ENTER_BARCODE:
                        {
                            // ANI - enter barcode manually
                            manualScan = true;
                            EnterManualBarcode();
                            manualScan = false;
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.INVENTORY_REPORT:
                        {
                            ReportAllLocations();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.INVENTORY_REPORT_LOC:
                        {
                            ReportByLocation();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.INVENTORY_REPORT_CURRENT:
                        new LocationViewer().ShowDialog();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.PRINT_HAWB_LABEL:
                        {
                            hawbDetails.PrintLabels();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_INVENTORY:
                        {
                            TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.FinalizeInventory(Forklift.Instance.TaskID, Forklift.Instance.ForkliftID);
                            if (!status.TransactionStatus1)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                                new DiscrepancyReport().ShowDialog();
                            }
                            else
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Inventory has been successfully finalized", "Finalized", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                CMXAnimationmanager.GoBack();

                                // ANI - print all housebill labels selected on manual barcode enter
                                foreach (var item in manualScannedHawbs)
                                {
                                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Reprint label for Housebill " + item.HousebillNumber, "Print Label", CargoMatrix.UI.CMXMessageBoxIcon.Message))
                                    {
                                        HawbDetailedItem.PrintLabels(item.HousebillId, item.HousebillNumber, item.TotalPieces);
                                    }
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

            }
        }

        private void lookuphawb()
        {
            try
            {
                CustomUtilities.ScanEnterPopup_old hawbNoPopup = new CustomUtilities.ScanEnterPopup_old(CustomUtilities.BarcodeType.Housebill);
                hawbNoPopup.DisplayHousebillPiece = false;
                hawbNoPopup.Title = "Enter Housebill Number";
                if (DialogResult.OK == hawbNoPopup.ShowDialog())
                {
                    Cursor.Current = Cursors.WaitCursor;
                    var hawUtil = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(hawbNoPopup.ScannedText);
                    if (hawUtil.HousebillId == 0)
                        throw new Exception();
                    var hawb = CargoMatrix.Communication.Inventory.Instance.GetHousebillInfo(hawUtil.HousebillId, Forklift.Instance.TaskID);
                    if (hawb.HousebillId == 0)
                        throw new Exception();

                    DisplayHawb(hawb);
                    Cursor.Current = Cursors.Default;
                }

            }
            catch (Exception)
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Unable to Locate Hawb Details", "Error", CMXMessageBoxIcon.Message);
            }
        }

        /// <summary>
        /// Opens a popup for manual/scanning barcode, and passes the field value to
        /// BarcodeScanned() method as though scanned
        /// </summary>
        private void EnterManualBarcode()
        {
            BarcodeEnabled = false;
            CustomUtilities.EnterManualBarcodePopup hawbNoPopup = new CustomUtilities.EnterManualBarcodePopup(CustomUtilities.BarcodeType.None, false);
            hawbNoPopup.DisplayHousebillPiece = false;
            hawbNoPopup.Title = "Enter Barcode";
            if (DialogResult.OK == hawbNoPopup.ShowDialog())
            {
                BarcodeScanned(hawbNoPopup.ScannedText);
            }
            if (!BarcodeEnabled)
            {
                BarcodeEnabled = true;
            }
        }

        private void ReportAllLocations()
        {
            new LocationViewer(string.Empty).ShowDialog();
        }

        private void ReportByLocation()
        {
            BarcodeEnabled = false;
            CustomUtilities.ScanEnterPopup locPopup = new CustomUtilities.ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            //locPopup.Title = "";
            locPopup.TextLabel = "Scan a location barcode";
            if (DialogResult.OK == locPopup.ShowDialog())
            {
                var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(locPopup.ScannedText);
                switch (barcode.BarcodeType)
                {
                    case CMXBarcode.BarcodeTypes.Area:
                    case CMXBarcode.BarcodeTypes.Door:
                        //int index = locPopup.ScannedText.IndexOf('-');
                        //string loc = index > 0 ? locPopup.ScannedText.Substring(index + 1) : locPopup.ScannedText;
                        new LocationViewer(barcode.Location).ShowDialog();
                        break;

                    default:
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location barcode!", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        break;
                }
            }
            BarcodeEnabled = true;
        }

        #region Download
        private List<HousebillDownload> downloads;
        private Timer statusCheckTimer;
        internal void QueueDownload(HousebillDownload hawbDload)
        {
            if (downloads == null)
            {
                downloads = new List<HousebillDownload>();
                statusCheckTimer = new Timer();
                statusCheckTimer.Interval = 1500;
                statusCheckTimer.Tick += new EventHandler(statusCheckTimer_Tick);
                statusCheckTimer.Enabled = true;
            }
            downloads.Add(hawbDload);
            statusCheckTimer.Enabled = true;
        }
        void statusCheckTimer_Tick(object sender, EventArgs e)
        {
            foreach (HousebillDownload dlod in downloads)
            {
                DownloadStatus status = CargoMatrix.Communication.Inventory.Instance.GetDownloadStatus(dlod.downId);
                dlod.status = status;
                switch (status)
                {
                    case DownloadStatus.Downloaded:
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format("Hawb {0} was downloaded", dlod.hawbNo), "Download Completed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                        break;
                    case DownloadStatus.Failed:
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format("Download was failed for hawb {0}", dlod.hawbNo), "Download Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        break;
                    case DownloadStatus.PendingDownload:
                    case DownloadStatus.Downloading:
                    default:
                        break;
                }
            }

            downloads = (from d in downloads
                         where d.status == DownloadStatus.Downloading || d.status == DownloadStatus.PendingDownload
                         select d).ToList<HousebillDownload>();

            if (downloads.Count == 0)
                statusCheckTimer.Enabled = false;
        }
        #endregion
    }
    public class HousebillDownload
    {
        public HousebillDownload(int id, string hawbNo)
        {
            this.hawbNo = hawbNo;
            downId = id;
        }
        public string hawbNo;
        public int downId;
        public DownloadStatus status;
    }
}

