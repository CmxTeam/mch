﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Inventory
{
    public partial class InventoryMenu : CargoUtilities.SmoothListBoxOptions
    {
        private SmoothListbox.ListItems.StandardListItem shipmentInventory;
        private SmoothListbox.ListItems.StandardListItem uldInventory;

        public InventoryMenu()
        {
            InitializeComponent();
            TitleText = InventoryResources.Text_MenuTitle;
            shipmentInventory = new SmoothListbox.ListItems.StandardListItem(InventoryResources.Text_MenuHawb, CargoMatrix.Resources.Skin.PieceMode);
            uldInventory = new SmoothListbox.ListItems.StandardListItem(InventoryResources.Text_MenuUld, CargoMatrix.Resources.Skin.Freight_Car);
            AddMainListItem(shipmentInventory);
            AddMainListItem(uldInventory);

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(InventoryMenu_ListItemClicked);
            // download barcode rules
            CargoMatrix.Communication.BarcodeParser.Instance.Parse(string.Empty);

        }

        void InventoryMenu_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (listItem == shipmentInventory)
            {
                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new InventoryTasks());

            }
            if (listItem == uldInventory)
            {
                Forklift.Instance.ResetPreAssignedLocation();
                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ULDInventory());

            }
            Cursor.Current = Cursors.Default;
            sender.Reset();
        }
    }
}
