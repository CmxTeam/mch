﻿using System;
using System.Windows.Forms;
using CargoMatrix.Communication;

namespace CargoMatrix.FreightScreening
{
    public partial class MainMenu : SmoothListbox.SmoothListbox
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.labelEmpty.Visible = false;
            this.TitleText = "Cargo Screener";
            pictureBoxDown.Enabled = false;
            pictureBoxUp.Enabled = false;
            panelSoftkeys.Refresh();
            smoothListBoxMainList.RemoveAll();
            //CargoMatrix.Communication.WSPieceScan.FSPTaskSummaryItem taskDetails = CargoMatrix.Communication.HostPlusIncomming.Instance.GetFreightScreeningTaskDetails();
            //AddMainListItem(new SmoothListbox.ListItems.TwoLineListItem(string.Format("MOVE TO SCREENING ({0})", taskDetails.NoOfIncompletedMoveToEtdTasks), FreightScreening.Freight_screening, 106, "Not Assigned: " + taskDetails.NoOfUnassignedMoveToEtdTasks) { Enabled = false });
            //AddMainListItem(new SmoothListbox.ListItems.TwoLineListItem(string.Format("MOVE FROM SCREENING ({0})", taskDetails.NoOfIncompletedMoveFromEtdTasks), FreightScreening.Screening_completed, 107, "Not Assigned: " + taskDetails.NoOfUnassignedMoveFromEtdTasks) { Enabled = false });
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("PHYSICAL INSPECTION ", FreightScreening.Visual_Inspection, (int)InspectionMode.Physical));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("CANINE INSPECTION", FreightScreening.Canine, (int)InspectionMode.Canine));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("VERIFY CUSTOMER SCREENING", FreightScreening.Customer_screened, (int)InspectionMode.Customer));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("XRAY SCREENING", FreightScreening.x_ray, (int)InspectionMode.XRay));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("ETD SCREENING", FreightScreening.ETD, (int)InspectionMode.ETD));
        }
        private void MainMenu_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //if (listItem is SmoothListbox.ListItems.TwoLineListItem)
            //{
            //    Cursor.Current = Cursors.WaitCursor;
            //    ScreeningList list = new ScreeningList((listItem as SmoothListbox.ListItems.StandardListItem).ID);
            //    list.Location = this.Location;
            //    list.Size = this.Size;
            //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(list);
            //}
            //else
            //{
                if (listItem is SmoothListbox.ListItems.StandardListItem)
                {
                    //if (CustomUtilities.VisualInspectionSupervisorMessageBox.Show(this, "Confirm Pin") == DialogResult.OK)
                    //{
                    //    string tsaCertUserName = CustomUtilities.VisualInspectionSupervisorMessageBox.SupervisorName;
                    //    // pass count
                        Cursor.Current = Cursors.WaitCursor;

                        InspectionMode mode = (InspectionMode)(listItem as SmoothListbox.ListItems.StandardListItem).ID;
                        CargoInspection list = new CargoInspection(mode);
                        list.Location = this.Location;
                        list.Size = this.Size;
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(list);
                    //}
                }
            //}
            smoothListBoxMainList.Reset();
        }

        private void MainMenu_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        private void MainMenu_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
                        if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                            CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                        break;
                }
            }
        }

    }

}
