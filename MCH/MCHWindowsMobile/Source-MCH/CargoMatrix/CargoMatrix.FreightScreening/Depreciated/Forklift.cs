﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSFreightScreening;
using CargoMatrix.Communication.WSPieceScan;

namespace CargoMatrix.FreightScreening
{
    public partial class Forklift : CargoMatrix.Utilities.MessageListBox
    {
        private static Forklift instance;
        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                    instance = new Forklift();
                return instance;
            }
        }

        public int ForkliftID
        { get; set; }
        public int ActionID
        { get; set; }
        private Forklift()
        {
            InitializeComponent();
            smoothListBoxBase1.MultiSelectEnabled = true;
            this.HeaderText = "Scanned List";
            this.HeaderText2 = "Select shipments to drop into location";
            this.HeaderText2 = string.Empty;
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Forklift_LoadListEvent);
        }

        void Forklift_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            PopulateForkliftContent();
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            if (DialogResult.OK == confirmLoc.ShowDialog())
            {
                CMXBarcode.ScanObject scan = BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                if (Forklift.Instance.ActionID == 107 && (scan.BarcodeType == CMXBarcode.BarcodeTypes.Area || scan.BarcodeType == CMXBarcode.BarcodeTypes.Door))
                {
                    DropForkliftManually(scan.Location);
                }
                else if (Forklift.Instance.ActionID == 106 && scan.BarcodeType == CMXBarcode.BarcodeTypes.ScreeningArea)
                {

                }
                else

                    CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            PopulateForkliftContent();
        }

        private void DropForkliftManually(string loc)
        {
            foreach (var item in smoothListBoxBase1.Items)
            {
                if (item is CustomListItems.ComboBox && (item as CustomListItems.ComboBox).SubItemSelected)
                {
                    //foreach (var pieceId in (item as CustomListItems.ComboBox).SelectedIds)

                    MoveHBPiecesResponse status = CargoMatrix.Communication.HostPlusIncomming.Instance.DropPiecesIntoLocation((item as CustomListItems.ComboBox).ID,
                        (item as CustomListItems.ComboBox).SelectedIds.ToArray<int>(), Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, loc);
                    if (!status.IsSuccess)
                        CargoMatrix.UI.CMXMessageBox.Show(status.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

                }
            }
            PopulateForkliftContent();
        }

        private void PopulateForkliftContent()
        {
            smoothListBoxBase1.RemoveAll();
            foreach (var hawb in CargoMatrix.Communication.FreightScreening.Instance.GetForkLiftHouseBills(ForkliftID))
            {
                List<CustomListItems.ComboBoxItemData> pieces = new List<CustomListItems.ComboBoxItemData>();

                foreach (var hawbPiece in hawb.Pieces)
                {
                    pieces.Add(new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", hawbPiece.PieceNumber), "Loc " + hawbPiece.Location, hawbPiece.PieceId));
                }
                Image icon;
                string line2;
                if (hawb.ScanMode == CargoMatrix.Communication.WSFreightScreening.ScanModes.Piece)
                {
                    icon = CargoMatrix.Resources.Skin.PieceMode;
                    line2 = string.Format("Pieces: {0} of {1}", hawb.Pieces.Length, hawb.TotalPieces);
                }
                else
                {
                    icon = CargoMatrix.Resources.Skin.countMode;
                    line2 = string.Format("Pieces: {0} of {1}", hawb.Pieces.Length, hawb.TotalPieces);
                }

                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, hawb.HousebillNumber, line2, icon, pieces);
                combo.ContainerName = "ScanView";
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                smoothListBoxBase1.AddItem(combo);
            }

        }
        void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            if (comboIsEmpty)
                smoothListBoxBase1.RemoveItem(sender as Control);
            combo_SelectionChanged(null, null);
            smoothListBoxBase1.LayoutItems();
            TransactionStatus finalStatus = new TransactionStatus() { TransactionStatus1 = true };
            if (subItemId == -1)
            {

                finalStatus = CargoMatrix.Communication.FreightScreening.Instance.RemoveHousebillFromForklift(sender.ID, ForkliftID);

                if (finalStatus.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            else
            {
                finalStatus = CargoMatrix.Communication.FreightScreening.Instance.RemovePieceFromForkLift(sender.ID, subItemId, ForkliftID, ScanModes.Piece);
                if (finalStatus.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            if (smoothListBoxBase1.ItemsCount == 0)
                DialogResult = DialogResult.Cancel;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if (smoothListBoxBase1.SelectedItems.Count == 0)
            {
                bool enableflag = false;

                CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
                if (item is CustomListItems.ComboBox && (item as CustomListItems.ComboBox).SubItemSelected)
                {
                    enableflag = true;
                }
                OkEnabled = enableflag;
            }
            else
                OkEnabled = true;
        }
    }
}
