// CameraCaptureDLL.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include <windows.h>
#include <winbase.h>
#include <objbase.h>
#include <commctrl.h>

CGraphManager *m_pGraphManager;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{

	if (ul_reason_for_call==DLL_PROCESS_ATTACH)
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (ul_reason_for_call==DLL_PROCESS_DETACH)
		CoUninitialize();
    return TRUE;
}

extern "C" int __declspec(dllexport) InitializeGraph(HWND hWnd, int resolution)
{
	HRESULT hr = S_OK;
	

    // Create the graph manager. This will control the dshow capture pipeline
    m_pGraphManager = new CGraphManager();
	m_pGraphManager->SetResolution(resolution);
    if( m_pGraphManager == NULL )
    {
        ERR( E_OUTOFMEMORY );
    }

    CHK( m_pGraphManager->RegisterNotificationWindow( hWnd ));

    
    CHK( m_pGraphManager->BuildCaptureGraph());

    CHK( m_pGraphManager->RunCaptureGraph());




Cleanup:
    return (int)hr; 
}

extern "C" bool __declspec(dllexport) CaptureStill(WCHAR* LocationToStore)
{
	m_pGraphManager->CaptureStillImage(LocationToStore);
	return true;
}
extern "C" bool __declspec(dllexport) EnableFlash(bool enabled)
{
	return m_pGraphManager->EnableFlash(enabled);

}
//extern "C" bool __declspec(dllexport) SetResolution(int resolution)
//{
//	return m_pGraphManager->SetResolution(resolution);
//	
//
//}

extern "C" void __declspec(dllexport) ShutDown()
{
	if(m_pGraphManager != NULL)
	{
		//m_pGraphManager->ShutDown();
		delete m_pGraphManager;
		m_pGraphManager = NULL;
	}

	
}
extern "C" void __declspec(dllexport) PreviewResize()
{
	m_pGraphManager->PreviewResize();
	
}
