﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.DamageCapture
{
    public class ShipmentConditionsSummaryEventArgs : EventArgs
    {
        int houseBillID;

        public int HouseBillID
        {
            get { return houseBillID; }
            set { houseBillID = value; }
        }
        IShipmentConditionSummary[] conditionsSummaries;

        public IShipmentConditionSummary[] ConditionsSummaries
        {
            get { return conditionsSummaries; }
            set { conditionsSummaries = value; }
        }
    }
}
