﻿using System;
using System.Windows.Forms;
using CargoMatrix.Communication;
using System.Data;
using CargoMatrix.Communication.WSScannerMCHService;
namespace CargoMatrix.CargoScreener
{
    public partial class MainMenu : SmoothListbox.SmoothListbox
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.labelEmpty.Visible = false;
            this.TitleText = "Cargo Screener";
            pictureBoxDown.Enabled = false;
            pictureBoxUp.Enabled = false;
            panelSoftkeys.Refresh();
            smoothListBoxMainList.RemoveAll();
                    AddMainListItem(new SmoothListbox.ListItems.StandardListItem("PHYSICAL INSPECTION ", CargoMatrix.CargoScreenerMCH.Resource.Visual_Inspection, 1));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("ETD SCREENING", CargoMatrix.CargoScreenerMCH.Resource.ETD, 2));

        }
        private void MainMenu_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            bool isCertified = false;
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("exec verifyscreeningcertified " + CargoMatrix.Communication.WebServiceManager.UserID().ToString());
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString().ToUpper() == "TRUE")
                {
                    isCertified = true;
                }
            }

            if (!isCertified)
            {
                CargoMatrix.UI.CMXMessageBox.Show("You are not authorized to use this feature.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }



            if (listItem is SmoothListbox.ListItems.StandardListItem)
            {

                Cursor.Current = Cursors.WaitCursor;

                switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
                { 
                    case 1:
                      
         
                        break;

                    case 2:
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new Morpho());
                        break;
                }
 

            }
            Cursor.Current = Cursors.Default;
            smoothListBoxMainList.Reset();
        }

        private void MainMenu_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        private void MainMenu_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
                        if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                            CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                        
                        break;
                }
            }
        }

    }

}
