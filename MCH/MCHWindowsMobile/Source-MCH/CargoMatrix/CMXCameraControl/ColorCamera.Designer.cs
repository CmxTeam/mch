﻿namespace CMXCameraControl
{
    partial class ColorCamera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                ShutDown();
            }
            catch (System.MissingMethodException e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 90003);
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ColorCamera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Name = "ColorCamera";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ColorCamera_Paint);
            this.Resize += new System.EventHandler(this.ColorCamera_Resize);
            this.EnabledChanged += new System.EventHandler(this.ColorCamera_EnabledChanged);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
