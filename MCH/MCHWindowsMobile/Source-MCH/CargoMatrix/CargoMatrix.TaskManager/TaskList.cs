﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.TaskManager;
using CustomUtilities;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;

namespace CargoMatrix.TaskManager
{
    public partial class TaskList : MessageListBoxAsync<BusinessTask>
    {
        private Label filterLabel;

        private string taskID;
        private TaskProgressTypeEnum taskFilter;
        private CargoMatrix.UI.CMXPictureButton filterButton;


        public TaskList(string taskID, string taskName)
        {
            InitializeComponents();

            this.taskID = taskID;
            taskFilter = TaskProgressTypeEnum.NotAssigned;
            filterLabel.Text = taskFilter.AsString();

            this.HeaderText = string.Format("TaskManager: {0}", taskName);
            this.MultiSelectListEnabled = true;
            this.LoadListEvent += new LoadSmoothList(TaskList_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(TaskList_ListItemClicked);
      
        }

        void TaskList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (isSelected == false) return;

            var item = (TaskItemRendered)listItem;
            
            if(item.Task.Status == TaskProgressTypeEnum.Completed)
            {
                this.smoothListBoxBase1.UnSelectControl(item);

                this.OkEnabled = this.smoothListBoxBase1.SelectedItems.Count > 0;
                
                return;
            }

            int selecteItemsCount = this.smoothListBoxBase1.SelectedItems.Count;

            if(selecteItemsCount > 1)
            {
                var exists = this.smoothListBoxBase1.SelectedItems.OfType<TaskItemRendered>().ToList().Exists(taskItem => 
                    {
                        var task = taskItem.Task;

                        return string.Compare(task.ID, item.Task.ID, true) != 0                         //  different from clicked one
                            && CommonMethods.SameValuesLists(task.UserIDs, item.Task.UserIDs) == true;  // same users assigned
                    });

                if(exists == false)
                {
                    this.smoothListBoxBase1.UnSelectControl(item);
                }
            }


            this.OkEnabled = this.smoothListBoxBase1.SelectedItems.Count > 0;
        }

        void TaskList_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.LoadControl();
        }



        private void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            var tasks = CargoMatrix.Communication.TaskManager.TaskManagerProvider.Instance.GetTasks(this.taskID, taskFilter);

            this.smoothListBoxBase1.RemoveAll();

            foreach (var task in tasks)
            {
                smoothListBoxBase1.AddItem2(this.InitializeItem(task));
            }

            smoothListBoxBase1.LayoutItems();
            smoothListBoxBase1.RefreshScroll();

            Cursor.Current = Cursors.Default;
        }

        private void InitializeComponents()
        {
            this.SuspendLayout();
            SizeF scalSize = new System.Drawing.SizeF(this.CurrentAutoScaleDimensions.Height / 96F, this.CurrentAutoScaleDimensions.Height / 96F);
          
            filterLabel = new Label();
            filterLabel.Location = new System.Drawing.Point(4, 2);
            filterLabel.Size = new System.Drawing.Size(186, 15);
            filterLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            filterLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.panelHeader.Controls.Add(filterLabel);
            this.panelHeader.Height = (int)(40 * scalSize.Height);
            this.smoothListBoxBase1.Height = (int)(163 * scalSize.Height);
            this.smoothListBoxBase1.Top = panelHeader.Bottom;
            filterLabel.Scale(scalSize);

            filterButton = new CargoMatrix.UI.CMXPictureButton();
            filterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            filterButton.Location = new System.Drawing.Point(196, 6);
            filterButton.Size = new System.Drawing.Size(32, 32);
            filterButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            filterButton.Image = Resources.Skin.filter_btn;
            filterButton.PressedImage = Resources.Skin.filter_btn_over;
            filterButton.Click += new EventHandler(filterButton_Click);
            this.filterTextBox.Width = filterLabel.Width;
            this.panelHeader.Controls.Add(filterButton);
            filterButton.Scale(scalSize);

            this.ResumeLayout(false);
        }

        void filterButton_Click(object sender, EventArgs e)
        {
            MessageListBox filterPopup = new MessageListBox();
            filterPopup.HeaderText = "Filter by";
            filterPopup.HeaderText2 = "Select filter";
            filterPopup.MultiSelectListEnabled = false;
            filterPopup.OneTouchSelection = true;

            var items = new Control[] 
                {
                    new StandardListItem(TaskProgressTypeEnum.NotAssigned.AsString(), null, (int)TaskProgressTypeEnum.NotAssigned),
                    new StandardListItem(TaskProgressTypeEnum.NotStarted.AsString(), null, (int)TaskProgressTypeEnum.NotStarted),
                    new StandardListItem(TaskProgressTypeEnum.InProgress.AsString(), null, (int)TaskProgressTypeEnum.InProgress),
                    new StandardListItem(TaskProgressTypeEnum.Completed.AsString(), null, (int)TaskProgressTypeEnum.Completed),
               };

            filterPopup.AddItems(items);


            if (filterPopup.ShowDialog() == DialogResult.OK)
            {
                this.Refresh();
                
                taskFilter = (TaskProgressTypeEnum)((StandardListItem)filterPopup.SelectedItems[0]).ID;
                filterLabel.Text = taskFilter.AsString();

                this.LoadControl();
            }
        }


        protected override Control InitializeItem(BusinessTask item)
        {
            var listItem = new TaskItemRendered(item);
            listItem.ButtonDetailsClick+=new EventHandler(listItem_ButtonDetailsClick);

            return listItem;
        }

        void listItem_ButtonDetailsClick(object sender, EventArgs e)
        {
            var listItem = (TaskItemRendered)sender;

            var businessTask = listItem.Task;

            TaskDetailsList taskDetails = new TaskDetailsList();
            taskDetails.HeaderText = "Task Details";
            taskDetails.HeaderText2 = string.Empty;
            taskDetails.GetTask = () => TaskManagerProvider.Instance.GetTask(businessTask.ID);
            taskDetails.ShowDialog();

            this.Refresh();

            var actionType = taskDetails.ActionType;

            if (actionType == TaskDetailsList.ActionTypeEnum.Assign)
            {
                this.OpenUsersList(new BusinessTask[] { businessTask });
            }
        
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            if(this.smoothListBoxBase1.SelectedItems.Count <= 0) return;

            BusinessTask[] tasks = (from item in this.smoothListBoxBase1.SelectedItems.OfType<TaskItemRendered>()
                                   select item.Task).ToArray();

            this.OpenUsersList(tasks);
        }

        private void OpenUsersList(BusinessTask[] tasks)
        {
            UserList userList = new UserList(tasks);
            userList.ShowDialog();

            this.Refresh();

            if (userList.ActionType == UserList.ActionTypeEnum.Assign)
            {
                this.LoadControl();
            }
        }
    
    }
}