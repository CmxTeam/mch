﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CustomUtilities;
using System.Drawing;
using System.Windows.Forms;

namespace CargoMatrix.TaskManager
{
    public class TaskProgressViewMessageListBox : MessageListBoxAsync<SmoothListbox.ListItems.StandardListItem>
    {
        Label label1;


        private void InitializeComponents()
        {
            this.SuspendLayout();
            SizeF scalSize = new System.Drawing.SizeF(this.CurrentAutoScaleDimensions.Height / 96F, this.CurrentAutoScaleDimensions.Height / 96F);

            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(23, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.Text = "Task :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
                

            this.ResumeLayout(false);
        }


        protected override System.Windows.Forms.Control InitializeItem(SmoothListbox.ListItems.StandardListItem item)
        {
            return null;
        }
    }
}
