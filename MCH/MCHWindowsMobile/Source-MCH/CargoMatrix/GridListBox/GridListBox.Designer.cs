﻿namespace CargoMatrix.GridListBox
{
    partial class GridListBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.horizontalSmoothListbox1 = new SmoothListbox.HorizontalSmoothListbox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxIcon = new OpenNETCF.Windows.Forms.PictureBox2();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelLine2 = new System.Windows.Forms.Label();
            this.labelHouseBill = new System.Windows.Forms.Label();
            this.pictureBoxDetails = new System.Windows.Forms.PictureBox();
            this.panelViewer = new System.Windows.Forms.Panel();
            this.summaryRow1 = new CargoMatrix.GridListBox.SummaryRow();
            this.headerRow1 = new CargoMatrix.GridListBox.HeaderRow();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // horizontalSmoothListbox1
            // 
            this.horizontalSmoothListbox1.BackColor = System.Drawing.Color.White;
            this.horizontalSmoothListbox1.Location = new System.Drawing.Point(3, 219);
            this.horizontalSmoothListbox1.Name = "horizontalSmoothListbox1";
            this.horizontalSmoothListbox1.Size = new System.Drawing.Size(234, 38);
            this.horizontalSmoothListbox1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pictureBoxIcon);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.labelLine2);
            this.panel1.Controls.Add(this.labelHouseBill);
            this.panel1.Controls.Add(this.pictureBoxDetails);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 35);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMoveHandler);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDownHandler);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseUpHandler);
            this.panel1.EnabledChanged += new System.EventHandler(this.panel1_EnabledChanged);
            // 
            // pictureBoxIcon
            // 
            this.pictureBoxIcon.Location = new System.Drawing.Point(3, 0);
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMoveHandler);
            this.pictureBoxIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDownHandler);
            this.pictureBoxIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseUpHandler);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 34);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 1);
            // 
            // labelLine2
            // 
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.Location = new System.Drawing.Point(40, 18);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(160, 13);
            // 
            // labelHouseBill
            // 
            this.labelHouseBill.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelHouseBill.Location = new System.Drawing.Point(40, 4);
            this.labelHouseBill.Name = "labelHouseBill";
            this.labelHouseBill.Size = new System.Drawing.Size(160, 13);
            // 
            // pictureBoxDetails
            // 
            this.pictureBoxDetails.Location = new System.Drawing.Point(205, 1);
            this.pictureBoxDetails.Name = "pictureBoxDetails";
            this.pictureBoxDetails.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDetails.Click += new System.EventHandler(this.pictureBoxDetails_Click);
            // 
            // panelViewer
            // 
            this.panelViewer.Location = new System.Drawing.Point(48, 98);
            this.panelViewer.Name = "panelViewer";
            this.panelViewer.Size = new System.Drawing.Size(78, 50);
            // 
            // summaryRow1
            // 
            this.summaryRow1.BackColor = System.Drawing.Color.White;
            this.summaryRow1.Location = new System.Drawing.Point(0, 164);
            this.summaryRow1.Name = "summaryRow1";
            this.summaryRow1.Size = new System.Drawing.Size(240, 14);
            this.summaryRow1.TabIndex = 1;
            // 
            // headerRow1
            // 
            this.headerRow1.BackColor = System.Drawing.Color.White;
            this.headerRow1.Location = new System.Drawing.Point(0, 75);
            this.headerRow1.Name = "headerRow1";
            this.headerRow1.Size = new System.Drawing.Size(240, 14);
            this.headerRow1.TabIndex = 2;
            // 
            // GridListBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.BarcodeEnabled = true;
            this.Controls.Add(this.summaryRow1);
            this.Controls.Add(this.panelViewer);
            this.Controls.Add(this.headerRow1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.horizontalSmoothListbox1);
            this.Name = "GridListBox";
            this.Size = new System.Drawing.Size(240, 300);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SmoothListbox.HorizontalSmoothListbox horizontalSmoothListbox1;
        protected SummaryRow summaryRow1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelLine2;
        private System.Windows.Forms.Label labelHouseBill;
        private System.Windows.Forms.PictureBox pictureBoxDetails;
        private HeaderRow headerRow1;
        //private System.Windows.Forms.PictureBox pictureBox1;
        //protected CargoMatrix.UI.CMXCounterIcon counterIcon;
        protected OpenNETCF.Windows.Forms.PictureBox2 pictureBoxIcon;
        private System.Windows.Forms.Panel panelViewer;
    }
}
