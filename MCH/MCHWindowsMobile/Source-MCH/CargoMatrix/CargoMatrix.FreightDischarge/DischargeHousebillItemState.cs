﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.CargoDischarge;

namespace CargoMatrix.FreightDischarge
{
    public class DischargeHousebillItemState
    {
        public IDischargeHouseBillItem Item { get; set; }

        public DischargeHousebillItemStateEnum State { get; set; }

        public int ScannedPeces { get; set; }

        public int TotalPieces { get; set; }


        public DischargeHousebillItemState(IDischargeHouseBillItem item, DischargeHousebillItemStateEnum state)
        {
            this.Item = item;
            this.State = state;
            this.ScannedPeces = 0;
            this.TotalPieces = 0;
        }


    }

    public enum DischargeHousebillItemStateEnum
    {
        NonScanned,
        PartiallyScanned,
        CompletlyScanned
    }
}
