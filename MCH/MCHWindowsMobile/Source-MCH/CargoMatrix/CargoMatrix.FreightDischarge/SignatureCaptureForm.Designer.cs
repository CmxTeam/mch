﻿namespace CargoMatrix.FreightDischarge
{
    partial class SignatureCaptureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
    
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPieces = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pcxPhoto = new CustomUtilities.ChangeSizePictureBox();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.lblDriversName = new System.Windows.Forms.Label();
            this.pcxHeader = new System.Windows.Forms.PictureBox();
            this.buttonClear = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.lblSignatureRequired = new System.Windows.Forms.Label();
            this.signatureControl = new CustomUtilities.Signature();
            this.pcxFooter = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pcxPhoto);
            this.panel1.Controls.Add(this.pcxHeader);
            this.panel1.Controls.Add(this.buttonClear);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.lblSignatureRequired);
            this.panel1.Controls.Add(this.signatureControl);
            this.panel1.Controls.Add(this.pcxFooter);
            this.panel1.Controls.Add(this.lblPieces);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblCompanyName);
            this.panel1.Controls.Add(this.lblDriversName);
            this.panel1.Location = new System.Drawing.Point(3, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 279);
            // 
            // lblPieces
            // 
            this.lblPieces.BackColor = System.Drawing.Color.White;
            this.lblPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPieces.Location = new System.Drawing.Point(172, 89);
            this.lblPieces.Name = "lblPieces";
            this.lblPieces.Size = new System.Drawing.Size(57, 13);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(95, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.Text = "Total Pieces :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(95, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.Text = "Company :";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(95, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.Text = "Driver :";
            // 
            // pcxPhoto
            // 
            this.pcxPhoto.EnlargeToParent = false;
            this.pcxPhoto.Header = "Click the image to switch to regular size";
            this.pcxPhoto.Location = new System.Drawing.Point(5, 23);
            this.pcxPhoto.LocationRegular = new System.Drawing.Point(5, 23);
            this.pcxPhoto.LocationEnlarged = new System.Drawing.Point(0, 105);
            this.pcxPhoto.Name = "pcxPhoto";
            this.pcxPhoto.PersistLocation = false;
            this.pcxPhoto.Size = new System.Drawing.Size(85, 61);
            this.pcxPhoto.SizeEnlarged = new System.Drawing.Size(234, 169);
            this.pcxPhoto.SizeRegular = new System.Drawing.Size(85, 61);
          
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.BackColor = System.Drawing.Color.White;
            this.lblCompanyName.Location = new System.Drawing.Point(96, 72);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(136, 13);
            // 
            // lblDriversName
            // 
            this.lblDriversName.BackColor = System.Drawing.Color.White;
            this.lblDriversName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblDriversName.Location = new System.Drawing.Point(96, 39);
            this.lblDriversName.Name = "lblDriversName";
            this.lblDriversName.Size = new System.Drawing.Size(136, 13);
            // 
            // pcxHeader
            // 
            this.pcxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcxHeader.Location = new System.Drawing.Point(0, 0);
            this.pcxHeader.Name = "pcxHeader";
            this.pcxHeader.Size = new System.Drawing.Size(234, 20);
            this.pcxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.Location = new System.Drawing.Point(90, 238);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(52, 37);
            this.buttonClear.TabIndex = 4;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(166, 238);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.TabIndex = 5;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(17, 238);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.TabIndex = 6;
            // 
            // lblSignatureRequired
            // 
            this.lblSignatureRequired.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblSignatureRequired.Location = new System.Drawing.Point(5, 114);
            this.lblSignatureRequired.Name = "lblSignatureRequired";
            this.lblSignatureRequired.Size = new System.Drawing.Size(198, 17);
            this.lblSignatureRequired.Text = "Signature required";
            // 
            // signatureControl
            // 
            this.signatureControl.Location = new System.Drawing.Point(5, 134);
            this.signatureControl.Name = "signatureControl";
            this.signatureControl.Size = new System.Drawing.Size(224, 95);
            this.signatureControl.TabIndex = 7;
            // 
            // pcxFooter
            // 
            this.pcxFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxFooter.Location = new System.Drawing.Point(0, 232);
            this.pcxFooter.Name = "pcxFooter";
            this.pcxFooter.Size = new System.Drawing.Size(234, 47);

            this.InitializeFields();
            
            // 
            // SignatureCaptureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "SignatureCaptureForm";
            this.Text = "SignatureCaptureForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pcxHeader;
        private CargoMatrix.UI.CMXPictureButton buttonClear;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private System.Windows.Forms.Label lblSignatureRequired;
        private CustomUtilities.Signature signatureControl;
        private System.Windows.Forms.PictureBox pcxFooter;
        private System.Windows.Forms.Label lblDriversName;
        private System.Windows.Forms.Label lblCompanyName;
        private CustomUtilities.ChangeSizePictureBox pcxPhoto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPieces;
        private System.Windows.Forms.Label label3;
        
    }
}