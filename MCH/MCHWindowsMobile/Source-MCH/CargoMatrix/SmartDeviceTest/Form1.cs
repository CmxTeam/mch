﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CustomUtilities;
using CargoMatrix.FreightDischarge;

namespace SmartDeviceTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button1.Click += new EventHandler(button1_Click);

            Scanner.Scanner.BarcodeScanned += new EventHandler<SmartDeviceTest.Scanner.ScanEventArgs>(Scanner_BarcodeScanned);

            Scanner.Scanner.Start();
        }

        void Scanner_BarcodeScanned(object sender, SmartDeviceTest.Scanner.ScanEventArgs e)
        {
            button1.Text = e.ScannedText;

            Scanner.ScanObject result = Scanner.BarcodeParser.Parse(e.ScannedText);

            CMXBarcode.BarcodeParser p = new CMXBarcode.BarcodeParser();
            var o = p.Parse(e.ScannedText);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            TestForm tf = new TestForm();
            tf.ShowDialog();

            Scanner.Scanner.ContinueListen(Scanner_BarcodeScanned);


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Scanner.Scanner.ContinueListen(Scanner_BarcodeScanned);
        }

       }
}