﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest
{
    public enum CMXTextBoxInputMode { Normal, IsAlphaNumeric, Alphabetic, Numeric }
    public class CMXAlphaNumTextBox : System.Windows.Forms.TextBox
    {
        public CMXAlphaNumTextBox()
            : base()
        { InputMode = CMXTextBoxInputMode.Normal; }
        public CMXTextBoxInputMode InputMode
        { get; set; }
        protected override void OnKeyPress(System.Windows.Forms.KeyPressEventArgs e)
        {
            switch (InputMode)
            {
                case CMXTextBoxInputMode.IsAlphaNumeric:
                    if (char.IsLetterOrDigit(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == '\r')
                        e.Handled = false;
                    else
                        e.Handled = true;
                    break;
                case CMXTextBoxInputMode.Alphabetic:
                    if (char.IsLetter(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == '\r')
                        e.Handled = false;
                    else
                        e.Handled = true;

                    break;
                case CMXTextBoxInputMode.Numeric:
                    if (char.IsDigit(e.KeyChar) || e.KeyChar == '\b' || e.KeyChar == '\r')
                        e.Handled = false;
                    else
                        e.Handled = true;

                    break;
                case CMXTextBoxInputMode.Normal:
                default:
                    break;
            }
            base.OnKeyPress(e);

        }
    }
}
