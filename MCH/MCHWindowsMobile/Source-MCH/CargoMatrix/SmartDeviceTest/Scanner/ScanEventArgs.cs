﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner
{
    public class ScanEventArgs : EventArgs
    {
        string scannedText;

        public string ScannedText
        {
            get { return scannedText; }
        }

        public ScanEventArgs(string scannedText)
            : base()
        {
            this.scannedText = scannedText;
        }
    }
}
