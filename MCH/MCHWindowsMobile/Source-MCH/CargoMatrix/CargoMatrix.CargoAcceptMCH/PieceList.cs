﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using CargoMatrix.Communication.WSCargoLoaderMCHService;

namespace CargoMatrix.CargoAccept
{
    public partial class PieceList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

        private DataRow Awb;
        private long TaskId;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
         
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;

        public PieceList(DataRow awb,long taskId)
        {

            this.TaskId = taskId;
            this.Awb = awb;

            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(List_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(List_ListItemClicked);
        }

   



    

        protected virtual void List_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            PieceCargoItem tempCargoItem = (PieceCargoItem)listItem;
            DataRow tempData = (DataRow)tempCargoItem.ItemData;
           // MessageBox.Show(tempData["awbid"].ToString());
 
            

        }


        void Item_OnMoreClick(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //DataRow tempItemData = (sender as ShipmentCargoItem).ItemData;
            //Cursor.Current = Cursors.Default;
            //MessageBox.Show(tempItemData["AwbRef"].ToString());
        }

  
        void buttonAdd_Click(object sender, System.EventArgs e)
        {
            AddNew();
        }


        void AddNew()
        {


            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(string.Format("exec GetCargoAcceptAWBById {0},{1}", TaskId, Awb["awbid"].ToString()));
            int remainingPcs = int.Parse(dt.Rows[0]["tpcs"].ToString()) - int.Parse(dt.Rows[0]["RcvPcs"].ToString());
            if (remainingPcs < 0)
            {
                remainingPcs = 0;
            }

          

            UldType selectedUldType = GetUldType();
            string uldNumber = string.Empty;
            string uldPrefix = string.Empty;
            long uldPrefixId = 0;
            //int userid = CargoMatrix.Communication.WebServiceManager.UserID();

            if (selectedUldType != null)
            {


                long awbid = long.Parse(Awb["awbid"].ToString());
                string reference = Awb["awbref"].ToString();

                if (selectedUldType.Code.ToUpper() == "LOOSE")
                {




                    decimal weight = 0; // decimal.Parse(Awb["twt"].ToString());
                    int pcs = remainingPcs; // int.Parse(Awb["tpcs"].ToString());
                    bool screened = false;
                    if (Awb["screened"].ToString() == "1")
                    {
                        screened = true;
                    }
                    if (DialogResult.OK == UldWeightMessageBox.Show(reference,"LOOSE", ref weight, ref pcs, ref screened,false))
                    {

                        CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_AddCargoAcceptAWBDims(TaskId, awbid.ToString(), (int)selectedUldType.Id, 6, "", pcs, (double)weight, screened);
                    }


                    LoadControl();

                }
                else
                {
                    if (DialogResult.OK == UldEditMessageBox.Show(selectedUldType.Code, ref uldPrefix, ref uldNumber, ref uldPrefixId))
                    {
                         List<long> awbids = new List<long> ();
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Is this a mixed pallet?", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                         awbids =   GetAwbList();
 
                        }

                        if (!awbids.Contains(awbid))
                        {
                            awbids.Add(awbid);
                        }
                        StringBuilder  list = new StringBuilder("");
                         
                        foreach (long awb in awbids)
                        {
                            if (list.ToString() == string.Empty)
                            {
                                list.Append(awb.ToString());
                            }
                            else
                            {
                                list.Append("," + awb.ToString());
                            }

                        }


                        decimal weight = 0; //  decimal.Parse(Awb["twt"].ToString());
                        int pcs = 1;// int.Parse(Awb["tpcs"].ToString());
                        bool screened = false;
                        if (Awb["screened"].ToString() == "1")
                        {
                            screened = true;
                        }
                        if (DialogResult.OK == UldWeightMessageBox.Show(reference, uldPrefix + uldNumber, ref weight, ref pcs, ref screened,true))
                        {

                            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_AddCargoAcceptAWBDims(TaskId, list.ToString(), (int)selectedUldType.Id, (int)uldPrefixId, uldNumber, pcs, (double)weight, screened);
                        }


                     
                        LoadControl();
                    }
                }


            }
        }


        UldType GetUldType()
        {
            UldType[] uldTypes = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetUldTypeList();
            if (uldTypes == null)
                return null;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Uld Types";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoAccept";


            actPopup.RemoveAllItems();

            for (int i = 0; i < uldTypes.Length; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(uldTypes[i].Code, null, i + 1));

            }





            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    for (int i = 0; i < uldTypes.Length; i++)
                    {
                        if (actPopup.SelectedItems[0].Name == uldTypes[i].Code)
                        {
                            return uldTypes[i];
                        }
                    }
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }



        }




        List<long> GetAwbList()
        {
            List<long> l = new List<long>();
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Awbs";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = true;
            actPopup.MultiSelectListEnabled = true;

            actPopup.HeaderText = "CargoAccept";


            actPopup.RemoveAllItems();


         
            DataTable dtList = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(string.Format("exec GetCargoAcceptAvailableAWBs {0}", TaskId));
            for (int i = 0; i < dtList.Rows.Count; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(dtList.Rows[i]["awbref"].ToString(), CargoMatrix.Resources.Skin.PieceMode, i + 1));
            }
 
            


            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
              


                    for (int i = 0; i < actPopup.SelectedItems.Count; i++)
                    {
                         

                        for(int j = 0 ; j<dtList.Rows.Count;j++)
                        {
                        if(dtList.Rows[j]["awbref"].ToString() == actPopup.SelectedItems[i].Name)
                        {

                            l.Add(long.Parse(dtList.Rows[j]["awbid"].ToString()));
                        }
                        }



                    }


                    return l;
                }
                else
                {
                    return l;
                }
            }
            catch
            {
                return l;
            }



        }


        void buttonForkLift_Click(object sender, System.EventArgs e)
        {
            this.buttonForklift.Value = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_GetForkliftCounter(TaskId);
            if (this.buttonForklift.Value == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                BarcodeEnabled = false;
                DialogResult dr = ForkLiftViewer.Instance.ShowDialog(TaskId, Awb["awbref"].ToString());
                LoadControl();
            }
          
        }


        void List_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
            

                    
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }
 
        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));

        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;


            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(string.Format("exec GetCargoAcceptAWBById {0},{1}", TaskId, Awb["awbid"].ToString()));

            this.label1.Text = string.Format("{0}", dt.Rows[0]["awbref"].ToString());
            this.label2.Text = string.Format("Pcs: {0} of {1} - Wgt: {2}{3}", dt.Rows[0]["RcvPcs"].ToString(), dt.Rows[0]["tpcs"].ToString(),dt.Rows[0]["twt"].ToString(), dt.Rows[0]["weightuom"].ToString());
            this.label3.Text = string.Format("Loc: {0}", dt.Rows[0]["Loc"].ToString());


            this.buttonForklift.Value = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_GetForkliftCounter(TaskId);
        
  



            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;



            DataTable dtPieces = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(string.Format("exec GetCargoAcceptAWBDims {0},{1}", TaskId, Awb["awbid"].ToString()));
            for (int i = 0; i < dtPieces.Rows.Count; i++)
            {
                PieceCargoItem tempPiece = new PieceCargoItem(dtPieces.Rows[i]);
                //tempPiece.ButtonClick += new EventHandler(Item_OnMoreClick);
                smoothListBoxMainList.AddItem(tempPiece);
            }

           





            this.TitleText = string.Format("CargoAccept - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
         
            Cursor.Current = Cursors.Default;

 
           

        }


    
        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoAccept - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
        }





  

  
 
 

        void MawbList_BarcodeReadNotify(string barcodeData)
        {

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {

                this.buttonForklift.Value = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_GetForkliftCounter(TaskId);

                if (this.buttonForklift.Value > 0)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into this location?", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(scanItem.Location);
                        if (locationId == 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            BarcodeEnabled = true;
                            return;
                        }


                        CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_DropAllAcceptForkliftPieces(TaskId, (int)locationId);


                        LoadControl();
                        return;
                    }
                }





            }

          

            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }

   

    }

}
