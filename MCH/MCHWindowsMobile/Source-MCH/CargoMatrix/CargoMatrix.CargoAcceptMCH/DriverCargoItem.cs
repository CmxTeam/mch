﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using System.Text;
using System.Data;
using System.IO;
namespace CargoMatrix.CargoAccept
{
    public partial class DriverCargoItem : CustomListItems.ExpandableRenderListitem<DataRow>, ISmartListItem
    {

        public event EventHandler ButtonClick;

        public DriverCargoItem(DataRow flight)
            : base(flight)
        {
            InitializeComponent();
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }


        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, EventArgs.Empty);
            }
        }

        protected override void InitializeControls()
        {
            this.SuspendLayout();


            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBrowse.Location = new System.Drawing.Point(171, 4);
            this.buttonBrowse.Name = "btnMore";
            this.buttonBrowse.Image = Resources.Skin.magnify_btn;
            this.buttonBrowse.PressedImage = Resources.Skin.magnify_btn_over;
            this.buttonBrowse.Size = new System.Drawing.Size(64, 64);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);
            this.Controls.Add(buttonBrowse);

            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.panelIndicators.Anchor = System.Windows.Forms.AnchorStyles.None;
            //this.panelIndicators.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelIndicators.Location = new System.Drawing.Point(59, 70);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(180, 16);
            //this.panelIndicators.TabIndex = 19;
            this.Controls.Add(panelIndicators);


            int f = int.Parse(ItemData["Flags"].ToString());
            this.panelIndicators.Flags = f;
            if (f == 0)
            {
                this.panelIndicators.Visible = false;
            }
            else
            {
                this.panelIndicators.Visible = true;
            }

            this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.ResumeLayout(false);
        }
        //AWBCount	TotalPieces	ReceiveLocation

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                Brush whiteBrush = new SolidBrush(Color.White);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = ItemData["Driver"].ToString();
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 145 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line2 = ItemData["Company"].ToString();
                    gOffScreen.DrawString(line2.ToString(), fnt, brush, new RectangleF(40 * hScale, 17 * vScale, 145 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line3 = string.Format("Awb: {0}   Pcs: {1}", ItemData["AWBCount"].ToString(), ItemData["TotalPieces"].ToString());
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 31 * vScale, 170 * hScale, 13 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line4 = string.Format("Loc: {0}", ItemData["ReceiveLocation"].ToString());
                    gOffScreen.DrawString(line4, fnt, brush, new RectangleF(40 * hScale, 44 * vScale, 170 * hScale, 13 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line5 = string.Format("Received by: {0}", ItemData["ReceivedBy"].ToString());
                    gOffScreen.DrawString(line5, fnt, brush, new RectangleF(40 * hScale, 57 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                     string line6 = string.Format("{0:0}%",double.Parse( ItemData["Progress"].ToString()));
                    gOffScreen.DrawString(line6, fnt, redBrush, new RectangleF(4 * hScale, 30 * vScale, 145 * hScale, 14 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.Clipboard;
                switch (int.Parse(ItemData["StatusId"].ToString()))
                {
                    case 5:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case 3:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case 2:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }
                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);



              //Image logo =  Base64ToImage(ItemData["DriverImage"].ToString());
              //if (logo != null)
              //{
              //    gOffScreen.DrawImage(logo, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(34 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
              //}


              Image driver = Base64ToImage(ItemData["DriverImage"].ToString());
              //if (driver != null)
              //{
              //    gOffScreen.DrawImage(driver, new Rectangle((int)(171 * hScale), (int)(4 * vScale), (int)(64 * hScale), (int)(64 * vScale)), new Rectangle(0, 0, driver.Width, driver.Height), GraphicsUnit.Pixel);
 
              //}

              if (driver != null)
              {
                  this.buttonBrowse.Image = driver;
                  this.buttonBrowse.PressedImage = driver;
              }

                //this.panelIndicators.Flags = (int)ItemData.Flag;

                //this.panelIndicators.Visible = true;
 
                //using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                //{
                //    Rectangle topRect = new Rectangle((int)(190 * hScale), (int)(4 * vScale), (int)(47 * hScale), (int)((47 * hScale) / 2));
                //    Rectangle botRect = new Rectangle((int)(190 * hScale), (int)(18 * vScale), (int)(47 * hScale), (int)((47 * hScale) / 2));
                //    StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };

                //    gOffScreen.DrawString(string.Format("{0:ddd}", ItemData.CutOfTime), font, new SolidBrush(Color.White), topRect, sf);
                //    gOffScreen.DrawString(string.Format("{0:HH:mm}", ItemData.CutOfTime), font, new SolidBrush(Color.Black), botRect, sf);

                //}

                StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };


 

 

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }

            }
        }

        #region ISmartListItem Members



        public Image Base64ToImage(string base64String)
        {
            try
            {
                Image image;
                byte[] imageBytes = Convert.FromBase64String(base64String.Split(',')[1]);
                using (MemoryStream ms = new MemoryStream(imageBytes))
                {
                    image = new Bitmap(ms);
                }
                return image;
            }
            catch
            {
                return null;
            }


        }


        public bool Contains(string text)
        {

            StringBuilder destinations = new StringBuilder(string.Empty);


            string content = ItemData["driver"].ToString() + ItemData["Company"].ToString() + ItemData["Awblist"].ToString();

            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
