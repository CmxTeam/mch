﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CustomListItems;
using CargoMatrix.Communication.CargoMeasurements;

namespace CargoMatrix.CargoMeasurements
{
    public partial class MetricSystemPopup : MessageListBox
    {
        MetricSystemTypeEnum metricSystemType;

        public MetricSystemTypeEnum MetricSystemType
        {
            get { return metricSystemType; }
            set { metricSystemType = value; }
        }


        public MetricSystemPopup(MetricSystemTypeEnum metricSystemType)
        {
            InitializeComponent();

            this.HeaderText = "Select metric system";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = true;
            this.OkEnabled = false;
            TopPanel = false;

            this.metricSystemType = metricSystemType;
            
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MetricSystemPopup_ListItemClicked);

            this.LoadListEvent += new LoadSmoothList(MetricSystemPopup_LoadListEvent);
        }

        void MetricSystemPopup_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            var item = new SmoothListbox.ListItems.TwoLineListItem<MetricSystemTypeEnum>("Imperial", CargoMatrix.Resources.Skin.Menu, "USA", MetricSystemTypeEnum.Imperial);
            this.AddItem(item);

            item = new SmoothListbox.ListItems.TwoLineListItem<MetricSystemTypeEnum>("Metric", CargoMatrix.Resources.Skin.Menu, string.Empty, MetricSystemTypeEnum.Metric);
            this.AddItem(item);
            
            Cursor.Current = Cursors.Default;
        }

        void MetricSystemPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var clickedItem = (SmoothListbox.ListItems.TwoLineListItem<MetricSystemTypeEnum>)listItem;

            this.metricSystemType = clickedItem.ItemData;
        }
    }
}