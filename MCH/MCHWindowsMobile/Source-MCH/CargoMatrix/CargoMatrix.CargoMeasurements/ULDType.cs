﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.CargoMeasurements
{
    internal class ULDType : IULDType
    {
        #region IULDType Members

        public string ULDName
        {
            get;
            set;
        }

        public int TypeID
        {
            get;
            set;
        }

        public bool IsLoose
        {
            get;
            set;
        }

        #endregion

        public ULDType() { }

        public ULDType(IULDType uldType)
        {
            this.ULDName = uldType.ULDName;
            this.TypeID = uldType.TypeID;
            this.IsLoose = uldType.IsLoose;
        }
    }
}
