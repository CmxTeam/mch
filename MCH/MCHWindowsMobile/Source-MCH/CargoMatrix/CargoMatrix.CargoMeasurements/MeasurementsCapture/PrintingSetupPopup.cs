﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.CargoMeasurements;
using SmoothListbox.ListItems;
using CustomUtilities;

namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    public partial class PrintingSetupPopup : MessageListBox
    {
        public PrintingType PrintingMethod { get; private set; }

        public PrintingSetupPopup(PrintingType printingMethod)
        {
            this.PrintingMethod = printingMethod;

            InitializeComponent();

            this.TopPanel = true;
            this.ButtonVisible = false;
            this.HeaderText = "Select printing type";
            this.HeaderText2 = this.PrintingMethod == PrintingType.Automatic ? "Automatic" : "Manual";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = true;
            this.OkEnabled = false;
     
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DevicesPopup_ListItemClicked);
            this.LoadListEvent += new LoadSmoothList(DevicesPopup_LoadListEvent);
        }

        void DevicesPopup_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            Control[] list = new Control[]
            {
                new StandardListItem<PrintingType>("Manual", null, PrintingType.Manual),
                new StandardListItem<PrintingType>("Automatic", null, PrintingType.Automatic)
            };

            this.AddItems(list);
        }

        void DevicesPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var item = (StandardListItem<PrintingType>)listItem;

            if (item.ItemData == PrintingType.Automatic)
            {
                CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter printer = new CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter();
                bool result = PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.MML, ref printer);

                if (result == true)
                {
                    this.PrintingMethod = item.ItemData;
                }
            }
            else
            {
                this.PrintingMethod = item.ItemData;
            }
        }
    }
}