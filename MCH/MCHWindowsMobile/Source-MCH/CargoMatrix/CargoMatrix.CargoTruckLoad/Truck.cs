﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXBarcode;
using CMXExtensions;
using CargoMatrix.Communication.CargoTruckLoad;
using CustomListItems.SelectHighlightListItem;
using CustomListItems;


namespace CargoMatrix.CargoTruckLoad
{
    public partial class Truck : CargoMatrix.Utilities.MessageListBox
    {
        private IMasterBillItem masterBill;
        CargoMatrix.Communication.WSLoadConsol.LocationItem truck;

        private TruckActionTypeEnum lastActionType;

        #region Properties
        public TruckActionTypeEnum LastActionType
        {
            get { return lastActionType; }
        }

        #endregion

        public Truck(IMasterBillItem masterBill, CargoMatrix.Communication.WSLoadConsol.LocationItem truck)
        {
            InitializeComponent();

            this.masterBill = masterBill;
            this.truck = truck;

            this.HeaderText = "Truck View";
            this.HeaderText2 = truck.Location;
            this.IsSelectable = false;
           
            this.lastActionType = TruckActionTypeEnum.Close;

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Truck_LoadListEvent);
        }

        #region Events
        void Truck_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent();
        }

        void combo_ButtonClick(object sender, EventArgs e)
        {
            var forklift = Forklift.Instance;
            
            if (sender is SelectHighlightItem<LoadTruckULD>)
            {
                var combo = (SelectHighlightItem<LoadTruckULD>)sender;

                string message = string.Format("Are you sure you want to remove {0} from truck?", combo.CustomData.ULDNo);

                var dialodResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

                if (dialodResult != DialogResult.OK) return;

                var status = CargoMatrix.Communication.LoadConsol.Instance.RemoveUldFromTruck(combo.CustomData.ID, this.truck.LocationId, this.masterBill.TaskId, forklift.ID);

                if (status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                    return;
                }
                else
                {
                    this.RemoveItem(combo);
                }

            }
            else
            {
                if (sender is SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)
                {
                    var combo = (SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)sender;

                    string message = string.Format("Are you sure you want to remove {0} from truck?", combo.CustomData.HousebillNumber);

                    var dialodResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

                    if (dialodResult != DialogResult.OK) return;

                    foreach(var piece in combo.CustomData.Pieces)
                    {
                        piece.HouseBillId = combo.CustomData.HousebillId;
                        piece.TaskId = this.masterBill.TaskId;
                    }

                    var status = CargoMatrix.Communication.LoadConsol.Instance.RemovePiecesFromTruck(this.truck.LocationId, combo.CustomData.Pieces, this.masterBill.TaskId, forklift.ID);

                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                        return;
                    }
                    else
                    {
                        this.RemoveItem(combo);
                    }
                }
            }

            this.lastActionType = TruckActionTypeEnum.Remove;

            if (this.smoothListBoxBase1.Items.Count <= 0)
            {
                this.Close();
            }
        }


        void combo_SubItemButtonClick(object sender, SubItemButtonClickEventArgs e)
        {
            var forklift = Forklift.Instance;

            var combo = (SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)sender;

            if (combo == null) return;

            var piece = (from item in combo.CustomData.Pieces
                         where item.PieceId == e.SubItem.ID
                         select item).FirstOrDefault();

            if (piece == null) return;

            piece.HouseBillId = combo.CustomData.HousebillId;
            piece.TaskId = this.masterBill.TaskId;
          
            string message = string.Format("Are you sure you want to remove pieces {0} from truck?", piece.PieceNumber);

            var dialodResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

            if (dialodResult != DialogResult.OK) return;

            var pieces = new CargoMatrix.Communication.WSLoadConsol.PieceItem[] { piece };

            var status = CargoMatrix.Communication.LoadConsol.Instance.RemovePiecesFromTruck(this.truck.LocationId, pieces, this.masterBill.TaskId, forklift.ID);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                return;
            }

            combo.RemoveItem(e.SubItem);

            if(combo.Items.Count() <= 0)
            {
                this.smoothListBoxBase1.RemoveItem(combo);
            }

            if(this.smoothListBoxBase1.Items.Count <= 0)
            {
                this.Close();
            }

        }


        #endregion

        #region Helpe Maethods
        private void PopulateContent()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            this.smoothListBoxBase1.RemoveAll();
            
            var forklift = Forklift.Instance;
            if (forklift.IsLoaded == false) return;

            LoadTruckULD[] ulds;
            var status = CargoMatrix.Communication.LoadConsol.Instance.GetLoadTruckContent(this.masterBill.MasterBillId, this.masterBill.TaskId, this.truck.LocationId, forklift.ID, out ulds);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                Cursor.Current = Cursors.Default;

                return;
            }

            var nonLooses = from item in ulds
                         where item.IsLoose() == false
                         select item;

            this.PopulateNonLooses(nonLooses);

            var looses = from item in ulds
                            where item.IsLoose() == true
                            select item;

            this.PopulateLooses(looses);

            this.OkEnabled = false;

            Cursor.Current = Cursors.Default;

        }

        private void PopulateLooses(IEnumerable<LoadTruckULD> ulds)
        {
            foreach (var uld in ulds)
            {
                foreach (var houseBill in uld.HouseBills)
                {
                    var housebillIcon = houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode;

                    var combo = new SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>(houseBill, uld.ID, housebillIcon);

                    combo.IsReadonly = false;
                    combo.TitleLine = string.Format("{0}-{1}-{2}", houseBill.Origin, houseBill.HousebillNumber, houseBill.Destination);
                    combo.Expandable = houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? true : false;
                    combo.IsSelectable = this.IsSelectable;
                    combo.ButtonClick += new EventHandler(combo_ButtonClick);
                    combo.SubItemButtonClick += new EventHandler<SubItemButtonClickEventArgs>(combo_SubItemButtonClick);
                    combo.DescriptionLine = string.Format("PCS: {0}", houseBill.Pieces.Length);

                    if (houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                    {
                        List<ExpenListItemData> items = new List<ExpenListItemData>();

                        foreach (var piece in houseBill.Pieces)
                        {
                            ExpenListItemData itemData = new ExpenListItemData(piece.PieceId,
                                                                       string.Format("Piece {0}", piece.PieceNumber),
                                                                       string.Empty);
                            itemData.IsReadonly = false;
                            itemData.IsSelectable = false;
                            itemData.IsChecked = true;
                            items.Add(itemData);
                        }
                        combo.AddExpandItemsRange(items);

                        combo.MaxItemsSelectHighlightCount = items.Count;
                    }


                    smoothListBoxBase1.AddItem(combo);
                }
            }

        }

        private void PopulateNonLooses(IEnumerable<LoadTruckULD> ulds)
        {
            var icon = CargoMatrix.Resources.Skin.Freight_Car;

            
            foreach (var uld in ulds)
            {
                var combo = new SelectHighlightItem<LoadTruckULD>(uld, uld.ID, icon);
                combo.IsReadonly = false;
                combo.TitleLine = uld.ULDNo;
                combo.Expandable = false;
                combo.DescriptionLine = string.Format("PCs: {0}", uld.Pieces);
                combo.ButtonClick += new EventHandler(combo_ButtonClick);
                combo.MaxItemsSelectHighlightCount = 1;

                smoothListBoxBase1.AddItem(combo);
            }
        }

       #endregion

    }
}