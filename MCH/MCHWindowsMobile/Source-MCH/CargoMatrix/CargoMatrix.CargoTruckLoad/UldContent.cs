﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CMXBarcode;
using CustomListItems.SelectHighlightListItem;
using CargoMatrix.Communication.DTO;


namespace CargoMatrix.CargoTruckLoad
{
    public partial class UldContent : CargoMatrix.Utilities.MessageListBox
    {
        IMasterBillItem masterBill;
        IULD uld;
        CargoMatrix.UI.BarcodeReader barcode;
        UldContentViewAction viewAction;
        CargoMatrix.Communication.CargoTruckLoad.UldList uldList;
        bool previewOnly;

        public CargoMatrix.Communication.CargoTruckLoad.UldList UldList
        {
            get { return uldList; }
        }


        public UldContentViewAction ViewAction
        {
            get { return viewAction; }
        }


        public UldContent(IMasterBillItem masterBill, IULD uld, bool previewOnly)
        {
            InitializeComponent();

            this.masterBill = masterBill;
            this.uld = uld;
            this.previewOnly = previewOnly;

            this.MultiSelectListEnabled = true;
            this.OneTouchSelection = false;
            this.OkEnabled = false;

            this.viewAction = UldContentViewAction.Close;

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(UldContent_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UldContent_ListItemClicked);

            this.barcode = new CargoMatrix.UI.BarcodeReader();

            this.OKClicked += new EventHandler(UldContent_OKClicked);

            if (previewOnly == true)
            {
                this.IsSelectable = false;
                return;
            }

            this.IsSelectable = true;
            
            this.barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcode_BarcodeReadNotify);
            
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.barcode.StopRead();

            base.OnClosing(e);
        }
       
        void UldContent_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            SelectHighlightItem<HouseBillItem> combo = (SelectHighlightItem<HouseBillItem>)listItem;
            var houseBill = combo.CustomData;

            if (houseBill.ScanMode != ScanModes.Count)
            {
                combo.HighlightAll();

                this.EnableOKButon();

                return;
            }

            if (isSelected == false)
            {
                combo.HighlightAll(false);
            }
            else
            {
                combo.HighlightItemsByUserInput();

            }

            this.EnableOKButon();
        }

        void barcode_BarcodeReadNotify(string barcodeData)
        {
            this.barcode.StopRead();
            
            ScanObject scanObject = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanObject.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    this.Scan(scanObject.HouseBillNumber, scanObject.PieceNumber);

                    break;

                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongBarcodeType, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    break;
            }

            this.barcode.StartRead();
        }

        void UldContent_OKClicked(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            var forklift = Forklift.Instance;

            var pieceItems = new List<CargoMatrix.Communication.WSLoadConsol.PieceItem>();

            var scannedItems = from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<HouseBillItem>>()
                               where item.SelectState != SelectStateEnum.NotSelected
                               select item;

            if (scannedItems.Count() <= 0) return;

            foreach (var item in scannedItems)
            {
                var houseBill = item.CustomData;

                if(houseBill.ScanMode == ScanModes.Piece)
                {
                    IEnumerable<int> ids = from itm in item.SelectedItems
                                           select itm.ID;

                    foreach (var id in ids)
                    {
                        var piece = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                        piece.HouseBillId = houseBill.HousebillId;
                        piece.PieceId = id;
                        piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece;
                        piece.TaskId = this.masterBill.TaskId;

                        pieceItems.Add(piece);
                    }
                }
                else
                {
                    var piece = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                    piece.PieceId = item.ItemsHeighlightCount;
                    piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Count;
                    piece.TaskId = this.masterBill.TaskId;
                    piece.HouseBillId = houseBill.HousebillId;
                    pieceItems.Add(piece);
                }
            }

            this.uldList = CargoMatrix.Communication.LoadConsol.Instance.ScanPiecesIntoLoadTruckForkLift(pieceItems.ToArray(), this.masterBill.MasterBillId, this.masterBill.TaskId, forklift.ID);

            Cursor.Current = Cursors.Default;

            if(uldList.TransactionStatus.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(uldList.TransactionStatus.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                this.viewAction = UldContentViewAction.Error;
            }
            else
            {
                this.viewAction = UldContentViewAction.Scan;
            }
        }

        void UldContent_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent();

            this.barcode.StartRead();
        }

        private void PopulateContent()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            Forklift forklift = Forklift.Instance;
            if (forklift.IsLoaded == false) return;

            var scanStatus = this.previewOnly == true ? CargoMatrix.Communication.WSLoadConsol.ScanStatuses.All : CargoMatrix.Communication.WSLoadConsol.ScanStatuses.NotScanned;

            var houseBills = CargoMatrix.Communication.LoadConsol.Instance.GetLoadTruckUldContent(this.uld.ID, forklift.ID, scanStatus);
                                                           

            foreach (var houseBill in houseBills)
            {
                var icon = houseBill.ScanMode == ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode;

                var combo = new SelectHighlightItem<HouseBillItem>(houseBill, houseBill.HousebillId, icon);

                List<ExpenListItemData> items = new List<ExpenListItemData>();

                if (houseBill.ScanMode == ScanModes.Piece)
                {
                    foreach (var piece in houseBill.Pieces)
                    {
                        ExpenListItemData itemData = new ExpenListItemData(piece.PieceId,
                                                                           string.Format("Piece {0}", piece.PieceNumber),
                                                                           piece.Location);
                        itemData.IsReadonly = true;
                        itemData.IsSelectable = piece.LocationId != forklift.ID && this.IsSelectable;
                        itemData.IsChecked = piece.LocationId == forklift.ID;
                        items.Add(itemData);
                    }
                }

                int totalPieces = houseBill.Pieces.Length;

                combo.SubItemSelected += new EventHandler<ExpandItemEventArgs>(combo_SubItemSelected);
                combo.IsSelectable = this.IsSelectable;
                combo.Expandable = houseBill.ScanMode == ScanModes.Piece;
                combo.IsReadonly = true;
                combo.TitleLine = houseBill.HousebillNumber;
                combo.DescriptionLine = this.GetExpandItemDescription(0, houseBill.Pieces.Length);
                combo.AddExpandItemsRange(items);
                combo.ManualEnterDialogHeader = string.Format("{0}-{1}-{2}", houseBill.Origin, houseBill.HousebillNumber, houseBill.Destination);
                combo.ManualEnterDialogSubHeader = "Enter number of pieces";
                combo.DescriptionLineUpdateText = () =>
                    {
                        return this.GetExpandItemDescription(combo.ItemsHeighlightCount, combo.CustomData.Pieces.Length);
                    };

                combo.MaxItemsSelectHighlightCount = combo.CustomData.Pieces.Length;

                this.smoothListBoxBase1.AddItem(combo);
            }

            this.EnableOKButon();

            Cursor.Current = Cursors.Default;

        }

        void combo_SubItemSelected(object sender, ExpandItemEventArgs e)
        {
            this.EnableOKButon();
        }

        private void EnableOKButon()
        {
            var scannedItem = this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<HouseBillItem>>().
                                FirstOrDefault(item => item.SelectState != SelectStateEnum.NotSelected);

            this.OkEnabled = scannedItem != null;
        }

        public string GetExpandItemDescription(int selectedPieces, int totalPieces)
        {
            return string.Format("PCS: {0} of {1}", selectedPieces, totalPieces);
        }

        private void Scan(string houseBillNumber, int pieceNumber)
        {
            CargoMatrix.Communication.DTO.HouseBillItem housebill;
            var status = CargoMatrix.Communication.LoadConsol.Instance.CanScanIntoLoadTruckForkLift(this.masterBill.MasterBillNumber, this.masterBill.CarrierNumber, Forklift.Instance.ID, houseBillNumber, pieceNumber, out housebill);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            
            
            var listItem = (
                                from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<HouseBillItem>>()
                                where string.Compare(item.CustomData.HousebillNumber, houseBillNumber) == 0
                                select item
                           ).FirstOrDefault();

            if (listItem == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtHouseBillNotInList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            var houseBill = listItem.CustomData;

            var piecesIds = from p in houseBill.Pieces
                            where p.PieceNumber == pieceNumber
                            select p.PieceId;


            if (houseBill.ScanMode == ScanModes.Piece)
            {
                if (piecesIds.Count() <= 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongPieceNumber, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                listItem.HighlightPieces(piecesIds, true);
            }
            else
            {
                listItem.HighlightItemsByUserInput();
                
            }

            if (listItem.ItemsHeighlightCount == listItem.MaxItemsSelectHighlightCount)
            {
                listItem.SelectedChanged(true);
            }

            this.EnableOKButon();
        }

        public enum UldContentViewAction
        {
            Close,
            Scan,
            Error
        }
    }


    
}