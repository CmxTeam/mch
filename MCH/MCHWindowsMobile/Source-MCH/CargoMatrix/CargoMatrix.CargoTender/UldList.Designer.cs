﻿namespace CargoMatrix.CargoTender
{
    partial class UldList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.headerItem = new CustomListItems.HeaderItem();

            this.panelHeader2.SuspendLayout();

            //
            // headerItem
            //

            this.headerItem.Location = new System.Drawing.Point(0, 0);
            this.headerItem.Size = new System.Drawing.Size(240, 48);
            this.headerItem.Height = 49;
            this.headerItem.Label1 = "JFK-180-27497212-ICN";
            this.headerItem.Label2 = "HBs :    Wt :";
            this.headerItem.Label3 = "SCAN/SELECT ULD";
            this.headerItem.ButtonImage = CargoMatrix.Resources.Skin.damage;
            this.headerItem.ButtonPressedImage = CargoMatrix.Resources.Skin.damage_over;


            this.panelHeader2.Controls.Add(this.headerItem);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });

            this.panelHeader2.Height = 49;

            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.panelHeader2.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private CustomListItems.HeaderItem headerItem;
    }
}
