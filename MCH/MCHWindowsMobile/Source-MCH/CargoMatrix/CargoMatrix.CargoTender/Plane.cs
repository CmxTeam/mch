﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CustomListItems.SelectHighlightListItem;
using CustomListItems;

namespace CargoMatrix.CargoTender
{
    public partial class Plane : CargoMatrix.Utilities.MessageListBox
    {
        private static Plane instance;
        private int iD;

        #region Properties
        public int ID
        {
            get { return iD; }
        }

        public static Plane Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Plane();
                }

                return instance;
            }
        } 
        #endregion

        #region Constructors
        private Plane()
        {
            InitializeComponent();
            this.HeaderText = "Plane View";
            this.HeaderText2 = "";
            this.IsSelectable = false;
            this.OkEnabled = false;
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;

            this.iD = 0;
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Forklift_LoadListEvent);
        } 
        #endregion

        #region Handlers
      
        void Forklift_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent();
        } 
        
        #endregion

        #region Additional Methods
        private void PopulateContent()
        {
            var combo = new SelectHighlightItem(1, CargoMatrix.Resources.Skin.Airplane24x24);

            
            combo.IsReadonly = false;
            combo.TitleLine = string.Format("{0}-{1}-{2}", "ORG", "1234567", "DES");
            combo.Expandable = true;
            combo.IsSelectable = false;
            combo.DescriptionLine = string.Format("ULDs: {0}", 2);
            combo.ButtonClick += new EventHandler(combo_ButtonClick);

            List<ExpenListItemData> items = new List<ExpenListItemData>();

            ExpenListItemData itemData = new ExpenListItemData(1, "1234567", "Pieces : 7");
            itemData.IsReadonly = true;
            itemData.IsSelectable = false;
            itemData.IsChecked = true;
            items.Add(itemData);

            itemData = new ExpenListItemData(1, "7654321", "Pieces : 3");
            itemData.IsReadonly = true;
            itemData.IsSelectable = false;
            itemData.IsChecked = true;
            items.Add(itemData);

            combo.AddExpandItemsRange(items);

            combo.MaxItemsSelectHighlightCount = items.Count;



            smoothListBoxBase1.AddItem(combo);       
        }

        void combo_ButtonClick(object sender, EventArgs e)
        {
            var combo = (SelectHighlightItem)sender;

            if(combo == null) return;

            var dialodResult = CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to remove ORG-1234567-DES from plane", "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

            if (dialodResult != DialogResult.OK) return;

            this.RemoveItem(combo);
        } 
        #endregion

    }
}