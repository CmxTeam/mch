﻿namespace CargoMatrix.CargoTender
{
    partial class SignatureCaptureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
      
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignatureCaptureForm));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.headerItem = new CustomListItems.HeaderItem();
            this.pcxHeader = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.signature = new CustomUtilities.Signature();
            this.pcxFooter = new System.Windows.Forms.PictureBox();
            this.pnlMain.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlHeader);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.signature);
            this.pnlMain.Controls.Add(this.pcxFooter);
            this.pnlMain.Location = new System.Drawing.Point(3, 14);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(234, 237);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.headerItem);
            this.pnlHeader.Controls.Add(this.pcxHeader);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(234, 69);
            // 
            // headerItem
            // 
            this.headerItem.Label1 = "ULDs :";
            this.headerItem.Label2 = "Weight :";
            this.headerItem.Label3 = "";
            this.headerItem.Location = new System.Drawing.Point(0, 20);
            this.headerItem.Name = "headerItem";
            this.headerItem.Size = new System.Drawing.Size(234, 49);
            this.headerItem.TabIndex = 1;
            // 
            // pcxHeader
            // 
            this.pcxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcxHeader.Image = ((System.Drawing.Image)(resources.GetObject("pcxHeader.Image")));
            this.pcxHeader.Location = new System.Drawing.Point(0, 0);
            this.pcxHeader.Name = "pcxHeader";
            this.pcxHeader.Size = new System.Drawing.Size(234, 20);
            this.pcxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pcxHeader_Paint);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(7, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 17);
            this.label1.Text = "Signature required";
            // 
            // signature
            // 
            this.signature.BackgroundBitmap = ((System.Drawing.Bitmap)(resources.GetObject("signature.BackgroundBitmap")));
            this.signature.Location = new System.Drawing.Point(3, 107);
            this.signature.Name = "signature";
            this.signature.Size = new System.Drawing.Size(228, 79);
            this.signature.TabIndex = 5;
            this.signature.Text = "signature";
            // 
            // pcxFooter
            // 
            this.pcxFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxFooter.Image = ((System.Drawing.Image)(resources.GetObject("pcxFooter.Image")));
            this.pcxFooter.Location = new System.Drawing.Point(0, 190);
            this.pcxFooter.Name = "pcxFooter";
            this.pcxFooter.Size = new System.Drawing.Size(234, 47);
            this.pcxFooter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;


            this.InitializeFields();

            // 
            // SignatureCaptureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.pnlMain);
            this.Name = "SignatureCaptureForm";
            this.Text = "SignatureCaptureForm";
            this.pnlMain.ResumeLayout(false);
            this.pnlHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.PictureBox pcxFooter;
        private System.Windows.Forms.PictureBox pcxHeader;
        private CustomUtilities.Signature signature;
        private System.Windows.Forms.Label label1;
        private CustomListItems.HeaderItem headerItem;
        private System.Windows.Forms.Panel pnlHeader;

    }
}