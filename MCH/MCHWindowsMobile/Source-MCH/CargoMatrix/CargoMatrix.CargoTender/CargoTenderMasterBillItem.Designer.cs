﻿namespace CargoMatrix.CargoTender
{
    partial class CargoTenderMasterBillItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            btnDamageCapture = new CargoMatrix.UI.CMXPictureButton();
            labelLine2Hdr = new System.Windows.Forms.Label();
            labelLine3Hdr = new System.Windows.Forms.Label();
            labelLine4Hdr = new System.Windows.Forms.Label();
            labelLine5Hdr = new System.Windows.Forms.Label();
            labelLine4 = new System.Windows.Forms.Label();
            labelLine5 = new System.Windows.Forms.Label();
            labelLine6 = new System.Windows.Forms.Label();
            labelLine6Hdr = new System.Windows.Forms.Label();


            // 
            // labelLine2Hdr
            // 
            this.labelLine2Hdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine2Hdr.Location = new System.Drawing.Point(41, 23);
            this.labelLine2Hdr.Name = "labelLine4";
            this.labelLine2Hdr.Size = new System.Drawing.Size(40, 13);
            this.labelLine2Hdr.Text = "Hbs :";

            this.labelLine2.Location = new System.Drawing.Point(73, 23);
            this.labelLine2.Size = new System.Drawing.Size(37, 13);


            // labelLine3Hdr
            // 
            this.labelLine3Hdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine3Hdr.Location = new System.Drawing.Point(110, 23);
            this.labelLine3Hdr.Name = "labelLine3Hdr";
            this.labelLine3Hdr.Size = new System.Drawing.Size(43, 13);
            this.labelLine3Hdr.Text = "ULDs :";

            this.labelLine3.Location = new System.Drawing.Point(150, 23);
            this.labelLine3.Size = new System.Drawing.Size(43, 13);


            // labelLine4Hdr
            // 
            this.labelLine4Hdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine4Hdr.Location = new System.Drawing.Point(41, 41);
            this.labelLine4Hdr.Name = "labelLine4Hdr";
            this.labelLine4Hdr.Size = new System.Drawing.Size(30, 13);
            this.labelLine4Hdr.Text = "Pcs :";

            // 
            // labelLine4
            // 
            this.labelLine4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine4.Location = new System.Drawing.Point(73, 41);
            this.labelLine4.Name = "labelLine4";
            this.labelLine4.Size = new System.Drawing.Size(37, 13);


            // labelLine5Hdr
            // 
            this.labelLine5Hdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine5Hdr.Location = new System.Drawing.Point(110, 41);
            this.labelLine5Hdr.Name = "labelLine5Hdr";
            this.labelLine5Hdr.Size = new System.Drawing.Size(35, 13);
            this.labelLine5Hdr.Text = "Wht :";

            // 
            // labelLine5
            // 
            this.labelLine5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine5.Location = new System.Drawing.Point(145, 41);
            this.labelLine5.Name = "labelLine4";
            this.labelLine5.Size = new System.Drawing.Size(63, 13);


            // labelLine6Hdr
            // 
            this.labelLine6Hdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLine6Hdr.Location = new System.Drawing.Point(41, 59);
            this.labelLine6Hdr.Name = "labelLine6Hdr";
            this.labelLine6Hdr.Size = new System.Drawing.Size(60, 13);
            this.labelLine6Hdr.Text = "Flight No :";

            // 
            // labelLine6
            // 
            this.labelLine6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine6.Location = new System.Drawing.Point(102, 59);
            this.labelLine6.Name = "labelLine6";
            this.labelLine6.Size = new System.Drawing.Size(43, 13);
            
            
            //
            //btnDamageCapture
            //
            this.btnDamageCapture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDamageCapture.Location = new System.Drawing.Point(202, 47);
            this.btnDamageCapture.Name = "cmxPictureButton1";
            this.btnDamageCapture.Image = CargoMatrix.Resources.Skin.damage;
            this.btnDamageCapture.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.btnDamageCapture.Size = new System.Drawing.Size(32, 32);
            this.btnDamageCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDamageCapture.TransparentColor = System.Drawing.Color.White;
            this.btnDamageCapture.Click += new System.EventHandler(btnDamageCapture_Click);

            this.panelIndicators.Location = new System.Drawing.Point(this.panelIndicators.Location.X, 108 - 16 - 8);

            this.Controls.Add(this.labelLine2Hdr);
            this.Controls.Add(this.labelLine3Hdr);
            this.Controls.Add(this.labelLine4Hdr);
            this.Controls.Add(this.labelLine5Hdr);
            this.Controls.Add(this.labelLine6Hdr);
            this.Controls.Add(this.labelLine4);
            this.Controls.Add(this.labelLine5);
            this.Controls.Add(this.labelLine6);
            this.Controls.Add(this.btnDamageCapture);
            this.btnDamageCapture.BringToFront();

            this.Size = new System.Drawing.Size(240, 102);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);

            this.ResumeLayout(false);
        }

        #endregion

        private CargoMatrix.UI.CMXPictureButton btnDamageCapture;
        protected System.Windows.Forms.Label labelLine2Hdr;
        protected System.Windows.Forms.Label labelLine3Hdr;
        protected System.Windows.Forms.Label labelLine4Hdr;
        protected System.Windows.Forms.Label labelLine5Hdr;
        protected System.Windows.Forms.Label labelLine6Hdr;
        protected System.Windows.Forms.Label labelLine4;
        protected System.Windows.Forms.Label labelLine5;
        protected System.Windows.Forms.Label labelLine6;
    }
}
