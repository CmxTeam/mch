﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.CargoTender
{
    public static class CommonMethods
    {
        public static int? GetPiecesCountFromUser(string header, string subHeder, int piecesCount, bool isReadonly)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = header;
            cntMsg.LabelReference = subHeder;
            cntMsg.LabelDescription = "Enter pieces count";
            cntMsg.PieceCount = piecesCount;
            cntMsg.Readonly = isReadonly;

            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }


        public static void ShowPlane()
        {
            Plane plane = Plane.Instance;

            plane.ShowDialog();
        }
    }
}
