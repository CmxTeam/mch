﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.Messageing;

namespace CargoMatrix.Messageing
{
    public class MessageManager
    {
        private static MessageManager instance;
        private Timer timerMessageCheck;

        public static MessageManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new MessageManager();
                return instance;
            }
        }

        /// <summary>
        /// Message check interval in miliseconds
        /// </summary>
        public int Interval
        {
            get { return timerMessageCheck.Interval; }
            set { timerMessageCheck.Interval = value; }
        }

        private MessageManager()
        {
            timerMessageCheck = new Timer();
            timerMessageCheck.Tick += new EventHandler(timerMessageCheck_Tick);
            timerMessageCheck.Interval = 10000;
            CargoMatrix.UI.CMXAnimationmanager.GetParent().MessagesIndicatorClick += new EventHandler(MessageManager_MessagesIndicatorClick);

        }

        void MessageManager_MessagesIndicatorClick(object sender, EventArgs e)
        {
            DipslayMessages();
        }

        void timerMessageCheck_Tick(object sender, EventArgs e)
        {
            var result = CargoMatrix.Communication.Messageing.Messageing.Instance.GetUnreadMessagesCount();
            if (result.UnreadMsgsCount > 0)
            {
                CargoMatrix.UI.CMXAnimationmanager.GetParent().NewMessage  = true;
                CargoMatrix.UI.CMXAnimationmanager.GetParent().UnreadMessagesCount = result.UnreadMsgsCount;
            }
            if (result.UrgentMsgExists)
            {
                List<IMessage> urgMessages = CargoMatrix.Communication.Messageing.Messageing.Instance.GetUrgentMessages().ToList<IMessage>();
                //foreach (var urgMessage in )
                //{
                ViewMessagePopup msgPopup = new ViewMessagePopup(urgMessages,0);
                    msgPopup.ShowDialog();
                //}
            }
        }



        public Message[] GetUrgentMessages()
        {
            throw new NotImplementedException();
        }

        public void MarkAsRead(int msgId)
        {
            CargoMatrix.Communication.Messageing.Messageing.Instance.MarkAsRead(msgId);
        }

        public void SendMessage(Message msg)
        {
            CargoMatrix.Communication.Messageing.Messageing.Instance.QueueMessage(msg);
        }

        public void DeleteMessage(int messageId)
        {
            CargoMatrix.Communication.Messageing.Messageing.Instance.DeleteMessage(messageId);
        }

        public void Start()
        {
            this.timerMessageCheck.Enabled = true;
        }

        public void Stop()
        {
            this.timerMessageCheck.Enabled = false;
            CargoMatrix.UI.CMXAnimationmanager.GetParent().NewMessage = false;
        }

        public void DipslayMessages()
        {
            MessagesList messages = new MessagesList();
            messages.ShowDialog();
            CargoMatrix.UI.CMXAnimationmanager.GetParent().NewMessage = false;
        }

        public IMessage[] GetRecentMessages()
        {
            return CargoMatrix.Communication.Messageing.Messageing.Instance.GetRecentMessages();
        }
    }
}
