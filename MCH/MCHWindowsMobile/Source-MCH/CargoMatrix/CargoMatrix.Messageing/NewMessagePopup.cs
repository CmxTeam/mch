﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.Messageing;
using CargoMatrix.Communication.WSLoadConsol;
using SmoothListbox.ListItems;

namespace CargoMatrix.Messageing
{
    public partial class NewMessagePopup : CMXMessageBoxPopup
    {
        private int[] receivers = new int[0];

        public int ReplyMessageId
        {
            get;
            set;
        }

        public int[] ReceipientIds
        {
            set { receivers = value; }
        }

        public string RecipientName
        {
            set
            { labelReceivers.Text = value; }
        }

        /// <summary>
        /// display form to compse new message
        /// </summary>
        public NewMessagePopup()
        {
            InitializeComponent();
            this.Title = "New Message";
            this.buttonOk.Enabled = false;
            this.textBox1.BackColor = Color.White;
            this.textBox1.ReadOnly = false;
            this.textBox1.TextChanged += new EventHandler(textBox1_TextChanged);
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            Message ms = new Message()
            {
                Body = textBox1.Text,
                DateCreated = DateTime.Now,
                ReceiverIds = receivers,
                SenderId = CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID

            };
            if (this.ReplyMessageId == 0)
                ms.ResponseTo = ReplyMessageId;
            MessageManager.Instance.SendMessage(ms);
            base.buttonOk_Click(sender, e);
        }

        void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (receivers.Length > 0 && textBox1.Text.Length > 0)
                buttonOk.Enabled = true;
            else
                buttonOk.Enabled = false;
        }

        private string FormatMessageString(IMessage msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(msg.Body);
            return sb.ToString();
        }

        void buttonReceivers_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            CustomUtilities.SearchMessageListBox usersList = new CustomUtilities.SearchMessageListBox();
            usersList.HeaderText = "Select users from list";
            usersList.MultiSelectListEnabled = true;

            foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetWarehouseUsers(0))
            {
                usersList.smoothListBoxBase1.AddItem2(new StandardListItem<UserItem>(item.UserName, CargoMatrix.Resources.Skin.worker, item));
            }

            usersList.smoothListBoxBase1.LayoutItems();
            usersList.smoothListBoxBase1.RefreshScroll();

            Cursor.Current = Cursors.Default;
            if (DialogResult.OK == usersList.ShowDialog())
            {
                receivers = (from user in usersList.SelectedItems.OfType<StandardListItem<UserItem>>()
                             select user.ItemData.UserId).ToArray<int>();

                this.labelReceivers.Text =

                String.Join(",", (from user in usersList.SelectedItems.OfType<StandardListItem<UserItem>>()
                                  select user.ItemData.UserName).ToArray<string>());

                if (textBox1.Text.Length > 0)
                    this.buttonOk.Enabled = true;
            }
        }
    }
}
