﻿namespace CargoMatrix.Messageing
{
    partial class ViewMessagePopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        protected override void InitializeChildComponents()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonDelete = new CargoMatrix.UI.CMXPictureButton();
            this.buttonPrev = new CargoMatrix.UI.CMXPictureButton();
            this.buttonNext = new CargoMatrix.UI.CMXPictureButton();
            //
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 30);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(224, 150);
            this.textBox1.TabIndex = 0;
            //
            //buttonPrev
            //
            this.buttonPrev.Location = new System.Drawing.Point(3, 146);
            this.buttonPrev.Name = "button1";
            this.buttonPrev.Size = new System.Drawing.Size(32, 37);
            this.buttonPrev.TabIndex = 0;
            this.buttonPrev.Image = global::Resources.Graphics.Skin.pic_thumb_left;
            this.buttonPrev.PressedImage = global::Resources.Graphics.Skin.pic_thumb_left_over;
            this.buttonPrev.Text = "buttonPrev";
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Click += new System.EventHandler(buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(199, 146);
            this.buttonNext.Name = "button1";
            this.buttonNext.Size = new System.Drawing.Size(32, 37);
            this.buttonNext.TabIndex = 0;
            this.buttonNext.Image = global::Resources.Graphics.Skin.pic_thumb_right;
            this.buttonNext.PressedImage = global::Resources.Graphics.Skin.pic_thumb_right_over;
            this.buttonNext.Text = "buttonNext";
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Click += new System.EventHandler(buttonNext_Click);
            //
            //ViewMessagePopup
            //
            this.panelContent.Location = new System.Drawing.Point(3, 40);
            this.panelContent.Name = "panelContent";
            //this.panelContent.Size = new System.Drawing.Size(234, 230);
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.ReadOnly = true;
            // 
            //buttonDelete
            //
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(91, 146);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(52, 37);
            this.buttonDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelete.Image = CargoMatrix.Resources.Skin.cc_trash;
            this.buttonDelete.PressedImage = CargoMatrix.Resources.Skin.cc_trash_over;
            this.buttonDelete.Click += new System.EventHandler(buttonDelete_Click);

            this.panelContent.Controls.Add(this.textBox1);
            this.panelContent.Controls.Add(this.buttonDelete);
            this.panelContent.Controls.Add(this.buttonPrev);
            this.panelContent.Controls.Add(this.buttonNext);
            this.buttonPrev.BringToFront();
            this.buttonNext.BringToFront();
            this.buttonDelete.BringToFront();
            this.Name = "ViewMessagePopup";
            this.buttonOk.Left = 37;
            this.buttonCancel.Left = 145;

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private CargoMatrix.UI.CMXPictureButton buttonDelete;
        private CargoMatrix.UI.CMXPictureButton buttonPrev;
        private CargoMatrix.UI.CMXPictureButton buttonNext;

    }
}
