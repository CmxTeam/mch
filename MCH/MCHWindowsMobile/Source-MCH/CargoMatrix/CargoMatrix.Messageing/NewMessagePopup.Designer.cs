﻿namespace CargoMatrix.Messageing
{
    partial class NewMessagePopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonReceivers = new CargoMatrix.UI.CMXPictureButton();
            this.labelReceivers = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 64);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(224, 116);
            this.textBox1.TabIndex = 0;
            textBox1.TextChanged += new System.EventHandler(textBox1_TextChanged);
            // 
            // button1
            // 
            this.buttonReceivers.Location = new System.Drawing.Point(196, 26);
            this.buttonReceivers.Name = "button1";
            this.buttonReceivers.Size = new System.Drawing.Size(32, 32);
            this.buttonReceivers.TabIndex = 0;
            this.buttonReceivers.Image = global::Resources.Graphics.Skin.add_btn;
            this.buttonReceivers.PressedImage = global::Resources.Graphics.Skin.add_btn_over;
            this.buttonReceivers.Text = "button1";
            this.buttonReceivers.Click += new System.EventHandler(buttonReceivers_Click);
            // 
            // labelTo
            // 
            this.labelTo.Location = new System.Drawing.Point(6, 30);
            this.labelTo.Name = "labelTo";
            this.labelTo.Font = new System.Drawing.Font("Tahoma", 8, System.Drawing.FontStyle.Bold);
            this.labelTo.Size = new System.Drawing.Size(25, 13);
            this.labelTo.Text = "To:";
            // 
            // labelReceivers
            // 
            this.labelReceivers.Location = new System.Drawing.Point(30, 26);
            this.labelReceivers.Font = new System.Drawing.Font("Tahoma", 8, System.Drawing.FontStyle.Regular);
            this.labelReceivers.Name = "labelReceivers";
            this.labelReceivers.Size = new System.Drawing.Size(150, 28);
            // 
            // ViewMessagePopup
            // 
            this.panelContent.Location = new System.Drawing.Point(3, 40);
            this.panelContent.Name = "panelContent";
            this.panelContent.Size = new System.Drawing.Size(234, 230);

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.panelContent.Controls.Add(this.textBox1);
            this.panelContent.Controls.Add(this.labelTo);
            this.panelContent.Controls.Add(this.labelReceivers);
            this.panelContent.Controls.Add(this.buttonReceivers);

            this.Name = "ViewMessagePopup";
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private CargoMatrix.UI.CMXPictureButton buttonReceivers;
        private System.Windows.Forms.Label labelReceivers;
        private System.Windows.Forms.Label labelTo;
    }
}
