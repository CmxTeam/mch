﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.Messageing;
using CargoMatrix.Communication.WSLoadConsol;
using SmoothListbox.ListItems;
using CargoMatrix.UI;

namespace CargoMatrix.Messageing
{
    public partial class ViewMessagePopup : CMXMessageBoxPopup
    {
        private List<IMessage> messages;
        private IMessage msg;
        private int index;

        /// <summary>
        /// display message with reply,reply to all, delete and cencel buttons
        /// </summary>
        /// <param name="msg"></param>
        /// 
        public ViewMessagePopup(List<IMessage> msgs, int inx)
        {
            //InitializeComponent();
            messages = msgs;
            this.index = inx;
            DisplayMessage();
        }

        private void DisplayMessage()
        {
            this.msg = messages[index];
            this.Title = msg.SenderName + "  " + msg.DateCreated.ToString("MM/dd/yy HH:mm");
            this.textBox1.Text = FormatMessageString(msg);
            MessageManager.Instance.MarkAsRead(msg.ID);
            this.panelContent.Size = new System.Drawing.Size((int)CMXAnimationmanager.ScaleRatio.Width * 234, (int)CMXAnimationmanager.ScaleRatio.Height * 230);
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            var replyPopup = new NewMessagePopup();
            replyPopup.ReplyMessageId = msg.ID;
            replyPopup.ReceipientIds = new int[] { msg.SenderId };
            replyPopup.RecipientName = msg.SenderName;
            if (DialogResult.OK == replyPopup.ShowDialog())
                this.DialogResult = DialogResult.OK;
        }

        void buttonDelete_Click(object sender, EventArgs e)
        {
            MessageManager.Instance.DeleteMessage(this.msg.ID);
            this.DialogResult = DialogResult.Cancel;
        }

        private string FormatMessageString(IMessage msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(msg.Body);
            return sb.ToString();
        }

        void buttonPrev_Click(object sender, System.EventArgs e)
        {
            index = (index - 1 + messages.Count) % messages.Count;
            DisplayMessage();
        }

        void buttonNext_Click(object sender, System.EventArgs e)
        {
            index = (index + 1) % messages.Count;
            DisplayMessage();

        }
    }
}
