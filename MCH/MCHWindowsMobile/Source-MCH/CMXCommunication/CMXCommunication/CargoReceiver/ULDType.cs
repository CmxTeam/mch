﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.WSCargoReceiver_v2
{
    public partial class ULDType : IULDType
    {

        public string ULDName
        {
            get { return this.uLDField; }
        }

        public int TypeID
        {
            get { return this.idField; }
        }

        public string Description
        {
            get { return this.uLDDescriptionField; }
        }
    }
}