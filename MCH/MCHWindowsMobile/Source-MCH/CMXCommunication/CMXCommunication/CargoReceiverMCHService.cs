﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using CargoMatrix.Communication.WSCargoReceiverMCHService;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class CargoReceiverMCHServiceObj : CargoMatrix.Communication.WSCargoReceiverMCHService.CargoReceiverMCHService
    {
        //string path = "http://10.0.0.235/ScannerServices/";
        string asmxName = "CargoReceiverMCHService.asmx";
        public CargoReceiverMCHServiceObj()
        {
            //Do not Update from here
            //if (path == string.Empty)
            //{
                Url = Settings.Instance.AppURLPath + asmxName;
            //}
            //else
            //{
            //    Url = path + asmxName;
            //}
     

            //string currentUrl = Url;

        }
        public string URL
        {
            get { return Url; }
        }

    }

    public class CargoReceiverMCHService
    {

        //private static string gateway = "BOS";
        //private static string connectionName = "BOS";

        private static CargoReceiverMCHService instance;
        CargoReceiverMCHServiceObj ws;
        private CargoReceiverMCHService()
        {
            ws = new CargoReceiverMCHServiceObj();
            
        }
        public static CargoReceiverMCHService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CargoReceiverMCHService();
                }

                return instance;
            }

        }

        public string GetConnectionName()
        {

            //if (CargoLoaderMCHService.connectionName == string.Empty)
            //{
            return Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    return CargoLoaderMCHService.connectionName;
            //}

        }


        public string GetLocationMCH()
        {
            //string currentConnectionName = string.Empty;
            //if (CargoReceiverMCHService.connectionName == string.Empty)
            //{
            //    currentConnectionName = Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    currentConnectionName = CargoReceiverMCHService.connectionName;
            //}
            //return currentConnectionName;
            return GetConnectionName();
        }

 

        public bool DropPiecesToLocationMCH(long taskId,long  userId,int  locationId,long forkLiftDetailId, int pieces)
        {
 
          ws.DropPiecesToLocation(taskId,userId,locationId,forkLiftDetailId,pieces);


            return true;
        }







        public WSCargoReceiverMCHService.CargoReceiverFlight[] GetFlightsMCH(int userId, string gateway, string connection, CargoMatrix.Communication.DTO.FlightStatus status, string carrierNo, string origin, string flightNo, CargoMatrix.Communication.DTO.FlightSort sortBy)
        {

        
            //string currentGateway = string.Empty;
            //if (CargoReceiverMCHService.connectionName == string.Empty)
            //{
            //    currentGateway = gateway;
            //}
            //else
            //{
            //    currentGateway = CargoReceiverMCHService.gateway;
            //}


            TaskStatuses tempStatus = (TaskStatuses)status;
            ReceiverSortFields tempSortBy = (ReceiverSortFields)sortBy;

            WSCargoReceiverMCHService.CargoReceiverFlight[] flights = ws.GetCargoReceiverFlights(GetConnectionName(), userId, tempStatus, carrierNo, origin, flightNo, tempSortBy);



            return flights;
        }


        public WSCargoReceiverMCHService.CargoReceiverFlight GetFlightMCH(long manifestId,long taskId,long userId)
        {



            WSCargoReceiverMCHService.CargoReceiverFlight flight = ws.GetFlightManifestById(manifestId, taskId, userId);



            return flight;
        }



  

        public bool RecoverFlightMCH(int userId,  long taskId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}


                WSCargoReceiverMCHService.TransactionStatus t = ws.RecoverFlight(GetConnectionName(), taskId, userId);
               return t.Status;
            }
            catch
            {
                return false;
            }

            

             
        }


        public WSCargoReceiverMCHService.ValidatedShipment ValidateShipmentMCH(long userId, long taskId, long uldId, string shipment)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}


                return ws.ValidateShipment(userId, taskId, uldId, shipment);
               
            }
            catch
            {
                return null;
            }




        }

        public WSCargoReceiverMCHService.TransactionStatus AddForkliftPiecesMCH(long detailId,   long taskId, int pcs, long userId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}


                WSCargoReceiverMCHService.TransactionStatus t = ws.AddForkliftPieces(detailId, taskId, pcs, userId);
                return t;
            }
            catch
            {
                return null;
            }




        }


        public bool RecoverUldMCH(int userId, long uldId,   long locationId, long manifestId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}


                ws.RecoverUld(uldId, userId, manifestId, locationId);
                return true;
            }
            catch
            {
                return false;
            }




        }




        public  WSCargoReceiverMCHService.FlightUldInfo   GetFlightULDsMCH(int userId, string gateway, string connection, CargoMatrix.Communication.DTO.RecoverStatuses status,  long manifestId)
        {


            //string currentGateway = string.Empty;
            //if (CargoReceiverMCHService.connectionName == string.Empty)
            //{
            //    currentGateway = gateway;
            //}
            //else
            //{
            //    currentGateway = CargoReceiverMCHService.gateway;
            //}

            
            WSCargoReceiverMCHService.RecoverStatuses tempStatus = (WSCargoReceiverMCHService.RecoverStatuses)status;

            WSCargoReceiverMCHService.FlightUldInfo flightInfo = ws.GetFlightULDs(manifestId, tempStatus, "");
           
           return flightInfo;
        }


        public WSCargoReceiverMCHService.ForkliftView[] GetForkliftViewMCH(int userId,  long taskId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}

                return ws.GetForkliftView(taskId, userId);
               

            }
            catch
            {
                return null;
            }




        }


 
        public WSCargoReceiverMCHService.UldView[] GetFlightViewMCH(long taskId, long manifestId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}

                return ws.GetFlightView(taskId, manifestId);


            }
            catch
            {
                return null;
            }




        }




        public WSCargoReceiverMCHService.UldView[] GetUldViewMCH(int userId, long taskId, long uldId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}

                return ws.GetUldView(taskId, userId, uldId);


            }
            catch
            {
                return null;
            }




        }



        public bool DropForkliftPiecesMCH(int userId, string gateway, long taskId, int locationId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}


                ws.DropForkliftPieces(taskId, userId, locationId);
  return true;

            }
            catch
            {
                return false;
            }




        }

        public bool FinalizeReceiverMCH(long userId, long taskId, long manifestId)
        {
            try
            {



                ws.FinalizeReceiver(taskId, manifestId, userId);
                return true;

            }
            catch
            {
                return false;
            }




        }





        public bool UpdateFlightETAMCH(int userId, long taskId, long manifestId, DateTime eta)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                WSCargoReceiverMCHService.TransactionStatus t = ws.UpdateFlightETA(GetConnectionName(), manifestId, taskId, userId, eta);
                return t.Status;

            }
            catch
            {
                return false;
            }




        }


        public bool BUPChangeMCH(int userId, long manifestId, long uldId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                WSCargoReceiverMCHService.TransactionStatus t = ws.SwitchBUPMode(GetConnectionName(), uldId, userId, manifestId);
                 return t.Status;
 
            }
            catch
            {
                return false;
            }




        }


        public bool RemoveItemsFromForkliftMCH(long forkliftDetailsId, int piece)
        {
            try
            {
                ws.RemoveItemsFromForklift(forkliftDetailsId, piece);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public WSCargoReceiverMCHService.Uld GetFlightULDMCH(long uldId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                return ws.GetFlightULD(uldId);
          

            }
            catch
            {
                return null;
            }




        }


        public CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] GetFlightAlertsMCH(long manifestId)
        {
           
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] alerts = ws.GetFlightAlerts(GetConnectionName(),  manifestId);
                return alerts;

            }
            catch
            {
                return null;
            }
        }


        public CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo[] ScanShipmentMCH(long userId, long taskId, string shipmentNumber)
        {

            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                return ws.ScanShipment(userId, taskId, shipmentNumber);
                 

            }
            catch
            {
                return null;
            }
        }




        public CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] GetUldAlertsMCH( long uldId)
        {

            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] alerts = ws.GetUldAlerts(GetConnectionName(), uldId);
                return alerts;

            }
            catch
            {
                return null;
            }
        }





        public int GetForkliftCountMCH(long userId,long taskId)
        {
            try
            {

                //string currentGateway = string.Empty;
                //if (CargoReceiverMCHService.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = CargoReceiverMCHService.gateway;
                //}



                return ws.GetForkliftCount(userId, taskId);


            }
            catch
            {
                return 0;
            }




        }


        public  AvailableItmes[] GetAvailableCarriers(  long userId)
        {
            try
            {


                return ws.GetAvailableCarriers(GetConnectionName(), userId);


            }
            catch
            {
                return null;
            }
 
        }


        public AvailableFlight[] GetAvailableFlights(long carrierId, DateTime etd, long userId)
        {
            try
            {


                return ws.GetAvailableFlights(carrierId, etd, GetConnectionName(), userId);


            }
            catch
            {
                return null;
            }

        }


        public AvailableFlight[] GetAvailableFlightsByEta(long carrierId, DateTime eta, long userId)
        {
            try
            {


                return ws.GetAvailableFlightsByEta(carrierId, eta, GetConnectionName(), userId);


            }
            catch
            {
                return null;
            }

        }


        public DateTime[] GetAvailableEtds(long carrierId, long userId)
        {
            try
            {


                return ws.GetAvailableEtds(carrierId, GetConnectionName(), userId);


            }
            catch
            {
                return null;
            }

        }



        public DateTime[] GetAvailableEtas(long carrierId, long userId)
        {
            try
            {


                return ws.GetAvailableEtas(carrierId, GetConnectionName(), userId);


            }
            catch
            {
                return null;
            }

        }



        public bool LinkTaskToUserId(long taskId, long userId)
        {
            try
            {


                  WSCargoReceiverMCHService.TransactionStatus t=   ws.LinkTaskToUserId(taskId, userId);
                  return t.Status;
            }
            catch
            {
                return false;
            }

        }



        public FlightProgress GetFlightProgress(long manifestId, long taskId)
        {
            try
            {


                return ws.GetFlightProgress(manifestId, taskId);
                
            }
            catch
            {
                return  null;
            }

        }


        public int GetAwbPiecesCountInLocation(long awbId, long locationId)
        {
            try
            {


                return ws.GetAwbPiecesCountInLocation(awbId, locationId);

            }
            catch
            {
                return  0;
            }

        }



        public bool RelocateAwbPieces(long awbId, int locationId, int newlocationId, int pcs, long taskid,long userId)
        {
            try
            {


                WSCargoReceiverMCHService.TransactionStatus t = ws.RelocateAwbPieces(awbId, locationId, newlocationId, pcs, taskid, userId);
                return t.Status;
            }
            catch
            {
                return  false;
            }

        }




        public int GetLocationId(string locationName)
        {
            try
            {


                return ws.GetLocationId(locationName, GetConnectionName());
                  
            }
            catch
            {
                return 0;
            }

        }

    }
}
