﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
namespace CargoMatrix.Communication.ScannerUtilityWS
{
    public partial class UldItem : IULDType
    {
        #region IULDType Members

        public string ULDName
        {
            get { return this.uldField; }
            set { this.uldField = value; }
        }

        public int TypeID
        {
            get { return this.uldTypeIdField; }
            set { this.uldTypeIdField = value; }
        }

        public bool IsLoose
        {
            get { return string.Equals(this.uldField, "loose", StringComparison.OrdinalIgnoreCase); }
        }

        #endregion
    }
}
