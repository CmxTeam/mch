﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol
{
    public enum CommandTypesEnum
    {
        STOP,
        IP,
        ALIVE,
        DATA,
        BUSY,
        UNAVAILABLE
    }
}
