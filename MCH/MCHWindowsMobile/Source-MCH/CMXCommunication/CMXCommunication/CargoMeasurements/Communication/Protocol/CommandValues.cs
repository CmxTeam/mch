﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;


namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol
{
    public class CommandValues
    {
        CommandTypesEnum commnadType;
        CommunicatorTypesEnum communicatorType;
        MetricSystemTypeEnum metricSystemType;
        string data;

        public CommandTypesEnum CommnadType
        {
            get { return commnadType; }
            set { commnadType = value; }
        }

        public CommunicatorTypesEnum CommunicatorType
        {
            get { return communicatorType; }
            set { communicatorType = value; }
        }

        public string Data
        {
            get { return data; }
            set { data = value; }
        }

        public byte[] DataAsBytes
        {
            get
            {
                return CommandsParser.ToByteArray(this.data);
            }
        }

        public MetricSystemTypeEnum MetricSystemType
        {
            get { return metricSystemType; }
            set { metricSystemType = value; }
        }

        public MettlerMeasurements DataAsMeasurements
        {
            get
            {
                if (string.IsNullOrEmpty(this.data)) return null;

                return MettlerMeasurements.Parse(this.metricSystemType, data);

            }
        }

        public CommandValues(MetricSystemTypeEnum metricSystemType)
        {
            this.metricSystemType = metricSystemType;
        }

        public CommandValues()
        {
            this.metricSystemType = MetricSystemTypeEnum.Imperial;
        }
        
       
    }
}
