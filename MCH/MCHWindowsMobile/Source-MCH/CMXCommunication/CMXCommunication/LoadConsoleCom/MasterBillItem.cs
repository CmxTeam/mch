﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.CargoTruckLoad;

namespace CargoMatrix.Communication.WSLoadConsol
{
    public partial class MasterBillItem : CargoMatrix.Communication.DTO.IMasterBillItem, ITruckLoadMasterBillItem
    {

        #region IMasterBillItem Members


        int CargoMatrix.Communication.DTO.IMasterBillItem.Weight
        {
            get { return (int)this.weightField; }
        }

        DateTime CargoMatrix.Communication.DTO.IMasterBillItem.CutOff
        {
            get { return this.cutOffField ?? DateTime.MinValue; }
        }

        CargoMatrix.Communication.DTO.MasterbillStatus CargoMatrix.Communication.DTO.IMasterBillItem.Status
        {

            get
            {
                //switch (this.statusField)
                //{
                //    case MasterBillStatuses.All:
                //        return MasterbillStatus.All;
                //    case MasterBillStatuses.Completed:
                //        return MasterbillStatus.Completed;
                //    case MasterBillStatuses.InProgress:
                //        return MasterbillStatus.InProgress;
                //    case MasterBillStatuses.Open:
                //        return MasterbillStatus.Open;
                //    case MasterBillStatuses.Pending:
                //        return MasterbillStatus.Pending;
                //        break;
                //    default:
                //        break;
                //}
                return (MasterbillStatus)Enum.Parse(typeof(MasterbillStatus), this.statusField.ToString(), true);
            }
        }

        #endregion

        #region ITruckLoadMasterBillItem Members

        public LoadTruckLocationItem[] Doors
        {
            get { return this.loadTruckLocationsField; }
        }

        #endregion
    }
}
