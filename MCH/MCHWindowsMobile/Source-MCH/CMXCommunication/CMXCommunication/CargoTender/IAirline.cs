﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoTender
{
    public interface IAirline
    {
        string Name
        {
            get;
        }
    }
}
