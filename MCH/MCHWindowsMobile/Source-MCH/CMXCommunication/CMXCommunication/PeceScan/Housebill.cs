﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.WSPieceScan
{
    public partial class HouseBill
    {
        public bool ScreeningFailed
        {
            // mapped to the field in GlobalDb fsp.statuses table
            get { return (this.houseBillStatusField ?? 0) == 56; }
        }
    }
}
