﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Messageing
{
    public class Messageing : CMXServiceWrapper
    {
        private static Messageing instance;
        public static Messageing Instance
        {
            get
            {
                if (null == instance)
                    instance = new Messageing();
                return instance;
            }
        }

        private Messageing()
        {

        }

        public IMessage[] GetRecentMessages()
        {
            return messages.ToArray();
        }

        public void QueueMessage(Message message)
        {
            int maxId = messages.Max<Message>(m => m.ID);
            message.ID = maxId + 1;
            message.MessageDirection = MessageType.Out;
            messages.Add(message);
        }

        public IMessage[] GetUrgentMessages()
        {
            var urgMsgs = from m in messages
                          where m.IsUnread && m.Priority == MessagePriority.High
                          select m;
            return urgMsgs.ToArray();
        }

        public MessageCheckResponse GetUnreadMessagesCount()
        {
            int count = messages.Count(m => m.IsUnread);
            return new MessageCheckResponse() { UnreadMsgsCount = count, UrgentMsgExists = false };
        }

        public int DeleteMessage(int msgId)
        {
            messages.Remove(messages.Find(m => m.ID == msgId));
            return 1;
        }

        public int MarkAsRead(int msgId)
        {
            messages.Find(m => m.ID == msgId).IsUnread = false;
            return 1;
        }


        private static List<Message> messages = new List<Message> { };
        /*
        {
            new Message() { Body = @"Yahoo, the Internet Company, plans to cut 600 to 700 jobs or nearly five percent of its
                                     workforce to reduce costs and reposition the company in the faster-growing markets, San Dieo News Net reported.
                                     According to a person familiar to the plans of Yahoo, the employees that will be laid off can be notified as early as Tuesday.
                                     The person asked for anonymity because Yahoo had not made any formal announcement about the issue.",
                            SenderName = "John Smith", DateCreated = DateTime.Now, ID = 1, IsUnread = true, MessageDirection = MessageType.In
                                                                                                                                                                                                                                   
             },
             new Message() { Body = "Msg 2",  SenderName = "Mike Watt", DateCreated = DateTime.Now, ID = 2, IsUnread = true,MessageDirection = MessageType.In},
             new Message() { Body = "Retape hawb 1GT4563 with blue tape!!", SenderName = "Supervisor", DateCreated = DateTime.Now, Priority = MessagePriority.High, ID = 3, IsUnread = true,MessageDirection = MessageType.In },
             new Message() { Body = "Put Hawb 3GE2123 int High Value Cage", SenderName = "Supervisor", DateCreated = DateTime.Now, Priority = MessagePriority.High, ID = 4, IsUnread = true,MessageDirection = MessageType.In }
        };
         * */
    }
}
