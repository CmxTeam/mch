﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.Messageing
{
    public class MessageCheckResponse
    {
        public int UnreadMsgsCount;
        public bool UrgentMsgExists;
    }
    public enum MessageType
    {
        In,Out,
    }
    public enum MessageResponseType
    {
        Acknowledgement,
        YesNo,
        YesNoCancel,
        Text,
    }
    public enum MessagePriority
    {
        Normal,
        High,
        Low,
    }
}
