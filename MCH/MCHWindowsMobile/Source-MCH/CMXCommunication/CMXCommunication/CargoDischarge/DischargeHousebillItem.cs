﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSCargoDischarge;

namespace CargoMatrix.Communication.CargoDischarge
{
    public class DischargeHousebillItem : IDischargeHouseBillItem
    {
        WSCargoDischarge.HouseBillItem wsHouseBillItem;

        internal DischargeHousebillItem(WSCargoDischarge.HouseBillItem houseBillItem)
        {
            this.wsHouseBillItem = houseBillItem;
        }
        
        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get 
            {
                return this.wsHouseBillItem.HousebillNumber;
            }
        }

        public int HousebillId
        {
            get 
            {
                return this.wsHouseBillItem.HousebillId;     
            }
        }

        public string Origin
        {
            get 
            {
                return this.wsHouseBillItem.Origin;     
            }
        }

        public string Destination
        {
            get 
            {
                return this.wsHouseBillItem.Destination;
            }
        }

        public int TotalPieces
        {
            get 
            {
                return this.wsHouseBillItem.TotalPieces;     
            }
        }

        public int ScannedPieces
        {
            get 
            {
                return this.wsHouseBillItem.ScannedPieces;
            }
        }

        public int Slac
        {
            get 
            {
                return this.wsHouseBillItem.TotalSlac;
            }
        }

        public int AvailableSlac
        {
            get
            {
                return this.wsHouseBillItem.AvailableSlac;
            }
        }

        public int AvailablePieces
        {
            get
            {
                return this.wsHouseBillItem.AvailablePieces;

            }
        }

        public int Weight
        {
            get 
            {
                return (int)this.wsHouseBillItem.Weight;
            }
        }

        public string Location
        {
            get
            {
                return this.wsHouseBillItem.LastLocation;
            }
        }

        public int Flags
        {
            get
            {
                return this.wsHouseBillItem.Flags;
            }
                
        }

        public DTO.ScanModes ScanMode
        {
            get
            {
                return (DTO.ScanModes)Enum.Parse(typeof(DTO.ScanModes), this.wsHouseBillItem.ScanMode.ToString(), true);
            }
        }

        public DTO.HouseBillStatuses status
        {
            get
            {
                return (DTO.HouseBillStatuses)Enum.Parse(typeof(DTO.HouseBillStatuses), this.wsHouseBillItem.Status.ToString(), true);
            }
        }

        public DTO.RemoveModes RemoveMode
        {
            get
            {
                return (DTO.RemoveModes)Enum.Parse(typeof(DTO.RemoveModes), this.wsHouseBillItem.RemoveMode.ToString(), true);
            }
        }

        #endregion

        #region IDischargeHouseBillItem Members

        public DateTime ReleaseTime
        {
            get 
            {
                return this.wsHouseBillItem.ReleaseDate; 
            }
        }

        public string DriverFullName
        {
            get 
            {
                return this.wsHouseBillItem.ReleaseDriver;
            }
        }

        public string SenderName
        {
            get 
            {
                return this.wsHouseBillItem.ShipperName;
            }
        }

        public string ReceiverName
        {
            get 
            {
                return this.wsHouseBillItem.ConsigneeName;    
            }
        }

        public string CompanyName
        {
            get 
            {
                return this.wsHouseBillItem.ReleaseCompany;
            }
        }

        public string AliasNumber
        {
            get
            {
                return this.wsHouseBillItem.AliasNumber;
            }
        }

        public string ScanName
        {
            get
            {
                return this.wsHouseBillItem.LastScan;
            }
        }

        public string Description
        {
            get
            {
                return this.wsHouseBillItem.Description;
            }

        }

        public PieceItem[] Pieces
        {
            get
            {
                return this.wsHouseBillItem.Pieces;
            }
        }

        public int TaskID
        {
            get 
            {
                return this.wsHouseBillItem.TaskId;
            }
        }

        public int ScannedSlac
        {
            get
            {
                return this.wsHouseBillItem.ScannedSlac;
            }
        }

        public int ReleseSlac
        {
            get
            {
                return this.wsHouseBillItem.ReleaseSlac;
            }
        }

        public ShipmentAlert[] ShipmentAlerts
        {
            get
            {
                return this.wsHouseBillItem.ShipmentAlerts;
            }
        }

        public string MasterBillNumber
        {
            get
            {
                return this.wsHouseBillItem.MasterBillNumber;
            }
        }
     
        public TransactionStatus TransactionStatus
        {
            get 
            {
                return this.wsHouseBillItem.Transaction;
            }
        }


        public ReleaseStatuses ReleaseStatus
        {
            get
            {
                return this.wsHouseBillItem.ReleaseStatus;
            }
        }

        #endregion
    }
}
