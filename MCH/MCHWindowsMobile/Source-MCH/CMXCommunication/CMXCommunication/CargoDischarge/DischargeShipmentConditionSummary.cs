﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.CargoDischarge
{
    public class DischargeShipmentConditionSummary : IShipmentConditionSummary
    {
        ScannerUtilityWS.ShipmentConditionSummary wsShipmentConditionSummary;

        public DischargeShipmentConditionSummary(ScannerUtilityWS.ShipmentConditionSummary wsShipmentConditionSummary)
        {
            this.wsShipmentConditionSummary = wsShipmentConditionSummary;
        }
        
        #region IShipmentConditionSummary Members

        public string ConditionTypeName
        {
            get 
            {
                return this.wsShipmentConditionSummary.ConditionTypeName;
            }
        }

        public int ConditionTypeId
        {
            get 
            {
                return this.wsShipmentConditionSummary.ConditionTypeId; 

            }
        }

        public string ConditionTypeCode
        {
            get 
            {
                return this.wsShipmentConditionSummary.ConditionTypeCode; 

            }
        }

        public int Pieces
        {
            get 
            {
                return this.wsShipmentConditionSummary.Pieces;
            }
        }

        public bool ContainsAqm
        {
            get
            {
                return this.wsShipmentConditionSummary.ContainsAqm;
            }
        }

        #endregion
    }
}
