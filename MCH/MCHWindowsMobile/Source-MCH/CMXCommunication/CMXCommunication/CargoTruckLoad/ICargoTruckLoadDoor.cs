﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public interface ICargoTruckLoadDoor
    {
        int ID { get; }
        string Name { get; }
        LocationTypes locationType { get; }

        WSLoadConsol.LocationItem Truck { get; }
    }
}
