﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public enum SortingFieldsEnum
    {
        NA = 1,
        CutOff = 2,
        MasterBillNumber = 3,
        Origin = 4,
        Weight = 5,
        CarrierNumber = 6,
    }
}
