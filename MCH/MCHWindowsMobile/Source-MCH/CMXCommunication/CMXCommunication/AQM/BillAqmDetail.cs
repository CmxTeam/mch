﻿using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.WSAQM
{
    public partial class HouseBillAqmDetail : IAQMInfo
    {
        public string CarrierNo
        {
            get { throw new System.NotImplementedException(); }
        }

        public string AwbNo
        {
            get { return this.houseBillNoField; }
        }
        public AQMType Type
        {
            get { return AQMType.HAWB; }
        }

        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}", this.originField, this.houseBillNoField, this.destinationField); }
        }





        public int ID
        {
            get { return this.houseBillIDField; }
        }

        double IAQMInfo.Weight
        {
            get { return this.weightField ?? 0; }
        }


        #region IAQMInfo Members


        int IAQMInfo.TotalPieces
        {
            get { return this.totalPiecesField ?? 1; }
        }

        #endregion
    }
    public partial class MasterBillAqmDetail : IAQMInfo
    {

        public string AwbNo
        {
            get { return this.masterBillNoField; }
        }

        public AQMType Type
        {
            get { return AQMType.MAWB; }
        }
        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}-{3}", this.originField, this.carrierNoField, this.masterBillNoField, this.destinationField); }
        }
        public int ID
        {
            get { return this.masterBillIDField; }
        }


        #region IAQMInfo Members


        int IAQMInfo.TotalPieces
        {
            get { return this.totalPiecesField ?? 0; }
        }

        double IAQMInfo.Weight
        {
            get { return this.weightField ?? 0; }
        }

        #endregion
    }

}