﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Parsing.Results
{
    public class HouseBillResult : ParseResult
    {
        string pieceID;

        public string PieceID
        {
            get { return pieceID; }
        }


        public HouseBillResult(string barcode, ParsingPattern.PatternValue pasingValue)
            : base(barcode, ResultTypeEnum.HouseBill, pasingValue)
        { }

        protected override void EmbedValue(string value, string name)
        {
            switch (name.ToLower())
            {
                case "pieceid":
                    this.pieceID = value;
                    break;

                case "id":
                    this.iD = value;
                    break;
            }
        }

        public override ScanObjectLegacy AsScanObject()
        {
            var result = base.AsScanObject();

            result.HouseBillNumber = this.ID;

            try
            {
                result.PieceNumber = int.Parse(this.PieceID);
            }
            catch {}
            

            return result;
        }
    }
}
