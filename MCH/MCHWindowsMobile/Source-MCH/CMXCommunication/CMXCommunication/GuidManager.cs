using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CargoMatrix.Utilities;

namespace CargoMatrix.Communication
{
    /// <summary>
    /// Generate GUIDs on the .NET Compact Framework.
    /// </summary>
    public class GuidManager : IDisposable
    {
        private Timer m_guidTimer;
        private static Guid m_guid;
        private static GuidManager instance;
        private static int m_timeout = 30; // by default its 30 minutes
        static DateTime thresholdTime;// = DateTime.Now;
        // guid variant types
        private enum GuidVariant
        {
            ReservedNCS = 0x00,
            Standard = 0x02,
            ReservedMicrosoft = 0x06,
            ReservedFuture = 0x07
        }

        // guid version types
        private enum GuidVersion
        {
            TimeBased = 0x01,
            Reserved = 0x02,
            NameBased = 0x03,
            Random = 0x04
        }

        // constants that are used in the class
        private class Const
        {
            // number of bytes in guid
            public const int ByteArraySize = 16;

            // multiplex variant info
            public const int VariantByte = 8;
            public const int VariantByteMask = 0x3f;
            public const int VariantByteShift = 6;

            // multiplex version info
            public const int VersionByte = 7;
            public const int VersionByteMask = 0x0f;
            public const int VersionByteShift = 4;
        }

        // imports for the crypto api functions
        private class WinApi
        {
            public const uint PROV_RSA_FULL = 1;
            public const uint CRYPT_VERIFYCONTEXT = 0xf0000000;

            [DllImport("coredll.dll")]
            public static extern bool CryptAcquireContext(
                ref IntPtr phProv, string pszContainer, string pszProvider,
                uint dwProvType, uint dwFlags);

            [DllImport("coredll.dll")]
            public static extern bool CryptReleaseContext(
                IntPtr hProv, uint dwFlags);

            [DllImport("coredll.dll")]
            public static extern bool CryptGenRandom(
                IntPtr hProv, int dwLen, byte[] pbBuffer);
        }

        // all static methods
        private GuidManager()
        {
            SuspendDeviceLock();
            m_guidTimer = new Timer();
            this.m_guidTimer.Interval = 30000;
            OpenNETCF.Windows.Forms.Application2.AddMessageFilter(new CMXMessageFilter());
            m_guidTimer.Tick += new System.EventHandler(GuidTimer_Tick);
            m_guidTimer.Enabled = true;
            BarcodeParser.BarcodParsed += (o, e) => SuspendDeviceLock(); // suspend device lock if there is a scan activity
        }
        public static int TimeOut
        {
            get { return m_timeout; }
            set
            {
                m_timeout = value;
                SuspendDeviceLock();
            }
        }

        private static void SuspendDeviceLock()
        {
            thresholdTime = DateTime.Now.AddMinutes(m_timeout);
        }
        public static Guid GetGuid()
        {
            DateTime currentTime;

            if (instance == null)
            {
                instance = new GuidManager();
                GuidManager.m_guid = NewGuid();
            }
            currentTime = DateTime.Now;
            //if (previousTime.AddMinutes(m_timeout) < currentTime)
            //    ResetGuid();
            SuspendDeviceLock();

            return GuidManager.m_guid;


        }
        public static void SetGuid(string guid, int timeout)
        {
            m_timeout = timeout;
            GetGuid();
            GuidManager.m_guid = new Guid(guid);
            CargoMatrix.UI.CMXAnimationmanager.GetParent().LoggedIn = true;
        }

        public static Guid ResetGuid()
        {
            if (instance == null)
            {
                instance = new GuidManager();

            }
            //previousTime = DateTime.Now;
            GuidManager.m_guid = NewGuid();
            return GuidManager.m_guid;


        }

        /// <summary>
        /// Return a new System.Guid object.
        /// </summary>
        private static Guid NewGuid()
        {
            IntPtr hCryptProv = IntPtr.Zero;
            m_guid = Guid.Empty;

            try
            {
                // holds random bits for guid
                byte[] bits = new byte[Const.ByteArraySize];

                // get crypto provider handle
                if (!WinApi.CryptAcquireContext(ref hCryptProv, null, null,
                    WinApi.PROV_RSA_FULL, WinApi.CRYPT_VERIFYCONTEXT))
                {
                    throw new SystemException(
                        "Failed to acquire cryptography handle.");
                }

                // generate a 128 bit (16 byte) cryptographically random number
                if (!WinApi.CryptGenRandom(hCryptProv, bits.Length, bits))
                {
                    throw new SystemException(
                        "Failed to generate cryptography random bytes.");
                }

                // set the variant
                bits[Const.VariantByte] &= Const.VariantByteMask;
                bits[Const.VariantByte] |=
                    ((int)GuidVariant.Standard << Const.VariantByteShift);

                // set the version
                bits[Const.VersionByte] &= Const.VersionByteMask;
                bits[Const.VersionByte] |=
                    ((int)GuidVersion.Random << Const.VersionByteShift);

                // create the new System.Guid object
                m_guid = new Guid(bits);
            }
            finally
            {
                // release the crypto provider handle
                if (hCryptProv != IntPtr.Zero)
                    WinApi.CryptReleaseContext(hCryptProv, 0);
            }

            return m_guid;
        }

        private void GuidTimer_Tick(object sender, EventArgs e)
        {
            if (CargoMatrix.UI.CMXAnimationmanager.GetParent().LoggedIn &&
                thresholdTime < DateTime.Now)
            {
                string userLabel = string.Format("User: {0} ({1} {2})", WebServiceManager.Instance().m_user.UserName,
                WebServiceManager.Instance().m_user.firstName, WebServiceManager.Instance().m_user.lastName);
                m_guidTimer.Enabled = false;
                if (DialogResult.OK == CargoMatrix.Utilities.LockMessageBoxWithBarcode.ShowDialog("Idle", userLabel, ValidateUserPin, ValidateScannedPin))
                {
                    SuspendDeviceLock();
                    m_guidTimer.Enabled = true;
                    UI.CMXAnimationmanager.CurrentControl.LoadControl();
                }
            }
        }

        private bool ValidateUserPin(string pin)
        {
            return string.Equals(pin, WebServiceManager.Instance().m_user.pin);
        }

        private int ConvertToNumericInt(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch { return 0; }
        }

        private bool ValidateScannedPin(string barocdeData)
        {
            var scan = BarcodeParser.Instance.Parse(barocdeData);

            int userNumber = ConvertToNumericInt(scan.BarcodeData);
            if (scan.BarcodeType == CMXBarcode.BarcodeTypes.NA && userNumber > 0)
            {
                scan.BarcodeType = CMXBarcode.BarcodeTypes.User;
                scan.UserNumber = userNumber;
            }

            return scan.BarcodeType == CMXBarcode.BarcodeTypes.User && ValidateUserPin(scan.UserNumber.ToString());
        }
        public void Dispose()
        {
            m_guidTimer.Dispose();

            instance = null;

            BarcodeParser.BarcodParsed -= (o, e) => SuspendDeviceLock();
        }

        class CMXMessageFilter : OpenNETCF.Windows.Forms.IMessageFilter
        {
            public bool PreFilterMessage(ref Microsoft.WindowsCE.Forms.Message msg)
            {

                switch ((OpenNETCF.Win32.WM)msg.Msg)
                {

                    case OpenNETCF.Win32.WM.KEYDOWN:
                    case OpenNETCF.Win32.WM.LBUTTONDOWN:
                        SuspendDeviceLock();
                        break;


                }


                return false;
            }
        }
    }
}
