﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Reflection;


namespace CargoMatrixScanner
{
    
    public partial class ConnectionForm : Form
    {
        
        public ConnectionForm()
        {
            InitializeComponent();
            comboBoxCamera.SelectedIndex = 0;
            //string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
            if (File.Exists(CargoMatrix.Program.ConfigFile) == true)
            {
                CargoMatrixScanner.CMXConfig config;
                XmlSerializer s = new XmlSerializer(typeof(CargoMatrixScanner.CMXConfig));

                TextReader r = new StreamReader(CargoMatrix.Program.ConfigFile);
                config = (CargoMatrixScanner.CMXConfig)s.Deserialize(r);
                r.Close();
                textBoxConnectionString.Text = config.connectionName;
                textBoxGateway.Text = config.gateway;
                textBoxURL.Text = config.WSURL;
                comboBoxCamera.SelectedIndex = config.camera;
                
            }

            
        }
                

        private void button1_Click(object sender, EventArgs e)
        {
            //string []str = new string[4];
            //str[0] = textBoxConnectionString.Text;
            //str[1] = textBoxGateway.Text;
            //str[2] = textBoxURL.Text;
            //str[3] = Convert.ToString(comboBoxCamera.SelectedIndex);
            
            //XmlSerializer s = new XmlSerializer(typeof(string[]));
            //TextWriter w = new StreamWriter(@"ConnectionString.xml");
            //s.Serialize(w, str);
            

            WriteXML();

            this.Close();
        }

        void WriteXML()
        {
            try
            {
                //pick whatever filename with .xml extension
                //string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
                //string filename = path + "\\CMXScannerConfig.xml";

                CMXConfig config = new CMXConfig();
                config.camera = comboBoxCamera.SelectedIndex;
                config.connectionName = textBoxConnectionString.Text;
                config.gateway = textBoxGateway.Text;
                config.WSURL = textBoxURL.Text;
                

                XmlSerializer s = new XmlSerializer(typeof(CMXConfig));

                XmlTextWriter writer = new XmlTextWriter(CargoMatrix.Program.ConfigFile, null);
                writer.Formatting = Formatting.Indented;
                s.Serialize(writer, config);
                writer.Close();
                


                //XmlDocument xmlDoc = new XmlDocument();

                //try
                //{
                //    xmlDoc.Load(filename);
                //}
                //catch (System.IO.FileNotFoundException)
                //{
                //    //if file is not found, create a new xml file
                //    XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                //    xmlWriter.Formatting = Formatting.Indented;
                //    xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                //    xmlWriter.WriteStartElement("CMXScannerConfig");
                //    //If WriteProcessingInstruction is used as above,
                //    //Do not use WriteEndElement() here
                //    //xmlWriter.WriteEndElement();
                //    //it will cause the <Root></Root> to be <Root />
                //    xmlWriter.Close();
                //    xmlDoc.Load(filename);
                //}
                //XmlNode root = xmlDoc.DocumentElement;
                //XmlElement xmlConnectionString = xmlDoc.CreateElement("ConnectionName");
                //xmlConnectionString.InnerText = textBoxConnectionString.Text;
                //XmlElement xmlGateway = xmlDoc.CreateElement("Gateway");
                //xmlGateway.InnerText = textBoxGateway.Text;
                //XmlElement xmlScannerWSURL = xmlDoc.CreateElement("ScannerWSURL");
                //xmlScannerWSURL.InnerText = textBoxURL.Text;
                //XmlElement xmlUpdaterWSURL = xmlDoc.CreateElement("UpdaterWSURL");
                //XmlElement xmlCamera = xmlDoc.CreateElement("CameraType");
                //xmlCamera.InnerText = Convert.ToString(comboBoxCamera.SelectedIndex);

                //root.AppendChild(xmlConnectionString);
                //root.AppendChild(xmlGateway);
                //root.AppendChild(xmlScannerWSURL);
                //root.AppendChild(xmlUpdaterWSURL);
                //root.AppendChild(xmlCamera);

                ////XmlElement childNode = xmlDoc.CreateElement("childNode");
                ////XmlElement childNode2 = xmlDoc.CreateElement("SecondChildNode");
                ////XmlText textNode = xmlDoc.CreateTextNode("hello");
                ////textNode.Value = "hello, world";

                ////root.AppendChild(childNode);
                ////childNode.AppendChild(childNode2);
                ////childNode2.SetAttribute("Name", "Value");
                ////childNode2.AppendChild(textNode);

                ////textNode.Value = "replacing hello world";
                //xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                //WriteError(ex.ToString());
                MessageBox.Show(ex.Message);
            }
        }

    }
    [XmlRoot("CMXConfig", IsNullable = false)]
    public class CMXConfig
    {
        public string connectionName;
        public string gateway;
        public string WSURL;
        public int camera;
    }
}