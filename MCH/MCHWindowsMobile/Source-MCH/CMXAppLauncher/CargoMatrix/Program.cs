﻿using System;

using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace CargoMatrix
{
    
    static class Program
    {

        public static string ConfigFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\CMXScannerConfig.xml";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            //try
            //{
            //    //CargoMatrixScanner.CMXConfig config;
            //    //XmlSerializer s = new XmlSerializer(typeof(TaskItem[]));
                
            //    //TextReader r = new StreamReader(@"CMXScannerConfig.xml");
            //    //config = (CargoMatrixScanner.CMXConfig)s.Deserialize(r);
            //    //r.Close();

               

                

            //}
            //catch (FileNotFoundException e)
            //{
            //    Application.Run(new CargoMatrixScanner.ConnectionForm());
            //    //new CargoMatrixScanner.ConnectionForm().Show();
            //}

            
            try
            {
                
                if (File.Exists(ConfigFile) == false)
                    Application.Run(new CargoMatrixScanner.ConnectionForm());

                Application.Run(new CMXShell());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }

            
        }
    }
}