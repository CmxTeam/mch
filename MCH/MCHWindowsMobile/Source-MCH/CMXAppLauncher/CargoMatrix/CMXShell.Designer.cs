﻿namespace CargoMatrix
{
    partial class CMXShell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CMXShell));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelFooter = new System.Windows.Forms.Label();
            this.LoadTimer = new System.Windows.Forms.Timer();
            this.labelMessage = new System.Windows.Forms.Label();
            this.cmxProgressBar1 = new CargoMatrix.CMXProgressBar();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(25, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(190, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(3, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 14);
            this.label1.Text = "Initializing please wait...";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelFooter
            // 
            this.labelFooter.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.labelFooter.ForeColor = System.Drawing.Color.Gray;
            this.labelFooter.Location = new System.Drawing.Point(3, 306);
            this.labelFooter.Name = "labelFooter";
            this.labelFooter.Size = new System.Drawing.Size(234, 14);
            this.labelFooter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LoadTimer
            // 
            this.LoadTimer.Tick += new System.EventHandler(this.LoadTimer_Tick);
            // 
            // labelMessage
            // 
            this.labelMessage.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelMessage.ForeColor = System.Drawing.Color.Gray;
            this.labelMessage.Location = new System.Drawing.Point(3, 205);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(234, 14);
            this.labelMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmxProgressBar1
            // 
            this.cmxProgressBar1.BackColor = System.Drawing.Color.White;
            this.cmxProgressBar1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmxProgressBar1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.cmxProgressBar1.ForeColor = System.Drawing.Color.LimeGreen;
            this.cmxProgressBar1.Location = new System.Drawing.Point(25, 237);
            this.cmxProgressBar1.Maximum = 0;
            this.cmxProgressBar1.Minimum = 0;
            this.cmxProgressBar1.Name = "cmxProgressBar1";
            this.cmxProgressBar1.Size = new System.Drawing.Size(189, 17);
            this.cmxProgressBar1.TabIndex = 9;
            this.cmxProgressBar1.TextColor = System.Drawing.Color.Black;
            this.cmxProgressBar1.Value = 0;
            this.cmxProgressBar1.Visible = false;
            // 
            // CMXShell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.cmxProgressBar1);
            this.Controls.Add(this.labelFooter);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "CMXShell";
            this.Text = "CMXShell";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFooter;
        private System.Windows.Forms.Timer LoadTimer;
        private System.Windows.Forms.Label labelMessage;
        private CMXProgressBar cmxProgressBar1;
    }
}

