﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MCH.Web.Utilities.Extensions
{
    public static class Int32TimeZoneExtension
    {
        public static string ToTimeZoneString(this int x)
        {
            StringBuilder tmpResult = new StringBuilder("UTC");
            if (x != 0)
            {
                int tmpSign = Math.Sign(x);
                if (tmpSign > 0)
                {
                    tmpResult.Append(" + ");
                }
                else 
                {
                    tmpResult.Append(" - ");
                }
                if (x > 9)
                {
                    tmpResult.Append(String.Format("{0}",Math.Abs(x)));
                }
                else 
                {
                    tmpResult.Append(String.Format("0{0}", Math.Abs(x)));
                }
                tmpResult.Append(":00");
            }
                
            return tmpResult.ToString();
        }
    }
}