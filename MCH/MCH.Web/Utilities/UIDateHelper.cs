﻿using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Utilities
{
    public static class UIDataHelper
    {

        public static IEnumerable<SelectListItem> GetPriorities(int? selection, string optionalText, string optionalValue) 
        {
            var tmpItems = new List<SelectListItem>();
            tmpItems.Add(new SelectListItem() {Selected = false,Text = "Standard", Value="1" });
            tmpItems.Add(new SelectListItem() { Selected = false, Text = "Expedited", Value = "2" });

            return tmpItems;
        }

        public static IEnumerable<SelectListItem> GetShipperTypes(long? selection, string optionalText, string optionalValue) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.ShipperTypes.Select(item => new SelectListItem { Text = item.Name, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetStates(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.States.Select(item => new SelectListItem { Text = item.TwoCharacterCode, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetUNClasses(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.UNClasses.Select(item => new SelectListItem { Text = item.Description, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetSpecialHandlingCodes(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.SpecialHandlingCodes.Select(item => new SelectListItem { Text = item.Code, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetAttributeTypes(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.Attributes.Select(item => new SelectListItem { Text = item.Name, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetCredentialTypes(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.CredentialTypes.Select(item => new SelectListItem { Text = item.Name, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetCCSF(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.Shippers.Where(item => item.CCSFStatus == "Active").Select(item => new SelectListItem { Text = item.Name + " " + item.TSAApprovalNumber, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetSealTypes(int ? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.ScreeningSealTypes.Select(item => new SelectListItem { Text = item.Type, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetScreeningMethods(int? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.Context.ScreeningMethods.Select(item => new SelectListItem { Text = item.Name, Value = SqlFunctions.StringConvert((double)item.Id).Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }
        

        public static IEnumerable<SelectListItem> GetStations(long ? selection, string optionalText, string optionalValue) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpItems= unitOfWork.PortRepository.Get().OrderBy(item=> item.IATACode).Select(item => new SelectListItem { Text = item.IATACode, Value = item.Id.ToString(), Selected= (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetCarriers(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.CarrierRepostiory.Get().Select(item => new SelectListItem { Text = item.CarrierCode, Value = item.Id.ToString().Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetTruckingCos(long? selection, string optionalText, string optionalValue)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.CarrierRepostiory.Get().Where(item=> item.MOTId == 3).Select(item => new SelectListItem { Text = item.CarrierName, Value = item.Id.ToString().Trim(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }


        public static IEnumerable<SelectListItem> GetTaskTypes(int? selection, string optionalText, string optionalValue) 
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpItems= unitOfWork.TaskTypeRepository.Get().Select(item => new SelectListItem { Text = item.Description, Value = item.Id.ToString(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetUsers(int? selection, string optionalText, string optionalValue) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.UserProfileRepository.Get().Select(item => new SelectListItem { Text = item.FirstName + " " +item.LastName, Value = item.Id.ToString(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText))
                {
                    tmpItems.Insert(0, new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }
                return tmpItems;
            }
        }

        public static IEnumerable<SelectListItem> GetStationUsers(long? stationId) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpItems = unitOfWork.UserProfileRepository.Get().Where(item => item.UserStations.Where(us => us.PortId == stationId && us.IsDefault).Any()).Select(item => new SelectListItem { Text = item.FirstName + " " + item.LastName, Value = item.Id.ToString(), Selected = false }).ToList();

                return tmpItems;
            }
        }


        public static IEnumerable<SelectListItem> GetTaskStatuses(int? selection, string optionalText, string optionalValue) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems =  unitOfWork.StatusRepository.Get().Where(item=> item.EntityTypeId == (int)enumEntityTypes.Task).Select(item => new SelectListItem { Text = item.DisplayName, Value = item.Id.ToString(), Selected = (selection.HasValue && item.Id == selection.Value) }).ToList();

                if (!String.IsNullOrWhiteSpace(optionalText)) 
                {
                    tmpItems.Insert(0,new SelectListItem() { Selected = false, Text = optionalText, Value = optionalValue });
                }

                return tmpItems;
            }
        }
    }
}