﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MCH.Web.Utilities.Extensions;
using System.Text;
using CMX.Shell.Model;

namespace MCH.Web.Utilities
{
    public class UserInfo
    {
        private int timezone;
        private int clientTimeZone;

        public string UserId { get; set; }
        public long UserAliasId { get; set; }
        public string SecurityToken { get; set; }
        public string ThemeName { get; set; }
        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string TimeZone { get { return this.clientTimeZone.ToTimeZoneString(); } }
        public int UserTimeZone { get { return this.timezone; } set { this.timezone = value; } }
        public string ClientTimeZone { get { return this.clientTimeZone.ToTimeZoneString(); } }
        public int ClientTimeZoneOffset { get { return this.clientTimeZone; } set { this.clientTimeZone = value; } }
        public IList<MenuItem> UserMenuItems { get; set; }
        public string UserDefaultMenu { get; set; }
        public string RoleNames { get; set; }

        public UserInfo() { }
        public UserInfo(string argUserId, string argUserToken, string argThemeName)
        {
            this.UserId = argUserId;
            this.SecurityToken = argUserToken;
            this.ThemeName = argThemeName;
        }
        public UserInfo(long id, string argUserId, string argUserToken, string argThemeName)
        {
            this.UserId = argUserId;
            this.SecurityToken = argUserToken;
            this.ThemeName = argThemeName;
            this.UserAliasId = id;
        }


        public long? DefaultAccountId { get; set; }
    }
}