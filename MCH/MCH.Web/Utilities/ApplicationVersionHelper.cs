﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MCH.Web.Utilities
{
    public class ApplicationVersionHelper
    {

        public static VersionInfo GetVersionInfo(string argFullAssemblyName, string argDescription)
        {
            VersionInfo tmpVersionInfo = new VersionInfo();
            try
            {
                string[] tmpInfo = argFullAssemblyName.Split(',');
                string tmpName = tmpInfo[0];
                string tmpVersion = tmpInfo[1];
                string tmpFullVersion = tmpVersion.Split('=')[1];
                int tmpBuild = int.Parse(tmpFullVersion.Split('.')[2]);
                int tmpRevision = int.Parse(tmpFullVersion.Split('.')[3]);
                DateTime tmpBuildDate = new DateTime(2000, 1, 1).AddDays(tmpBuild).AddSeconds(tmpRevision * 2);
                String tmpFullDate = tmpBuildDate.ToUniversalTime().ToString("dd-MMMM-yyyy", CultureInfo.InvariantCulture);

                tmpVersionInfo.Name = tmpName;
                tmpVersionInfo.Description = argDescription;
                tmpVersionInfo.Version = tmpFullVersion;
                tmpVersionInfo.UtcBuildDate = tmpFullDate;
            }
            catch (Exception)
            {
                tmpVersionInfo.Name = String.Empty;
                tmpVersionInfo.Description = String.Empty;
                tmpVersionInfo.UtcBuildDate = String.Empty;
                tmpVersionInfo.Version = String.Empty;
            }
            return tmpVersionInfo;
        }

        public static VersionInfo GetUIClientVersionInfo()
        {
            VersionInfo tmpClientVersionInfo = ApplicationVersionHelper.GetVersionInfo(Assembly.GetExecutingAssembly().FullName, "UI Client");
            return tmpClientVersionInfo;
        }
    }
}