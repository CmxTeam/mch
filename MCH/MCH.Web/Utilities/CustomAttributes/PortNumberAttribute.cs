﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace MCH.Web.Utilities.CustomAttributes
{
    public class PortNumberAttribute : ValidationAttribute, IClientValidatable
    {
        private Regex _regex = new Regex(@"^(6553[0-5]|655[0-2]\d|65[0-4]\d\d|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}|0)$");
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRegexRule("", _regex.ToString())
            {
                ValidationType = "regex",
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName())
            };
        }
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            string valueAsString = value.ToString();
            return valueAsString != null && _regex.Match(valueAsString.Trim()).Length > 0;
        }
    }
}