﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Models;
using MCH.Web.ShellApi;
using MCH.Web.ShellApi.Model;
using System.Web.Routing;
using MCH.Web.Models.Account;
using MCH.Web.Utilities;

namespace MCH.Web.Controllers
{
    public class AccountController : Controller
    {
        private IAuthenticationService AuthenticationService = null;

        protected override void Initialize(RequestContext requestContext)
        {
            if (this.AuthenticationService == null)
            {
                this.AuthenticationService = ServiceFactory.CreateAuthenticationService();
            }
            base.Initialize(requestContext);
        }

        //
        // GET: /Account/Login
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DoLogin(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    switch (AuthenticationService.CheckUserValidity(model.UserId, model.Password))
                    {
                        case UserValidaitonResult.UserIsValid:
                            {
                                return this.AuthenticateUserHelper(model, returnUrl);
                            }
                        case UserValidaitonResult.InvalidPassword:
                            {
                                ModelState.AddModelError("Password", "Incorrect password, please try again");
                                break;
                            }
                        case UserValidaitonResult.PasswordExpired:
                            {
                                //UserInfo tmpUserInfo = AuthenticationService.SignIn(model.UserId, model.Password, model.RememberMe);
                                //SessionManager.UserInfo = tmpUserInfo;
                                //return RedirectToAction("ChangePassword", new
                                //{
                                //    reason = "Your password has been expired, it is strongly recommended to change it!"
                                //});
                                return this.AuthenticateUserHelper(model, returnUrl);
                            }
                        case UserValidaitonResult.FirstTimeLogin:
                            {
                                //UserInfo tmpUserInfo = AuthenticationService.SignIn(model.UserId, model.Password, model.RememberMe);
                                //SessionManager.UserInfo = tmpUserInfo;
                                //return RedirectToAction("ChangePassword", new
                                //{
                                //    reason = "You are logging in first time, is it recommended to change your password!"
                                //});
                                return this.AuthenticateUserHelper(model, returnUrl);
                            }
                        case UserValidaitonResult.PasswordReset:
                            {
                                return this.AuthenticateUserHelper(model, Url.Action("ChangePasswordView", "Account").ToString());
                            }
                        case UserValidaitonResult.InvalidUserId:
                            {
                                ModelState.AddModelError("UserId", "Incorrect username, please try again");
                                break;
                            }
                        case UserValidaitonResult.UserIsInactive:
                            {
                                //TODO : Harut -> add contact administrators contact info here.
                                ModelState.AddModelError("", "Your account has been deactivated - please contact Administrator");
                                break;
                            }
                        case UserValidaitonResult.UserIsLocked:
                            {
                                //TODO : Harut -> add contact administrators contact info here.
                                ModelState.AddModelError("", "Your account is locked - please contact Administrator");
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // if we got this far, return to the same page, there are model errors to show.
            return View("Login",model);

        }


        public ActionResult Logoff()
        {
            this.AuthenticationService.SignOut();
            return RedirectToAction("Login", "Account");
        }


        #region Helper Methods
        private ActionResult AuthenticateUserHelper(LoginViewModel model, string returnUrl)
        {
            UserAuthModel tmpUserInfo = AuthenticationService.SignIn(model.UserId, model.Password, model.RememberMe);
            tmpUserInfo.ClientTimeZoneOffset = model.ClientTimeZone;
            if (tmpUserInfo != null)
            {
                if (tmpUserInfo.DefaultAccountId.HasValue)
                {
                    SessionManager.AccountId = tmpUserInfo.DefaultAccountId.Value;

                    tmpUserInfo.UserMenuItems = AuthenticationService.GetUserMenuItems(tmpUserInfo.RoleNames, tmpUserInfo.DefaultAccountId.Value);
                }
            }
            SessionManager.UserInfo = tmpUserInfo;


            AuthenticationService.SetAuthTicket(model.UserId, model.RememberMe, tmpUserInfo);
            if (!String.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl) && returnUrl != "/")
            {
                ////redirect to controller ignoring the action
                //int tmpIndex = returnUrl.IndexOf('/', 1);

                //string tmpUrl = returnUrl;
                //if (tmpIndex != -1)
                //{
                //    tmpUrl = returnUrl.Substring(0, tmpIndex);
                //}
                //add cookie for user theme name.
                if (tmpUserInfo != null)
                {
                    Response.Cookies.Add(new HttpCookie(MCH.Web.Utilities.Constants.Keys.ThemeName, tmpUserInfo.ThemeName));
                }
                return Redirect(returnUrl);
            }
            else
            {
               

                return RedirectToAction("Index","Home");
            }
        }
        #endregion


    }
}