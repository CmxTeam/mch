﻿using MCH.BLL.DAL;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Customs;
using MCH.Web.Models.DGChecklist;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class CustomsController : Controller
    {

        public PartialViewResult GetCustomsHistoryView(GetCustomsHistoryModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = new CustomsHistoryViewModel()
                {
                    ShipmentId = model.ShipmentId,
                    TaskId = model.TaskId
                };

                var tmpAwb = unitOfWork.Context.AWBs.SingleOrDefault(item => item.Id == model.ShipmentId);

                var tmpCustomsHeader = unitOfWork.Context.GetAWBCustomsHeader(model.ShipmentId).OrderBy(item => item.Part).ToList();

                tmpModel.Parts = tmpCustomsHeader.Select(item => new SelectListItem { Text = item.Part, Value = item.Part });
                var tmpSelectByPart = tmpCustomsHeader[0];
                tmpModel.Consignee = tmpSelectByPart.AgentName;
                tmpModel.AWB = tmpSelectByPart.AwbNumber;

                return PartialView("_CustomsHistoryView", tmpModel);
            }
        }

        public PartialViewResult GetCustomsHistoryDetails(CustomsHistoryDetailsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = new CustomsDetailsViewModel()
                {
                    ShipmentId = model.ShipmentId,
                    TaskId = model.TaskId
                };

                var tmpSelectByPart = unitOfWork.Context.GetAWBCustomsHeader(model.ShipmentId).Where(item => item.Part == model.Part).FirstOrDefault();
                tmpModel.WayBillId = tmpSelectByPart.WaybillId;
                tmpModel.CarrierFlight = tmpSelectByPart.CarrierFlight;
                tmpModel.AirrivalAirport = tmpSelectByPart.ArrivalAirport;
                tmpModel.ArrivalDate = tmpSelectByPart.ArrivalDate.HasValue ? tmpSelectByPart.ArrivalDate.Value.ToShortDateString() : String.Empty;
                tmpModel.AirportTerminal = tmpSelectByPart.ArrivalTerminal;
                tmpModel.Status = tmpSelectByPart.CStatus + " "+ (tmpSelectByPart.TxnDate.HasValue ? tmpSelectByPart.TxnDate.Value.ToString("MM/dd/yyyy HH:mm") :String.Empty) ;
                tmpModel.QTY = tmpSelectByPart.Qty;
                tmpModel.EntityType = tmpSelectByPart.EntryType;
                tmpModel.EntityNo = tmpSelectByPart.EntryNo;

                return PartialView("_CustomsHistoryDetails", tmpModel);
            }
        }


        public JsonResult GetCustomsHistoryData(CustomsHistoryFilter filter)
        {
            List<CustomsRow> tmpList = new List<CustomsRow>();

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpCustomsDetails = unitOfWork.Context.GetAWBCustomsDetails(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.WayBillId);

                tmpList = tmpCustomsDetails.Select(item => new CustomsRow
                {
                    Code = item.Code,
                    Description = item.CDescription,
                    EntryNo = item.EntryNo,
                    EntryType = item.EntryType,
                    HWB = item.HWB,
                    Qty = item.Qty,
                    Status = item.Status,
                    StatusTime = item.StatusTime,
                    Text = item.CText,
                    Total = item.Total
                }).ToList();

                if (tmpList.Any())
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpList.Count,
                        iTotalDisplayRecords = tmpList[0].Total,
                        aaData = tmpList
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = tmpList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        public PartialViewResult GetNotocView(GetNotocViewModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpNototMasterDetails = unitOfWork.Context.GetNotocMaster(model.FlightId).FirstOrDefault();
                if (tmpNototMasterDetails != null)
                {
                    var tmpModel = new NotocDetailsModel()
                    {
                        TaskId = model.TaskId,
                        NotocId = tmpNototMasterDetails.NotocId,
                        Carrier = tmpNototMasterDetails.Carrier,
                        FlightNumber = tmpNototMasterDetails.FlightNumber,
                        FlightId = model.FlightId,
                        FlightDate = tmpNototMasterDetails.FlightDate,
                        StationOfLoading = tmpNototMasterDetails.StationOfLoading,
                        Origin = tmpNototMasterDetails.Origin,
                        Destination = tmpNototMasterDetails.Destination,
                        AircraftRegNumber = tmpNototMasterDetails.AircraftRegistration,
                        LoadingSupervisor = tmpNototMasterDetails.LoadingSupervisor,
                        OtherInformation = tmpNototMasterDetails.OtherInfo,
                        PreparedBy = tmpNototMasterDetails.PreparedBy,
                        CheckedBy = tmpNototMasterDetails.CheckedBy
                    };
                    return PartialView("_TaskNotocView", tmpModel);
                }
                else
                {
                    return PartialView("_TaskNotocView", new NotocDetailsModel() { TaskId = model.TaskId, FlightId = model.FlightId });
                }
            }
        }


        public JsonResult GetNotocDGData(NotocDGGridFilter filter)
        {
            var tmpList = new List<NotocDGRow>();

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpNotocDGData = unitOfWork.Context.GetNotocDGDetails(filter.FlightId).ToList();
                if (tmpNotocDGData.Any())
                {
                    foreach (var item in tmpNotocDGData)
                    {
                        tmpList.Add(new NotocDGRow
                        {
                            Id = item.DetailId,
                            NotocId = item.NotocId,
                            IMPCode = item.IMPCode,
                            NetQTIorTI = item.PackingTypeAndQty,
                            NumberOfPackages = item.NoOfpackages,
                            PackingGroup = item.PackingGroup,
                            PSN = item.ProperShippingName,
                            RadioActiveCat = item.RadioactiveMaterials,
                            SubRisk = item.SubRisk,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            UNClass = item.UNClass,
                            UNClassId = item.UNClassId,
                            UNNumber = item.UNNumber,
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            CAO = item.AircraftTypeCode,
                            CAOId = item.AircraftTypeId,
                            Compartment = item.AircraftPosition,
                            Destination = item.Destination,
                            ERGCode = item.EmergencyResponseCode,
                            FlightId = filter.FlightId
                        });
                    }
                }
            }

            return Json(new
            {
                sEcho = filter.sEcho,
                iTotalRecords = tmpList.Count,
                iTotalDisplayRecords = tmpList.Count,
                aaData = tmpList
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveNotocMaster(NotocMasterModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                unitOfWork.Context.UpdateNotocMaster(model.FlightId, SessionManager.UserInfo.UserLocalId, model.CheckedBy, model.LoadingSupervisor, null, model.OtherInfo);
                var jsonData = new { };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetNotocNonHazData(NotocNonHazGridFilter filter)
        {
            var tmpList = new List<NotocNoHazRow>();

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpNotocDGData = unitOfWork.Context.GetNotocOtherCargoDetails(filter.FlightId).ToList();
                if (tmpNotocDGData.Any())
                {
                    foreach (var item in tmpNotocDGData)
                    {
                        tmpList.Add(new NotocNoHazRow
                        {
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            Code = item.Code,
                            Description = item.ContentsDescription,
                            Destination = item.Destination,
                            Id = item.DetailId,
                            LoadingPosition = item.AircraftPosition,
                            PackageCount = item.NoOfpackages,
                            SupInfo = item.SupplementaryInfo,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            NotocId = item.NotocId.HasValue ? item.NotocId.Value : 0,
                            FlightId = filter.FlightId
                        });
                    }
                }
            }

            return Json(new
            {
                sEcho = filter.sEcho,
                iTotalRecords = tmpList.Count,
                iTotalDisplayRecords = tmpList.Count,
                aaData = tmpList
            }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetAddEditNotocDGView(NotocDGRow model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = model;
                if (model.Id != 0)
                {
                    var tmpNotocDGData = unitOfWork.Context.GetNotocDGDetails(model.FlightId).Where
                        (item => item.DetailId == model.Id).Select(item => new NotocDGRow
                        {
                            Id = item.DetailId,
                            NotocId = item.NotocId,
                            IMPCode = item.IMPCode,
                            NetQTIorTI = item.PackingTypeAndQty,
                            NumberOfPackages = item.NoOfpackages,
                            PackingGroup = item.PackingGroup,
                            PSN = item.ProperShippingName,
                            RadioActiveCat = item.RadioactiveMaterials,
                            SubRisk = item.SubRisk,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            UNClass = item.UNClass,
                            UNClassId = item.UNClassId,
                            UNNumber = item.UNNumber,
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            CAO = item.AircraftTypeCode,
                            CAOId = item.AircraftTypeId,
                            Compartment = item.AircraftPosition,
                            Destination = item.Destination,
                            ERGCode = item.EmergencyResponseCode,
                            FlightId = model.FlightId
                        }).FirstOrDefault();
                    tmpModel = tmpNotocDGData;
                }
                return PartialView("_AddEditNotocDG", tmpModel);
            }
        }

        public PartialViewResult GetAddEditNotocSPView(NotocNoHazRow model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = model;
                if (model.Id != 0)
                {
                    var tmpNotocSPData = unitOfWork.Context.GetNotocOtherCargoDetails(model.FlightId).Where
                        (item => item.DetailId == model.Id).Select(item => new NotocNoHazRow
                        {
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            Code = item.Code,
                            Description = item.ContentsDescription,
                            Destination = item.Destination,
                            Id = item.DetailId,
                            LoadingPosition = item.AircraftPosition,
                            PackageCount = item.NoOfpackages,
                            SupInfo = item.SupplementaryInfo,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            NotocId = item.NotocId.HasValue ? item.NotocId.Value : 0,
                            FlightId = model.FlightId
                        }).FirstOrDefault();
                    tmpModel = tmpNotocSPData;
                }
                return PartialView("_AddEditNotocNoHaz", tmpModel);
            }
        }


        public JsonResult DeleteNotocDG(long Id)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    var tmpItemToDelete = unitOfWork.Context.FlightNotocDGDetails.FirstOrDefault(item => item.Id == Id);
                    if (tmpItemToDelete != null)
                    {
                        unitOfWork.Context.FlightNotocDGDetails.Remove(tmpItemToDelete);
                        unitOfWork.Context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not delete Dangerous Goods info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }


        public JsonResult FinalizeLACheckResult(ChecklistResultsParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                SaveLAChecklistInner(param, unitOfWork);

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                unitOfWork.Context.Transactions.Add(new Transaction()
                {
                    Description = "LA Checklist Completed",
                    EntityId = param.EntityId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    StatusId = tmpTask.StatusId,
                    Reference = tmpTask.TaskReference,
                    TaskId = tmpTask.Id,
                    TaskTypeId = tmpTask.TaskTypeId,
                    UserName = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname
                });

                tmpTask.EndDate = DateTime.Now;
                tmpTask.StatusId = (int)enumTaskStatuses.Completed;

                unitOfWork.Context.TaskTransactions.Add(new TaskTransaction()
                {
                    Description = "Task Completed",
                    RecDate = DateTime.Now,
                    StatusTimestamp = DateTime.Now,
                    StatusId = (int)enumTaskStatuses.Completed,
                    TaskId = tmpTask.Id,
                    UserId = SessionManager.UserInfo.UserLocalId
                });

                unitOfWork.Context.HostPlus_CIMPOutboundQueue.Add(new HostPlus_CIMPOutboundQueue()
                {
                    AWBId = param.EntityId,
                    Emailed = false,
                    Timestamp = DateTime.Now,

                    Transmitted = false,
                    StationId = SessionManager.UserInfo.UserDefaultStationId.Value,
                    MessageType = "LAR"
                });

                unitOfWork.Context.SaveChanges();


                var tmpjsonData = new { };

                return Json(tmpjsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FinalizeDGCheckResult(ChecklistResultsParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                SaveDGChecklistInner(param, unitOfWork);

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                unitOfWork.Context.Transactions.Add(new Transaction()
                {
                    Description = "DG Checklist Completed",
                    EntityId = param.EntityId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    StatusId = tmpTask.StatusId,                    
                    TaskId = tmpTask.Id,
                    TaskTypeId = tmpTask.TaskTypeId,
                    UserName = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname
                });

                tmpTask.EndDate = DateTime.Now;
                tmpTask.StatusId = (int)enumTaskStatuses.Completed;

                unitOfWork.Context.TaskTransactions.Add(new TaskTransaction()
                {
                    Description = "Task Completed",
                    RecDate = DateTime.Now,
                    StatusTimestamp = DateTime.Now,
                    StatusId = (int)enumTaskStatuses.Completed,
                    TaskId = tmpTask.Id,
                    UserId = SessionManager.UserInfo.UserLocalId
                });

                unitOfWork.Context.HostPlus_CIMPOutboundQueue.Add(new HostPlus_CIMPOutboundQueue()
                {
                    AWBId = param.EntityId,
                    Emailed = false,
                    Timestamp = DateTime.Now,
                    
                    Transmitted = false,
                    StationId = SessionManager.UserInfo.UserDefaultStationId.Value,
                    MessageType = "DGC"
                });

                unitOfWork.Context.SaveChanges();


                var tmpjsonData = new { };

                return Json(tmpjsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveDGCheckResult(ChecklistResultsParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                SaveDGChecklistInner(param, unitOfWork);

                var tmpTask = unitOfWork.Context.Tasks.Single(item=> item.Id == param.TaskId);

                unitOfWork.Context.Transactions.Add(new Transaction()
                {
                    Description = "DG Checklist Saved",
                    EntityId = param.EntityId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    StatusId = tmpTask.StatusId,
                    TaskId = tmpTask.Id,
                    TaskTypeId = tmpTask.TaskTypeId,
                    UserName = SessionManager.UserInfo.Firstname + " "+ SessionManager.UserInfo.Lastname
                });

                unitOfWork.Context.SaveChanges();


                var tmpjsonData = new { };

                return Json(tmpjsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveLACheckResult(ChecklistResultsParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                SaveLAChecklistInner(param, unitOfWork);

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                unitOfWork.Context.Transactions.Add(new Transaction()
                {
                    Description = "LA Checklist Saved",
                    EntityId = param.EntityId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    StatusId = tmpTask.StatusId,
                    TaskId = tmpTask.Id,
                    TaskTypeId = tmpTask.TaskTypeId,
                    UserName = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname
                });

                unitOfWork.Context.SaveChanges();


                var tmpjsonData = new { };

                return Json(tmpjsonData, JsonRequestBehavior.AllowGet);
            }
        }

        

        public JsonResult AddEditNotocDG(NotocDGRow model)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    int tmpResult = unitOfWork.Context.AddEditNotocDGDetails(model.FlightId, model.AwbId, model.PSN, model.UNClassId, model.UNNumber, model.PackingGroup, model.Packages, model.NetQTIorTI, model.RadioActiveCat, model.IMPCode, model.ERGCode, model.UldId, model.CAOId, model.Compartment, model.Id, model.SubRisk);
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not update Dangerous Goods info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteNotocSP(long Id)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    var tmpItemToDelete = unitOfWork.Context.FlightNotocOtherCargoDetails.FirstOrDefault(item => item.Id == Id);
                    if (tmpItemToDelete != null)
                    {
                        unitOfWork.Context.FlightNotocOtherCargoDetails.Remove(tmpItemToDelete);
                        unitOfWork.Context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not delete Cargo Special info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditNotocOtherInfo(NotocNoHazRow model)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    int tmpResult = unitOfWork.Context.AddEditNotocOtherCargoDetails(model.FlightId, model.AwbId, model.Description, model.PackageCount, model.Qty.ToString(), model.SupInfo, model.Code, model.UldId, model.LoadingPosition, model.Id);
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not update Other Special Cargo info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetLAChecklistView(AwbDGChecklistParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                AwbDGChecklistModel tmpModel = new AwbDGChecklistModel();

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                // if Task is Not Assigned, assign to the logged in user
                if (tmpTask.StatusId == (int)enumTaskStatuses.NotAssigned)
                {
                    tmpTask.TaskAssignments.Add(new TaskAssignment() { UserId = SessionManager.UserInfo.UserLocalId, AssignedBy = SessionManager.UserInfo.UserLocalId, RecDate = DateTime.Now, AssignedOn = DateTime.Now });
                    unitOfWork.Context.SaveChanges();
                }

                var tmpLA = unitOfWork.Context.LARCheckLists.OrderByDescending(item => item.RecDate).FirstOrDefault();

                if (tmpLA == null)
                {
                    //TODO : Harut handle this case, return error view with message
                }

                var tmpAwg = unitOfWork.Context.AWBs.SingleOrDefault(i => i.Id == param.AwbId);

                var tmpLAResult = unitOfWork.Context.LARCheckListResultsParents.SingleOrDefault(item => item.EntityId.HasValue && item.EntityId.Value == param.AwbId && item.EntityTypeId == (int)enumEntityTypes.AWB);

                var tmpUserWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == SessionManager.UserInfo.UserLocalId);

                var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

                var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

                DGListModel tmpResult = new DGListModel
                {
                    EntityId = param.AwbId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    TaskId = param.TaskId,
                    Username = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname,
                    ShipmentNumber = tmpAwg.AWBSerialNumber,
                    Place = tmpPlace

                };

                if (tmpLAResult != null)
                {
                    tmpResult.Place = tmpLAResult.Place;
                    tmpResult.CommentedOn = tmpLAResult.CompletedOn;
                    tmpResult.Comments = tmpLAResult.Comments;
                    tmpResult.Username = tmpLAResult.UserName;
                    tmpResult.SignatureData = tmpLAResult.SignatureData;
                    tmpResult.Place = tmpPlace;
                }

                var tmpLAListData = tmpLA.LARCheckListItemHeaders.Where(item => item.IsActive.HasValue && item.IsActive.Value).Select(item => new DGListHeader
                {
                    Id = item.Id,
                    Name = item.ListItemHeader,
                    Items = item.LARCheckListItems.Where(e => e.ParentId == null).Select(e => new DGCheckItem
                    {
                        Id = e.Id,
                        Options = e.ValueList,
                        ParentId = e.ParentId,
                        SubItems = e.LARCheckListItems1.Select(p => new DGCheckItem
                        {
                            Id = p.Id,
                            Options = p.ValueList,
                            Number = p.SortOrder,
                            Text = p.ListItemText,
                            SelectedOption = tmpLAResult != null && tmpLAResult.LARCheckListResults.FirstOrDefault(u => u.CheckListItemId == p.Id) != null ? tmpLAResult.LARCheckListResults.First(u => u.CheckListItemId == p.Id).ResultValue : String.Empty
                        }).ToList(),
                        Text = e.ListItemText,
                        Number = e.SortOrder,
                        SelectedOption = tmpLAResult != null && tmpLAResult.LARCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id) != null ? tmpLAResult.LARCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id).ResultValue : String.Empty
                    }).ToList()
                }).ToList();

                tmpResult.CheckList = tmpLAListData;

                return PartialView("_LAChecklist", tmpResult);
            }
        }

        public PartialViewResult GetDGChecklistView(AwbDGChecklistParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                AwbDGChecklistModel tmpModel = new AwbDGChecklistModel();

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                // if Task is Not Assigned, assign to the logged in user
                if (tmpTask.StatusId == (int)enumTaskStatuses.NotAssigned) 
                {
                    tmpTask.TaskAssignments.Add(new TaskAssignment() { UserId = SessionManager.UserInfo.UserLocalId, AssignedBy = SessionManager.UserInfo.UserLocalId, RecDate = DateTime.Now, AssignedOn = DateTime.Now});
                    unitOfWork.Context.SaveChanges();
                }

                var tmpDG = unitOfWork.Context.DGRCheckLists.OrderByDescending(item => item.RecDate).FirstOrDefault();

                if (tmpDG == null)
                {
                    //TODO : Harut handle this case, return error view with message
                }

                var tmpAwg = unitOfWork.Context.AWBs.SingleOrDefault(i => i.Id == param.AwbId);

                var tmpDGResult = unitOfWork.Context.DGRCheckListResultsParents.SingleOrDefault(item => item.EntityId.HasValue && item.EntityId.Value == param.AwbId && item.EntityTypeId == (int)enumEntityTypes.AWB);

                var tmpUserWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == SessionManager.UserInfo.UserLocalId);

                var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

                var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

                DGListModel tmpResult = new DGListModel
                {
                    EntityId = param.AwbId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    TaskId = param.TaskId,
                    Username = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname,
                    ShipmentNumber = tmpAwg.AWBSerialNumber,
                    Place = tmpPlace

                };

                if (tmpDGResult != null)
                {
                        tmpResult.Place = tmpDGResult.Place;
                        tmpResult.CommentedOn = tmpDGResult.CompletedOn;
                        tmpResult.Comments = tmpDGResult.Comments;
                        tmpResult.Username = tmpDGResult.UserName;
                        tmpResult.SignatureData = tmpDGResult.SignatureData;
                        tmpResult.Place = tmpPlace;
                }

                var tmpDGListData = tmpDG.DGRCheckListItemHeaders.Where(item => item.IsActive.HasValue && item.IsActive.Value).Select(item => new DGListHeader
                {
                    Id = item.Id,
                    Name = item.ListItemHeader,
                    Items = item.DGRCheckListItems.Where(e => e.ParentId == null).Select(e => new DGCheckItem
                    {
                        Id = e.Id,
                        Options = e.ValueList,
                        ParentId = e.ParentId,
                        SubItems = e.DGRCheckListItems1.Select(p => new DGCheckItem
                        {
                            Id = p.Id,
                            Options = p.ValueList,
                            Number = p.SortOrder,
                            Text = p.ListItemText,
                            SelectedOption = tmpDGResult != null && tmpDGResult.DGRCheckListResults.FirstOrDefault(u => u.CheckListItemId == p.Id) != null ? tmpDGResult.DGRCheckListResults.First(u => u.CheckListItemId == p.Id).ResultValue : String.Empty
                        }).ToList(),
                        Text = e.ListItemText,
                        Number = e.SortOrder,
                        SelectedOption = tmpDGResult != null && tmpDGResult.DGRCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id) != null ? tmpDGResult.DGRCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id).ResultValue : String.Empty
                    }).ToList()
                }).ToList();

                tmpResult.CheckList = tmpDGListData;

                return PartialView("_DGChecklist", tmpResult);
            }
        }


        #region Helper Methods

        private static void SaveLAChecklistInner(ChecklistResultsParam param, TasksUnitOfWork unitOfWork)
        {
            var tmpDBResult = unitOfWork.Context.LARCheckListResultsParents.Where(item => item.EntityId == param.EntityId && item.EntityTypeId == (int)enumEntityTypes.AWB).FirstOrDefault();

            var tmpUserWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == SessionManager.UserInfo.UserLocalId);

            var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

            var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

            string tmpUsername = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname;


            if (tmpDBResult == null)
            {
                tmpDBResult = new BLL.DAL.LARCheckListResultsParent();
                tmpDBResult.CompletedOn = DateTime.UtcNow;
                tmpDBResult.EntityId = param.EntityId;
                tmpDBResult.EntityTypeId = (int)enumEntityTypes.AWB;
                tmpDBResult.TaskId = param.TaskId;
                tmpDBResult.RecDate = DateTime.UtcNow;

                unitOfWork.Context.LARCheckListResultsParents.Add(tmpDBResult);
            }

            tmpDBResult.Comments = param.Comments;
            tmpDBResult.SignatureData = param.SignatureData;
            tmpDBResult.UserName = tmpUsername;
            tmpDBResult.Place = tmpPlace;

            unitOfWork.Context.SaveChanges();

            if (param.Items != null && param.Items.Any())
            {
                foreach (var item in param.Items)
                {
                    var tmpResultItemQuery = unitOfWork.Context.LARCheckListResults.Where(e => e.CheckListItemId == item.ItemId);
                    if (tmpDBResult.Id != 0)
                    {
                        tmpResultItemQuery = tmpResultItemQuery.Where(e => e.ParentId == tmpDBResult.Id);
                    }

                    var tmpResutlItem = tmpResultItemQuery.FirstOrDefault();
                    if (tmpResutlItem == null)
                    {
                        tmpResutlItem = new BLL.DAL.LARCheckListResult();
                        tmpResutlItem.LARCheckListResultsParent = tmpDBResult;
                        tmpResutlItem.CheckListItemId = item.ItemId;
                        tmpResutlItem.CreateDate = DateTime.UtcNow;

                        unitOfWork.Context.LARCheckListResults.Add(tmpResutlItem);
                    }
                    tmpResutlItem.LastUpdated = DateTime.UtcNow;
                    tmpResutlItem.ResultValue = item.OptionName;
                    tmpResutlItem.UpdatedBy = tmpUsername;

                }
                unitOfWork.Context.SaveChanges();
            }
        }
        private static void SaveDGChecklistInner(ChecklistResultsParam param, TasksUnitOfWork unitOfWork)
        {
            var tmpDBResult = unitOfWork.Context.DGRCheckListResultsParents.Where(item => item.EntityId == param.EntityId && item.EntityTypeId == (int)enumEntityTypes.AWB).FirstOrDefault();

            var tmpUserWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == SessionManager.UserInfo.UserLocalId);

            var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

            var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

            string tmpUsername = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname;

            
            if (tmpDBResult == null)
            {
                tmpDBResult = new BLL.DAL.DGRCheckListResultsParent();
                tmpDBResult.CompletedOn = DateTime.UtcNow;
                tmpDBResult.EntityId = param.EntityId;
                tmpDBResult.EntityTypeId = (int)enumEntityTypes.AWB;
                tmpDBResult.TaskId = param.TaskId;
                tmpDBResult.RecDate = DateTime.UtcNow;

                unitOfWork.Context.DGRCheckListResultsParents.Add(tmpDBResult);
            }

            tmpDBResult.Comments = param.Comments;
            tmpDBResult.SignatureData = param.SignatureData;
            tmpDBResult.UserName = tmpUsername;
            tmpDBResult.Place = tmpPlace;

            unitOfWork.Context.SaveChanges();

            if (param.Items != null && param.Items.Any())
            {
                foreach (var item in param.Items)
                {
                    var tmpResultItemQuery = unitOfWork.Context.DGRCheckListResults.Where(e => e.CheckListItemId == item.ItemId);
                    if (tmpDBResult.Id != 0)
                    {
                        tmpResultItemQuery = tmpResultItemQuery.Where(e => e.ParentId == tmpDBResult.Id);
                    }

                    var tmpResutlItem = tmpResultItemQuery.FirstOrDefault();
                    if (tmpResutlItem == null)
                    {
                        tmpResutlItem = new BLL.DAL.DGRCheckListResult();
                        tmpResutlItem.DGRCheckListResultsParent = tmpDBResult;
                        tmpResutlItem.CheckListItemId = item.ItemId;
                        tmpResutlItem.CreateDate = DateTime.UtcNow;

                        unitOfWork.Context.DGRCheckListResults.Add(tmpResutlItem);
                    }
                    tmpResutlItem.LastUpdated = DateTime.UtcNow;
                    tmpResutlItem.ResultValue = item.OptionName;
                    tmpResutlItem.UpdatedBy = tmpUsername;

                }
                unitOfWork.Context.SaveChanges();
            }
        }
        #endregion


    }
}
