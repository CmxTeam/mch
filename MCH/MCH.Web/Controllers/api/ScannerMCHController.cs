﻿using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Controllers.api
{
    public class ScannerMCHController : ApiController
    {
        public UserSession LoginByPin(string station, string pin)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.LoginByPin(station, pin);
            }
        }

        public UserSession LoginByUserName(string station, string userName, string password, Platforms platformID)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.LoginByUserName(station, userName, password, platformID);
            }
        }

        public LoginTypes LoginPreference(string gatewayName)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.LoginPreference(gatewayName);
            }
        }

        public WarehouseLocationModel[] GetWarehouseLocations(string station, LocationType location)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetWarehouseLocations(station, location);
            }
        }

        public MainMenuItem[] GetMainMenu(string guid, long userID, string gateway, string station, out int latestVersion)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetMainMenu(guid, userID, gateway, station, out latestVersion);
            }
        }

        public long GetLocationIdByLocationBarcode(string station, string barcode)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetLocationIdByLocationBarcode(station, barcode);
            }
        }

        public void AddTaskSnapshotReference(long taskId, string reference)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                scannerUnit.AddTaskSnapshotReference(taskId, reference);
            }
        }

        public bool ExecuteNonQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteNonQuery(station, query);
            }
        }

        public DataTable ExecuteQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteQuery(station, query);
            }
        }

        public AwbInfo GetAwbInfo(string station, string shipment)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetAwbInfo(station, shipment);
            }
        }

    }
}
