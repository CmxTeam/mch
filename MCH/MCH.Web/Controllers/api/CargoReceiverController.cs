﻿using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Controllers.api
{
    public class CargoReceiverController : ApiController
    {
        public CargoReceiverFlight[] GetCargoReceiverFlights(string station, int userID, TaskStatuses status, string carrierNo, string origin, string flightNo, ReceiverSortFields sortBy)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetCargoReceiverFlights(station, userID, status, carrierNo, origin, flightNo, sortBy);
            }
        }

        public TransactionStatus RecoverFlight(string station, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RecoverFlight(station, taskId, userId);
            }
        }

        public FlightUldInfo GetFlightULDs(long flightManifestId, RecoverStatuses status, string filter)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetFlightULDs(flightManifestId, status, filter);
            }
        }

        public TransactionStatus SwitchBUPMode(string station, long uldId, long userId, long manifestId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.SwitchBUPMode(station, uldId, userId, manifestId);
            }
        }

        public TransactionStatus UpdateFlightETA(string station, long flightManifestId, long taskId, long userId, DateTime eta)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.UpdateFlightETA(station, flightManifestId, taskId, userId, eta);
            }
        }

        public int GetForkliftCount(long userId, long taskId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetForkliftCount(userId, taskId);
            }
        }

        public Uld GetFlightULD(long uldId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetFlightULD(uldId);
            }
        }

        public void RecoverUld(long uldId, long userId, long flightManifestId, long locationId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.RecoverUld(uldId, userId, flightManifestId, locationId);
            }
        }

        public void DropForkliftPieces(long taskId, long userId, int locationId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.DropForkliftPieces(taskId, userId, locationId);
            }
        }

        public void DropPiecesToLocation(long taskId, long userId, int locationId, long forkliftDetailsId, int pieces)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.DropPiecesToLocation(taskId, userId, locationId, forkliftDetailsId, pieces);
            }
        }

        public ValidatedShipment ValidateShipment(long userId, long taskId, long uldId, string shipment)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.ValidateShipment(userId, taskId, uldId, shipment);
            }
        }

        public ForkliftView[] GetForkliftView(long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetForkliftView(taskId, userId);
            }
        }

        public UldView[] GetUldView(long taskId, long userId, long uldid)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetUldView(taskId, userId, uldid);
            }
        }

        public UldView[] GetFlightView(long taskId, long manifestId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetFlightView(taskId, manifestId);
            }
        }

        public TransactionStatus AddForkliftPieces(long detailsId, long taskId, int quantity, long userid)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.AddForkliftPieces(detailsId, taskId, quantity, userid);
            }
        }

        public CargoReceiverFlight GetFlightManifestById(long manifestId, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetFlightManifestById(manifestId, taskId, userId);
            }
        }

        public void RemoveItemsFromForklift(long forkliftDetailsId, int count)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.RemoveItemsFromForklift(forkliftDetailsId, count);
            }
        }

        public void RemoveAllItemsFromForklift(long userId, long taskId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.RemoveAllItemsFromForklift(userId, taskId);
            }
        }

        public AlertModel[] GetFlightAlerts(string port, long manifestId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetFlightAlerts(port, manifestId);
            }
        }

        public AlertModel[] GetUldAlerts(string port, long uldId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetUldAlerts(port, uldId);
            }
        }

        public AlertModel[] GetTaskAlerts(string port, long taskId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetTaskAlerts(port, taskId);
            }
        }

        public AlertModel[] GetAwbAlerts(string port, long awbId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAwbAlerts(port, awbId);
            }
        }

        public FlightProgress GetFlightProgress(long manifestId, long taskId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetFlightProgress(manifestId, taskId);
            }
        }

        public TransactionStatus FinalizeReceiver(long taskId, long manifestId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.FinalizeReceiver(taskId, manifestId, userId);
            }
        }

        public ScannedShipmentInfo[] ScanShipment(long userId, long taskId, string shipmentNumber)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.ScanShipment(userId, taskId, shipmentNumber);
            }
        }

        public TransactionStatus AddToForklift(long taskId, long userId, long uldId, long hwbId, long awbId, int count)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.AddToForklift(taskId, userId, uldId, hwbId, awbId, count);
            }
        }

        public AvailableItmes[] GetAvailableCarriers(string station, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAvailableCarriers(station, userId);
            }
        }

        public DateTime[] GetAvailableEtds(long carrierId, string station, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAvailableEtds(carrierId, station, userId);
            }
        }

        public AvailableFlight[] GetAvailableFlights(long carrierId, DateTime etd, string station, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAvailableFlights(carrierId, etd, station, userId);
            }
        }

        public AvailableFlight[] GetAvailableFlightsByEta(long carrierId, DateTime eta, string station, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAvailableFlightsByEta(carrierId, eta, station, userId);
            }
        }

        public DateTime[] GetAvailableEtas(long carrierId, string station, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAvailableEtas(carrierId, station, userId);
            }
        }

        public TransactionStatus LinkTaskToUserId(long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.LinkTaskToUserId(taskId, userId);
            }
        }

        public int GetAwbPiecesCountInLocation(long awbId, long locationId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetAwbPiecesCountInLocation(awbId, locationId);
            }
        }

        public TransactionStatus RelocateAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RelocateAwbPieces(awbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
        }

        public int GetLocationId(string locationName, string station)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.GetLocationId(locationName, station);
            }
        }

    }
}
