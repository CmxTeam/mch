﻿using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class CommonController : Controller
    {
        public JsonResult GetCarriers(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpCarriersQuery = unitOfWork.CarrierRepostiory.Get().Where(item=> !String.IsNullOrWhiteSpace(item.CarrierCode));
                if (!String.IsNullOrWhiteSpace(query)) 
                {
                    tmpCarriersQuery = tmpCarriersQuery.Where(item => 
                        (!String.IsNullOrWhiteSpace(item.CarrierName) && item.CarrierName.IndexOf(query,0,StringComparison.InvariantCultureIgnoreCase) != -1)||
                        (!String.IsNullOrWhiteSpace(item.CarrierCode) && item.CarrierCode.IndexOf(query,0,StringComparison.InvariantCultureIgnoreCase) != -1));
                }

                var tmpCarriers = tmpCarriersQuery.Select(item => new { name = (item.CarrierName + " (" + item.CarrierCode + ")"), id = item.Id });

                return Json(tmpCarriers, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetTaskTypes(string query) 
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskTypeQuery = unitOfWork.TaskTypeRepository.Get().Where(item => !String.IsNullOrWhiteSpace(item.Description));
                if (!String.IsNullOrWhiteSpace(query))
                {
                    tmpTaskTypeQuery = tmpTaskTypeQuery.Where(item =>
                        (!String.IsNullOrWhiteSpace(item.Description) && item.Description.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) != -1) );
                }

                var tmpCarriers = tmpTaskTypeQuery.Select(item => new { name = item.Description, id = item.Id });

                return Json(tmpCarriers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTaskStatuses(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpTaskStatusesQuery = unitOfWork.StatusRepository.Get(item => item.EntityTypeId == (int)enumEntityTypes.Task);

                var tmpTaskStatuses = tmpTaskStatusesQuery.Select(item => new { name = item.DisplayName, id = item.Id });

                return Json(tmpTaskStatuses, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetStations(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
               var tmpPortQuery =  unitOfWork.PortRepository.Get();

               if (!String.IsNullOrWhiteSpace(query)) 
               {
                   tmpPortQuery = tmpPortQuery.Where(item => item.IATACode.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) != -1);
               }

               var tmpStations = tmpPortQuery.Select(item => new { name = item.IATACode, id = item.Id });

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUsers(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpPortQuery = unitOfWork.UserProfileRepository.Get();

                if (!String.IsNullOrWhiteSpace(query))
                {
                    tmpPortQuery = tmpPortQuery.Where(item => 
                        (!String.IsNullOrWhiteSpace(item.FirstName) &&  item.FirstName.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) != -1) || 
                        (!String.IsNullOrWhiteSpace(item.LastName) && item.LastName.IndexOf(query,0, StringComparison.InvariantCultureIgnoreCase)!= -1));
                }

                var tmpStations = tmpPortQuery.Select(item => new { name = item.FirstName + " " + item.LastName, id = item.Id });

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomers(string prefixText) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCustomersQuery = unitOfWork.Context.Customers.AsQueryable();
                
                if (!String.IsNullOrWhiteSpace(prefixText))
                {
                    tmpCustomersQuery = tmpCustomersQuery.Where(item =>
                        (item.Name != null && item.Name.StartsWith(prefixText.Trim())));
                }

                var tmpCarriers = tmpCustomersQuery.Select(item => new { Text = item.Name, Value = item.Id, Code= item.AccountNumber, Type = "" });

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetShippers(string prefixText) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpShippers = unitOfWork.Context.SearchShipper(String.Empty, prefixText);

                var tmpCarriers = tmpShippers.Select(item => new { Text = item.Name, Value = item.Id, Code = item.IATACode, Type = item.STypeId });

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDrivers(string prefixText) 
        {
             using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpDrivers = unitOfWork.Context.SearchCarrierDriver("",prefixText);

                var tmpCarriers = tmpDrivers.Select(item => new { 
                    Text = item.DriverName,
                    Value = item.Id, 
                    TrackingCo = item.TruckingCo,
                    TrackingCoId = item.TruckingCompanyId,
                    PrimaryIdType = item.CredentialTypeId,
                    LicenseNumber = item.LicenseNumber,
                    LicensePhoto = item.LicenseImage,
                    DriverPhoto = item.DriverPhoto
                });
                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
             }
        }

    }
}
