﻿using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            if (SessionManager.UserInfo != null && !String.IsNullOrWhiteSpace(SessionManager.UserInfo.UserDefaultMenu))
            {
                string tmpActionName = SessionManager.UserInfo.UserDefaultMenu.Split('/')[1];
                string tmpControllerName = SessionManager.UserInfo.UserDefaultMenu.Split('/')[0];
                ViewBag.DefaultAction = tmpActionName;
                ViewBag.DefaultController = tmpControllerName;
            }

            return View();
        }
    }
}