﻿using MCH.BLL.Units;
using MCH.Web.Models.Awbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class AwbController : Controller
    {

        public JsonResult GetUNClasses(UNClassSimpleFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpUnClasses = unitOfWork.Context.UNClasses.AsQueryable();
                if (filter != null) { 

                if (!String.IsNullOrWhiteSpace(filter.Term))
                {
                    tmpUnClasses = tmpUnClasses.Where(item => item.Description.IndexOf(filter.Term) > 0);
                }
                if (filter.Id.HasValue && filter.Id.Value != 0) 
                {
                    tmpUnClasses = tmpUnClasses.Where(item => item.Id == filter.Id);
                }
                }

                var tmpJsonData = tmpUnClasses.Select(item => new { id = item.Id, text = item.Description });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AircraftTypes(AirCraftSimpleFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpAirType = unitOfWork.Context.AircraftTypes.AsQueryable();
                if (filter != null)
                {
                    if (!String.IsNullOrWhiteSpace(filter.Term))
                    {
                        tmpAirType = tmpAirType.Where(item => item.AircraftTypeCode.IndexOf(filter.Term) > 0);
                    }

                    if (filter.Id.HasValue && filter.Id.Value != 0)
                    {
                        tmpAirType = tmpAirType.Where(item => item.Id == filter.Id);
                    }
                }

                var tmpJsonData = tmpAirType.Select(item => new { id = item.Id, text = item.AircraftTypeCode });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAwbsBy(AwbsListSimpleFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpAwbs = unitOfWork.Context.AWBs.AsQueryable();

                if (filter != null)
                {
                    if (filter.FlightId.HasValue && filter.FlightId.Value != 0)
                    {
                        tmpAwbs = unitOfWork.Context.ManifestAWBDetails.Where(item => item.FlightManifestId == filter.FlightId).Select(item => item.AWB);
                    }

                    if (filter.CarrierId.HasValue && filter.CarrierId.Value != 0)
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.CarrierId == filter.CarrierId.Value);
                    }
                    if (filter.DestinationId.HasValue && filter.DestinationId.Value == filter.DestinationId.Value)
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.DestinationId == filter.DestinationId.Value);
                    }
                    if (filter.OriginId.HasValue && filter.OriginId.Value != 0)
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.OriginId == filter.OriginId.Value);
                    }

                    if (!String.IsNullOrWhiteSpace(filter.Term))
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.AWBSerialNumber.IndexOf(filter.Term) > 0);
                    }
                    if (filter.Id.HasValue && filter.Id.Value != 0) 
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.Id == filter.Id);
                    }

                }

                var tmpJsonData = tmpAwbs.Select(item => new { id = item.Id, text = item.AWBSerialNumber });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }



    }
}
