﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Assignment
{
    public class TaskUserAssignments
    {
        public long TaskId { get; set; }
        public string UserIdString { get; set; }

        public long OwnerId { get; set; }
    }
}