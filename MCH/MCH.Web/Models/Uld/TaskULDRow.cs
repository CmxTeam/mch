﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Uld
{
    public class TaskULDRow
    {
        public string Type { get; set; }

        public string Reference { get; set; }

        public long Id { get; set; }

        public int? Pieces { get; set; }

        public int? STC { get; set; }

        public bool? IsBUP { get; set; }

        public int ? StatusId { get; set; }

        public string Location { get; set; }

        public string Prefix { get; set; }

        public double? TotalWeight { get; set; }

        public double? TotalVolume { get; set; }

        public double? TareWeight { get; set; }

        public int NumberAwbs { get; set; }

        public double? FreightWeight { get; set; }
    }
}