﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Media
{
    public class TaskMedia
    {
        public long TaskId {get;set;}
        public List<MediaModel> MediaArray { get; set; }
    }
}