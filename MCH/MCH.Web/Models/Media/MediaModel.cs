﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MCH.Web.Models.Media
{
    public class MediaModel
    {

        public string ImageLargUrl { get; set; }
        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageThumbUrl { get; set; }
        public string ImageMainUrl { get; set; }
    }



    public class MediaDescription 
    {
        public string UserId { get; set; }
        public string DateTaken { get; set; }
        public string Reason { get; set; }

        public string Location { get; set; }
        public string PCS { get; set; }

        public override string ToString()
        {
            var tmpString = new StringBuilder();

            tmpString.Append(this.UserId);
            tmpString.Append(" ,");
            tmpString.Append(this.DateTaken);
            tmpString.Append(" ,");
            tmpString.Append(this.Reason);
            tmpString.Append(" ,");
            tmpString.Append(this.Location);
            tmpString.Append(" ,");
            tmpString.Append(this.PCS);

            return tmpString.ToString();
        }
    }
}