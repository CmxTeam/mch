﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.DGChecklist
{
    public class ChecklistResultsParam
    {

        public long EntityId { get; set; }
        public string SignatureData { get; set; }
        public long TaskId { get; set; }
        public string Comments { get; set; }
        public IEnumerable<CheckListReslutItem> Items { get; set; }
    }

    public class CheckListReslutItem
    {
        public int ItemId { get; set; }
        public string OptionName { get; set; }
    }
}