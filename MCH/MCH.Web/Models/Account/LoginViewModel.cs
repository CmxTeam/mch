﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Account
{
    public class LoginViewModel
    {
        private const string userNameError = "Incorrect username. Please try again.";
        private const string passwordError = "Incorrect password. Please try again.";
        public long Id
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Username is required.")]
        [Display(Name = "Username")]
        [RegularExpression(@"(\S|(?<=\\) )+", ErrorMessage = userNameError)]
        public string UserId
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression(@"(\S|(?<=\\) )+", ErrorMessage = passwordError)]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "Keep me signed in?")]
        public bool RememberMe
        {
            get;
            set;
        }

        public int ClientTimeZone
        {
            get;
            set;
        }


        public bool IsGuest
        {
            get;
            set;
        }
    }
}