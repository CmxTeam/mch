﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Dashboard
{
    public class DashTileContainer
    {
        public long StationId { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public IList<DashTaskTileModel> Tiles { get; set; }
    }
}