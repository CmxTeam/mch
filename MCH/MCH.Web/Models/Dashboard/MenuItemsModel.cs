﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Dashboard
{
    public class MenuItemsModel
    {
        public IEnumerable<CMX.Shell.Model.MenuItem> MenuItems { get; set; }
        public int ? WarehouseId { get; set; }
    }
}