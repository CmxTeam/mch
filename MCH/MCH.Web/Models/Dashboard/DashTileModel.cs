﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Dashboard
{
    public class DashTaskTileModel
    {

        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int TotalCount { get; set; }

        public IList<TaskByStatusModel> TasksByStatus { get; set; }
    }

    public class TaskByStatusModel 
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int TotalCount { get; set; }
    }
}