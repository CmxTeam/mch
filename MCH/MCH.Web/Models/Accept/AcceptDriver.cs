﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AcceptDriver
    {
        public long? TaskId { get; set; }
        public long? DriverId { get; set; }
        public string Name { get; set; }
        public int?  TruckingCoId { get; set; }
        public int ? PrimaryId { get; set; }
        public string PrimaryString { get; set; }
        public string PhotoString { get; set; }
        public string IDPhotoString { get; set; }
        public string IDNumber { get; set; }
        public bool  IsPhotoMatched { get; set; }

    }
}