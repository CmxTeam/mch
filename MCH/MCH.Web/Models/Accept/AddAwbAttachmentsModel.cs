﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AddAwbAttachmentsModel
    {
        public long? AwbId { get; set; }

        public long? ParentTaskId { get; set; }

        public string AttachmentImage { get; set; }

        public string FileName { get; set; }

        public string Description { get; set; }
    }
}