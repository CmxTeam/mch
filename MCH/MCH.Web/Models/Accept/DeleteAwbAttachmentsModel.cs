﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class DeleteAwbAttachmentsModel
    {
        public string AwbAttachmentIds { get; set; }
        public long? TaskId { get; set; }
        public long? AwbId { get; set; }
    }

    public class DeleteAwbAttributeModel
    {
        public string AwbAttributeIds { get; set; }
        public long? TaskId { get; set; }
        public long? AwbId { get; set; }
    }
}