﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AcceptAwbAttributeModel
    {
        public string FormId { get; set; }
        public long? TaskId { get; set; }
        public long? ParentTaskId { get; set; }
        public long? AwbId { get; set; }

        public long? AttributeId { get; set; }


        #region Hazmat
        public string UNNumber { get; set; }

        public long ? UNClassId { get; set; }
        public string UNClass { get; set; }
        public string UNShippingGame { get; set; }
        #endregion

        #region Perishables
        public string TempFrom { get; set; }
        public string TempTo { get; set; }
        public string TempDegree { get; set; }
        #endregion

        #region Dry Ice
        public int Quantity { get; set; }
        public string Unit { get; set; }
        #endregion

        #region Engine
        public string SerialNumber { get; set; }

        #endregion

        #region Vehicle
        public string Vin { get; set; }
        public long ? StateId { get; set; }
        public string State { get; set; }
        #endregion

        #region License
        public string NoEEI { get; set; }
        public string AES { get; set; }
        public string ITN { get; set; }
        public string AESPost { get; set; }
        #endregion

        #region Other Handling
        public string Code { get; set; }
        public long? HandlingCodeId { get; set; }
        #endregion
    }
}