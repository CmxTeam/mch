﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AddEditAcceptTaskModel
    {
        public long? TaskId { get; set; }
        public AcceptCustomer Customer { get; set; }

        public AcceptDriver Driver { get; set; }

        public IList<AcceptAwbListItem> AwbList { get; set; }

        public int TotalAwbs { get; set; }
        public int TotalPcs { get; set; }
        public double TotalWeight { get; set; }

        public string WeightUom { get; set; }
        public double  TotalVolume { get; set; }
        public string VolumeUom { get;set; }

        public bool IsEdit { get; set; }

    }

    public class AcceptAwbListItem 
    {

        public long? AwbId { get; set; }

        public long? TaskId { get; set; }

        public int? TotalPieces { get; set; }

        public double? TotalVolume { get; set; }

        public double? TotalWeight { get; set; }

        public string WeightUOM { get; set; }

        public string VolumeUom { get; set; }

        public string Reference { get; set; }
    }
}