﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AcceptScreeningDataFilter : DataTableFilter
    {
        public long AwbId { get; set; }
        public long TaskId { get; set; }
    }
}