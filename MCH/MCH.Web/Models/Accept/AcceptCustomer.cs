﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AcceptCustomer
    {
        public long? TaskId { get; set; }
        public string IATACode { get; set; }
        public int ?  CustomerTypeId { get; set; }
        public string CustomerName { get; set; }
    }
}