﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class SelectAwbFilter
    {
        public long? TaskId { get; set; }
        public long ? ParentTaskId { get; set; }
        public long? AwbId { get; set; }

        public string FormId { get; set; }

        public string TaskReference { get; set; }
    }
}