﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class UNClassSimpleFilter
    {
        public long? Id { get; set; }
        public string Term { get; set; }
    }
}