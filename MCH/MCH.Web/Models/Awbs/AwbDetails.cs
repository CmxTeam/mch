﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class AwbDetails
    {
        public long Id { get; set; }

        public string CarrierName { get; set; }

        public string FlightNumber { get; set; }

        public DateTime ETD { get; set; }
    }
}