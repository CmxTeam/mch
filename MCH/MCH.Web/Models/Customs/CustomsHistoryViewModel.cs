﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class CustomsHistoryViewModel
    {
        public long TaskId { get; set; }
        public long ShipmentId { get; set; }

        public string AWB { get; set; }

        public string Consignee { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> Parts { get; set; }

        
    }
}