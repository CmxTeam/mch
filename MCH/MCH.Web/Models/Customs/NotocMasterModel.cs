﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class NotocMasterModel
    {
        public long NotocId { get; set; }
        public string LoadingSupervisor { get; set; }
        public string OtherInfo { get; set; }
        public string CheckedBy { get; set; }

        public long ? FlightId { get; set; }

    }
}