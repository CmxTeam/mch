﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class CustomsRow
    {
        public DateTime ? StatusTime { get; set; }
        public string HWB { get; set; }
        public String Status { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long Qty { get; set; }
        public string EntryType { get; set; }
        public string EntryNo { get; set; }
        public string Text { get; set; }


        public int? Total { get; set; }
    }
}