﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class NotocNoHazRow
    {
        public long  Id { get; set; }
        public string Destination { get; set; }
        public string Awb { get; set; }
        public string Description { get; set; }
        public int ? PackageCount { get; set; }
        public int Qty { get; set; }
        public string SupInfo { get; set; }
        public string Code { get; set; }
        public string LoadingPosition { get; set; }

        public long? AwbId { get; set; }

        public string ULDNumber { get; set; }

        public long? UldId { get; set; }

        public long NotocId { get; set; }

        public long? FlightId { get; set; }
    }
}