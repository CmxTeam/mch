﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class NotocDGRow
    {
        public long NotocId { get; set; }
        public string Destination { get; set; }
        public string Awb { get; set; }
        public string PSN { get; set; }
        public string UNClass { get; set; }
        public int ?  UNId { get; set; }
        public string SubRisk { get; set; }
        public int ?  NumberOfPackages { get; set; }
        public string NetQTIorTI { get; set; }
        public string RadioActiveCat { get; set; }
        public int Packages { get; set; }
        public string IMPCode { get; set; }
        public string CAO { get; set; }

        public string ULDNumber { get; set; }

        public string Compartment { get; set; }
        public string ERGCode { get; set; }

        public string PackingGroup { get; set; }

        public int? UNClassId { get; set; }

        public long? AwbId { get; set; }

        public int? CAOId { get; set; }

        public long ? FlightId { get; set; }

        public long? UldId { get; set; }

        public string UNNumber { get; set; }

        public long Id { get; set; }
    }
}