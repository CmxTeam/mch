﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskAssignment
    {
        public long? UserId { get; set; }

        public string Assigner { get; set; }
        public DateTime? AssignmentDate { get; set; }

    }
}