﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskRemark
    {

        public long TaskId { get; set; }
        public string Remark { get; set; }
    }
}