﻿// requires common-dialog.js CommonDialog module

$(function () {

    $('.cm-photo-enlargable').die().live('click', function () {
        var imageSrc = $(this).attr('src');
        if (imageSrc && imageSrc != '')
        {
            var imageViewerData = {
                imageId : 'cmImageViewerId'
            };

            var htmlContent = $("#cm-enlarge-photo").tmpl(imageViewerData);
            window.DIALOG_HANDLER.openPhotoZoomWindow(htmlContent, "Image Viewer");

            setTimeout(function () {
                var imageControl = $("#cmImageViewerId");
                if (imageControl.length > 0)
                {
                    $(imageControl).attr('src', imageSrc);
                }
            }, 500);
        }
    })


    $("div.pull-left").die().live('click', function () {
        $(window).resize();
    })
});