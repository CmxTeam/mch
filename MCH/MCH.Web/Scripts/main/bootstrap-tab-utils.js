﻿function TabHandler(baseUrl) {
    this.baseUrl = baseUrl;
}

TabHandler.prototype.selectTab = function (containerId, tabId) {

    var tabRoot = $("#" + containerId);

    $("#" + tabId).siblings().removeClass('active');
    $("#" + containerId + ">div.tab-content").children('div').removeClass('active');

    var targetTabHeader = tabRoot.find("#" + tabId)


    var targetTabContentId = targetTabHeader.children('a').first().attr('href');

    targetTabHeader.toggleClass('active', true);

    $(targetTabContentId).toggleClass('active', true);

    $(window).trigger("resize");
    $(window).trigger("cm-sub-resize");

}


TabHandler.prototype.addTab = function (containerId, templateData, url, data, busyIndicatorClass) {
    var self = this;
    var tabHeaderContainer = $("#" + containerId).children('div').eq(0).find('ul');
    var tabContantContainer = $("#" + containerId).children('div').eq(1);

    // check if tab is already loaded
    if ($(tabHeaderContainer).find("#" + templateData.headerId).length > 0) {
        self.selectTab(containerId, templateData.headerId);
    } else {

        var tabHeaderLoading = $("#tabsbar-header-loading").tmpl(templateData);

        var tabHeader = $("#tabsbar-header").tmpl(templateData);

        var tabContent = $("#tabsbar-content").tmpl(templateData);

        // append tab header
        tabHeaderContainer.append(tabHeaderLoading);

        //append tab content
        tabContantContainer.append(tabContent);

        $("." + busyIndicatorClass).show();

        $("#" + templateData.contentId).load(url, data, function () {
            
            self.selectTab(containerId, templateData.headerId);
            tabHeaderContainer.find("#" + templateData.headerId).html(tabHeader);

            $("#" + templateData.contentId).css("height", "100%");

            $(window).trigger('resize');
        });
    }
}






