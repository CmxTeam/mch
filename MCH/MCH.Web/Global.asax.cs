﻿using CMX.Shell.BLL.Abstraction;
using CMX.Shell.BLL.Factories;
using MCH.Web.ShellApi.Model;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace MCH.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        protected void Session_Start()
        {
            //bring the session back for 'remember me' functionality
            if (Request.IsAuthenticated)
            {
                FormsIdentity tmpIdentity = User.Identity as FormsIdentity;
                if (tmpIdentity != null)
                {
                    string[] tmpUserData = tmpIdentity.Ticket.UserData.Split('|');
                    SessionManager.UserInfo = new UserAuthModel();
                    if (tmpUserData.Length >= 1)
                    {
                        SessionManager.UserInfo.UserId = tmpUserData[0];
                    }
                    if (tmpUserData.Length >= 2)
                    {
                        SessionManager.UserInfo.SecurityToken = tmpUserData[1];
                    }
                    if (tmpUserData.Length >= 3)
                    {
                        SessionManager.UserInfo.ThemeName = tmpUserData[2];
                    }
                    if (tmpUserData.Length >= 4)
                    {
                        SessionManager.UserInfo.CompanyName = tmpUserData[3];
                    }
                    if (tmpUserData.Length >= 5)
                    {
                        SessionManager.UserInfo.EmailAddress = tmpUserData[4];
                    }
                    if (tmpUserData.Length >= 6)
                    {
                        SessionManager.UserInfo.UserAliasId = Int64.Parse(tmpUserData[5]);
                    }
                    if (tmpUserData.Length >= 7)
                    {
                        SessionManager.UserInfo.Firstname = tmpUserData[6];
                    }
                    if (tmpUserData.Length >= 8)
                    {
                        SessionManager.UserInfo.Lastname = tmpUserData[7];
                    }
                    if (tmpUserData.Length >= 9)
                    {
                        SessionManager.UserInfo.ClientTimeZoneOffset = (int)Int32.Parse(tmpUserData[8]);
                    }
                    if (tmpUserData.Length >= 10)
                    {
                        SessionManager.UserInfo.DefaultAccountId = (long)Int32.Parse(tmpUserData[9]);
                        SessionManager.AccountId = SessionManager.UserInfo.DefaultAccountId.Value;
                    }
                    if (tmpUserData.Length >= 11)
                    {
                        SessionManager.UserInfo.RoleNames = tmpUserData[10];
                    }
                    if (tmpUserData.Length >= 12)
                    {
                        SessionManager.UserInfo.UserDefaultMenu = tmpUserData[11];
                    }
                    if (tmpUserData.Length >= 13) 
                    {
                        SessionManager.UserInfo.UserDefaultStationId = !String.IsNullOrWhiteSpace(tmpUserData[12]) ? Convert.ToInt32(tmpUserData[12]) : new Nullable<int>();
                    }
                    if (tmpUserData.Length >= 14) 
                    {
                        SessionManager.UserInfo.UserLocalId = Int64.Parse(tmpUserData[13]);
                    }

                    using (IMenuManager menuManager = ShellManagerFactory.CreateMenuManager())
                    {
                        SessionManager.UserInfo.UserMenuItems = menuManager.GetMenusByRoleAccount(SessionManager.UserInfo.RoleNames, SessionManager.UserInfo.DefaultAccountId.Value);
                    }
                }
            }
        }
    }
}