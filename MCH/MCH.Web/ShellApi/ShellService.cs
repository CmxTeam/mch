﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMX.Shell.Model;
using CMX.Shell.BLL.Abstraction;
using CMX.Shell.BLL.Factories;
using MCH.Web.ShellApi.Model;

namespace MCH.Web.ShellApi
{
	public interface IShellService
	{
		IList<MenuItemModel> GetRoleMenus(string argUsername);
	}

	public class ShellService : IShellService
	{
		public IList<MenuItemModel> GetRoleMenus(string argRoleName)
		{
            using (IMenuManager menuManager = ShellManagerFactory.CreateMenuManager())
            {
                IList<MenuItemModel> tmpMenus = null;
                IList<MenuItem> tmpMenuItems = menuManager.GetApplicationMenu(argRoleName);
                if (tmpMenuItems != null && tmpMenuItems.Count > 0)
                {
                    tmpMenus = new List<MenuItemModel>();
                    foreach (var item in tmpMenuItems)
                    {
                        tmpMenus.Add(GetMenuItem(item));
                    }
                }

                return tmpMenus;
            }
		}

		#region Private Helper methods
		private MenuItemModel GetMenuItem(MenuItem argMenuItem)
		{
			MenuItemModel tmpMenuItem = null;
			if (argMenuItem != null)
			{
				tmpMenuItem = new MenuItemModel
				{
					Id = argMenuItem.Id,
					ImagePath = argMenuItem.MenuImagePath,
					IsGroup = argMenuItem.IsGroup,
					Name = argMenuItem.Label,
					TargetUrl = argMenuItem.Url
				};
				if (argMenuItem.ChildItems != null && argMenuItem.ChildItems.Count > 0)
				{
					tmpMenuItem.Children = new List<MenuItemModel>();
					foreach (var item in argMenuItem.ChildItems)
					{
						tmpMenuItem.Children.Add(GetMenuItem(item));
					}
				}
			}
			return tmpMenuItem;
		}
		#endregion
	}

}