﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.ShellApi
{
    public class ServiceFactory
    {
        public static IAuthenticationService CreateAuthenticationService() 
        {
            return new AuthenticationService();
        }

        public static IShellService CreateShellService() 
        {
            return new ShellService();
        }

    }
}