﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.ShellApi
{
    public enum UserValidaitonResult
    {
        UserIsValid,
        InvalidUserId,
        InvalidPassword,
        FirstTimeLogin,
        PasswordExpired,
        PasswordReset,
        UserIsLocked,
        UserIsInactive
    }
}