﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMX.Shell.Model;

namespace MCH.Web.ShellApi.Model
{
    public class UserAuthModel
    {
        private int timezone;
        private int clientTimeZone;

        public string UserId { get; set; }
        public long UserAliasId { get; set; }
        public string SecurityToken { get; set; }
        public string ThemeName { get; set; }
        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string TimeZone { get { return this.clientTimeZone.ToString(); } }
        public int UserTimeZone { get { return this.timezone; } set { this.timezone = value; } }
        public string ClientTimeZone { get { return this.clientTimeZone.ToString(); } }
        public int ClientTimeZoneOffset { get { return this.clientTimeZone; } set { this.clientTimeZone = value; } }
        public IList<MenuItem> UserMenuItems { get; set; }
        public string UserDefaultMenu { get; set; }
        public string RoleNames { get; set; }

        public UserAuthModel() { }
        public UserAuthModel(string argUserId, string argUserToken, string argThemeName)
        {
            this.UserId = argUserId;
            this.SecurityToken = argUserToken;
            this.ThemeName = argThemeName;
        }
        public UserAuthModel(long id, string argUserId, string argUserToken, string argThemeName)
        {
            this.UserId = argUserId;
            this.SecurityToken = argUserToken;
            this.ThemeName = argThemeName;
            this.UserAliasId = id;
        }     

        public long? DefaultAccountId { get; set; }

        public int? UserDefaultStationId { get; set; }

        public long UserLocalId { get; set; }
    }
}