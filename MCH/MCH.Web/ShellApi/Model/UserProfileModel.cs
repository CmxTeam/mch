﻿using CMX.Framework.Web.MVC.Attributes.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.ShellApi.Model
{
    public class UserProfileModel
    {
        [ReadOnly(true)]
        public string ProfilePhoto { get; set; }

        [ReadOnly(true)]
        public string UserId { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        [ReadOnly(true)]
        public string EmailAddress { get; set; }

        [EmailAddressValidation(ErrorMessage = "Invalid email address format.")]
        public string AlterEmailAddress { get; set; }

        [ReadOnly(true)]
        public string CompanyName { get; set; }

        public string JobTitle { get; set; }

        public string Department { get; set; }

        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [Required(ErrorMessage = "Address is required.")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Country is required.")]
        public string Country { get; set; }

        [PhoneValidator(ErrorMessage = "Invalid phone number format.")]
        public string WorkPhone { get; set; }

        [PhoneValidator(ErrorMessage = "Invalid phone number format.")]
        public string MobilePhone { get; set; }

        [PhoneValidator(ErrorMessage = "Invalid fax number format.")]
        public string Fax { get; set; }
    }
}