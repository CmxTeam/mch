﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.Web.ShellApi.Model
{
    public class MenuItemModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsGroup { get; set; }
        public string TargetUrl { get; set; }
        public string ImagePath { get; set; }
        public string Target
        {
            get
            {
                return this.IsGroup ? "#" : this.TargetUrl;
            }
        }
        public List<MenuItemModel> Children { get; set; }
    }
}
