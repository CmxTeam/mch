﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Text;
using System.Web.Mvc;
using CMX.Shell.Model;
using CMX.Shell.BLL.Factories;
using CMX.Shell.BLL.Abstraction;
using CMX.Shell.BLL.Model.Exceptions;
using MCH.Web.ShellApi.Model;
using CMX.Shell.BLL.Impl.DAL;
using MCH.BLL.Units;

namespace MCH.Web.ShellApi
{
    public interface IAuthenticationService
    {
        UserAuthModel SignIn(string userName, string argPassword, bool createPersistentCookie);
        UserAuthModel LogUserIn(string userName, string argPassword);
        void SignOut();
        UserValidaitonResult CheckUserValidity(string argUsername, string argPassword);
        MembershipUserModel Get(string userName);
        MembershipUserModel Get(Guid providerUserKey);
        bool ChangePassword(string argUsername, string argOdlPass, string argNewPass);
        ResetPasswordData ResetPassword(string argUsername, string argAnswer);
        bool ValidateUser(string argUsername, string argPassword);
        bool ChangePasswordQuestionAndAnswer(string argUsername, string argPassword, string argAnswer, string argQuestion);
        bool ChangeMobilePin(string argUsername, string argPassword, string argMobilePin);
        void UpdateUserProfile(UserProfileModel tmpModel);
        UserProfileModel GetUserProfile(string argUsername);
        void SetAuthTicket(string argUsername, bool isPersistant, UserAuthModel argUserInfo);

        IList<MenuItem> GetUserMenuItems(string argRoleNames, long argAccountId);
    }
    public class AuthenticationService : IAuthenticationService
    {
        #region Helper Methods
        private string SplitUserData(UserAuthModel argUserInfo)
        {
            string tmpUserData = String.Empty;
            if (argUserInfo != null)
            {
                StringBuilder tmpUserDataBuilder = new StringBuilder(argUserInfo.UserId);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.SecurityToken);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.ThemeName);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.CompanyName);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.EmailAddress);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.UserAliasId);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.Firstname);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.Lastname);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.ClientTimeZoneOffset);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.DefaultAccountId);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.RoleNames);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.UserDefaultMenu);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.UserDefaultStationId);
                tmpUserDataBuilder.Append('|');
                tmpUserDataBuilder.Append(argUserInfo.UserLocalId);
                tmpUserData = tmpUserDataBuilder.ToString();
            }
            return tmpUserData;
        }

        public void SetAuthTicket(string argUsername, bool isPersistant, UserAuthModel argUserInfo)
        {
            HttpCookie tmpCookie = FormsAuthentication.GetAuthCookie(argUsername, isPersistant);
            FormsAuthenticationTicket oldAuthTicket = FormsAuthentication.Decrypt(tmpCookie.Value);
            FormsAuthenticationTicket newAuthTicket = new FormsAuthenticationTicket
                (
                 oldAuthTicket.Version, oldAuthTicket.Name, oldAuthTicket.IssueDate,
                 oldAuthTicket.Expiration, isPersistant, this.SplitUserData(argUserInfo)
                 );
            string encryptedTicket = FormsAuthentication.Encrypt(newAuthTicket);
            tmpCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            if (isPersistant)
            {
                tmpCookie.Expires = oldAuthTicket.Expiration;
            }
            HttpContext.Current.Response.Cookies.Set(tmpCookie);
        }
        #endregion

        public UserAuthModel SignIn(string userName, string argPassword, bool createPersistentCookie)
        {
            UserAuthModel tmpUserInfo = null;
            if (String.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Value cannot be null or empty.", "userName");
            }
            tmpUserInfo = this.LogUserIn(userName, argPassword);
            //FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
            return tmpUserInfo;
        }

        public IList<MenuItem> GetUserMenuItems(string argRoleNames, long argAccountId)
        {
            using (IMenuManager menuManager = ShellManagerFactory.CreateMenuManager())
            {
                return menuManager.GetMenusByRoleAccount(argRoleNames, argAccountId);
            }
        }

        public UserAuthModel LogUserIn(string userName, string argPassword)
        {
            UserAuthModel tmpUserInfo = null;
            using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
            {
                UserCredentials tmpUserCredentials = userManager.AuthenticateUser(userName, argPassword);//SecureServiceFactory.Instance.CreateMembershipClient(null).Login(userName, argPassword);


                if (tmpUserCredentials != null)
                {
                    tmpUserInfo = new UserAuthModel(tmpUserCredentials.UserSessionInfo.UserId.ToString(), tmpUserCredentials.UserSessionInfo.SessionToken, tmpUserCredentials.ThemeName);
                    tmpUserInfo.EmailAddress = tmpUserCredentials.EmailAddress;
                    tmpUserInfo.CompanyName = tmpUserCredentials.CompanyName;
                    tmpUserInfo.UserAliasId = tmpUserCredentials.UserSessionInfo.UserIdAlias;
                    tmpUserInfo.RoleNames = tmpUserCredentials.RoleNames;
                    tmpUserInfo.DefaultAccountId = tmpUserCredentials.AccountId;
                    tmpUserInfo.UserDefaultMenu = tmpUserCredentials.DefaultMenuPath;

                    UserProfileModel tmpModel = GetUserProfile(userName);
                    if (tmpModel != null)
                    {
                        tmpUserInfo.Firstname = tmpModel.FirstName;
                        tmpUserInfo.Lastname = tmpModel.LastName;
                    }

                }

                using (CommonUnit unitOfWork = new CommonUnit())
                {
                    var tmpUserProfile = unitOfWork.UserProfileRepository.Get().SingleOrDefault(item => item.UserGuid == tmpUserCredentials.UserSessionInfo.UserId);
                    if (tmpUserProfile != null)
                    {
                        var tmpDefaultStation = tmpUserProfile.UserStations.Where(item => item.IsDefault);
                        if (tmpDefaultStation.Any()) 
                        {
                            tmpUserInfo.UserDefaultStationId = tmpDefaultStation.First().PortId;
                        }
                    }

                    tmpUserInfo.UserLocalId = tmpUserProfile.Id;
                }
            }


            return tmpUserInfo;
        }

        [Authorize]
        public void SignOut()
        {
            FormsAuthentication.SignOut();
            if (!String.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
            {
                //SecureServiceFactory.Instance.CreateMembershipClient(null).SignOut(HttpContext.Current.User.Identity.Name);
            }
        }

        public UserValidaitonResult CheckUserValidity(string argUsername, string argPassword)
        {
            UserValidaitonResult tmpValidationResult = UserValidaitonResult.UserIsValid;
            using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
            {
                CMXMembershipUser tmpUser = userManager.GetMembershipUser(argUsername, false);
                if (tmpUser == null)
                {
                    return UserValidaitonResult.InvalidUserId;
                }
                if (!tmpUser.IsApproved)
                {
                    return UserValidaitonResult.UserIsInactive;
                }
                if (tmpUser.IsLockedOut)
                {
                    return UserValidaitonResult.UserIsLocked;
                }
                if (!this.ValidateUser(argUsername, argPassword))
                {
                    return UserValidaitonResult.InvalidPassword;
                }
                else
                {
                    #region Check for first time login

                    DateTime tmpCreationDate = new DateTime(tmpUser.CreationDate.Year, tmpUser.CreationDate.Month, tmpUser.CreationDate.Day, tmpUser.CreationDate.Hour, tmpUser.CreationDate.Minute, 0);
                    DateTime tmpLastLoginDate = new DateTime(tmpUser.LastLoginDate.Year, tmpUser.LastLoginDate.Month, tmpUser.LastLoginDate.Day, tmpUser.LastLoginDate.Hour, tmpUser.LastLoginDate.Minute, 0);
                    if (tmpLastLoginDate == tmpCreationDate)
                    {
                        return UserValidaitonResult.FirstTimeLogin;
                    }
                    #endregion

                    #region Check for password expired
                    int tmpPassExpPeriod = Int32.Parse(ConfigurationManager.AppSettings[MCH.Web.Utilities.Constants.Config.PASSWORD_EXPIRATION_PERIOD]);
                    if ((DateTime.Now - tmpUser.LastPasswordChangedDate).Days >= tmpPassExpPeriod)
                    {
                        return UserValidaitonResult.PasswordExpired;
                    }
                    #endregion

                    #region Check for password has beed reseted
                    if (tmpUser.LastPasswordChangedDate > tmpUser.LastActivityDate)
                    {
                        return UserValidaitonResult.PasswordReset;
                    }

                    #endregion
                }
                return tmpValidationResult;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            try
            {
                using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
                {
                    return userManager.ValidateUser(userName, password);
                }
            }
            catch (Exception)
            {
                throw new ShellException("Internal Server Error");
            }
        }

        public void UpdateUserProfile(UserProfileModel tmpModel)
        {
            if (tmpModel == null)
            {
                throw new ArgumentNullException("User Profile is null or empty.", "UserProfile");
            }
            UserGenericProfile tmpProfile = ConvertToGenericProfile(tmpModel.UserId, tmpModel);
            using (IProfileManager profileManager = ShellManagerFactory.CreateProfileManager())
            {
                profileManager.UpdateUserProfile(tmpProfile);
            }
        }

        public UserProfileModel GetUserProfile(string argUsername)
        {
            UserProfileModel tmpModel = null;
            using (IProfileManager profileManager = ShellManagerFactory.CreateProfileManager())
            {
                UserGenericProfile tmpProfile = profileManager.GetUserProfile(argUsername);
                if (tmpProfile != null)
                {
                    tmpModel = ConvertFromGenericProfile(tmpProfile);
                }
                return tmpModel;
            }
        }

        private static UserGenericProfile ConvertToGenericProfile(string argUsername, UserProfileModel tmpModel)
        {
            UserGenericProfile tmpProfile = null;
            if (tmpModel != null)
            {
                tmpProfile = new UserGenericProfile() { Username = argUsername };
                List<ProfileFieldDTO> tmpProfileFields = new List<ProfileFieldDTO>();
                if (tmpModel.Address1 == null)
                {
                    tmpModel.Address1 = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "Address1", FieldType = tmpModel.Address1.GetType().FullName, FieldValue = tmpModel.Address1 });

                if (tmpModel.Address2 == null)
                {
                    tmpModel.Address2 = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "Address2", FieldType = tmpModel.Address2.GetType().FullName, FieldValue = tmpModel.Address2 });

                if (tmpModel.Address3 == null)
                {
                    tmpModel.Address3 = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "Address3", FieldType = tmpModel.Address3.GetType().FullName, FieldValue = tmpModel.Address3 });

                if (tmpModel.AlterEmailAddress == null)
                {
                    tmpModel.AlterEmailAddress = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "AlterEmailAddress", FieldType = tmpModel.AlterEmailAddress.GetType().FullName, FieldValue = tmpModel.AlterEmailAddress });

                if (tmpModel.City == null)
                {
                    tmpModel.City = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "City", FieldType = tmpModel.City.GetType().FullName, FieldValue = tmpModel.City });

                if (tmpModel.CompanyName == null)
                {
                    tmpModel.CompanyName = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "CompanyName", FieldType = tmpModel.CompanyName.GetType().FullName, FieldValue = tmpModel.CompanyName });

                if (tmpModel.Country == null)
                {
                    tmpModel.Country = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "Country", FieldType = tmpModel.Country.GetType().FullName, FieldValue = tmpModel.Country });

                if (tmpModel.Department == null)
                {
                    tmpModel.Department = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "Department", FieldType = tmpModel.Department.GetType().FullName, FieldValue = tmpModel.Department });

                if (tmpModel.EmailAddress == null)
                {
                    tmpModel.EmailAddress = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "EmailAddress", FieldType = tmpModel.EmailAddress.GetType().FullName, FieldValue = tmpModel.EmailAddress });

                if (tmpModel.Fax == null)
                {
                    tmpModel.Fax = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "Fax", FieldType = tmpModel.Fax.GetType().FullName, FieldValue = tmpModel.Fax });

                if (tmpModel.FirstName == null)
                {
                    tmpModel.FirstName = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "FirstName", FieldType = tmpModel.FirstName.GetType().FullName, FieldValue = tmpModel.FirstName });

                if (tmpModel.ProfilePhoto == null)
                {
                    tmpModel.ProfilePhoto = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "ProfilePhoto", FieldType = tmpModel.ProfilePhoto.GetType().FullName, FieldValue = tmpModel.ProfilePhoto });

                if (tmpModel.JobTitle == null)
                {
                    tmpModel.JobTitle = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "JobTitle", FieldType = tmpModel.JobTitle.GetType().FullName, FieldValue = tmpModel.JobTitle });

                if (tmpModel.LastName == null)
                {
                    tmpModel.LastName = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "LastName", FieldType = tmpModel.LastName.GetType().FullName, FieldValue = tmpModel.LastName });

                if (tmpModel.MobilePhone == null)
                {
                    tmpModel.MobilePhone = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "MobilePhone", FieldType = tmpModel.MobilePhone.GetType().FullName, FieldValue = tmpModel.MobilePhone });

                if (tmpModel.State == null)
                {
                    tmpModel.State = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "State", FieldType = tmpModel.State.GetType().FullName, FieldValue = tmpModel.State });

                if (tmpModel.WorkPhone == null)
                {
                    tmpModel.WorkPhone = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "WorkPhone", FieldType = tmpModel.WorkPhone.GetType().FullName, FieldValue = tmpModel.WorkPhone });

                if (tmpModel.ZipCode == null)
                {
                    tmpModel.ZipCode = String.Empty;
                }
                tmpProfileFields.Add(new ProfileFieldDTO() { FieldName = "ZipCode", FieldType = tmpModel.ZipCode.GetType().FullName, FieldValue = tmpModel.ZipCode });

                tmpProfile.ProfileFields = tmpProfileFields.ToArray<ProfileFieldDTO>();
            }
            return tmpProfile;
        }



        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
            {
                return userManager.ChangePassword(userName, oldPassword, newPassword);
            }
        }

        public ResetPasswordData ResetPassword(string argUsername, string argAnswer)
        {
            try
            {
                using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
                {
                    return userManager.ResetPassword(argUsername, argAnswer);
                }
            }
            catch (ShellException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new ShellException("Internal Server Error");
            }
        }

        public MembershipUserModel Get(string userName)
        {
            MembershipUserModel tmpModel = null;
            try
            {
                using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
                {
                    CMXMembershipUser tmpUser = userManager.GetMembershipUser(userName, false);
                    if (tmpUser != null)
                    {
                        tmpModel = new MembershipUserModel()
                        {
                            Comment = tmpUser.Comment,
                            CreationDate = tmpUser.CreationDate,
                            Email = tmpUser.Email,
                            IsApproved = tmpUser.IsApproved,
                            IsLockedOut = tmpUser.IsLockedOut,
                            IsOnline = tmpUser.IsOnline,
                            LastActivityDate = tmpUser.LastActivityDate,
                            LastLockoutDate = tmpUser.LastLockoutDate,
                            LastLoginDate = tmpUser.LastLoginDate,
                            LastPasswordChangedDate = tmpUser.LastLoginDate,
                            PasswordQuestion = tmpUser.PasswordQuestion,
                            UserName = tmpUser.UserName
                        };
                    }
                }
            }
            catch (NullReferenceException)
            {
                //TODO:Harut-> handle this
            }
            return tmpModel;
        }

        public MembershipUserModel Get(Guid providerUserKey)
        {
            throw new NotImplementedException();
        }

        public bool ChangePasswordQuestionAndAnswer(string argUsername, string argPassword, string argAnswer, string argQuestion)
        {
            try
            {
                using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
                {
                    return userManager.ChanagePasswordQuestionAnswer(argUsername, argPassword, argAnswer, argQuestion);
                }

            }
            catch (Exception)
            {
                throw new ShellException("Internal Server Error");
            }
        }

        public bool ChangeMobilePin(string argUsername, string argPassword, string argMobilePin)
        {
            try
            {
                using (IUserManager userManager = ShellManagerFactory.CreateUserManager())
                {
                    return userManager.ChangeMobilePin(argUsername, argPassword, argMobilePin);
                }
            }
            catch (Exception)
            {
                throw new ShellException("Internal Server Error");
            }
        }

        #region Helper Methods
        private static UserProfileModel ConvertFromGenericProfile(UserGenericProfile tmpProfile)
        {
            UserProfileModel tmpModel = new UserProfileModel();
            tmpModel.UserId = tmpProfile.Username;
            if (tmpProfile.ProfileFields != null && tmpProfile.ProfileFields.Any())
            {
                foreach (var item in tmpProfile.ProfileFields)
                {
                    switch (item.FieldName)
                    {
                        case "Address1":
                            {
                                tmpModel.Address1 = item.FieldValue;
                                break;
                            }
                        case "Address2":
                            {
                                tmpModel.Address2 = item.FieldValue;
                                break;
                            }
                        case "Address3":
                            {
                                tmpModel.Address3 = item.FieldValue;
                                break;
                            }
                        case "AlterEmailAddress":
                            {
                                tmpModel.AlterEmailAddress = item.FieldValue;
                                break;
                            }
                        case "City":
                            {
                                tmpModel.City = item.FieldValue;
                                break;
                            }
                        case "CompanyName":
                            {
                                tmpModel.CompanyName = item.FieldValue;
                                break;
                            }
                        case "Country":
                            {
                                tmpModel.Country = item.FieldValue;
                                break;
                            }
                        case "Department":
                            {
                                tmpModel.Department = item.FieldValue;
                                break;
                            }
                        case "EmailAddress":
                            {
                                tmpModel.EmailAddress = item.FieldValue;
                                break;
                            }
                        case "Fax":
                            {
                                tmpModel.Fax = item.FieldValue;
                                break;
                            }
                        case "FirstName":
                            {
                                tmpModel.FirstName = item.FieldValue;
                                break;
                            }
                        case "ProfilePhoto":
                            {
                                tmpModel.ProfilePhoto = item.FieldValue;
                                break;
                            }
                        case "JobTitle":
                            {
                                tmpModel.JobTitle = item.FieldValue;
                                break;
                            }
                        case "LastName":
                            {
                                tmpModel.LastName = item.FieldValue;
                                break;
                            }
                        case "MobilePhone":
                            {
                                tmpModel.MobilePhone = item.FieldValue;
                                break;
                            }
                        case "State":
                            {
                                tmpModel.State = item.FieldValue;
                                break;
                            }
                        case "WorkPhone":
                            {
                                tmpModel.WorkPhone = item.FieldValue;
                                break;
                            }
                        case "ZipCode":
                            {
                                tmpModel.ZipCode = item.FieldValue;
                                break;
                            }
                    }
                }
            }
            return tmpModel;
        }
        #endregion
    }
}