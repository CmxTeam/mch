﻿using MCH.BLL.DAL;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL
{
    public class UnitOfWork : IDisposable
    {
        protected readonly MCHDBEntities DataContext = new MCHDBEntities();

        protected long GetWarehouseId(string station)
        {
            return DataContext.Warehouses.First(w => w.Port.IATACode == station).Id;
        }

        public void Save()
        {
            DataContext.SaveChanges();
        }

        public MCHDBEntities Context 
        {
            get { return this.DataContext; }
        }

        #region IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    DataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        } 
        #endregion
    }
}
