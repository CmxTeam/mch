﻿using MCH.BLL.DAL;
using MCH.BLL.Model.DataContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace MCH.BLL.Units
{
    public class ScannerUnitOfWork:UnitOfWork
    {

        public UserSession LoginByPin(string station, string pin)
        {
            var membership = DataContext.aspnet_Membership.SingleOrDefault(m => m.MobilePIN == pin);
            if (membership == null) return null;
            return GetUserSession(membership);
        }

        public UserSession LoginByUserName(string station, string userName, string password, Platforms platformID)
        {
            var user = DataContext.aspnet_Users.SingleOrDefault(u => u.UserName == userName);
            if (user == null) return null;

            var hashedPassword = EncodePassword(password, user.aspnet_Membership.PasswordSalt);
            var membership = DataContext.aspnet_Membership.SingleOrDefault(m => m.Password == hashedPassword);
            if (membership == null) return null;

            return GetUserSession(membership);
        }

        public LoginTypes LoginPreference(string gatewayName)
        {
            var port = DataContext.Ports.SingleOrDefault(p => p.IATACode == gatewayName);
            if (port == null) return LoginTypes.Pin;

            return (LoginTypes)Enum.Parse(typeof(LoginTypes), port.Warehouses.First().LoginPreference);
        }

        public WarehouseLocationModel[] GetWarehouseLocations(string station, LocationType location)
        {
            var query = DataContext.WarehouseLocations.Where(wl => wl.Warehouse.Port.IATACode == station && wl.LocationTypeId != 7).Select(wl => new
            {
                LocationId = wl.Id,
                Location = wl.Location,
                LocationBarcode = wl.Barcode,
                LocationType = wl.WarehouseLocationType.LocationType
            }).ToList();

            return query.Select(i => new WarehouseLocationModel
            {
                LocationId = i.LocationId,
                Location = i.Location,
                LocationBarcode = i.LocationBarcode,
                LocationType = i.LocationType == null ? LocationType.NA : (LocationType)Enum.Parse(typeof(LocationType), i.LocationType)
            }).ToArray();
        }

        public MainMenuItem[] GetMainMenu(string guid, long userID, string gateway, string station, out int latestVersion)
        {
            latestVersion = 0;
            var tasksTree = DataContext.GetTasksTree(DateTime.UtcNow.Date, "NOT STARTED", userID, "mobile");
            return tasksTree.Select(t => new MainMenuItem
            {
                ActionID = t.ActionID.Value,
                Icon = t.Icon,
                Label = t.ActionName,
                TotalCount = t.TaskCount.Value.ToString(),
                Completed = t.CompletedTaskCount.Value.ToString(),
                NotAssigned = t.NotAssignedTaskCount.Value.ToString(),
                NotCompleted = t.NotCompletedTaskCount.Value.ToString(),
                TotalCountByUser = t.TaskCountByUser.Value.ToString(),
                MobileResource = t.MobileResource,
                ActionCategory = t.ActionCategory,
                NotCompletedTaskCountByUserID = t.NotCompletedTaskCountByUserID
            }).ToArray();
        }

        public long GetLocationIdByLocationBarcode(string station, string barcode)
        {
            var loc = DataContext.WarehouseLocations.SingleOrDefault(l => l.Barcode == barcode && l.Warehouse.Port.IATACode == station);
            return loc == null ? 0 : loc.Id;
        }

        public void AddTaskSnapshotReference(long taskId, string reference)
        {
            DataContext.AddTaskSnapshotReference(taskId, reference);
        }

        public bool ExecuteNonQuery(string station, string query)
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[station].ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                    finally
                    {
                        if (connection.State != System.Data.ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }

        public DataTable ExecuteQuery(string station, string query)
        {
            try
            {
                var datatable = new DataTable();
                datatable.TableName = "Data";
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[station].ConnectionString))
                {
                    using (var adapter = new SqlDataAdapter(query, connection))
                    {
                        adapter.Fill(datatable);
                    }
                }
                return datatable;
            }
            catch
            {
                return null;
            }
        }

        public AwbInfo GetAwbInfo(string station, string shipment)
        {
            shipment = shipment.Replace(" ", "").Replace("-", "").ToLower();

            var awb = DataContext.AWBs.Where(a => a.Carrier.Carrier3Code + a.AWBSerialNumber == shipment).ToList().LastOrDefault();
            if (awb == null)
                return null;
            var alerts = DataContext.GetAwbAlerts(station, awb.Id, false).ToList();
            return new AwbInfo
            {
                AwbId = awb.Id,
                SerialNumber = awb.AWBSerialNumber,
                Carrier = awb.Carrier.CarrierCode,
                Pieces = awb.TotalPieces.HasValue ? awb.TotalPieces.Value : 0,
                Origin = awb.Port.IATACode,
                Destination = awb.Port1.IATACode,
                HasAlarm = alerts.Any(),
                //Alert = alerts.Select(a => new Models.Alert 
                //{
                //    Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                //    Message = a.AlertMessage,
                //    SetBy = a.SetBy
                //}).ToArray()
            };
        }

        #region [ -- Private Methods -- ]

        private string EncodePassword(string pass, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] saltString = Convert.FromBase64String(salt);
            byte[] result;

            var hashAlgorithm = this.GetHashAlgorithm();

            if (hashAlgorithm is KeyedHashAlgorithm)
            {
                var keyedHashAlgorithm = (KeyedHashAlgorithm)hashAlgorithm;
                if (keyedHashAlgorithm.Key.Length == saltString.Length)
                    keyedHashAlgorithm.Key = saltString;
                else if (keyedHashAlgorithm.Key.Length < saltString.Length)
                {
                    byte[] keyArray = new byte[keyedHashAlgorithm.Key.Length];
                    Buffer.BlockCopy((Array)saltString, 0, (Array)keyArray, 0, keyArray.Length);
                    keyedHashAlgorithm.Key = keyArray;
                }
                else
                {
                    byte[] keyArray = new byte[keyedHashAlgorithm.Key.Length];
                    int dstOffset = 0;
                    while (dstOffset < keyArray.Length)
                    {
                        int count = Math.Min(saltString.Length, keyArray.Length - dstOffset);
                        Buffer.BlockCopy((Array)saltString, 0, (Array)keyArray, dstOffset, count);
                        dstOffset += count;
                    }
                    keyedHashAlgorithm.Key = keyArray;
                }
                result = keyedHashAlgorithm.ComputeHash(bytes);
            }
            else
            {
                byte[] buffer = new byte[saltString.Length + bytes.Length];
                Buffer.BlockCopy((Array)saltString, 0, (Array)buffer, 0, saltString.Length);
                Buffer.BlockCopy((Array)bytes, 0, (Array)buffer, saltString.Length, bytes.Length);
                result = hashAlgorithm.ComputeHash(buffer);
            }

            return Convert.ToBase64String(result);
        }

        private HashAlgorithm GetHashAlgorithm()
        {
            return HashAlgorithm.Create("SHA1");
        }

        private UserSession GetUserSession(aspnet_Membership membership)
        {
            var profile = membership.UserProfiles.Single();
            var warehouse = profile.UserWarehouses.FirstOrDefault();
            var warehouseName = warehouse == null ? string.Empty : warehouse.Warehouse.Port.IATACode;
            var timeout = warehouse == null ? null : warehouse.Warehouse.ScannerTimeOut;

            return new UserSession
            {
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                UserID = profile.Id,
                GroupID = profile.IsSupervisor.HasValue && profile.IsSupervisor == 1 ? UserTypes.Admin : UserTypes.User,
                GatewayName = warehouseName,
                UserName = membership.aspnet_Users.UserName,
                LocalTime = DateTime.Now,
                Pin = membership.MobilePIN,
                SessionGuid = Guid.NewGuid(),
                Timeout = timeout
            };
        }

        #endregion
    }
}
