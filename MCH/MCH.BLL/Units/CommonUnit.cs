﻿using MCH.BLL.DAL;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Units
{
    public class CommonUnit: UnitOfWork
    {
        private GenericRepository<Carrier> carrierRepository;
        private GenericRepository<Status> statusRepository;
        private GenericRepository<Port> portRepository;
        private GenericRepository<UserProfile> userProfileRepository;

        public GenericRepository<Carrier> CarrierRepostiory
        {
            get
            {
                if (this.carrierRepository == null)
                    this.carrierRepository = new GenericRepository<Carrier>(base.DataContext);

                return this.carrierRepository;
            }
        }

        public GenericRepository<Status> StatusRepository 
        {
            get
            {
                if (this.statusRepository == null) 
                {
                    this.statusRepository = new GenericRepository<Status>(base.DataContext);
                }
                return this.statusRepository;
            }
        }

        public GenericRepository<Port> PortRepository
        {
            get
            {
                if (this.portRepository == null)
                {
                    this.portRepository = new GenericRepository<Port>(base.DataContext);
                }
                return this.portRepository;
            }
        }

        public GenericRepository<UserProfile> UserProfileRepository
        {
            get
            {
                if (this.userProfileRepository == null)
                {
                    this.userProfileRepository = new GenericRepository<UserProfile>(base.DataContext);
                }
                return this.userProfileRepository;
            }
        }

    }
}
