﻿using MCH.BLL.DAL;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Units
{
    public class TasksUnitOfWork : UnitOfWork
    {
        private GenericRepository<TaskType> taskTypeRepository;
        private GenericRepository<Warehouse> warehouseRepository;
        private GenericRepository<Task> taskRepository;
        private GenericRepository<FlightManifest> flightRepository;


        public GenericRepository<TaskType> TaskTypeRepository
        {
            get
            {
                if (this.taskTypeRepository == null)
                    this.taskTypeRepository = new GenericRepository<TaskType>(base.DataContext);
                return taskTypeRepository;
            }
        }

        public GenericRepository<Warehouse> WarehouseRepository
        {
            get
            {
                if (this.warehouseRepository == null)
                    this.warehouseRepository = new GenericRepository<Warehouse>(base.DataContext);
                return warehouseRepository;
            }
        }

        public GenericRepository<Task> TaskRepository
        {
            get
            {
                if (this.taskRepository == null)
                    this.taskRepository = new GenericRepository<Task>(base.DataContext);
                return taskRepository;
            }
        }

        public GenericRepository<FlightManifest> FlightRepository
        {
            get
            {
                if (this.flightRepository == null)
                    this.flightRepository = new GenericRepository<FlightManifest>(base.DataContext);
                return flightRepository;
            }
        }

        public IEnumerable<GetCargoReceiverTaskList_Result> GetCargoReceiverTasksList(Nullable<short> pStartIndex, Nullable<byte> pPageSize, string pOrderByAndDir, Nullable<System.DateTime> pStart, Nullable<System.DateTime> pEnd, Nullable<long> pCarrierId, Nullable<long> pOriginId, Nullable<long> pDestinationId, Nullable<int> pTaskStatusId, Nullable<long> pUserId, Nullable<long> pWarehouseId, string pGeneralSearch, string pFlightNumber)
        {
            return base.DataContext.GetCargoReceiverTaskList(pStartIndex,
                pPageSize,
                pOrderByAndDir,
                pGeneralSearch,
                pStart,
                pEnd,
                pCarrierId,
                pOriginId,
                pDestinationId,
                pTaskStatusId,
                pUserId,
                pWarehouseId,
                pFlightNumber).AsEnumerable();
        }

    }
}
