﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class UserSession
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserTypes GroupID { get; set; }
        public string GatewayName { get; set; }
        public DateTime LocalTime { get; set; }
        public int? Timeout { get; set; }
        public string Pin { get; set; }
        public Guid SessionGuid { get; set; }
    }
}