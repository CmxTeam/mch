﻿namespace MCH.BLL.Model.DataContracts
{
    public class IdNamedEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}