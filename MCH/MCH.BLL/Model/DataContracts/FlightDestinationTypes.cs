﻿namespace MCH.BLL.Model.DataContracts
{
    public enum FlightDestinationTypes
    {
        All, 
        Attached, 
        NotAttached 
    }
}