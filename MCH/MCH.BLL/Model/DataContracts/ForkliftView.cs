﻿namespace MCH.BLL.Model.DataContracts
{
    public class ForkliftView
    {        
        public long AwbId { get; set; }
        public long UldId { get; set; }
        public string Uld { get; set; }       
        public string AWB { get; set; }
        public int TotalPieces { get; set; }
        public int ForkliftPieces { get; set; }
        public int ReceivedPieces { get; set; }
        public int AvailablePieces { get; set; }
        public string Locations { get; set; }
        public long DetailId { get; set; }
        public int Counter { get; set; }        
    }
}