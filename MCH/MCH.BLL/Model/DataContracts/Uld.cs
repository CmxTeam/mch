﻿namespace MCH.BLL.Model.DataContracts
{
    public class Uld : UldBase
    {
        public string UldPrefix { get; set; }
        public string ULD { get; set; }
        public string CarrierCode { get; set; }
        public int TotalUnitCount { get; set; }
        public double PercentRecovered { get; set; }
        public int RecoveredCount { get; set; }
        public bool IsBUP { get; set; }
        public string Location { get; set; }
        public RecoverStatuses Status { get; set; }
        public int TotalPieces { get; set; }
        public int ReceivedPieces { get; set; }
        public double PercentReceived { get; set; }
        public string[] References { get; set; }
        public long Flag { get; set; }
    }
}