﻿namespace MCH.BLL.Model.DataContracts
{
    public class ScannedShipmentInfo
    {
        public Uld Uld { get; set; }
        public ValidatedShipment Shipment { get; set; }
    }
}