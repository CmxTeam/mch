﻿namespace MCH.BLL.Model.DataContracts
{
    public class CargoLoaderFlightLegUld
    {
        public long UldId { get; set; }
        public string Location { get; set; }
        public string UldType { get; set; }
        public string UldSerialNo { get; set; }
        public int TotalAwbs { get; set; }
        public int TotalSTC { get; set; }
        public bool IsBup { get; set; }
        public decimal Weight { get; set; }
        public string WeightUOM { get; set; }
        public decimal TareWeight { get; set; }
        public string UldPrefix { get; set; }
    }
}