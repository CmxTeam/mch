﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class UldView : ForkliftView
    {
        public string[] References { get; set; }
        public long Flag { get; set; }        
    }
}