﻿namespace MCH.BLL.Model.DataContracts
{
    public enum RecoverStatuses
    {
        All,
        Pending,
        InProgress,
        Complete,
        NotCompleted,
        NA
    }
}