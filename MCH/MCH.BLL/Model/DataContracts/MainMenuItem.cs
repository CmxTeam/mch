﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class MainMenuItem
    {
        public string Label {get; set;}
        public int ActionID{get; set;}
        public string TotalCount{get; set;}
        public string Icon { get; set; }
        public string NotCompleted { get; set; }
        public string NotAssigned { get; set; }
        public string Completed { get; set; }
        public string TotalCountByUser { get; set; }
        public string MobileResource { get; set; }
        public string ActionCategory { get; set; }
        public int? NotCompletedTaskCountByUserID { get; set; }
    }
}