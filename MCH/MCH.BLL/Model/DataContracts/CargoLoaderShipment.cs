﻿namespace MCH.BLL.Model.DataContracts
{
    public class CargoLoaderShipment
    {
        public TaskStatuses Status { get; set; }
        public long ShipmentId { get; set; }
        public string ShimpmentNumber { get; set; }
        public string ShimpmentType { get; set; }
        public int TotalPcs { get; set; }
        public int TotalSTC { get; set; }
        public int Load { get; set; }        
        public int Loaded { get; set; }        
        public char LoadIndicator { get; set; }
        public string[] Locations { get; set; }
        public decimal Weight { get; set; }
        public string WeigthUOM { get; set; }
        public int Flag { get; set; }
        public int TotalAwbs { get; set; }
        public int Counter { get; set; }
        public int ForkliftPieces { get; set; }
    }
}