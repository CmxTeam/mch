﻿using System;
namespace MCH.BLL.Model.DataContracts
{
    public class AlertModel
    {
        public string Message { get; set; }
        public string SetBy { get; set; }
        public DateTime Date { get; set; }
    }
}