﻿namespace MCH.BLL.Model.DataContracts
{
    public class ValidatedShipment
    {
        public long DetailId { get; set; }
        public long AwbId { get; set; }
        public string Awb { get; set; }
        //public int TotalPieces { get; set; }
        //public int ReceivedPieces { get; set; }
        public int AvailablePieces { get; set; }
        //public int ForkliftPieces { get; set; }
        //public long DetailsId { get; set; }
        //public bool IsSplit { get; set; }
        public TransactionStatus TransactionStatus { get; set; }        
     }    
}