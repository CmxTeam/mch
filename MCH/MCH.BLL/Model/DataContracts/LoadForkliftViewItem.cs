﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class LoadForkliftViewItem
    {
        public long DetailsId { get; set; }
        public long AwbId { get; set; }
        public string AwbNumber { get; set; }
        public long UserId { get; set; }
        public long TaskId { get; set; }
        public int ForkliftPieces { get; set; }
        public int TTLForkliftPcs { get; set; }
        public int LoadPieces { get; set; }
        public int LoadedPcs { get; set; }
        public int AvailablePcs { get; set; }
        public int Counter { get; set; }
    }
}