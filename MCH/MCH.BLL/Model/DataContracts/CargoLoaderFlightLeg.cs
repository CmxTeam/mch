﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class CargoLoaderFlightLeg
    {
        public long LegId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Location { get; set; }
        public TaskStatuses Status { get; set; }
        public int TotalAwbs { get; set; }
        public int TotalSTC { get; set; }
        public int TotalPcs { get; set; }
        public float Progress { get; set; }
        public UldBase[] Ulds { get; set; }
        public int Flag { get; set; }
    }
}