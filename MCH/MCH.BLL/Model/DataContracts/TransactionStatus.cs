﻿namespace MCH.BLL.Model.DataContracts
{
    public class TransactionStatus
    {
        public bool Status { get; set; }
        public string Error { get; set; }
    }
}