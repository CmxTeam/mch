﻿namespace MCH.BLL.Model.DataContracts
{
    public class UldBase
    {
        public long UldId { get; set; }
        public string UldType { get; set; }
        public string UldSerialNo { get; set; }
    }
}