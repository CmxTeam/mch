﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class UldViewItem
    {
        public long DetailId { get; set; }
        public long AWBID { get; set; }
        public string AWBNumber { get; set; }
        public long LoadCount { get; set; }
    }
}