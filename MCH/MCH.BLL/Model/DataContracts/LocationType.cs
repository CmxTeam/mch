﻿namespace MCH.BLL.Model.DataContracts
{
    public enum LocationType
    {
        NA,
        Door,
        Area,
        Truck,
        Cage,
        Rack,
        Screening,
        Forklift,
        ULD
    }
}