﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class AwbInfo
    {
        public long AwbId { get; set; }
        public string SerialNumber { get; set; }
        public string Carrier { get; set; }
        public int Pieces { get; set; }
        public bool HasAlarm { get; set; }
        public AlertModel[] Alert { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }
}