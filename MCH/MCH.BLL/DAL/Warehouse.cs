//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Warehouse
    {
        public Warehouse()
        {
            this.Alerts = new HashSet<Alert>();
            this.Onhands = new HashSet<Onhand>();
            this.OnhandTasks = new HashSet<OnhandTask>();
            this.RangePoolItems = new HashSet<RangePoolItem>();
            this.RangePools = new HashSet<RangePool>();
            this.Tasks = new HashSet<Task>();
            this.UserWarehouses = new HashSet<UserWarehous>();
            this.WarehouseLocations = new HashSet<WarehouseLocation>();
            this.WarehouseZones = new HashSet<WarehouseZone>();
        }
    
        public int Id { get; set; }
        public string WarehouseName { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Supervisor { get; set; }
        public string SupervisorCode { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> PortId { get; set; }
        public string FirmCode { get; set; }
        public Nullable<int> ScannerTimeOut { get; set; }
        public string LoginPreference { get; set; }
    
        public virtual ICollection<Alert> Alerts { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<Onhand> Onhands { get; set; }
        public virtual ICollection<OnhandTask> OnhandTasks { get; set; }
        public virtual Port Port { get; set; }
        public virtual ICollection<RangePoolItem> RangePoolItems { get; set; }
        public virtual ICollection<RangePool> RangePools { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<UserWarehous> UserWarehouses { get; set; }
        public virtual ICollection<WarehouseLocation> WarehouseLocations { get; set; }
        public virtual ICollection<WarehouseZone> WarehouseZones { get; set; }
    }
}
