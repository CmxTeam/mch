﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace MCH.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            MembershipCreateStatus creationResult;
            //Membership.CreateUser("Oniel.Parker@cargomatrix.com", "RDiaz123#", "Oniel.Parker@cargomatrix.com", "Are you the admin ?", "YEP", true, out creationResult);
            var tmpMembershipUser = Membership.GetUser("oniel.parker@cargomatrix.com");

            Console.WriteLine("Changed : " + tmpMembershipUser.ChangePassword("OParker123#", "OParker123#"));

            //Membership.DeleteUser("Oniel.Parker@cargomatrix.com");


        }

        internal string EncodePassword(string pass, int passwordFormat, string salt)
        {
            if (passwordFormat == 0) // MembershipPasswordFormat.Clear
                return pass;

            byte[] bIn = Encoding.Unicode.GetBytes(pass);
            byte[] bSalt = Convert.FromBase64String(salt);
            byte[] bAll = new byte[bSalt.Length + bIn.Length];
            byte[] bRet = null;

            Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
            Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);

            HashAlgorithm s = HashAlgorithm.Create();
            bRet = s.ComputeHash(bAll);

            return Convert.ToBase64String(bRet);
        }
    }
}
