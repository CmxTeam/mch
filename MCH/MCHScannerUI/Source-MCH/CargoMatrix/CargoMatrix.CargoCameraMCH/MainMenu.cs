﻿using System;
using System.Windows.Forms;
using CargoMatrix.Communication;
using System.Data;
namespace CargoMatrix.CargoCamera
{

 
         
    public partial class MainMenu : SmoothListbox.SmoothListbox
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.labelEmpty.Visible = false;
            this.TitleText = "Camera";
            pictureBoxDown.Enabled = false;
            pictureBoxUp.Enabled = false;
            panelSoftkeys.Refresh();
            smoothListBoxMainList.RemoveAll();
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("Take a Picture", CargoMatrix.Resources.Skin.Digital_Camera, 1));
             
        }
        private void MainMenu_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            if (!CargoMatrix.Communication.Utilities.CameraPresent)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Camera not present.", "Cargo Camera", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new CameraScreen());


            Cursor.Current = Cursors.Default;
            smoothListBoxMainList.Reset();
        }

        private void MainMenu_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        private void MainMenu_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
                        if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                            CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                        
                        break;
                }
            }
        }

    }

}
