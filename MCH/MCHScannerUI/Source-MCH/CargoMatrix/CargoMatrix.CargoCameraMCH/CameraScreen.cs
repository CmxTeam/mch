﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Data;
using System.Collections;
using System.Threading;
using OpenNETCF.Drawing.Imaging;

namespace CargoMatrix.CargoCamera
{
    public partial class CameraScreen : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        Thread m_thread;
        public ImageReadNotifyHandler ImageReadNotifyEvent;
        private CMXCameraBase cmxCameraControl1;

        public CameraScreen()
        {
         
            

            InitializeComponent();

            this.TitleText = "Camera";

            this.LoadOptionsMenu += new EventHandler(CameraScreen_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(CameraScreen_MenuItemClicked);
            //this.BarcodeReadNotify += new BarcodeReadNotifyHandler(CameraScreen_BarcodeReadNotify);
            this.ImageReadNotifyEvent += new ImageReadNotifyHandler(ImageReadNotify);

            this.Visible = false;
            this.ResumeLayout(false);

            StartCamera();

        }

        void CameraScreen_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));;
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            this.SuspendLayout();

            //BarcodeEnabled = false;
            //BarcodeEnabled = true;

            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            this.ResumeLayout(false);
            this.Visible = true;
            Cursor.Current = Cursors.Default;

        }

        void CameraScreen_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        //LoadControl();
                        cameraTimer.Enabled = true;
                        break;
                }
        }

        //void CameraScreen_BarcodeReadNotify(string barcodeData)
        //{
        //    //CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
        //    Cursor.Current = Cursors.Default;
        //    BarcodeEnabled = true;
        //}

        //NEW

 


  

        private void MiniPhotoCaptureControl_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled)
            {
                if (cmxCameraControl1 != null)
                    return;
                this.cmxCameraControl1 = new CMXColorCameraControl(4);

                this.SuspendLayout();
                cmxCameraControl1.ImageReadNotify += ImageReadNotifyEvent;

                this.cmxCameraControl1.BackColor = System.Drawing.Color.White;
                this.cmxCameraControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.cmxCameraControl1.Name = "cmxCameraControl1";
                this.cmxCameraControl1.Bounds = panel2.Bounds;
     

                this.cmxCameraControl1.TabIndex = 9;
                
                this.Controls.Add(this.cmxCameraControl1);
              
                cmxCameraControl1.BringToFront();
                this.ResumeLayout();
            }
            else
            {
                if (m_thread != null)
                {
                    m_thread.Abort();
                    m_thread = null;

                }

                if (cmxCameraControl1 != null)
                {

                    cmxCameraControl1.Enabled = false;
                    cmxCameraControl1.ImageReadNotify -= ImageReadNotifyEvent;
                    if (Controls.Contains(cmxCameraControl1))
                        Controls.Remove(cmxCameraControl1);

                    cmxCameraControl1.Dispose();
                    cmxCameraControl1 = null;

                }

            }
        }

        private void ImageReadNotify(System.IO.MemoryStream stream)
        {
            try
            {
                if (stream == null)
                {
 

                }
                else
                {
                   

                    IBitmapImage imageBitmap = CreateThumbnail(stream, new Size(64, 48));
                    Image myimage = ImageUtils.IBitmapImageToBitmap(imageBitmap);

 
                    cameraTimer.Enabled = true;

                    byte[] array = stream.ToArray();
                  
                }
            }
            catch (System.OutOfMemoryException e)
            {

                MessageBox.Show(e.Message);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            Cursor.Current = Cursors.Default;
        }


        private IBitmapImage CreateThumbnail(System.IO.Stream stream, Size size)
        {
            IBitmapImage imageBitmap;
            ImageInfo ii;
            IImage image;

            ImagingFactory factory = new ImagingFactoryClass();
            factory.CreateImageFromStream(new StreamOnFile(stream), out image);
            image.GetImageInfo(out ii);
            factory.CreateBitmapFromImage(image, (uint)size.Width, (uint)size.Height,
          ii.PixelFormat, InterpolationHint.InterpolationHintDefault, out imageBitmap);
            return imageBitmap;
        }

        public void StartCamera()
        {
            UI.BarcodeReader.TermReader();
            this.Enabled = true;
            if (cmxCameraControl1 != null)
            {
                cmxCameraControl1.StartAcquisition();
            }
        }

        public void StopCamera()
        {
            if (cmxCameraControl1 != null)
            {
                cmxCameraControl1.Enabled = false;
                cmxCameraControl1.StopAcquisition();
            }
        }

        private void cameraTimer_Tick(object sender, EventArgs e)
        {
            cameraTimer.Enabled = false;
            StartCamera();
        }

    }

}
