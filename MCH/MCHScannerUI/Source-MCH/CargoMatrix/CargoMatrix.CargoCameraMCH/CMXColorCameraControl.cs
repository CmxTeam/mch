﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Drawing.Imaging;

namespace CargoMatrix.CargoCamera
{
    //public delegate void ImageReadNotifyHandler(Image image);
    //public delegate void extern ImageReadNotifyHandler(System.IO.MemoryStream stream);
    public partial class CMXColorCameraControl : CMXCameraBase
    {
        public override event ImageReadNotifyHandler ImageReadNotify;
        FullscreenForm fullscreenDialog;
        
        //private Symbol.Imaging.Imager imager;
        private System.IO.MemoryStream imageStream;
        private Symbol.ResourceCoordination.Trigger trigger;
        Symbol.ResourceCoordination.Trigger.TriggerEventHandler triggerHandler;
        private bool imageView;
        private bool activated;
        ArrayList triggerList;
        bool isFullScreen = false;
        //private Image m_image = null;

        public CMXColorCameraControl()
        {
            InitializeComponent();
            fullscreenDialog = new FullscreenForm();

            triggerList = new ArrayList();
            foreach (Symbol.ResourceCoordination.TriggerDevice device
                             in Symbol.ResourceCoordination.TriggerDevice.AvailableTriggers)
            {
                try
                {
                    trigger = new Symbol.ResourceCoordination.Trigger(device);
                    triggerList.Add(trigger);
                    triggerHandler = new Symbol.ResourceCoordination.Trigger.TriggerEventHandler(trigger_Stage2Notify);
                    trigger.Stage2Notify += triggerHandler;//triggerHandlerList[triggerHandlerList.Count-1];
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to create Trigger: " + ex.Message, "Error");
                    return;
                }
            }

            activated = true;
        
        }
        public CMXColorCameraControl(int resolution)
        {
            InitializeComponent(resolution);
            fullscreenDialog = new FullscreenForm();

            triggerList = new ArrayList();
            foreach (Symbol.ResourceCoordination.TriggerDevice device
                             in Symbol.ResourceCoordination.TriggerDevice.AvailableTriggers)
            {
                try
                {
                    trigger = new Symbol.ResourceCoordination.Trigger(device);
                    triggerList.Add(trigger);
                    triggerHandler = new Symbol.ResourceCoordination.Trigger.TriggerEventHandler(trigger_Stage2Notify);
                    trigger.Stage2Notify += triggerHandler;//triggerHandlerList[triggerHandlerList.Count-1];
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to create Trigger: " + ex.Message, "Error");
                    return;
                }
            }

            activated = true;

        }
        private void InitializeComponent(int resolution)
        {
            this.colorCamera1 = new global::CMXCameraControl.ColorCamera(resolution);
            this.cameraPictureBox = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // colorCamera1
            // 
            this.colorCamera1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorCamera1.Location = new System.Drawing.Point(0, 0);
            this.colorCamera1.Name = "colorCamera1";
            this.colorCamera1.Size = new System.Drawing.Size(240, 180);
            this.colorCamera1.TabIndex = 0;
            // 
            // cameraPictureBox
            // 
            this.cameraPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraPictureBox.Location = new System.Drawing.Point(0, 0);
            this.cameraPictureBox.Name = "cameraPictureBox";
            this.cameraPictureBox.Size = new System.Drawing.Size(240, 180);
            this.cameraPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // CMXColorCameraControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.colorCamera1);
            this.Controls.Add(this.cameraPictureBox);
            this.Name = "CMXColorCameraControl";
            this.Size = new System.Drawing.Size(240, 180);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CMXCameraControl_KeyDown);
            this.ResumeLayout(false);

        }
        protected override void OnEnabledChanged(EventArgs e)
        {
            if (Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                //try
                //{
                //    Symbol.Imaging.Device[] device = Symbol.Imaging.Device.AvailableDevices;
                //    imager = new Symbol.Imaging.Imager(Symbol.Imaging.Device.AvailableDevices[0]);
                //    Symbol.Imaging.Capabilities cap = new Symbol.Imaging.Capabilities(imager);

                //}
                //catch (Exception ex)
                //{
                //    //CargoMatrix.UI.CMXMessageBox.Show("Failed to create Imager: " + ex.Message, "Error!" + " (30011)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                //    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30011);
                //    return;
                //}

                //imager.ImageFormat.FileFormat = Symbol.Imaging.FileFormats.JPEG;
                if (triggerList == null)
                {
                    triggerList = new ArrayList();
                    foreach (Symbol.ResourceCoordination.TriggerDevice device
                                     in Symbol.ResourceCoordination.TriggerDevice.AvailableTriggers)
                    {
                        try
                        {
                            trigger = new Symbol.ResourceCoordination.Trigger(device);
                            triggerList.Add(trigger);
                            triggerHandler = new Symbol.ResourceCoordination.Trigger.TriggerEventHandler(trigger_Stage2Notify);
                            trigger.Stage2Notify += triggerHandler;//triggerHandlerList[triggerHandlerList.Count-1];
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Failed to create Trigger: " + ex.Message, "Error");
                            return;
                        }
                    }
                }

                // Trigger only works then ImagerForm is activated.
                activated = true;
                //StopAcquisition();
                //StartAcquisition();
                //imageView = true;
                //imager.ImageFormat.JPEGQuality = 50;
                Cursor.Current = Cursors.Default;

            }
            else
            {
                //StopAcquisition();
                ///////////////////
                if (triggerList != null)
                {
                    foreach (Symbol.ResourceCoordination.Trigger trigger
                                     in triggerList)
                    {
                        if (trigger != null)
                            trigger.Dispose();
                    }
                    triggerList.Clear();
                    triggerList = null;
                }
                //if (colorCamera1 != null)
                //{
                //    colorCamera1.Dispose();
                //    colorCamera1 = null;
                //}




            }
            base.OnEnabledChanged(e);
        }
        private void trigger_Stage2Notify(object sender, Symbol.ResourceCoordination.TriggerEventArgs e)
        {
            if (isFullScreen)
            {
                fullscreenDialog.Close();
                isFullScreen = false;
                return;
            }

            if (activated && e.NewState == Symbol.ResourceCoordination.TriggerState.STAGE2)
            {
                if (!imageView)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    GetImage();
                    
                }
                else
                    StartAcquisition();
            }
            //if (activated && e.NewState == Symbol.ResourceCoordination.TriggerState.RELEASED)
            //{
            //    StartAcquisition();
            //}
        }
        //int indexer = 0;
        private void GetImage()
        {
            
            if (ImageReadNotify != null)
            {

                colorCamera1.TakePicture();
                imageStream = colorCamera1.GetImageMemoryStream();
                
                ImageReadNotify(imageStream);
            }
            imageView = true;
            
            
        }
        public override void StopAcquisition()
        {
            if (this.IsDisposed == false)
            {
                if (InvokeRequired)
                    this.Invoke(new Action<object>(DisableCamera));
                else
                    DisableCamera(null);
            }
            //if(colorCamera1 != null)
            //    colorCamera1.CloseCamera();
            //if (imager != null)
            //{
            //    if (imager.Viewfinder.IsStarted)
            //        imager.Viewfinder.Stop();
            //    imager.StopAcquisition();
            //    imageView = true;
            //}
        
        }
        private void DisableCamera(object o)
        {
            colorCamera1.Visible = false;
            imageView = true;

        }
        public override void StartAcquisition()
        {
            colorCamera1.Visible = true;
            imageView = false;
            if (ImageReadNotify != null)
                ImageReadNotify(null);
            //try
            //{
            //    imager.StartAcquisition();
            //}
            //catch (Exception ex)
            //{
            //    CargoMatrix.UI.CMXMessageBox.Show("Failed to start Imager: " + ex.Message, "Error!" + " (30012)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //    //this.Close();
            //    return;
            //}
            ////if (this.menuItemViewfinder.Checked)
            //imager.Viewfinder.Start(this.cameraPictureBox);
            //if(ImageReadNotify != null)
            //    ImageReadNotify(null);
            
        }

        public bool IsFullScreen
        {
            get
            {
                return isFullScreen;
            }
        }

        public bool IsImageView
        {
            get
            {
                return imageView;
            }
        }

        public override Image image
        {
            get
            {
                return cameraPictureBox.Image;
            }
            set
            {
                try
                {
                    //GC.Collect();
                    if (value == null)
                    {
                        if (cameraPictureBox.Image != null)
                        {
                            cameraPictureBox.Image.Dispose();
                            cameraPictureBox.Image = null;
                        }
                    }

                    else
                    {
                        cameraPictureBox.Image = new Bitmap(value);

                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                
            }
        }
        
        public override void FullScreen(bool fullscreen)
        {
            if (!imageView)
                return;
            isFullScreen = true;
            try
            {
               fullscreenDialog.Image = cameraPictureBox.Image;
               fullscreenDialog.ShowDialog();
               
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            isFullScreen = false;
                
        }
        
        private void CMXCameraControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (isFullScreen)
            {
                FullScreen(false);
                return;
            }
        }

        public int CameraResolution
        {
            set 
            {
                colorCamera1.Resolution = value;

            }
        }


        
    }
}

