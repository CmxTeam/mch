﻿namespace CargoMatrix.CargoCamera
{
    partial class CameraScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();

            this.panelHeader2.SuspendLayout();
  
            this.panel2.BackColor = System.Drawing.Color.Black;
            
            this.panel2.Location = new System.Drawing.Point(0, 17);
            this.panel2.Name = "panel2";
            int h = 231;
            this.panel2.Size = new System.Drawing.Size(240, h);
            
 
            this.cameraTimer = new System.Windows.Forms.Timer();
            this.cameraTimer.Interval = 1000;
            this.cameraTimer.Tick += new System.EventHandler(this.cameraTimer_Tick);


            this.Controls.Add(this.panel2);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
   
            panelHeader2.Height = 292;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.EnabledChanged += new System.EventHandler(this.MiniPhotoCaptureControl_EnabledChanged);
            this.panelHeader2.ResumeLayout(false);

        }




        public System.Windows.Forms.Timer cameraTimer;
        private System.Windows.Forms.Panel panel2;

        #endregion
    }
}
