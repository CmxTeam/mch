﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.GridListBox
{
    public partial class SummaryRow : UserControl
    {
        //int m_mode;
        int m_totalPieces = 0;
        public SummaryRow()
        {
            InitializeComponent();
            //m_mode = mode;
            
            //progressBar21.Text = "In Progress";
        }
        public CargoMatrix.Communication.WSPieceScan.Modes Mode
        {
            set
            {
                switch (value)
                {
                    case CargoMatrix.Communication.WSPieceScan.Modes.Count:
                        labelMode.Text = GridListBoxResources.Text_CountMode;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.Modes.Piece:
                        labelMode.Text = GridListBoxResources.Text_PieceMode;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.Modes.Slac:
                        labelMode.Text = GridListBoxResources.Text_SlacMode;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.Modes.NA: 
                        labelMode.Text = GridListBoxResources.Text_NAMode;
                        break;
                }
            
            }
        }
        public int TotalCount
        {
            get;set;
            //{
            //    return progressBar.Maximum;
            //    //return Convert.ToInt32(label1.Text);              
            //}
            //set
            //{
            //    //label1.Text = Convert.ToString(value);
            //    progressBar.Maximum = value;
            //}
        }
        public int Progress
        {
            set
            {
                m_totalPieces = value;
                //if (progressBar.Maximum >= value)
                {
                    //label1.Text = Convert.ToString(value);
                    if (value > 0)
                    {
                        int temp = (value * 100) / TotalCount;
                        progressBar.Value = temp;
                        progressBar.Minimum = 0;

                    }
                    else
                    {
                        progressBar.Minimum = -1;
                        progressBar.Value = 0;
                        //label1.Text = "0";
                    }
                }
            }
            get
            {
                return m_totalPieces;// progressBar.Value;
            }
        }
    }
}
