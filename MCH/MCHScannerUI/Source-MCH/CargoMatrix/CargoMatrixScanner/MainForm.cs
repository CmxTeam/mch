﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CargoMatrixScanner
{
    public partial class MainForm : SmoothForm
    {
        LoginMainScreen loginMainScreen;// = new LoginMainScreen();
        CargoMatrixScanner.MainMenu mainMenu;// = new MainMenu();

        public MainForm()
            : base(false)
        {
            //MainMenuShorcutEnabled = true;
            BackButtonHoldEnabled = true;
            SlidingEffectEnabled = false;
            MainForm_InitFormNotify();
            pictureBoxMessages.Image = CargoMatrix.Resources.Skin.message;
            this.pictureBoxMessages.BringToFront();
            //InitFormNotify += new InitForm(MainForm_InitFormNotify);
            //SplashScreenLogo = CargoMatrixScannerResource.cargo_matrix_logo;

        }

        public override void DoLogin()
        {
            //hasFooter = false;

            if (mainMenu != null)
                mainMenu.Dispose();

            mainMenu = new MainMenu();

            mainMenu.Location = new Point(Left, HeaderHeight);
            mainMenu.Size = new Size(Width, Height - HeaderHeight - FooterHeight);
            //mainMenu.User = user;
            SetDefaultForm(mainMenu);
            DisplayForm(mainMenu);

            base.DoLogin();
            //CargoMatrix.Messageing.MessageManager.Instance.Start();
        }

        public override void DoLogout()
        {
            //CargoMatrix.Messageing.MessageManager.Instance.Stop();
            if (loginMainScreen == null)
                loginMainScreen = new LoginMainScreen();

            CargoMatrix.Communication.GuidManager.ResetGuid();
            SetDefaultForm(loginMainScreen);
            DisplayDefaultForm();
            m_bLoggedIn = false;
            //hasFooter = true;


        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (loadSplash)
                base.OnPaintBackground(e);
        }

        private void MainForm_InitFormNotify()
        {
            InitializeComponent();

            loginMainScreen = new LoginMainScreen();
            SetDefaultForm(loginMainScreen);
            DisplayForm2(loginMainScreen);
            //this.EndSplashScreen();
        }
 

        public override bool NewMessage
        {
            set
            {
                pictureBoxMessages.Visible = value;
            }
        }
    }
}