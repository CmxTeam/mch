﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using System.Collections.Generic;
using System.Text;
namespace CargoMatrix.CargoLoader
{
    public partial class FlightLegUldFinalCargoItem : CustomListItems.ExpandableRenderListitem<CargoLoaderFlightLegUld>, ISmartListItem
    {

        public event EventHandler ButtonLocationClick;
        public event EventHandler ButtonWeightClick;

        public FlightLegUldFinalCargoItem(CargoLoaderFlightLegUld uld)
            : base(uld)
        {
            InitializeComponent();

          
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonLocation_Click(object sender, System.EventArgs e)
        {
            if (ButtonLocationClick != null)
            {
                ButtonLocationClick(this, EventArgs.Empty);
            }
        }


        void buttonWeight_Click(object sender, System.EventArgs e)
        {
            if (ButtonWeightClick != null)
            {
                ButtonWeightClick(this, EventArgs.Empty);
            }
        }

        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonLocation = new CargoMatrix.UI.CMXPictureButton();
            this.buttonLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonLocation.Location = new System.Drawing.Point(202, 4);
            this.buttonLocation.Name = "buttonLocation";
            this.buttonLocation.Image = Resources.Skin.button_map;
            this.buttonLocation.PressedImage = Resources.Skin.button_map_over;
            this.buttonLocation.Size = new System.Drawing.Size(32, 32);
            this.buttonLocation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonLocation.TransparentColor = System.Drawing.Color.White;
            this.buttonLocation.Click += new System.EventHandler(buttonLocation_Click);


            this.buttonWeigth = new CargoMatrix.UI.CMXPictureButton();
            this.buttonWeigth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonWeigth.Location = new System.Drawing.Point(168, 4);
            this.buttonWeigth.Name = "buttonWeigth";
            this.buttonWeigth.Image = Resources.Skin.button_scale;
            this.buttonWeigth.PressedImage = Resources.Skin.button_scale_over;
            this.buttonWeigth.Size = new System.Drawing.Size(32, 32);
            this.buttonWeigth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonWeigth.TransparentColor = System.Drawing.Color.White;
            this.buttonWeigth.Click += new System.EventHandler(buttonWeight_Click);


            this.Controls.Add(this.buttonLocation);
            this.Controls.Add(this.buttonWeigth);

            this.buttonLocation.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.buttonWeigth.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));

            this.ResumeLayout(false);
        }
 
        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("{0}", ItemData.UldType);
                    if (ItemData.UldType.ToUpper() == "LOOSE")
                    {
                        line1 = string.Format("{0}", ItemData.UldType);
                    }
                    else
                    {
                        if (ItemData.UldSerialNo == string.Empty)
                        {
                            line1 = string.Format("{0}", ItemData.UldType);
                        }
                        else
                        {
                            if (ItemData.UldType == null || ItemData.UldType == string.Empty)
                            {
                                line1 = string.Format("{0}{1}", ItemData.UldPrefix, ItemData.UldSerialNo);
                            }
                            else
                            {
                                line1 = string.Format("{0}-{1}{2}", ItemData.UldType, ItemData.UldPrefix, ItemData.UldSerialNo);
                            }
                        }
                    }


                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 1 * vScale, 120 * hScale, 14 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line2 = string.Format("Awbs: {0} STC:{1}", ItemData.TotalAwbs, ItemData.TotalSTC);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 13 * vScale, 120 * hScale, 13 * vScale));
                }



                if(ItemData.UldType.ToUpper()=="LOOSE")
                {
                    this.buttonLocation.Enabled = false;
                    this.buttonLocation.Image = CargoMatrix.Resources.Skin.button_map_dis;
                }
                else
                {
                    this.buttonLocation.Enabled = true;
                    this.buttonLocation.Image = CargoMatrix.Resources.Skin.button_map;
                }

                if (ItemData.Weight > 0)
                {
                    this.buttonWeigth.Image = CargoMatrix.Resources.Skin.button_scale_check;
                }
                else
                {
                    this.buttonWeigth.Image = CargoMatrix.Resources.Skin.button_scale;
                }

                //Image img =null;
                //if (ItemData.IsBup)
                //{
                //    img = CargoMatrix.Resources.Skin.button_uldlocked;
                //}
                //else
                //{
                //    if (ItemData.UldType.ToUpper() == "LOOSE")
                //    {
                //        img = CargoMatrix.Resources.Skin.countMode;
                //    }
                //    else
                //    {
                //        img = CargoMatrix.Resources.Skin.button_uld;
                //    }
                //}



                Image img = null;

                    if (ItemData.UldType.ToUpper() == "LOOSE")
                    {
                        img = CargoMatrix.Resources.Skin.countMode;
                    }
                    else
                    {
                        if (ItemData.IsBup)
                        {
                            img = CargoMatrix.Resources.Skin.button_uldlocked;
                        }
                        else
                        {
                            img = CargoMatrix.Resources.Skin.button_uld;
                        }

                        using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                        {
                            string line3 = string.Format("Loc: {0}", ItemData.Location);
                            gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 25 * vScale, 120 * hScale, 13 * vScale));
                        }

                    }
             





                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(32 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = string.Empty;
            content = ItemData.UldType + ItemData.UldSerialNo;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
