﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using System.Collections.Generic;
using System.Text;
namespace CargoMatrix.CargoLoader
{
    public partial class FlightLegUldCargoItem : CustomListItems.ExpandableRenderListitem<CargoLoaderFlightLegUld>, ISmartListItem
    {

        public event EventHandler ButtonDeleteClick;
        public event EventHandler ButtonEditClick;

        public FlightLegUldCargoItem(CargoLoaderFlightLegUld uld)
            : base(uld)
        {
            InitializeComponent();

          
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonDelete_Click(object sender, System.EventArgs e)
        {
            if (ButtonDeleteClick != null)
            {
                ButtonDeleteClick(this, EventArgs.Empty);
            }
        }


        void buttonEdit_Click(object sender, System.EventArgs e)
        {
            if (ButtonEditClick != null)
            {
                ButtonEditClick(this, EventArgs.Empty);
            }
        }

        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonDelete = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDelete.Location = new System.Drawing.Point(202, 4);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Image = Resources.Skin.cc_trash;
            this.buttonDelete.PressedImage = Resources.Skin.cc_trash_over;
            this.buttonDelete.Size = new System.Drawing.Size(32, 32);
            this.buttonDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelete.TransparentColor = System.Drawing.Color.White;
            this.buttonDelete.Click += new System.EventHandler(buttonDelete_Click);


            this.buttonEdit = new CargoMatrix.UI.CMXPictureButton();
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEdit.Location = new System.Drawing.Point(168, 4);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Image = Resources.Skin.edit_btn;
            this.buttonEdit.PressedImage = Resources.Skin.edit_btn_over;
            this.buttonEdit.Size = new System.Drawing.Size(32, 32);
            this.buttonEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonEdit.TransparentColor = System.Drawing.Color.White;
            this.buttonEdit.Click += new System.EventHandler(buttonEdit_Click);


            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonEdit);

            this.buttonDelete.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.buttonEdit.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));

            this.ResumeLayout(false);
        }
 
        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("{0}", ItemData.UldType);
                    if (ItemData.UldType.ToUpper() == "LOOSE")
                    {
                        line1 = string.Format("{0}", ItemData.UldType);
                    }
                    else
                    {
                        if (ItemData.UldSerialNo == string.Empty)
                        {
                            line1 = string.Format("{0}", ItemData.UldType);
                        }
                        else
                        {
                            if (ItemData.UldType == null || ItemData.UldType == string.Empty)
                            {
                                line1 = string.Format("{0}{1}",  ItemData.UldPrefix, ItemData.UldSerialNo);
                            }
                            else
                            {
                                line1 = string.Format("{0}-{1}{2}", ItemData.UldType, ItemData.UldPrefix, ItemData.UldSerialNo);
                            }
                           
                        }
                    }


                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 1 * vScale, 120 * hScale, 14 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line2 = string.Format("Awbs: {0} STC:{1}", ItemData.TotalAwbs, ItemData.TotalSTC);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 13 * vScale, 120 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line3 = string.Format("Loc: {0}", ItemData.Location);
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 25 * vScale, 120 * hScale, 13 * vScale));
                }

                //this.buttonDelete.Image = CargoMatrix.Resources.Skin.cc_trash;
                //this.buttonDelete.PressedImage = CargoMatrix.Resources.Skin.cc_trash;


                Image img =null;
                if (ItemData.IsBup)
                {
                    this.buttonDelete.Visible = false; 
                    this.buttonEdit.Visible = false;
                    img = CargoMatrix.Resources.Skin.button_uldlocked;
                }
                else
                {
                    this.buttonDelete.Visible = true; 
                    if (ItemData.UldType.ToUpper() == "LOOSE")
                    {
                        this.buttonEdit.Visible = false;
                        img = CargoMatrix.Resources.Skin.countMode;
                    }
                    else
                    {
                        this.buttonEdit.Visible = true;
                        img = CargoMatrix.Resources.Skin.button_uld;
                    }
                }


                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(32 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = string.Empty;
            content = ItemData.UldType + ItemData.UldSerialNo;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
