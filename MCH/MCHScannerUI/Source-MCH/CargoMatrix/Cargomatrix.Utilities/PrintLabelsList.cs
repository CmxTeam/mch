﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CargoMatrix.UI;
using CustomUtilities;


namespace CargoMatrix.CargoUtilities
{
    public partial class PrintLabelsList : SmoothListbox.SmoothListbox2
    {
        public PrintLabelsList()
        {
            InitializeComponent();

            this.panel1.Height = 0;
            this.panelHeader2.Height = 0;
            this.smoothListBoxMainList.MultiSelectEnabled = false;
            this.TitleText = "CargoUtilities";

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(PrintLabelsList_ListItemClicked);
        }

        void PrintLabelsList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            StandardListItem item = (StandardListItem)listItem;

            if (item == null) return;

            WorkItemTypeEnum? workItemType = null;

            switch ((ListItemTypeEnum)item.ID)
            {
                case ListItemTypeEnum.HOUSEBILL:

                    workItemType = WorkItemTypeEnum.HOUSEBILL;

                    break;

                case ListItemTypeEnum.MASTERBILL:

                    workItemType = WorkItemTypeEnum.MASTERBILL;

                    break;

                case ListItemTypeEnum.ULD:

                    workItemType = WorkItemTypeEnum.ULD;

                    break;

                case ListItemTypeEnum.ONHAND:

                    workItemType = WorkItemTypeEnum.ONHAND;

                    break;

                case ListItemTypeEnum.LOCATION:

                    workItemType = WorkItemTypeEnum.LOCATION;

                    break;

                case ListItemTypeEnum.TRUCK:

                    workItemType = WorkItemTypeEnum.TRUCK;

                    break;

                case ListItemTypeEnum.USER:

                    workItemType = WorkItemTypeEnum.USER;

                    break;
            }

            if (workItemType.HasValue == true)
            {
                this.PrintLabes(workItemType.Value);
            }
            sender.Reset();
        }


        private void PrintLabes(WorkItemTypeEnum workItemType)
        {
            string scannedText;
            string scannedItemMainPart;
            CMXBarcode.ScanObject scanItem;

            bool result = CommonMethods.GetItemFromUser(workItemType, out scannedText, out scannedItemMainPart, out scanItem);

            if (result == false) return;


            switch (workItemType)
            {
                case WorkItemTypeEnum.HOUSEBILL:

                    Cursor.Current = Cursors.WaitCursor;

                    var houseBillItem = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(scannedText);

                    Cursor.Current = Cursors.Default;

                    if (houseBillItem != null &&
                        houseBillItem.HousebillId != 0)
                    {

                        CommonMethods.PrintHouseBillLabels(houseBillItem);
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Housebill was not found", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                    break;
                case WorkItemTypeEnum.ONHAND:
                    Cursor.Current = Cursors.WaitCursor;

                    var onhand = CargoMatrix.Communication.OnHand.OnHand.Instance.GetOnHandDetails(scanItem.OnHandNumber);

                    Cursor.Current = Cursors.Default;

                    if (onhand != null &&
                        onhand.ShipmentID != 0)
                    {
                        CommonMethods.PrintOnhandLabels(onhand);
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Onhand was not found", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                    break;
                case WorkItemTypeEnum.MASTERBILL:

                    if (scanItem.BarcodeType == CMXBarcode.BarcodeTypes.MasterBill)
                    {
                        var masterbill = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillByNumber(scanItem.CarrierNumber, scanItem.MasterBillNumber);

                        if (masterbill != null &&
                            masterbill.MasterBillId != 0)
                        {
                            CommonMethods.PrintMasterBillLabels(masterbill);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Masterbill was not found", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                    break;

                case WorkItemTypeEnum.ULD:

                    CommonMethods.PrintUldLables(scannedItemMainPart);

                    break;

                case WorkItemTypeEnum.LOCATION:

                    var location = new CargoMatrix.Communication.ScannerUtilityWS.LocationItem();
                    location.Location = scannedItemMainPart;
                    location.LocationType = CommonMethods.GetLocationTypeByBarcodeType(scanItem.BarcodeType);

                    CommonMethods.PrintLocationLabels(location);

                    break;

                case WorkItemTypeEnum.TRUCK:

                    CommonMethods.PrintTruckLabel(scannedItemMainPart);

                    break;

                case WorkItemTypeEnum.USER:

                    CommonMethods.PrintUserLabel(scannedItemMainPart);

                    break;
            }
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.RemoveAll();

            this.AddMainListItem(new StandardListItem("HOUSEBILL", null, (int)ListItemTypeEnum.HOUSEBILL));

            this.AddMainListItem(new StandardListItem("MASTERBILL", null, (int)ListItemTypeEnum.MASTERBILL));

            this.AddMainListItem(new StandardListItem("ONHAND", null, (int)ListItemTypeEnum.ONHAND));

            this.AddMainListItem(new StandardListItem("ULD", CargoMatrix.Resources.Icons.ULD, (int)ListItemTypeEnum.ULD));

            this.AddMainListItem(new StandardListItem("LOCATION", CargoMatrix.Resources.Icons.Selection, (int)ListItemTypeEnum.LOCATION));

            this.AddMainListItem(new StandardListItem("TRUCK", CargoMatrix.Resources.Icons.Truck, (int)ListItemTypeEnum.TRUCK));

            this.AddMainListItem(new StandardListItem("USER", CargoMatrix.Resources.Skin.worker, (int)ListItemTypeEnum.USER));

        }

        enum ListItemTypeEnum
        {
            HOUSEBILL,
            MASTERBILL,
            ONHAND,
            ULD,
            LOCATION,
            TRUCK,
            USER
        }
    }
}
