﻿using System;

using System.Collections.Generic;
using System.Text;


namespace CargoMatrix.Utilities
{
    class ExceptionLookUpTable
    {
        public static string GetDetails(Exception exception)
        {
            ExceptionDatabaseDataSet ds = new ExceptionDatabaseDataSet();
            
            string details = (string)UtilitiesResource.ResourceManager.GetObject(exception.Message);
            if (details == null || details == "")
                return exception.Message;
            return details;
        }
    }
}
