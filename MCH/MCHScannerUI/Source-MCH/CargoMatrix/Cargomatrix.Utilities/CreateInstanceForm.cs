﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.CargoUtilities
{
    public abstract partial class CreateInstanceForm : Form
    {
        string header;
      
        #region Properties
        public string InstanceName
        {
            get { return this.txtName.Text.Trim(); }
        }

        public string InstanceCode
        {
            get { return this.txtCode.Text.Trim(); }

        }

        public string Header
        {
            get { return header; }
            set { header = value; }
        } 
        #endregion
        
        public CreateInstanceForm()
        {
            InitializeComponent();
        }

        

        #region Handlers
        void btnOk_Click(object sender, EventArgs e)
        {
            string errorMessage;
            bool validate = this.Validate(out errorMessage);

            if (validate == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(errorMessage, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                    
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(this.pnlMain.Left - 1, this.pnlMain.Top - 1, this.pnlMain.Width + 1, this.pnlMain.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        void pcxHeader_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            using (Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.Header,
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }     
        #endregion


        #region Additional Methods

        protected void InitializeFields()
        {
            this.pcxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            this.pcxFooter.Image = global::Resources.Graphics.Skin.nav_bg;
            this.pcxFooter.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pcxHeader.Paint += new PaintEventHandler(pcxHeader_Paint);

            this.btnCancel.Image = global::Resources.Graphics.Skin.nav_cancel;  //CargoMatrix.Resources.Skin.button_default;
            this.btnCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over; //CargoMatrix.Resources.Skin.button_pressed;//   ;
            this.btnCancel.Click += new EventHandler(btnCancel_Click);

            this.btnOk.Image = global::Resources.Graphics.Skin.nav_ok; //CargoMatrix.Resources.Skin.button_default; 
            this.btnOk.PressedImage = global::Resources.Graphics.Skin.nav_ok_over; //CargoMatrix.Resources.Skin.button_pressed;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new EventHandler(btnOk_Click);
        }

        protected virtual bool Validate(out string errorMessage)
        {
            errorMessage = null;

            if (string.IsNullOrEmpty(this.txtCode.Text.Trim()) == true)
            {
                errorMessage = "Code cannot be empty";
                return false;
            }

            if (string.IsNullOrEmpty(this.txtName.Text.Trim()) == true)
            {
                errorMessage = "Name cannot be empty";
                return false;
            }

            return true;
        }

        #endregion
    }
}