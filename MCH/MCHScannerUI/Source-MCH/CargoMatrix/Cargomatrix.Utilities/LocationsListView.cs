﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WarehouseManager;

namespace CargoMatrix.CargoUtilities
{
    public partial class LocationsListView : CustomListItems.MessageListBoxAsync<ILocation>
    {
        private ILocation[] locations;

        public LocationsListView(ILocation[] locations)
        {
            InitializeComponent();

            this.locations = locations;

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(LocationsListView_LoadListEvent);
            this.OkEnabled = false;
            this.OneTouchSelection = false;
            this.IsSelectable = false;
        }

        void LocationsListView_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent(this.locations);
        }

        private void PopulateContent(ILocation[] locations)
        {
            this.ReloadItemsAsync(locations.ToArray());
        }

        protected override Control InitializeItem(ILocation item)
        {
            var icon = CommonMethods.GetLocationIcon(item.LocationType);

            if(icon == null)
            {
                icon = CommonMethods.GetLocationIcon((LocationTypeEnum)item.TypeID);
            }

            return new SmoothListbox.ListItems.TwoLineListItem<ILocation>(item.Code, icon, item.TypeName, item);
        }
    }
}