﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class TaskDetailsItem : UserControl
    {
        public event EventHandler ButtonDetailsClick;
        public event EventHandler ButtonVendorClick;
        public event EventHandler ButtonTruckerClick;
        public event EventHandler ButtonRecoverClick;
        public event EventHandler ButtonKnownClick;
        public event EventHandler ButtonUnKnownClick;

        public string TruckingCompany
        {
            get { return textBoxTruckingCompany.Text; }
            set { textBoxTruckingCompany.Text = value; }
        }

        public string TruckerName
        {
            get { return textBoxTrucker.Text; }
            set { textBoxTrucker.Text = value; }
        }

        public string VehicleID
        {
            get { return textBoxIDNumber.Text; }
            set { textBoxIDNumber.Text = value; }
        }

        public string PRO
        {
            get { return textBoxPro.Text; }
            set { textBoxPro.Text = value; }
        }


        public string ShipperRef
        {
            get { return textBoxShipperRef.Text; }
            set { textBoxShipperRef.Text = value; }
        }

        public TaskDetailsItem()
        {
            InitializeComponent();
            InitialzeDetails();
        }

        public new void Focus()
        {
            textBoxTruckingCompany.Focus();
        }

        private void InitialzeDetails()
        {
            //this.labelDest.Text = SharedMethods.CurrentPickup.Destination;
            //this.labelPickupNo.Text = SharedMethods.CurrentPickup.PONumber;
            //this.labelVendor.Text = SharedMethods.CurrentPickup.Vendor;
            //this.labelRefNo.Text = SharedMethods.CurrentPickup.ShipmentNO;
            //this.panelIndicators.Flags = SharedMethods.CurrentPickup.Flags;
            //this.panelIndicators.PopupHeader = SharedMethods.CurrentPickup.ShipmentNO;
            //this.itemPicture.Image = SharedMethods.GetStatusIcon(SharedMethods.CurrentPickup.Status);
        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonDetailsClick != null)
                ButtonDetailsClick(sender, e);
        }

        void buttonTruckCompany_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonVendorClick != null)
                ButtonVendorClick(sender, e);
        }

        void buttonTrucker_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonTruckerClick != null)
                this.ButtonTruckerClick(sender, e);
        }

        void buttonRecover_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonRecoverClick != null)
                ButtonRecoverClick(sender, e);
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonKnown.Enabled = buttonUnKnown.Enabled = ValidateInput();
        }

        private bool ValidateInput()
        {
            return !string.IsNullOrEmpty(textBoxPro.Text) &&
                        !string.IsNullOrEmpty(textBoxTrucker.Text) &&
                        !string.IsNullOrEmpty(textBoxIDNumber.Text) &&
                        !string.IsNullOrEmpty(textBoxTruckingCompany.Text);
        }

        void buttonShipperRef_Click(object sender, System.EventArgs e)
        {

        }

        void buttonIDNumber_Click(object sender, System.EventArgs e)
        {
            //try
            //{
            //    UI.BarcodeReader.TermReader();
            //    using (FreightPhotoCapture.CameraPopup popup = new CargoMatrix.FreightPhotoCapture.CameraPopup(DataProvider.Title, "Trucker's ID NUmber", textBoxIDNumber.Text))
            //    {
            //        popup.ShowDialog();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    UI.CMXMessageBox.Show(ex.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            //}
            ShowVehicleOptions();
        }

        private void ShowVehicleOptions()
        {

            var actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;
            actPopup.HeaderText2 = "Vehicle Options";
            Control[] actions = new Control[]
                {   
                    new SmoothListbox.ListItems.StandardListItem("VEHICLE LOOKUP",null,1 ),
                    new SmoothListbox.ListItems.StandardListItem("LICENSE CAPTURE", null, 2),
                    new SmoothListbox.ListItems.StandardListItem("VEHICLE CONDITION",null, 3),
                };
            actPopup.AddItems(actions);
            actPopup.ShowDialog();
        }

        void textBoxTrackingList_TextChanged(object sender, System.EventArgs e)
        {
            buttonTruckCompany.Enabled = textBoxTruckingCompany.Text.Length > 2;
        }

        void buttonUnKnown_Click(object sender, System.EventArgs e)
        {
            if (buttonUnKnown != null)
                ButtonUnKnownClick(this, e);
        }

        void buttonKnown_Click(object sender, System.EventArgs e)
        {
            if (ButtonKnownClick != null)
                ButtonKnownClick(this, e);
        }
    }
}
