﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class PrintLabelsPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private LabelPrinter defaultPrinter;
        public PrintLabelsPopup()
        {
            InitializeComponent();
            this.buttonOk.Enabled = false;
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxCount.Text);
        }
        public new DialogResult ShowDialog()
        {
            defaultPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);
            SetTitle();
            return base.ShowDialog();
        }

        private void SetTitle()
        {
            this.Title = string.Format("Print Labels - {0} Printer", defaultPrinter.PrinterName);
        }
        void buttonPackageType_Click(object sender, System.EventArgs e)
        {
            if (CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD, ref defaultPrinter))
            {
                SetTitle();
            }
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
