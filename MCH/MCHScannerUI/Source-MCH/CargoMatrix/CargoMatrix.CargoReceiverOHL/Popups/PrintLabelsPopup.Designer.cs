﻿using CargoMatrix.UI;
namespace CargoMatrix.CargoReceiverOHL
{
    partial class PrintLabelsPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCount = new CMXTextBox();
            this.buttonPrinterList = new CMXPictureButton();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(4, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 36);
            this.label2.Text = string.Format("Ref# {0} has been received successfully",DataProvider.Title);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(3, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 30);
            this.label3.Text = "Confirm Print Label Count";
            // 
            // textBoxPieces
            // 
            this.textBoxCount.Location = new System.Drawing.Point(120, 85);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.InputMode = CMXTextBoxInputMode.Numeric;
            this.textBoxCount.Size = new System.Drawing.Size(60, 21);
            this.textBoxCount.TabIndex = 2;
            this.textBoxCount.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // buttonPackageType
            // 
            this.buttonPrinterList.Location = new System.Drawing.Point(195, 40);
            this.buttonPrinterList.Name = "buttonPrinterList";
            this.buttonPrinterList.Size = new System.Drawing.Size(32, 32);
            this.buttonPrinterList.TabIndex = 3;
            this.buttonPrinterList.Image = CargoMatrix.Resources.Skin.printerButton;
            this.buttonPrinterList.PressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            this.buttonPrinterList.Click += new System.EventHandler(buttonPackageType_Click);
            // 
            // LoosePiecesPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelContent.Controls.Add(this.buttonPrinterList);
            this.panelContent.Controls.Add(this.textBoxCount);
            this.panelContent.Controls.Add(this.label3);
            this.panelContent.Controls.Add(this.label2);
            this.Name = "LoosePiecesPopup";
            this.Size = new System.Drawing.Size(236, 150);
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private CMXTextBox textBoxCount;
        private CMXPictureButton buttonPrinterList;
    }
}
