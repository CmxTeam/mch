﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.CargoReceiverOHL
{
    public class DataProvider
    {

        internal static TruckManifest[] GetManifestList()
        {
            return new TruckManifest[]
            {
                new TruckManifest(){ ManifestNo = "BGW-155-10412356-JFK", Line2 = "HBs :1 Loc: ", Logo = CargoMatrix.Resources.Skin.status_history},
                new TruckManifest(){ ManifestNo = "OSR-615-83754775-JFK", Line2 = "HBs :1 Loc: ", Logo = CargoMatrix.Resources.Skin.Clipboard}
            };
        }

        public static List<DumbPackage> Packages
        {
            get { return packages; }
        }


        public static string Title { get; set; }
        private static List<DumbPackage> packages = new List<DumbPackage>();
    }

    public class TruckManifest 
    {
        public string ManifestNo { get; set; }
        public string Line2 { get; set; }
        public Image Logo { get; set; }
    }
    public class DumbPackage
    {
        public CMXPackageType Package{get;set;}
        public int Pieces { get; set; }
    }
    /*
    public class DumbULD : IULDItem 
    {
        #region IULDItem Members

        public string ULDNo
        {
            get;
            set;
        }

        public int ID
        {
            get;set;
        }

        public double Weight
        {
            get;set;
        }

        public int Pieces
        {
            get;
            set;
        }

        public string Location
        {
            get;
            set;
        }

        #endregion

        #region IULDType Members

        public string ULDName
        {
            get;
            set;
        }

        public int TypeID
        {
            get;
            set;
        }

        public bool IsLoose
        {
            get { return this.ULDName.Equals("loose", StringComparison.OrdinalIgnoreCase); }
        }

        #endregion
    }
    */
}
