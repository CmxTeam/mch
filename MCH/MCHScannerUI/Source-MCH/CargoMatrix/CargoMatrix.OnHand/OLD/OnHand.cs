﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using SmoothListbox.ListItems;
using CargoMatrix.Communication;
using CustomUtilities;
using CargoMatrix.Communication.ScannerUtilityWS;
using CargoMatrix.CargoMeasurements;
using CargoMatrix.Communication.WSOnHand;
using CargoMatrix.CargoMeasurements.MeasurementsCapture;

namespace CargoMatrix.OnHand
{
    public partial class OnHand : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        private OnHandDetailedItem hawbDetails;
        private bool dimsCaptured = false; // true if dims were succesfully captured
        private bool dimCaptureFlow = true; // true when dism capture screen is part of the creation flow else capture screen is an edit screen
        private OnhandDetails onHandDetails;

        public OnHand()
        {
            InitializeComponent();
            this.TitleText = "CargoReceiver - OnHand";
            BackButtonHold = false;
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(OnHand_BarcodeReadNotify);
        }

        public override void LoadControl()
        {
            //continue with popups
            if (dimCaptureFlow == true && dimsCaptured == true)
            {
                dimsCaptured = false;
                Application.DoEvents();
                CapureAdditionalDetails();
            }
            else
            {
                BarcodeEnabled = false;
                BarcodeEnabled = true;
            }
        }

        void OnHand_BarcodeReadNotify(string barcodeData)
        {
            var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (barcode.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    if (buttonScannedList.Value > 0)
                    {
                        string msg = string.Format("{0} piece/s will be dropped at location {1}", buttonScannedList.Value, barcode.Location);
                        CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, DialogResult.OK);
                    }
                    break;
                default:
                    break;
            }
            bool flag = false;
            dimsCaptured = false;

            if (flag)
                DiplayExistingOnhand();
            else
                NonExistingOnHandHandler();
            BarcodeEnabled = true;
        }

        private void DiplayExistingOnhand()
        {
            // call a webmethod to display existing onhand details.
            Communication.CargoReceiver.Instance.GetSealTypes();
            OnhandDetails ohd = new OnhandDetails()
            {
                carrier = "006",
                customerAccnt = "Account 2",
                destination = "ORD",
                measurements = new CargoMatrix.Communication.CargoMeasurements.DimensioningMeasurementDetails[0],
                onHandNo = "12345678",

            };
        }

        private void PhotoCapture()
        {
            Cursor.Current = Cursors.WaitCursor;
            //CargoMatrix.FreightPhotoCapture.ReasonsLogic.DisplayReasons(string.Empty, "rr", CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL, this, false);
        }

        private void NonExistingOnHandHandler()
        {
            onHandDetails = new OnhandDetails();

            if (false == SelectCarrier())
                return;

            if (false == SelectCustomerAccount())
                return;
            SelectAttributes();
            if (false == DisplayOriginDestintaionPopup())
                return;
            DisplayDocumentsScan();
            DisplayDimsCapture(false);

        }

        private void CapureAdditionalDetails()
        {
            if (hawbDetails == null)
            {
                hawbDetails = new OnHandDetailedItem(this) { Top = 50 };
                this.panelHeader2.Controls.Add(hawbDetails);
            }
            SelectReferences();
            PrintOnHandLabels();
            hawbDetails.DisplayHousebill(onHandDetails);
        }

        private void PrintOnHandLabels()
        {
            //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Do you want to print labels?", "Print Labels", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                LabelPrinter printer = new LabelPrinter();
                CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(LabelTypes.ULD, ref printer);
            }
        }

        private void GetDimensions(CargoMatrix.Communication.CargoMeasurements.DimensioningMeasurementDetails[] measures, CargoMatrix.Communication.WSCargoDimensioner.TransactionStatus[] statuses)
        {
            var fails = (from item in statuses
                        where item.transactionStatus == false
                        select item).ToList();

            if (fails.Count <= 0)
            {
                dimsCaptured = true;
                onHandDetails.measurements = measures;
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
            
            }
        }

        internal void DisplayDimsCapture(bool edit)
        {
            Cursor.Current = Cursors.WaitCursor;
            Measurements dimCapture;
            if (edit && onHandDetails != null)
            {
                dimCapture = new Measurements(GetDimensions);
                dimCapture.AddMeasurements(onHandDetails.measurements);
                dimCaptureFlow = false;
            }
            else
            {
                dimCapture = dimCapture = new Measurements(GetDimensions, 2);
                dimCaptureFlow = true;
            }
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(dimCapture);
            Cursor.Current = Cursors.Default;
        }

        private bool DisplayDocumentsScan()
        {
            CargoMatrix.UI.CMXMessageBox.Show("Do you want to scan documents?", "Scan Documents", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No);
            return false;
        }


        private bool DisplayCapturePhotos()
        {
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Do you want to take photos?", "Photo Capture", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                CargoMatrix.FreightPhotoCapture.Reasons reasons = new CargoMatrix.FreightPhotoCapture.Reasons(false);
                CargoMatrix.FreightPhotoCapture.ReasonsLogic.DisplayReasons(string.Empty, "4AJ8539", CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL, this, ref reasons);
            }
            return false;
        }

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {

        }

        private void InitializeComponentHelper()
        {
            panelHeader2.Height = 232;
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;

            this.LoadOptionsMenu += new EventHandler(OnHand_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(OnHand_MenuItemClicked);
            topLabel.Text = "Scan Package Label";
            smoothListBoxMainList.Scrollable = false;
        }


        private bool DisplayOriginDestintaionPopup()
        {
            string org = WebServiceManager.Instance().m_gateway, des = string.Empty;
            int pcs = 1;
            if (DialogResult.OK == CustomUtilities.OriginDestinationMessageBox.Show2("Enter On Hand Info", ref pcs, ref org, ref des))
            {
                onHandDetails.noOfPcs = pcs;
                onHandDetails.onHandNo = org + " - 889276352 - " + des;
                return true;
            }
            return false;
        }

        internal bool SelectCarrier()
        {
            CustomUtilities.SearchMessageListBox carriers = new CustomUtilities.SearchMessageListBox();
            carriers.HeaderText = "Select Domestic Carrier";
            foreach (var item in CargoMatrix.Communication.OnHand.OnHand.Instance.GetDomesticCarriers())
            {
                carriers.AddItem(new StandardListItem<Carrier>(item.CarrierName, null, (int)(item.CarrierrId), item));
            }
            
            carriers.OneTouchSelection = true;
            carriers.MultiSelectListEnabled = false;
            if (DialogResult.OK == carriers.ShowDialog())
            {
                onHandDetails.carrier = (carriers.SelectedItems[0] as StandardListItem).title.Text;
                return true;
            }
            else
            {
                onHandDetails.carrier = string.Empty;
                return false;
            }
        }

        internal bool SelectCustomerAccount()
        {
            CustomUtilities.SearchMessageListBox accountsList = new CustomUtilities.SearchMessageListBox();
            accountsList.HeaderText = "Select Customer Account";
            foreach (var item in CargoMatrix.Communication.OnHand.OnHand.Instance.GetCustomerAccounts())
            {
                accountsList.AddItem(new SmoothListbox.ListItems.StandardListItem<Customer>(item.CustomerName, null, item));

            }
            accountsList.OneTouchSelection = true;
            accountsList.MultiSelectListEnabled = false;
            if (DialogResult.OK == accountsList.ShowDialog())
            {
                onHandDetails.customerAccnt = (accountsList.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).title.Text;
                return true;
            }
            else
                return false;
        }

        internal void SelectAttributes()
        {
            var attributesList = new CargoMatrix.Utilities.MessageListBox();
            attributesList.HeaderText = "Attributes List";
            attributesList.HeaderText2 = "Select/deselect attributes";
            attributesList.ButtonImage = CargoMatrix.Resources.Skin.printerButton;
            attributesList.ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            attributesList.OkEnabled = true;
            foreach (var item in CargoMatrix.Communication.OnHand.OnHand.Instance.GetShipmentAttributes(1))
            {
                attributesList.AddItem(new CustomListItems.CkeckItem<ShipmentAttribute>(item.AttributeName, item.AttribueId, item));
            }

            if (DialogResult.OK == attributesList.ShowDialog())
            {
                onHandDetails.attributes = (from attr in attributesList.SelectedItems.OfType<CustomListItems.CkeckItem<string>>()
                                            select attr.ItemData).ToArray<string>();
            }
        }

        internal void SelectReferences()
        {
            new ReferenceEditor().ShowDialog();
        }

        void OnHand_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                        BarcodeEnabled = false;
                        //CargoMatrix.FreightPhotoCapture.PhotoCapturePopup popup = new CargoMatrix.FreightPhotoCapture.PhotoCapturePopup();
                        var popup = new CustomUtilities.EnterReferencePopup();
                        popup.Title = "Enter Shipment Number";
                        if (DialogResult.OK == popup.ShowDialog())
                        {
                            OnHand_BarcodeReadNotify(popup.EnteredText);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        void OnHand_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO) { Title = "Enter Reference Number" });
        }
    }
}
