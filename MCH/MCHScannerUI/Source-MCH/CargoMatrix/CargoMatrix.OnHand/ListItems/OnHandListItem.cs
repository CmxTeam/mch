﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;
using SmoothListbox;
using CargoMatrix.Communication.OnHand;

namespace CargoMatrix.OnHand
{

    public partial class OnHandListItem : SmoothListbox.ListItems.ListItem<IShipment>, ISmartListItem
    {
        public OnHandListItem(IShipment dataItem)
            : base(dataItem)
        {
            InitializeComponent();
            DisplayFields();
        }

        private void DisplayFields()
        {
            this.labelLine1.Text = this.ItemData.ShipmentNO;
            this.labelLine2.Text = this.ItemData.Vendor;
            this.labelLine3.Text = this.ItemData.PONumber;
            this.labelLine4.Text = string.Format("Pcs: {0}\tDest: {1}", this.ItemData.TotalPieces, this.ItemData.Destination);
            //this.labelLine5.Text = this.ItemData.TruckingCompany;
            this.panelIndicators.Flags = this.ItemData.Flags;
            this.panelIndicators.PopupHeader = this.ItemData.ShipmentNO;
            this.pictureBoxItemStatus.Image = SharedMethods.GetStatusIcon(this.ItemData.Status);
        }

        public bool Contains(string text)
        {
            string txtToMatch = text.ToUpper();
            return this.labelLine1.Text.ToUpper().Contains(txtToMatch) ||
                this.labelLine2.Text.ToUpper().Contains(txtToMatch) ||
                this.labelLine3.Text.ToUpper().Contains(txtToMatch);// ||
                //this.labelLine5.Text.ToUpper().Contains(txtToMatch);
        }

        public bool Filter
        {
            get;
            set;
        }
    }

}
