﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.PieceScanWCF;
using SmoothListbox.ListItems;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;
using CargoMatrix.Communication.OnHand;
using System.Drawing;
using CustomListItems;
using CargoMatrix.Utilities;

namespace CargoMatrix.OnHand
{
    class SharedMethods
    {
        private static IShipment currentPickup;

        public static IShipment CurrentPickup
        {
            get
            {
                if (currentPickup == null)
                    currentPickup = new CMXShipment();
                return currentPickup;
            }
            internal set
            {
                currentPickup = value;
                if (value is CMXPickup || value is OnHandTaskItem)
                    CurrentPickupID = value.ShipmentID;
                if (value is OnHandTaskItem)
                {
                    CurrentPONumber = value.PONumber;
                    CurrentVendorName = value.Vendor;
                }
            }
        }
        public static long CurrentPickupID
        {
            get;
            internal set;
        }

        public static string CurrentPONumber { get; private set; }
        public static string CurrentVendorName { get; private set; }

        public static CMXVendor DisplayVendorsList(string searchStr)
        {
            var VendorList = new CustomUtilities.SearchMessageListBox();
            VendorList.HeaderText = "Select Vendor";
            VendorList.MultiSelectListEnabled = false;
            VendorList.OneTouchSelection = true;
            foreach (var vendor in Communication.OnHand.OnHand.Instance.GetVendors(searchStr))
            {
                VendorList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXVendor>(vendor.VendorName, null, vendor));
            }
            VendorList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == VendorList.ShowDialog())
            {
                return (VendorList.SelectedItems[0] as StandardListItem<CMXVendor>).ItemData;
            }
            else return null;
        }

        public static CMXAccount DisplayCustomersList()
        {
            var VendorList = new CustomUtilities.SearchMessageListBox();
            VendorList.HeaderText = "Select Customer";
            VendorList.MultiSelectListEnabled = false;
            VendorList.OneTouchSelection = true;
            foreach (var accnt in Communication.OnHand.OnHand.Instance.GetAllCustomersList())
            {
                VendorList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXAccount>(accnt.AccountName, null, accnt));
            }
            VendorList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == VendorList.ShowDialog())
            {
                return (VendorList.SelectedItems[0] as StandardListItem<CMXAccount>).ItemData;
            }
            else return null;
        }

        public static CMXCarrier DisplayTruckingCompanies(string searchStr)
        {
            var truckingCompList = new CustomUtilities.SearchMessageListBox();
            truckingCompList.HeaderText = "Select Customer";
            truckingCompList.MultiSelectListEnabled = false;
            truckingCompList.OneTouchSelection = true;
            foreach (var carrier in Communication.OnHand.OnHand.Instance.GetTruckingCompanyList(searchStr))
            {
                truckingCompList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXCarrier>(carrier.CarrierName, null, carrier));
            }
            truckingCompList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == truckingCompList.ShowDialog())
            {
                return (truckingCompList.SelectedItems[0] as StandardListItem<CMXCarrier>).ItemData;
            }
            else return null;
        }

        public static string DisplayTruckersList(string company)
        {
            var truckersList = new CustomUtilities.SearchMessageListBox();
            truckersList.HeaderText = "Select Trucker";
            truckersList.MultiSelectListEnabled = false;
            truckersList.OneTouchSelection = true;
            foreach (var trucker in Communication.OnHand.OnHand.Instance.GetTruckerNameList(company))
            {
                truckersList.smoothListBoxBase1.AddItem2(new StandardListItem<string>(trucker, null, trucker));
            }
            truckersList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == truckersList.ShowDialog())
            {
                return (truckersList.SelectedItems[0] as StandardListItem<string>).ItemData;
            }
            else return null;
        }

        public static string DisplayPackageTypesList()
        {
            var pkgTypeList = new CustomUtilities.SearchMessageListBox();
            pkgTypeList.HeaderText = "Select Package Type";
            pkgTypeList.MultiSelectListEnabled = false;
            pkgTypeList.OneTouchSelection = true;
            foreach (var pkgType in Communication.OnHand.OnHand.Instance.GetPackageTypes())
            {
                pkgTypeList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXPackageType>(pkgType.Name, null, pkgType));
            }
            pkgTypeList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == pkgTypeList.ShowDialog())
            {
                return (pkgTypeList.SelectedItems[0] as StandardListItem<CMXPackageType>).ItemData.Name;
            }
            else return null;
        }

        public static string DisplayConditionsList()
        {
            var condList = new CustomUtilities.SearchMessageListBox();
            condList.HeaderText = "Select Condition";
            condList.MultiSelectListEnabled = false;
            condList.OneTouchSelection = true;
            foreach (var pkgType in Communication.OnHand.OnHand.Instance.GetConditions())
            {
                condList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXPackageCondition>(pkgType.Name, null, pkgType));
            }
            condList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == condList.ShowDialog())
            {
                return (condList.SelectedItems[0] as StandardListItem<CMXPackageCondition>).ItemData.Name;
            }
            else return null;
        }

        public static string DisplayMOTList()
        {
            var motList = new CustomUtilities.SearchMessageListBox();
            motList.HeaderText = "Select Mode of Transportation";
            motList.MultiSelectListEnabled = false;
            motList.OneTouchSelection = true;
            foreach (var mot in Communication.OnHand.OnHand.Instance.GetMOTList())
            {
                motList.smoothListBoxBase1.AddItem2(new StandardListItem<MODItem>(mot.Description, null, mot));
            }
            motList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == motList.ShowDialog())
            {
                return (motList.SelectedItems[0] as StandardListItem<MODItem>).ItemData.Description;
            }
            else return null;
        }

        public static Image GetStatusIcon(ShipmentStatus status)
        {
            switch (status)
            {
                case ShipmentStatus.Complete:
                    return CargoMatrix.Resources.Skin.Clipboard_Check;
                case ShipmentStatus.Inprogress:
                    return CargoMatrix.Resources.Skin.status_history;
                default:
                    return CargoMatrix.Resources.Skin.Clipboard;
            }
        }

        public static void EditCurretShipmentAttributes()
        {
            var attribList = new MessageListBox();
            attribList.HeaderText = CurrentPONumber;
            attribList.HeaderText2 = "Select Attribute";
            attribList.MultiSelectListEnabled = true;
            attribList.OneTouchSelection = false;
            attribList.OkEnabled = true;
            attribList.ListItemClicked += attribList_ListItemClicked;// (sender, listItem, isSelected) => { };
            foreach (var attr in Communication.OnHand.OnHand.Instance.GetAttributes(SharedMethods.CurrentPickup.ShipmentID))
            {
                var item = new CustomListItems.CkeckItem<CMXAttribute>(attr.AttributeDescription, attr.ID, attr);
                attribList.smoothListBoxBase1.AddItem2(item);
            }
            attribList.smoothListBoxBase1.LayoutItems();
            foreach (var item in attribList.smoothListBoxBase1.Items.OfType<CkeckItem<CMXAttribute>>().Where(listItem => listItem.ItemData.IsActive))
            {
                attribList.smoothListBoxBase1.SelectControl(item);
            }

            if (DialogResult.OK == attribList.ShowDialog())
            {
                var atributes = attribList.Items.OfType<CkeckItem<CMXAttribute>>().Select<CkeckItem<CMXAttribute>, CMXAttribute>(litem => litem.ItemData).ToArray<CMXAttribute>();
                var status = Communication.OnHand.OnHand.Instance.UpdateOnhandAttributes(CurrentPickup.ShipmentID, atributes);
                if (status.TransactionStatus1 == false)
                    ShowFailMessage(status.TransactionError);
            }
        }

        static void attribList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            (listItem as CkeckItem<CMXAttribute>).ItemData.IsActive = isSelected;
        }

        public static CMXInvoiceOrder DisplayOrderList()
        {
            var orderList = new CustomUtilities.SearchMessageListBox();
            orderList.HeaderText = "Select Line Item";
            orderList.MultiSelectListEnabled = false;
            orderList.OneTouchSelection = true;
            foreach (var invoice in Communication.OnHand.OnHand.Instance.GetPickupOrderList(SharedMethods.CurrentPickupID, SharedMethods.CurrentPONumber, null, SharedMethods.CurrentPickup.CustomerCode))
            {
                string line1 = string.Format("Line item# {0}: PO Qty {1}", invoice.LineItemNumber, invoice.OrderQty);
                orderList.smoothListBoxBase1.AddItem2(new TwoLineListItem<CMXInvoiceOrder>(line1, global::Resources.Graphics.Skin.radio_button, "Part# " + invoice.PartNumber, invoice));
            }
            orderList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == orderList.ShowDialog())
            {
                return (orderList.SelectedItems[0] as TwoLineListItem<CMXInvoiceOrder>).ItemData;
            }
            else return null;
        }

        public static CMXReference DisplayReferenceTypes()
        {
            var refTypeList = new CustomUtilities.SearchMessageListBox();
            refTypeList.HeaderText = "Select Reference Type";
            refTypeList.MultiSelectListEnabled = false;
            refTypeList.OneTouchSelection = true;

            foreach (var refType in Communication.OnHand.OnHand.Instance.GetReferenceTypes(SharedMethods.CurrentPickup.CustomerCode))
            {
                refTypeList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXReference>(refType.ReferenceTitle, null, refType));
            }
            refTypeList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == refTypeList.ShowDialog())
            {
                return (refTypeList.SelectedItems[0] as StandardListItem<CMXReference>).ItemData;
            }
            else return null;
        }
        public static int[] DisplayPiecesList()
        {
            var PiecesList = new MessageListBox();
            PiecesList.HeaderText = "Pieces List";
            PiecesList.HeaderText2 = "Select Pieces to Move to Forklift";
            PiecesList.MultiSelectListEnabled = true;
            PiecesList.OneTouchSelection = false;

            var pieces = Communication.OnHand.OnHand.Instance.GetOnhandPieces(Forklift.Instance.TaskID);
            foreach (var pieceItem in pieces)
            {
                string line1 = string.Format("Piece: {0} of {1}", pieceItem.PieceNumber, pieces.Length);
                //string line2 = string.Format("Pkg: {0}  Wgt: {1} {2}", pieceItem.Type, pieceItem.Weight.HasValue ? pieceItem.Weight.Value.ToString("0.") : "0", pieceItem.WeightUOM);
                //string line3 = string.Format("Dims : {0} x {1} x {2} {3}", pieceItem.Length, pieceItem.Width, pieceItem.Height, pieceItem.DimUOM);

                var pieceListItem = new StandardListItem<PieceItem>(line1, Resources.Skin.PieceMode, pieceItem);
                PiecesList.AddItem(pieceListItem);
            }
            if (DialogResult.OK == PiecesList.ShowDialog())
            {
                return PiecesList.SelectedItems.OfType<StandardListItem<PieceItem>>().Select(selIte => selIte.ItemData.PieceId).ToArray<int>();
            }
            else

                return null;

        }

        public static void ShowFailMessage(string msg)
        {
            UI.CMXMessageBox.Show(msg, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }
    }


    public class SearchItem
    {
        public string PickupRefNo;
        public string Customer;
        public string Vendor;
        public string OrderRef;
        public SearchItem(string pickupRef, string cust, string vndr, string orderRef)
        {
            PickupRefNo = pickupRef;
            Customer = cust;
            Vendor = vndr;
            OrderRef = orderRef;
        }
    }
}
