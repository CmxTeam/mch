﻿using System;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using System.Windows.Forms;

namespace CargoMatrix.OnHand
{
    partial class OnHandTasksList
    {

        private void InitializeComponents()
        {
            this.SuspendLayout();
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.SuspendLayout();
            //
            // searchbox
            //
            this.searchBox = new CargoMatrix.UI.CMXTextBox();
            this.searchBox.Location = new System.Drawing.Point(4, 21);
            this.searchBox.Size = new System.Drawing.Size(232, 23);
            this.searchBox.TextChanged += new EventHandler(searchBox_TextChanged);
            //
            //label1
            //
            this.labelFilterSort = new System.Windows.Forms.Label();
            this.labelFilterSort.Location = new System.Drawing.Point(4, 4);
            this.labelFilterSort.Size = new System.Drawing.Size(198, 15);
            this.labelFilterSort.BackColor = System.Drawing.Color.White;
            this.labelFilterSort.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelFilterSort.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //
            //
            //this.buttonFilter = new CargoMatrix.UI.CMXPictureButton();
            //this.buttonFilter.Location = new System.Drawing.Point(202, 10);
            //this.buttonFilter.Size = new System.Drawing.Size(32, 32);
            //this.buttonFilter.Image = CargoMatrix.Resources.Skin.filter_btn;
            //this.buttonFilter.PressedImage = CargoMatrix.Resources.Skin.filter_btn_over;
            //this.buttonFilter.Click += new EventHandler(buttonFilter_Click);
            //
            //
            //
            this.panelHeader2.Controls.Add(searchBox);
            //this.panelHeader2.Controls.Add(buttonFilter);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            this.panelHeader2.Controls.Add(labelFilterSort);
            this.panelHeader2.Height = 49;
            this.panelHeader2.Visible = true;
            this.Name = FORM_NAME; // used for navigation purposes
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);

            this.ResumeLayout(false);
        }

        void buttonFilter_Click(object sender, EventArgs e)
        {
        }

        protected System.Windows.Forms.Label labelFilterSort;
        //protected CargoMatrix.UI.CMXPictureButton buttonFilter;
        protected CargoMatrix.UI.CMXTextBox searchBox;

    }
}