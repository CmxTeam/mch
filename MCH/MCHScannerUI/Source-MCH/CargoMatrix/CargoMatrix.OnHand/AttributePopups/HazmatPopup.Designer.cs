﻿namespace CargoMatrix.OnHand
{
    partial class HazmatPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxUnNo = new CargoMatrix.UI.CMXTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxUNClass = new CargoMatrix.UI.CMXTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxUNShipName = new CargoMatrix.UI.CMXTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(4, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.Text = "UN Number";
            // 
            // textBoxUnNo
            // 
            this.textBoxUnNo.Location = new System.Drawing.Point(4, 47);
            this.textBoxUnNo.Name = "textBoxUnNo";
            this.textBoxUnNo.Size = new System.Drawing.Size(230, 21);
            this.textBoxUnNo.TabIndex = 1;
            this.textBoxUnNo.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            this.textBoxUnNo.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.Text = "UN Class";
            // 
            // textBoxUNClass
            // 
            this.textBoxUNClass.Location = new System.Drawing.Point(4, 87);
            this.textBoxUNClass.Name = "textBoxUNClass";
            this.textBoxUNClass.Size = new System.Drawing.Size(230, 21);
            this.textBoxUNClass.TabIndex = 2;
            this.textBoxUNClass.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            this.textBoxUNClass.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 13);
            this.label3.Text = "UN Shipping Name";
            // 
            // textBoxUNShipName
            // 
            this.textBoxUNShipName.Location = new System.Drawing.Point(4, 126);
            this.textBoxUNShipName.Name = "textBoxUNShipName";
            this.textBoxUNShipName.Size = new System.Drawing.Size(230, 21);
            this.textBoxUNShipName.TabIndex = 3;
            this.textBoxUNShipName.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            this.textBoxUNShipName.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // HazmatPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.textBoxUNShipName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxUNClass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxUnNo);
            this.Controls.Add(this.label1);
            this.Name = "HazmatPopup";
            this.Size = new System.Drawing.Size(240, 150);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CargoMatrix.UI.CMXTextBox textBoxUnNo;
        private System.Windows.Forms.Label label2;
        private CargoMatrix.UI.CMXTextBox textBoxUNClass;
        private System.Windows.Forms.Label label3;
        private CargoMatrix.UI.CMXTextBox textBoxUNShipName;
    }
}
