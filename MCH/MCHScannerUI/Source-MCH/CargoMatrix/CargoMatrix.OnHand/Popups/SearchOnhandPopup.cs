﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.OnHand
{
    public partial class SearchOnhandPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        public string PickupRefNo
        {
            get { return textBoxPickupRef.Text; }
            set { textBoxPickupRef.Text = value; }
        }
        public string Customer
        {
            get { return textBoxCustomer.Text; }
            set { textBoxCustomer.Text = value; }
        }
        public string Vendor
        {
            get { return textBoxVendor.Text; }
            set { textBoxVendor.Text = value; }
        }
        public string OrderReference
        {
            get { return textBoxOrdRef.Text; }
            set { textBoxOrdRef.Text = value; }
        }

        public SearchOnhandPopup()
        {
            InitializeComponent();
            this.Title = "CargoOnhand - Search";
            textBox_TextChanged(null, EventArgs.Empty);
        }

        private void textBox_TextChanged(object sender, System.EventArgs e)
        {
            // disable OK button if texboxes are empty
            // enable OK button if one of the textboxes has text
            bool enableFlag = !string.IsNullOrEmpty(textBoxPickupRef.Text.Trim()) ||
                //!string.IsNullOrEmpty(textBoxCustomer.Text.Trim()) ||
                !string.IsNullOrEmpty(textBoxVendor.Text.Trim()) ||
                !string.IsNullOrEmpty(textBoxOrdRef.Text.Trim());

            buttonOk.Enabled = enableFlag;
        }

        void textBoxVendor_TextChanged(object sender, System.EventArgs e)
        {
            //enable ventdor popup if text is 3 or more characters
            buttonVendor.Enabled = textBoxVendor.Text.Trim().Length > 2;
        }

        void buttonVendor_Click(object sender, System.EventArgs e)
        {
            var vndr = SharedMethods.DisplayVendorsList(textBoxVendor.Text);
            if (vndr != null)
            {
                Vendor = vndr.VendorName;
            }
        }

        void buttonCustomer_Click(object sender, System.EventArgs e)
        {
            var accnt = SharedMethods.DisplayCustomersList();
            if (accnt != null)
            {
                Customer = accnt.AccountName;
            }
        }
    }
}
