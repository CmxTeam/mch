﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.TaskManager;
using CustomUtilities;
using CargoMatrix.Utilities;
using CargoMatrix.UI;

namespace CargoMatrix.TaskManager
{
    public partial class UserList : MessageListBoxAsync<IBusinessUser>
    {
        BusinessTask[] tasks;
        ActionTypeEnum actionType;

        public ActionTypeEnum ActionType
        {
            get { return actionType; }
        }

        public UserList(BusinessTask[] tasks)
        {
            this.tasks = tasks;
            
            this.actionType = ActionTypeEnum.NA;
            this.MultiSelectListEnabled = true;
            this.OneTouchSelection = false;
            this.HeaderText = "Assign users";
            this.OkEnabled = true;

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(UserList_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UserList_ListItemClicked);
        }

        void UserList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var item = (UserItemRendered)listItem;

            item.IsChecked = isSelected;
        }

        void UserList_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.LoadControl();
        }

        protected override Control InitializeItem(IBusinessUser item)
        {
            return null; // is not used.  legacy stuff
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            var users = from item in this.smoothListBoxBase1.SelectedItems.OfType<UserItemRendered>()
                        select item.User;
            
            if(users.Count() <= 0)
            {
                CMXMessageBox.Show("Select at least one user", "Error", CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                return;
            }

            IBusinessUser taskOwner = null;

            if(users.Count() == 1)
            {
                taskOwner = users.First();
            }
            else
            {
                MessageListBox userList = new MessageListBox();
                userList.OneTouchSelection = true;
                userList.MultiSelectListEnabled = false;
                userList.HeaderText = "Select task owner";
                userList.HeaderText2 = string.Empty;


                foreach (var user in users)
                {
                    string userFullName = string.Format("{0} {1}", user.FirstName, user.LastName);

                    userList.AddItem(new SmoothListbox.ListItems.TwoLineListItem<IBusinessUser>(userFullName, CargoMatrix.Resources.Skin.worker, user.UserId, user));
                }

                var result = userList.ShowDialog();

                if (result != DialogResult.OK) return;

                 taskOwner = ((SmoothListbox.ListItems.TwoLineListItem<IBusinessUser>)userList.SelectedItems[0]).ItemData;
            }
           

            Cursor.Current = Cursors.WaitCursor;
            
            
            foreach (var task in tasks)
            {
                CargoMatrix.Communication.TaskManager.TaskManagerProvider.Instance.AssignUsers(task, taskOwner, users.ToArray());
            }
           
            Cursor.Current = Cursors.Default;
            
            this.actionType = ActionTypeEnum.Assign;

            this.Close();
        }

        private void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            var userNames = (from task in this.tasks
                            from userName in task.UserIDs
                            select userName).Distinct();


            List<IBusinessUser> criterionUsers;
            List<IBusinessUser> inCriterionUsers;
            CargoMatrix.Communication.TaskManager.TaskManagerProvider.Instance.GetUsersByCriterion(userNames.ToArray(), out criterionUsers, out inCriterionUsers);

            this.smoothListBoxBase1.RemoveAll();

            foreach (var user in criterionUsers)
            {
                var item = new UserItemRendered(user, true);
                smoothListBoxBase1.AddItem2(item);

                this.SelectControl(item);
            }

            foreach (var user in inCriterionUsers)
            {
                smoothListBoxBase1.AddItem2(new UserItemRendered(user));
            }

          
            smoothListBoxBase1.LayoutItems();
            smoothListBoxBase1.RefreshScroll();

            Cursor.Current = Cursors.Default;
        }

       
        public enum ActionTypeEnum
        {
            Assign,
            NA
        }
    }
}