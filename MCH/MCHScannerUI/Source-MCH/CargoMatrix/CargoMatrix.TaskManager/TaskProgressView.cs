﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.TaskManager;
using CargoMatrix.Utilities;

namespace CargoMatrix.TaskManager
{
    public partial class TaskProgressView : Form
    {
        private CargoMatrix.UI.CMXPictureButton btnCancel;
        private CargoMatrix.UI.CMXPictureButton btnAssign;
        private CargoMatrix.UI.CMXPictureButton btnUsers;

        string[] assignedUsers;
        string taskReference;
        ActionTypeEnum actionType;
        Timer timer;

        public Func<CargoMatrix.Communication.WSTaskManager.Task> GetTask;



        public string Header
        {
            get;
            set;
        }

        public ActionTypeEnum ActionType
        {
            get { return actionType; }
        }

        public TaskProgressView()
        {
            InitializeComponent();

            this.actionType = ActionTypeEnum.NoAction;

            this.timer = new Timer();
            this.timer.Interval = 30000;
            this.timer.Tick += new EventHandler(timer_Tick);

            this.Load += new EventHandler(TaskProgressView_Load);
        }

        protected void InitializeFields()
        {
            this.btnCancel = new CargoMatrix.UI.CMXPictureButton();
            this.btnAssign = new CargoMatrix.UI.CMXPictureButton();
            this.btnUsers = new CargoMatrix.UI.CMXPictureButton();
            
            this.pcxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            this.pcxHeader.Paint += new PaintEventHandler(pcxHeader_Paint);
            
            this.pcxFooter.Image = global::Resources.Graphics.Skin.nav_bg;

            // 
            // buttonCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(150, 208);
            this.btnCancel.Name = "buttonCancel";
            this.btnCancel.Size = new System.Drawing.Size(52, 37);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.TransparentColor = System.Drawing.Color.White;

            this.btnCancel.Image = global::Resources.Graphics.Skin.nav_cancel;  //CargoMatrix.Resources.Skin.button_default;
            this.btnCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over; //CargoMatrix.Resources.Skin.button_pressed;//   ;
            this.btnCancel.Click += new EventHandler(btnCancel_Click);


            // 
            // btnAssign
            // 
            this.btnAssign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAssign.Location = new System.Drawing.Point(40, 208);
            this.btnAssign.Name = "btnAssign";
            this.btnAssign.Size = new System.Drawing.Size(52, 37);
            this.btnAssign.TransparentColor = System.Drawing.Color.White;

            this.btnAssign.Image = CargoMatrix.Resources.Skin.worker_btn;  
            this.btnAssign.PressedImage = CargoMatrix.Resources.Skin.worker_btn_over;
            this.btnAssign.Click += new EventHandler(btnAssign_Click);


            // 
            // btnUsers
            // 
            this.btnUsers.Location = new System.Drawing.Point(200, 191);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(32, 32);
            this.btnUsers.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.btnUsers.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;
            this.btnUsers.TransparentColor = System.Drawing.Color.White;
            this.btnUsers.Click += new System.EventHandler(this.btNextUser_Click);

            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAssign);
            this.Controls.Add(this.btnUsers);
        }

        void btnAssign_Click(object sender, EventArgs e)
        {
            this.actionType = ActionTypeEnum.Assign;

            this.Close();
        }

        void pcxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.Header,
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void TaskProgressView_Load(object sender, EventArgs e)
        {
            var getTask = this.GetTask;

            if (getTask != null)
            {
                var task = GetTask();
                this.PopulateTask(task);
            }

            this.timer.Enabled = true;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            var getTask = this.GetTask;

            if (getTask != null)
            {
                var task = GetTask();
                this.PopulateTask(task);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(this.pnlMain.Left - 1, this.pnlMain.Top - 1, this.pnlMain.Width + 1, this.pnlMain.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }
    
        public void PopulateTask(CargoMatrix.Communication.WSTaskManager.Task task)
        {
            this.assignedUsers = task.AssignUsers;
            this.taskReference = task.Reference;
            
            this.lblTask.Text = task.TaskName;
            this.lblConsol.Text = task.Reference;
            
            this.lblCloseOut.Text = task.CloseoutTime.HasValue ? task.CloseoutTime.Value.ToString("G") : string.Empty;

            if (this.assignedUsers != null && this.assignedUsers.Length > 0)
            {
                string users = this.assignedUsers.Length > 1 ? string.Format("{0} ...", this.assignedUsers[0]) : this.assignedUsers[0];

                this.lblUsers.Text = users;
            }
            else
            {
                this.lblUsers.Text = string.Empty;
            }

            this.lblStatus.Text = task.Status.AsString();
            this.lblStart.Text = task.StartDate.HasValue ? task.StartDate.Value.ToString("G") : string.Empty;
            this.lblEnd.Text = task.EndDate.HasValue ? task.EndDate.Value.ToString("G") : string.Empty;
            this.lblPieces.Text = string.Format("{0} of {1}", task.CompletedPieces, task.TotalPieces);
            this.lblLastHawb.Text = task.LastScanHouseBillNo;
            this.lblLastScan.Text = task.LastScan.HasValue ? task.LastScan.Value.ToString("G") : string.Empty;
            this.lblProgress.Text = double.IsNaN(task.Progress) == false ? string.Format("{0:N2} %", task.Progress) : string.Empty;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.timer.Enabled = false;
            
            base.OnClosing(e);
        }

        private void btNextUser_Click(object sender, EventArgs e)
        {
            if(this.assignedUsers != null && this.assignedUsers.Length > 0)
            {
                MessageListBox usersList = new MessageListBox();
                usersList.HeaderText = this.taskReference;
                usersList.HeaderText2 = "Assigned Users";

                foreach (var user in this.assignedUsers)
                {
                    var listItem = new SmoothListbox.ListItems.StandardListItem(user, CargoMatrix.Resources.Skin.worker);
                    usersList.AddItem(listItem);
                }

                usersList.OneTouchSelection = false;
                usersList.OkEnabled = false;
                usersList.MultiSelectListEnabled = false;

                usersList.ShowDialog();
            }
        }


        public enum ActionTypeEnum
        {
            NoAction,
            Assign
        }
    }
}