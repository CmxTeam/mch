﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Utilities;
using System.Windows.Forms;

namespace CargoMatrix.TaskManager
{
    public class TaskDetailsList : MessageListBox
    {
        TaskDetailsControl detailsControl;
        ActionTypeEnum actionType;
        Timer timer;

        public Func<CargoMatrix.Communication.WSTaskManager.Task> GetTask;

        public ActionTypeEnum ActionType
        {
            get { return actionType; }
        }
        
        public TaskDetailsList()
        {
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            this.actionType = ActionTypeEnum.NoAction;

            this.timer = new Timer();
            this.timer.Interval = 30000;
            this.timer.Tick += new EventHandler(timer_Tick);

            this.LoadListEvent += new LoadSmoothList(TaskDetailsList_LoadListEvent);
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.timer.Enabled = false;
            base.OnClosing(e);
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            this.actionType = ActionTypeEnum.Assign;
            base.pictureBoxOk_Click(sender, e);
        }

        void TaskDetailsList_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            var getTask = this.GetTask;

            if (getTask != null)
            {
                var task = GetTask();
                this.PopulateTask(task);
            }

            this.timer.Enabled = true;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            var getTask = this.GetTask;

            if (getTask != null)
            {
                var task = GetTask();
                this.PopulateTask(task);
            }
        }

        private void PopulateTask(CargoMatrix.Communication.WSTaskManager.Task task)
        {
            if(this.detailsControl == null)
            {
                this.detailsControl = new TaskDetailsControl();
                this.AddItem(this.detailsControl);
            }

            this.detailsControl.PopulateTask(task);

            var userItems = this.smoothListBoxBase1.Items.OfType<SmoothListbox.ListItems.StandardListItem>().ToList();

            foreach (var item in userItems)
            {
                this.RemoveItem(item);
            }

            foreach (var user in task.AssignUsers)
            {
                var listItem = new SmoothListbox.ListItems.StandardListItem(user, CargoMatrix.Resources.Skin.worker);
                this.AddItem(listItem);
            }
                            
        }

        public enum ActionTypeEnum
        {
            NoAction,
            Assign
        }
    }
}
