﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.TaskManager
{
    public static class StartPoint
    {
        public static void Start(string taskID, string taskName)
        {
            TaskList taskList = new TaskList(taskID, taskName);
            taskList.ShowDialog();
        }
    }
}
