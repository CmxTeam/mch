﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Utilities;
using CustomListItems;
using CargoMatrix.UI;
using CMXExtensions;


namespace CargoMatrix.CargoTruckLoad
{
    public partial class DoorTruckSetUp : CargoUtilities.SmoothListBoxAsync2Options<ICargoTruckLoadDoor>
    {
        int listItemsCount = 0;
        bool isUserAdmin = false;
        CustomListItems.OptionsListITem finalizeMenu;

        IMasterBillItem masterBill;
        
        public DoorTruckSetUp(IMasterBillItem masterBill)
        {
            this.masterBill = masterBill;
            
            InitializeComponent();

            InitializeFields();

            this.ButtonFinalizeText = "Next";
            this.SetWindowTitle();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            this.finalizeMenu = new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.FINALIZE_LOAD_TRUCK);
            this.finalizeMenu.Enabled = false;

            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(DoorTruckSetUp_BarcodeReadNotify);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DoorTruckSetUp_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(DoorTruckSetUp_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(DoorTruckSetUp_MenuItemClicked);
            
            this.isUserAdmin = CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin;

            this.FinalizeButtonEnabled = false;
        }

        void DoorTruckSetUp_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;

                case OptionsListITem.OptionItemID.PRINT_TRUCK_LABEL:

                    var trucks = from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                 where item.ItemData.Truck != null
                                 group item by item.ItemData.Truck.LocationId into truckGroups
                                 select truckGroups.First().ItemData.Truck;

                    TruckLabels truckLabels = new TruckLabels(trucks.ToArray());
                    truckLabels.ShowDialog();

                    break;

                case OptionsListITem.OptionItemID.FINALIZE_LOAD_TRUCK:

                    Cursor.Current = Cursors.WaitCursor;
                    
                    bool result = CommonMethods.Finalize(this.masterBill);

                    Cursor.Current = Cursors.Default;

                    if(result == true)
                    {
                        CMXAnimationmanager.GoBack();
                    }

                    break;

                default: return;
            }
        }

        void DoorTruckSetUp_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.PRINT_TRUCK_LABEL));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.finalizeMenu);
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void DoorTruckSetUp_BarcodeReadNotify(string barcodeData)
        {
            this.BarcodeEnabled = true;

            var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            if (scanItem.BarcodeType != CMXBarcode.BarcodeTypes.Door)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongBarcodeType, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

            var listItem = (
                                from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                where string.Compare(item.ItemData.Name, scanItem.Location) == 0
                                select item
                           ).FirstOrDefault();

            if (listItem == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtDoorBillNotInList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            this.UpdateDoorTruck(listItem);

            this.Refresh();
            
        }

        private void InitializeFields()
        {
            this.panel1.SuspendLayout();

            var headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = masterBill.Reference();
            headerItem.Label2 = masterBill.Line2();
            headerItem.Label3 = "SCAN/SELECT DOOR";
            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);

            headerItem.Location = panel1.Location;
            panel1.Visible = true;
            this.panel1.Controls.Add(headerItem);
            headerItem.BringToFront();
            panel1.Height = headerItem.Height;
            panelHeader2.Height = headerItem.Height;

            var carrier = CargoMatrix.Communication.ScannerUtility.Instance.GetCarrierInfo(this.masterBill.CarrierNumber);
            headerItem.Logo  = CargoMatrix.Communication.Utilities.ConvertToImage(carrier.CarrierLogo);

            this.panel1.ResumeLayout();
        }

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;


            CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem door;
            bool result = CommonMethods.AddDoor(this.isUserAdmin, out door);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            if (result == false) return;

            var existingDoor = (
                                   from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                   where item.ItemData.Name.Equals(door.Door)
                                   select item
                               ).FirstOrDefault();

            if (existingDoor != null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtDoorAlreadyAssigned, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }


            CargoTruckLoadDoor newDoor = new CargoTruckLoadDoor(door);

            var status = CargoMatrix.Communication.LoadConsol.Instance.AssigneNewDoor(this.masterBill.MasterBillId, this.masterBill.TaskId, newDoor);

            if (status.TransactionStatus1 == true)
            {
                door.DoorId = status.TransactionId;
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            var listItem = this.InitializeItem(newDoor);

            this.smoothListBoxMainList.AddItem(listItem);

            this.listItemsCount++;

            this.SetWindowTitle();
        }

        protected override Control InitializeItem(ICargoTruckLoadDoor item)
        {
            DoorViewItem listItem = new DoorViewItem(item);
            listItem.OnBrowseClick += new EventHandler(listItem_OnBrowseClick);
            listItem.OnEditClick += new EventHandler(listItem_OnEditClick);
            listItem.OnEnterClick += new EventHandler(listItem_OnEnterClick);
            

            return listItem;
        }

        void DoorTruckSetUp_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var doorView = (DoorViewItem)listItem;

            if (this.isUserAdmin == false) return;

            this.UpdateDoorTruck(doorView);

            this.Refresh();
          
            this.LayoutItems();
        }

        void listItem_OnEnterClick(object sender, EventArgs e)
        {
            var listItem = (DoorViewItem)sender;

            this.UpdateDoorTruck(listItem);

            listItem.ClearEnter();

            this.Refresh();

            this.LayoutItems();
        }

        void listItem_OnEditClick(object sender, EventArgs e)
        {
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;
            
            var listItem = (DoorViewItem)sender;
            
            var cargoloadItem = listItem.ItemData;

            CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem newDoorItem;
            bool result = CommonMethods.EditDoor(this.isUserAdmin, cargoloadItem, out newDoorItem);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            if (result == false) return;

            var existingDoor = (
                               from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                               where item.ItemData.Name.Equals(newDoorItem.Door)
                               select item
                           ).FirstOrDefault();

            if (existingDoor != null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtDoorAlreadyAssigned, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            CargoTruckLoadDoor newDoor = new CargoTruckLoadDoor(newDoorItem);

            var status = CargoMatrix.Communication.LoadConsol.Instance.ReAssignDoor(this.masterBill.MasterBillId, cargoloadItem, this.masterBill.TaskId, newDoor);

            if (status.TransactionStatus1 == true)
            {
                newDoorItem.DoorId = status.TransactionId;
                newDoor.Truck = cargoloadItem.Truck;
                listItem.ItemData = newDoor;
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
        }

        void listItem_OnBrowseClick(object sender, EventArgs e)
        {
            var listItem = (DoorViewItem)sender;
            
            MessageListBox actPopup = new MessageListBox();
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = listItem.ItemData.Name;
            actPopup.HeaderText2 = string.Format("Select action for {0}", listItem.ItemData.Name);

            var viewTuckContentitem = new SmoothListbox.ListItems.StandardListItem("VIEW CONTENT", null, 1);
            viewTuckContentitem.Enabled = listItem.ItemData.Truck != null;
            actPopup.AddItem(viewTuckContentitem);

            var printTruckLabelsItem = new SmoothListbox.ListItems.StandardListItem("PRINT TRUCK LABEL", null, 2);
            printTruckLabelsItem.Enabled = listItem.ItemData.Truck != null;
            actPopup.AddItem(printTruckLabelsItem);

            var dialogResult = actPopup.ShowDialog();

            if (dialogResult != DialogResult.OK) return;

            switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:

                    Truck truckView = new Truck(this.masterBill, listItem.ItemData.Truck);
                    truckView.ShowDialog();

                    this.EnableFinalizeLoadTruck();
                    break;

                case 2:

                    CommonMethods.PrintTruckLabels(listItem.ItemData.Truck);

                    break;
            }

        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            this.BarcodeEnabled = false;
                      
            var doors = CargoMatrix.Communication.LoadConsol.Instance.GetMasterBillDoors(this.masterBill.MasterBillId);

            this.listItemsCount = doors.Count();

            this.SetFinalizeButtonEnabled(doors);

            this.SetWindowTitle();

            this.ReloadItemsAsync(doors);

            this.ShowContinueButton(true);

            this.EnableFinalizeTaskMenu();

            this.EnableFinalizeLoadTruck();

            this.BarcodeEnabled = true;

            Cursor.Current = Cursors.Default;
        }


        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            base.buttonContinue_Click(sender, e);

            var assignedDoors = from doorItem in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                where doorItem.ItemData.Truck != null
                                select doorItem.ItemData;


            Cursor.Current = Cursors.WaitCursor;

            CMXAnimationmanager.DisplayForm(new ULDList(this.masterBill, assignedDoors));

            Cursor.Current = Cursors.Default;
        }


        private void UpdateDoorTruck(DoorViewItem listItem)
        {
            this.barcode.StopRead();
            
            string scannedText;
            string locationName;


            var oldTruckName = listItem.ItemData.Truck == null ? null : listItem.ItemData.Truck.Location;

            bool result = CommonMethods.GetTruckFromUser(oldTruckName, string.Format("Assign truck for door: {0}", listItem.ItemData.Name), 
                                                         "Scan/enter truck",
                                                         out scannedText, out locationName);

            this.barcode.StartRead();
            
            if (result == false) return;

            CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem locationItem = new CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem();
            locationItem.Door = listItem.ItemData.Name;
            locationItem.DoorId = listItem.ItemData.ID;
            
            CargoTruckLoadDoor door = new CargoTruckLoadDoor(locationItem);

            CargoMatrix.Communication.WSLoadConsol.LocationItem truck = new CargoMatrix.Communication.WSLoadConsol.LocationItem();
            truck.LocationType = CargoMatrix.Communication.WSLoadConsol.LocationTypes.Truck;
            truck.LocationBarcode = scannedText;
            truck.Location = locationName;
         
            door.Truck = truck;

            var status = CargoMatrix.Communication.LoadConsol.Instance.AssigneDoorTruck(this.masterBill.MasterBillId, this.masterBill.TaskId, door);

            if(status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            truck.LocationId = status.TransactionId;

            listItem.ItemData = door;

            this.FinalizeButtonEnabled = true;
        }


        private void SetFinalizeButtonEnabled()
        {
            var doos = from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                       select item.ItemData;

            this.SetFinalizeButtonEnabled(doos);
        }

        private void SetFinalizeButtonEnabled(IEnumerable<ICargoTruckLoadDoor> doors)
        {
            var listItem = (
                                from item in doors
                                where item.Truck != null
                                select item
                           ).FirstOrDefault();

            this.FinalizeButtonEnabled = listItem != null;
        }


        private void SetWindowTitle()
        {
            this.TitleText = string.Format("CargoTruckLoad ({0})", listItemsCount);        
        }


        private void EnableFinalizeTaskMenu()
        {
            this.finalizeMenu.Enabled = false;        
        }


        private void EnableFinalizeLoadTruck()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            var status = CargoMatrix.Communication.LoadConsol.Instance.IsLoadTruckReady(this.masterBill.TaskId);

            finalizeMenu.Enabled = status.TransactionStatus1;

            Cursor.Current = Cursors.Default;
        }
    }
}
