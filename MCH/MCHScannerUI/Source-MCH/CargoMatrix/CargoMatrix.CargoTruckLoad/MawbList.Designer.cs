﻿namespace CargoMatrix.CargoTruckLoad
{
    partial class MawbList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.labelBlink = new CustomUtilities.CMXBlinkingLabel();
            this.panelHeader2.SuspendLayout();


            // 
            // labelBlink
            // 
            this.labelBlink.Location = new System.Drawing.Point(45, 3);
            this.labelBlink.Name = "labelBlink";
            this.labelBlink.ForeColor = System.Drawing.Color.Red;
            this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelBlink.Size = new System.Drawing.Size(155, 13);
            this.labelBlink.Text = "SCAN/SELECT MASTERBILL";
            this.labelBlink.Blink = true;
            
            //
            // searchbox
            //
            this.txtSearch = new CargoMatrix.UI.CMXTextBox();
            this.txtSearch.Location = new System.Drawing.Point(4, 19);
            this.txtSearch.Size = new System.Drawing.Size(230, 22);
            this.txtSearch.WatermarkText = "Enter Filter text";
            this.txtSearch.TextChanged += new System.EventHandler(searchBox_TextChanged);


            this.panelHeader2.Controls.Add(this.labelBlink);
            this.panelHeader2.Controls.Add(txtSearch);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            panelHeader2.Height = 47;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }

        #endregion


        private CustomUtilities.CMXBlinkingLabel labelBlink;
    }
}
