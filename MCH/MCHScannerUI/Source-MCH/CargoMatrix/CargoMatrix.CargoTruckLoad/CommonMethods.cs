﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using System.Windows.Forms;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using CargoMatrix.Communication.CargoTruckLoad;
using CustomListItems;
using System.Drawing;
using CMXExtensions;


namespace CargoMatrix.CargoTruckLoad
{
    public static class CommonMethods
    {
        public static int GetNumberOfPieces(IEnumerable<IULD> ulds)
        {
            return ulds.Sum(uld => uld.IsLoose() == true ? uld.Pieces : 1);
        }

        public static bool GetLocationFromUser(string title, string subTitle, CargoMatrix.Communication.ScannerUtilityWS.LocationTypes locationType, out string scannedText, out string locationName)
        {
            scannedText = null;
            locationName = null;

            ScanEnterPopup scanInfo = new ScanEnterPopup(locationType);
            scanInfo.Title = title;
            scanInfo.TextLabel = subTitle;
            DialogResult result = scanInfo.ShowDialog();

            if (result == DialogResult.OK)
            {
                scannedText = scanInfo.ScannedText;
                locationName = scanInfo.Text;
                return true;
            }

            return false;
        }

        public static bool GetTruckFromUser(string truckName, string title, string subTitle, out string scannedText, out string locationName)
        {
            scannedText = null;
            locationName = null;

            ScanEnterPopup_old scanInfo = new ScanEnterPopup_old(BarcodeType.Truck);
            scanInfo.Title = title;
            scanInfo.TextLabel = subTitle;

            if(string.IsNullOrEmpty(truckName) == false)
            {
                scanInfo.Text = truckName;
            }

            DialogResult result = scanInfo.ShowDialog();

            if (result == DialogResult.OK)
            {
                scannedText = scanInfo.ScannedText;
                locationName = scanInfo.ScannedItemMainPart;
                return true;
            }

            return false;
        }

        public static bool GetTruckFromUser(Func<CargoMatrix.Communication.ScannerUtilityWS.LocationItem[]> getTrucksMethod, out string scannedText, out string locationName)
        {
            scannedText = null;
            locationName = null;
            
            CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Truck);
            confirmLoc.Title = "Scan/select truck";
            confirmLoc.TextLabel = "TRUCK #";
            confirmLoc.GetLocationsMathod = getTrucksMethod;
            confirmLoc.ShouldBelongToList = true;
            confirmLoc.NotInTheListMessage = LoadTruckResources.TxtScannedBarcodeNotinList;

            var dResult = confirmLoc.ShowDialog();

            if (dResult == DialogResult.OK)
            {
                scannedText = confirmLoc.ScannedText;
                locationName = confirmLoc.Text;

                return true;
            }

            return false;
        }


        public static bool GetTruckFromUser(string truckName, out string scannedText, out string locationName)
        {
            return GetTruckFromUser(truckName, "Scan/Enter truck", "TRUCK # ", out scannedText, out locationName);
        }

        public static bool GetTruckFromUser(out string scannedText, out string locationName)
        {
            return GetTruckFromUser(null, "Scan/Enter truck", "TRUCK # ", out scannedText, out locationName);
        }

        public static int? GetPiecesCountFromUser(string header, string subHeder, int piecesCount, bool isReadonly)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = header;
            cntMsg.LabelReference = subHeder;
            cntMsg.LabelDescription = "Enter pieces count";
            cntMsg.PieceCount = piecesCount;
            cntMsg.Readonly = isReadonly;

            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }
        
        public static bool Finalize(IMasterBillItem masterBill)
        {
            Cursor.Current = Cursors.WaitCursor;

            TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.FinalizeLoadTruck(masterBill.TaskId);

            Cursor.Current = Cursors.Default;

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        private static CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter GetPrinter(string header, CargoMatrix.Communication.ScannerUtilityWS.LabelTypes labelType)
        {
            Cursor.Current = Cursors.WaitCursor;

            MessageListBox printersList = new MessageListBox();

            printersList.HeaderText = header;

            printersList.HeaderText2 = LoadTruckResources.TxtSelectPirnter;
            printersList.MultiSelectListEnabled = false;
            printersList.OneTouchSelection = false;

            StandardListItem<CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter> defaultPrinterItem = null;
            var printers = CargoMatrix.Communication.ScannerUtility.Instance.GetPrinters(labelType);

            foreach (var printer in printers)
            {
                Image image = null;

                switch (printer.PrinterType)
                {
                    case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel: // bluetooth
                        image = printer.IsDefault ? CargoMatrix.Resources.Icons.printerBtooth_check : CargoMatrix.Resources.Icons.printerBtooth;
                        break;

                    case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:

                        image = printer.IsDefault ? CargoMatrix.Resources.Icons.printer_wifi_check : CargoMatrix.Resources.Icons.printer_wifi;
                        break;

                    default:
                        image = printer.IsDefault ? CargoMatrix.Resources.Skin.Printer_Check : CargoMatrix.Resources.Skin.Printer;
                        break;
                }

                var printerListItem = new StandardListItem<CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter>(printer.PrinterName, image, printer.PrinterId, printer);
                printersList.AddItem(printerListItem);

                if (printer.IsDefault == true)
                {
                    defaultPrinterItem = printerListItem;
                }
            }

            if (defaultPrinterItem != null)
            {
                defaultPrinterItem.SelectedChanged(true);
                defaultPrinterItem.Focus(true);
                printersList.OkEnabled = true;
            }

            Cursor.Current = Cursors.Default;

            DialogResult dialogResult = printersList.ShowDialog();

            if (dialogResult != DialogResult.OK) return null;

            if (defaultPrinterItem == null &&
                printersList.SelectedItems.Count <= 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtPrinterNotselected, LoadTruckResources.TxtPrintError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return null;
            }

            var selectedPrinterItem = printersList.SelectedItems.Count > 0 ? (StandardListItem<CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter>)printersList.SelectedItems[0] : defaultPrinterItem;

            return selectedPrinterItem.ItemData;
        }

        public static void PrintMasterBillLabels(IMasterBillItem masterBill)
        {
            MawbLabelPrinter.Show(masterBill);
        }

        public static void PrintUldLabels(IULD uld)
        {
            var printer = CommonMethods.GetPrinter(string.Format("Print labels for {0}", uld.ULDNo), CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);

            if (printer == null) return;

            if (printer.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel ||
                printer.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless)
            {
                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.UldPrint(uld.ULDNo);

                Cursor.Current = Cursors.Default;

                Application.DoEvents();

                return;
            }

            
            Cursor.Current = Cursors.WaitCursor;

            bool result = CargoMatrix.Communication.ScannerUtility.Instance.PrintULDLabel(uld.ID, printer.PrinterId, 1);

            Cursor.Current = Cursors.Default;

            if (result == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtErrorPrintingUldLabels, LoadTruckResources.TxtPrintError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + printer.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
        }

        public static void PrintTruckLabels(CargoMatrix.Communication.WSLoadConsol.LocationItem truck)
        {
            var printer = CommonMethods.GetPrinter(string.Format("Print labels for {0}", truck.Location), CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.LOCATION);

            if (printer == null) return;

            if (printer.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel ||
                printer.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless)
            {
                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.TruckPrint(truck.Location);

                Cursor.Current = Cursors.Default;

                Application.DoEvents();

                return;
            }

            
            Cursor.Current = Cursors.WaitCursor;


            bool result = CargoMatrix.Communication.ScannerUtility.Instance.PrintTruckLabels(new int[]{truck.LocationId}, printer.PrinterId, 1);

            Cursor.Current = Cursors.Default;

            if (result == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtErrorPrintingTruckLabels, LoadTruckResources.TxtPrintError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + printer.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
        }

        public static void PrintTruckLabels(IEnumerable<string> truckNames, IEnumerable<int> truckIDs)
        {
            var printer = CommonMethods.GetPrinter("Print labels", CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.LOCATION);

            if (printer == null) return;

            if (printer.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel ||
                printer.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless)
            {
                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.TruckPrint(truckNames.ToArray());

                Cursor.Current = Cursors.Default;

                Application.DoEvents();

                return;
            }
            
            Cursor.Current = Cursors.WaitCursor;

            bool result = CargoMatrix.Communication.ScannerUtility.Instance.PrintTruckLabels(truckIDs.ToArray(), printer.PrinterId, 1);

            Cursor.Current = Cursors.Default;

            if (result == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtErrorPrintingTruckLabels, LoadTruckResources.TxtPrintError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + printer.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);

            }
        }

        public static bool AddDoor(bool isUserAdmin, out  CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem door)
        {
            door = null;
            DialogResult dialogResult;
         
            if (isUserAdmin == false)
            {
                dialogResult = CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor");

                if (dialogResult != DialogResult.OK) return false;
            }

            ScanEnterPopup scanInfo = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Door);
            scanInfo.Title = "Scan/select door";
            scanInfo.TextLabel = "DOOR #";

            dialogResult = scanInfo.ShowDialog();

            if (dialogResult != DialogResult.OK) return false;

            var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(scanInfo.ScannedText);

            if (scanItem.BarcodeType != CMXBarcode.BarcodeTypes.Door)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtInvalidDoorBarcode, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return false;
            }

            door = new CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem();
            door.Door = scanInfo.Text;

            return true;
        }

        public static bool EditDoor(bool isUserAdmin, ICargoTruckLoadDoor currentDoor, out CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem newDoor)
        {
            DialogResult dialogResult;

            newDoor = null;


            if (isUserAdmin == false)
            {
                dialogResult = CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor");

                if (dialogResult != DialogResult.OK) return false;
            }

            ScanEnterPopup scanInfo = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Door);
            scanInfo.Title = "Scan/Select door";
            scanInfo.TextLabel = "DOOR #";
            scanInfo.Text = currentDoor.Name;
            scanInfo.ScannedPrefix = "D";

            dialogResult = scanInfo.ShowDialog();

            if (dialogResult != DialogResult.OK) return false;

            var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(scanInfo.ScannedText);

            if (scanItem.BarcodeType != CMXBarcode.BarcodeTypes.Door)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtInvalidDoorBarcode, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return false;
            }

            if (string.Compare(scanInfo.Text, currentDoor.Name) == 0) return false;


            newDoor = new CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem();
            newDoor.Door = scanInfo.Text;

            return true;
        }
    
        public static CargoMatrix.Communication.ScannerUtilityWS.LocationItem[] GetUtilityTrucks(IEnumerable<ICargoTruckLoadDoor> doors)
        {
            var result = new List<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>();

            foreach (var item in doors)
            {
                if (item.Truck == null) continue;
                if (result.Exists(itm => itm.LocationId == item.Truck.LocationId) == true) continue;
                
                var truck = new CargoMatrix.Communication.ScannerUtilityWS.LocationItem();
                truck.LocationId = item.Truck.LocationId;
                truck.Location = item.Truck.Location;
                truck.LocationBarcode = item.Truck.LocationBarcode;
                truck.LocationType = CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Truck;

                result.Add(truck);
            }

            return result.ToArray();
        }
    
        public static Func<T> Func<T>(Func<T> f)
        {
            return f;
        }

        public static CargoMatrix.Communication.ScannerUtilityWS.LocationItem GetTruckByName(string truckName, Func<CargoMatrix.Communication.ScannerUtilityWS.LocationItem[]> getTrucks)
        {
            return (from item in getTrucks()
                    where string.Compare(item.Location, truckName, StringComparison.OrdinalIgnoreCase) == 0
                    select item).FirstOrDefault();
        }
   
    }
}
