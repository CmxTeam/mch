﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox;
using System.Drawing;

namespace CustomListItems
{
    public class StaticListItem : SmoothListbox.ListItems.StandardListItem, ISmartListItem
    {
        public StaticListItem(string title, Image icon)
            : base(title, icon)
        {

        }

        bool ISmartListItem.Contains(string text)
        {
            return true;
        }

    }
}
