﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomListItems
{
    public class ExpenListItemData : IExpandItemData
    {
        private int id;
        public ExpenListItemData(int ID)
        {
            this.id = ID;
            this.IsSelectable = true;
        }

        public ExpenListItemData(int ID, string title, string description)
            : this(ID)
        {
            this.TitleLine = title;
            this.DescriptionLine = description;
        }
        
        #region IExpandItemData Members

        public int ID
        {
            get { return id; }
        }

        public string TitleLine
        {
            get;
            set;
        }

        public string DescriptionLine
        {
            get;
            set;
        }

        public bool IsSelected
        {
            get;
            set;
        }

        public bool IsSelectable
        {
            get;
            set;
        }

        public bool IsReadonly
        {
            get;
            set;
        }

        public bool IsChecked
        {
            get;
            set;
        }

        public bool IsLast
        {
            get;
            set;
        }

        public bool Expandable
        {
            get;
            set;
        }

        public IExpandItemData ParentItemData
        {
            get;
            set;

        }

        [Obsolete("Not a control")]
        public void RefreshView()
        {

        }

        #endregion
    }
}
