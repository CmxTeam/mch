﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmoothListbox;
using SmoothListbox.ListItems;

namespace CustomListItems.RenderItems
{
    public class ListItemWithButtonWide<T> : ListItemWithButton<T>
    {
        public ListItemWithButtonWide(string title, string line2, Image picture, T data) :
            base(title, line2, picture, CargoMatrix.Resources.Skin.printerButton, CargoMatrix.Resources.Skin.printerButton_over, data)
        {

        }
        public ListItemWithButtonWide(string title, string line2, Image picture, Image button, Image pressedButton, T data)
            : base(title, line2, picture, button, pressedButton, data)
        { }
        protected override void InitializeControls()
        {
            this.button = new CargoMatrix.UI.CMXPictureButton();
            this.button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button.Location = new System.Drawing.Point(202, 3);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(32, 32);
            this.button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.button.TransparentColor = System.Drawing.Color.White;
            button.Image = buttonImage;
            button.PressedImage = butonPressedImage;
            this.button.Click += new EventHandler(button_Click);
            this.Controls.Add(this.button);
            this.button.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
        }

    }
}
