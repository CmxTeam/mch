﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmoothListbox;
using SmoothListbox.ListItems;

namespace CustomListItems.RenderItems
{
    public class ListItemWithButton<T> : CustomListItems.RenderItemBase<T>, ISmartListItem
    {
        protected string line1, line2;
        protected Image logo, buttonImage, butonPressedImage;
        protected CargoMatrix.UI.CMXPictureButton button;

        public event EventHandler OnButtonClick;


        public ListItemWithButton(string title, string line2, Image picture, T data) :
            this(title, line2, picture, CargoMatrix.Resources.Skin.printerButton, CargoMatrix.Resources.Skin.printerButton_over, data)
        {

        }
        public ListItemWithButton(string title, string line2, Image picture, Image button, Image pressedButton, T data)
            : base(data)
        {
            this.line1 = title;
            this.line2 = line2;
            logo = picture ?? Resources.Graphics.Skin.radio_button;
            buttonImage = button;
            butonPressedImage = pressedButton;

            this.SuspendLayout();
            this.BackColor = System.Drawing.Color.White;
            this.Name = "ListItemWithButton";
            this.Size = new System.Drawing.Size(240, 40);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);
        }
        #region ISmartListItem Members

        bool ISmartListItem.Contains(string text)
        {
            return line1.ToLower().Contains(text.ToLower());
        }

        bool ISmartListItem.Filter
        {
            get;
            set;
        }

        #endregion

        protected override void InitializeControls()
        {
            this.button = new CargoMatrix.UI.CMXPictureButton();
            this.button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button.Location = new System.Drawing.Point(196, 3);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(32, 32);
            this.button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.button.TransparentColor = System.Drawing.Color.White;
            button.Image = buttonImage;
            button.PressedImage = butonPressedImage;
            this.button.Click += new EventHandler(button_Click);
            this.Controls.Add(this.button);
            this.button.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
        }

        protected void button_Click(object sender, EventArgs e)
        {
            if (OnButtonClick != null)
                OnButtonClick(this, e);
        }
        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 20 * vScale, 170 * hScale, 13 * vScale));
                }
                int logoY = (int)((this.Height - (32 * vScale)) / 2);
                if (logo != null)
                    gOffScreen.DrawImage(logo, new Rectangle((int)(4 * hScale), logoY, (int)(32 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, logo.Width, logo.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
                }
            }
        }
    }
}
