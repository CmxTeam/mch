﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class HousebillViewer : BillViewerShell //UserControl
    {
        string m_housebill, m_carrier;
        CargoMatrix.Communication.DTO.ShellData m_houseBillData;
        public HousebillViewer(string housebill)
        {
            InitializeComponent();

            m_housebill = housebill;
        }

        public HousebillViewer(string carrier, string housebill)
        {
            InitializeComponent();

            m_housebill = housebill;
            m_carrier = carrier;
        }
        public override void LoadControl()
        {
            //Application.DoEvents();
            Cursor.Current = Cursors.WaitCursor;
            base.LoadControl();
            panelSoftkeys.Width = Width;
            panelSoftkeys.Top = Height - panelSoftkeys.Height - 1;
            //this.panelSoftkeys.BringToFront();
            panelSoftkeys.Refresh();
            //this.smoothListBoxMainList.progressBar1.BringToFront();
            this.smoothListBoxMainList.progressBar1.Minimum = 0;
            this.smoothListBoxMainList.progressBar1.Maximum = 100;
            this.smoothListBoxMainList.progressBar1.Visible = true;
            this.smoothListBoxMainList.progressBar1.Value = 10;
            this.smoothListBoxMainList.progressBar1.Refresh();



            if (m_housebill != null)
            {
                if (m_carrier == null)
                    m_houseBillData = CargoMatrix.Communication.WebServiceManager.Instance().GetHousebillDetails(null, m_housebill);
                else
                    m_houseBillData = CargoMatrix.Communication.WebServiceManager.Instance().GetHousebillDetails(m_carrier, m_housebill);
                this.smoothListBoxMainList.progressBar1.Value = 45;
                this.smoothListBoxMainList.progressBar1.Refresh();
            }
            else
                smoothListBoxMainList.labelEmpty.Visible = true;

            if (m_houseBillData != null)
            {
                this.TitleText = m_houseBillData.Name;

                for (int i = 0; i < m_houseBillData.m_pages.Count; i++)
                {

                    InsertPage(new SmoothListbox.ListItems.CheckBox(m_houseBillData.m_pages[i].Name), m_houseBillData.m_pages[i], m_houseBillData.Reference);

                }

                OpenPage(0);


            }
            else
                smoothListBoxMainList.labelEmpty.Visible = true;
            //this.smoothListBoxMainList.progressBar1.Value = 75;
            //this.smoothListBoxMainList.progressBar1.Refresh();



            this.smoothListBoxMainList.progressBar1.Value = 100;
            this.smoothListBoxMainList.progressBar1.Invalidate();
            this.smoothListBoxMainList.progressBar1.Visible = false;
            Cursor.Current = Cursors.Default;
        }

        private void HousebillViewer_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU));
            if (m_housebill != null)
            {
                if (m_carrier == null)
                {
                    //this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_HAWB_LABEL));
                }
                else
                {
                    this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_MAWB_LABEL));

                }

            }
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem("About", ""));
        }

        private void HousebillViewer_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {

                    case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
                        if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                            CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU:

                        if (CargoMatrix.UI.CMXAnimationmanager.CanGoBack())
                        {
                            this.Enabled = false;
                            CargoMatrix.UI.CMXAnimationmanager.GoBack();
                        }
                        else
                        {
                            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to exit Housebill viewer?",
                               "Exit Viewer", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                               MessageBoxButtons.OKCancel, DialogResult.Cancel))
                            {
                                CargoMatrix.UI.CMXAnimationmanager.GoBack();
                            }
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            this.Refresh();
                            this.Reset();
                            LoadControl();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.PRINT_HAWB_LABEL:
                        CustomUtilities.MawbLabelPrinter.Show(this.m_housebill, this.m_carrier);
                        break;
                }
            }
        }
        private void HousebillViewer_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled == false)
            {
                this.Reset();
                labelTitle.Visible = false;
                this.Refresh();
            }
        }

    }
}
