﻿namespace CargoMatrix.Viewer
{
    partial class ShellImageItemA
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (m_thread != null)
                m_thread.Abort();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel = new System.Windows.Forms.Panel();
            this.labelDescription = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxIcon = new OpenNETCF.Windows.Forms.PictureBox2();
            this.panelBaseLine = new System.Windows.Forms.Panel();
            this.Panel.SuspendLayout();
            this.panel1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.Controls.Add(this.labelDescription);
            this.Panel.Controls.Add(this.panel1);
            this.Panel.Location = new System.Drawing.Point(3, 3);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(234, 28);
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDescription.BackColor = System.Drawing.Color.White;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescription.Location = new System.Drawing.Point(24, 1);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(209, 26);
            this.labelDescription.Text = "<desc>";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pictureBoxIcon);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(24, 26);
            // 
            // pictureBoxIcon
            // 
            this.pictureBoxIcon.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxIcon.BackColor = System.Drawing.Color.White;
            this.pictureBoxIcon.Location = new System.Drawing.Point(4, 5);
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new System.Drawing.Size(17, 16);
            this.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxIcon.TransparentColor = System.Drawing.Color.White;
            // 
            // panelBaseLine
            // 
            this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;
            this.panelBaseLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBaseLine.Location = new System.Drawing.Point(0, 33);
            this.panelBaseLine.Name = "panelBaseLine";
            this.panelBaseLine.Size = new System.Drawing.Size(240, 1);
            // 
            // ShellImageItemA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelBaseLine);
            this.Controls.Add(this.Panel);
            this.Name = "ShellImageItemA";
            this.Size = new System.Drawing.Size(240, 34);
            this.Panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel Panel;
        public System.Windows.Forms.Label labelDescription;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxIcon;
        public System.Windows.Forms.Panel panel1;
        protected System.Windows.Forms.Panel panelBaseLine;
    }
}
