﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CargoMatrix.Communication.CargoMeasurements;
using CargoMatrix.Utilities;
using CargoMatrix.ExceptionManager;
using CargoMatrix.Communication.CargoMeasurements.Communication;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.ScannerUtilityWS;
using CustomUtilities;

namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    public partial class ManualMesurementsForm : Form
    {
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXPictureButton buttonOk;

        private CargoMatrix.UI.CMXPictureButton btnDamgeCapture;
        private CargoMatrix.UI.CMXPictureButton btnSelectBoxType;
        private CargoMatrix.UI.CMXPictureButton btnSelectDevice;
        private CargoMatrix.UI.CMXPictureButton btnAquire;
        private CargoMatrix.UI.CMXPictureButton btnForth;
        private CargoMatrix.UI.CMXPictureButton btnBack;
        private CargoMatrix.UI.CMXPictureButton btnEditPackageReference;

        ICommunicationProtocolDriver protocolDriver;
        
        string header;
        int currentMeasurenentNumber;
        ActionType actionType;
        MetricSystemTypeEnum metricSystemType;
      
        CargoMatrix.UI.BarcodeReader barcode;
        DimensioningMeasurementDetails[] measurements;
        List<MettlerMeasurements> compareMeasurements;
        PackageType[] packageTypes;
        PackageType currentPackageType;

       
        #region Properties
        public DimensioningMeasurementDetails[] Measurements
        {
            get { return this.measurements; }
        }

        public List<MettlerMeasurements> CompareaMeasurementsList
        {
            get { return compareMeasurements; }
            set { compareMeasurements = value; }
        }
        #endregion

        #region Constructors
        public ManualMesurementsForm(MetricSystemTypeEnum metricSystemType, ActionType actionType, DimensioningMeasurementDetails[] measurements)
        {
            InitializeComponent();

            this.currentMeasurenentNumber = 0;
            this.metricSystemType = metricSystemType;
            this.actionType = actionType;
            this.measurements = measurements;

            this.barcode = new CargoMatrix.UI.BarcodeReader();
            this.barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcode_BarcodeReadNotify);

            this.EnebleCancelButton();

            this.ClearFields();
            
            this.Load += new EventHandler(ManualMesurementsForm_Load);
            this.Closing += new CancelEventHandler(ManualMesurementsForm_Closing);
        }

        public ManualMesurementsForm(MetricSystemTypeEnum metricSystemType, ActionType actionType, DimensioningMeasurementDetails[] measurements, int currentIndex)
            : this(metricSystemType, ActionType.Edit, measurements)
        {
            this.currentMeasurenentNumber = currentIndex;
        }

        public ManualMesurementsForm(MetricSystemTypeEnum metricSystemType, DimensioningMeasurementDetails measurement)
            : this(metricSystemType, ActionType.Edit, new DimensioningMeasurementDetails[] { measurement })
        { } 
        #endregion

        #region Events
        void ManualMesurementsForm_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            this.packageTypes = Communication.ScannerUtility.Instance.GetPackageTypeList();

            if (packageTypes.Length > 0)
            {
                this.SetCurrentPackageType(packageTypes[0]);
            }
            
            this.SetHeaderText();
            
            this.DisplayMetricSystem(this.metricSystemType);

            this.DisplayMesurement(this.currentMeasurenentNumber);
            
            if (CommonMethods.CurrentDevice == null)
            {
                CommonMethods.CurrentDevice = CargoMeasurementsService.Instance.GetDefaultDimensioningDevice();
            }
           
            this.UpdateDeviceInformation();

            this.AttachDriver(CommonMethods.CurrentDevice);

            this.btnAquire.Enabled = false;

            if (CommonMethods.CurrentDevice != null)
            {
                this.SetInfoStatusMessage("Checking connection");
            }
           
            this.DriverPerformStep();

            Cursor.Current = Cursors.Default;

            this.barcode.StartRead();
        }

        void ManualMesurementsForm_Closing(object sender, CancelEventArgs e)
        {
            this.barcode.StopRead();
        }

        void barcode_BarcodeReadNotify(string barcodeData)
        {
            this.ParsePackegeReference(barcodeData, this.currentPackageType);

            this.barcode.StartRead();
        }

        void buttonOk_Click(object sender, EventArgs e)
        {
            string errorMessage;
            bool validateResult = this.ValidateInput(out errorMessage);

            if (validateResult == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(errorMessage, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (this.SaveMeasurement(this.currentMeasurenentNumber) == false) return;

            this.SetHeaderText();

            //this.EnebleCancelButton();
            this.DialogResult = DialogResult.OK;

        }

        void buttonCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        void btnForth_Click(object sender, EventArgs e)
        {
            this.MoveToNextMeasurement();
        }

        void btnBack_Click(object sender, EventArgs e)
        {
            this.MoveToPreviousMeasurement();
        }

        void btnSelectDevice_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DevicesPopup devicePopup = new DevicesPopup();

            if (devicePopup.ShowDialog() != DialogResult.OK) return;

            CommonMethods.CurrentDevice = devicePopup.CurrentDevice;

            this.UpdateDeviceInformation("Changing device");

            this.Refresh();

            this.ShutDownDriver();

            this.AttachDriver(CommonMethods.CurrentDevice);
           
            this.btnAquire.Enabled = false;

            this.DriverPerformStep();
        }

        void btnDamgeCapture_Click(object sender, EventArgs e)
        {
            if (CargoMatrix.Communication.Utilities.CameraPresent == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Camera is not present on the device", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                return;
            }

            if (string.IsNullOrEmpty(this.txtPkgRef.Text.Trim()) == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Enter package ID", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                return;
            }

            this.barcode.StopRead();

            this.ShutDownDriver();
           
            Cursor.Current = Cursors.WaitCursor;

            CargoMatrix.FreightPhotoCapture.PhotoCapturePopup photoCapturePopup = new CargoMatrix.FreightPhotoCapture.PhotoCapturePopup();
            photoCapturePopup.ShowDialog();

            this.barcode.StartRead();

            this.SetInfoStatusMessage("Checking connection");

            this.btnAquire.Enabled = false;

            this.AttachDriver(CommonMethods.CurrentDevice);

            this.DriverPerformStep();
        }

        void btnAquire_Click(object sender, EventArgs e)
        {
            this.btnAquire.Enabled = false;
            this.btnDamgeCapture.Enabled = false;
            
            this.DriverPerformStep();
        }

        void btnSelectBoxType_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            MessageListBox listBox = new MessageListBox();
            listBox.MultiSelectListEnabled = false;
            listBox.OneTouchSelection = true;
            listBox.OkEnabled = false;
            listBox.HeaderText = "Select package type";
            listBox.HeaderText2 = string.Empty;

            foreach (var packageType in packageTypes)
            {
                var listItem = new StandardListItem<PackageType>(packageType.name, CargoMatrix.Resources.Skin.Shipping_Crate1, packageType);

                listBox.AddItem(listItem);
            }

            Cursor.Current = Cursors.Default;

            if (listBox.ShowDialog() != DialogResult.OK) return;

            this.SetCurrentPackageType(((StandardListItem<PackageType>)listBox.SelectedItems[0]).ItemData);
        }

        void btnEditPackageReference_Click(object sender, EventArgs e)
        {
            this.barcode.StopRead();
            
            ScanEnterPopup_old popUp = new ScanEnterPopup_old(CustomUtilities.BarcodeType.All);
            popUp.Title = "Scan/Enter barcode";
            popUp.TextLabel = "Barcode";
            var result = popUp.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (CommonMethods.MeasurementExists(popUp.ScannedText, measurements) == true)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Barcode already scanned", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    this.barcode.StartRead();
                    return;
                }

                this.ParsePackegeReference(popUp.ScannedText, this.currentPackageType);
            }

            this.barcode.StartRead();
        }
        
        void CommunicationClient_OnError(object sender, CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs e)
        {
            Action action = () =>
                {
                    Logger.Log(e.Exception, 70000001);

                    this.SetErrorStatusMessage("Checking connection");
                   
                    this.btnAquire.Enabled = false;
                    this.btnDamgeCapture.Enabled = true;
                };

            this.SafeInvoke(action);
        }

        void CommunicationClient_OnResponseReceived(object sender, CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ResponseReceivedEventArgs e)
        {
            Action action = () =>
                {
                    string message = CommonMethods.CommunicatorCommandAsString(e.Command);

                    switch (e.Command.CommnadType)
                    {
                        case CommandTypesEnum.ALIVE:

                            this.btnAquire.Enabled = true;
                            this.SetInfoStatusMessage(message);

                            break;

                        case CommandTypesEnum.BUSY:
                        case CommandTypesEnum.UNAVAILABLE:

                            this.SetErrorStatusMessage(message);

                            break;

                        case CommandTypesEnum.DATA:

                            DimensioningMeasurementDetails details = this.measurements[this.currentMeasurenentNumber];
                            if (details == null)
                            {
                                details = new DimensioningMeasurementDetails();
                            }

                            details.Measurement = e.Command.DataAsMeasurements;
                            this.measurements[this.currentMeasurenentNumber] = details;


                            this.DisplayMesurement(details, false);

                            this.SetInfoStatusMessage(message);

                            break;
                    }

                    this.btnAquire.Enabled = true;
                    this.btnDamgeCapture.Enabled = true;
                };

            this.SafeInvoke(action);
        }

        void CommunicationClient_OnMettlerIPSent(object sender, EventArgs e)
        {
            Action action = () =>
                {
                    this.SetInfoStatusMessage("Waiting");
                };

            this.SafeInvoke(action);
        }

        void CommunicationClient_OnConnected(object sender, EventArgs e)
        {
            Action action = () =>
                {
                    this.SetInfoStatusMessage("Checking connection");

                    this.DriverPerformStep();
                };

            this.SafeInvoke(action);
        }

        void pcxHeader_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            using (Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.header,
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }

        #endregion

        #region Overrides
        protected override void OnClosing(CancelEventArgs e)
        {
            this.barcode.StopRead();

            this.ShutDownDriver();

            base.OnClosing(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(pnlMain.Left - 1, pnlMain.Top - 1, pnlMain.Width + 1, pnlMain.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }

        }
        
        #endregion

        #region Additional Methods
        private void InitializeFields()
        {
            this.pcxFooter.Image = global::Resources.Graphics.Skin.nav_bg;
            this.pcxHeader.Image = global::Resources.Graphics.Skin.popup_header;

            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();


            this.btnAquire = new CargoMatrix.UI.CMXPictureButton();
            this.btnForth = new CargoMatrix.UI.CMXPictureButton();
            this.btnBack = new CargoMatrix.UI.CMXPictureButton();
            this.btnSelectDevice = new CargoMatrix.UI.CMXPictureButton();
            this.btnDamgeCapture = new CargoMatrix.UI.CMXPictureButton();
            this.btnSelectBoxType = new CargoMatrix.UI.CMXPictureButton();
            this.btnEditPackageReference = new CargoMatrix.UI.CMXPictureButton();


            this.txtWidth.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtLength.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtHeight.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtWeight.InputMode = UI.CMXTextBoxInputMode.Decimal;


            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(123, 245);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.BackColor = Color.White;
            this.buttonCancel.Image = global::Resources.Graphics.Skin.nav_cancel;  //CargoMatrix.Resources.Skin.button_default;
            this.buttonCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over; //CargoMatrix.Resources.Skin.button_pressed;//   ;
            this.buttonCancel.DisabledImage = global::Resources.Graphics.Skin.nav_cancel_dis;
            this.buttonCancel.Click += new System.EventHandler(buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(64, 245);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.BackColor = Color.White;
            this.buttonOk.Image = global::Resources.Graphics.Skin.nav_ok; //CargoMatrix.Resources.Skin.button_default; 
            this.buttonOk.PressedImage = global::Resources.Graphics.Skin.nav_ok_over; //CargoMatrix.Resources.Skin.button_pressed;
            this.buttonOk.DisabledImage = global::Resources.Graphics.Skin.nav_ok_dis;
            this.buttonOk.Click += new EventHandler(buttonOk_Click);



            // 
            // btnAquire
            // 
            this.btnAquire.Location = new System.Drawing.Point(104, 161);
            this.btnAquire.Name = "btnAquire";
            this.btnAquire.Size = new System.Drawing.Size(125, 32);
            this.btnAquire.BackColor = Color.White;
            this.btnAquire.Image = CargoMatrix.Resources.Skin.button_acquire;
            this.btnAquire.PressedImage = CargoMatrix.Resources.Skin.button_acquire_over;
            this.btnAquire.DisabledImage = CargoMatrix.Resources.Skin.button_acquire_dis;
            this.btnAquire.Click += new EventHandler(btnAquire_Click);


            // 
            // btnForth
            // 
            this.btnForth.Location = new System.Drawing.Point(182, 245);
            this.btnForth.Name = "btnForth";
            this.btnForth.Size = new System.Drawing.Size(52, 37);
            this.btnForth.BackColor = Color.White;
            this.btnForth.Image = global::Resources.Graphics.Skin.gen_btn_right;
            this.btnForth.PressedImage = global::Resources.Graphics.Skin.gen_btn_right_over;
            this.btnForth.DisabledImage = global::Resources.Graphics.Skin.gen_btn_right_over;
            this.btnForth.SizeMode = PictureBoxSizeMode.StretchImage;
            
            this.btnForth.Click += new EventHandler(btnForth_Click);
            

            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(5, 245);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(52, 37);
            this.btnBack.BackColor = Color.White;
            this.btnBack.Image = global::Resources.Graphics.Skin.gen_btn_left;
            this.btnBack.PressedImage = global::Resources.Graphics.Skin.gen_btn_left_over;
            this.btnForth.SizeMode = PictureBoxSizeMode.StretchImage;
            this.btnBack.Click += new EventHandler(btnBack_Click);

            // 
            // btnSelectDevice
            // 
            this.btnSelectDevice.Location = new System.Drawing.Point(193, 200);
            this.btnSelectDevice.Name = "btnSelectDevice";
            this.btnSelectDevice.Size = new System.Drawing.Size(36, 36);
            this.btnSelectDevice.BackColor = Color.White;
            this.btnSelectDevice.Image = CargoMatrix.Resources.Skin.button_scale;
            this.btnSelectDevice.PressedImage = CargoMatrix.Resources.Skin.button_scale_over;
            this.btnSelectDevice.Click += new EventHandler(btnSelectDevice_Click);

            // 
            // btnDamgeCapture
            // 
            this.btnDamgeCapture.Location = new System.Drawing.Point(193, 77);
            this.btnDamgeCapture.Name = "btnDamgeCapture";
            this.btnDamgeCapture.Size = new System.Drawing.Size(36, 36);
            this.btnDamgeCapture.BackColor = Color.White;
            this.btnDamgeCapture.Image = CargoMatrix.Resources.Skin.damage;
            this.btnDamgeCapture.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.btnDamgeCapture.DisabledImage = CargoMatrix.Resources.Skin.damage_dis;
            this.btnDamgeCapture.Click += new EventHandler(btnDamgeCapture_Click);


            // 
            // btnSelectBoxType
            // 
            this.btnSelectBoxType.Location = new System.Drawing.Point(150, 77);
            this.btnSelectBoxType.Name = "btnPrint";
            this.btnSelectBoxType.Size = new System.Drawing.Size(36, 36);
            this.btnSelectBoxType.BackColor = Color.White;
            this.btnSelectBoxType.Image = CargoMatrix.Resources.Skin.shipping_box_help;
            this.btnSelectBoxType.PressedImage = CargoMatrix.Resources.Skin.shipping_box_help_over;
            this.btnSelectBoxType.Click += new EventHandler(btnSelectBoxType_Click);

            //
            // btnEditPackageReference
            // 
            this.btnEditPackageReference.Location = new System.Drawing.Point(193, 35);
            this.btnEditPackageReference.Size = new System.Drawing.Size(36, 36);
            this.btnEditPackageReference.BackColor = System.Drawing.Color.White;
            this.btnEditPackageReference.Image = CargoMatrix.Resources.Skin.edit_btn;
            this.btnEditPackageReference.PressedImage = CargoMatrix.Resources.Skin.edit_btn_over;
            this.btnEditPackageReference.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditPackageReference.Click += new EventHandler(btnEditPackageReference_Click);
            


            this.pnlMain.Controls.Add(this.buttonCancel);
            this.pnlMain.Controls.Add(this.buttonOk);

            this.pnlMain.Controls.Add(this.btnAquire);
            this.pnlMain.Controls.Add(this.btnForth);
            this.pnlMain.Controls.Add(this.btnBack);
            this.pnlMain.Controls.Add(this.btnSelectDevice);
            this.pnlMain.Controls.Add(this.btnDamgeCapture);
            this.pnlMain.Controls.Add(this.btnSelectBoxType);
            this.pnlMain.Controls.Add(this.btnEditPackageReference);

            this.buttonCancel.BringToFront();
            this.buttonOk.BringToFront();
            this.btnBack.BringToFront();
            this.btnForth.BringToFront();
        }

        private bool ParsePackegeReference(string data, PackageType packageType)
        {
            if (CommonMethods.MeasurementExists(data, this.measurements) == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Package already scanned", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return false;
            }
            
            DimensioningMeasurementDetails details;

            bool result = CommonMethods.ParsePackegeReference(data, out details);

            if (result == true)
            {
                if (CommonMethods.UndentifiedMeasurementExists(details, this.measurements) == true)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Package already scanned", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return false;
                }
                
                this.txtPkgRef.Text = details.PackageNumber;

                this.SetPackageRefText(details);

                MettlerMeasurements measurement = null;
                DimensioningMeasurementDetails currentDetails = measurements[this.currentMeasurenentNumber];

                if (currentDetails != null)
                {
                    measurement = currentDetails.Measurement;
                }

                measurements[this.currentMeasurenentNumber] = details;
                details.Measurement = measurement;
                details.PackageType = packageType;

                this.btnDamgeCapture.Enabled = details.BarcodeType == CMXBarcode.BarcodeTypes.HouseBill;
            }

            return result;
        }

        private bool ValidateInput(out string errorMessage)
        {
            errorMessage = null;

            string packageId = this.txtPkgRef.Text.Trim();

            if (string.IsNullOrEmpty(packageId) == true)
            {
                errorMessage = "Package ID cannot be empty";
                return false;
            }

            if (this.currentPackageType == null)
            {
                errorMessage = "Package type cannot be empty";
                return false;
            }

            try
            {
                double val = double.Parse(this.txtWeight.Text);

                if (val <= 0.0d)
                {
                    errorMessage = "Weight must be greater then zero";
                    return false;
                }
            }
            catch
            {
                errorMessage = "Weight must be a number greater then zero";
                return false;
            }

            try
            {
                double val = double.Parse(this.txtLength.Text);

                if (val <= 0.0d)
                {
                    errorMessage = "Length must be greater then zero";
                    return false;
                }
            }
            catch
            {
                errorMessage = "Length must be a number greater then zero";
                return false;
            }

            try
            {
                double val = double.Parse(this.txtWidth.Text);

                if (val <= 0.0d)
                {
                    errorMessage = "Width must be greater then zero";
                    return false;
                }
            }
            catch
            {
                errorMessage = "Width must be a number greater then zero";
                return false;
            }

            try
            {
                double val = double.Parse(this.txtHeight.Text);

                if (val <= 0.0d)
                {
                    errorMessage = "Height must be greater then zero";
                    return false;
                }
            }
            catch
            {
                errorMessage = "Height must be a number greater then zero";
                return false;
            }

            return true;
        }

        private void SetHeaderText()
        {
            this.header = string.Format("{0} measurements {1} of {2}", this.actionType, this.currentMeasurenentNumber + 1, this.measurements.Length);
            this.pcxHeader.Refresh();
        }

        private void SetPackageRefText(DimensioningMeasurementDetails details)
        {
            this.label1.Text = CommonMethods.GetHeaderText(details);

            this.label1.Refresh();
        } 

        private bool SaveMeasurement(int arrayIndex)
        {
            var measurementDetails = this.measurements[arrayIndex];

            if (measurementDetails == null)
            {
                if(this.ParsePackegeReference(this.txtPkgRef.Text.Trim(), this.currentPackageType) == false) return false;
            }

            measurementDetails.PackageType = this.currentPackageType;

            if (measurementDetails.Measurement == null)
            {
                measurementDetails.Measurement = MettlerMeasurements.CreateFromSystemType(this.metricSystemType, MeasurementsOriginEnum.Manual);
                measurementDetails.Measurement.Origin = MeasurementsOriginEnum.Manual;
            }
            
            measurementDetails.Measurement.Width = double.Parse(this.txtWidth.Text);
            measurementDetails.Measurement.Length = double.Parse(this.txtLength.Text);
            measurementDetails.Measurement.Weight = double.Parse(this.txtWeight.Text);
            measurementDetails.Measurement.Height = double.Parse(this.txtHeight.Text);
            

            return true;
         
        }

        private void DisplayMesurement(DimensioningMeasurementDetails measurement, bool overridePackageID)
        {
            if (overridePackageID == true)
            {
                this.txtPkgRef.Text = measurement.PackageNumber;
            }

            this.SetPackageRefText(measurement);

            if (measurement.PackageType != null)
            {
                this.lblPkgType.Text = measurement.PackageType.name;
            }
            else
            {
                this.lblPkgType.Text = this.currentPackageType == null ? string.Empty : this.currentPackageType.name;
            }

            if (measurement.Measurement != null)
            {
                if (measurement.Measurement.Width != 0.0)
                {
                    this.txtWidth.Text = string.Format("{0:N2}", measurement.Measurement.Width);
                }
                else
                {
                    this.txtWidth.Text = string.Empty;
                }

                if (measurement.Measurement.Length != 0.0)
                {
                    this.txtLength.Text = string.Format("{0:N2}", measurement.Measurement.Length);
                }
                else
                {
                    this.txtLength.Text = string.Empty;
                }

                if (measurement.Measurement.Height != 0.0)
                {
                    this.txtHeight.Text = string.Format("{0:N2}", measurement.Measurement.Height);
                }
                else
                {
                    this.txtHeight.Text = string.Empty;
                }

                if (measurement.Measurement.Weight != 0.0)
                {
                    this.txtWeight.Text = string.Format("{0:N2}", measurement.Measurement.Weight);
                }
                else
                {
                    this.txtWeight.Text = string.Empty;
                }
                
                string mesuresWeightSign = measurement.Measurement.WeightMesurementsUnit.ToString().ToLower();
                string mesuresLengthSign = measurement.Measurement.LengthMesurementsUnit.ToString().ToLower();

                this.lblWeightSystem.Text = mesuresWeightSign;
                this.lblWidthSystem.Text = mesuresLengthSign;
            }
            else
            {
                this.DisplayMetricSystem(this.metricSystemType);
            }
        }

        private void DisplayMesurement(int arrayIndex)
        {
            DimensioningMeasurementDetails measurement = this.measurements[arrayIndex];

            if (measurement == null) return;

            this.DisplayMesurement(measurement, true);
        }

        private void DisplayMetricSystem(MetricSystemTypeEnum metricSystemType)
        {
            var weightMesurementsUnit = metricSystemType == MetricSystemTypeEnum.Imperial ? MesurementsUnitEnum.Lb : MesurementsUnitEnum.Kg;
            var lengthMesurementsUnit = metricSystemType == MetricSystemTypeEnum.Imperial ? MesurementsUnitEnum.In : MesurementsUnitEnum.Cm;

            string mesuresWeightSign = weightMesurementsUnit.ToString().ToLower();
            string mesuresLengthSign = lengthMesurementsUnit.ToString().ToLower();

            this.lblWeightSystem.Text = mesuresWeightSign;
            this.lblWidthSystem.Text = mesuresLengthSign;
        }

        private void ClearFields()
        {
            this.txtPkgRef.Text = string.Empty;
            this.txtWidth.Text = string.Empty;
            this.txtLength.Text = string.Empty;

            this.txtHeight.Text = string.Empty;
            this.txtWeight.Text = string.Empty;

            this.lblPkgType.Text = this.currentPackageType == null ? string.Empty : this.currentPackageType.name;
        }

        private void MoveToNextMeasurement()
        {
            string errorMessage;
            bool validateResult = this.ValidateInput(out errorMessage);

            if (validateResult == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(errorMessage, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (this.SaveMeasurement(this.currentMeasurenentNumber) == false) return;
            
            this.currentMeasurenentNumber = this.currentMeasurenentNumber >= this.measurements.Length - 1 ? 0 : this.currentMeasurenentNumber + 1;
           
            this.ClearFields();

            this.DisplayMesurement(this.currentMeasurenentNumber);

            this.SetHeaderText();

            this.EnebleCancelButton();
        }

        private void MoveToPreviousMeasurement()
        {
            string errorMessage;
            bool validateResult = this.ValidateInput(out errorMessage);

            if (validateResult == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(errorMessage, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (this.SaveMeasurement(this.currentMeasurenentNumber) == false) return;

            this.currentMeasurenentNumber = this.currentMeasurenentNumber <= 0 ? this.measurements.Length - 1 : this.currentMeasurenentNumber - 1;

            this.ClearFields();

            this.DisplayMesurement(this.currentMeasurenentNumber);

            this.SetHeaderText();

            this.EnebleCancelButton();
        }

        private void EnebleCancelButton()
        {
            this.buttonCancel.Enabled = true;
            
            //foreach (var mesurement in measurements)
            //{
            //    if(mesurement == null)
            //    {
            //        this.buttonCancel.Enabled = false;
            //    }
            //}
        }

        private void UpdateDeviceInformation(string status)
        {
            this.lblDevice.Text = CommonMethods.CurrentDevice != null ? CommonMethods.CurrentDevice.Name : string.Empty;

            this.lblStatus.Text = status;
        }

        private void UpdateDeviceInformation()
        {
            this.UpdateDeviceInformation(string.Empty);
        }

        private void AttachDriver(DimensionerDevice device)
        {
            if (device == null) return;
            
            this.protocolDriver = new AutoConnectCommunicationProtocolDriver(device);
            this.protocolDriver.CommunicationClient.OnConnected += new EventHandler(CommunicationClient_OnConnected);
            this.protocolDriver.CommunicationClient.OnMettlerIPSent += new EventHandler(CommunicationClient_OnMettlerIPSent);
            this.protocolDriver.CommunicationClient.OnResponseReceived += new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ResponseReceivedEventArgs>(CommunicationClient_OnResponseReceived);
            this.protocolDriver.CommunicationClient.OnError += new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs>(CommunicationClient_OnError);
        }

        private void ResetDriver(DimensionerDevice device)
        {
            if (this.protocolDriver == null) return;

            this.protocolDriver.Reset(device);
        }

        private void ShutDownDriver()
        {
            if (this.protocolDriver == null) return;

            this.protocolDriver.CommunicationClient.OnConnected -= new EventHandler(CommunicationClient_OnConnected);
            this.protocolDriver.CommunicationClient.OnMettlerIPSent -= new EventHandler(CommunicationClient_OnMettlerIPSent);
            this.protocolDriver.CommunicationClient.OnResponseReceived -= new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ResponseReceivedEventArgs>(CommunicationClient_OnResponseReceived);
            this.protocolDriver.CommunicationClient.OnError -= new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs>(CommunicationClient_OnError);

            this.protocolDriver.ShutDown();

        }

        private void DriverPerformStep()
        {
            if (this.protocolDriver == null) return;

            this.protocolDriver.PerformAction();
        }

        private void SetStatusMessage(string text, Color color)
        {
            this.lblStatus.Text = text;
            this.lblStatus.ForeColor = color;
        }

        private void SetErrorStatusMessage(string text)
        {
            this.SetStatusMessage(text, Color.Red);
        }

        private void SetInfoStatusMessage(string text)
        {
            this.SetStatusMessage(text, Color.Green);
        }

        private void SafeInvoke(Action action)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(action);
            }
            else
            {
                action();
            }
        }

        private void SetCurrentPackageType(PackageType packageType)
        {
            this.currentPackageType = packageType;

            this.lblPkgType.Text = this.currentPackageType != null ? this.currentPackageType.name : string.Empty;
        }

        #endregion

        public enum ActionType
        {
            Enter,
            Edit
        }
    }
}