﻿namespace CargoMatrix.CargoMeasurements
{
    partial class MeasurementsDisplayControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasurementsDisplayControl));
            this.lblPkgRefHeader = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.btnDelete = new CargoMatrix.UI.CMXPictureButton();
            this.txtPackageReferenceNumber = new System.Windows.Forms.TextBox();
            this.lblBoxType = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPkgRefHeader
            // 
            this.lblPkgRefHeader.BackColor = System.Drawing.Color.White;
            this.lblPkgRefHeader.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPkgRefHeader.Location = new System.Drawing.Point(6, 1);
            this.lblPkgRefHeader.Name = "lblPkgRefHeader";
            this.lblPkgRefHeader.Size = new System.Drawing.Size(158, 14);
            this.lblPkgRefHeader.Text = "Package Ref #";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(86, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 14);
            this.label2.Text = "W";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 14);
            this.label3.Text = "L";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(10, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.Text = "Weight :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(164, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 14);
            this.label5.Text = "H";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWeight
            // 
            this.lblWeight.BackColor = System.Drawing.Color.White;
            this.lblWeight.Location = new System.Drawing.Point(74, 81);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(108, 14);
            // 
            // lblLength
            // 
            this.lblLength.BackColor = System.Drawing.Color.White;
            this.lblLength.Location = new System.Drawing.Point(7, 61);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(67, 14);
            // 
            // lblWidth
            // 
            this.lblWidth.BackColor = System.Drawing.Color.White;
            this.lblWidth.Location = new System.Drawing.Point(86, 61);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(67, 14);
            // 
            // lblHeight
            // 
            this.lblHeight.BackColor = System.Drawing.Color.White;
            this.lblHeight.Location = new System.Drawing.Point(165, 61);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(67, 14);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(195, 80);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.PressedImage = ((System.Drawing.Image)(resources.GetObject("btnDelete.PressedImage")));
            this.btnDelete.Size = new System.Drawing.Size(32, 32);
            this.btnDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDelete.TransparentColor = System.Drawing.Color.White;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtPackageReferenceNumber
            // 
            this.txtPackageReferenceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPackageReferenceNumber.BackColor = System.Drawing.Color.White;
            this.txtPackageReferenceNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtPackageReferenceNumber.ForeColor = System.Drawing.Color.Black;
            this.txtPackageReferenceNumber.Location = new System.Drawing.Point(4, 15);
            this.txtPackageReferenceNumber.Multiline = true;
            this.txtPackageReferenceNumber.Name = "txtPackageReferenceNumber";
            this.txtPackageReferenceNumber.ReadOnly = true;
            this.txtPackageReferenceNumber.Size = new System.Drawing.Size(231, 31);
            this.txtPackageReferenceNumber.TabIndex = 12;
            this.txtPackageReferenceNumber.ReadOnly = true;
            this.txtPackageReferenceNumber.BackColor = System.Drawing.Color.White;

            // 
            // lblBoxType
            // 
            this.lblBoxType.BackColor = System.Drawing.Color.White;
            this.lblBoxType.Location = new System.Drawing.Point(74, 99);
            this.lblBoxType.Name = "lblBoxType";
            this.lblBoxType.Size = new System.Drawing.Size(108, 14);
            this.lblBoxType.BackColor = System.Drawing.Color.White;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(4, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 14);
            this.label6.Text = "Box Type:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MeasurementsDisplayControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblBoxType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPackageReferenceNumber);
            this.Controls.Add(this.lblPkgRefHeader);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "MeasurementsDisplayControl";
            this.Size = new System.Drawing.Size(240, 123);
           
            this.ResumeLayout(false);

        }
        
        #endregion

        private System.Windows.Forms.Label lblPkgRefHeader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.Label lblHeight;
        private CargoMatrix.UI.CMXPictureButton btnDelete;
        private System.Windows.Forms.TextBox txtPackageReferenceNumber;
        private System.Windows.Forms.Label lblBoxType;
        private System.Windows.Forms.Label label6;
        //private CustomUtilities.IndicatorPanel panelIndicators;
    }
}
