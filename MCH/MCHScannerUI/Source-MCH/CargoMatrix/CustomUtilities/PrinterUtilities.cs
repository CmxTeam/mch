﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using System.Windows.Forms;
using System.Drawing;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CustomUtilities
{
    public class PrinterUtilities
    {
        private static MessageListBox reasonsList;
        private static MessageListBox printersList;

        /// <summary>
        /// Displays a popup list box with available reasons to select
        /// </summary>
        /// <param name="isMasterbill">if true displays Masterbill reasons: otherwise housebill</param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public static bool PopulatePrintReasons(bool isMasterbill, out string reason)
        {
            if (reasonsList == null)
            {
                reasonsList = new CargoMatrix.Utilities.MessageListBox();
                reasonsList.HeaderText = "Select Reason";
                reasonsList.HeaderText2 = "Select a Reason from the list";
                reasonsList.MultiSelectListEnabled = false;
                reasonsList.OneTouchSelection = true;
            }
            else
            {
                reasonsList.RemoveAllItems();
            }
            foreach (var rsn in CargoMatrix.Communication.ScannerUtility.Instance.GetPrintReasons(isMasterbill))
            {
                reasonsList.AddItem(new StandardListItem(rsn.ReasonName, null));
            }
            if (DialogResult.OK == reasonsList.ShowDialog())
            {
                reason = (reasonsList.SelectedItems[0] as StandardListItem).title.Text;
                return true;
            }
            else
            {
                reason = string.Empty;
                return false;
            }
        }
        public static bool PopulateAvailablePrinters(LabelTypes labelType, ref LabelPrinter retPrinter)
        {
            return PopulateAvailablePrinters(labelType, ref retPrinter, false);
        }

        public static bool PopulateAvailablePrinters(LabelTypes labelType, ref LabelPrinter retPrinter, bool showSetDefaultButton)
        {
            LabelPrinter prtr = retPrinter;
            if (printersList == null)
            {
                printersList = new CargoMatrix.Utilities.MessageListBox();
                printersList.HeaderText = "Change printer";
                printersList.HeaderText2 = "Select From Available Printers";
                printersList.MultiSelectListEnabled = false;
                printersList.OneTouchSelection = true;
                printersList.ButtonImage = CargoMatrix.Resources.Skin.printerButton;
                printersList.ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;
                printersList.ButtonClick += (s, e) =>
                {
                    
                    if( printersList.SelectedItems.Count == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_NoPrinterSelected,"Select Printer", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        return;
                    }


                    Cursor.Current = Cursors.WaitCursor;
                    LabelPrinter printer = (printersList.SelectedItems[0] as StandardListItem<LabelPrinter>).ItemData; 
                    var status = CargoMatrix.Communication.ScannerUtility.Instance.SetDefaultPrinter(printer);
                    Cursor.Current = Cursors.Default;
                    if(status.TransactionStatus1 == false)
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    else
                        displayPrinters(labelType, prtr);
                };
            }

            printersList.ButtonVisible = showSetDefaultButton;
            printersList.OneTouchSelection = !showSetDefaultButton;

            displayPrinters(labelType, retPrinter);

            if (printersList.ShowDialog() == DialogResult.OK)
            {
                retPrinter = (printersList.SelectedItems[0] as StandardListItem<LabelPrinter>).ItemData;
                return true;
            }
            else
                return false;
        }

        private static void displayPrinters(LabelTypes labelType, LabelPrinter retPrinter)
        {
            printersList.RemoveAllItems();
            printersList.OkEnabled = false;
            foreach (var printer in CargoMatrix.Communication.ScannerUtility.Instance.GetPrinters(labelType))
            {
                string name = printer.IsDefault ? printer.PrinterName + " (Default)" : printer.PrinterName; // append Default suffix to default rpinter name
                Image img = CargoMatrix.Resources.Skin.Printer;

                switch (printer.PrinterType)
                {
                    case PrinterType.MobileLabel: // bluetooth
                        img = printer.PrinterId == retPrinter.PrinterId ? CargoMatrix.Resources.Icons.printerBtooth_check : CargoMatrix.Resources.Icons.printerBtooth;
                        break;
                    case PrinterType.MobileWireless: // wifi
                        img = printer.PrinterId == retPrinter.PrinterId ? CargoMatrix.Resources.Icons.printer_wifi_check : CargoMatrix.Resources.Icons.printer_wifi;
                        break;
                    default: // network
                        img = printer.PrinterId == retPrinter.PrinterId ? CargoMatrix.Resources.Skin.Printer_Check : img;
                        break;
                }

                printersList.AddItem(new SmoothListbox.ListItems.StandardListItem<LabelPrinter>(name, img, printer.PrinterId, printer));
            }
        }
    }
}
