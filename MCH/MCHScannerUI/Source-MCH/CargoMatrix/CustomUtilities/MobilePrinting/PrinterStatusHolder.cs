﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomUtilities.MobilePrinting
{
    public class PrinterStatusHolder
    {
        PrinterStatusEnum statusType;

        public string Message { get; private set; }
        public bool IsError { get; private set; }
        public bool IsWarning { get; private set; }
        public int RemainingPrintNumber{get; set;}


        public PrinterStatusEnum StatusType
        {
            get { return this.statusType; }

            set
            {
                this.statusType = value;

                switch (this.statusType)
                {
                    case PrinterStatusEnum._0:
                        this.Message = "OFFLINE. No error";
                        this.IsError = true;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum._1:
                        this.Message = "OFFLINE. Battery near end";
                        this.IsError = true;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum._2:
                        this.Message = "OFFLINE. Buffer near full";
                        this.IsError = true;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum._3:
                        this.Message = "OFFLINE. Battery near end & buffer near full";
                        this.IsError = true;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum.A:
                        this.Message = "ONLINE. Wait to receive data. No error";
                        this.IsError = false;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum.B:
                        this.Message = "ONLINE. Wait to receive data. Battery near end";
                        this.IsError = false;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.C:
                        this.Message = "ONLINE. Wait to receive data. Buffer near full";
                        this.IsError = false;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.D:
                        this.Message = "ONLINE. Wait to receive data. Battery near end & buffer near full";
                        this.IsError = false;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.G:
                        this.Message = "ONLINE. Printing. No error";
                        this.IsError = false;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum.H: 
                        this.Message = "ONLINE. Printing. Battery near end";
                        this.IsError = false;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.I: 
                        this.Message = "ONLINE. Printing. Buffer near full";
                        this.IsError = false;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.J: 
                        this.Message = "ONLINE. Printing. Battery near end & buffer near full";
                        this.IsError = false;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.M: 
                        this.Message = "ONLINE. Standby (Wait to Dispense). No error";
                        this.IsError = false;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum.N: 
                        this.Message = "ONLINE. Standby (Wait to Dispense). Battery near end";
                        this.IsError = false;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.O: 
                        this.Message = "ONLINE. Standby (Wait to Dispense). Buffer near full";
                        this.IsError = false;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.P: 
                        this.Message = "ONLINE. Standby (Wait to Dispense). Battery near end & buffer near full";
                        this.IsError = false;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.S: 
                        this.Message = "ONLINE. Analyzing/Editing. No error";
                        this.IsError = false;
                        this.IsWarning = false;
                        break;
                    case PrinterStatusEnum.T: 
                        this.Message = "ONLINE. Analyzing/Editing. Battery near end";
                        this.IsError = false;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.U: 
                        this.Message = "ONLINE. Analyzing/Editing. Buffer near full";
                        this.IsError = false;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.V: 
                        this.Message = "ONLINE. Analyzing/Editing. Battery near end & buffer near full";
                        this.IsError = false;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.a: 
                        this.Message = "ERROR DETECTION. Buffer over";
                        this.IsError = true;
                        this.IsWarning = true;
                        break;
                    case PrinterStatusEnum.b: 
                        this.Message = "ERROR DETECTION. Not used";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.c: 
                        this.Message = "ERROR DETECTION. Paper end";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.d: 
                        this.Message = "ERROR DETECTION. Battery error";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.e: 
                        this.Message = "ERROR DETECTION. Not used";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.f: 
                        this.Message = "ERROR DETECTION. Sensor error";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.g: 
                        this.Message = "ERROR DETECTION. Head error";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.h: 
                        this.Message = "ERROR DETECTION. Cover open";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.i: 
                        this.Message = "ERROR DETECTION. Not used";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.j: 
                        this.Message = "ERROR DETECTION. Not used";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;
                    case PrinterStatusEnum.k: 
                        this.Message = "ERROR DETECTION. Other errors";
                        this.IsError = true;
                        this.IsWarning = true; 
                        break;

                    case PrinterStatusEnum.NoError:
                        this.Message = "NO ERROR";
                        this.IsError = false;
                        this.IsWarning = false;
                        break;

                    case PrinterStatusEnum.Error:
                        this.Message = "ERROR";
                        this.IsError = true;
                        this.IsWarning = true;
                        break;

                    default:
                        this.Message = "WARNING. Unknown status";
                        this.IsError = false;
                        this.IsWarning = false;
                        break;
                }
            }
        }
    }
}
