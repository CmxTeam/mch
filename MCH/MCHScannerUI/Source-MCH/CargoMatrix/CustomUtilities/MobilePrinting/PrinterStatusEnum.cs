﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomUtilities.MobilePrinting
{
    public enum PrinterStatusEnum
    {
        NA = -1, // status unknown 
        _0 = 0x30, // '0'
        _1 = 0x31, // '1'
        _2 = 0x32, // '2'
        _3 = 0x33, // '3'
        A = 0x41, // 'A'
        B = 0x42, // 'B'
        C = 0x43, // 'C'
        D = 0x44, // 'D'
        G = 0x47, // 'G'
        H = 0x48, // 'H'
        I = 0x49, // 'I'
        J = 0x4A, // 'J'
        M = 0x4D, // 'M'
        N = 0x4E, // 'N'
        O = 0x4F, // 'O'
        P = 0x50, // 'P'
        S = 0x53, // 'S'
        T = 0x54, // 'T'
        U = 0x55, // 'U'
        V = 0x56, // 'V'
        a = 0x61, // 'a'
        b = 0x62, // 'b'
        c = 0x63, // 'c'
        d = 0x64, // 'd'
        e = 0x65, // 'e'
        f = 0x66, // 'f'
        g = 0x67, // 'g'
        h = 0x68, // 'h'
        i = 0x69, // 'i'
        j = 0x6A, // 'j'
        k = 0x6B, // 'k'
        NoError = 0x06, // General no error
        Error = 0x15 // General error
    }
}
