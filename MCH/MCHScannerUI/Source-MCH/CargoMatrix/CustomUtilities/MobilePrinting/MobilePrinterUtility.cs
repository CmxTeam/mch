﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Symbol.WPAN.Bluetooth;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoMeasurements;

namespace CustomUtilities.MobilePrinting
{
    public class MobilePrinterUtility
    {
        #region Fields
        int numberOfCopies = 1;
        CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter labelPrinter;
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets or Set label printer
        /// </summary>
        public CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter LabelPrinter
        {
            get { return labelPrinter; }
            set { labelPrinter = value; }
        }

        /// <summary>
        /// Gets or Sets number of copies to print. Default value 1
        /// </summary>
        public int NumberOfCopies
        {
            get { return numberOfCopies; }
            set { numberOfCopies = value; }
        }

        #endregion

        public MobilePrinterUtility(CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter labelPrinter)
        {
            this.labelPrinter = labelPrinter;

            if(string.IsNullOrEmpty(this.labelPrinter.Pin) == true)
            {
                this.labelPrinter.Pin = "0000000000000000";
            }

            if (this.labelPrinter.Port == 0)
            {
                this.labelPrinter.Port = 1024;
            }
        }

        #region Wireless
        /// <summary>
        /// Prints barcode label using wireless connection. Shows message on error
        /// </summary>
        /// <param name="barcode">Barcode to be printed</param>
        /// <param name="humanReadibleText">Human readable text</param>
        public void WirelessPrint(string barcode, string humanReadibleText)
        {
            MobilePrinterUtility.RawWirelessPrint(this.labelPrinter.IPAddress, this.labelPrinter.Port, barcode, humanReadibleText, this.numberOfCopies);
        }

        /// <summary>
        /// Prints barcode label using wireless connection. Shows message on error
        /// </summary>
        /// <param name="barcode">Barcode to be printed</param>
        public void WirelessPrint(string barcode)
        {
            MobilePrinterUtility.RawWirelessPrint(this.labelPrinter.IPAddress, this.labelPrinter.Port, barcode, barcode, this.numberOfCopies);
        }

        /// <summary>
        /// Prints housebill barcodes using wireless connection. Shows message on error
        /// </summary>
        /// <param name="houseBillNuber">Housebill number</param>
        /// <param name="piecesNumbers">Pieces numbers</param>
        public void WirelessHouseBillPrint(string houseBillNuber, string[] piecesNumbers)
        {
            string[] barcodes = CreateHousebillBarcodes(houseBillNuber, piecesNumbers);

            RawWirelessPrint(this.labelPrinter.IPAddress, this.labelPrinter.Port, barcodes, barcodes, 1);
        }

        /// <summary>
        /// Calibrates printer for gap paper labels.
        /// </summary>
        /// <param name="printerIpAddress">Printer IP address</param>
        /// <param name="printerPort">Printer port</param>
        public static void CalibrateWirelessPrinter(string printerIpAddress, int printerPort)
        {
            PrintProgressMessageBox umb = new PrintProgressMessageBox(PrintProgressMessageBox.PrinterTypeEnum.Wireless, PrintProgressMessageBox.PrintActionType.Calibrate);
            umb.PrinterIpAddress = printerIpAddress;
            umb.PrinterPort = printerPort;
            umb.ShowDialog();

            Application.DoEvents();
        }

        /// <summary>
        /// Prints uld barcode using wireless connection. Shows message on error
        /// </summary>
        /// <param name="uldNumber">Uld number</param>
        public void WirelessUldPrint(string uldNumber)
        {
            this.WirelessPrint(CreateULDBarcode(uldNumber));
        }

        /// <summary>
        /// Prints truck barcode using wireless connection. Shows message on error
        /// </summary>
        /// <param name="truckName">Truck name</param>
        public void WirelessTruckPrint(string truckName)
        {
            this.WirelessPrint(CreateTruckBarcode(truckName));
        }

        /// <summary>
        /// Prints barcodes for multiple trucks using wireless connection. Shows message on error
        /// </summary>
        /// <param name="truckNames">Trucks Names</param>
        public void WirelessTruckPrint(string[] truckNames)
        {
            var trackLabels = from truckName in truckNames
                              select CreateTruckBarcode(truckName);

            RawWirelessPrint(this.labelPrinter.IPAddress, this.labelPrinter.Port, trackLabels.ToArray(), truckNames, 1);
        }

        /// <summary>
        /// Prints area barcode using wireless connection. Shows message on error
        /// </summary>
        /// <param name="areaName">Area name</param>
        public void WirelessAreaPrint(string areaName)
        {
            this.WirelessPrint(CreateAreaBarcode(areaName));
        }

        /// <summary>
        /// Prints door barcode using wireless connection. Shows message on error
        /// </summary>
        /// <param name="doorName">Door name</param>
        public void WirelessDoorPrint(string doorName)
        {
            this.WirelessPrint(CreateDoorBarcode(doorName));
        }

        /// <summary>
        /// Prints screening area barcode using wireless connection. Shows message on error
        /// </summary>
        /// <param name="screeningAreaName">Screening area name</param>
        public void WirelessScreeningAreaPrint(string screeningAreaName)
        {
            this.WirelessPrint(CreateSreeningAreaBarcode(screeningAreaName));
        }

        /// <summary>
        /// Prints Area, Door, ScreeningArea barcodes using wireless connection. Shows message on error
        /// </summary>
        /// <param name="location">Location name</param>
        public void WirelessLocationPrint(CargoMatrix.Communication.ScannerUtilityWS.LocationItem location)
        {
            switch (location.LocationType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Area:
                    this.WirelessAreaPrint(location.Location);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Door:
                    this.WirelessDoorPrint(location.Location);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.ScreeningArea:
                    this.WirelessScreeningAreaPrint(location.Location);
                    break;
            }
        } 
        
        /// <summary>
        /// Prints User name barcode using wireless connection. Shows message on error
        /// </summary>
        /// <param name="userName">User name</param>
        public void WirelessUserPrint(string userName)
        {
            this.WirelessPrint(userName);
        }

        public void WirelessMeasurementsPrint(DimensioningMeasurementDetails[] details)
        {
            
        }

        #endregion

        #region Bluetooth

        /// <summary>
        /// Prints barcode label using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="barcode">Printer pair pin code</param>
        /// <param name="humanReadibleText">Human readable text</param>
        public void BluetoothPrint(string barcode, string humanReadibleText)
        {
            MobilePrinterUtility.RawBluetoothPrint(this.labelPrinter.MacAddress, this.labelPrinter.Pin, barcode, humanReadibleText, numberOfCopies);
        }

        /// <summary>
        /// Prints barcode label using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="barcode">Printer pair pin code</param>
        public void BluetoothPrint(string barcode)
        {
            this.BluetoothPrint(barcode, barcode);
        }

        /// <summary>
        /// Prints housebill barcodes using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="houseBillNuber">Housebill number</param>
        /// <param name="piecesNumbers">Pieces numbers</param>
        public void BluetoothHouseBillPrint(string houseBillNuber, string[] piecesNumbers)
        {
            string[] barcodes = CreateHousebillBarcodes(houseBillNuber, piecesNumbers);

            MobilePrinterUtility.RawBluetoothPrint(this.labelPrinter.MacAddress, this.labelPrinter.Pin, barcodes, barcodes, 1);
        }

        /// <summary>
        /// Calibrates printer for gap paper labels
        /// </summary>
        /// <param name="printerMacAddress">Printer mac address in the format ww:ww:ww:ww:ww:ww, example 00:17:42:9B:59:E2</param>
        /// <param name="pin">Printer pair pin code</param>
        public static void CalibrateBluetoothPrinter(string printerMacAddress, string pin)
        {
            PrintProgressMessageBox umb = new PrintProgressMessageBox(PrintProgressMessageBox.PrinterTypeEnum.Bluetooth, PrintProgressMessageBox.PrintActionType.Calibrate);
            umb.PrinterMacAddress = printerMacAddress;
            umb.Pin = pin;
            umb.ShowDialog();

            Application.DoEvents();
        }

        /// <summary>
        /// Calibrates printer for gap paper labels
        /// </summary>
        /// <param name="printerMacAddress">Printer mac address in the format ww:ww:ww:ww:ww:ww, example 00:17:42:9B:59:E2</param>
        public static void CalibrateBlueToothPrinter(string printerMacAddress)
        {
            CalibrateBluetoothPrinter(printerMacAddress, "0000000000000000");
        }

        /// <summary>
        /// Prints Uld barcode using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="uldNumber">Uld number</param>
        public void BluetoothUldPrint(string uldNumber)
        {
            this.BluetoothPrint(CreateULDBarcode(uldNumber));
        }

        /// <summary>
        /// Prints Truck barcode using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="truckName">Truck name</param>
        public void BluetoothTruckPrint(string truckName)
        {
            this.BluetoothPrint(CreateTruckBarcode(truckName));
        }

        /// <summary>
        /// Prints barcodes for multiple trucks using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="truckNames">Trucks names</param>
        public void BluetoothTruckPrint(string[] truckNames)
        {
            var trackLabels = from truckName in truckNames
                              select CreateTruckBarcode(truckName);

            MobilePrinterUtility.RawBluetoothPrint(this.labelPrinter.MacAddress, this.labelPrinter.Pin, trackLabels.ToArray(), truckNames, 1);
        }

        /// <summary>
        /// Prints Area barcode using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="areaName">Area name</param>
        public void BluetoothAreaPrint(string areaName)
        {
            this.BluetoothPrint(CreateAreaBarcode(areaName));
        }

        /// <summary>
        /// Prints Door barcode using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="doorName">Door name</param>
        public void BluetoothDoorPrint(string doorName)
        {
            this.BluetoothPrint(CreateDoorBarcode(doorName));
        }

        /// <summary>
        /// Prints Screening Area barcode using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="screeningAreaName">Screening Area name</param>
        public void BluetoothScreeningAreaPrint(string screeningAreaName)
        {
            this.BluetoothPrint(CreateSreeningAreaBarcode(screeningAreaName));
        }

        /// <summary>
        /// Prints User name barcode using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="userName">User name</param>
        public void BluetoothUserPrint(string userName)
        {
            this.BluetoothPrint(CreateUserBarcode(userName));
        }

        /// <summary>
        /// Prints Area, Door, ScreeningArea barcodes using bluetooth connection. Shows message on error
        /// </summary>
        /// <param name="location">Location item</param>
        public void BluetoothLocationPrint(CargoMatrix.Communication.ScannerUtilityWS.LocationItem location)
        {
            switch (location.LocationType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Area:
                    this.BluetoothAreaPrint(location.Location);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Door:
                    this.BluetoothDoorPrint(location.Location);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.ScreeningArea:
                    this.BluetoothScreeningAreaPrint(location.Location);
                    break;
            }
        }

        #endregion

        /// <summary>
        /// Prints barcode label. Shows message on error
        /// </summary>
        /// <param name="barcode">Barcode to be printed</param>
        /// <param name="humanReadibleText">Human readable text</param>
        public void Print(string barcode, string humanReadibleText)
        {
            switch(labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothPrint(barcode, humanReadibleText);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessPrint(barcode, humanReadibleText);
                    break;
            }
        }

        /// <summary>
        /// Prints barcode label. Shows message on error
        /// </summary>
        /// <param name="barcode">Barcode to be printed</param>
        public void Print(string barcode)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothPrint(barcode);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessPrint(barcode);
                    break;
            }
        }

        /// <summary>
        /// Prints housebill barcodes. Shows message on error
        /// </summary>
        /// <param name="houseBillNuber">Housebill number</param>
        /// <param name="piecesNumbers">Pieces numbers</param>
        public void HouseBillPrint(string houseBillNuber, string[] piecesNumbers)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothHouseBillPrint(houseBillNuber, piecesNumbers);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessHouseBillPrint(houseBillNuber, piecesNumbers);
                    break;
            }
        }

        public void HouseBillPrint(List<KeyValuePair<string, int>> housebillBarcodes)
        {
            List<string> barcodes = new List<string>();

            foreach (var pair in housebillBarcodes)
            {
                List<string> pieceNumbers = new List<string>();

                for(int i = 0; i < pair.Value; i++)
                {
                    pieceNumbers.Add((i + 1).ToString());
                }

                barcodes.AddRange(CreateHousebillBarcodes(pair.Key, pieceNumbers.ToArray()));
            }
            
            var barcodeArray = barcodes.ToArray();
            
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    MobilePrinterUtility.RawBluetoothPrint(this.labelPrinter.MacAddress, this.labelPrinter.Pin, barcodeArray, barcodeArray, 1);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    MobilePrinterUtility.RawWirelessPrint(this.labelPrinter.IPAddress, this.labelPrinter.Port, barcodeArray, barcodeArray, 1);
                    break;
            }
        }


        /// <summary>
        /// Prints uld barcode. Shows message on error
        /// </summary>
        /// <param name="uldNumber">Uld number</param>
        public void UldPrint(string uldNumber)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothUldPrint(uldNumber);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessUldPrint(uldNumber);
                    break;
            }
        }

        /// <summary>
        /// Prints truck barcode. Shows message on error
        /// </summary>
        /// <param name="truckName">Truck name</param>
        public void TruckPrint(string truckName)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothTruckPrint(truckName);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessTruckPrint(truckName);
                    break;
            }
        }

        /// <summary>
        /// Prints barcodes for multiple trucks using wireless connection. Shows message on error
        /// </summary>
        /// <param name="truckNames">Trucks Names</param>
        public void TruckPrint(string[] truckNames)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothTruckPrint(truckNames);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessTruckPrint(truckNames);
                    break;
            }
        }

        /// <summary>
        /// Prints area barcode. Shows message on error
        /// </summary>
        /// <param name="areaName">Area name</param>
        public void AreaPrint(string areaName)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothAreaPrint(areaName);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessAreaPrint(areaName);
                    break;
            }
        }

        /// <summary>
        /// Prints door barcode. Shows message on error
        /// </summary>
        /// <param name="doorName">Door name</param>
        public void DoorPrint(string doorName)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothDoorPrint(doorName);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessDoorPrint(doorName);
                    break;
            }
        }

        /// <summary>
        /// Prints screening area barcode. Shows message on error
        /// </summary>
        /// <param name="screeningAreaName">Screening area name</param>
        public void ScreeningAreaPrint(string screeningAreaName)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothScreeningAreaPrint(screeningAreaName);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessScreeningAreaPrint(screeningAreaName);
                    break;
            }
        }

        /// <summary>
        /// Prints Area, Door, ScreeningArea barcodes. Shows message on error
        /// </summary>
        /// <param name="location">Location name</param>
        public void LocationPrint(CargoMatrix.Communication.ScannerUtilityWS.LocationItem location)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothLocationPrint(location);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessLocationPrint(location);
                    break;
            }
        } 

        /// <summary>
        /// Prints User name barcode. Shows message on error
        /// </summary>
        /// <param name="userName">User name</param>
        public void UserPrint(string userName)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel:
                    this.BluetoothUserPrint(userName);
                    break;

                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessUserPrint(userName);
                    break;
            }
        }


        public void MeasurementsPrint(DimensioningMeasurementDetails[] details)
        {
            switch (labelPrinter.PrinterType)
            {
                case CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless:
                    this.WirelessMeasurementsPrint(details);
                    break;
            }
        }

        #region Additional methods

        private string[] CreateHousebillBarcodes(string houseBillNumber, string[] piecesNumbers)
        {
            List<string> barcodes = new List<string>();

            foreach (var piecesNumber in piecesNumbers)
            {
                var pn = piecesNumber.Length < 4 ? string.Format("{0}{1}", new string('0', 4 - piecesNumber.Length), piecesNumber) : piecesNumber;

                var barcode = string.Format("{0}+Y{1}+", houseBillNumber, pn);
                barcodes.Add(barcode);
            }

            return barcodes.ToArray();
        }

        private string CreateDoorBarcode(string doorName)
        {
            return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixDoor, doorName);
        }

        private string CreateSreeningAreaBarcode(string screeningAreaName)
        {
            return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixScreeningArea, screeningAreaName);
        }

        private string CreateAreaBarcode(string areaName)
        {
            return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixArea, areaName);
        }

        private string CreateUserBarcode(string userName)
        {
            return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixUser, userName);
        }

        private string CreateTruckBarcode(string truckName)
        {
            return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixTruck, truckName);
        }

        private string CreateULDBarcode(string uldNumber)
        {
            return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixULD, uldNumber);
        }

        private static bool HasDevice(Bluetooth bluetooth, string printerMacAddress)
        {
            RemoteDevice device = null;
            for (int i = 0; i < bluetooth.RemoteDevices.Length; i++)
            {
                if (bluetooth.RemoteDevices[i].Address.Equals(printerMacAddress))
                {
                    device = bluetooth.RemoteDevices[i];
                    break;
                }
            }

            return device != null;
        }

        private static RemoteDevice GetDevice(Bluetooth bluetooth, string printerMacAddress)
        {
            RemoteDevice device = null;
            for (int i = 0; i < bluetooth.RemoteDevices.Length; i++)
            {
                if (bluetooth.RemoteDevices[i].Address.Equals(printerMacAddress))
                {
                    device = bluetooth.RemoteDevices[i];
                    break;
                }
            }

            return device;
        }

        private static void RawBluetoothPrint(string printerMacAddress, string pin, string barcode, string humanReadableText, int numberOfCopies)
        {
            RawBluetoothPrint(printerMacAddress, pin, new string[] { barcode }, new string[] { humanReadableText }, numberOfCopies);
        }

        private static void RawBluetoothPrint(string printerMacAddress, string pin, string[] barcodes, string[] humanReadableTexts, int numberOfCopies)
        {
            PrintProgressMessageBox umb = new PrintProgressMessageBox(PrintProgressMessageBox.PrinterTypeEnum.Bluetooth, PrintProgressMessageBox.PrintActionType.Print);
            umb.PrinterMacAddress = printerMacAddress;
            umb.Pin = pin;
            umb.Barcodes = barcodes;
            umb.HumanReadableTexts = humanReadableTexts;
            umb.NumberOfCopies = numberOfCopies;
            umb.ShowDialog();

            Application.DoEvents();


        }

        private static string GetBarcodePositionOffset(int barcodeLength)
        {
            if (barcodeLength < 2) return "0300";

            if (barcodeLength > 19) return "0005";

            switch (barcodeLength)
            {
                case 19: return "0010";

                case 18: return "0020";

                case 17: return "0030";

                case 16: return "0040";

                case 15: return "0050";

                case 14: return "0060";

                case 13: return "0080";

                case 12: return "0100";

                case 11: return "0120";

                case 10: return "0140";

                case 9: return "0160";

                case 8: return "0180";

                case 7: return "0200";

                case 6: return "0220";

                case 5: return "0240";

                case 4: return "0260";

                case 3: return "0280";

                case 2: return "0290";

                default: return "0005";
            }

        }

        private static void RawWirelessPrint(string printerIpAddress, int printerPort, string[] barcodes, string[] humanReadableTexts, int numberOfCopies)
        {
            PrintProgressMessageBox umb = new PrintProgressMessageBox(PrintProgressMessageBox.PrinterTypeEnum.Wireless, PrintProgressMessageBox.PrintActionType.Print);
            umb.PrinterIpAddress = printerIpAddress;
            umb.PrinterPort = printerPort;
            umb.Barcodes = barcodes;
            umb.HumanReadableTexts = humanReadableTexts;
            umb.NumberOfCopies = numberOfCopies;
            umb.ShowDialog();

            Application.DoEvents();
        }
        
        private static void RawWirelessPrint(string printerIpAddress, int printerPort, string barcode, string humanReadableText, int numberOfCopies)
        {
            MobilePrinterUtility.RawWirelessPrint(printerIpAddress, printerPort, new string[] { barcode }, new string[] { humanReadableText }, numberOfCopies);
        }
        #endregion

    }
}
