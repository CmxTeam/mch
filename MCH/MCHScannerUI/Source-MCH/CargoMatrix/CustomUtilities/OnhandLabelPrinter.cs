﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CustomUtilities
{
    public class OnhandLabelPrinter : HawbLabelPrinter
    {
        public static new bool Show(long hawbId, string hawbNo, int NumberOfPcs)
        {
            var instance = new OnhandLabelPrinter(hawbId, hawbNo, NumberOfPcs);
            //if (PrinterUtilities.PopulatePrintReasons(false, out instance.reason))
            instance.printModePopup.ShowDialog();
            instance.barcode.StopRead();
            return false;
        }

        private OnhandLabelPrinter(long hawbId, string hawbNo, int noOfPcs)
            : base(hawbId, hawbNo, noOfPcs, LabelTypes.ULD)
        {
        }

        protected override void PrintLabels(List<int> piecesToPrint)
        {
            string pcsString = string.Join(",", piecesToPrint.ConvertAll<string>(p => p.ToString()).ToArray());
            var status = CargoMatrix.Communication.OnHand.OnHand.Instance.PrintPackageLabel(selectedPrinter.PrinterName, hawbID, pcsString);
            if (status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + selectedPrinter.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            else
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Fail", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        }
    }
}
