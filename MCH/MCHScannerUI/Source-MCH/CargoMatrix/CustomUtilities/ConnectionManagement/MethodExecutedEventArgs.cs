﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomUtilities.ConnectionManagement
{
    public class MethodExecutedEventArgs<T> : EventArgs
    {
        T result;
        ExecutionResultTypeEnum executionResultType;
        Exception exception;

        public Exception Exception
        {
            get { return exception; }
            set { exception = value; }
        }

        public ExecutionResultTypeEnum ExecutionResultType
        {
            get { return executionResultType; }
        }

        public T Result
        {
            get { return result; }
            set { result = value; }
        }

        public MethodExecutedEventArgs(ExecutionResultTypeEnum executionResultType)
        {
            this.executionResultType = executionResultType;
        }
    }
}
