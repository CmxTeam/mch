﻿namespace CargoMatrix.AQM
{
    partial class ManualHawbMawb
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.labelPcs = new System.Windows.Forms.Label();
            this.textBoxCarrier = new CargoMatrix.UI.CMXTextBox();
            this.textBoxConsol = new CargoMatrix.UI.CMXTextBox();
            this.textBoxHawb = new CargoMatrix.UI.CMXTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelCarrier = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Controls.Add(this.labelPcs);
            this.panel1.Controls.Add(this.textBoxCarrier);
            this.panel1.Controls.Add(this.textBoxConsol);
            this.panel1.Controls.Add(this.textBoxHawb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.labelCarrier);
            this.panel1.Controls.Add(this.message);
            this.panel1.Location = new System.Drawing.Point(3, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 217);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 176);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOk.Location = new System.Drawing.Point(56, 176);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.TabIndex = 11;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 173);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // labelPcs
            // 
            this.labelPcs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPcs.BackColor = System.Drawing.Color.White;
            this.labelPcs.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPcs.ForeColor = System.Drawing.Color.Black;
            this.labelPcs.Location = new System.Drawing.Point(6, 117);
            this.labelPcs.Name = "labelPcs";
            this.labelPcs.Size = new System.Drawing.Size(64, 15);
            this.labelPcs.Text = "HAWB#:";
            // 
            // textBoxCarrier
            // 
            this.textBoxCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCarrier.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxCarrier.Location = new System.Drawing.Point(6, 71);
            this.textBoxCarrier.MaxLength = 4;
            this.textBoxCarrier.Name = "textBoxCarrier";
            this.textBoxCarrier.Size = new System.Drawing.Size(64, 26);
            this.textBoxCarrier.TabIndex = 1;
            this.textBoxCarrier.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            this.textBoxCarrier.TextChanged += new System.EventHandler(this.textBoxMmawb_TextChanged);
            // 
            // textBoxConsol
            // 
            this.textBoxConsol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxConsol.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxConsol.Location = new System.Drawing.Point(76, 71);
            this.textBoxConsol.MaxLength = 8;
            this.textBoxConsol.Name = "textBoxConsol";
            this.textBoxConsol.Size = new System.Drawing.Size(146, 26);
            this.textBoxConsol.TabIndex = 2;
            this.textBoxConsol.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textBoxConsol.TextChanged += new System.EventHandler(this.textBoxMmawb_TextChanged);
            // 
            // textBoxHawb
            // 
            this.textBoxHawb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxHawb.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxHawb.Location = new System.Drawing.Point(6, 137);
            this.textBoxHawb.Name = "textBoxHawb";
            this.textBoxHawb.Size = new System.Drawing.Size(216, 26);
            this.textBoxHawb.TabIndex = 3;
            this.textBoxHawb.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            this.textBoxHawb.TextChanged += new System.EventHandler(this.textBoxHawb_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(76, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.Text = "MAWB#:";
            // 
            // labelCarrier
            // 
            this.labelCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCarrier.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelCarrier.ForeColor = System.Drawing.Color.Black;
            this.labelCarrier.Location = new System.Drawing.Point(1, 53);
            this.labelCarrier.Name = "labelCarrier";
            this.labelCarrier.Size = new System.Drawing.Size(50, 15);
            this.labelCarrier.Text = "Carrier:";
            // 
            // message
            // 
            this.message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.message.BackColor = System.Drawing.Color.White;
            this.message.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.message.Location = new System.Drawing.Point(13, 31);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(209, 13);
            this.message.Text = "Provide the following details:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.Text = "Carrier:";
            InitializeImages();
            // 
            // ManualHawbMawb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "ManualHawbMawb";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelPcs;
        private System.Windows.Forms.Label labelCarrier;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CargoMatrix.UI.CMXTextBox textBoxHawb;//CargoMatrix.UI.CMXTextBox
        private CargoMatrix.UI.CMXTextBox textBoxCarrier;
        private CargoMatrix.UI.CMXTextBox textBoxConsol;
        private System.Windows.Forms.Label label1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
    }
}

