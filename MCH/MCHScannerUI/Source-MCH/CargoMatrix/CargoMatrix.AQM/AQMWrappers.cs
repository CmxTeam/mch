﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSAQM;

namespace CargoMatrix.AQM
{
    public class MawbItemAsHousebill : CargoMatrix.Communication.DTO.IHouseBillItem
    {
        private MasterBillAqmDetail mawb;

        public MawbItemAsHousebill(MasterBillAqmDetail mawb)
        {
            this.mawb = mawb;
        }
        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get { return mawb.MasterBillNo; }
        }

        public int HousebillId
        {
            get { return mawb.MasterBillID; }
        }

        public string Origin
        {
            get { return mawb.Origin; }
        }

        public string Destination
        {
            get { return mawb.Destination; }
        }

        public int TotalPieces
        {
            get { return mawb.TotalPieces ?? 1; }
        }

        public int ScannedPieces
        {
            get { return 0; }
        }

        public int Slac
        {
            get { return 0; }
        }

        public int Weight
        {
            get
            {
                double wgt = mawb.Weight ?? 0;
                return (int)wgt;
            }
        }

        public string Location
        {
            get { return string.Empty; }
        }

        public int Flags
        {
            get { return 0; }
        }

        public CargoMatrix.Communication.DTO.ScanModes ScanMode
        {
            get { return CargoMatrix.Communication.DTO.ScanModes.Count; }
        }

        public CargoMatrix.Communication.DTO.HouseBillStatuses status
        {
            get
            {
                return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
            }
        }

        public CargoMatrix.Communication.DTO.RemoveModes RemoveMode
        {
            get { return CargoMatrix.Communication.DTO.RemoveModes.NA; }
        }

        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}-{3}", mawb.Origin, mawb.CarrierNo, mawb.MasterBillNo, mawb.Destination); }
        }

        public string Line2
        {
            get { return string.Format("Weight: {0} KGS", mawb.Weight ?? 0); }
        }

        public string Line3
        {
            get { return string.Format("Pieces: {0}", mawb.TotalPieces.HasValue ? mawb.TotalPieces.ToString() : "N/A"); }
        }

        public string Line4
        {
            get { return string.Empty; }
        }

        #endregion
    }

    public class AQMIHousebill : CargoMatrix.Communication.DTO.IHouseBillItem
    {

        private HouseBillAqmDetail hawb;
        public AQMIHousebill(HouseBillAqmDetail hawb)
        {
            this.hawb = hawb;
        }
        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get { return hawb.HouseBillNo; }
        }

        public int HousebillId
        {
            get { return hawb.HouseBillID; }
        }

        public string Origin
        {
            get { return hawb.Origin; }
        }

        public string Destination
        {
            get { return hawb.Destination; }
        }

        public int TotalPieces
        {
            get { return hawb.TotalPieces ?? 1; }
        }

        public int ScannedPieces
        {
            get { return 0; }
        }

        public int Slac
        {
            get { return 0; }
        }

        public int Weight
        {
            get { return (int)hawb.Weight; }
        }

        public string Location
        {
            get { return string.Empty; }
        }

        public int Flags
        {
            get { return 0; }
        }

        public CargoMatrix.Communication.DTO.ScanModes ScanMode
        {
            get
            {
                return CargoMatrix.Communication.DTO.ScanModes.Count;
            }
        }

        public CargoMatrix.Communication.DTO.HouseBillStatuses status
        {
            get
            {

                return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
            }
        }

        public CargoMatrix.Communication.DTO.RemoveModes RemoveMode
        {
            get
            {

                return CargoMatrix.Communication.DTO.RemoveModes.NA;
            }
        }
        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}", hawb.Origin, hawb.HouseBillNo, hawb.Destination); }
        }

        public string Line2
        {
            get { return string.Format("Weight: {0} KGS", hawb.Weight); }
        }

        public string Line3
        {
            get { return string.Format("Pieces: {0}", hawb.TotalPieces); }
        }

        public string Line4
        {
            get { return string.Empty; }
        }

        #endregion
    }

}
