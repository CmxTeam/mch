﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SmartDeviceTest
{
    public class ConnectionManager
    {
        private string checkUri;
        private int stopped;
        private int isConnecting;
        private int checkInterval;
        private OpenNETCF.Net.ConnectionManager connectionManager;

        public event EventHandler<ConnectionEventArgs> BeforeConnect;
        public event EventHandler<ConnectionEventArgs> ConnectionFailed;
        public event EventHandler<ConnectionEventArgs> AfterConnect;

        /// <summary>
        /// Get or set the interval to check if connection exists. Is specified in seconds.
        /// </summary>
        public int CheckInterval
        {
            get { return checkInterval; }
            set { checkInterval = value; }
        }
        
        public string CheckUri
        {
            get { return checkUri; }
            set { checkUri = value; }
        }
        
        public bool IsConnectedToInternet
        {
            get
            {
                System.Net.WebRequest webRequest;

                try
                {
                    System.Uri url = new System.Uri(checkUri);
                    webRequest = System.Net.WebRequest.Create(url);
                    var response = webRequest.GetResponse();
                    response.Close();
                    webRequest = null;
                    return true;
                }

                catch
                {
                    webRequest = null;
                    return false;
                }
            }
        }

        public ConnectionManager()
        {
            this.checkUri = "http://www.microsoft.com";
            this.stopped = 0;
            this.isConnecting = 0;
            this.checkInterval = 5;
            
            this.connectionManager = new OpenNETCF.Net.ConnectionManager();
        }

        public void Start()
        {
            this.IsStopped = false;
            
            ThreadPool.QueueUserWorkItem(o =>
                {
                    while (this.IsStopped == false)
                    {
                        var isConnected = this.IsConnectedToInternet;

                        if (isConnected == false)
                        {
                            this.ReConnect();
                        }

                        Thread.Sleep(checkInterval * 1000);
                    }
                });
        }

        public void Stop()
        {
            this.IsStopped = true;
        }

        public bool IsStopped
        {
            get
            {
                int s = this.stopped;
                return s == 1 ? true : false;
            }
            private set
            {
                int s = value == true ? 1 : 0;
                Interlocked.Exchange(ref this.stopped, s);
            }
        }

        private bool IsConnecting
        {
            get
            {
                int s = this.isConnecting;
                return s == 1 ? true : false;
            }
            set
            {
                int s = value == true ? 1 : 0;
                Interlocked.Exchange(ref this.isConnecting, s);
            }
        }


        public void ReConnect()
        {
            var destinations = this.connectionManager.EnumDestinations();

            ConnectionEventArgs args = null;
            foreach (var destination in destinations)
            {
                this.FireBeforeConnect(destination);

                try
                {
                    this.connectionManager.Connect(destination.Guid, true, OpenNETCF.Net.ConnectionMode.Synchronous);
                }
                catch (Exception ex)
                {
                    args = new ConnectionEventArgs(destination, ex);
                    this.FireConnectionFailed(args);

                    continue;
                }

                if (this.IsConnectedToInternet == true)
                {
                    this.FireAfterConnect(destination);
                    return;
                }

                args = new ConnectionEventArgs(destination, connectionManager.GetConnectionDetailItems());
                this.FireConnectionFailed(args);
            }
        }

        private void FireConnectionFailed(ConnectionEventArgs args)
        {
            var connectionFailed = this.ConnectionFailed;

            if (connectionFailed != null)
            {
                connectionFailed(this, args);
            }
        }

        private void FireBeforeConnect(OpenNETCF.Net.DestinationInfo destination)
        {
            var beforeConnect = this.BeforeConnect;

            if (beforeConnect != null)
            {
                ConnectionEventArgs args = new ConnectionEventArgs(destination);

                beforeConnect(this, args);
            }
        }

        private void FireAfterConnect(OpenNETCF.Net.DestinationInfo destination)
        {
            var afterConnect = this.AfterConnect;

            if (afterConnect != null)
            {
                ConnectionEventArgs args = new ConnectionEventArgs(destination);

                afterConnect(this, args);
            }
        }


        public class ConnectionEventArgs : EventArgs
        {
            OpenNETCF.Net.DestinationInfo destination;
            OpenNETCF.Net.ConnectionDetailCollection connectionDetails;
            Exception exception;

            public OpenNETCF.Net.DestinationInfo Destination
            {
                get { return destination; }
            }
            
            public Exception Exception
            {
                get { return exception; }
            }

            public OpenNETCF.Net.ConnectionDetailCollection ConnectionDetails
            {
                get { return connectionDetails; }
            }

            public ConnectionEventArgs(Exception ex)
            {
                this.exception = ex;
            }

            public ConnectionEventArgs(OpenNETCF.Net.DestinationInfo destination)
            {
                this.destination = destination;
            }

            public ConnectionEventArgs(OpenNETCF.Net.DestinationInfo destination, OpenNETCF.Net.ConnectionDetailCollection connectionDetails)
                : this(destination)
            {
                this.connectionDetails = connectionDetails;
            }

            public ConnectionEventArgs(OpenNETCF.Net.DestinationInfo destination, Exception ex)
                : this(destination)
            {
                this.exception = ex;
            }
        }
    }
}
