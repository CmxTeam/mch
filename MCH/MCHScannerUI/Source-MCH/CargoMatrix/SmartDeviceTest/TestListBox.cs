﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SmoothListbox;
using System.Linq;
using SmoothListbox.ListItems;
using CargoMatrix.UI;
using System.Collections.Generic;
using UIInterface;

namespace SmartDeviceTest.TestApp
{
    public partial class TestListBox : SmoothListbox.SmoothListbox
    {
        public TestListBox()
        {
            InitializeComponent();
            this.IsSelectable = false;
            this.ListItemClicked += new ListItemClickedHandler(TestListBox_ListItemClicked);
        }

        void TestListBox_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            
        }


        public override void LoadControl()
        {
            smoothListBoxMainList.AddItem(new CustomListItems.RenderItems.RenderButtonTest());
        }

    }
}