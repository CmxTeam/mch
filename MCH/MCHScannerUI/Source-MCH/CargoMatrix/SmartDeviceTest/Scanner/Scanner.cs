﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner
{
    public static class Scanner
    {
        private static readonly object lockObject = new object();

        private static EventHandler<ScanEventArgs> currentEvent;
        private static readonly CargoMatrix.UI.BarcodeReader barcodeReader = new CargoMatrix.UI.BarcodeReader();

        public static event EventHandler<ScanEventArgs> BarcodeScanned
        {
            add
            {
                lock (lockObject)
                {
                    if (currentEvent != null &&
                       object.ReferenceEquals(currentEvent.Target, value.Target) == true) return;
                  
                    currentEvent = value;
                }
            }

            remove
            {
                lock (lockObject)
                {
                    if (currentEvent != null &&
                       object.ReferenceEquals(currentEvent.Target, value.Target) == false) return;
                  
                    currentEvent = null;
                    
                }
            }
        }

        static Scanner()
        {
            barcodeReader = new CargoMatrix.UI.BarcodeReader();
            barcodeReader.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcodeReader_BarcodeReadNotify);
        }

        static void barcodeReader_BarcodeReadNotify(string barcodeData)
        {
            var scannedEvent = currentEvent;

            if (scannedEvent != null)
            {
                scannedEvent(barcodeReader, new ScanEventArgs(barcodeData));
            }

            barcodeReader.StartRead();
        }

        public static void ContinueListen(EventHandler<ScanEventArgs> fireEvent)
        {
            BarcodeScanned += fireEvent;
        }

        public static void Start()
        {
            barcodeReader.StartRead();
        }

        public static void Stop()
        {
            barcodeReader.StopRead();
        }
    
    }
}
