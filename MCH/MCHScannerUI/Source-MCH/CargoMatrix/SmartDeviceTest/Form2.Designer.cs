﻿namespace SmartDeviceTest
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnInternetConnect = new System.Windows.Forms.Button();
            this.btnGprsConnect = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnInternetConnect
            // 
            this.btnInternetConnect.Location = new System.Drawing.Point(20, 24);
            this.btnInternetConnect.Name = "btnInternetConnect";
            this.btnInternetConnect.Size = new System.Drawing.Size(126, 20);
            this.btnInternetConnect.TabIndex = 0;
            this.btnInternetConnect.Text = "Start";
            this.btnInternetConnect.Click += new System.EventHandler(this.btnInternetConnect_Click);
            // 
            // btnGprsConnect
            // 
            this.btnGprsConnect.Location = new System.Drawing.Point(20, 62);
            this.btnGprsConnect.Name = "btnGprsConnect";
            this.btnGprsConnect.Size = new System.Drawing.Size(126, 20);
            this.btnGprsConnect.TabIndex = 1;
            this.btnGprsConnect.Text = "Stop";
            this.btnGprsConnect.Click += new System.EventHandler(this.btnGprsConnect_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(20, 102);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(206, 145);
            this.textBox1.TabIndex = 2;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnGprsConnect);
            this.Controls.Add(this.btnInternetConnect);
            this.Menu = this.mainMenu1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnInternetConnect;
        private System.Windows.Forms.Button btnGprsConnect;
        private System.Windows.Forms.TextBox textBox1;
    }
}