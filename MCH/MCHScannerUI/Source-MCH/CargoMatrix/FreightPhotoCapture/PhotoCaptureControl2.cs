﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class PhotoCaptureControl2 : SmoothListBox.UI.SmoothListbox// UserControl
    {
        string m_housebill;
        bool backButtonStatus = false;
        //ListAllReasons reasonsList;
        private SmoothListBox.UI.ListItems.OptionsListITem uploadMenuItem;
        private SmoothListBox.UI.ListItems.OptionsListITem exitwithoutSavingMenuItem;
        private PhotoCaptureControlType m_type = PhotoCaptureControlType.REASONS;
        public enum PhotoCaptureControlType
        { 
            REASONS,
            NO_REASONS
        }
        public PhotoCaptureControl2(string housebill)
        {
            InitializeComponent();
            
            this.miniPhotoCaptureControl1.labelTitle.Height = 0;
            //this.miniPhotoCaptureControl1.Top = panel1.Height;// labelTitle.Height;
            Location = new Point(CargoMatrix.UI.CMXAnimationmanager.GetParent().Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            Size = new Size(CargoMatrix.UI.CMXAnimationmanager.GetParent().Width,
                CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            smoothListBoxMainList.Height = 0;
            smoothListBoxMainList.Visible = false;

            this.TitleText = "Camera Mode";

            uploadMenuItem = new SmoothListBox.UI.ListItems.OptionsListITem(PhotoCaptureResource.Text_SubmitPicture, PhotoCaptureResource.Save_Dis);
            exitwithoutSavingMenuItem = new SmoothListBox.UI.ListItems.OptionsListITem(PhotoCaptureResource.Text_ExitWithoutSaving, PhotoCaptureResource.Symbol_Delete_2_dis);

            AddOptionsListItem(uploadMenuItem);
            AddOptionsListItem(exitwithoutSavingMenuItem);

            uploadMenuItem.Enabled = false;
            exitwithoutSavingMenuItem.Enabled = false;
            pictureBox1.Image = PhotoCaptureResource.Digital_Camera;
            Color thumbnailBackColor;
            using (Bitmap bmp = new Bitmap(PhotoCaptureResource.thumbnailBackColor))
            {
                thumbnailBackColor = bmp.GetPixel(0, 0);
            }
            miniPhotoCaptureControl1.FilmBackColor = thumbnailBackColor;// Color.DarkRed;
            miniPhotoCaptureControl1.DataChangedEvent += new EventHandler(miniPhotoCaptureControl1_DataChangedEvent);
            m_housebill = housebill;
            //labelHouseBill.Text = "Housebill: " + m_housebill;
            
            miniPhotoCaptureControl1.Visible = false;

            
            
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            miniPhotoCaptureControl1.Enabled = m_bOptionsMenu;
            base.pictureBoxMenu_Click(sender, e);
        }
        void miniPhotoCaptureControl1_DataChangedEvent(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            HasImages(miniPhotoCaptureControl1.HasImage);
            
        }

        private void miniPhotoCaptureControl1_ExitNotify(Control sender)
        {
            if (miniPhotoCaptureControl1.HasImage)
            {
                uploadMenuItem.Enabled = true;
                exitwithoutSavingMenuItem.Enabled = true;

                uploadMenuItem.Picture = PhotoCaptureResource.Save;
                exitwithoutSavingMenuItem.Picture = PhotoCaptureResource.Symbol_Delete_2;
            }
            else
            {
                uploadMenuItem.Enabled = false;
                exitwithoutSavingMenuItem.Enabled = false;

                uploadMenuItem.Picture = PhotoCaptureResource.Save_Dis;
                exitwithoutSavingMenuItem.Picture = PhotoCaptureResource.Symbol_Delete_2_dis;
            
            }
            //if (reasonsList == null)
            //    reasonsList = new ListAllReasons();

            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(reasonsList);

        }

        private void PhotoCaptureControl2_MenuItemClicked(SmoothListBox.UI.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            miniPhotoCaptureControl1.Enabled = false;
            if ((listItem as SmoothListBox.UI.ListItems.OptionsListITem).title.Text == PhotoCaptureResource.Text_SubmitPicture)
            {
                UploadPictures();
                
               


            }
            else if ((listItem as SmoothListBox.UI.ListItems.OptionsListITem).title.Text == PhotoCaptureResource.Text_ExitWithoutSaving)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(PhotoCaptureResource.Text_ExitCameraWarning,
                           PhotoCaptureResource.Text_ConfirmExit, CargoMatrix.UI.CMXMessageBoxIcon.Hand,
                           MessageBoxButtons.OKCancel, DialogResult.Cancel))
                {
                
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                }
                


            }
            miniPhotoCaptureControl1.Enabled = true;
        }

        private void PhotoCaptureControl2_EnabledChanged(object sender, EventArgs e)
        {
            
            //string housebill, loc, desc;
            //Cursor.Current = Cursors.WaitCursor;
            //if (Enabled)
            //{
            //    int result = CargoMatrix.Communication.WebServiceManager.Instance().GetHouseBill(m_housebill, out housebill, out desc, out loc);
                
            //    if(result == 1) 
            //    {
            //        //labelHouseBill.Text = "Housebill: " + housebill;
            //        labelHouseBill.Text = housebill;
            //        labelLocation.Text = loc;
            //        labelPiece.Text = desc;
                    
            //    }
            //    else if(result == 0)
            //    {
            //        //labelHouseBill.Text = "Housebill: " + m_housebill;
            //        labelHouseBill.Text = housebill;
            //    }
                
            //    else
            //    {
            //        CargoMatrix.UI.CMXAnimationmanager.GoBack();
            //    }
            //}
            //miniPhotoCaptureControl1.Visible = Enabled;
            //miniPhotoCaptureControl1.Enabled = Enabled;
            //Cursor.Current = Cursors.Default;
        }

        //public void Header(string houseBill, string loc, string desc)
        //{
        //    //reasonsList.ResetSelection();
        //    labelLocation.Text = "Loc: " + loc;
        //    labelHouseBill.Text = "HB: " + houseBill;
        //    labelPiece.Text = "Des: " + desc;
        //    m_housebill = houseBill;
        //}

        public PhotoCaptureControlType ControlType
        {
            set { m_type = value; }
        }

      

       
        void HasImages(bool bImg)
        {

            backButtonStatus = bImg;
            
            uploadMenuItem.Enabled = bImg;
            exitwithoutSavingMenuItem.Enabled = bImg;

            if (bImg)
            {
                uploadMenuItem.Picture = PhotoCaptureResource.Save;
                exitwithoutSavingMenuItem.Picture = PhotoCaptureResource.Symbol_Delete_2;
                pictureBoxBack.Enabled = false;
            }
            else
            {
                uploadMenuItem.Picture = PhotoCaptureResource.Save_Dis;
                exitwithoutSavingMenuItem.Picture = PhotoCaptureResource.Symbol_Delete_2_dis;
                pictureBoxBack.Enabled = true;
            }

            
        
        }
        protected override void SetBackButton()
        {
            base.SetBackButton();
            if (m_bOptionsMenu == false && backButtonStatus == true)
            {
                pictureBoxBack.Enabled = false;
            }
        }

        void UploadPictures()
        {
            Cursor.Current = Cursors.WaitCursor;
            CargoMatrix.Communication.Data.FreightPhotoCapture data = new CargoMatrix.Communication.Data.FreightPhotoCapture();
            data.reference = m_housebill;
            
            
            data.itemsList = new List<CargoMatrix.Communication.Data.FreightPhotoCaptureItem>();
            CargoMatrix.Communication.Data.FreightPhotoCaptureItem item = new CargoMatrix.Communication.Data.FreightPhotoCaptureItem();
            item.imagelist = miniPhotoCaptureControl1.ImageList;
            //item.reason = "No Reason";
            data.itemsList.Add(item);
            //if(UploadPhotoCaptureNotify != null)
            //    UploadPhotoCaptureNotify(data);
            if (CargoMatrix.Communication.WebServiceManager.Instance().UploadPhotoCapture("104", data) == true)
            {
                //if (ExitNotify != null)
                //   ExitNotify();
                CargoMatrix.UI.CMXAnimationmanager.GoBack();

            }
            Cursor.Current = Cursors.Default;
        }

        
       
    }
}
