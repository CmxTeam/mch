﻿using System;


using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;

namespace CargoMatrix.FreightPhotoCapture
{
    class ImagesDBManager
    {
        private static ImagesDBManager m_instance;
        private string imgConnectionString;
        private ImagesDBManager()
        {
            string path;
            path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            imgConnectionString = "Data Source =  " + path + "\\ImagesDB.sdf; Password =''";

            //imgConnectionString = "Data Source = \\Program Files\\cargomatrixscanner\\ImagesDB.sdf; Password =''";
            
        }
        public static bool InsertImage(string UserID, string HousebillNo, Guid GUID, string Reason, DateTime TimeStamp, byte[] Image)
        {

            try
            {
                if (m_instance == null)
                    m_instance = new ImagesDBManager();

                SaveImage(UserID, HousebillNo, GUID, Reason, TimeStamp, Image);
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30016);
                return false;
                
            }
            return true;
        }

        internal static void SaveImage(string UserID, string HousebillNo, Guid GUID, string Reason, DateTime TimeStamp, byte[] Image)
        {
            if (m_instance == null)
                m_instance = new ImagesDBManager();

            using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
            {
                conn.Open();
                using (SqlCeCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Images ([UserID], [HousebillNo], [GUID], [Image], [Reason], [TimeStamp] ) Values(@userID, @housebill, @guid, @pic, @reason, @time )";

                    SqlCeParameter userID = new SqlCeParameter("@userID", SqlDbType.NVarChar);
                    userID.Value = UserID;
                    cmd.Parameters.Add(userID);

                    SqlCeParameter housebill = new SqlCeParameter("@housebill", SqlDbType.NVarChar);
                    housebill.Value = HousebillNo;
                    cmd.Parameters.Add(housebill);

                    SqlCeParameter guid = new SqlCeParameter("@guid", SqlDbType.UniqueIdentifier);
                    guid.Value = GUID;
                    cmd.Parameters.Add(guid);

                    SqlCeParameter pic = new SqlCeParameter("@pic", SqlDbType.Image);
                    pic.Value = Image;
                    cmd.Parameters.Add(pic);

                    SqlCeParameter reason = new SqlCeParameter("@reason", SqlDbType.NVarChar);
                    reason.Value = Reason;
                    cmd.Parameters.Add(reason);

                    SqlCeParameter time = new SqlCeParameter("@time", SqlDbType.DateTime);
                    time.Value = TimeStamp;
                    cmd.Parameters.Add(time);

                    cmd.ExecuteNonQuery();
                }


            }
        }

        public static byte[] GetImageFromDB(Guid GUID)
        {
            byte[] img = null;
            try
            {
                if (m_instance == null)
                    m_instance = new ImagesDBManager();

                DataSet ds = new DataSet("GetImage");

                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "select image from Images where GUID = '" + GUID + "'";
                        SqlCeDataAdapter adapter = new SqlCeDataAdapter(cmd);
                        adapter.Fill(ds);
                        adapter.Dispose();
                        if (ds.Tables.Count > 0)
                        {

                            if (ds.Tables[0].Rows.Count > 0)
                            {

                                DataRow dr = ds.Tables[0].Rows[0];
                                img = dr["Image"] as byte[];

                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30017);
                return null;
            }
        
           return img;
        }

        public static void RemoveImageFromDB(Guid guid)
        {

            
            try
            {
                if (m_instance == null)
                    m_instance = new ImagesDBManager();

                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "delete from Images where GUID = '" + guid + "'";

                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30015);
            }

          

        }

        public static void RemoveImageFromDB(string housebill)
        {

           
            try
            {
                if (m_instance == null)
                    m_instance = new ImagesDBManager();

                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "delete from Images where HousebillNo = '" + housebill + "'";

                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30018);
            }



        }

    }
}
