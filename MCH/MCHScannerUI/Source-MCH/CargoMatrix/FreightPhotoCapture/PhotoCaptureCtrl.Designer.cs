﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class PhotoCaptureControl 
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            //horizontalSmoothListbox1.RemoveAll();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExit = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDelete = new CargoMatrix.UI.CMXPictureButton();
            this.buttonZoom = new CargoMatrix.UI.CMXPictureButton();
            this.cameraTimer = new System.Windows.Forms.Timer();
            this.labelTitle = new System.Windows.Forms.PictureBox();
            this.horizontalSmoothListbox1 = new SmoothListbox.HorizontalSmoothListbox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.White;
            this.buttonExit.Location = new System.Drawing.Point(193, 106);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(45, 43);
            this.buttonExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click_1);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.White;
            this.buttonDelete.Location = new System.Drawing.Point(193, 64);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(45, 42);
            this.buttonDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonZoom
            // 
            this.buttonZoom.BackColor = System.Drawing.Color.White;
            this.buttonZoom.Location = new System.Drawing.Point(193, 21);
            this.buttonZoom.Name = "buttonZoom";
            this.buttonZoom.Size = new System.Drawing.Size(45, 43);
            this.buttonZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonZoom.Click += new System.EventHandler(this.buttonZoom_Click);
            // 
            // cameraTimer
            // 
            this.cameraTimer.Interval = 1000;
            this.cameraTimer.Tick += new System.EventHandler(this.cameraTimer_Tick);
            // 
            // labelTitle
            // 
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.labelTitle.Paint += new System.Windows.Forms.PaintEventHandler(this.labelTitle_Paint);
            // 
            // horizontalSmoothListbox1
            // 
            this.horizontalSmoothListbox1.BackColor = System.Drawing.Color.White;
            this.horizontalSmoothListbox1.Location = new System.Drawing.Point(0, 150);
            this.horizontalSmoothListbox1.Name = "horizontalSmoothListbox1";
            this.horizontalSmoothListbox1.Size = new System.Drawing.Size(240, 53);
            this.horizontalSmoothListbox1.TabIndex = 13;
            this.horizontalSmoothListbox1.ListItemClicked += new SmoothListbox.ListItemClickedHandler(this.horizontalSmoothListbox1_ListItemClicked);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(3, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(188, 128);
            // 
            // PhotoCaptureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonZoom);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.horizontalSmoothListbox1);
            this.Controls.Add(this.panel2);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "PhotoCaptureControl";
            this.Size = new System.Drawing.Size(240, 203);
            this.EnabledChanged += new System.EventHandler(this.MiniPhotoCaptureControl_EnabledChanged);
            this.ResumeLayout(false);

        }

        #endregion

        //private CMXColorCameraControl cmxCameraControl1;
        
        private SmoothListbox.HorizontalSmoothListbox horizontalSmoothListbox1;
        private CargoMatrix.UI.CMXPictureButton buttonExit;
        private CargoMatrix.UI.CMXPictureButton buttonDelete;
        private CargoMatrix.UI.CMXPictureButton buttonZoom;
        public System.Windows.Forms.PictureBox labelTitle;
        public System.Windows.Forms.Timer cameraTimer;
        private System.Windows.Forms.Panel panel2;
    }
}
