﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using System.Threading;
using System.Data.SqlServerCe;
using OpenNETCF.Drawing.Imaging;

namespace CargoMatrix.FreightPhotoCapture
{

    public partial class PhotoCaptureControl : UserControl
    {
        Thread m_thread;
        public string HouseBill;
        public event EventHandler DataChangedEvent;
        public event ExitEventHandler ExitNotify;
        public string Header;
        public ImageReadNotifyHandler ImageReadNotifyEvent;
        private string reference;
        private string reason;
        private CMXCameraBase cmxCameraControl1;
        Font titleFont = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
        CustomListItems.Thumbnail selectedThumbnail;



        public PhotoCaptureControl(string reference, string reason)
        {
            this.reference = reference;
            this.reason = reason;
            InitPhotoCapture();
            this.ImageReadNotifyEvent += new ImageReadNotifyHandler(ImageReadNotify);
            using (Bitmap bmp = new Bitmap(global::Resources.Graphics.Skin.thumbnailBackColor))
            {
                horizontalSmoothListbox1.BackColor = bmp.GetPixel(0, 0);
            }
        }

        private void InitPhotoCapture()
        {
            InitializeComponent();
            buttonDelete.Image = FreightPhotoCapture.PhotoCaptureResource.cc_trash;
            buttonDelete.DisabledImage = FreightPhotoCapture.PhotoCaptureResource.cc_trash_dis;
            buttonDelete.PressedImage = FreightPhotoCapture.PhotoCaptureResource.cc_trash_over;

            buttonZoom.Image = FreightPhotoCapture.PhotoCaptureResource.cc_zoom;//.FullScreen_disable;
            buttonZoom.DisabledImage = FreightPhotoCapture.PhotoCaptureResource.cc_zoom_dis;//.FullScreen_disable;
            buttonZoom.PressedImage = FreightPhotoCapture.PhotoCaptureResource.cc_zoom_over;//.FullScreen_disable;
            buttonExit.Image = FreightPhotoCapture.PhotoCaptureResource.cc_notes;
            buttonExit.PressedImage = FreightPhotoCapture.PhotoCaptureResource.cc_notes_over;
            buttonExit.DisabledImage = FreightPhotoCapture.PhotoCaptureResource.cc_notes_dis;
            //this.Enabled = false;
            labelTitle.Image = global::Resources.Graphics.Skin.popup_header;
            buttonDelete.Enabled = false;
            buttonZoom.Enabled = false;

        }

        private void ImageReadNotify(System.IO.MemoryStream stream)
        {
            try
            {
                if (stream == null)
                {
                    if (selectedThumbnail != null)
                    {
                        horizontalSmoothListbox1.Reset();
                    }
                    EnableButtons(false);

                }
                else
                {
                    selectedThumbnail = new CustomListItems.Thumbnail();

                    IBitmapImage imageBitmap = CreateThumbnail(stream, new Size(64, 48));
                    selectedThumbnail.image = ImageUtils.IBitmapImageToBitmap(imageBitmap);


                    //selectedThumbnail.Time = DateTime.Now;
                    //selectedThumbnail.image = new Bitmap(image);
                    horizontalSmoothListbox1.AddItem(selectedThumbnail);
                    selectedThumbnail.SelectedChanged(true);
                    horizontalSmoothListbox1.MoveListToEnd();

                    EnableButtons(true);
                    cameraTimer.Enabled = true;
                    horizontalSmoothListbox1.Refresh();
                    Cursor.Current = Cursors.Default;

                    if (DataChangedEvent != null)
                        DataChangedEvent(this, EventArgs.Empty);


                    DateTime timeStamp = DateTime.Now;
                    selectedThumbnail.Time = timeStamp.ToString("MM/dd/yyyy hh:mm:ss tt");
                    ImagesDBManager.SaveImage(CargoMatrix.Communication.WebServiceManager.GetUserID(), reference, Guid.NewGuid(), reason, timeStamp, stream.ToArray());

                }
            }

            catch (Exception e)
            {
                if (selectedThumbnail != null)
                {
                    selectedThumbnail.Dispose();
                    selectedThumbnail = null;
                }

                cmxCameraControl1.Enabled = false;
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 40002);
                cmxCameraControl1.Enabled = true;
            }

        }

        private void buttonCaptureSave_Click(object sender, EventArgs e)
        {
        }


        void UpdateImage()
        {
            this.Invoke(new EventHandler(WorkerUpdateImage));


        }
        public void WorkerUpdateImage(object sender, EventArgs e)
        {
            try
            {

                CargoMatrix.Communication.DTO.ImageObject imgObj = CargoMatrix.Communication.WebServiceManager.Instance().GetFullImage("", selectedThumbnail.ID);
                if (imgObj.m_rawImage != null)
                {
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream(imgObj.m_rawImage))
                    {
                        if (cmxCameraControl1.image != null)
                        {
                            cmxCameraControl1.image = null; // this will dispose the image too
                        }
                        cmxCameraControl1.image = new Bitmap(stream);
                    }
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30013);

                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn() == false)
                {
                    cmxCameraControl1.Enabled = false;

                }
            }
        }

        private void horizontalSmoothListbox1_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (isSelected)
            {
                selectedThumbnail = listItem as CustomListItems.Thumbnail;
                cmxCameraControl1.StopAcquisition();
                //cmxCameraControl1.Enabled = false;

                cmxCameraControl1.image = selectedThumbnail.image;
                cmxCameraControl1.Update();
                if (selectedThumbnail.ID > 0)
                {
                    ThreadStart starter = new ThreadStart(UpdateImage);
                    m_thread = new Thread(starter);
                    m_thread.Priority = ThreadPriority.Normal;//.BelowNormal;
                    //m_thread.IsBackground = true;

                    m_thread.Start();
                }
                else
                {

                    byte[] img = ImagesDBManager.GetImageFromDB(selectedThumbnail.GUID);
                    if (img != null)
                    {
                        using (System.IO.MemoryStream fs = new System.IO.MemoryStream(img))
                        {
                            if (cmxCameraControl1.image != null)
                            {
                                //    cmxCameraControl1.image.Dispose();
                                cmxCameraControl1.image = null;
                            }
                            IBitmapImage imageBitmap = CreateThumbnail(fs, new Size(640, 480));
                            cmxCameraControl1.image = ImageUtils.IBitmapImageToBitmap(imageBitmap);

                        }
                    }
                }

                cameraTimer.Enabled = false;
                EnableButtons(true);
            }
            else
            {
                StartCamera();
            }
        }

        public bool LeftButtonPressed
        {
            set
            {
                horizontalSmoothListbox1.leftButtonPressed = value;
            }

        }

        public bool RightButtonPressed
        {
            set
            {
                horizontalSmoothListbox1.rightButtonPressed = value;
            }

        }


        protected virtual void buttonExit_Click_1(object sender, EventArgs e)
        {
            buttonExit.Refresh();
            cameraTimer.Enabled = false;
            cmxCameraControl1.StopAcquisition();
            if (m_thread != null)
            {
                m_thread.Abort();
                m_thread = null;

            }
            if (ExitNotify != null)
                ExitNotify(this);
        }
        private void EnableButtons(bool enable)
        {
            buttonDelete.Enabled = buttonZoom.Enabled = enable;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            buttonDelete.Refresh();
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to delete this picture ?", "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Cancel))
                return;

            if (selectedThumbnail != null)
            {
                if (selectedThumbnail.ID == -1)
                {
                    ImagesDBManager.RemoveImageFromDB(selectedThumbnail.GUID);
                    horizontalSmoothListbox1.RemoveItem(selectedThumbnail);
                    selectedThumbnail.image.Dispose();
                    selectedThumbnail.image = null;
                    selectedThumbnail.Dispose();
                    selectedThumbnail = null;

                }
                else
                {

                    if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                    {
                        this.Enabled = false;
                        if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(this, "Delete Picture") == DialogResult.OK)
                        {
                            if (selectedThumbnail.image != null)
                            {
                                selectedThumbnail.image.Dispose();
                                selectedThumbnail.image = null;
                            }
                            selectedThumbnail.Visible = false;
                            selectedThumbnail.Width = selectedThumbnail.Height = 0;

                            if (DataChangedEvent != null)
                                DataChangedEvent(this, EventArgs.Empty);
                        }
                        this.Enabled = true;

                        if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn())
                            cmxCameraControl1.Enabled = true;
                    }
                    else
                    {
                        if (selectedThumbnail.image != null)
                        {
                            selectedThumbnail.image.Dispose();
                            selectedThumbnail.image = null;
                        }
                        selectedThumbnail.Visible = false;
                        selectedThumbnail.Width = selectedThumbnail.Height = 0;

                        if (DataChangedEvent != null)
                            DataChangedEvent(this, EventArgs.Empty);

                    }


                }


                horizontalSmoothListbox1.MoveListToEnd();

                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn())
                    StartCamera();

            }

        }

        private void buttonCameraMode_Click(object sender, EventArgs e)
        {
            cmxCameraControl1.StartAcquisition();
        }

        private void buttonNote_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        private void buttonZoom_Click(object sender, EventArgs e)
        {
            buttonZoom.Refresh();
            GC.Collect();
            cmxCameraControl1.FullScreen(true);

        }
        public void StartCamera()
        {
            cameraTimer.Enabled = false;
            if (cmxCameraControl1 != null)
            {
                if (cmxCameraControl1.Enabled == false)
                    cmxCameraControl1.Enabled = true;
                cmxCameraControl1.StartAcquisition();
            }
        }
        public void StopCamera()
        {
            if (cmxCameraControl1 != null)
                cmxCameraControl1.StopAcquisition();
        }

        public bool HasImage
        {
            get
            {
                return this.Image != null;
            }
        }

        public Image Image
        {
            get
            {
                if (horizontalSmoothListbox1.Items.Count > 0)
                    for (int i = 0; i < horizontalSmoothListbox1.Items.Count; i++)
                    {
                        if ((horizontalSmoothListbox1.Items[i] as CustomListItems.Thumbnail).image != null)
                            return (horizontalSmoothListbox1.Items[i] as CustomListItems.Thumbnail).image;
                    }

                return null;
            }
        }

        public List<ImageObject> UploadImageList
        {
            get
            {
                List<ImageObject> list;
                System.IO.MemoryStream imageStream;// = new System.IO.MemoryStream();
                if (horizontalSmoothListbox1.Items.Count > 0)
                {
                    list = new List<ImageObject>();
                    foreach (CustomListItems.Thumbnail temp in horizontalSmoothListbox1.Items)
                    {
                        try
                        {
                            imageStream = new System.IO.MemoryStream();

                            if (temp.ID == -1)
                            {
                                byte[] img = ImagesDBManager.GetImageFromDB(temp.GUID);

                                imageStream = new System.IO.MemoryStream(img);
                                list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                imageStream.Close();
                            }
                            else
                            {
                                if (temp.image == null)
                                {
                                    imageStream = new System.IO.MemoryStream();
                                    list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                    imageStream.Close();
                                }

                            }
                        }
                        catch (Exception e)
                        {
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30004);
                        }
                    }
                    return list;
                }
                return null;
            }

        }
        public List<ImageObject> GetImageListAndDispose
        {
            get
            {
                List<ImageObject> list;
                System.IO.MemoryStream imageStream;// = new System.IO.MemoryStream();
                if (horizontalSmoothListbox1.Items.Count > 0)
                {
                    list = new List<ImageObject>();
                    foreach (CustomListItems.Thumbnail temp in horizontalSmoothListbox1.Items)
                    {
                        try
                        {
                            GC.Collect();

                            if (temp.image != null)
                            {
                                imageStream = new System.IO.MemoryStream();
                                temp.image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                imageStream.Close();
                            }
                            else
                                list.Add(new ImageObject(null, temp.Time, null, null, null, temp.ID));
                        }
                        catch (Exception e)
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30005)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30005);
                        }

                        horizontalSmoothListbox1.RemoveItem(temp);
                        temp.Dispose();
                        //temp = null;

                    }
                    return list;
                }
                return null;
            }
        }

        public List<ImageObject> ImageList
        {
            get
            {
                List<ImageObject> list;
                System.IO.MemoryStream imageStream;// = new System.IO.MemoryStream();
                if (horizontalSmoothListbox1.Items.Count > 0)
                {
                    list = new List<ImageObject>();
                    GC.Collect();
                    foreach (CustomListItems.Thumbnail temp in horizontalSmoothListbox1.Items)
                    {
                        try
                        {
                            if (temp.image != null)
                            {
                                imageStream = new System.IO.MemoryStream();
                                temp.image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                imageStream.Close();
                            }
                            else
                                list.Add(new ImageObject(null, temp.Time, null, null, null, temp.ID));
                        }
                        catch (Exception e)
                        {
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30006);
                        }

                    }
                    return list;

                }
                return null;
            }
            set
            {
                horizontalSmoothListbox1.RemoveAll();
                if (value != null)
                {
                    for (int i = 0; i < value.Count; i++)
                    {
                        horizontalSmoothListbox1.AddItem(new CustomListItems.Thumbnail(value[i]));
                    }

                    horizontalSmoothListbox1.MoveListToEnd();
                    horizontalSmoothListbox1.Reset();
                }

            }

        }
        private void cameraTimer_Tick(object sender, EventArgs e)
        {
            cameraTimer.Enabled = false;
            StartCamera();
        }

        public string Title
        {
            set { Header = value; }
        }

        

        private void labelTitle_Paint(object sender, PaintEventArgs e)
        {
            SizeF textSize = e.Graphics.MeasureString(Header, titleFont);
            int x = (labelTitle.Width - (int)textSize.Width) / 2;
            int y = (labelTitle.Height - (int)textSize.Height) / 2;
            e.Graphics.DrawString(Header, titleFont, new SolidBrush(Color.White), x, y);
        }
        public Color FilmBackColor
        {

            set
            {
                horizontalSmoothListbox1.BackColor = value;
                //base.BackColor = value;
            }
        }
        //Rectangle cameraBounds;
        private void MiniPhotoCaptureControl_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled)
            {
                if (cmxCameraControl1 != null)
                    return;

                this.cmxCameraControl1 = new CMXColorCameraControl();

                this.SuspendLayout();
                cmxCameraControl1.ImageReadNotify += ImageReadNotifyEvent;

                this.cmxCameraControl1.BackColor = System.Drawing.Color.White;
                this.cmxCameraControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.cmxCameraControl1.Name = "cmxCameraControl1";
                this.cmxCameraControl1.Bounds = panel2.Bounds;
                this.cmxCameraControl1.TabIndex = 9;

                this.Controls.Add(this.cmxCameraControl1);
                cmxCameraControl1.BringToFront();
                this.ResumeLayout();
            }
            else
            {
                if (m_thread != null)
                {
                    m_thread.Abort();
                    m_thread = null;

                }

                if (cmxCameraControl1 != null)
                {

                    cmxCameraControl1.Enabled = false;
                    cmxCameraControl1.ImageReadNotify -= ImageReadNotifyEvent;
                    if (Controls.Contains(cmxCameraControl1))
                        Controls.Remove(cmxCameraControl1);

                    cmxCameraControl1.Dispose();
                    cmxCameraControl1 = null;

                }

            }
        }
        private IBitmapImage CreateThumbnail(System.IO.Stream stream, Size size)
        {
            IBitmapImage imageBitmap;
            ImageInfo ii;
            IImage image;

            ImagingFactory factory = new ImagingFactoryClass();
            factory.CreateImageFromStream(new StreamOnFile(stream), out image);
            image.GetImageInfo(out ii);
            factory.CreateBitmapFromImage(image, (uint)size.Width, (uint)size.Height,
          ii.PixelFormat, InterpolationHint.InterpolationHintDefault, out imageBitmap);
            return imageBitmap;
        }

        public void SetColorCameraResolution()
        {
            //if (cmxCameraControl1 != null)
            //{
            //    if (cmxCameraControl1 is CMXColorCameraControl)
            //    {
            //        if (m_parent != null)
            //        {
            //            // m_parent.ColorCameraResolution = resolution;
            //            (cmxCameraControl1 as CMXColorCameraControl).CameraResolution = m_parent.ColorCameraResolution;
            //        }
            //    }
            //}
        }

    }
}
