﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class AddNewMasterbill
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonClear = new OpenNETCF.Windows.Forms.Button2();
            this.buttonDamage = new OpenNETCF.Windows.Forms.Button2();
            this.label2 = new System.Windows.Forms.Label();
            this.cmxTextBox2 = new CargoMatrix.UI.CMXTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmxTextBox1
            // 
            this.cmxTextBox1.Location = new System.Drawing.Point(2, 139);
            this.cmxTextBox1.Name = "cmxTextBox1";
            this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBox1.Size = new System.Drawing.Size(67, 28);
            this.cmxTextBox1.TabIndex = 0;
            this.cmxTextBox1.Click += new System.EventHandler(this.cmxTextBox1_Click);
            this.cmxTextBox1.GotFocus += new System.EventHandler(this.cmxTextBox1_GotFocus);
            this.cmxTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
            this.cmxTextBox1.EnabledChanged += new System.EventHandler(this.cmxTextBox1_EnabledChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 17);
            this.label1.Text = "Scan label or Enter code to start";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(8, -7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(220, 154);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // buttonClear
            // 
            this.buttonClear.ActiveBackColor = System.Drawing.Color.White;
            this.buttonClear.ActiveBorderColor = System.Drawing.Color.White;
            this.buttonClear.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonClear.BorderColor = System.Drawing.Color.White;
            this.buttonClear.DisabledBackColor = System.Drawing.Color.White;
            this.buttonClear.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.Location = new System.Drawing.Point(8, 172);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(88, 60);
            this.buttonClear.TabIndex = 4;
            this.buttonClear.BackgroundImage = CargoMatrix.Resources.Skin.button_default;
            this.buttonClear.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            this.buttonClear.Text = "Clear";
            this.buttonClear.TextAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleCenter;
            this.buttonClear.TransparentImage = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);

            // 
            // buttonDamage
            // 
            this.buttonDamage.ActiveBackColor = System.Drawing.Color.White;
            this.buttonDamage.ActiveBorderColor = System.Drawing.Color.White;
            this.buttonDamage.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonDamage.BorderColor = System.Drawing.Color.White;
            this.buttonDamage.DisabledBackColor = System.Drawing.Color.White;
            this.buttonDamage.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonDamage.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDamage.Location = new System.Drawing.Point(140, 172);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.Size = new System.Drawing.Size(88, 60);
            this.buttonDamage.TabIndex = 3;
            this.buttonDamage.BackgroundImage = CargoMatrix.Resources.Skin.button_default;
            this.buttonDamage.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            this.buttonDamage.Text = "Reasons";
            this.buttonDamage.TextAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleCenter;
            this.buttonDamage.TransparentImage = true;
            this.buttonDamage.Click += new System.EventHandler(this.buttonDamage_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(2, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 12);
            this.label2.Text = "Carrier:";
            // 
            // cmxTextBox2
            // 
            this.cmxTextBox2.Location = new System.Drawing.Point(75, 139);
            this.cmxTextBox2.Name = "cmxTextBox2";
            this.cmxTextBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBox2.Size = new System.Drawing.Size(159, 28);
            this.cmxTextBox2.TabIndex = 1;
            this.cmxTextBox2.GotFocus += new System.EventHandler(this.cmxTextBox2_GotFocus);
            this.cmxTextBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox2_KeyDown);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(75, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 12);
            this.label3.Text = "Masterbill:";
            // 
            // AddNewMasterbill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.BarcodeEnabled = true;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmxTextBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmxTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDamage);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AddNewMasterbill";
            this.Size = new System.Drawing.Size(240, 240);
            this.LoadOptionsMenu += new System.EventHandler(this.AddNewMasterbill_LoadOptionsMenu);
            this.GotFocus += new System.EventHandler(this.AddNewMasterbill_GotFocus);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(this.NewPhotoCaptureTask_BarcodeReadNotify);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(this.AddNewMasterbill_MenuItemClicked);
            this.EnabledChanged += new System.EventHandler(this.NewPhotoCaptureTask_EnabledChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AddNewMasterbill_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private OpenNETCF.Windows.Forms.Button2 buttonClear;
        private OpenNETCF.Windows.Forms.Button2 buttonDamage;
        public CargoMatrix.UI.CMXTextBox cmxTextBox1;
        private System.Windows.Forms.Label label2;
        public CargoMatrix.UI.CMXTextBox cmxTextBox2;
        private System.Windows.Forms.Label label3;
    }
}
