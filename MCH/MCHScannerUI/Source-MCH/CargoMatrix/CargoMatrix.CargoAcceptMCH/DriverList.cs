﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.IO;
using System.Data;
using System.Reflection;
//using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CargoMatrix.Communication.WSScannerMCHService;

namespace CargoMatrix.CargoAccept
{
    public partial class DriverList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;

        public DriverList()
        {

            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(FlightList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(FlightList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(FlightList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FlightList_ListItemClicked);
        }

        protected virtual void FlightList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

                Item_Enter_Click(listItem, null);


        }
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;

            if (ShowFilterPopup() == false)
                BarcodeEnabled = true;
        }
        void FlightList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Flight);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter + " / " + sort;
                LoadControl();
                return true;
            }
            return false;
        }

        void FlightList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
        }

        public override void LoadControl()
        {
           
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoAccept - {0}", smoothListBoxMainList.VisibleItemsCount);
        }



        //void Flight_OnMoreClick(object sender, EventArgs e)
        //{
        //    Cursor.Current = Cursors.WaitCursor;
        //    var mawb = (sender as FlightCargoItem).ItemData;
        //    Cursor.Current = Cursors.Default;
        //}


        protected virtual void Item_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            DataRow dr = (sender as DriverCargoItem).ItemData;
            Cursor.Current = Cursors.Default;
            //MessageBox.Show("image " + dr["driver"].ToString());
          
        }
 

        protected virtual void Item_Enter_Click(object sender, EventArgs e)
        {
            barcode.StopRead();
            Cursor.Current = Cursors.WaitCursor;
            DataRow drDriver = (sender as DriverCargoItem).ItemData;
            Cursor.Current = Cursors.Default;

            long taskId = long.Parse(drDriver["TaskId"].ToString());


            if (drDriver["receivelocation"].ToString() != "N/A" && drDriver["receivelocation"].ToString() != "")
            {


                if (drDriver["StatusId"].ToString() == "2")
                {
                    CargoMatrix.UI.CMXMessageBox.Show("This task is finalized.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ShipmentList(drDriver));
                return;
            }

            string enteredLocation = string.Empty;
                bool isFPC = false;
                if (DoLocation(out enteredLocation, out isFPC, "Location", false))
                {
                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                    if (locationId == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }

                    string truckNumber = string.Empty;
                    string sealType = string.Empty;
                    string sealNumber = string.Empty;


                    if (DialogResult.OK == ScreeningMessageBox.Show(ref truckNumber, ref sealType, ref sealNumber))
                    {

                        DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("exec dbo.GetScreeningSealtypes");

                        int sealTypeId = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["SealType"].ToString().ToUpper() == sealType.ToUpper())
                            {
                                sealTypeId = int.Parse(dt.Rows[i]["ID"].ToString());
                                CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_StartAcceptTask(taskId, (int)locationId, truckNumber, sealTypeId, sealNumber);
                                break;
                            }
                        }


   

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ShipmentList(drDriver));

                   
 

                        LoadControl();
                    }
                    else
                    {
                        barcode.StartRead();
                    }
 


                }
                else
                { 
                 barcode.StartRead();
                }
               


           
        }

 

        void FlightList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            CargoMatrix.Communication.WSCargoReceiver.MasterBillItem tempMawb = null;
            switch (barcode.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    break;
                case BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    break;
                default:
                    CargoMatrix.UI.CMXMessageBox.Show("CargoAccept", "Invalid Barcode", CMXMessageBoxIcon.Hand);
                    break;
            }

        }



        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title,long taskId)
        {

            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(taskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }




    }

}
