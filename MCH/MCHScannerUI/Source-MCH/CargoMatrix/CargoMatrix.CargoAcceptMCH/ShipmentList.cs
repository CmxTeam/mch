﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Collections.Generic;
using System.Data;
namespace CargoMatrix.CargoAccept
{
    public partial class ShipmentList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;
        private long TaskId;
        private DataRow DriverTask;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
         
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;
        
        public ShipmentList(DataRow driverTask)
        {

             
            this.DriverTask = driverTask;

            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(List_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(List_ListItemClicked);
        }

   



    


  
  
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;

            if (ShowFilterPopup() == false)
                BarcodeEnabled = true;
        }




        public byte[] Base64ToByte(string base64String)
        {
            try
            {

                byte[] imageBytes = Convert.FromBase64String(base64String.Split(',')[1]);

                return imageBytes;
            }
            catch
            {
                return null;
            }


        }

        bool Signature()
        {

            DataTable dtSignature = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(string.Format("exec GetCargoAcceptTaskById {0}", TaskId));



            SignatureCaptureForm signatureForm = new SignatureCaptureForm();


            byte[] driverImage = null;


            if (dtSignature.Rows.Count > 0)
            {
                driverImage = Base64ToByte(dtSignature.Rows[0]["driverimage"].ToString());
            }


            if (driverImage != null)
            {
                signatureForm.DriverPhotoAsArray = driverImage;
            }

            signatureForm.Header = "Finalize";
            signatureForm.DriversName = dtSignature.Rows[0]["driver"].ToString();
            signatureForm.CompanyName = dtSignature.Rows[0]["company"].ToString();

            try
            {
                signatureForm.Pieces = int.Parse(dtSignature.Rows[0]["receivedpieces"].ToString());
            }
            catch
            {
                signatureForm.Pieces = 0;
            }
  

            Cursor.Current = Cursors.Default;

            DialogResult result = signatureForm.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                Cursor.Current = Cursors.WaitCursor;

                byte[] signature = signatureForm.SignatureAsBytes;


                CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_FinalizeAccept(TaskId, Convert.ToBase64String(signature));

                Cursor.Current = Cursors.Default;

                CargoMatrix.UI.CMXAnimationmanager.GoBack();
                return true;
            }
            return false;
        }


        void List_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;

                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK:

                        int fl = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_GetForkliftCounter(TaskId);
                        if (fl > 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Please drop all pieces from your forklift before you finalize.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to finalize?", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                            {
                                if (Signature())
                                {
                                    return;
                                }

                            }
                        }

 


                        break;
                    
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Uld);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter;
                LoadControl();
                return true;
            }
            return false;
        }

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {

            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));


            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter  };
            this.AddOptionsListItem(filterOption);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

      
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("exec dbo.GetCargoAcceptTaskById " + DriverTask["TaskId"].ToString());


            this.TaskId = long.Parse(DriverTask["TaskId"].ToString());

            this.label1.Text = dt.Rows[0]["driver"].ToString() + " - " + dt.Rows[0]["receivelocation"].ToString();
            this.label2.Text = string.Format("Awbs: {0} of {1}  Pcs: {2} of {3}", dt.Rows[0]["ReceivedAWBCount"].ToString(), dt.Rows[0]["AWBCount"].ToString(), dt.Rows[0]["receivedpieces"].ToString(), dt.Rows[0]["totalpieces"].ToString());
            this.label3.Text = string.Format("Progress: {0:0}%", double.Parse(dt.Rows[0]["progress"].ToString()));



            switch (int.Parse(dt.Rows[0]["StatusId"].ToString()))
            {
                case 5:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case 3:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case 2:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }






            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;


            DataTable dtAwbs = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("exec dbo.GetCargoAcceptAWBs " + DriverTask["TaskId"].ToString());
            if (dtAwbs != null)
            {
                foreach (DataRow dr in dtAwbs.Rows)
                {
                    ShipmentCargoItem tempShipment = new ShipmentCargoItem(dr);
                    tempShipment.ButtonClick += new EventHandler(Item_OnMoreClick);
                    smoothListBoxMainList.AddItem(tempShipment);
                }
            }





            this.TitleText = string.Format("CargoAccept - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
         
            Cursor.Current = Cursors.Default;

 
           

        }


    
        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoAccept - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
        }


        void Item_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            DataRow tempItemData = (sender as ShipmentCargoItem).ItemData;
            Cursor.Current = Cursors.Default;
            MessageBox.Show(tempItemData["AwbRef"].ToString());
        }




        protected virtual void List_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            ShipmentCargoItem tempCargoItem = (ShipmentCargoItem)listItem;
            DataRow tempData = (DataRow)tempCargoItem.ItemData;
            //MessageBox.Show(tempData["AwbRef"].ToString());

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new PieceList(tempData, long.Parse(DriverTask["TaskId"].ToString())));

        }



        string ValidateShipment(string value)
        { 
            if(value==string.Empty)
            {
                return string.Empty;
            }

            value = value.Replace("-","");
            value = value.Replace(" ", "");

            try
            {
                long longnumber = long.Parse(value);

                string carrier = value.Substring(0, 3);
                string awb = value.Substring(3, 8);
                return carrier + "-"+ awb;
            }
            catch
            {
                return string.Empty;
            }
        
 
        }

        void MawbList_BarcodeReadNotify(string barcodeData)
        {

 
            string shipment = ValidateShipment(barcodeData);
            if (shipment != string.Empty)
            {

                for (int i = 0; i < smoothListBoxMainList.ItemsCount; i++)
                {
                    ShipmentCargoItem tempCargoItem = (ShipmentCargoItem)smoothListBoxMainList.Items[i];
                    DataRow tempData = (DataRow)tempCargoItem.ItemData;
                    if (tempData["AwbRef"].ToString().Contains(shipment))
                    {
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new PieceList(tempData, long.Parse(DriverTask["TaskId"].ToString())));
                    }
                }
 
            }
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }

   

    }

}
