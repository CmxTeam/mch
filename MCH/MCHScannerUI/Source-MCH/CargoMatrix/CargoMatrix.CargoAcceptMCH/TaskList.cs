﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CMXBarcode;
using SmoothListbox;
using System.Linq;
using System.Data;
namespace CargoMatrix.CargoAccept
{
    public class TaskList : DriverList
    {
        public TaskList()
            : base()
        {
            this.Name = "TaskList";
        }

        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();

            Cursor.Current = Cursors.WaitCursor;
            this.label1.Text = string.Format("{0}/{1}", filter, sort);
            this.searchBox.Text = string.Empty;
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;

            //TaskStatuses status = TaskStatuses.All;
            //switch (filter)
            //{
            //    case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
            //        status = TaskStatuses.Pending;
            //        break;
            //    case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
            //        status = TaskStatuses.InProgress;
            //        break;
            //    case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
            //        status = TaskStatuses.Completed;
            //        break;
            //    case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
            //        status = TaskStatuses.Open;
            //        break;
            //    default:
            //        status = TaskStatuses.All;
            //        break;
            //}


            //All-1, Completed-2, InProgress-3, Open-4, Pending-5
            int status = 1;
            switch (filter)
            {
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                    status = 5;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                    status = 3;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                    status = 2;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                    status = 4;
                    break;
                default:
                    status = 1;
                    break;
            }

            ReceiverSortFields sortfield = ReceiverSortFields.Carrier;
            switch (sort)
            {
                case CargoMatrix.Communication.DTO.FlightSorts.ORIGIN:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.CARRIER:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.FLIGHTNO:
                    sortfield = ReceiverSortFields.FlightNo;
                    break;
                default:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
            }




            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_GetTasks(status);
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DriverCargoItem tempRow = new DriverCargoItem(dr);
                    tempRow.OnEnterClick += new EventHandler(Item_Enter_Click);
                    tempRow.ButtonClick += new EventHandler(Item_OnMoreClick);
                    smoothListBoxMainList.AddItem(tempRow);
                }
            }



            this.TitleText = string.Format("CargoAccept - ({0})", smoothListBoxMainList.VisibleItemsCount);
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

    


        protected override void FlightList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
                MessageBox.Show("This feature is not available");
            else
                base.FlightList_ListItemClicked(sender, listItem, isSelected);
        }

        protected override void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoAccept - ({0})", smoothListBoxMainList.VisibleItemsCount);
        }

    }
}
