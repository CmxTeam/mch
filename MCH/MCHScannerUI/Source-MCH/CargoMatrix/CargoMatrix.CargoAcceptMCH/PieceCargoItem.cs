﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using System.Data;
namespace CargoMatrix.CargoAccept
{
    public partial class PieceCargoItem : CustomListItems.ExpandableRenderListitem<DataRow>, ISmartListItem
    {

        //public event EventHandler ButtonClick;

        public PieceCargoItem(DataRow piecedata)
            : base(piecedata)
        {
            InitializeComponent();
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        //void buttonBrowse_Click(object sender, System.EventArgs e)
        //{
        //    if (ButtonClick != null)
        //    {
        //        ButtonClick(this, EventArgs.Empty);
        //    }
        //}
        protected override void InitializeControls()
        {
            this.SuspendLayout();
            //this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            //this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            //this.buttonBrowse.Location = new System.Drawing.Point(202, 6);
            //this.buttonBrowse.Name = "btnMore";
            //this.buttonBrowse.Image = Resources.Skin.magnify_btn;
            //this.buttonBrowse.PressedImage = Resources.Skin.magnify_btn_over;
            //this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            //this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            //this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);
            //this.Controls.Add(this.buttonBrowse);
            //this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width/96F,CurrentAutoScaleDimensions.Height/96F));
            this.ResumeLayout(false);
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("Type: {0}  Ref:{1}", ItemData["ULDType"].ToString(), ItemData["ULDNumber"].ToString());
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {

                    string line2 = string.Format("Pcs: {0}  Wgt: {1}{2}", ItemData["Pieces"].ToString(), ItemData["Weight"].ToString(),ItemData["WeightUOM"].ToString());
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 16 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {
                    string Screened = "NO";
                    if(ItemData["Screened"].ToString().ToUpper()=="TRUE")
                    {
                        Screened ="YES";
                    }


                    string line3 = string.Format("Screened: {0}", Screened);
                    //string line3 = string.Format("Screened: {0}  Loc: {1}",Screened , ItemData["LOC"].ToString());
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 30 * vScale, 170 * hScale, 13 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.PieceMode;
                //switch (ItemData.Status)
                //{
                //    case FlightStatus.Open:
                //    case FlightStatus.Pending:
                //        img = CargoMatrix.Resources.Skin.Clipboard;
                //        break;
                //    case FlightStatus.InProgress:
                //        img = CargoMatrix.Resources.Skin.status_history;
                //        break;
                //    case FlightStatus.Completed:
                //        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                //        break;

                //}

                
                //object O = Resources.Airlines.GetImage(ItemData.CarrierCode);
                //if (O != null)
                //{
                //    Image logo = (Image)O;
                //    //gOffScreen.DrawImage(logo, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                //    gOffScreen.DrawImage(logo, new Rectangle((int)(202 * hScale), (int)(6 * vScale), (int)(34 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                
                //}


              


                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = ItemData["UldNumber"].ToString();
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
