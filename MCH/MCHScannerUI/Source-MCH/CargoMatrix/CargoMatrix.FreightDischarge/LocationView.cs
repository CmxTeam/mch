﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoDischarge;
using CargoMatrix.Communication.ScannerUtilityWS;
using CargoMatrix.Communication;
using CMXExtensions;

namespace CargoMatrix.FreightDischarge
{
    public partial class LocationView : CargoMatrix.Utilities.MessageListBox
    {
        bool previewOnly;
        int forkliftID;
        IDischargeHouseBillItem houseBillItem;
        LocationItem[] locations;

        List<int> selectedPiecesIDs;
        List<int> frokliftPiecesIDs;

        bool wasScanned = false;
       
        public bool WasScanned
        {
            get { return wasScanned; }
        }

   


        public IDischargeHouseBillItem HouseBillItem
        {
            get { return houseBillItem; }
        }

        public LocationView(IDischargeHouseBillItem houseBillItem, int forkliftID, bool previewOnly)
        {
            InitializeComponent();

            this.previewOnly = previewOnly;
            this.houseBillItem = houseBillItem;
            this.forkliftID = forkliftID;

            this.HeaderText = houseBillItem.Reference();
            this.HeaderText2 = this.previewOnly == true ? "Pieces locations" : "Move pieces to forklift";
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(LocationView_LoadListEvent);
            
        }

        void LocationView_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            this.LoadData();

            Cursor.Current = Cursors.Default;
        }

        void LoadData()
        {
            this.selectedPiecesIDs = new List<int>();

         
            smoothListBoxBase1.RemoveAll();

            this.locations = ScannerUtility.Instance.GetPiecesByLocation(houseBillItem.HousebillId,0, MOT.Air);

            this.frokliftPiecesIDs = (
                                        from location in locations
                                        from piece in location.Piece
                                        where piece.LocationId == forkliftID
                                        select piece.PieceId
                                     ).ToList();

            int totalPieces = this.locations.Where(location => location.LocationId != this.forkliftID).Sum(location => location.Piece.Length);
            bool itemsSelectable = houseBillItem.AvailablePieces != 0 && totalPieces >= houseBillItem.AvailablePieces;

            foreach (var loc in locations)
            {
                List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();

                if (this.houseBillItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
                {
                    foreach (var piece in loc.Piece)
                    {
                        CustomListItems.ComboBoxItemData comboData = new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", piece.PieceNumber), string.Empty, piece.PieceId);

                        comboData.isChecked = loc.LocationId == forkliftID;
                        comboData.isReadonly = true;
                        comboData.IsSelectable = itemsSelectable;

                        items.Add(comboData);
                    }
                }
              
                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(houseBillItem.HousebillId, 
                                                                              loc.Location, 
                                                                              string.Format("Pieces: {0} of {1}", 
                                                                                            loc.Piece.Length, 
                                                                                            houseBillItem.TotalPieces), 
                                                                              houseBillItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Count ? CargoMatrix.Resources.Skin.countMode : CargoMatrix.Resources.Skin.PieceMode, 
                                                                              items);

                bool isSelectable = this.previewOnly == false && loc.LocationId != forkliftID;
                combo.IsSelectable = isSelectable;
                combo.Expandable = this.houseBillItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece;
                combo.ReadOnly = true;
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                combo.SubItemSelecting += new EventHandler<CustomListItems.ComboBoxEventArgs>(combo_SubItemSelecting);

                if (isSelectable == true)
                {
                    combo.Click += new EventHandler(combo_Click);
                }
                smoothListBoxBase1.AddItem(combo);
            }


            this.smoothListBoxBase1.IsSelectable = false;

            OkEnabled = false;
        }

        void combo_Click(object sender, EventArgs e)
        {
            if (this.houseBillItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece) return;

            if (this.houseBillItem.AvailablePieces <= 0) return;

            int? piecesCount = DischargeItemFunctionallity.GetPiecesCountFromUser(this.houseBillItem.Reference(), "Number of pieces to move", this.houseBillItem.AvailablePieces, false);

            if (piecesCount.HasValue == false) return;

            var piecesNumbers = (
                                from location in locations
                                from piece in location.Piece
                                where location.LocationId != forkliftID
                                select piece.PieceNumber).Take(piecesCount.Value);

            int slac = piecesCount.Value;

            if (houseBillItem.TotalPieces != houseBillItem.Slac)
            {
                bool isReadonly = false; // houseBillItem.AvailablePieces == piecesCount;
                int maxReleseSlac = houseBillItem.CalculateMaxReleaseSlac(piecesCount.Value);
                int? enteredSlac = DischargeItemFunctionallity.GetSlacFromUser(piecesCount.Value, maxReleseSlac, isReadonly);

                if (enteredSlac.HasValue == false) return;

                slac = enteredSlac.Value;
            }

            Cursor.Current = Cursors.WaitCursor;

            var hbItem = CargoDischarge.Instance.ScanPiecesIntoForkLift(this.houseBillItem, piecesNumbers.ToArray(), slac, forkliftID);

            if (hbItem.TransactionStatus.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(hbItem.TransactionStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return;
            }
            else
            {
                foreach (var alert in hbItem.ShipmentAlerts)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage, string.Format("Alert {0:MM/dd/yyyy}", alert.AlertDate), CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                }
            }

            this.houseBillItem = hbItem;
            this.selectedPiecesIDs.Clear();

            this.LoadData();

            this.wasScanned = true;

            Cursor.Current = Cursors.Default;

            this.Close();
        }

        void combo_SubItemSelecting(object sender, CustomListItems.ComboBoxEventArgs e)
        {
            if(this.frokliftPiecesIDs.Contains(e.SubItemID) == true)
            {
                e.Cancel = true;
                return;
            }

            var listItem = (CustomListItems.ComboBox)sender;
            var listSubItemData = listItem.Items.Where(item => item.id == e.SubItemID).First();

            if (listSubItemData.IsSelectable == false)
            {
                e.Cancel = true;
                return;
            }

            if (e.IsSelected == true)
            {
                if (this.selectedPiecesIDs.Contains(e.SubItemID) == false)
                {
                    this.selectedPiecesIDs.Add(e.SubItemID);
                }
            }
            else
            {
                if (this.selectedPiecesIDs.Contains(e.SubItemID) == true)
                {
                    this.selectedPiecesIDs.Remove(e.SubItemID);
                }
            }


            if (this.selectedPiecesIDs.Count >= this.houseBillItem.AvailablePieces)
            {
                var notSelectedItems = from item in listItem.Items
                                       where !(from ID in this.selectedPiecesIDs
                                               select ID).Contains(item.id) &&
                                               item.IsSelectable == true
                                       select item;

                listItem.UpdateSubItemsIconsSelectable(false, notSelectedItems);
            }
            else
            {
                var notSelectedItems = from item in listItem.Items
                                       where item.IsSelectable == false
                                       select item;

                listItem.UpdateSubItemsIconsSelectable(true, notSelectedItems);
            }


            //if (e.IsSelected == true &&
            //    this.selectedPiecesIDs.Contains(e.SubItemID) == false &&
            //    this.selectedPiecesIDs.Count < this.houseBillItem.AvailablePieces)
            //{
            //    this.selectedPiecesIDs.Add(e.SubItemID);
            //    e.Cancel = false;
            //    return;
            //}

            //if (e.IsSelected == false &&
            //    this.selectedPiecesIDs.Contains(e.SubItemID) == true)
            //{
            //    this.selectedPiecesIDs.Remove(e.SubItemID);
            //    e.Cancel = false;
            //    return;
            //}

            e.Cancel = false;
        }
       
        void combo_SelectionChanged(object sender, EventArgs e)
        {
            CustomListItems.ComboBox cmb = (CustomListItems.ComboBox)sender;

            OkEnabled = this.selectedPiecesIDs.Count > 0;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        { 
            
        }
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            var notRelatedIDs = from combo in this.smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox>()
                                from item in combo.SelectedIds
                                where (from id in this.frokliftPiecesIDs
                                       select id).Contains(item)
                                select item;

            if (notRelatedIDs.Count() > 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Some selected items are already on the forklift", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return;
            }

            var piecesNumbers = from location in locations
                                from piece in location.Piece
                                where
                                  (from id in this.selectedPiecesIDs
                                   select id).Contains(piece.PieceId)
                                select piece.PieceNumber;

            int piecesCount = piecesNumbers.Count();

            Cursor.Current = Cursors.WaitCursor;

            int slac = piecesCount;

            
            if (houseBillItem.TotalPieces != houseBillItem.Slac)
            {
                bool isReadonly = false;// houseBillItem.AvailablePieces == piecesCount;
                int maxReleseSlac = houseBillItem.CalculateMaxReleaseSlac(piecesCount);
                int? enteredSlac = DischargeItemFunctionallity.GetSlacFromUser(piecesCount, maxReleseSlac, isReadonly);

                if (enteredSlac.HasValue == false) return;

                slac = enteredSlac.Value;
            }
            

            var hbItem = CargoDischarge.Instance.ScanPiecesIntoForkLift(this.houseBillItem, piecesNumbers.ToArray(), slac, forkliftID);

            if (hbItem.TransactionStatus.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(hbItem.TransactionStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return;
            }
            else
            {
                foreach (var alert in hbItem.ShipmentAlerts)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage, string.Format("Alert {0:MM/dd/yyyy}", alert.AlertDate), CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                }
            }
           
            this.houseBillItem = hbItem;
            this.selectedPiecesIDs.Clear();

            this.LoadData();

            this.wasScanned = true;

             

            Cursor.Current = Cursors.Default;
        }

    }
}
