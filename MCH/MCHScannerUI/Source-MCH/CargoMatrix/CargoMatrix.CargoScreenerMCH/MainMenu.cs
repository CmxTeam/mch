﻿using System;
using System.Windows.Forms;
using CargoMatrix.Communication;
using System.Data;
using CargoMatrix.Communication.WSScannerMCHService;
namespace CargoMatrix.CargoScreener
{

    public enum ScreeningType
    { 
        PHYSICAL_INSPECTION = 1,
        ETD_SCREENING = 2,
        VERIFY_CUSTOMER_SCREENING = 3,
    }
         
    public partial class MainMenu : SmoothListbox.SmoothListbox
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.labelEmpty.Visible = false;
            this.TitleText = "Cargo Screener";
            pictureBoxDown.Enabled = false;
            pictureBoxUp.Enabled = false;
            panelSoftkeys.Refresh();
            smoothListBoxMainList.RemoveAll();
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("PHYSICAL INSPECTION ", CargoMatrix.CargoScreenerMCH.Resource.Visual_Inspection, (int)ScreeningType.PHYSICAL_INSPECTION));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("ETD SCREENING", CargoMatrix.CargoScreenerMCH.Resource.ETD, (int)ScreeningType.ETD_SCREENING));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("VERIFY CUSTOMER SCREENING", CargoMatrix.CargoScreenerMCH.Resource.Customer_screened, (int)ScreeningType.VERIFY_CUSTOMER_SCREENING));
            UnlinkETDDevices();
        }




        void UnlinkETDDevices()
        {
            try
            {
                CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteNonQuery("UPDATE       ITDX SET       UserId = 0 WHERE      UserId = " + CargoMatrix.Communication.WebServiceManager.UserID().ToString());
            }
            catch
            {

            }
        }



        private int GetScreeningMethodId(string method)
        {
          
            try
            {
                DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("GetScreeningMethods");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ScreeningMethod"].ToString().ToUpper() == method.ToUpper())
                    {
                        return int.Parse(dt.Rows[i]["Id"].ToString());
                    }
                }
                return 0;
            }
            catch
            {
                return 0;
            }

              


        }

        private void MainMenu_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            bool isCertified = false;
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("exec verifyscreeningcertified " + CargoMatrix.Communication.WebServiceManager.UserID().ToString());
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString() == "1")
                {
                    isCertified = true;
                }
            }

            if (!isCertified)
            {
                CargoMatrix.UI.CMXMessageBox.Show("You are not authorized to use this feature.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

         

           

            if (listItem is SmoothListbox.ListItems.StandardListItem)
            {
                ScreeningType mode = (ScreeningType)(listItem as SmoothListbox.ListItems.StandardListItem).ID;
                switch (mode)
                { 
                    case ScreeningType.ETD_SCREENING:

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new Morpho(mode, GetScreeningMethodId("ETD")));
                        break;
                    case ScreeningType.PHYSICAL_INSPECTION :
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new Screening(mode, GetScreeningMethodId("PHYSICAL")));
                        break;
                    case ScreeningType.VERIFY_CUSTOMER_SCREENING :
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new Screening(mode,0));
                        break;
                }
 

            }
            Cursor.Current = Cursors.Default;
            smoothListBoxMainList.Reset();
        }

        private void MainMenu_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        private void MainMenu_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
                        if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                            CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                        
                        break;
                }
            }
        }

    }

}
