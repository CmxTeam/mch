﻿namespace CargoMatrix.CargoScreener
{
    partial class Screening
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            components = new System.ComponentModel.Container();
            this.topLabel = new CustomUtilities.CMXBlinkingLabel();
            this.buttonScannedList = new CargoMatrix.UI.CMXCounterIcon();
            this.buttonSearch = new CargoMatrix.UI.CMXPictureButton();
            
            this.panelHeader2.SuspendLayout();


            // 
            // itemPicture
            // 
            this.itemPicture = new System.Windows.Forms.PictureBox();
            this.itemPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPicture.BackColor = System.Drawing.Color.White;
            this.itemPicture.Location = new System.Drawing.Point(6, 10);//new System.Drawing.Point(202, 41);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //
            // searchbox
            //
            this.searchBox = new CargoMatrix.UI.CMXTextBox();
            this.searchBox.Location = new System.Drawing.Point(6, 108);
            this.searchBox.Size = new System.Drawing.Size(195, 31);
            this.searchBox.TextChanged += new System.EventHandler(searchBox_TextChanged);
            this.searchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchBox_KeyDown);
            this.searchBox.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular);
            this.searchBox.Visible = false;
 
 
       
            this.label6 = new System.Windows.Forms.Label();
            this.label6.Location = new System.Drawing.Point(6, 145);
            this.label6.Size = new System.Drawing.Size(228, 45);
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.Text = "Last Scanned: N/A";
            this.label6.Visible = false;


            this.topLabel.BackColor = System.Drawing.Color.White;
            this.topLabel.Blink = true;
            this.topLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.topLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.topLabel.ForeColor = System.Drawing.Color.Red;
            this.topLabel.Location = new System.Drawing.Point(2, 15);
            this.topLabel.Name = "topLabel";
            this.topLabel.Size = new System.Drawing.Size(234, 15);
            this.topLabel.Text = "SCAN SHIPMENT";

            this.buttonScannedList.Location = new System.Drawing.Point(202, 10);
            this.buttonScannedList.Name = "buttonScannedList";
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            this.buttonScannedList.Size = new System.Drawing.Size(32, 32);
            this.buttonScannedList.TabIndex = 1;
            this.buttonScannedList.Click += new System.EventHandler(buttonScannedList_Click);
            this.buttonScannedList.Visible = false;

 


            this.buttonSearch.Location = new System.Drawing.Point(202, 108);
            this.buttonSearch.Size = new System.Drawing.Size(32, 32);
            this.buttonSearch.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonSearch.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonSearch.Click += new System.EventHandler(buttonSearch_Click);

        
 
            this.panelHeader2.Controls.Add(searchBox);
 
            this.panelHeader2.Controls.Add(buttonScannedList);
            this.panelHeader2.Controls.Add(buttonSearch);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
 
            this.panelHeader2.Controls.Add(label6);
            this.panelHeader2.Controls.Add(itemPicture);
  
  
            this.panelHeader2.Controls.Add(this.topLabel);
 
  
            panelHeader2.Height = 292;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);

        }


    
 
        protected System.Windows.Forms.Label label6;
 
        private CargoMatrix.UI.CMXCounterIcon buttonScannedList;
        protected CargoMatrix.UI.CMXPictureButton buttonSearch;
        private System.Windows.Forms.PictureBox itemPicture;
    
 
        private CustomUtilities.CMXBlinkingLabel topLabel;

        #endregion
    }
}
