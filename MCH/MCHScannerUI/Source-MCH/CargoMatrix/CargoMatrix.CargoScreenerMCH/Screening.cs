﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Data;
using CargoMatrix.Communication.WSScannerMCHService;
using System.Collections;
using System.Threading;

namespace CargoMatrix.CargoScreener


{
    public partial class Screening : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;
  
        protected CargoMatrix.UI.CMXTextBox searchBox;
        private int currentuserid = 0;
        
        private string shipmentnumber = string.Empty;
        private int pieces = 0;
        private int methodId = 0;
        
        private string lastshipment = string.Empty;
        private ScreeningType mode;

                    CargoMatrix.Utilities.RemarksMessageBox remarkbox;
   

        public Screening(ScreeningType mode,int methodId)
        {
            this.mode = mode;
            this.methodId = methodId;
            this.Visible = false;

            InitializeComponent();

            searchBox.WatermarkText = "Scan/Enter shipment";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            DoBarcodeEnabled(false);

            remarkbox = new CargoMatrix.Utilities.RemarksMessageBox();

        }




        void buttonScannedList_Click(object sender, System.EventArgs e)
        {



        }


        int SelectScreeningMethods()
        {

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Select a screening method";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "Screening Methods";


            actPopup.RemoveAllItems();



            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("GetScreeningMethods");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(dt.Rows[i]["ScreeningMethod"].ToString(), null,int.Parse( dt.Rows[i]["ID"].ToString())));
            }

            if (DialogResult.OK == actPopup.ShowDialog())
            {

                int id = (actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID;
                string name = (actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).Name;

                return id;


            }

            return 0;
        }


        void buttonSearch_Click(object sender, System.EventArgs e)
        {
            DoBarcodeEnabled(false);
            ValidateShipment(searchBox.Text);
            DoBarcodeEnabled(true);
        }

 

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));;
        }


        public override void LoadControl()
        {


            Cursor.Current = Cursors.WaitCursor;
            this.SuspendLayout();

            DoBarcodeEnabled(false);
            DoBarcodeEnabled(true);

           
     

              currentuserid = CargoMatrix.Communication.WebServiceManager.UserID();
 


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
  

            switch (this.mode)
            { 
                case ScreeningType.PHYSICAL_INSPECTION:
                      this.TitleText = "PHYSICAL INSPECTION"; 
                    break;
                case ScreeningType.VERIFY_CUSTOMER_SCREENING:
                    this.TitleText = "VERIFY CUSTOMER SCREENING"; 
                    break;
            }
 


            itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
 

            if (lastshipment == string.Empty)
            {
                this.label6.Text = "Last Scanned: N/A";
            }
            else
            {
                this.label6.Text = "Last Scanned: " + lastshipment;
            }
           
            this.buttonScannedList.Value = 0;



            this.searchBox.Visible = true;
            this.label6.Visible = true;
 
            this.buttonScannedList.Visible = false;
            this.itemPicture.Visible = false;


            this.Refresh();
            this.ResumeLayout(false);
            this.Visible = true;
            Cursor.Current = Cursors.Default;

        }

        private void DoBarcodeEnabled(bool enable)
        {
            if (enable)
            {
                if (!BarcodeEnabled)
                {
                    BarcodeEnabled = false;
                    BarcodeEnabled = true;
                }
            }
            else
            {
                BarcodeEnabled = false;
            }
        }



        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {

        }


        protected void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:

                    e.Handled = true;
                    break;
                case Keys.Enter:
                    ValidateShipment(searchBox.Text);
                    e.Handled = true;
                    break;

                case Keys.Up:

                    e.Handled = true;
                    break;
            }



        }


        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            DoBarcodeEnabled(false);
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }

            DoBarcodeEnabled(false);
            DoBarcodeEnabled(true);
        }


        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            

            //CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            ValidateShipment(barcodeData);


            Cursor.Current = Cursors.Default;
            DoBarcodeEnabled(true);
        }

   

        void ValidateShipment(string shipmentdata)
        {

      
             
            shipmentnumber = string.Empty;
            pieces = 0;
 

            string awbNumber = "";
            string origin = "";
            string destination = "";
            long taskId = 0;
            bool hasAlarm = false;
            int screenedPieces = 0;
            int totalPiece = 0;
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("GetCargoScreenerShipmentInfo '" + CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH() + "','" + shipmentdata + "'");
            if (dt.Rows.Count == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid Shipment.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
                return;
            }
            else
            {
                awbNumber = dt.Rows[0]["Shipment"].ToString();
                taskId = long.Parse(dt.Rows[0]["TaskId"].ToString());

                if (dt.Rows[0]["HasAlarm"].ToString() == "1")
                {
                    hasAlarm = true;
                }
                screenedPieces = int.Parse(dt.Rows[0]["ScreenedPieces"].ToString());
                origin = dt.Rows[0]["Origin"].ToString();
                destination = dt.Rows[0]["Destination"].ToString();
                totalPiece = int.Parse(dt.Rows[0]["Pieces"].ToString());
            }

 

            if (hasAlarm)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("This shipment has an Alarm. Do you want to clear it?", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {



                    remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
                    remarkbox.HeaderText = "Cargo Screening";
                    remarkbox.Remarks = string.Empty;
                    remarkbox.Reference = awbNumber;
                    remarkbox.Description = "Enter a remark:";
                    if (remarkbox.ShowDialog() != DialogResult.OK)
                    {
                        LoadControl();
                        return;
                    }
  
                    CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteNonQuery("ProcessScreeningClearAlarm " + taskId + "," + currentuserid + ",'" + remarkbox.Remarks + "'");

 
                }
                else
                {
                    LoadControl();
                    return;
                }
            }

   
            int remaningPieces = totalPiece - screenedPieces;
            if (remaningPieces < 0) { remaningPieces = 0; }
            int enteredPieces = 0;
            if (DoPieces(awbNumber, remaningPieces, out enteredPieces, true))
            {

                pieces = enteredPieces;
                shipmentnumber = awbNumber;
                lastshipment = awbNumber;


                switch (this.mode)
                {
                    case ScreeningType.PHYSICAL_INSPECTION:
                        VisualInspection(awbNumber, awbNumber, enteredPieces, remaningPieces, origin, destination, taskId);
                        break;
                    case ScreeningType.VERIFY_CUSTOMER_SCREENING:
                        VerifyCustomerInspection(awbNumber, awbNumber, enteredPieces, remaningPieces, origin, destination, taskId);
                        break;
                }
              

      


                LoadControl();
            }
            else
            {
                LoadControl();
            }
 


        }


        bool DoPieces(string reference, int remaningPieces, out int enteredPieces, bool allowOverage)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
            CntMsg.AllowOverage = allowOverage;
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }



        private void VisualInspection(string hawbNo, string reference, int count, int total, string origin, string destination, long taskId)
        {

 
            string remark = string.Empty;
            bool requiresPhotos = false;

            if (DialogResult.OK == MCHVisualInspectionMessageBox.Show(reference, ref remark, count, total, ref requiresPhotos))
            {



                string sql = string.Format("EXEC AddScreeningTransaction @UserId={0},@TaskId={1},@ScreenedPcs={2}, @MethodId={3}, @Result = '{4}', @ScreeningTimestamp = '{9}', @SampleNumber = '{5}', @DeviceSerialNo = '{6}', @CCSFId = {7}, @Comment = '{8}'", currentuserid, taskId, count, methodId, "PASS", "", "", "NULL", remark, DateTime.Now);
                CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteNonQuery(sql);


                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    topLabel.SplashText(string.Format(" {0} ({1}) Was Inspected", reference, count));
                    if (requiresPhotos)
                    {
                        ShowFreightPhotoCapture(hawbNo, origin, destination, count, "Visual Inspection", taskId);
                    }
             



            }


        }



        private void VerifyCustomerInspection(string hawbNo, string reference, int count, int total, string origin, string destination,long taskId)
        {

            remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
            remarkbox.HeaderText = "Confirm Customer Inspection";
            remarkbox.Remarks = string.Empty;
            remarkbox.Reference = reference;
            remarkbox.Description = "ENTER CUSTOMER CCSF#";
            if (remarkbox.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            string ccsf = remarkbox.Remarks;

            int ccsfId = 0;
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("VerifyValidScreeningFacility '" + ccsf + "'");
            if (dt.Rows.Count > 0)
            {
                ccsfId = int.Parse(dt.Rows[0][0].ToString());
            }

            if (ccsfId<1)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("The CCSF# you have entered is invalid. Do you want to re-enter it again?", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    VerifyCustomerInspection(hawbNo, reference, count, total, origin, destination, taskId);
                    return;
                }
                else
                {
                    return;
                }
            }


            int screenMethodId = SelectScreeningMethods();
            if (screenMethodId == 0)
            {
                return;
            }




            string remark = string.Empty;
            bool requiresPhotos = false;

            if (DialogResult.OK == MCHVisualInspectionMessageBox.Show(reference, ref remark, count, total, ref requiresPhotos))
            {

                string sql = string.Format("EXEC AddScreeningTransaction @UserId={0},@TaskId={1},@ScreenedPcs={2}, @MethodId={3}, @Result = '{4}', @ScreeningTimestamp = '{9}', @SampleNumber = '{5}', @DeviceSerialNo = '{6}', @CCSFId = {7}, @Comment = '{8}'", currentuserid, taskId, count, screenMethodId, "PASS", "", "", ccsfId, remark, DateTime.Now);
                CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteNonQuery(sql);


                CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                topLabel.SplashText(string.Format(" {0} ({1}) Was Inspected", reference, count));
                if (requiresPhotos)
                {
                    ShowFreightPhotoCapture(hawbNo, origin, destination, count, "Customer Inspection", taskId);
                }




            }

 
           
        }





        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title, long taskId)
        {


            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(taskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }

    }

}
