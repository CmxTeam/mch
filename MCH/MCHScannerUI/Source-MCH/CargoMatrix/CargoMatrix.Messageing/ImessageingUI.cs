﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Messageing
{
    public interface IMessageingUI
    {
        void UpdateUnreadMessages();
        void DisplayMessage();
        void DisplayListOfMessages();
    }
}
