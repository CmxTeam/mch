﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTender;
using CustomListItems;
using CargoMatrix.UI;
using CargoMatrix.Communication.Common;
using CustomUtilities;
using CargoMatrix.Utilities;

namespace CargoMatrix.CargoTender
{
    public partial class MasterbillList : CargoUtilities.SmoothListBoxAsync2Options<ICargTenderMasterBillItem>
    {
        int listItemHeight;
        
        #region Constructors
        public MasterbillList()
        {
            InitializeComponent();

            this.LoadOptionsMenu += new EventHandler(MasterbillList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(MasterbillList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MasterbillList_ListItemClicked);

            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(MasterbillList_BarcodeReadNotify);

            this.BarcodeEnabled = true;

            this.ButtonFinalizeText = "TENDER";
            this.FinalizeButtonEnabled = true;
        }
        #endregion

        #region Overrides
        protected override Control InitializeItem(ICargTenderMasterBillItem item)
        {
            CargoTenderMasterBillItemRendered listItem = new CargoTenderMasterBillItemRendered(item);
            listItem.ViewerClick += new EventHandler(listItem_OnViewerClick);
            listItem.DamageCaptureClick += new EventHandler(listItem_DamageCaptureClick);
            listItem.DetailsClick += new EventHandler(listItem_DetailsClick);
        
            return listItem;
        }

        public override void LoadControl()
        {
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;
            
            Cursor.Current = Cursors.WaitCursor;
            
            this.SetMainListHeaderText();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            Cursor.Current = Cursors.Default;

            this.smoothListBoxMainList.RemoveAll();

            this.smoothListBoxMainList.AddItem(this.InitializeItem(null));

            this.smoothListBoxMainList.AddItem(this.InitializeItem(null));

            this.headerItem.Counter = 7;

            this.ShowContinueButton(true);
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            base.buttonContinue_Click(sender, e);

            SignatureCaptureForm form = new SignatureCaptureForm();
            form.Header = "CARGO TENDER (>>>)";
            form.ShowDialog();
        }
        
        #endregion

        #region Handlers

        void headerItem_ButtonClick(object sender, System.EventArgs e)
        {
            if (this.headerItem.Counter <= 0) return;

            CommonMethods.ShowPlane();
        }

        void listItem_DetailsClick(object sender, EventArgs e)
        {
            var masterBillLIstItem = (CargoTenderMasterBillItemRendered)sender;

            MessageListBox list = new MessageListBox();
            list.TopPanel = false;
            list.ButtonVisible = false;
            list.HeaderText = "Select";
            list.HeaderText2 = "111-123456789-001";
            list.MultiSelectListEnabled = false;
            list.OneTouchSelection = true;
            list.OkEnabled = false;

            list.AddItem(new SmoothListbox.ListItems.StandardListItem<MasterbillDetailsEnum>("REJECT", null, MasterbillDetailsEnum.Reject));
            list.AddItem(new SmoothListbox.ListItems.StandardListItem<MasterbillDetailsEnum>("PRINT LABELS", null, MasterbillDetailsEnum.PrintLables));

            var dialogResult = list.ShowDialog();

            if (dialogResult != DialogResult.OK) return;

            var listItem = (SmoothListbox.ListItems.StandardListItem<MasterbillDetailsEnum>)list.SelectedItems[0];

            list.Dispose();

            switch (listItem.ItemData)
            {
                case MasterbillDetailsEnum.PrintLables:

                    break;
                case MasterbillDetailsEnum.Reject:

                    this.smoothListBoxMainList.RemoveItem(masterBillLIstItem);

                    break;
            }
        }    

        void listItem_DamageCaptureClick(object sender, EventArgs e)
        {
            var masterBillListItem = (CargoTenderMasterBillItemRendered)sender;

            //MssterBillAsHouseBill masterAsHouseBill = new MssterBillAsHouseBill(masterBillListItem.MasterBill);
            
          
            //DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(masterAsHouseBill, DamageCapture.DamageCaptureApplyTypeEnum.MasterBill);

            //damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            //{
            //    args.ShipmentConditions = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionTypes();
            //};

            //damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            //{
            //    args.ConditionsSummaries = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionsSummarys(masterBillListItem.MasterBill.TaskId);
            //};

            //damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            //{
            //    args.Result = CargoMatrix.Communication.ScannerUtility.Instance.UpdateMasterBillConditions(masterBillListItem.MasterBill.TaskId, args.ShipmentConditions);
            //};

            //using (var cursorChanger = CursorChanger.WaitChanger)
            //{
            //    CMXAnimationmanager.DisplayForm(damageCapture);
            //}
        }

        void listItem_OnViewerClick(object sender, EventArgs e)
        {
            var masterBillLIstItem = (CargoTenderMasterBillItemRendered)sender;

            if (masterBillLIstItem == null) return;

            var masterBill = masterBillLIstItem.ItemData;

            //CargoMatrix.Viewer.HousebillViewer billViewer = new CargoMatrix.Viewer.HousebillViewer(masterBill., masterBill.MasterBillNumber);
            //billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            //billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }
        
        void MasterbillList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
            {
                var masterBilllistItem = (CargoTenderMasterBillItemRendered)listItem;

                this.DisplayUldsScreen(masterBilllistItem.ItemData);
            }
            else
            {
                smoothListBoxMainList.MoveControlToTop(listItem);
                smoothListBoxMainList.RefreshScroll();
            }
        }
        
        void txtFilter_TextChanged(object sender, System.EventArgs e)
        {
            string filterText = this.txtFilter.Text.TrimEnd(new char[] { ' ' });

            this.smoothListBoxMainList.Filter(filterText);

            this.SetMainListHeaderText();
        }

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            this.ShowPlane();
        }

        void MasterbillList_BarcodeReadNotify(string barcodeData)
        {
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    var masterbill = (from item in this.smoothListBoxMainList.Items.OfType<CargoTenderMasterBillItem>()
                                     //where string.Compare(item.MasterBill.MasterBillNumber, scanItem.MasterBillNumber, true) == 0
                                     select item.MasterBill).FirstOrDefault();

                    if (masterbill == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Scanned masterbill is not in the list", "Scan Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }

                    this.DisplayUldsScreen(masterbill);

                    break;

                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    break;
            }


            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
        }

        void MasterbillList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;
            }

        }

        void MasterbillList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        } 
        
        #endregion

        #region Additional Methods
        private void SetMainListHeaderText()
        {
            this.TitleText = string.Format("CargoDischarge ({0})", this.smoothListBoxMainList.VisibleItemsCount);
        }

        private bool ItemTextMatch()
        {
            return false;
        }

        private void ShowPlane()
        {
            if (this.headerItem.Counter <= 0) return;


            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            var plane = Plane.Instance;

            plane.ShowDialog();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

        }

        private void DisplayUldsScreen(ICargTenderMasterBillItem masterbill)
        {
            Cursor.Current = Cursors.WaitCursor;
                   
            CMXAnimationmanager.DisplayForm(new UldList(masterbill));
           
        }

        #endregion

        private enum MasterbillDetailsEnum
        {
            PrintLables,
            Reject
        }
    }
}
