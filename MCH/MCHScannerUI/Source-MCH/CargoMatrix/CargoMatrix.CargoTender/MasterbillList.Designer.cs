﻿namespace CargoMatrix.CargoTender
{
    partial class MasterbillList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.panel1.Visible = false;
            this.panel1.Height = 0;

            this.panelHeader2.SuspendLayout();
            this.logo = new System.Windows.Forms.PictureBox();
            this.headerItem = new CustomListItems.HeaderItem();

            //
            // headerItem
            //

            this.headerItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerItem.Height = 49;
            this.headerItem.Label1 = "JOB :";
            this.headerItem.Label2 = "TRUCK# :";
            this.headerItem.Label3 = "SCAN/SELECT MASTERBILL";
            this.headerItem.ButtonImage = CargoMatrix.Resources.Skin.Airplane24x24;
            this.headerItem.ButtonClick += new System.EventHandler(headerItem_ButtonClick);

            //
            // txtFilter
            //
            this.txtFilter = new CargoMatrix.UI.CMXTextBox();
            this.txtFilter.WatermarkText = "Enter Filter text";
            this.txtFilter.Location = new System.Drawing.Point(4, 52);
            this.txtFilter.Size = new System.Drawing.Size(230, 23);
            this.txtFilter.TextChanged += new System.EventHandler(txtFilter_TextChanged);

            // 
            // logo
            // 
            this.logo.Location = new System.Drawing.Point(3, 2);
            this.logo.Name = "logo";
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.Size = new System.Drawing.Size(24, 24);

            this.panelHeader2.Controls.Add(this.headerItem);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Top, Height = 1, BackColor = System.Drawing.Color.Black });
            this.panelHeader2.Controls.Add(this.txtFilter);
            this.panelHeader2.Height = 78;
            this.panelHeader2.Visible = true;


            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.panelHeader2.ResumeLayout(false);
        }

        #endregion

        private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.UI.CMXTextBox txtFilter;
        private System.Windows.Forms.PictureBox logo;
    }
}
