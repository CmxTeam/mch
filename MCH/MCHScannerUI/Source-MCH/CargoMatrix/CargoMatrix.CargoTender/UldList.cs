﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CustomListItems;
using CMXExtensions;
using CargoMatrix.Communication.CargoTender;

namespace CargoMatrix.CargoTender
{
    public partial class UldList : CargoUtilities.SmoothListBoxAsyncOptions<IULD>
    {
        ICargTenderMasterBillItem masterbill;

        #region Constructors
        public UldList(ICargTenderMasterBillItem masterbill)
        {
            this.masterbill = masterbill;
            
            InitializeComponent();
            this.LoadOptionsMenu += new EventHandler(UldList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(UldList_BarcodeReadNotify);

            this.BarcodeEnabled = true;
           
            SetMainListHeaderText(0);
        }
        #endregion

        #region Handlers
        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;
            }
        }

        void UldList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void UldList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Uld:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    string uldNumber = scanItem.UldNumber.StartsWith("L-") ? scanItem.UldNumber.Remove(0, 2) : scanItem.UldNumber;

                    var uldListItem = (
                                        from item in this.smoothListBoxMainList.Items.OfType<UldListItemRendered>()
                                        where string.Compare(item.ItemData.ULDNo, uldNumber, true) == 0 &&
                                        item.ItemData.IsLoose() == false
                                        select item
                                      ).FirstOrDefault();

                    if (uldListItem == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Scanned uld is not in the list", "Scan Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }

                    this.Scan(uldListItem);


                    break;

                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                    break;
            }
        }
        #endregion

        #region Overrides
        protected override Control InitializeItem(IULD item)
        {
            var uldItem = new UldListItemRendered(item);
            uldItem.IsRejected = true;
            uldItem.ButtonBrosweClick +=new EventHandler(uldItem_ButtonBrosweClick);
            //uldItem.ButtonEnterClick += new EventHandler(uldItem_ButtonEnterClick);
            return uldItem;
        }

        void uldItem_ButtonEnterClick(object sender, EventArgs e)
        {
            if ((sender is UldListItemRendered) == false) return;

            var uldListItem = (UldListItemRendered)sender;

            this.Scan(uldListItem);
        }

        void uldItem_ButtonBrosweClick(object sender, EventArgs e)
        {
            var listItem = (UldListItemRendered)sender;

            if(listItem == null) return;

            UldContent uldContent = new UldContent(listItem.ItemData);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            uldContent.ShowDialog();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
        }

        public override void LoadControl()
        {
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            var uldList = CargoMatrix.Communication.LoadConsol.Instance.GetCargTenderULDList(this.masterbill);
            this.SetMainListHeaderText(uldList.Count());

            this.ReloadItemsAsync(uldList);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
        }
        
        #endregion

        #region Additional Methods
        private void SetMainListHeaderText(int listItemsCount)
        {
            this.TitleText = string.Format("CargoTender ({0})", listItemsCount);
        }

        private void Scan(UldListItemRendered uldListItem)
        {
            var uld = uldListItem.ItemData;

            if (uld.IsLoose())
            {
                this.ScanLOOSEToPlane(uldListItem);
            }
            else
            {
                this.ScanULDToPlane(uldListItem);
            }

            this.SetMainListHeaderText(this.smoothListBoxMainList.Items.Count);
        }

        private void ScanULDToPlane(UldListItemRendered uldListItem)
        {
            //uldListItem.SetImage(CargoMatrix.Resources.Skin.PieceMode);
            //uldListItem.Collapse();
            //uldListItem.Expandable = false;
        }

        private void ScanLOOSEToPlane(UldListItemRendered uldListItem)
        {
            //var uld = uldListItem.Uld;
            //int availablePieces = uld.Pieces - uld.ScannedPieces;

            //int? result = CommonMethods.GetPiecesCountFromUser("Scan LOOSE to plane", "Enter number of pieces", availablePieces, availablePieces <= 1);

            //if (result.HasValue == false) return;

            //uldListItem.Collapse();
            
            //if (result.Value == availablePieces)
            //{
            //    uldListItem.SetImage(CargoMatrix.Resources.Skin.PieceMode);
            //    uldListItem.Expandable = false;
            //}
        }
        #endregion

    }
}
