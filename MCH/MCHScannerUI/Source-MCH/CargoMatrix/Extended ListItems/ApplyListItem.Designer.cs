﻿namespace SmoothListBox.UI.ListItems
{
    partial class ApplyListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyListItem));
            this.buttonApply = new System.Windows.Forms.PictureBox();
            this.pictureBoxBg = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(60, 2);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(120, 36);
            this.buttonApply.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            this.buttonApply.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonApply_MouseDown);
            this.buttonApply.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonApply_Paint);
            this.buttonApply.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonApply_MouseUp);
            // 
            // pictureBoxBg
            // 
            this.pictureBoxBg.BackColor = System.Drawing.Color.White;
            this.pictureBoxBg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxBg.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxBg.Image")));
            this.pictureBoxBg.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxBg.Name = "pictureBoxBg";
            this.pictureBoxBg.Size = new System.Drawing.Size(240, 42);
            this.pictureBoxBg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // ApplyListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.pictureBoxBg);
            this.Name = "ApplyListItem";
            this.Size = new System.Drawing.Size(240, 42);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox buttonApply;
        private System.Windows.Forms.PictureBox pictureBoxBg;


    }
}
