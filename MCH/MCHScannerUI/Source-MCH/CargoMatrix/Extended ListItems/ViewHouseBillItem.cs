﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class ViewHouseBillItem : UserControl
    {
        public event EventHandler Enter;
        public string Text;
        public ViewHouseBillItem()
        {
            InitializeComponent();
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            if (Enter != null)
            {
                Text = textBoxHousebill.Text;
                Enter(sender, e);
            }

        }

     
    }
}
