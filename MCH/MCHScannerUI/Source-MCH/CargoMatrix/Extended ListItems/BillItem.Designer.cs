﻿namespace SmoothListBox.UI.ListItems
{
    partial class BillItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {            
            if (pictureBoxLogo.Image != null)
            {
                pictureBoxLogo.Image.Dispose();
                pictureBoxLogo.Image = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLine2 = new System.Windows.Forms.Label();
            this.labelLine3 = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.title = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.pictureBoxExpedite = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelLine4 = new System.Windows.Forms.Label();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.itemPicture)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxExpedite)).BeginInit();
            this.SuspendLayout();
            // 
            // labelLine2
            // 
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelLine2.Location = new System.Drawing.Point(31, 19);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(180, 13);
            this.labelLine2.Text = "<labelDescription>";
            
            // 
            // labelLine3
            // 
            this.labelLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelLine3.Location = new System.Drawing.Point(31, 33);
            this.labelLine3.Name = "labelLine3";
            this.labelLine3.Size = new System.Drawing.Size(180, 13);
            this.labelLine3.Text = "<labelHouseBill>";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Location = new System.Drawing.Point(213, 26);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Location = new System.Drawing.Point(3, 29);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(24, 24);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(31, 3);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(196, 14);
            this.title.Text = "y";
            // 
            // itemPicture
            // 
            this.itemPicture.Location = new System.Drawing.Point(3, 3);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // pictureBoxExpedite
            // 
            this.pictureBoxExpedite.Location = new System.Drawing.Point(220, 10);
            this.pictureBoxExpedite.Name = "pictureBoxExpedite";
            this.pictureBoxExpedite.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxExpedite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxExpedite.TransparentColor = System.Drawing.Color.White;
            this.pictureBoxExpedite.Visible = false;
            // 
            // labelLine4
            // 
            this.labelLine4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelLine4.Location = new System.Drawing.Point(31, 47);
            this.labelLine4.Name = "labelLine4";
            this.labelLine4.Size = new System.Drawing.Size(180, 13);
            this.labelLine4.Text = "XOY1234:ky";
            // 
            // BillItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelLine4);
            this.Controls.Add(this.pictureBoxExpedite);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.labelLine2);
            this.Controls.Add(this.labelLine3);
            this.Name = "BillItem";
            this.Size = new System.Drawing.Size(240, 62);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.itemPicture)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxExpedite)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelLine2; 
        private System.Windows.Forms.Label labelLine3;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxLogo;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
        public System.Windows.Forms.Label title;
        public OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxExpedite;
        private System.Windows.Forms.Label labelLine4;// OpenNETCF.Windows.Forms.PictureBox2 buttonDetails;
        
    }
}
