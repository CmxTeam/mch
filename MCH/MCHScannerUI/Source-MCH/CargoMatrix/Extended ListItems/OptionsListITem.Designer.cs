﻿namespace SmoothListBox.UI.ListItems
{
    partial class OptionsListITem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelUser = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelUser
            // 
            this.LabelUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelUser.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.LabelUser.ForeColor = System.Drawing.Color.Gray;
            this.LabelUser.Location = new System.Drawing.Point(41, 25);
            this.LabelUser.Name = "LabelUser";
            this.LabelUser.Size = new System.Drawing.Size(196, 12);
            // 
            // OptionsListITem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.LabelUser);
            this.Name = "OptionsListITem";
            this.Size = new System.Drawing.Size(240, 40);
            this.EnabledChanged += new System.EventHandler(this.OptionsListITem_EnabledChanged);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label LabelUser;
    }
}
