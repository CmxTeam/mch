﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class Task : ListItem
    {
        
        public Task()
        {
            InitializeComponent();
        }

        public Task(string title, string description,  Image picture)
        {
            InitializeComponent();
            this.taskPicture.Image = picture;
            this.title.Text = title;
            this.description.Text = description;
            SelectedChanged(false);
            
        }

        #region IExtendedListItem Members

        public override void SelectedChanged(bool isSelected)
        {
            if (isSelected)
            {
                Height = 72;
                taskPicture.Size = new Size(64, 64);
                description.Visible = true;
                title.Left = 74;
                
            }
            else
            {
                Height = 40;
                taskPicture.Size = new Size(32, 32);
                description.Visible = false;
                title.Left = 42;
                
            }
            base.SelectedChanged(isSelected);
        }

        public override void PositionChanged(int index)
        {
            
        }

        #endregion



    }
}
