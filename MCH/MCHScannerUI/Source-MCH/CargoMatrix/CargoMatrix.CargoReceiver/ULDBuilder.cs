﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomListItems;
using CustomUtilities;
using CMXBarcode;
using CMXExtensions;
using CargoMatrix.Communication;
using System.Linq;
using CargoMatrix.Communication.DTO;
namespace CargoMatrix.CargoReceiver
{
    public partial class ULDBuilder : SmoothListbox.SmoothListbox2
    {
        private CustomListItems.HeaderItem headerItem;
        private bool HasLoose
        {
            get
            {
                foreach (var item in smoothListBoxMainList.Items)
                {
                    if ((item as ULDListItem_new).ItemData.IsLoose)
                        return true;
                }
                return false;
            }
        }


        public ULDBuilder()
        {

            InitializeComponent();
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = Forklift.Instance.Mawb.Reference();
            headerItem.Label2 = Forklift.Instance.Mawb.Line2();
            headerItem.Label3 = "ADD/EDIT ULD";
            //headerItem.BlinkColor = Color.Red;
            //headerItem.Blink = true;

            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            headerItem.Location = panel1.Location;
            panel1.Visible = true;
            this.panel1.Controls.Add(headerItem);
            headerItem.BringToFront();
            panel1.Height = headerItem.Height;
            panelHeader2.Height = headerItem.Height;

            this.TitleText = string.Format("CargoReceiver - {0}", Forklift.Instance.ReceiveMode.ToString());

            LoadOptionsMenu += new EventHandler(ULDBuilder_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ULDBuilder_MenuItemClicked);
            this.ButtonFinalizeText = "Next";
            headerItem.Logo = CargoMatrix.Communication.Utilities.ConvertToImage(Forklift.Instance.Mawb.CarrierInfo.CarrierLogo);

        }

        void ULDBuilder_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL:
                        populatePrintableUlds();
                        break;
                }
            }
        }

        private void DisplayExistingULDS()
        {
            if (Forklift.Instance.Mawb == null)
                return;

            smoothListBoxMainList.RemoveAll();
            foreach (var item in CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillULDs(Forklift.Instance.Mawb.MasterBillId))
            {

                ULDListItem_new uld = new CustomListItems.ULDListItem_new(item, false);//{ ID = item.ID, Logo = CargoMatrix.Resources.Skin.Freight_Car, Line2 = string.Format("PCS: {0} WT: {1}KG", item.Pieces, (int)item.Weight) };
                uld.ButtonDeleteClick += new EventHandler(Ulditem_ButtonDeleteClick);
                uld.ButtonEditClick += new EventHandler(Ulditem_ButtonEditClick);


                //Force to Set ULD Number  if Number is not set.
                if (!item.IsLoose && string.IsNullOrEmpty(item.ULDNo))
                {
                    Application.DoEvents();
                    do
                    {
                        CustomUtilities.ScanEnterPopup_old uldNumPop = new ScanEnterPopup_old(BarcodeType.ULD);
                        uldNumPop.Title = item.ULDName;
                        uldNumPop.TextLabel = "Scan or Enter ULD Number";
                        if (DialogResult.OK == uldNumPop.ShowDialog())
                        {
                            var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(uldNumPop.ScannedText);
                            if (scanItem.BarcodeType == CMXBarcode.BarcodeTypes.Uld)
                            {
                                /// if number already exists
                                if (smoothListBoxMainList.Items.OfType<ULDListItem_new>().Any(uldItem => uldItem.ItemData.ULDNo.Equals(scanItem.UldNumber, StringComparison.OrdinalIgnoreCase)))
                                {
                                    string msg = string.Format(CargoReceiverResources.Text_UldNoExists, scanItem.UldNumber);
                                    CargoMatrix.UI.CMXMessageBox.Show(msg, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                                    continue;
                                }
                                else
                                {
                                    uld.ItemData.ULDNo = item.ULDNo = uldNumPop.ScannedText;
                                    CargoMatrix.Communication.CargoReceiver.Instance.UpdateULD(item);
                                    break;
                                }
                            }
                            else
                                CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_InvalidUldNo, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        }
                    } while (true);
                }
                smoothListBoxMainList.AddItem(uld);
            }
        }

        void ULDBuilder_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL));
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            Control[] btns = new Control[]{
            new SmoothListbox.ListItems.StandardListItem("STAGE",null,1),
            new SmoothListbox.ListItems.StandardListItem("CHECK IN",null,2)};

            CargoMatrix.Utilities.MessageListBox popup = new CargoMatrix.Utilities.MessageListBox();
            popup.AddItems(btns);
            popup.MultiSelectListEnabled = false;
            popup.OneTouchSelection = true;
            popup.HeaderText = headerItem.Label1;
            popup.HeaderText2 = "Select Action To Continue";
            popup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(popup_ListItemClicked);
            popup.ShowDialog();
        }

        void popup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:
                    if (stageUlds())
                    {
                        Forklift.Instance.Mawb = null;
                        CargoMatrix.UI.CMXAnimationmanager.GoToContol("TaskList");
                    }
                    break;
                case 2:
                    ProceedNext();
                    break;
            }
            sender.Reset();
        }

        private bool stageUlds()
        {
            bool result = true;
            CustomUtilities.ScanEnterPopup uldDropLoc = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            foreach (ULDListItem_new uld in smoothListBoxMainList.Items)
            {
                while (true) // loop while successfuly staged of canceled
                {
                    uldDropLoc.Reset();
                    uldDropLoc.Title = uld.ItemData.ULDName;
                    uldDropLoc.TextLabel = CargoReceiverResources.Text_ScanULDDrop;
                    if (DialogResult.OK == uldDropLoc.ShowDialog())
                    {
                        uld.ItemData.Location = uldDropLoc.Text;
                        var status = CargoMatrix.Communication.CargoReceiver.Instance.StageMasterBill(Forklift.Instance.Mawb.MasterBillId, uld.ItemData.ID, uldDropLoc.Text);
                        if (status.TransactionStatus1 == false)
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        else
                            break;
                    }
                    else
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        #region PrintULDLabels
        private void populatePrintableUlds()
        {
            CargoMatrix.Utilities.MessageListBox printableUlds = new CargoMatrix.Utilities.MessageListBox();
            printableUlds.ButtonVisible = true;
            printableUlds.ButtonImage = CargoMatrix.Resources.Skin.printerButton;
            printableUlds.ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            var defPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);
            printableUlds.HeaderText = defPrinter.PrinterName;
            printableUlds.HeaderText2 = "Select ULDs to print labels";

            printableUlds.ButtonClick += delegate(object sender, EventArgs e)
            {
                if (CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD, ref defPrinter))
                    printableUlds.HeaderText = defPrinter.PrinterName;

            };



            var uldsCheckItems = from uld in smoothListBoxMainList.Items.OfType<ULDListItem_new>()
                                 where !uld.ItemData.IsLoose
                                 select new CustomListItems.CkeckItem<IULDItem>(uld.ULDReference, uld.ID, uld.ItemData);

            printableUlds.AddItems(uldsCheckItems.OfType<Control>().ToArray());

            if (DialogResult.OK == printableUlds.ShowDialog())
            {
                SendPrintingJobToPrinter(printableUlds.SelectedItems, defPrinter);
            }
        }

        private void SendPrintingJobToPrinter(List<Control> list, CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter selectedPrinter)
        {
            if (selectedPrinter.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel ||
    selectedPrinter.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless)
            {
                /// Capture printing reason if printed from mobile printer

                var mbilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(selectedPrinter);
                foreach (var item in list.OfType<CustomListItems.CkeckItem<IULDItem>>())
                    mbilePrinter.UldPrint(item.ItemData.ULDNo);
            }
            else            /// if network printer
            {

                foreach (var item in list.OfType<CustomListItems.CkeckItem<IULDItem>>())
                    CargoMatrix.Communication.ScannerUtility.Instance.PrintULDLabel(item.ItemData.ID, selectedPrinter.PrinterId, 1);
                string message = string.Format("Printing has been scheduled on {0}", selectedPrinter.PrinterName);
                CargoMatrix.UI.CMXMessageBox.Show(message, "Print", CargoMatrix.UI.CMXMessageBoxIcon.Success);

            }
        }

        #endregion

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            ULDEditor.Instance.AddULDItem(Forklift.Instance.Mawb.MasterBillId, Forklift.Instance.Mawb.CarrierNumber, Forklift.Instance.Mawb.MasterBillNumber, !HasLoose);
            LoadControl();
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            DisplayExistingULDS();

            Cursor.Current = Cursors.Default;
            Application.DoEvents();
            if (smoothListBoxMainList.ItemsCount == 0)
            {
                ShowContinueButton(false);
                Application.DoEvents();
                if (ULDEditor.Instance.AddULDItem(Forklift.Instance.Mawb.MasterBillId, Forklift.Instance.Mawb.CarrierNumber, Forklift.Instance.Mawb.MasterBillNumber, !HasLoose))
                    LoadControl();
            }
            else
            {
                ShowContinueButton(false);
                ShowContinueButton(true);
            }
            Cursor.Current = Cursors.Default;
        }


        void Ulditem_ButtonEditClick(object sender, EventArgs e)
        {
            if (sender is ULDListItem_new)
            {
                ULDListItem_new item = sender as ULDListItem_new;
                ULDEditor.Instance.EditUldItem(item, !HasLoose);
                if (item.ItemData.IsLoose)
                {
                    smoothListBoxMainList.MoveItemToTop(sender as Control);
                }
                smoothListBoxMainList.LayoutItems();
            }
        }



        void Ulditem_ButtonDeleteClick(object sender, EventArgs e)
        {
            if (ULDEditor.Instance.DeleteULDItem((sender as ULDListItem_new)))
                LoadControl(); // if delete confirmed reload the uld items

            if (smoothListBoxMainList.ItemsCount == 0)
                ShowContinueButton(false);
        }


        private void ProceedNext()
        {
            Cursor.Current = Cursors.WaitCursor;

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new HawbScan());

            smoothListBoxMainList.Reset();
        }
    }
}
