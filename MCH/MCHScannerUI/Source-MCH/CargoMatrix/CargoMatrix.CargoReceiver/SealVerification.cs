﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CargoMatrix.Utilities;

namespace CargoMatrix.CargoReceiver
{
    public partial class SealVerification : CargoMatrix.Utilities.MessageListBox
    {
        public SealVerification()
        {
            InitializeComponent();
            MultiSelectListEnabled = false;
            OneTouchSelection = false;
            this.HeaderText2 = "Select Seal Type";
            this.HeaderText = string.Format("{0}-{1}-{2}", Forklift.Instance.Mawb.Origin, Forklift.Instance.Mawb.MasterBillNumber, Forklift.Instance.Mawb.Destination);
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(SealVerification_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(SealVerification_ListItemClicked);
        }

        void SealVerification_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as ListItem).ID)
            {
                case 1:
                    string reference = GetReference();
                    if (!string.IsNullOrEmpty(reference))
                    {
                        SaveSealInfo((listItem as ListItem).ID, reference);

                    }
                    else this.ResetSelection();
                    break;
                case 2:
                    string tetLevel = GetTETLevel();
                    if (!string.IsNullOrEmpty(tetLevel))
                    {
                        SaveSealInfo((listItem as ListItem).ID, tetLevel);

                    }
                    else this.ResetSelection();
                    break;

                default:
                    SaveSealInfo((listItem as ListItem).ID, string.Empty);
                    break;
            }
        }

        void SealVerification_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            var seals = CargoMatrix.Communication.CargoReceiver.Instance.GetSealTypes();
            foreach (var item in seals)
            {
                this.AddItem(new StandardListItem(item.SealType, null, item.RecordId));
            }
        }
        private void SaveSealInfo(int sealId, string sealRef)
        {
            Cursor.Current = Cursors.WaitCursor;
            CargoMatrix.Communication.CargoReceiver.Instance.SaveSealInformation(Forklift.Instance.Mawb.CarrierNumber, Forklift.Instance.Mawb.MasterBillNumber, sealId, sealRef, string.Empty, string.Empty);
            DialogResult = DialogResult.OK;
            Cursor.Current = Cursors.Default;
        }
        private string GetTETLevel()
        {
            MessageListBox tetList = new MessageListBox();
            tetList.HeaderText2 = this.HeaderText;
            tetList.HeaderText = "Select TET Application Level";
            tetList.OneTouchSelection = true;
            tetList.AddItem(new StandardListItem("Piece", null, 1));
            tetList.AddItem(new StandardListItem("SKID", null, 2));
            tetList.AddItem(new StandardListItem("ULD", null, 3));

            if (DialogResult.OK == tetList.ShowDialog())
            {
                return (tetList.SelectedItems[0] as StandardListItem).title.Text;
            }
            else return string.Empty;
        }
        private string GetReference()
        {
            CustomUtilities.EnterReferencePopup refPopup = new CustomUtilities.EnterReferencePopup(this.HeaderText, "Enter Seal Number");
            if (DialogResult.OK == refPopup.ShowDialog())
            {
                return refPopup.EnteredText;
            }
            else return string.Empty;
        }
    }
}
