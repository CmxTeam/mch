﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
//using CargoMatrix.Communication.DTO;
using CMXBarcode;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXExtensions;

namespace CargoMatrix.CargoReceiver
{
    public partial class CargoReceiver : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        MawbDetails mawbDetailsItem;
        CustomListItems.OptionsListITem mawbListOption;
        CustomListItems.OptionsListITem hawbListOption;
        public CargoReceiver()
        {
            InitializeComponent();
            this.LoadOptionsMenu += new EventHandler(CargoReceiver_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(CargoReceiver_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeReadNotify_Domestic);
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            base.pictureBoxMenu_Click(sender, e);
            if (m_bOptionsMenu)
            {
                if (Forklift.Instance.Mawb != null)
                {
                    smoothListBoxOptions.RemoveItem(mawbListOption);
                    if (!smoothListBoxOptions.itemsPanel.Controls.Contains(hawbListOption))
                        smoothListBoxOptions.AddItem(hawbListOption);
                }
                else
                {
                    smoothListBoxOptions.RemoveItem(hawbListOption);
                    if (!smoothListBoxOptions.itemsPanel.Controls.Contains(mawbListOption))
                        smoothListBoxOptions.AddItem(mawbListOption);

                }
            }

        }
        private void InitializeComponentHelper()
        {
            panelHeader2.Height = 231;
            this.Scrollable = false;
        }



        public override void LoadControl()
        {
            /// commented on ATL Aug 17 
            //Application.DoEvents();


            this.TitleText = string.Format("CargoReceiver - {0}", Forklift.Instance.ReceiveMode.ToString());
            if (Forklift.Instance.Mawb != null)
                DisplayMawbDetails();
            else
            {
                topLabel.Text = "Scan Masterbill or Housebill".ToUpper();
                ClearMawbDetails();
            }
            //if (mawbDetailsItem != null)
            //    mawbDetailsItem.Visible = false;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private void ClearMawbDetails()
        {
            if (mawbDetailsItem != null)
                mawbDetailsItem.Visible = false;
        }

        private void DisplayMawbDetails()
        {
            this.TitleText = string.Format("CargoReceiver - {0}", Forklift.Instance.ReceiveMode.ToString());

            BarcodeEnabled = false;

            if (mawbDetailsItem == null)
            {
                mawbDetailsItem = new MawbDetails() { Top = 20 };
                mawbDetailsItem.ProceedButton.Click += new EventHandler(ProceedButton_Click);
                this.panelHeader2.Controls.Add(mawbDetailsItem);
            }


            if (Forklift.Instance.Mawb.RecoveredDate.HasValue)
            {
                topLabel.Text = "CONTINUE RECOVERY";
                mawbDetailsItem.ProceedButton.Text = "CONTINUE";
            }
            else
            {
                topLabel.Text = "CONFIRM RECOVERY";
                mawbDetailsItem.ProceedButton.Text = "RECOVER";
            }


            mawbDetailsItem.DisplayMasterbill();
        }


        void CargoReceiver_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)

                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.VIEW_MAWB_LIST:
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new MawbList());
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.VIEW_HAWB_LIST:
                        new HawbListMessageBox(Forklift.Instance.Mawb.Reference(), Forklift.Instance.Mawb.TaskId).ShowDialog();
                        break;
                }
        }

        void CargoReceiver_LoadOptionsMenu(object sender, EventArgs e)
        {
            mawbListOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.VIEW_MAWB_LIST);
            hawbListOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.VIEW_HAWB_LIST);

            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(this.UtilitesOptionsListItem);
        }



        internal bool MapTruckToDoor()
        {
            bool result = false;
            int partId = 0;
            if (!Forklift.Instance.Mawb.IsRecovered)
            {
                CustomUtilities.MultiScanPopup mscanPop = new CustomUtilities.MultiScanPopup();
                mscanPop.Header = Forklift.Instance.Mawb.Reference();

                mscanPop.Caption = "Scan or enter number";
                mscanPop.Label1 = "Scan door barcode";
                mscanPop.Label2 = "Scan truck barcode (optional)";
                mscanPop.Text2ISOptional = true;
                mscanPop.Prefix1 = CustomUtilities.BarcodeType.Door;
                mscanPop.Prefix2 = CustomUtilities.BarcodeType.Truck;
                if (DialogResult.OK == mscanPop.ShowDialog())
                {
                    var status = CargoMatrix.Communication.CargoReceiver.Instance.RecoverConsol(Forklift.Instance.Mawb.TaskId, mscanPop.Text1, mscanPop.Text2, partId);
                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                    else
                    {
                        result = true;
                        Forklift.Instance.Mawb.IsRecovered = true;
                    }
                }
            }
            else
                result = true;
            if (result == true)
                Forklift.Instance.Mawb.RecoveredDate = DateTime.Now;
            return result;
        }

        void BarcodeReadNotify_Domestic(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            ScanObject barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            this.topLabel.SplashText("Scanned " + barcodeData);
            MasterBillItem tempMawb = null;
            switch (barcode.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    Forklift.Instance.FirstScannedHousebill = barcodeData;
                    tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(string.Empty, string.Empty, barcode.HouseBillNumber);
                    break;
                case BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(barcode.CarrierNumber, barcode.MasterBillNumber, string.Empty);
                    break;
                default:
                    break;
            }

            if (tempMawb != null)
            {
                if (tempMawb.Transaction.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(tempMawb.Transaction.TransactionError, "Error:", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
                else
                {
                    Forklift.Instance.Mawb = tempMawb;
                    DisplayMawbDetails();
                }
            }
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }
        void DisplayAlerts()
        {
            foreach (var alert in Forklift.Instance.Mawb.ShipmentAlerts)
                CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage + Environment.NewLine + alert.AlertSetBy + Environment.NewLine + alert.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + Forklift.Instance.Mawb.MasterBillNumber, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }

        void ProceedButton_Click(object sender, EventArgs e)
        {
            if (Forklift.Instance.Mawb.Direction == MasterBillDirection.Domestic) // Domestic Flow
            {
                if (Forklift.Instance.Mawb.SealVerificationPending)
                {
                    BarcodeEnabled = false;
                    CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_PendingSeal, "Seal Not Verified", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    if (CargoMatrix.Communication.Utilities.IsAdmin ||
                        DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                    {
                        if (DialogResult.OK == new SealVerification().ShowDialog())
                            Forklift.Instance.Mawb.SealVerificationPending = false;

                    }
                    BarcodeEnabled = true;
                    return;
                }

                BarcodeEnabled = false;
                if (MapTruckToDoor())
                {
                    DisplayAlerts();
                    DisplayStageCheckinPopup();

                }
                else BarcodeEnabled = true;
            }
            else // Import Flow
            {
                BarcodeEnabled = false;
                if (MapTruckToDoor())
                {
                    DisplayAlerts();

                    Cursor.Current = Cursors.WaitCursor;
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ULDBuilder());
                }
                else BarcodeEnabled = true;

            }
        }

        private void DisplayStageCheckinPopup()
        {
            Control[] btns = new Control[]{
            new SmoothListbox.ListItems.StandardListItem("STAGE",null,1),
            new SmoothListbox.ListItems.StandardListItem("CHECK IN",null,2)};

            CargoMatrix.Utilities.MessageListBox popup = new CargoMatrix.Utilities.MessageListBox();
            popup.AddItems(btns);
            popup.MultiSelectListEnabled = false;
            popup.OneTouchSelection = true;
            popup.HeaderText = Forklift.Instance.Mawb.Reference();
            popup.HeaderText2 = "Select Action To Continue";
            popup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(popup_ListItemClicked);
            if (DialogResult.OK != popup.ShowDialog())
                BarcodeEnabled = true;
        }

        void popup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:
                    if (stageConsol())
                    {
                        Forklift.Instance.Mawb = null;
                        CargoMatrix.UI.CMXAnimationmanager.GoBack();
                    }
                    break;
                case 2:
                    Cursor.Current = Cursors.WaitCursor;
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new HawbScan());
                    break;
            }
            sender.Reset();
        }

        private bool stageConsol()
        {
            CustomUtilities.ScanEnterPopup consolStagePop = new CustomUtilities.ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            consolStagePop.Title = Forklift.Instance.Mawb.Reference();
            consolStagePop.TextLabel = "Scan or Enter Stage Location";
            if (DialogResult.OK == consolStagePop.ShowDialog())
            {
                var status = CargoMatrix.Communication.CargoReceiver.Instance.StageMasterBill(Forklift.Instance.Mawb.MasterBillId, 0, consolStagePop.Text);
                if (status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
                return status.TransactionStatus1;
            }
            else return false;
        }
    }
}
