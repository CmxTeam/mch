﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSCargoReceiver;
using CargoMatrix.Communication;
using System.Windows.Forms;
using CMXExtensions;
using System.Drawing;
using SmoothListbox;
using CustomListItems.RenderItems;
namespace CargoMatrix.CargoReceiver
{
    internal class HawbListMessageBox : SearchMessageListBox
    {
        private Label filterLabel;
        ScanStatuses filter = ScanStatuses.NotScanned;
        HouseBillFields sort = HouseBillFields.HousebillNumber;
        int taskId;
        CargoMatrix.UI.CMXPictureButton filterButton;
        public HawbListMessageBox(string title, int taskId)
        {
            IsSelectable = false;
            this.HeaderText = title;
            InitializeComponents();
            this.taskId = taskId;

            PopulateList();
        }

        private void PopulateList()
        {
            this.Refresh();
            filterTextBox.Text = string.Empty;
            filterButton.Focus();
            filterLabel.Text = filter.ToString() + "/" + sort.ToString();
            smoothListBoxBase1.RemoveAll();
            Cursor.Current = Cursors.WaitCursor;
            var hawbs = CargoMatrix.Communication.CargoReceiver.Instance.GetHousebillListWithFSP(this.taskId, filter, sort,false);
            Cursor.Current = Cursors.Default;

            var lItems = from hb in hawbs
                         select InitializeItem(hb);

            smoothListBoxBase1.AddItemsR(lItems.ToArray<ICustomRenderItem>());
        }

        public HawbListMessageBox(string title)
        {
            MultiSelectListEnabled = false;
            UnselectListEnabled = true;
            OneTouchSelection = true;
            this.HeaderText = title;
            InitializeComponents();
            taskId = Forklift.Instance.Mawb.TaskId;
            PopulateList();
        }
        private void InitializeComponents()
        {
            this.SuspendLayout();
            SizeF scalSize = new System.Drawing.SizeF(this.CurrentAutoScaleDimensions.Height / 96F, this.CurrentAutoScaleDimensions.Height / 96F);
            filterLabel = new Label();
            filterLabel.Location = new System.Drawing.Point(4, 2);
            filterLabel.Size = new System.Drawing.Size(186, 15);
            filterLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            filterLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.panelHeader.Controls.Add(filterLabel);
            this.panelHeader.Height = (int)(42 * scalSize.Height);
            this.smoothListBoxBase1.Height = (int)(163 * scalSize.Height);
            this.smoothListBoxBase1.Top = panelHeader.Bottom;
            filterLabel.Scale(scalSize);

            filterButton = new CargoMatrix.UI.CMXPictureButton();
            filterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            filterButton.Location = new System.Drawing.Point(196, 6);
            filterButton.Size = new System.Drawing.Size(32, 32);
            filterButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            filterButton.Image = Resources.Skin.filter_btn;
            filterButton.PressedImage = Resources.Skin.filter_btn_over;
            filterButton.Click += new EventHandler(filterButton_Click);
            this.filterTextBox.Width = filterLabel.Width;
            this.panelHeader.Controls.Add(filterButton);
            filterButton.Scale(scalSize);
            this.ResumeLayout(false);
        }

        void filterButton_Click(object sender, EventArgs e)
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Hawb);
            fp.Filter = filter.ToString();
            fp.Sort = sort.ToString();
            if (DialogResult.OK == fp.ShowDialog())
            {
                try
                {
                    filter = (ScanStatuses)Enum.Parse(typeof(ScanStatuses), fp.Filter, true);

                }
                catch (Exception)
                {

                    filter = ScanStatuses.NotScanned;
                }
                try
                {
                    sort = (HouseBillFields)Enum.Parse(typeof(HouseBillFields), fp.Sort, true);

                }
                catch (Exception)
                {
                    sort = HouseBillFields.HousebillNumber;
                }
                PopulateList();
            }
        }

        protected ICustomRenderItem InitializeItem(HouseBillItem item)
        {
            Image logo = item.ScanMode == ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode;
            if (string.IsNullOrEmpty(item.AliasNumber))
            {
                var listItem = new ListItemWithButton<HouseBillItem>(item.Reference(), string.Format("Pcs: {0} Wt: {1} KGS", item.TotalPieces, item.Weight),
                      logo, item);
                listItem.OnButtonClick += new EventHandler(listItem_OnButtonClick);
                return listItem;
            }
            else
            {
                var listItem = new ThreeLineListItem<HouseBillItem>(item.Reference(), item.AliasNumber, string.Format("Pcs: {0} Wt: {1} KGS", item.TotalPieces, item.Weight),
                      logo, item);
                listItem.OnButtonClick += new EventHandler(listItem_OnButtonClick);
                return listItem;
            }
        }

        void listItem_OnButtonClick(object sender, EventArgs e)
        {
            var itemData = (sender as IDataHolder<HouseBillItem>).ItemData;
            var hawb = CargoMatrix.Communication.CargoReceiver.Instance.GetHousebillInfo(itemData.HousebillId,Forklift.Instance.Mawb.TaskId);//hawb;

            CustomUtilities.HawbLabelPrinter.Show(hawb.HousebillId, hawb.HousebillNumber, hawb.TotalPieces);

        }
    }



}
