﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.OnHand;

namespace CargoMatrix.Communication.WSOnHand
{
    public partial class CMXPickup : IShipment
    {
        #region IShipment Members

        public string ShipmentNO
        {
            get { return this.pickupRefField; }
        }

        public string Destination
        {
            get { return this.internationalDestinationField; }
        }

        public string Origin
        {
            get { return this.originStationField; }
        }

        public string MOT
        {
            get { return this.modeOfTransportField; }
        }

        public string CustomerCode
        {
            get { return this.accountField; }
        }

        public string Vendor
        {
            get { return this.pickupFromField.Name; }
        }

        public string TruckingCompany
        {
            get { return carrierField != null ? carrierField.CarrierName : string.Empty; }
        }

        public long ShipmentID
        {
            get { return this.idField ?? 0; }
        }

        public int TotalPieces
        {
            get { return this.piecesField; }
        }

        public string PONumber
        {
            get
            {
                if (this.orderItemsField.Length > 0)
                    return this.orderItemsField[0].PoNumber;
                else
                    return string.Empty;
            }
        }

        ShipmentStatus IShipment.Status
        {
            get { return this.pickupStatusField; }
        }

        public int Flags
        {
            get { return 0; }
        }

        #endregion
    }
}
