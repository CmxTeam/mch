﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.OnHand
{
    public interface IReference
    {
        string ReferenceType { get; set; }
        string ReferenceNumber { get; set; }
        long ID { get; set; }
        int ReferenceTypeID { get; set; }
    }
}
