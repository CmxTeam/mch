﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.Communication.OnHand
{
    public interface IShipment
    {
        string ShipmentNO { get; }
        string Destination { get; }
        string Origin { get; }
        string MOT { get; }
        ShipmentStatus Status { get; }
        string CustomerCode { get; }
        string Vendor { get; }
        string TruckingCompany { get; }
        long ShipmentID { get; }
        int TotalPieces { get; }
        string PONumber { get; }
        int Flags { get; }
    }
}
