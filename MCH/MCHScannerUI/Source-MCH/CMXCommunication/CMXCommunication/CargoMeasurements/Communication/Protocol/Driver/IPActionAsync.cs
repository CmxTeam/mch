﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    internal class IPActionAsync : CommunicatorAction
    {
        public IPActionAsync(IMettlerCommunicatorClient communicationClient)
            : base(communicationClient)
        { }

        public override CommunicationStateEnum Perform()
        {
            this.CommunicationClient.SendMettlerIPAsync();

            return CommunicationStateEnum.Connected;
        }
    }
}
