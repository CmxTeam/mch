﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    internal class DisconnectAction : CommunicatorAction
    {
        public DisconnectAction(IMettlerCommunicatorClient communicationClient)
            : base(communicationClient)
        { }

        public override CommunicationStateEnum Perform()
        {
            this.CommunicationClient.Disconnect();

            return CommunicationStateEnum.Disconnected;
        }
    }
}
