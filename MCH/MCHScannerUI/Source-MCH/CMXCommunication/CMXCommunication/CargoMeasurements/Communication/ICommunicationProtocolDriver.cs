﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;


namespace CargoMatrix.Communication.CargoMeasurements.Communication
{
    public interface ICommunicationProtocolDriver
    {
        IMettlerCommunicatorClient CommunicationClient { get; }

        CommunicationStateEnum State { get; }
        
        void Reset();

        void Reset(DimensionerDevice dimensionerDevice);

        void PerformAction();

        void ShutDown();
    }
}
