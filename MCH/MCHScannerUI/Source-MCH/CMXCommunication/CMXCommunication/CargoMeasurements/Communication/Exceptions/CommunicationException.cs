﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions
{
    public class CommunicationException : ApplicationException
    {
        ExceptionCodeEnum code;

        public ExceptionCodeEnum Code
        {
            get { return code; }
            set { code = value; }
        }

        public CommunicationException(ExceptionCodeEnum code)
            : base()
        {
            this.code = code;
        }

        public CommunicationException(ExceptionCodeEnum code, string message)
            : base(message)
        {
            this.code = code;
        }

        public CommunicationException(ExceptionCodeEnum code, string message, Exception innerException)
            : base(message, innerException)
        {
            this.code = code;
        }

    }
}
