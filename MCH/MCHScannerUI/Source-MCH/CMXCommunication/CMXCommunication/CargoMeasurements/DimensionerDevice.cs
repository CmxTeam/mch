﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace CargoMatrix.Communication.CargoMeasurements
{
    public sealed class DimensionerDevice
    {
        int iD;
        string name;
        IPAddress ipAddress;
        IPAddress serverIP;
        int serverPort;
        bool isDefault;

        public bool IsDefault
        {
            get { return isDefault; }
            set { isDefault = value; }
        }

        public int ID
        {
            get { return iD; }
        }
        
        public int ServerPort
        {
            get { return serverPort; }
            set { serverPort = value; }
        }

        public IPAddress ServerIP
        {
            get { return serverIP; }
            set { serverIP = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public IPAddress IpAddress
        {
            get { return ipAddress; }
            set { ipAddress = value; }
        }
      
        private DimensionerDevice(){}

        //public DimensionerDevice(string name, IPAddress ipAddress, IPAddress serverIP, int serverPort)
        //{
        //    this.name = name;
        //    this.ipAddress = ipAddress;
        //    this.serverIP = serverIP;
        //    this.serverPort = serverPort;
        //}

        //public DimensionerDevice(int iD, string name, string ipAddress, string serverIP, int serverPort)
        //    : this(name, IPAddress.Parse(ipAddress), IPAddress.Parse(serverIP), serverPort)
        //{
        //    this.iD = iD;
        //}
        
        //public DimensionerDevice(string name, string ipAddress, string serverIP, int serverPort)
        //    : this(name, IPAddress.Parse(ipAddress), IPAddress.Parse(serverIP), serverPort)
        //{ }


        internal DimensionerDevice(WSCargoDimensioner.DimensioningDevice device)
        {
            this.iD = device.Id;
            this.name = device.Name;
            this.ipAddress = IPAddress.Parse(device.IpAddress);
            this.serverIP = IPAddress.Parse(device.ServerIpAddress);
            this.serverPort = device.ServerPort;
            this.isDefault = device.IsDefault;
        }

        internal WSCargoDimensioner.DimensioningDevice ToWSDimensioningDevice()
        {
            var result = new WSCargoDimensioner.DimensioningDevice();

            result.Id = this.ID;
            result.Name = this.Name;
            result.IpAddress = this.IpAddress.ToString();
            result.ServerIpAddress = this.ServerIP.ToString();
            result.ServerPort = this.ServerPort;
            result.IsDefault = this.IsDefault;

            return result;
        }
    }
}
