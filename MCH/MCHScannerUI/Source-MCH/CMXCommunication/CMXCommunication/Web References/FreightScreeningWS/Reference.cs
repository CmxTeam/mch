﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.CompactFramework.Design.Data, Version 2.0.50727.1433.
// 
namespace CargoMatrix.Communication.FreightScreeningWS {
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="FreightScreeningWSSoap", Namespace="http://tempuri.org/")]
    public partial class FreightScreeningWS : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        /// <remarks/>
        public FreightScreeningWS() {
            this.Url = "http://10.0.0.40/scannerwebservice/FreightScreeningWS.asmx";
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/HelloWorld", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string HelloWorld() {
            object[] results = this.Invoke("HelloWorld", new object[0]);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginHelloWorld(System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("HelloWorld", new object[0], callback, asyncState);
        }
        
        /// <remarks/>
        public string EndHelloWorld(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetFreightScreeningTasks", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public TaskItem[] GetFreightScreeningTasks(string user, string gateway, string connection) {
            object[] results = this.Invoke("GetFreightScreeningTasks", new object[] {
                        user,
                        gateway,
                        connection});
            return ((TaskItem[])(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginGetFreightScreeningTasks(string user, string gateway, string connection, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("GetFreightScreeningTasks", new object[] {
                        user,
                        gateway,
                        connection}, callback, asyncState);
        }
        
        /// <remarks/>
        public TaskItem[] EndGetFreightScreeningTasks(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((TaskItem[])(results[0]));
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FreightPhotoCapture))]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class TaskItem {
        
        private string taskPictureField;
        
        private int directionField;
        
        private System.Nullable<int> totalPiecesField;
        
        private Modes modeField;
        
        private TaskType taskTypeField;
        
        private string taskPrefixField;
        
        private string taskIDField;
        
        private string statusField;
        
        private char statusCodeField;
        
        private byte[] logoField;
        
        private string originField;
        
        private string destinationField;
        
        private string carrierField;
        
        private string actualBillField;
        
        private string referenceField;
        
        private string line2Field;
        
        private string line3Field;
        
        /// <remarks/>
        public string taskPicture {
            get {
                return this.taskPictureField;
            }
            set {
                this.taskPictureField = value;
            }
        }
        
        /// <remarks/>
        public int direction {
            get {
                return this.directionField;
            }
            set {
                this.directionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<int> totalPieces {
            get {
                return this.totalPiecesField;
            }
            set {
                this.totalPiecesField = value;
            }
        }
        
        /// <remarks/>
        public Modes mode {
            get {
                return this.modeField;
            }
            set {
                this.modeField = value;
            }
        }
        
        /// <remarks/>
        public TaskType taskType {
            get {
                return this.taskTypeField;
            }
            set {
                this.taskTypeField = value;
            }
        }
        
        /// <remarks/>
        public string taskPrefix {
            get {
                return this.taskPrefixField;
            }
            set {
                this.taskPrefixField = value;
            }
        }
        
        /// <remarks/>
        public string taskID {
            get {
                return this.taskIDField;
            }
            set {
                this.taskIDField = value;
            }
        }
        
        /// <remarks/>
        public string status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
        
        /// <remarks/>
        public char statusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] logo {
            get {
                return this.logoField;
            }
            set {
                this.logoField = value;
            }
        }
        
        /// <remarks/>
        public string origin {
            get {
                return this.originField;
            }
            set {
                this.originField = value;
            }
        }
        
        /// <remarks/>
        public string destination {
            get {
                return this.destinationField;
            }
            set {
                this.destinationField = value;
            }
        }
        
        /// <remarks/>
        public string carrier {
            get {
                return this.carrierField;
            }
            set {
                this.carrierField = value;
            }
        }
        
        /// <remarks/>
        public string actualBill {
            get {
                return this.actualBillField;
            }
            set {
                this.actualBillField = value;
            }
        }
        
        /// <remarks/>
        public string reference {
            get {
                return this.referenceField;
            }
            set {
                this.referenceField = value;
            }
        }
        
        /// <remarks/>
        public string line2 {
            get {
                return this.line2Field;
            }
            set {
                this.line2Field = value;
            }
        }
        
        /// <remarks/>
        public string line3 {
            get {
                return this.line3Field;
            }
            set {
                this.line3Field = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.CargoMatrix.com/")]
    public enum Modes {
        
        /// <remarks/>
        Piece,
        
        /// <remarks/>
        Count,
        
        /// <remarks/>
        Slac,
        
        /// <remarks/>
        NA,
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public enum TaskType {
        
        /// <remarks/>
        FREIGHT_PHOTO_CAPTURE_HOUSEBILL,
        
        /// <remarks/>
        FREIGHT_PHOTO_CAPTURE_MASTERBILL,
        
        /// <remarks/>
        UNKNOWN,
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ImageObject {
        
        private byte[] m_rawImageField;
        
        private string m_timeField;
        
        private string m_locationField;
        
        private string m_useridField;
        
        private string m_reasonField;
        
        private int m_indexField;
        
        private int m_pieceNoField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] m_rawImage {
            get {
                return this.m_rawImageField;
            }
            set {
                this.m_rawImageField = value;
            }
        }
        
        /// <remarks/>
        public string m_time {
            get {
                return this.m_timeField;
            }
            set {
                this.m_timeField = value;
            }
        }
        
        /// <remarks/>
        public string m_location {
            get {
                return this.m_locationField;
            }
            set {
                this.m_locationField = value;
            }
        }
        
        /// <remarks/>
        public string m_userid {
            get {
                return this.m_useridField;
            }
            set {
                this.m_useridField = value;
            }
        }
        
        /// <remarks/>
        public string m_reason {
            get {
                return this.m_reasonField;
            }
            set {
                this.m_reasonField = value;
            }
        }
        
        /// <remarks/>
        public int m_index {
            get {
                return this.m_indexField;
            }
            set {
                this.m_indexField = value;
            }
        }
        
        /// <remarks/>
        public int m_pieceNo {
            get {
                return this.m_pieceNoField;
            }
            set {
                this.m_pieceNoField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class FreightPhotoCaptureItem {
        
        private string reasonField;
        
        private ImageObject[] imagelistField;
        
        private int reasonTypeField;
        
        /// <remarks/>
        public string reason {
            get {
                return this.reasonField;
            }
            set {
                this.reasonField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public ImageObject[] imagelist {
            get {
                return this.imagelistField;
            }
            set {
                this.imagelistField = value;
            }
        }
        
        /// <remarks/>
        public int ReasonType {
            get {
                return this.reasonTypeField;
            }
            set {
                this.reasonTypeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class FreightPhotoCapture : TaskItem {
        
        private FreightPhotoCaptureItem[] itemsListField;
        
        /// <remarks/>
        public FreightPhotoCaptureItem[] itemsList {
            get {
                return this.itemsListField;
            }
            set {
                this.itemsListField = value;
            }
        }
    }
}
