﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Parsing.Results
{
    public class ScanObjectLegacy
    {
        public string BarcodeData { get; set; }
        public BarcodeTypes BarcodeType { get; set; }
        public string CarrierNumber { get; set; }
        public string HouseBillNumber { get; set; }
        public string Location { get; set; }
        public string MasterBillNumber { get; set; }
        public int PieceNumber { get; set; }
        public int PinNumber { get; set; }
        public string PrinterName { get; set; }
        public string UldNumber { get; set; }
        public int UserNumber { get; set; }

        public enum BarcodeTypes
        {
            NA = 0,
            HouseBill = 1,
            MasterBill = 2,
            Uld = 3,
            Door = 4,
            Area = 5,
            Truck = 6,
            User = 7,
            ScreeningArea = 8,
            Printer = 9,
        }
    }

    
}
