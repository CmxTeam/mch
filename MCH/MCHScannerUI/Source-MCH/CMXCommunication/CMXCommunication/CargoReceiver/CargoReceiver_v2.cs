﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.ExceptionManager;
using CargoMatrix.Communication.WSCargoReceiver_v2;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication
{
    public class CargoReceiver_V2
    {
        private WSCargoreceiver_v2 service;
        private static CargoReceiver_V2 _instance;
        private string asmxName = "WSCargoReceiver_v2.asmx";
        private string failMessage = "Operation Failed!";

        private string connectionName { get { return WebServiceManager.Instance().m_connectionString; } }
        private int userID { get { return WebServiceManager.Instance().m_user.userID; } }
        public static CargoReceiver_V2 Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CargoReceiver_V2();
                return _instance;
            }
        }
        private CargoReceiver_V2()
        {
            service = new WSCargoreceiver_v2();
            service.Url = "http://10.0.0.23/WSCargoReceiver_v2.asmx"; //Settings.Instance.URLPath + asmxName;

        }


        public IEnumerable<IULDType> GetUldTypes(bool getLoose)
        {
            try
            {
                if (getLoose)
                    return service.GetUldTypes();
                else
                    return service.GetUldTypes().Where<IULDType>(uld => !string.Equals(uld.ULDName, "loose", StringComparison.OrdinalIgnoreCase));
            }
            catch (Exception ex)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 20001);
                return Enumerable.Empty<IULDType>();
            }
        }

    }
}
