﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.CargoDischarge
{
    public class DischargeShipmentConditionType : IShipmentConditionType
    {
        ScannerUtilityWS.ShipmentConditionType wsShipmentConditionType;

        public DischargeShipmentConditionType(ScannerUtilityWS.ShipmentConditionType wsShipmentConditionType)
        {
            this.wsShipmentConditionType = wsShipmentConditionType;
        }



        #region IShipmentConditionType Members

        public string ConditionTypeName
        {
            get 
            { 
                return this.wsShipmentConditionType.ConditionTypeName; 
            }
        }

        public int ConditionTypeId
        {
            get 
            {
                return this.wsShipmentConditionType.ConditionTypeId;     
            }
        }

        public string ConditionTypeCode
        {
            get 
            {
                return this.wsShipmentConditionType.ConditionTypeCode;
            }
        }

        #endregion
    }
}
