﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication
{
    public class FreightScreeningManager
    {
        FreightScreeningWS.FreightScreeningWS fsWS;
        private static FreightScreeningManager instance;
        public static FreightScreeningManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FreightScreeningManager();
                }

                return instance;
            }

        }
        private FreightScreeningManager()
        {
            fsWS = new CargoMatrix.Communication.FreightScreeningWS.FreightScreeningWS();
        }

        public CargoMatrix.Communication.Data.TaskItem[] GetFreightScreeningList()
        {
            //CargoMatrix.Communication.Data.TaskItem[] list = null;
            //try
            //{
            //    list = fsWS.GetFreightScreeningTasks(CargoMatrix.Communication.WebServiceManager.GetUserID(), WebServiceManager.Instance().m_gateway, WebServiceManager.Instance().m_connectionString);
            //}
            //catch (Exception ex)
            //{
            //    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 12001);
            //    return null;
            //}

            //return list;

            CargoMatrix.Communication.Data.TaskItem[] list = new CargoMatrix.Communication.Data.TaskItem[1];
            list[0] = new CargoMatrix.Communication.Data.TaskItem();
            list[0].actualBill = "3KM5362";
            list[0].destination = "ORD";
            list[0].origin = "HKG";
            list[0].reference = "HB# HKG-3KM5362-ORD";
            list[0].statusCode = 'N';
            list[0].status = "Not Started";
            list[0].taskID = "100";
            list[0].taskType = CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL;
            list[0].totalPieces = 10;
            list[0].line2 = "PCS: 10, WT: 58KG";
            list[0].line3 = "LOC: N/A";
            list[0].mode = CargoMatrix.Communication.Data.WSScanner.Modes.Count;

            return list;

        }
    }
}
