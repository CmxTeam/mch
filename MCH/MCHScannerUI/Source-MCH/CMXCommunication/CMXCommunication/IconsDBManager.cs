﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;

namespace CargoMatrix.Communication
{
    class IconsDBManager
    {
        private IconsDBManager m_instance = null;
        private string imgConnectionString;
        private IconsDBManager Instance
        {
            get
            {
                if (m_instance == null)
                    m_instance = new IconsDBManager();
                return m_instance;
            }
        }
        private IconsDBManager()
        {
            string path;
            path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            imgConnectionString = "Data Source =  " + path + "\\Icons.sdf; Password =''";
        
        }

        public bool InsertImage(DateTime TimeStamp, byte[] Image)
        {

            try
            {   
                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO Icons ([Image], [TimeStamp] ) Values(@pic, @time )";

                        SqlCeParameter pic = new SqlCeParameter("@pic", SqlDbType.Image);
                        pic.Value = Image;
                        cmd.Parameters.Add(pic);

                        SqlCeParameter time = new SqlCeParameter("@time", SqlDbType.DateTime);
                        time.Value = TimeStamp;
                        cmd.Parameters.Add(time);

                        cmd.ExecuteNonQuery();
                    }

                    
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 12001);
                return false;
                
            }
            return true;
        }

        public byte[] GetImageFromDB(int RecID)
        {
            byte[] img = null;
            try
            {
                
                DataSet ds = new DataSet("GetImage");

                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "select image from Icons where RecID = '" + RecID + "'";
                        SqlCeDataAdapter adapter = new SqlCeDataAdapter(cmd);
                        adapter.Fill(ds);
                        adapter.Dispose();
                        if (ds.Tables.Count > 0)
                        {

                            if (ds.Tables[0].Rows.Count > 0)
                            {

                                DataRow dr = ds.Tables[0].Rows[0];
                                img = dr["Data"] as byte[];

                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 12002);
                return null;
            }
        
           return img;
        }

        public void RemoveImageFromDB(int RecID)
        {

            
            try
            {
                

                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "delete from Icons where RecID = '" + RecID + "'";

                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 12003);
            }

          

        }

        public void RemoveAll()
        {

           
            try
            {
                
                using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(m_instance.imgConnectionString))
                {
                    conn.Open();
                    using (SqlCeCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "delete from Icons";

                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 12004);
            }



        }

    }

    
}
