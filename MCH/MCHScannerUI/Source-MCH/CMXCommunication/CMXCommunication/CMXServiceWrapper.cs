﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Communication
{
    public abstract class CMXServiceWrapper
    {
        private const int NoOfTrys = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        /// <param name="excCode">Exception code to Log</param>
        public void Invoke(Action action, int excCode)
        {
            int i = 0;
            while (i < NoOfTrys)
            {
                try
                {
                    action();
                    return;
                }
                catch (System.Net.WebException exc)
                {
                    if ((exc.Message.Contains("The remote name could not be resolved") || exc.Message.Contains("Unable to read data from the transport connection")) && (i < NoOfTrys - 1))
                    {
                        i++;
                        continue;
                    }
                    else
                    {
                        Cursor.Current = Cursors.Default;
                        if (System.Windows.Forms.DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Unable to connect... do you want to retry ?", "Retry", CargoMatrix.UI.CMXMessageBoxIcon.Question, System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.DialogResult.Yes))
                        {
                            i = 0;
                            continue;
                        }
                        else
                            break;
                    }
                }
                catch (Exception ex)
                {
                    CargoMatrix.Communication.ScannerUtility.Instance.SaveScannerLog(excCode, ex.ToString());
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, excCode);
                    break;
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="R"></typeparam>
        /// <param name="action"></param>
        /// <param name="excCode">Exception code to Log</param>
        /// <param name="refaultReturn">default value to return</param>
        /// <returns></returns>
        public R Invoke<R>(Func<R> action, int excCode, R defaultReturn)
        {
            int i = 0;

            while (i < NoOfTrys)
            {
                try
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        return action();
                    }
                    catch (System.Net.WebException exc)
                    {
                        if ((exc.Message.Contains("The remote name could not be resolved") || exc.Message.Contains("Unable to read data from the transport connection")) && (i < NoOfTrys))
                        {
                            i++;
                            continue;
                        }
                        else
                        {
                            Cursor.Current = Cursors.Default;
                            if (System.Windows.Forms.DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Unable to connect... do you want to retry ?", "Retry", CargoMatrix.UI.CMXMessageBoxIcon.Question, System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.DialogResult.Yes))
                            {
                                i = 0;
                                continue;
                            }
                            else
                                break;
                        }
                    }
                    finally
                    {
                        Cursor.Current = Cursors.Default;
                    }
                }
                catch (Exception ex)
                {
                    CargoMatrix.Communication.ScannerUtility.Instance.SaveScannerLog(excCode, ex.ToString());
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, excCode);
                    break;
                }
            }
            return defaultReturn;
        }

        protected string failMessage = "Operation Failed!";
        protected string connectionName { get { return WebServiceManager.Instance().m_connectionString; } }
        protected string gateway { get { return WebServiceManager.Instance().m_gateway; } }
        protected string userName { get { return WebServiceManager.Instance().m_user.UserName; } }
        protected int userId { get { return WebServiceManager.Instance().m_user.userID; } }

    }
}
