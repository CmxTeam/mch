﻿using System;
using System.ComponentModel;

namespace CargoMatrix.Communication.Messageing
{
    public class Message : IMessage
    {
        #region IMessage Members

        public string Body
        {
            get;set;
        }

        public DateTime DateCreated
        {
            get;
            set;

        }

        public DateTime DateReceived
        {
            get;
            set;

        }

        public int ID
        {
            get;
            set;

        }

        public bool IsUnread
        {
            get;
            set;

        }

        public MessagePriority Priority
        {
            get;
            set;

        }

        public int[] ReceiverIds
        {
            get;
            set;

        }

        public bool ResponseRequired
        {
            get;
            set;

        }

        public int ResponseTo
        {
            get;
            set;

        }

        public int SenderId
        {
            get;
            set;

        }

        public string SenderName
        {
            get;
            set;
        }

        public MessageType MessageDirection
        {
            get;
            set;
        }

        #endregion
    }
}