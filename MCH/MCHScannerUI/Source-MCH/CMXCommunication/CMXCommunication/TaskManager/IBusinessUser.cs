﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.TaskManager
{
    public interface IBusinessUser
    {
        string UserId
        {
            get;
        }

       
        string FirstName
        {
            get;
        }

        
        string LastName
        {
            get;
        }
    }
}
