﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common
{
    public interface IShipmentCondition
    {
        IShipmentConditionType[] Conditions
        {
            get;
        }

        int Pieces
        {
            get;
        }

        int PieceId
        {
            get;
        }
    }
}
