﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common
{
    public interface IShipmentConditionType
    {
        string ConditionTypeName 
        {
            get ;
        }
        
        int ConditionTypeId 
        {
            get;
        }

        string ConditionTypeCode
        {
            get;
        }
    }
}
