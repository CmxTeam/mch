﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSAQM;
using CargoMatrix.ExceptionManager;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication
{
    public class AQM : CMXServiceWrapper
    {
        private WSAQM.Aqm service;
        private static AQM _instance;
        private string asmxName = "aqm.asmx";

        public static AQM Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AQM();
                return _instance;
            }
        }
        
        private AQM()
        {
            service = new CargoMatrix.Communication.WSAQM.Aqm();
            service.Url = Settings.Instance.URLPath + asmxName;

        }

        public IAQMInfo GetAQMList(string carrier, string awbNo, AQMType type)
        {
            Func<IAQMInfo> f = () =>
                {
                    if (type == AQMType.MAWB)
                        return this.service.GetMasterBillAqmDetail(connectionName, carrier, awbNo);
                    else
                        return this.service.GetHouseBillAqmDetail(connectionName, awbNo);

                };
            return Invoke<IAQMInfo>(f, 310001, null);
        }

        public bool CreateMawbAQM(string carrier, string mawbNo, string irregCode, string remark)
        {
            Func<bool> f = ()=>
            {
                this.service.CreateAqm(connectionName, 16, gateway, userName, string.Empty, carrier, mawbNo, irregCode, 0, string.Empty, remark);
                return true;
            };
            return Invoke<bool>(f,310002,false);
        }

        public bool CreateHawbAQM(string hawbNo, string irregCode, int pieces, string excCode, string remark)
        {
            Func<bool> f = ()=>
            {
                this.service.CreateAqm(connectionName, 16, gateway, userName, hawbNo, string.Empty, string.Empty, irregCode, pieces, excCode, remark);
                return true;
            };
            return Invoke<bool>(f, 310003, false);
        }

        public Reference[] GetIrregularityCodes(bool isMaster)
        {
            Func<Reference[]> f = ()=>
            {
                return this.service.GetAQMIrregularityCodes(connectionName, isMaster ? ShipmentType.MasterBill : ShipmentType.HouseBill);
            };
            return Invoke<Reference[]>(f,310004,new Reference[0]);
        }
    }
}
