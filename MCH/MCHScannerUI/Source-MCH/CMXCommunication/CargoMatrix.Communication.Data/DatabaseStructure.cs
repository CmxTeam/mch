﻿using System.Xml.Serialization;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Web.Services;


namespace CargoMatrix.Communication.Data_v11
{
    
    public class GUIDException// : System.Web.Services.Protocols.SoapException
    {
        public static System.Web.Services.Protocols.SoapException RaiseGUIDException()
        {
            return new System.Web.Services.Protocols.SoapException("User logged in to another device.", new System.Xml.XmlQualifiedName("GUIDException"));
            
        }
        //private string m_message;
        //public GUIDException()
        //{
        //    this.
        //    //throw new System.Exception("User logged in to another device.");
        //    m_message = "User logged in to another device.";
        //}
        //public new string Message
        //{
        //    get
        //    {
        //        return m_message;
        //    }
        //}
    
    }
    
    
    //public enum UserTypes
    //{

    //    /// <remarks/>
    //    User,

    //    /// <remarks/>
    //    Admin,
    //}
    
    [XmlRoot("User", IsNullable = false)]
    public struct User
    {
        //public string username;
        public string password;
        public string firstName;
        public string lastName;
        public string UserName;
        public int userID;
        public WSScanner.UserTypes GroupID;
        public int errorCode;
        public UserSettings settings;


    }
    [XmlRoot("UserSettings", IsNullable = false)]
    public struct UserSettings
    {
        public string location;
        public int timeOut; // in minutes
        public int timeDifference;

    }
    //public class PeiceItem
    //{
    //    public System.Nullable<int> RecID;
    //    public System.Nullable<int> pieceMode;
    //    public System.Nullable<int> pieceNo;
    //    public System.Nullable<int> locationRecID;
    //    public WSScanner.StorageTypes locationType;
    //    public string locationName;
        
    //}
    [XmlRoot("TaskItem", IsNullable = false)]
    [XmlInclude(typeof(FreightPhotoCapture))]
    //[XmlInclude(typeof(TaskItem))]
    public class TaskItem
    {
        public string taskPicture;
        //public string task;
        //public string description;
        public int direction;
        public System.Nullable<int> totalPieces;
        public WSScanner.Modes mode;
        public TaskType taskType;
        public string taskPrefix;
        public string taskID;
        public string status;
        public char statusCode;

        public byte[] logo;  // for masterbill
        public string origin;
        public string destination;
        public string carrier; // for masterbill
        public string actualBill;  // actual house bill
        //public string actualMasterBill;
        public string reference;
        public string line2, line3, line4;
        //public bool isVisuallyInspected;
        public WSScanner.HouseBillStatuses SpecialStatus;

        public bool IsExpedite;
        //public string screeningStatusDescription;
        //public System.Nullable<int> screeningStatusID;

        public TaskItem()
        {
            taskType = TaskType.UNKNOWN;
            mode = WSScanner.Modes.NA;
            totalPieces = 0;
            IsExpedite = false;
            //isVisuallyInspected = false;
            SpecialStatus = WSScanner.HouseBillStatuses.NA;
        }

    }
    public class MainMenuItem   
    {
        public string Label;
        public int ActionID;
        public string TotalCount;
        public string Icon;
        public string NotCompleted;
        public string NotAssigned;
        public string Completed;
        public string TotalCountByUser;
    }
    //[XmlRoot("CheckInManifestItem", IsNullable = false)]
    //public struct CheckInManifestItem
    //{
    //    public string location;
    //    public string terminal;
    //    public string weight;
    //    public bool alert;
    //}
    [XmlRoot("FreightPhotoCaptureItem", IsNullable = false)]
    public class FreightPhotoCaptureItem
    {
        public FreightPhotoCaptureItem()
        { 
        }
        public FreightPhotoCaptureItem(string Reason, List<ImageObject> ImageList, int reasonType)
        {
            reason = Reason;
            imagelist = ImageList;
            ReasonType = reasonType;
        }
        public bool HasData
        {
            get
            { 
                if(imagelist != null)
                {
                    if(imagelist.Count>0)
                        return true;
                    
                }
                return false;
            }
        }
        public string reason;
        public List <ImageObject> imagelist;
        public int ReasonType;
        //public System.DateTime time;
        //public System.Windows.Forms.ImageList imagelist;
        

    }
    public enum ErrorCode
    { 
        SUCCESS,
        LOGGED_TO_ANOTHER_MACHINE,
        UNKNOWN
    }
    public enum TaskType
    { 
        FREIGHT_PHOTO_CAPTURE_HOUSEBILL,
        FREIGHT_PHOTO_CAPTURE_MASTERBILL,
        UNKNOWN
    }
    //public virtual class TaskObject
    //{ 
    //}

    [XmlRoot("FreightPhotoCapture", IsNullable = false)]
    public class FreightPhotoCapture : TaskItem
    {
        
        //public string piece;
        //public string location;
        //public string rack;
        //public string bin;
        //public bool isAlreadyExists;
        public List <FreightPhotoCaptureItem> itemsList;


        
        public FreightPhotoCapture()
        {
            //isAlreadyExists = false;
        }
        public bool HasData
        {
            get
            {
                if (itemsList != null)
                {
                    for (int i = 0; i < itemsList.Count; i++)
                    {
                        if (itemsList[i].HasData)
                            return true;
                    }
                }
                return false;
            }
        }
        

    }
    public struct ImageObject
    {
        //public byte m_flag;
        public byte[] m_rawImage;
        //public System.DateTime m_time;
        public string m_time;
        public string m_location;
        public string m_userid;
        public string m_reason;
        public int m_index;
        public int m_pieceNo;
        public ImageObject(byte[] img, string time, string location, string userid, string reason)
        {
            m_rawImage = img;
            m_time = time;
            //m_flag = flag;
            m_index = -1;
            m_location = location;
            m_userid = userid;
            m_reason = reason;
            m_pieceNo = 0;

        }
        public ImageObject(byte[] img, string time, string location, string userid, string reason, int index)
        {
            m_rawImage = img;
            m_time = time;
            //m_flag = flag;
            m_index = index;
            m_location = location;
            m_userid = userid;
            m_reason = reason;
            m_pieceNo = 0;
        }
    }

    #region housebill region
    public class AddressObject
    {
        public string address;
        public string addressType;
        

    }
    public class HousebillData
    {
        public string housebill;
        public int totalPieces;
        public string[] locations;
        public string[] locationsDetails;
        public string[] dimensions;
        public string senderName;
        //public string senderAddress;
        public string receiverName;
        //public string receiverAddress;
        public string description;
        public string[] history;
        public string weight;
        public AddressObject[] addresses;

    }
    public class ShellData
    {
        public string Name;
        public string Reference;
        public List<ShellPage> m_pages;
        
        public ShellData()
        {
            m_pages = new List<ShellPage>();
        }
        public void AddPage(ShellPage page)
        {
            if (page != null)
                m_pages.Add(page);
        }
       
    }
    public class ShellPage
    {
        public string Name;
        public string Icon;
        public List<ShellItem> m_items;
        
        public ShellPage()
        {
            m_items = new List<ShellItem>();
        }
        public void AddItem(ShellItem item)
        {
            if(item != null)
                m_items.Add(item);
            
        }
        
    }
    public class ShellItem
    {
        public ShellItemType m_type;
        public string m_heading;
        public string m_description;
        public string m_image;
        public ShellItem()
        { 
        
        }
        public ShellItem(ShellItemType type, string heading, string description, string image)
        {
            m_type = type;
            m_heading = heading;
            m_description = description;
            m_image = image;
        
        }
    }
    public enum ShellItemType
    { 
        CONTACT,
        SHELL_ITEM_A,
        SHELL_ITEM_B,
        SHELL_ITEM_C,
        SHELL_IMAGE_ITEM_A,
        SHELL_IMAGE_ITEM_B,
        SHELL_IMAGE_ITEM_C,
        SHELL_ITEM_PHOTO
    }
    #endregion


    public struct ULDTypeObject
    {
        public int UldID;
        public string ULDName;
                
    }
    public struct ULD
    {
        public int UldRecID;
        public string ULDNumber;
    }

}
