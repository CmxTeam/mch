﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CargoMatrix.Rapiscan.WebAPI
{
    /// <summary></summary>
    public class Database
    {

        private static readonly string CONNECTION_STRING = System.Configuration.ConfigurationManager.ConnectionStrings["RapiscanInterfaceDB"].ConnectionString;

        /// <summary></summary>
        public static DataSet ExecuteDataSet(string commandText, Dictionary<String, Object> dict, string[] tableNames)
        {
            var dataSet = new DataSet();
            using (var connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = commandText;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    if (dict != null)
                    {
                        foreach (var entry in dict)
                        {
                            var name = entry.Key;
                            var output = false;
                            var value = entry.Value;
                            if (name.EndsWith(" output"))
                            {
                                output = true;
                                name = name.Split(" "[0])[0];
                            }
                            var param = command.Parameters.AddWithValue(name, value);
                            if (output)
                            {
                                param.Direction = ParameterDirection.Output;
                            }
                        }
                    }
                    if (tableNames == null)
                    {
                        tableNames = new string[] { "Table1" };
                    }
                    dataSet.Load(command.ExecuteReader(), LoadOption.PreserveChanges, tableNames);
                }
                connection.Close();
            }
            return dataSet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandText">Command to execute</param>
        /// <param name="dict">Command parameters.</param>
        /// <param name="tableNames">Table names.</param>
        /// <returns>Resulting data set.</returns>
        public static DataSet ExecuteCommandDataSet(string commandText, Dictionary<String, Object> dict, string[] tableNames)
        {
            var dataSet = new DataSet();
            using (var connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = commandText;
                    command.CommandType = CommandType.Text;
                    command.CommandTimeout = 0;
                    if (dict != null)
                    {
                        foreach (var entry in dict)
                        {
                            var name = entry.Key;
                            var output = false;
                            var value = entry.Value;
                            if (name.EndsWith(" output"))
                            {
                                output = true;
                                name = name.Split(" "[0])[0];
                            }
                            var param = command.Parameters.AddWithValue(name, value);
                            if (output)
                            {
                                param.Direction = ParameterDirection.Output;
                            }
                        }
                    }
                    if (tableNames == null)
                    {
                        tableNames = new string[] { "Table1" };
                    }
                    dataSet.Load(command.ExecuteReader(), LoadOption.PreserveChanges, tableNames);
                }
                connection.Close();
            }
            return dataSet;
        }


        /// <summary></summary>
        public static void ExecuteNonQuery(string commandText, Dictionary<String, Object> dict)
        {
            using (var connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = commandText;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    if (dict != null)
                    {
                        foreach (var entry in dict)
                        {
                            var name = entry.Key;
                            var output = false;
                            var value = entry.Value;
                            if (name.EndsWith(" output"))
                            {
                                output = true;
                                name = name.Split(" "[0])[0];
                            }
                            var param = command.Parameters.AddWithValue(name, value);
                            if (output)
                            {
                                param.Direction = ParameterDirection.InputOutput;
                            }
                        }
                    }
                    command.ExecuteNonQuery();
                    foreach (SqlParameter param in command.Parameters)
                    {
                        if (param.Direction == ParameterDirection.InputOutput)
                        {
                            dict[param.ParameterName] = param.Value;
                        }
                    }
                }
                connection.Close();
            }
        }

        /// <summary></summary>
        public class Row
        {

            /// <summary></summary>
            public static dynamic ToJson(DataRow row)
            {
                var x = new XElement("root");
                foreach (DataColumn col in row.Table.Columns)
                {
                    string name = col.ColumnName;
                    string value = null;
                    if (row[col] != DBNull.Value)
                    {
                        value = row[col].ToString();
                    }
                    x.Add(new XElement(name, value));
                }
                var j = JsonConvert.SerializeXNode(x, Newtonsoft.Json.Formatting.None, true);
                return j;
            }
        }

        //public class Col {
        //	public static dynamic IsNull(dynamic dbNullable, dynamic newValue) {
        //		dynamic value = newValue;
        //		if (dbNullable != DBNull.Value) {
        //			value = dbNullable.ToString();
        //		}
        //		return value;
        //	}
        //}











    }
}