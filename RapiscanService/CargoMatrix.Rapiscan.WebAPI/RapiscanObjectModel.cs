﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CargoMatrix.Rapiscan.WebAPI {

	/// <summary></summary>
	public class api_exception {
		/// <summary></summary>
		public int exceptionId;
		/// <summary></summary>
		public string code;
		/// <summary></summary>
		public string message;
		/// <summary></summary>
		public static api_exception parse(XElement x) {
			return new api_exception() {
				exceptionId = int.Parse(x.Element("exceptionId").Value),
				code = x.Element("code").Value,
				message = x.Element("message").Value
			};
		}
	}

	///// <summary></summary>
	//public class api_token {
	//	/// <summary></summary>
	//	public static string parseGuid(string authorizationToken) {
	//		return authorizationToken.Trim().Split(char.Parse(" ")).LastOrDefault() + "";
	//	}
	//}



	/// <summary></summary>
	public class api_machine_bootup_authorize_response {
		/// <summary></summary>
		public string authorization;
		/// <summary></summary>
		public static api_machine_bootup_authorize_response parse(DataRow row) {
			return new api_machine_bootup_authorize_response() {
				authorization = row["authorization"].ToString()
			};
		}
	}


	/// <summary></summary>
	public class api_machine_bootup_load_response {
		/// <summary></summary>
		public string acceptedImageTypes;
		/// <summary></summary>
		public bool clearQueue;
		/// <summary></summary>
		public string realtimeUrl;
		/// <summary></summary>
		public static api_machine_bootup_load_response parse(DataRow row) {
			return new api_machine_bootup_load_response() {
				acceptedImageTypes = row["acceptedImageTypes"].ToString(),
				clearQueue = (bool)row["clearQueue"],
				realtimeUrl = row["realtimeUrl"].ToString()
			};
		}
	}

	/// <summary></summary>
	public class api_machine_bootup_log_request_body {
		/// <summary></summary>
		public string serial;
		/// <summary></summary>
		public string model;
		/// <summary></summary>
		public int imageCount;
		/// <summary></summary>
		public string comments;
		/// <summary></summary>
		public int scanCount;
		/// <summary></summary>
		public XElement ToXml() {
			return new XElement("root",
				new XElement("serial", serial),
				new XElement("model", model),
				new XElement("imageCount", imageCount),
				new XElement("comments", comments),
				new XElement("scanCount", scanCount)
			);
		}
	}


	/// <summary></summary>
	public class api_machine_item_enter_request_body {
		/// <summary></summary>
		public Int64 time;
		/// <summary></summary>
		public string operatorId;
		/// <summary></summary>
		public XElement ToXml() {
			return new XElement("root",
				new XElement("time", time),
				new XElement("operatorId", operatorId)
			);
		}
	}


	/// <summary></summary>
	public class api_machine_item_enter_response {
		/// <summary></summary>
		public Int64 time;
		/// <summary></summary>
		public response_reference reference;
		/// <summary></summary>
		public string operatorId;
		/// <summary></summary>
		public class response_reference {
			/// <summary></summary>
			public string name;
			/// <summary></summary>
			public string id;
			/// <summary></summary>
			public static response_reference parse(DataRow row) {
				return new response_reference() {
					name = row["reference.name"].ToString(),
					id = row["reference.id"].ToString()
				};
			}
		}

		/// <summary></summary>
		public static api_machine_item_enter_response parse(DataRow row) {
			return new api_machine_item_enter_response() {
				time = Int64.Parse(row["time"].ToString()),
				reference = response_reference.parse(row),
				operatorId = row["operatorId"].ToString()
			};
		}
	}





	/// <summary></summary>
	public class api_machine_item_scan_request_body {
		/// <summary></summary>
		public Int64 time;
		/// <summary></summary>
		public request_reference reference;
		/// <summary></summary>
		public bool clear;
		/// <summary></summary>
		public string operatorId;
		/// <summary></summary>
		public request_replay[] replay;
		/// <summary></summary>
		public string comments;
		/// <summary></summary>
		public List<List<List<List<int>>>> rectangles;
		/// <summary></summary>
		public class request_reference {
			/// <summary></summary>
			public string name;
			/// <summary></summary>
			public string id;
		}
		/// <summary></summary>
		public class request_replay {
			/// <summary></summary>
			public int usageId;
			/// <summary></summary>
			public int pageId;
			/// <summary></summary>
			public Int64 time;
			/// <summary></summary>
			public bool keyDown;
		}
		/// <summary></summary>
		public XElement ToXml() {
			var x = new XElement("root",
				new XElement("time", time),
				new XElement("reference",
					new XElement("name", reference.name),
					new XElement("id", reference.id)
				),
				new XElement("clear", clear),
				new XElement("operatorId", operatorId)
			);
			if (replay != null) {
				foreach (var it in replay) {
					var xReply = new XElement("replay",
						new XElement("usageId", it.usageId),
						new XElement("pageId", it.pageId),
						new XElement("time", it.time),
						new XElement("keyDown", it.keyDown)
					);
					x.Add(xReply);
				}
			}
			x.Add(new XElement("comments", comments));
			if (rectangles != null) {
				foreach (var image in rectangles) {
					var imageIndex = rectangles.IndexOf(image);
					foreach (var rectangle in image) {
						var rectangleIndex = image.IndexOf(rectangle);
						if (rectangle.Count == 2 && rectangle[0].Count == 2 && rectangle[1].Count == 2) {
							var xRectangle = new XElement("rectangle", 
								new XElement("imageIndex", imageIndex),
								new XElement("rectangleIndex", rectangleIndex),
								new XElement("x1", rectangle[0][0]),
                                new XElement("y1", rectangle[0][1]),
								new XElement("x2", rectangle[1][0]),
								new XElement("y2", rectangle[1][1])
							);
							x.Add(xRectangle);
						}
					}
				}
			}
			return x;
		}
	}

	/// <summary></summary>
	public class api_user_login_request_body {
		/// <summary></summary>
		public string id;
		/// <summary></summary>
		public XElement ToXml() {
			var x = new XElement("root",
				new XElement("id", id)
			);
			return x;
		}
	}

	/// <summary></summary>
	public class api_user_logout_request_body {
		/// <summary></summary>
		public string id;
		/// <summary></summary>
		public XElement ToXml() {
			var x = new XElement("root",
				new XElement("id", id)
			);
			return x;
		}
	}

	/// <summary></summary>
	public class api_machine_malfunction_request_body : List<api_machine_malfunction_request_body_item> {
		/// <summary></summary>
		public XElement ToXml() {
			var x = new XElement("root");
			foreach (var it in this) {
				x.Add(new XElement("malfunction",
					new XElement("id", it.id),
					new XElement("severity", it.severity),
					new XElement("message", it.message)
				));
			}
			return x;
		}
	}

	/// <summary></summary>
	public class api_machine_malfunction_request_body_item {
		/// <summary></summary>
		public string id;
		/// <summary></summary>
		public int severity;
		/// <summary></summary>
		public string message;
	}
}