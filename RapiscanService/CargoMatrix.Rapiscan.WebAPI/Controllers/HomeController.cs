﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CargoMatrix.Rapiscan.WebAPI.Controllers {
	/// <summary></summary>
	public class HomeController : Controller {
		/// <summary></summary>
		public ActionResult Index() {
			ViewBag.Title = "Home Page";

			return View();
		}
	}
}
