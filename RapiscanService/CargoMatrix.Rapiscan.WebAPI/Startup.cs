﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CargoMatrix.Rapiscan.WebAPI.Startup))]

namespace CargoMatrix.Rapiscan.WebAPI {

	/// <summary></summary>
	public partial class Startup {

		/// <summary></summary>
		public void Configuration(IAppBuilder app) {
			ConfigureAuth(app);
		}
	}
}
