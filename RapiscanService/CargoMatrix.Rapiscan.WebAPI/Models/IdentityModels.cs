﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace CargoMatrix.Rapiscan.WebAPI.Models {
	// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
	/// <summary></summary>
	public class ApplicationUser : IdentityUser {
		/// <summary></summary>
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType) {
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
			// Add custom user claims here
			return userIdentity;
		}
	}

	/// <summary></summary>
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
		/// <summary></summary>
		public ApplicationDbContext()
			: base("DefaultConnection", throwIfV1Schema: false) {
		}

		/// <summary></summary>
		public static ApplicationDbContext Create() {
			return new ApplicationDbContext();
		}
	}
}