﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;

namespace CargoMatrix.Rapiscan.WebAPI.Controllers
{


    /// <summary></summary>
    [RoutePrefix("api")]
    public class RapiscanController : ApiController
    {


        /// <summary>This resource is used to get an authorization token, when a valid shared secret is received in the "authorization" request header.</summary>
        /// <returns>
        /// Executes the api.[Machine.Bootup.Authorize] stored procedure passing in the @sharedSecret (from the authorization request header).
        /// The stored procedure validates the @sharedSecret, generates a new GUID, and inserts data into the dbo.[AuthorizationTokens] table.
        /// Returns a result containing an "authorization" GUID to be used for subsequent requests to the Rapiscan API.
        /// </returns>
        [Route("authorization/tokens"), HttpPost]
        public dynamic api_machine_bootup_authorize()
        {
            try
            {
                var authorization = Request.Headers.Authorization == null ? "" : Request.Headers.Authorization.ToString() + "";
                var dict = new Dictionary<string, object>() { { "@sharedSecret", authorization } };
                var row = Database.ExecuteDataSet("MachineBootupAuthorize", dict, null).Tables[0].Rows[0];
                var o = api_machine_bootup_authorize_response.parse(row);
                return o;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }


        /// <summary>This resource is used to send device information, including machine serial and odometer info.  NOTE:  A valid authorization token must be provided in "authorization" request header.</summary>
        /// <returns>
        /// Executes the api.[Machine.Bootup.Log] stored procedure passing in the @authorizationToken (from the authorization request header), and the serial, model, imageCount, comments, and scanCount from the request body.
        /// The stored procedure parses the request body, validates the machine serial, ensures that the received machine "serial" and received machine "model" are the consistent according to the dbo.[Machines] table, validates "imageCount" to ensure a value greater than zero, associates the machine with the authorization token, inserts data into the dbo.[Machine.Logs] table.
        /// Returns a 201 Created status code.
        /// </returns>
        [Route("devices"), HttpPost]
        public dynamic api_machine_bootup_log([FromBody]api_machine_bootup_log_request_body body)
        {
            try
            {
                var authorization = Request.Headers.Authorization.Parameter + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@request", body.ToXml().ToString() } };
                Database.ExecuteNonQuery("MachineBootupLog", dict);
                return Created("", "");
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }


        /// <summary>This resource is used to retrieve the settings that the X-Ray machine should use for accepted image types, whether or not to clear the image queue, and the realtime URL.  NOTE:  A valid authorization token must be provided in "authorization" request header.</summary>
        /// <returns>
        /// Executes the api.[Machine.Bootup.Load] stored procedure passing in the @authorizationToken (from the authorization request header).
        /// The stored procedure validates the @authorizationToken, retreives the data from the dbo.[Machines] table.  If any of values are NULL, then the data is retrieved from the dbo.[Config] and/or dbo.[Config.ImageTypes] tables.
        /// Returns a result containing "acceptedImageTypes", "clearQueue", and "realtimeUrl".
        /// </returns>
        [Route("configuration"), HttpGet]
        public dynamic api_machine_bootup_load()
        {
            try
            {
                var authorization = Request.Headers.Authorization.Parameter + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization } };
                var row = Database.ExecuteDataSet("MachineBootupLoad", dict, null).Tables[0].Rows[0];
                var o = api_machine_bootup_load_response.parse(row);
                return o;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }



        /// <summary>This resource is used to notify when an item has entered the X-Ray machine and includes related information in the request.  This creates a new reference id (transaction) and associates it with the item.  NOTE:  A valid authorization token must be provided in "authorization" request header.</summary>
        /// <param name="scanId"></param>
        /// <param name="body"></param>
        /// <returns>
        /// Executes the api.[Machine.Item.Enter] stored procedure passing in the @authorizationToken (from the authorization request header), the "scanId" from the request URI, and the "time" and "operatorId" from the request body.
        /// The stored procedure validates the @authorizationToken, parses the request body, validates that a reference name (shippment number) exists for the "operatorId" in the queue (and if so, retreives it), generates a new "reference.id" (transaction) GUID, inserts data into dbo.[Transactions] table.
        /// Returns a result containing the request and embedding the "reference" result which contains the "name" (shipment number) and "id" (transaction GUID).
        /// </returns>
        [Route("scans/{scanId}"), HttpPut]
        public dynamic api_machine_item_enter([FromUri] string scanId, [FromBody]api_machine_item_enter_request_body body)
        {
            try
            {
                var authorization = Request.Headers.Authorization.Parameter + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@scanId", scanId }, { "@request", body.ToXml().ToString(SaveOptions.DisableFormatting) } };
                var row = Database.ExecuteDataSet("MachineItemEnter", dict, null).Tables[0].Rows[0];
                var o = api_machine_item_enter_response.parse(row);

                dict = new Dictionary<string, object>() { { "@ReferenceId", o.reference.id } };
                var transactionRow =
                    Database.ExecuteCommandDataSet("select ReferenceId, MachineId, CreatedDate from Transactions where ReferenceId = @ReferenceId", dict, null).Tables[0].Rows[0];

                //TODO: Call Aram's method.
                string referenceId = o.reference.id;
                string deviceId = transactionRow["MachineId"].ToString();
                DateTime createdDate = (DateTime)transactionRow["CreatedDate"];

                try
                {
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                    HttpClient client = new HttpClient
                                        {
                                            BaseAddress =
                                                new Uri(ConfigurationManager.AppSettings["GlobalWebApiUrl"])
                                        };

                    var uri =
                        string.Format(
                            @"LiveScreeningHubHelper/UpdateTransaction?deviceId={0}&transaction={1}&timestamp={2}",
                            deviceId, referenceId, createdDate.ToString("yyyy/MM/dd HH:mm:ss"));
                    var response = client.GetAsync(uri).Result;
                }
                catch (Exception ex)
                {                    
                }
                

                return o;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }

        /// <summary>This resource is used to receive scan data metrics and decision.  NOTE:  A valid authorization token must be provided in "authorization" request header and a valid reference id must be included in the request body.</summary>
        /// <param name="scanId"></param>
        /// <param name="body"></param>
        /// <returns>
        /// Executes the api.[Machine.Item.Scan] stored procedure passing in the @authorizationToken (from the authorization request header), the "scanId" from the request URI, the "time", "operatorId", "reference.name", "reference.id", "comments", and "clear" from the request body.
        /// The stored procedure validates the @authorizationToken, parses the request body, ensures that the reference id (transaction) exists for the cooresponding machine based on the authorization token, inserts data dbo.[Transactions.Scans] and dbo.[Transactions.Scans.Replays] tables.
        /// Returns a 204 No Content status code.
        /// </returns>
        [Route("scans/{scanId}"), HttpPost]
        public dynamic api_machine_item_scan([FromUri] string scanId, [FromBody]api_machine_item_scan_request_body body)
        {
            try
            {
                var authorization = Request.Headers.Authorization.Parameter + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@scanId", scanId }, { "@request", body.ToXml().ToString(SaveOptions.DisableFormatting) } };
                Database.ExecuteNonQuery("MachineItemScan", dict);
                return null;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }

        /// <summary>This resource is used to receive an uploaded image.  NOTE:  A valid authorization token must be provided in "authorization" request header, a scan Id and image index in the request url, and a valid reference id must be included as a request url query string parameter.</summary>
        /// <param name="scanId"></param>
        /// <param name="imageIndex"></param>
        /// <param name="referenceId"></param>
        /// <returns>
        /// Executes the api.[Machine.Item.UploadImage] stored procedure passing in the @authorizationToken (from the authorization request header), the "scanId" and "imageIndex" from the request URI, the "referenceId" from the request URI query string, the "Content-Type" (from the request header), and the binary image data from the request body.
        /// The stored procedure validates the @authorizationToken, ensures that the reference id (transaction) exists for the cooresponding machine based on the authorization token, inserts data dbo.[Transactions.UploadedImages] table.
        /// Returns a 201 Created status code.
        /// </returns>
        [Route("scans/{scanId}/images/{imageIndex}"), HttpPut]
        public dynamic api_machine_item_uploadImage([FromUri] string scanId, [FromUri] int imageIndex, string referenceId)
        {
            try
            {
                var authorization = Request.Headers.Authorization.Parameter + "";
                var contentType = Request.Content.Headers.ContentType.MediaType + "";
                var bytes = Request.Content.ReadAsByteArrayAsync().Result;
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@scanId", scanId }, { "@imageIndex", imageIndex }, { "@referenceId", referenceId }, { "@contentType", contentType }, { "@request", bytes } };
                Database.ExecuteNonQuery("MachineItemUploadImage", dict);
                return Created("", "");
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }

        /// <summary>This resource is to notify that a user has logged in.  NOTE:  A valid authorization token must be provided in "token" request URI querystring.</summary>
        /// <param name="token"></param>
        /// <param name="body"></param>
        /// <returns>
        /// Executes the api.[User.Login] stored procedure passing in the @authorizationToken (from the token request URI querystring), and the id from the request body.
        /// The stored procedure validates the @authorizationToken, parses the request body, validates that "id" is in the queue, and inserts "Login" related data into the dbo.[Users.Logs] table.
        /// Returns a 204 No Content status code.
        /// </returns>
        [Route("login"), HttpPost]
        public dynamic api_user_login([FromUri] string token, [FromBody]api_user_login_request_body body)
        {
            try
            {
                var authorization = token + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@request", body.ToXml().ToString(SaveOptions.DisableFormatting) } };
                Database.ExecuteNonQuery("UserLogin", dict);
                return null;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }

        /// <summary>This resource is to notify that a user has logged out.  NOTE:  A valid authorization token must be provided in "token" request URI querystring.</summary>
        /// <param name="token"></param>
        /// <param name="body"></param>
        /// <returns>
        /// Executes the api.[User.Logout] stored procedure passing in the @authorizationToken (from the token request URI querystring), and the id from the request body.
        /// The stored procedure validates the @authorizationToken, parses the request body, validates that "id" is in the queue, and inserts "Logout" related data into the dbo.[Users.Logs] table.
        /// Returns a 204 No Content status code.
        /// </returns>
        [Route("logout"), HttpPost]
        public dynamic api_user_logout([FromUri] string token, [FromBody]api_user_logout_request_body body)
        {
            try
            {
                var authorization = token + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@request", body.ToXml().ToString(SaveOptions.DisableFormatting) } };
                Database.ExecuteNonQuery("UserLogout", dict);
                return null;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }


        /// <summary>This resource is to notify that a machine malfunction has occured.  NOTE:  A valid authorization token must be provided in "token" request URI querystring.</summary>
        /// <param name="token"></param>
        /// <param name="body"></param>
        /// <returns>
        /// Executes the api.[Machine.Malfunction] stored procedure passing in the @authorizationToken (from the token request URI querystring), and the related error information from the request body.
        /// The stored procedure validates the @authorizationToken, parses the request body, and inserts data into the dbo.[Machines.Malfunctions] table.
        /// Returns a 204 No Content status code.
        /// </returns>
        [Route("fault"), HttpPost]
        public dynamic api_machine_malfunction([FromUri]string token, [FromBody]api_machine_malfunction_request_body body)
        {
            try
            {
                var authorization = token + "";
                var dict = new Dictionary<string, object>() { { "@authorizationToken", authorization }, { "@request", body.ToXml().ToString(SaveOptions.DisableFormatting) } };
                Database.ExecuteNonQuery("MachineMalfunction", dict);
                return null;
            }
            catch (System.Exception ex)
            {
                return handleException(ex);
            }
        }


        private dynamic handleException(Exception ex)
        {
            var exception = ex as SqlException;
            if (exception != null)
            {
                SqlException sqlEx = exception;
                try
                {
                    XElement x = XElement.Parse(sqlEx.Message);
                    //var rm = new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError) { Content = new StringContent("string content"), ReasonPhrase = "reason phrase" };
                    var o = api_exception.parse(x);
                    //return HttpBadRequest(o);
                    return BadRequest(x.ToString(SaveOptions.DisableFormatting));
                }
                catch (Exception)
                {
                    return InternalServerError(exception);
                }

            }
            return InternalServerError(ex);
        }
    }


    public class Test
    {
        public string Member { get; set; }
    }
}
