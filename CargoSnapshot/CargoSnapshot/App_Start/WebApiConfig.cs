﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace CargoSnapshot.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //var json = config.Formatters.JsonFormatter;
            //json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            //config.Formatters.Remove(config.Formatters.XmlFormatter);
            //config.Filters.Add(new AuthorizeAttribute());
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApiWithActionID",
            //    routeTemplate: "api/{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional });

            //config.Routes.MapHttpRoute(
            //      name: "DefaultApiWithAction",
            //      routeTemplate: "api/{controller}/{action}",
            //      defaults: new { id = RouteParameter.Optional });
            //config.Routes.MapHttpRoute(
            //     name: "Api_Get",
            //     routeTemplate: "{controller}/{action}/{id}",
            //     defaults: new { id = RouteParameter.Optional, action = "Get" });

            //config.Routes.MapHttpRoute(
            //   name: "Api_Post",
            //   routeTemplate: "{controller}/{action}/{id}",
            //   defaults: new { id = RouteParameter.Optional, action = "Post" }
            //);
            ////GlobalConfiguration.Configuration.Filters.Add(new MyBasicAuthenticationFilter());
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            //GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

        }
    }
}