﻿using System.Web;
using System.Web.Optimization;

namespace CargoSnapshot
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/dhtmlx").Include(
                        "~/Scripts/dhtmlx.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageCapturer").Include(
                        "~/Scripts/image-capture.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/helpers.js",
                        "~/Scripts/global/common.js",
                        "~/Scripts/global/init.js",
                        "~/Scripts/global/map.js",
                        "~/Scripts/global/PhotoView.js",
                        "~/Scripts/global/settings.js",
                        "~/Scripts/global/share.js",
                        "~/Scripts/global/map.js",
                        "~/Scripts/helpers.js",
                        "~/Scripts/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/dhtmlx.css",
                      "~/Content/CargoSnapshot.css",
                      "~/Content/common.css",
                      "~/Content/dhtmlxcarousel_dhx_skyblue.css"
                      ));
        }
    }
}
