﻿using System.Web.Mvc;
namespace CargoSnapshot.Controllers
{
    [Authorize]
    public class AccountController : SharedViews.Controllers.LoginBaseController
    {
        public override bool UseShell
        {
            get { return true; }
        }
    }
}