﻿using System.Web.Mvc;
using SharedViews.Attributes;
using SharedViews.Controllers;

namespace CargoSnapshot.Controllers
{
    public class HomeController : LayoutBaseController
    {
        [Authorize]
        [LayoutType("2U")]
        public ActionResult Index()
        {
            return View();
        }
    }
}