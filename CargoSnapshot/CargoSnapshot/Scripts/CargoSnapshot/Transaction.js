﻿var transaction = {
    wnd: null,
    form: null,
    curId: 0,
    dataview: null,
    isLoaded: false,
    initForm: function (windows, id) {


        if (transaction.wnd) {
            transaction.wnd.wins.unload();
            transaction.wnd = null;
        }

        windows = new dhtmlXWindows();
        var wnd = windows.createWindow('wndLocation', 0, 0, 900, 600);

        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Transaction details");

        var layout = wnd.attachLayout("2E");

        var str = [
           { type: "hidden", name: "Id" },
           { type: "input", name: "ShipmentReference", label: "Shipping REF#:", inputWidth: 250, offsetTop: "13", labelWidth: 200, validate: "NotEmpty" },
           { type: "calendar", name: "DateTaken", label: "Date Taken:", dateFormat: "%m-%d-%Y %H:%i", labelWidth: 200, readonly: true, validate: "NotEmpty" },
           { type: "label", label: "<ul id='trans-annotations'></ul>", labelWidth: 50 },
           { type: "combo", name: "UserId", label: "Taken By:", inputWidth: 200, labelWidth: 200, validate: "NotEmpty" },
           { type: "combo", name: "OriginLocationId", label: "Origin:", inputWidth: 200, labelWidth: 200, validate: "NotEmpty" },
           { type: "combo", name: "DestinationLocationId", label: "Destination:", inputWidth: 200, labelWidth: 200, validate: "NotEmpty" },
           { type: "combo", name: "CarrierId", label: "Carrier:", inputWidth: 200, labelWidth: 200, validate: "NotEmpty" },
           {
               type: "block", offsetTop: "30", list: [
                   { type: "newcolumn" },
                   { type: "button", name: "Save", value: "SAVE", inputLeft: 550, disabled: true },
                   { type: "newcolumn" },
                   { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 550 },
                   { type: "newcolumn" },
                   { type: "button", name: "Delete", value: "DELETE", inputLeft: 550, disabled: true }
               ]
           }
        ];

        var form = layout.cells('a').attachForm(str);
        form.enableLiveValidation(true);

        var originCombo = form.getCombo('OriginLocationId');
        originCombo.loadExternal('/CargoSnapshotCommon/GetLocations', 'Id', 'Name');

        var destinationCombo = form.getCombo('DestinationLocationId');
        destinationCombo.loadExternal('/CargoSnapshotCommon/GetLocations', 'Id', 'Name');

        var carrierCombo = form.getCombo('CarrierId');
        carrierCombo.loadExternal('/CargoSnapshotCommon/GetCarriers', 'Id', 'CarrierName');

        var takenByCombo = form.getCombo('UserId');
        takenByCombo.loadExternal('/CargoSnapshotCommon/GetUserProfiles', 'Id', 'UserName');

        var calendar = form.getCalendar("DateTaken");
        calendar.showTime();

        takenByCombo.enableFilteringMode("between");
        originCombo.enableFilteringMode("between");
        destinationCombo.enableFilteringMode("between");
        carrierCombo.enableFilteringMode("between");

        transaction.frm = form;
        layout.cells('a').hideHeader();
        layout.cells('a').setHeight(330);

        helpers.getDataFromExternalResource("/CargoSnapshotCommon/GetAnnotations", null, function (data) {
            var lstAnnotations = document.getElementById("trans-annotations");
            var content = "";

            self.deleteChilds("ul#trans-annotations");
            lstAnnotations.innerHTML = "";

            for (var i = 0; i < data.length; i++) {

                var li = document.createElement("li");


                var inpt = document.createElement("input");
                inpt.setAttribute('type', 'checkbox');
                inpt.setAttribute('name', 'check-trans');
                inpt.setAttribute('value', data[i].Id);

                li.textContent = data[i].Name;
                li.appendChild(inpt);

                lstAnnotations.insertBefore(li, lstAnnotations.firstChild);
            }
            transaction.Get(id);
        });

        wnd.attachEvent("onClose", function () {
            transaction.wnd.hide();
        });

        form.attachEvent("onButtonClick", function (name) {

            var obj = form.getFormData();

            if (name === "Save") {
                if (form.validate()) {
                    transaction.Edit(obj, function (result) {

                        var obj = JSON.parse(result.xmlDoc.responseText);

                        var data = {
                            ShipmentReference: obj.ShipmentReference
                        }

                        searchResult.curId = obj.id;
                        searchResult.ShipmentReference = obj.ShipmentReference;

                        var cache = searchResult.dataview.get(transaction.curId);
                        if (cache) {
                            searchResult.dataview.update(cache.id, cache);
                            searchResult.dataview.refresh();
                            searchResult.dataview.select(cache.id);
                        }

                        self.thumnBar();
                        thumbnail.loadData(data);

                        snapshot.disableUpload(false);
                        snapshot.disableButton(true, "Save");
                        snapshot.disableButton(true, "Delete");
                    });
                }

            } else if (name === "Delete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this record?",
                    callback: function (result) {
                        if (result) {
                            transaction.Delete(obj.id);
                            searchResult.curId = 0;
                            transaction.wnd.hide();
                            searchResult.dataview.remove(obj.id);
                            thumbnail.dataview.clearAll();
                        }
                    }
                });


            } else {
                transaction.wnd.hide();
            }
        });

        form.attachEvent("onKeyUp", function (inp, ev, name, value) {
            if (name === 'ShipmentReference') {
                this.setItemValue("ShipmentReference", this.getItemValue('ShipmentReference').toUpperCase());
            }
        });
        form.attachEvent("onInputChange", function (name, value, form) {

            if (transaction.isLoaded) {
                var frm = transaction.form;
                if (frm.validate()) {
                    transaction.disableButton(false, "Save");
                } else {
                    transaction.disableButton(true, "Save");
                    snapshot.disableUpload(true);
                    snapshot.frm.lock();
                }

            };
        });
        takenByCombo.attachEvent("onChange", function (value, state) {
            if (transaction.isLoaded) {
                transaction.validate();
            }
        });
        originCombo.attachEvent("onChange", function (value, state) {
            if (transaction.isLoaded) {
                transaction.validate();
            }
        });
        destinationCombo.attachEvent("onChange", function (value, state) {
            if (transaction.isLoaded) {
                transaction.validate();
            }
        });

        carrierCombo.attachEvent("onChange", function (value, state) {
            if (transaction.isLoaded) {
                transaction.validate();
            }
        });

        var layPhotos = layout.cells('b').attachLayout({
            pattern: "2U"
        });

        snapshot.initForm(layPhotos, id);

        transaction.wnd = wnd;
        transaction.form = form;
    },
    validate: function () {
        var frm = transaction.form;
        if (frm.validate()) {
            transaction.disableButton(false, "Save");
        } else {
            transaction.disableButton(true, "Save");
            snapshot.disableUpload(true);
            snapshot.frm.lock();
        }
    },
    Get: function (id) {
        if (id > 0) {
            transaction.disableButton(true, "Save");
            transaction.disableButton(false, "Delete");

            snapshot.disableUpload(false);
        } else {
            transaction.disableButton(true, "Save");
            transaction.disableButton(true, "Delete");
        }

        window.dhx4.ajax.get("Transaction/Get?id=" + id, function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);

            var frm = transaction.form;

            frm.setItemValue("Id", data.id);
            frm.setItemValue("ShipmentReference", data.ShipmentReference);

            var takenBy = frm.getCombo("UserId");
            takenBy.selectOption(takenBy.getIndexByValue(data.UserId), true, true);

            frm.setItemValue("DateTaken", data.DateTaken);

            var annotations = document.querySelectorAll("input[name='check-trans']");
            var liAnnotations = document.querySelectorAll("ul#trans-annotations li");
            var arAnnotations;
            if (data.Annotations) {
                arAnnotations = data.Annotations.split(',');
            }

            if (arAnnotations) {
                for (var j = 0; j < liAnnotations.length; j++) {
                    for (var i = 0; i < arAnnotations.length; i++) {
                        if (arAnnotations[i] === liAnnotations[j].innerText) {
                            annotations[j].checked = true;
                            break;
                        } else {
                            annotations[j].checked = false;
                        }
                    }
                }
            }

            //_origin
            var origin = frm.getCombo("OriginLocationId");
            origin.selectOption(origin.getIndexByValue(data.OriginLocationId), true, true);
            //_destination
            var destination = frm.getCombo("DestinationLocationId");
            destination.selectOption(destination.getIndexByValue(data.DestinationLocationId), true, true);
            //_carrier
            var carrier = frm.getCombo("CarrierId");
            carrier.selectOption(carrier.getIndexByValue(data.CarrierId), true, true);

            transaction.curId = data.id;


            transaction.isLoaded = true;

        });
    },
    Edit: function (obj, callback) {
        transaction.curId = obj.id;

        var checsAr = common.toValArray(document.querySelectorAll("input[name='check-trans']:checked"));
        obj.Annotations = common.toString(checsAr);
        obj.DateTaken = common.dateToString(obj.DateTaken);
        window.dhx4.ajax.post("Transaction/Edit?" + common.serialize(obj), function (result) {

            var data = JSON.parse(result.xmlDoc.responseText);
            searchResult.dataview.update(data.id, data);

            transaction.curId = data.id;
            transaction.frm.setItemValue("id", data.id);
            transaction.disableButton(false, "Delete");
            transaction.disableButton(true, "Save");


            callback(result);
        });
    },
    Delete: function (id) {
        transaction.curId = id;
        window.dhx4.ajax.post("Transaction/Delete?id=" + id, function () {

        });
    },
    disableButton: function (m, n) {
        var form = transaction.frm;
        m === true ? form.disableItem(n) : form.enableItem(n);
    }

};