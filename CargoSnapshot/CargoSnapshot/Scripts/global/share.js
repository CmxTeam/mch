﻿var Share = function (obj) {
    this.dvContacts = obj.dvContacts;
    this.tbContact = obj.tbContact;
    // this.frmContactInfo = obj.frmContactInfo;
    this.drmSendEmail = obj.drmSendEmail;
    this.pContactInfo = obj.pContactInfo;
    this.cntEmails = '';
};

Share.prototype.popupDestructor = function (popup) {
    if (popup) {
        popup.unload();
        popup = null;
    }
    return popup;
}

Share.prototype.popupActions = function (obj) {
    var popup = obj.popup;
    var self = this;
    popup.attachEvent("onButtonClick", function (name) {

        if (name === "ctSend") {
            if (this.validate()) {
                //self.uploadPhoto();
            }
        }
        else if (name === "ctCancel") {
            self.pContactInfo.unload();
            self.pContactInfo = null;
        }
        else if (name === "ctDelete") {
            var ctId = this.getInput("ctid").value;
            window.dhx4.ajax.get("/Contact/Delete?id=" + ctId);
            self.dvContacts.remove(ctId);
            self.pContactInfo.unload();
            self.pContactInfo = null;
        }
    });

    popup.attachEvent("onHide", function () {
        self.popupDestructor(this);
    });
};

Share.prototype.popupInit = function (obj) {
    var data = obj.data;
    var inp = obj.inp;

    this.pContactInfo = new dhtmlXPopup({ mode: "right" });
    this.pContactInfo.setSkin(wndSkin);

    var str;

    if (!data) {
        str = [
            {
                type: "block",
                style: "margin: 10px; width:340px;height:340px;",
                list: [
                    { type: "hidden", name: "ctid", value: "0" },
                    { type: "label", label: "<img id='ctPath' class='col-2' src='" + window.baseUrl + "'Content/imgs/user.png' />" },
                    { type: "input", name: "ctName", label: "Name:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty", note: { text: "Enter name. This fields is requred.", width: "300" } },
                    { type: "input", name: "ctTitle", label: "Title:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty", note: { text: "Enter title. This fields is requred.", width: "300" } },
                    { type: "input", name: "ctEmail", label: "Email:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty,ValidEmail", note: { text: "Enter email. This fields is requred.", width: "300" } },
                    { type: "label", name: "ctPhoto", label: "<input type='file' class='btn-file " + wndSkin + "' name='photo' />" },
                    {
                        type: "block",
                        name: "fbContactBtnBlock",
                        list: [

                            { type: "button", name: "ctCancel", value: "CANCEL" },
                             { type: "newcolumn" },
                            { type: "button", name: "ctSend", value: "SAVE" }

                        ]
                    }
                ]
            }
        ];
    } else {
        str = [
            {
                type: "block",
                style: "margin: 10px; width:340px;height:240px;",
                list: [
                    { type: "hidden", name: "ctid", value: "0" },
                    { type: "label", label: "<img id='ctPath' class='col-2' src='" + window.baseUrl + "'Content/imgs/unuser.jpg' />" },
                    { type: "input", name: "ctName", label: "Name:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty" },
                    { type: "input", name: "ctTitle", label: "Title:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty" },
                    { type: "input", name: "ctEmail", label: "Email:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty,ValidEmail" },
                   { type: "label", name: "ctPhoto", label: "<input type='file' class='btn-file " + wndSkin + "' name='photo' />" },
                    {
                        type: "block",
                        name: "fbContactBtnBlock",
                        list: [
                            { type: "newcolumn" },
                            { type: "button", name: "ctDelete", value: "DELETE" },
                            { type: "newcolumn" },
                            { type: "button", name: "ctCancel", value: "CANCEL" },
                            { type: "newcolumn" },
                            { type: "button", name: "ctSend", value: "SAVE" }
                        ]
                    }
                ]
            }
        ];

    }
    var xOffset = 0;
    var frmContactInfo = this.pContactInfo.attachForm(str);
    if (data) {
        document.getElementById('ctPath').src = data.img;
        frmContactInfo.setItemValue("ctid", data.id);
        frmContactInfo.setItemValue("ctName", data.Name);
        frmContactInfo.setItemValue("ctTitle", data.Title);
        frmContactInfo.setItemValue("ctEmail", data.Email);

        frmContactInfo.enableLiveValidation(true);

    } else {
        xOffset = 85;
    }

    if (this.pContactInfo.isVisible()) {
        this.pContactInfo.hide();
    } else {
        var x = dhx4.absLeft(inp) + xOffset;
        var y = dhx4.absTop(inp);
        var w = inp.offsetWidth;
        var h = inp.offsetHeight;
        this.pContactInfo.show(x, y, w, h);
    }

    return frmContactInfo;

}

Share.prototype.contactOnDblClick = function () {
    var dataview = this.dvContacts;
    var self = this;
    dataview.attachEvent("onItemDblClick", function (id, ev, html) {
        var data = dataview.get(id);

        var popup = self.popupInit({
            data: data,
            inp: html
        });

        self.popupActions({
            popup: popup,
            data: data
        });
    });
};


Share.prototype.contactOnClick = function () {
    var self = this;//.cntEmails;
    this.dvContacts.attachEvent("onItemClick", function (id, ev, html) {
        /*if checked*/
        var result;
        var contacts = document.querySelectorAll("input[name='contacts']:checked");
        var helper = new Helper();
        contacts = helper.toArray(contacts);//self.toArray(contacts);

        if (contacts.length > 0) {
            result = contacts.filter(function (obj) {
                return obj.value === id;
            });
            var data = dvContacts.get(id);

            if (result.length === 0) {
                /*unchecked*/
                self.cntEmails = self.cntEmails.replace(data.Email + ',', '');

            } else {
                /*add if not exist*/
                if (self.cntEmails.indexOf(data.Email) < 0) {
                    self.cntEmails += (data.Email + ',');
                }
            }
        } else {
            self.cntEmails = '';
        }

        return false;
    });
};

Share.prototype.contactActions = function () {
    var recipients = this.drmSendEmail;
    var self = this;

    this.tbContact.attachEvent("onClick", function (id) {
        if (id === "btnCntNew") {
            var btn = tbContact.getInput('inCntSearch');
            var popup = self.popupInit({
                inp: btn
            });

            self.popupActions({
                popup: popup
            });

        } else if (id === "btnCntSelect") {
            recipients.setItemValue("fiSendTo", self.cntEmails.substring(0, self.cntEmails.length - 1));

        }
    });
};

Share.prototype.contactFilter = function () {
    var self = this;
    var infilterName = tbContact.getInput('inCntSearch');
    infilterName.onfocus = function () {
        tbContact.getInput('inCntSearch').value = '';
    };

    infilterName.oninput = function () {
        self.dvContacts.clearAll();
        var q = tbContact.getInput('inCntSearch').value;

        self.dvContacts.load('/Contact/GetAll?q=' + q, 'xml');
    };
};