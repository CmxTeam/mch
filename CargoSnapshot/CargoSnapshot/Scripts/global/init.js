﻿
var common = {
    serialize: function (obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    },
    toArray: function (coll) {
        for (var i = coll.length, a = []; i--;) {
            a[i] = coll[i];
        }
        return a;
    },
    toString: function (obj) {
        return obj.join(',');
    },
    dateToString: function (date) {
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' +
            date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();
    },
    uploadFile: function (photo, action, callback) {
        var data = new FormData();
        data.append('files', photo.files[0]);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', action, true);
        xmlhttp.send(data);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var result = JSON.parse(xmlhttp.responseText);
                    if (result != null) {
                        callback(result);
                    }
                }
            }
        }
    },
    toValArray: function (obj) {
        for (var i = obj.length, a = []; i--;) {
            a[i] = obj[i].value;
        }
        return a;
    }
};

var annotation = {
    wnd: null,
    form: null,
    dataview: null,
    curId: 0,
    layout:null,
    initForm: function(layout) {
        annotation.layout = layout;
        layout.cells('a').setWidth(200);
        layout.cells('a').setText('Annotations');
        layout.cells('b').setText('Annotation details');

        var toolbar = layout.cells('a').attachToolbar({
            items: [
                { id: "addNew", type: "button", text: "New annotation" }
            ]
        });

        toolbar.attachEvent("onClick", function(name) {
            if (name === "addNew") {
                annotation.form.unlock();
                annotation.dataview.unselectAll();
                annotation.disableButton(true, "Delete");
                annotation.disableButton(true, "Save");
                annotation.ClearAnnotationDetails();
            }
        });

        var dataview = layout.cells('a').attachDataView({
            type: {
                template: '<span class=\'col-12\'>#Name#</span>',
                width: 200,
                height: 50,
                template_loading: "Loading..."
            }
        });

        dataview.attachEvent("onItemClick", function(id, ev, html) {
            annotation.SetAnnotationDetail(annotation.dataview.get(id));
            annotation.disableButton(false, "Delete");
            annotation.form.unlock();
            annotation.disableButton(true, "Save");
            return true;
        });

        annotation.dataview = dataview;

        var str = [
            { type: "hidden", name: "Id", value: 0 },
            { type: "input", name: "Name", label: "Name:", inputWidth: 200, labelWidth: 120, validate: "NotEmpty" },
            { type: "input", name: "Description", label: "Description:", inputWidth: 200, labelWidth: 120, validate: "NotEmpty" },
            
            //{
            //    type: "template", label: "Annotation Image:", inputWidth: 200, name: "AnnotationImage",
            //    labelWidth: 120, className: "req-asterisk", format: helpers.uploaderFormTemplate
            //},
            {
                type: "block",
                offsetTop: 420,
                list: [
                    { type: "newcolumn" },
                    { type: "button", name: "Save", value: "SAVE", inputLeft: 380, disabled: true },
                    { type: "newcolumn" },
                    { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 380 },
                    { type: "newcolumn" },
                    { type: "button", name: "Delete", value: "DELETE", inputLeft: 380, disabled: true }
                ]
            }
        ];

        var form = layout.cells('b').attachForm(str);
        annotation.form = form;

        form.attachEvent("onButtonClick", function(name) {

            var annotationDetails = form.getFormData();

            if (name === "Save") {
                annotation.Edit(annotationDetails);
            } else if (name === "Delete") {

                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this record?",
                    callback: function(result) {
                        if (result) {
                            annotation.Delete(annotationDetails.Id);
                        }
                    }
                });

            } else {
                annotation.dataview.unselectAll();
                annotation.ClearAnnotationDetails();
            }
        });

        form.attachEvent("onKeyup", function() {
            form.enableItem("Save");
        });
        annotation.LoadData();
    },
    LoadData: function (annotationId) {
        annotation.dataview.clearAll();
        annotation.layout.progressOn();
        annotation.dataview.loadExternal("/CargoSnapshotCommon/GetAnnotations", { annotationId: annotationId }, function (data) {
            annotation.layout.progressOff();
        });
    },
    ClearAnnotationDetails: function() {
        var frm = annotation.form;
        frm.setItemValue("Id", "");
        frm.setItemValue("Name", "");
        frm.setItemValue("Description", "");
        frm.setItemValue("AnnotationImage", "");
        
        annotation.disableButton(true, "Delete");
        annotation.disableButton(true, "Save");
        frm.setFocusOnFirstActive();
    },
    SetAnnotationDetail: function (annotationDetails) {
        annotation.ClearAnnotationDetails();
        var frm = annotation.form;
        frm.setItemValue("Id", annotationDetails.Id);
        frm.setItemValue("Name", annotationDetails.Name);
        frm.setItemValue("Description", annotationDetails.Description);
        frm.setItemValue("AnnotationImage", annotationDetails.AnnotationImage);
        frm.setFocusOnFirstActive();
    },
    Edit: function (annotationDetails) {
        annotation.layout.progressOn();
        helpers.executePost("/CargoSnapshotCommon/SaveAnnotation", annotationDetails, function (data) {
            annotation.LoadData();
            annotation.ClearAnnotationDetails();
            dhtmlx.message({ text: "Annotation information is saved.", expire: 3000 });
            annotation.layout.progressOff();
        });
    },
    Delete: function (id) {
        annotation.layout.progressOn();
        helpers.executePost("/CargoSnapshotCommon/DeleteAnnotation", { Id: id }, function (data) {
            annotation.ClearAnnotationDetails();
            annotation.LoadData();
            annotation.layout.progressOff();
        });
    },
    disableButton: function (m, n) {
        var form = annotation.form;
        m === true ? form.disableItem(n) : form.enableItem(n);
    }
};

var carrier = {
    wnd: null,
    form: null,
    dataview: null,
    curId: 0,
    initForm: function (windows) {
        var wnd = windows.createWindow('wndAnnotation', 0, 0, 700, 300);

        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Carriers");

        var layout = wnd.attachLayout("2U");
        layout.cells('a').setWidth(200);
        layout.cells('a').setText('Carriers');

        var toolbar = layout.cells('a').attachToolbar({
            items: [
                { id: "addNew", type: "button", text: "New carrier" }
            ]
        });

        toolbar.attachEvent("onClick", function (name) {
            if (name === "addNew") {
                carrier.Get(0);
            }
        });

        var dataview = layout.cells('a').attachDataView({
            type: {
                template: '<span class=\'col-12\'>#Text#</span>',
                width: 200,
                template_loading: "Loading..."
            }
        });

        dataview.load("Carrier/List", "json");
        dataview.attachEvent("onItemClick", function (id, ev, html) {
            carrier.Get(id);
            return true;
        });

        carrier.dataview = dataview;

        var str = [
            { type: "hidden", name: "id" },
            { type: "input", name: "Text", label: "Text:" },
            { type: "input", name: "Abbr", label: "Abbr:" },
            {
                type: "block",
                list: [
                    { type: "newcolumn" },
                   { type: "button", name: "cSave", value: "SAVE", inputLeft: 50 },
                    { type: "newcolumn" },
                    { type: "button", name: "cCancel", value: "CANCEL", inputLeft: 50 },
                    { type: "newcolumn" },
                    { type: "button", name: "cDelete", value: "DELETE", inputLeft: 50 }

                ]
            }
        ];

        var form = layout.cells('b').attachForm(str);
        layout.cells('b').setText('Carrier information');
        form.attachEvent("onButtonClick", function (name) {

            var obj = carrier.form.getFormData();

            if (name === "cSave") {
                carrier.Edit(common.serialize(obj));
            } else if (name === "cDelete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this record?",
                    callback: function (result) {
                        if (result) {
                            carrier.Delete(obj.id);
                        }
                    }
                });

            } else if (name === "cCancel") {
                carrier.Close();
            }
        });
        carrier.form = form;
        carrier.wnd = wnd;

    },
    Get: function (id) {
        window.dhx4.ajax.get("Carrier/Get?id=" + id, function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);

            var frm = carrier.form;
            frm.setItemValue("id", data.id);
            frm.setItemValue("Text", data.Text);
            frm.setItemValue("Abbr", data.Abbr);
        });
    },
    Edit: function () {

        var obj = carrier.form.getFormData();
        carrier.curId = obj.id;
        window.dhx4.ajax.post("Carrier/Edit?" + common.serialize(obj), function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);
            var obj = {
                id: data.id,
                Text: data.Text,
                Abbr: data.Abbr
            }

            if (carrier.curId == 0) {
                carrier.dataview.add(obj, obj.id);

            } else {
                carrier.dataview.update(obj.id, obj);
            }

            dhtmlx.message({ text: "Carrier information is saved.", expire: 3000 });

        });
    },
    Delete: function () {

        var obj = carrier.form.getFormData();
        carrier.curId = obj.id;

        window.dhx4.ajax.get("Carrier/Delete?id=" + id, function (data) {
            carrier.dataview.remove(carrier.curId);
        });
    },
    Close: function () {
        carrier.wnd.hide();
    }
};

var locations = {
    wnd: null,
    form: null,
    dataview: null,
    curId: null,
    initForm: function (windows) {
        var wnd = windows.createWindow('wndLocation', 0, 0, 750, 300);

        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Locations");

        var layout = wnd.attachLayout("2U");
        layout.cells('a').setWidth(200);
        layout.cells('a').setText('Locations');

        var toolbar = layout.cells('a').attachToolbar({
            items: [
                { id: "addNew", type: "button", text: "New location" }
            ]
        });

        toolbar.attachEvent("onClick", function (name) {
            if (name === "addNew") {
                locations.Get(0);
            }
        });

        var dataview = layout.cells('a').attachDataView({
            type: {
                template: '<span class=\'col-12\'>#Name#</span>',
                width: 200,
                template_loading: "Loading..."
            }
        });

        dataview.load("Location/List", "json");
        dataview.attachEvent("onItemClick", function (id, ev, html) {
            locations.Get(id);
            return true;
        });
        locations.dataview = dataview;
        var str = [
           { type: "hidden", name: "id" },
           { type: "input", name: "Name", label: "Name:", inputWidth: 200, labelWidth: 200 },
           { type: "input", name: "Abbr", label: "Abbr:", inputWidth: 200, labelWidth: 200 },
           { type: "input", name: "Latitude", label: "Latitude", inputWidth: 200, labelWidth: 200 },
           { type: "input", name: "Longitude", label: "Longitude", inputWidth: 200, labelWidth: 200 },
           {
               type: "block", list: [
                   { type: "newcolumn" },
                   { type: "button", name: "Save", value: "SAVE", inputLeft: 30 },
                   { type: "newcolumn" },
                   { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 30 },
                   { type: "newcolumn" },
                   { type: "button", name: "Delete", value: "DELETE", inputLeft: 30 }

               ]
           }
        ];

        var form = layout.cells('b').attachForm(str);

        form.attachEvent("onButtonClick", function (name) {
            var obj = form.getFormData();
            if (name === "Save") {
                locations.Edit(common.serialize(obj));
            } else if (name === "Delete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this record?",
                    callback: function (result) {
                        if (result) {
                            locations.Delete(obj.id);
                        }
                    }
                });


            } else {
                locations.wnd.hide();
            }
        });

        locations.form = form;
    },
    Get: function (id) {
        window.dhx4.ajax.get("Location/Get?id=" + id, function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);

            var frm = locations.form;
            frm.setItemValue("id", data.id);
            frm.setItemValue("Name", data.Name);
            frm.setItemValue("Abbr", data.Abbr);
            frm.setItemValue("Latitude", data.Latitude);
            frm.setItemValue("Longitude", data.Longitude);
        });
    },
    Edit: function () {

        var obj = locations.form.getFormData();
        locations.curId = obj.id;

        window.dhx4.ajax.post("Location/Edit?" + common.serialize(obj), function (result) {

            var data = JSON.parse(result.xmlDoc.responseText);
            var obj = {
                id: data.id,
                Name: data.Name,
                Abbr: data.Abbr,
                Latitude: data.Latitude,
                Longitude: data.Longitude
            }

            if (locations.curId === 0) {
                locations.dataview.add(obj, obj.id);
            } else {
                locations.dataview.update(obj.id, obj);
            }

            dhtmlx.message({ text: "Location information is saved.", expire: 3000 });
        });

    },
    Delete: function () {

        var obj = locations.form.getFormData();
        locations.curId = obj.id;
        window.dhx4.ajax.post("Location/Delete?id=" + obj.id, function () {
            locations.dataview.remove(locations.curId);
        });
    },
    Close: function () {
        locations.wnd.hide();
    }
};

var transaction = {
    wnd: null,
    form: null,
    curId: 0,
    dataview: null,
    isLoaded: false,
    toolbar: null,
    initForm: function (windows, data) {
        if (transaction.wnd) {
            transaction.wnd = null;
        }

        windows = new dhtmlXWindows();
        var wnd = windows.createWindow('wndLocation', 0, 0, 800, 600);
        wnd.setModal(1);
        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Transaction details");

        var layout = wnd.attachLayout("2E");
        var layPhotos = layout.cells('b').attachLayout({
            pattern: "2U"
        });
        var str = [
            { type: "hidden", name: "TransactionId" },
            { type: "settings", labelWidth: 120, offsetLeft: "10", position: "label-left" }
        ];

        var form = layout.cells('a').attachForm(str);

        helpers.getDataFromExternalResource("/CargoSnapshotCommon/GetControls", {
            referenceTypeId: thumbnail.selectedReferenceType.Id,
            isDetails: true
        }, function (controlTypes) {
            self.generateFormDynamicly(form, controlTypes, data);
        });

        snapshot.initForm(layPhotos, data);
        var toolbar = layout.cells('a').attachToolbar({
            items: [
                { id: "btnSave", type: "button", text: "Save" },
                { id: "btnDelete", type: "button", text: "Delete", disabled: data ? false:true }
            ]
        });

        transaction.toolbar = toolbar;
        toolbar.attachEvent("onClick", function (id) {
            var transactionDetails = form.getFormData();
            
            var snapshotsData = snapshot.dataview.serialize();
            var transactionData = {
                TransactionPhotos: snapshotsData,
                Id: transactionDetails.TransactionId,
                Properties: self.getDynamicFormData(form),
                ReferenceType: thumbnail.selectedReferenceType.Id
            }

            if (id == "btnSave") {
                if (form.validate()  ) {
                    if (snapshotsData.length > 0) {
                        layout.progressOn();
                        
                        transaction.Edit(transactionData, function(result) {
                            layout.progressOff();
                            transaction.wnd.close();
                            filterPanel.refreshResult();
                        });
                        searchResult.curId = 0;
                        thumbnail.dataview.clearAll();
                        self.thumnBar();
                        self.showView('def');
                    } else {
                        dhtmlx.alert({
                            type: "warning",
                            title: "Warning",
                            text: "You no have any Snapshot!"
                        });
                    }
                }
            }else if (id == "btnDelete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this record?",
                    callback: function (result) {
                        if (result) {
                            layout.progressOn();
                            transaction.Delete(transactionData.Id, function(data) {
                                filterPanel.refreshResult();
                                transaction.wnd.close();
                            });
                            searchResult.curId = 0;
                            thumbnail.dataview.clearAll();
                            self.thumnBar();
                            self.showView('def');
                        }
                    }
                });
            }
        });
        form.enableLiveValidation(true);

        transaction.frm = form;
        layout.cells('a').hideHeader();
        layout.cells('a').setHeight(230);

       
        transaction.wnd = wnd;
        transaction.form = form;
    },
    fillData: function (data) {
        transaction.isLoaded = false;
        if (data == null) {
            transaction.toolbar.enableItem("btnSave");
            transaction.toolbar.disableItem("btnDelete");
        } else {
            var form = transaction.form;
            transaction.toolbar.enableItem("btnSave");
            transaction.toolbar.enableItem("btnDelete");
            form.setItemValue("Id", data.Id);
            form.setItemValue("Reference#", data.Reference);
            form.setItemValue("DateTaken", data.RecDate ? new Date(data.RecDate) : null);

            transaction.curId = data.id;
        }
        transaction.isLoaded = true;
    },
    Edit: function (obj, callback) {
        transaction.curId = obj.id;

        helpers.executePost("/CargoSnapshotCommon/SaveTransaction", obj, function(data) {
            callback(data);
        });
    },
    Delete: function (id,callback) {
        transaction.curId = 0;
        helpers.executePost("/CargoSnapshotCommon/DeleteTransaction", { Id: id },function (data){if(callback)callback()});
    },
    disableButton: function (m, n) {
        var form = transaction.frm;
        m === true ? form.disableItem(n) : form.enableItem(n);
    }
};

var snapshot = {
    layout: null,
    wnd: null,
    frm: null,
    curId: 0,
    dataview: null,
    isLoaded: false,
    currentImage:null,
    initForm: function (layout, data) {

        layout.cells('a').setWidth(155);
        layout.cells('a').setText("Snapshots");

        var toolbar = layout.cells('a').attachToolbar({
            items: [
                { id: "addNew", type: "button", text: "New Snapshot" }
            ]
        });

        toolbar.attachEvent("onClick", function (name) {
            if (name === "addNew") {
                snapshot.curId = 0;
                snapshot.fillData();
            }
        });

        var snapList = layout.cells('a').attachDataView({
            type: {
                template:
                    '<span class=\'col-12\'>' +
                        '<img class=\'img-block\' src=\'{common.getImage()}\'/>' +
                    '</span>',
                getImage: function (obj) {
                    if (obj.Image.indexOf("data:") == 0)
                        return obj.Image;
                    else {
                        return window.baseUrl + obj.Image;
                    }
                },
                width: 100,
                template_loading: "Loading..."
            }
        });
        
        snapList.attachEvent("onItemClick", function (id) {
            snapshot.curId = id;
            snapshot.currentImage = snapList.get(id).Image;
            snapshot.fillData(snapList.get(id));
        });

        snapshot.dataview = snapList;
        if (data && data.TransactionPhotos) {
            data.TransactionPhotos.forEach(function(item) { item.LocationId = item.Location.Id });
            snapshot.dataview.parse(data.TransactionPhotos, "json");
        }
            
        snapshot.dataview.unselectAll();
        var form = null;
        var nophotoImage = window.baseUrl + "Content/imgs/nophoto.png";
        var str = [
           { type: "hidden", name: "Id" },
           { type: "hidden", name: "Reference" },
           { type: "hidden", name: "Image" },
           { type: "label", label: "<img id='snapshot-img' src='" + nophotoImage + "'> <input type='file' accept='image/*' class='btn-file " + wndSkin + "' name='snapshot-photo' />", labelWidth: 50 },
           
           { type: "calendar", name: "DateTaken", label: "Date Taken:", dateFormat: "%m-%d-%Y %H:%i", labelWidth: 200, inputWidth: 200, validate: "NotEmpty" },
           {
               type: "combo", name: "LocationId", label: "Location:", inputWidth: 200, labelWidth: 200, required: true, className: "req-asterisk",
               validate: function(input) { return form.getCombo("LocationId").getSelectedIndex() !== -1; }
           },
           {
               type: "combo", name: "UserId", label: "Taken By:", inputWidth: 200, labelWidth: 200, required: true, className: "req-asterisk",
               validate: function (input) { return form.getCombo("UserId").getSelectedIndex() !== -1 }
           },
           { type: "combo", comboType: "checkbox", name: "Annotations", label: "Annotations:", inputWidth: 200, labelWidth: 200 },
          
           {
               type: "block", offsetTop: "30", list: [
                   { type: "newcolumn" },
                   { type: "button", name: "Save", value: "Save Snapshot", inputLeft: 300, disabled: true },
                   { type: "newcolumn" },
                   { type: "button", name: "Delete", value: "Delete Snapshot", inputLeft: 300, disabled: true }
               ]
           }
        ];

        form = layout.cells('b').attachForm(str);
        layout.cells('b').setText("Snapshot details");

        var annotations = form.getCombo('Annotations');
        annotations.makeMultiSelect();
        annotations.loadExternal("/CargoSnapshotCommon/GetAnnotations", 'Id', 'Name', function () {
            snapshot.fillData(snapshot.dataview.get(snapshot.dataview.getSelected()));
        });

        var locationCombo = form.getCombo('LocationId');
        locationCombo.loadExternal('/CargoSnapshotCommon/GetLocations', 'Id', 'Name', function () {
            snapshot.fillData(snapshot.dataview.get(snapshot.dataview.getSelected()));
        });

        var takenByCombo = form.getCombo('UserId');
        takenByCombo.loadExternal('/CargoSnapshotCommon/GetUserProfiles', 'Id', 'UserName', function() {
            snapshot.fillData(snapshot.dataview.get(snapshot.dataview.getSelected()));
        });

        var calendar = form.getCalendar("DateTaken");
        calendar.showTime();

        locationCombo.enableFilteringMode("between");
        form.enableLiveValidation(true);
      
        snapshot.frm = form;
        snapshot.layout = layout;
        snapshot.frm.unlock();
        snapshot.attachEvents();
    },
    attachEvents: function () {
        var frm = snapshot.frm;

        frm.attachEvent("onButtonClick", function (name) {
            var obj = snapshot.frm.getFormData();

            if (obj.Id.length < 1) {
                obj.Id = 0;
                obj.Reference = searchResult.curId;
            }

            if (name === "Save") {
                if (snapshot.frm.validate()) {
                    var photoAnnotations = [];
                    var annotationsIds = snapshot.frm.getCombo('Annotations').getAllCheckedIds();
                    for (var i = 0; i < annotationsIds.length; i++) {
                        photoAnnotations.push({ Annotation: { Id: annotationsIds[i] } });
                    }

                    var item = snapshot.dataview.get(snapshot.dataview.getSelected());
                    var index = snapshot.dataview.indexById(snapshot.dataview.getSelected());
                    if (item != null) {
                        snapshot.dataview.remove(item.id);
                    }
                    
                    snapshot.dataview.add({
                        Id: obj.Id,
                        LocationId: obj.LocationId,
                        Location: { Id: obj.LocationId },
                        UserProfile: { Id: obj.UserId },
                        Image: frm.getItemValue("Image") || "",
                        RecDate: snapshot.frm.getCalendar('DateTaken').getFormatedDate(),
                        PhotoAnnotations: photoAnnotations
                    }, index);
                    snapshot.fillData();
                }
                
            } else if (name === "Delete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this record?",
                    callback: function (result) {
                        if (result) {
                            snapshot.remove(snapshot.curId);
                            snapshot.curId = null;
                            snapshot.fillData();
                        }
                    }
                });
            }
        });

        var snpFile = document.querySelector("input[name='snapshot-photo']");
        snpFile.onchange = function (e) {
            if (e.target.files.length > 0) {
                var reader = new FileReader();
                
                reader.readAsDataURL(e.target.files[0]);
                reader.onload = (function (a) {
                    snapshot.currentImage = a.target.result;
                    frm.setItemValue("Image", a.target.result);
                    document.getElementById("snapshot-img").src = a.target.result;
                });
                
                snapshot.disableButton(false, "Save");
            } else {
                snapshot.currentImage = null;
                snapshot.disableButton(true, "Save");
            }

        };
    },
    fillData: function (data) {
        snapshot.isLoaded = false;
      
        var frm = snapshot.frm;
        var annotations = frm.getCombo("Annotations");

        frm.setItemValue("Id", '');

        frm.setItemValue("DateTaken", '');

        frm.getCombo("LocationId").unSelectOption();

        frm.getCombo("Annotations").unSelectOption();
        frm.getCombo("Annotations").checkUncheckAll();
        annotations.generateMultiSelectComboInputText();

        frm.getCombo("UserId").unSelectOption();

        document.getElementById("snapshot-img").src = window.baseUrl + "Content/imgs/nophoto.png";
        $("input[name='snapshot-photo']").val('');

        snapshot.currentImage = null;

        frm.setItemValue("Image", "");

        frm.setItemValue("Reference", "");

        snapshot.layout.cells('b').expand();
        snapshot.frm.unlock();
        if (data == null) {
            snapshot.disableButton(true, "Save");
            snapshot.disableButton(true, "Delete");
        } else {
            snapshot.disableButton(false, "Save");
            snapshot.disableButton(false, "Delete");
            snapshot.frm.unlock();
           
            for (var i = 0; i < data.PhotoAnnotations.length; i++) {
                annotations.forEachOption(function (optId) {
                    if (optId.value == data.PhotoAnnotations[i].Annotation.Id) {
                        annotations.setChecked(annotations.getIndexByValue(optId.value), true);
                        return false;
                    }
                });
            }
            annotations.unSelectOption();
            annotations.generateMultiSelectComboInputText();

            frm = snapshot.frm;

            frm.setItemValue("Id", data.Id);

            frm.setItemValue("DateTaken", new Date(data.RecDate));

            var location = frm.getCombo("LocationId");

            if (location.getIndexByValue(data.Location.Id) === -1) {
                location.selectOption(0, true, true);
            } else {
                location.selectOption(location.getIndexByValue(data.Location.Id), true, true);
            }

            var takenBy = frm.getCombo("UserId");

            if (takenBy.getIndexByValue(data.UserProfile.Id) === -1) {
                takenBy.selectOption(0, true, true);
            } else {
                takenBy.selectOption(takenBy.getIndexByValue(data.UserProfile.Id), true, true);
            }


            if (!data.Image) {
                data.Image = window.baseUrl + "Content/imgs/nophoto.png";
            } else if(data.Image.indexOf("data:") == 0) {
                document.getElementById("snapshot-img").src = data.Image;
            } else {
                 document.getElementById("snapshot-img").src = window.baseUrl +data.Image;
            }
            
            frm.setItemValue("Image", data.Image);

            frm.setItemValue("Reference", data.Reference);

            snapshot.layout.cells('b').expand();
        }

        snapshot.isLoaded = true;
    },
    remove: function (id) {
        snapshot.curId = id;
        snapshot.dataview.remove(snapshot.curId);
    },
    disableButton: function (m, n) {
        var form = snapshot.frm;
        m === true ? form.disableItem(n) : form.enableItem(n);
    }
}

var emailTemplate = {
    form: null,
    dataview: null,
    curId: 0,
    shared: true,
    layout:null,
    initForm: function (layout) {
        emailTemplate.layout = layout;
        layout.cells('a').setWidth(300);
        layout.cells('a').setText('Templates');

        var toolbar = layout.cells('a').attachToolbar({
            items: [
                { id: "addNew", type: "button", text: "New template" }
            ]
        });

        toolbar.attachEvent("onClick", function (name) {
            if (name === "addNew") {
                emailTemplate.ClearTemplateDetails();
                emailTemplate.dataview.unselectAll();
                emailTemplate.form.unlock();
                emailTemplate.form.setItemFocus("TemplateTitle");
                emailTemplate.disableButton(true, "Save");
                emailTemplate.disableButton(true, "Delete");
            }
        });

        var dvTemplateList = layout.cells('a').attachDataView({
            type: {
                template: '<span class=\'col-12\'>#TemplateTitle#</span>',
                width: 300,
                template_loading: "Loading..."
            }
        });

        dvTemplateList.attachEvent("onItemClick", function (id, ev, html) {
            emailTemplate.form.unlock();
            var data = emailTemplate.dataview.get(id);
            data.DeliveryVPOC = data.RecepientEmail != null && data.RecepientEmail != "";
            emailTemplate.form.setFormData(data);
            emailTemplate.form.setItemFocus("TemplateTitle");
            emailTemplate.disableButton(false, "Delete");
            emailTemplate.disableButton(true, "Save");
            return true;
        });

        emailTemplate.dataview = dvTemplateList;
        emailTemplate.LoadData();
        var str = [
            { type: "hidden", name: "Id", value: 0 },
            {
                type: "checkbox", name: "Shared", label: "Shared",
                labelWidth: 300, labelAlign: "left", inputWidth: 0, offsetLeft: "155", position: "label-right"
            },
            { type: "input", name: "TemplateTitle", label: "Template title:", labelWidth: 120, required: true, validate: "NotEmpty", inputWidth: 300 },
            { type: "input", name: "ConsigneeName", label: "Consignee Name:", labelWidth: 120, inputWidth: 300 },
            //{
            //    type: "checkbox", name: "UseEmailSender", label: "Always use the above email address as Sender email",
            //    labelWidth: 300, labelAlign: "left", inputWidth: 0, offsetLeft: "155", position: "label-right"
            //},
            { type: "input", name: "SendTo", label: "Send To:", rows: "3", required: true, validate: "NotEmpty", labelWidth: 120, inputWidth: 300 },
            { type: "input", name: "Subject", label: "Subject:", required: true, validate: "NotEmpty", labelWidth: 120, inputWidth: 300 },
            { type: "editor", name: "MsgBody", label: "Message Body:", inputWidth: 300, inputHeight: 200, labelWidth: 120, validate: "NotEmpty" },
            {
                type: "checkbox", name: "DeliveryVPOC", label: "Request Delivery Visual Proof of Condition(VPOC)",
                labelWidth: 350, labelAlign: "left", position: "label-right",
                list: [
                    {
                        type: "input", name: "RecepientEmail", label: "Recepient Email Address:", inputWidth: 300,
                        required: true, validate: "NotEmpty,ValidEmail", labelWidth: 200
                    },
                    { type: "input", name: "EmailNotification", label: "Send Email Notification to:", inputWidth: 300, labelWidth: 200, validate: "ValidEmail" }
                ]
            },
            {
                type: "block",
                name: "form_block_3",
                offsetTop: 70,
                list: [
                    { type: "newcolumn" },
                    { type: "button", name: "Save", value: "SAVE", inputLeft: 280, disabled: true },
                    { type: "newcolumn" },
                    { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 280 },
                    { type: "newcolumn" },
                    { type: "button", name: "Delete", value: "DELETE", inputLeft: 280, disabled: true }
                ]
            }
        ];
        layout.cells('b').setText('Template data');
        var frmTemplate = layout.cells('b').attachForm(str);

        frmTemplate.lock();
        frmTemplate.attachEvent("onButtonClick", function (name) {
            if (name === "Save") {
                var data = emailTemplate.form.getFormData();
                data.Shared = emailTemplate.form.isItemChecked("Shared");
                emailTemplate.Edit(data);
                emailTemplate.disableButton(true, "Save");

            } else if (name === "Delete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this template?",
                    callback: function (result) {
                        if (result) {
                            emailTemplate.Delete(emailTemplate.form.getFormData().Id);
                        }
                    }
                });
            } else if (name === "Cancel") {
                emailTemplate.ClearTemplateDetails();
            }
        });

        frmTemplate.attachEvent("onChange", function () {
            emailTemplate.disableButton(false, "Save");
        });

        emailTemplate.form = frmTemplate;
    },
    ClearTemplateDetails: function() {
        var frmTemplate = emailTemplate.form;
        frmTemplate.setItemValue("Id", "");
        frmTemplate.setItemValue("TemplateTitle", "");
        frmTemplate.setItemValue("ConsigneeName", "");
        frmTemplate.setItemValue("SendTo", "");
        frmTemplate.setItemValue("Subject","");
        frmTemplate.setItemValue("RecepientEmail", "");
        frmTemplate.setItemValue("EmailNotification", "");
        frmTemplate.uncheckItem("DeliveryVPOC");
        frmTemplate.uncheckItem("UseDefault");
        frmTemplate.uncheckItem("UseEmailSender");
        frmTemplate.uncheckItem("Shared");
        frmTemplate.getEditor("MsgBody").setContent("");
        frmTemplate.lock();
    },
    LoadData: function() {
        emailTemplate.dataview.clearAll();
        emailTemplate.layout.progressOn();
        emailTemplate.dataview.loadExternal("/CargoSnapshotCommon/GetUserEmailTemplates", null, function (data) {
            emailTemplate.layout.progressOff();
        });
    },
    Edit: function (templateDetails) {
        emailTemplate.layout.progressOn();
        helpers.executePost("/CargoSnapshotCommon/SaveEmailTemplate", templateDetails, function(data) {
            emailTemplate.LoadData();
            emailTemplate.ClearTemplateDetails();
            emailTemplate.form.lock();
        });
    },
    Delete: function (id) {
        emailTemplate.layout.progressOn();
        var templateId = { Id: id }
        helpers.executePost("/CargoSnapshotCommon/DeleteEmailTemplate", templateId, function (data) {
            emailTemplate.LoadData();
            emailTemplate.ClearTemplateDetails();
            emailTemplate.form.lock();
        });
    },
    disableButton: function (m, n) {
        var form = emailTemplate.form;
        m === true ? form.disableItem(n) : form.enableItem(n);
    }
};

var topHeader = {
    curEmail: null,
    curUserId: null,
    wnds: null,

    initHeader: function (windows) {
        topHeader.wnds = windows;
        /*topHeader.curEmail fill from login panel*/
        window.dhx4.ajax.get("/Profile/Get?email=" + topHeader.curEmail, function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);
            topHeader.loadData(data);
        });
    },
    loadData: function (settings) {
        //var settings = JSON.parse(data.xmlDoc.responseText);

        if (settings.ShowHeader) {
            document.getElementById("header").style.display = "";

            document.getElementById("header").innerHTML = "<div class='col-12'>" +
                "<div class='col-4'><img name='logo' src='" + settings.Logo + "'/></div>" +
                "<div class='col-4 text-center' name='product-name'>" + settings.ProductName + "</div>" +
                "<div class='col-3 text-right' id='user-name'>" +
                    "<ul class='menu'>" +
                            "<li><a href='#'><img name='user-logo' src='/img/cur-user.png'>" + settings.Name + "</a>" +
                                "<ul class='submenu'>" +
                                     "<li id='logout' style='text-align: right;'><a href='#'>Logout</a></li>" +
                                 "</ul>" +
                            "</li>" +
                    "</ul>" +
                "</div>" +
                "<div class='col-1' id='drop-menu'><img name='user-logo' src='/img/options.png'>" +

                "Options</div></div>" +

                "</div>";

            document.getElementById('drop-menu').onclick = function () {
                /*open tab bar*/
                tabbar.initForm();
            };

            topHeader.curEmail = settings.EmailAddress;
            topHeader.curUserId = settings.Id;
            topHeader.dropMenuActions();
        }
    },
    dropMenuActions: function () {
        var submenus = document.querySelectorAll('.submenu li');

        for (var i = 0; i < submenus.length; i++) {
            submenus[i].onclick = function (e) {
                var action = e.currentTarget.id;
                if (action === 'profile') {
                    profile.initForm(topHeader.wnds);
                } else if (action === 'template') {
                    emailTemplate.initForm(topHeader.wnds);
                } else if (action === 'annotations') {
                    annotation.initForm(topHeader.wnds);
                } else if (action === 'transactions') {
                    transaction.initForm(topHeader.wnds);
                } else if (action === 'locations') {
                    locations.initForm(topHeader.wnds);
                } else if (action === 'carrier') {
                    carrier.initForm(topHeader.wnds);
                } else if (action === 'imageSettings') {
                    imageSettings.initForm(topHeader.wnds);
                } else if (action === 'logout') {
                    account.logout();
                }
            }
        }
    }
};

var tabbar = {
    frm: null,
    wnd: null,
    initForm: function () {

        var windows = new dhtmlXWindows();
        var wnd = windows.createWindow('wndTabBar', 0, 0, 950, 680);

        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Options");

        var bar = wnd.attachTabbar({
            tabs: [
                //{ id: "tProfile", text: "My Profile", active: true },
                { id: "tTemplate", text: "Email Templates", active: true },
                { id: "tAnnotations", text: "Annotations" },
                //{ id: "tPathSet", text: "Image path setting" }
            ]
        });
        /*my profile form*/
        //var frmProfile = bar.tabs("tProfile").attachForm(profile.initForm());
        //profile.attachEvents(frmProfile);

        /*email templates*/
        var layTemplate = bar.tabs("tTemplate").attachLayout({
            pattern: "2U"
        });
        emailTemplate.initForm(layTemplate);
        /*Annotations*/
        var layannotations = bar.tabs("tAnnotations").attachLayout({
            pattern: "2U"
        });
        annotation.initForm(layannotations);

        /*Image settings*/

        //var frmSettings = bar.tabs("tPathSet").attachForm(imageSettings.initForm());
        //imageSettings.attachEvents(frmSettings);
        //imageSettings.Load();

        tabbar.wnd = wnd;
        tabbar.frm = bar;
    }
};

var profile = {
    form: null,
    wnd: null,
    isLoaded: false,
    initForm: function () {
        var str = [
            { type: "hidden", name: "Id" },
            { type: "label", label: "<img id='profile-photo' src=''>", labelWidth: 50 },
            { type: "input", name: "Name", label: "Name:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty" },
            { type: "input", name: "EmailAddress", label: "EmailAddress:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty,ValidEmail" },
            { type: "input", name: "Phone", label: "Phone:", inputWidth: 200, labelWidth: 120 },
            { type: "combo", name: "idTemplate", label: "Default template:", connector: "Template/GetUserTemplates?curId=" + topHeader.curUserId, inputWidth: 200, labelWidth: 120 },
            { type: "input", name: "Nickname", label: "Nickname:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty" },
            { type: "password", name: "Password", label: "Password:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty" },
           {
               type: "block",
               name: "form_block_1",
               offsetTop: 404,
               list: [
                   { type: "button", name: "Save", value: "SAVE", inputLeft: 680, disabled: true },
                   { type: "newcolumn" },
                   { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 680 }
               ]
           }
        ];

        return str;
    },
    attachEvents: function (frmProfile) {
        profile.form = frmProfile;
        profile.form.enableLiveValidation(true);
        profile.form.attachEvent("onButtonClick", function (name) {
            switch (name) {
                case 'Save':
                    {
                        if (profile.form.validate()) {

                            profile.Edit();

                        }

                    }
                    break;
                case 'Cancel':
                    {
                        profile.Close();
                    }
                    break;
            }
        });
        profile.form.attachEvent("onKeyUp", function () {
            if (profile.isLoaded) {
                profile.form.validate() ?
                    profile.form.enableItem("Save") :
                    profile.form.disableItem("Save");
            }
        });
        profile.Load(topHeader.curEmail);
    },
    UploadFile: function (photo) {
        var obj = profile.form.getFormData();

        var data = new FormData();
        data.append('files', photo.files[0]);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', "/Profile/UploadImage?id=" + obj.Id, true);
        xmlhttp.send(data);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var result = JSON.parse(xmlhttp.responseText);
                    if (result != null) {
                        document.getElementById('profile-img').src = result.name;
                        profile.Edit();
                    }
                }
            }
        }
    },
    Load: function (email) {
        window.dhx4.ajax.get("/Profile/Get?email=" + email, function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);
            var frm = profile.form;
            frm.setItemValue("Id", data.Id);
            frm.setItemValue("Name", data.Name);
            frm.setItemValue("EmailAddress", data.EmailAddress);
            frm.setItemValue("Phone", data.Phone);
            frm.setItemValue("Nickname", data.Nickname);
            frm.setItemValue("Password", data.Password);

            var idTempl = frm.getCombo("idTemplate");
            idTempl.selectOption(idTempl.getIndexByValue(data.idTemplate), true, true);
            profile.isLoaded = true;
        });
    },
    Edit: function () {
        var obj = profile.form.getFormData();

        obj.idTemplate = parseInt(obj.idTemplate);
        window.dhx4.ajax.post("/Profile/Edit?" +
            common.serialize(obj)
            , function (result) {
                var data = JSON.parse(result.xmlDoc.responseText);
                var frm = profile.form;

                frm.setItemValue("Id", data.Id);
                frm.setItemValue("Name", data.Name);
                frm.setItemValue("EmailAddress", data.EmailAddress);
                frm.setItemValue("Phone", data.Phone);

                dhtmlx.message({ text: "Profile information is saved.", expire: 3000 });

                profile.form.disableItem("Save");
            });


    },
    Close: function () {
        profile.wnd.hide();
    },
    disableButton: function (m, n) {
        var form = profile.form;
        m === true ? form.disableItem(n) : form.enableItem(n);
    }
};

var filterPanel = {
    form: null,
    layout:null,
    initForm: function (layout_1) {
        filterPanel.layout = layout_1;
        var cFilter = layout_1.cells('a');
        cFilter.setText('FILTERS');
        cFilter.setWidth(360);
        cFilter.setHeight(410);

        var str = [
            { type: "settings", labelWidth: 120, offsetLeft: "10", position: "label-left" },
            { type: "combo", name: "UserId", label: "Taken By:", inputWidth: 180 },
            { type: "combo", comboType: "checkbox", name:"Annotations", label: "Annotations:", inputWidth: 180 },
            {
                type: "block",
                name: "form_block_1",
                list: [
                    { type: "button", name: "btn_reset", value: "RESET" },
                    { type: "newcolumn" },
                    { type: "button", name: "btn_refresh", value: "REFRESH" }
                ]
            },
            { type: "newcolumn" }
        ];

        var frmFilter = layout_1.cells('a').attachForm(str);
        var annotations = frmFilter.getCombo('Annotations');
        annotations.makeMultiSelect();
        annotations.loadExternal("/CargoSnapshotCommon/GetAnnotations", 'Id', 'Name');

        var takenByCombo = frmFilter.getCombo('UserId');
        takenByCombo.loadExternal('/CargoSnapshotCommon/GetUserProfiles', 'Id', 'UserName');

        frmFilter.attachEvent("onButtonClick", function (name) {
            switch (name) {
                case 'btn_reset':
                    {
                        self.resetFormData(frmFilter);
                    }
                    break;
                case 'btn_refresh':
                    {
                        filterPanel.refreshResult();
                    }
                    break;
            }
        });

        helpers.getDataFromExternalResource("/CargoSnapshotCommon/GetControls", {
            referenceTypeId: thumbnail.selectedReferenceType.Id,
            isFilter: true
        }, function (data) {
            self.generateFormDynamicly(frmFilter, data);
        });
        filterPanel.form = frmFilter;

    },
    getFilterData:function() {
        var filterObject = {
            Properties: self.getDynamicFormData(filterPanel.form),
            ReferenceType: thumbnail.selectedReferenceType.Id,
            Annotations: filterPanel.form.getCombo("Annotations").getAllCheckedIds(),
            UserId: filterPanel.form.getCombo("UserId").getSelectedValue()
        }
       
        return filterObject;
    },
    refreshResult: function () {
        var filterObject = this.getFilterData();
        searchResult.clearAll();
        searchResult.loadData(filterObject);
    }
}

var searchResult = {
    Reference: null,
    curId: 0,
    dvStart: 0,
    dataview: null,
    layoutCell: null,
    layout:null,
    initForm: function (layout_1) {
        searchResult.layout = layout_1;
        searchResult.layoutCell = layout_1.cells('b');
        searchResult.layoutCell.setText('SEARCH RESULTS');

        var dvSearchResults = searchResult.layoutCell.attachDataView({
            type: {
                template: '<span class=\'col-8\'><span class=\'text-green col-12\'>Reference#</span>' +
                    '#Reference#<br/>' +
                    '<span class=\'col-12\'><span class=\'text-green col-6\'>Location</span> <span class=\'text-green col-6\'>Photos</span></span>' +
                    '<span class=\'col-6\'>{common.getPictureLocation()}</span><span class=\'col-6\'>{common.getPhotoCount()}</span></span><span class=\'col-4\'><img class=\'img-block\' src=\'{common.getImage()}\'/></span>',
                getPictureLocation: function (obj) {
                    return obj.TransactionPhotos.length > 0 ? obj.TransactionPhotos[0].Location.ShortName : "N/A";
                },
                getImage: function (obj) {
                    return obj.TransactionPhotos.length > 0 ? window.baseUrl + obj.TransactionPhotos[0].Image : "";
                },
                getPhotoCount:function(obj) {
                    return obj.TransactionPhotos.length;
                },
                select: true,
                width: 310,
                height:100,
                template_edit: '<input class=\'dhx_item_editor\' bind=\'obj.Package\'>',
                template_loading: "Loading..."
            }

        });
        dvSearchResults.attachEvent("onItemClick", function (id, ev, html) {
            thumbnail.clearAll();
            var data = dvSearchResults.get(id);
            searchResult.Reference = data.Reference;
            searchResult.curId = data.id;
            thumbnail.loadData(data);
            return true;
        });

        dvSearchResults.attachEvent("onAfterSelect", function (id) {
            self.thumnBar();
        });

        searchResult.dataview = dvSearchResults;
    },
    getTransactionById: function (Id) {
        var transaction = null;
        searchResult.dataview.serialize().forEach(function() {
            if (this.Id == Id) {
                transaction = this;
                return false;
            }
        });
    },
    clearAll: function () {
        searchResult.dataview.clearAll();
    },
    loadData: function (params) {
        searchResult.layoutCell.progressOn();
        self.thumnBar();
        thumbnail.clearAll();
        helpers.getDataFromExternalResource('/CargoSnapshotCommon/GetTransactions', { filter: params }, function (data) {
            searchResult.dataview.clearAll();
            searchResult.dataview.parse(data, "json");
            searchResult.dataview.unselectAll();
            searchResult.layoutCell.progressOff();
        });
    }
};

var imageSettings = {
    form: null,
    wnd: null,
    isLoaded: false,
    initForm: function () {

        var str = [
        { type: "hidden", name: "Id" },
        { type: "input", name: "ImageDownload", label: "Save images:", labelWidth: 120, inputWidth: 200, validate: "NotEmpty" },
        { type: "input", name: "ImageUpload", label: "Load images:", labelWidth: 120, inputWidth: 200, validate: "NotEmpty" },
        {
            type: "block",
            offsetTop: 540,
            list: [
                { type: "newcolumn" },
                { type: "button", name: "Save", value: "SAVE", inputLeft: 680, disabled: true },
                { type: "newcolumn" },
                { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 680 }
            ]
        }
        ];

        return str;
    },
    attachEvents: function (form) {
        imageSettings.form = form;
        form.attachEvent("onButtonClick", function (name) {

            if (name === "Save") {
                imageSettings.Edit();
            } else {
                annotation.wnd.hide();
            }
        });

        form.attachEvent("onKeyUp", function () {
            if (imageSettings.isLoaded) {
                var frm = imageSettings.form;
                frm.validate() ? frm.enableItem("Save") : frm.disableItem("Save");
            }
        });
    },
    Load: function () {
        window.dhx4.ajax.get("/Profile/Get?email=" + topHeader.curEmail, function (result) {
            var data = JSON.parse(result.xmlDoc.responseText);
            var frm = imageSettings.form;

            frm.setItemValue("Id", data.Id);
            frm.setItemValue("ImageDownload", data.ImageDownload);
            frm.setItemValue("ImageUpload", data.ImageUpload);

            imageSettings.isLoaded = true;
        });
    },
    Edit: function () {
        var obj = imageSettings.form.getFormData();
        window.dhx4.ajax.get("/Settings/Edit?id=" + obj.Id + "&save=" + obj.ImageDownload + "&load=" + obj.ImageUpload, function (result) {

            var data = JSON.parse(result.xmlDoc.responseText);
            var frm = imageSettings.form;

            frm.setItemValue("Id", data.Id);
            frm.setItemValue("ImageDownload", data.ImageDownload);
            frm.setItemValue("ImageUpload", data.ImageUpload);

            dhtmlx.message({ text: "Image settings information is saved.", expire: 3000 });
        });

    }
};

var thumbnailsMap = {
    coords: [],
    lines: [],
    markers: [],
    infos: [],
    gmap: null,
    setCoords: function (markers) {

        var coords = [];

        for (var i = 0; i < markers.length; i++) {
            var lat = markers[i].getPosition().lat();
            var lng = markers[i].getPosition().lng();

            var coord = [lat, lng];
            coords[i] = coord;

        }
        thumbnailsMap.coords = coords;
    },
    drawArrows: function () {

        var coords = thumbnailsMap.coords;
        var map = thumbnailsMap.gmap;

        var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            strokeOpacity: 1
        };

        var dashed = {
            path: 'M 0,-1 0,1',
            strokeOpacity: 1,
            scale: 4
        };

        for (var i = 0; i < coords.length - 1; i++) {
            var lineCoordinates = [
                new google.maps.LatLng(coords[i][0], coords[i][1]),
                new google.maps.LatLng(coords[i + 1][0], coords[i + 1][1])
            ];
            thumbnailsMap.lines[i] = new google.maps.Polyline({
                path: lineCoordinates,
                strokeColor: '#333385',
                strokeOpacity: 0,
                icons: [{
                    icon: lineSymbol,
                    offset: '100%'
                },
                {
                    icon: dashed,
                    offset: '0',
                    repeat: '20px'
                }],
                map: map
            });
        }

    },
    sortMarkers: function (obj) {
        var markers = obj.markers;

        markers.sort(function (a, b) {
            return (Date.parse(a.title)) > (Date.parse(b.title));
        });

        thumbnailsMap.setCoords(markers);
        thumbnailsMap.drawArrows();
    },
    loadMap: function () {

        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(33.890542, 151.274856),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        thumbnailsMap.gmap = thumbnail.layout.attachMap(myOptions);
        google.maps.event.trigger(thumbnailsMap.gmap, 'resize');

        google.maps.Map.prototype.clearOverlays({
            map: thumbnailsMap.gmap,
            markers: thumbnailsMap.markers,
            infos: thumbnailsMap.infos,
            lines: thumbnailsMap.lines
        });
        thumbnailsMap.markers = [];
        thumbnailsMap.infos = [];
        thumbnailsMap.lines = [];

        google.maps.event.addDomListener(window, 'load', self.setMarkers({
            map: thumbnailsMap.gmap,
            markers: thumbnailsMap.markers,
            infos: thumbnailsMap.infos
        }));
    }
};

var thumbnail = {
    dataview: null,
    layout: null,
    curShipRef: null,
    currentImg: 0,
    curShipment: null,
    selectedReferenceType: null,
    initForm: function (mainLayout, referenceTypes ) {
        var panel = mainLayout.cells('b');

        panel.setCollapsedText('SNAPSHOTS');
        panel.setText("<div class='customToolbarInLayout' ><div id='" + panel.getViewName() + "'></div></div>");
        panel.hideArrow(true);

        var opts = [];
        referenceTypes.forEach(function (item) {
            opts.push([item.Id, 'obj', item.Name]);
        });

        thumbnail.selectedReferenceType = { Id: referenceTypes[0].Id, Name: referenceTypes[0].Name };
      
        var dynamicToolbar = new dhtmlXToolbarObject({
            parent: panel.getViewName(),
            xml:
                    '<toolbar>' +
                    '<item type="text" id="headerText" text="SNAPSHOTS" />' +
                    '<item type="button" id="btnCargoSnapshotSettings" text="OPTIONS" image="" />' +
                    '<item type="separator" id="button_separator_1" />' +
                    '</toolbar>'
        });

        dynamicToolbar.attachEvent("onClick", function(id) {
            if (id == "btnCargoSnapshotSettings") {
                tabbar.initForm();
            } else if (id != thumbnail.selectedReferenceType.Id) {
                thumbnail.selectedReferenceType = { Id: id, Name: dynamicToolbar.getListOptionText("referenceTypesMenu", id) };
                dynamicToolbar.setItemText("referenceTypesMenu", "Reference Type (" + thumbnail.selectedReferenceType.Name + ")");
                thumbnail.dataview.clearAll();
                searchResult.dataview.clearAll();
                filterPanel.initForm(filterPanel.layout);
                /*search result panel*/
                searchResult.initForm(searchResult.layout);
                self.thumnBar();
            }
        });

        dynamicToolbar.addSpacer("headerText");
        dynamicToolbar.addButtonSelect("referenceTypesMenu", 3, "Reference Type", opts, null, null, "disabled", true);
        dynamicToolbar.setItemText("referenceTypesMenu", "Reference Type (" + thumbnail.selectedReferenceType.Name + ")");
        dynamicToolbar.setWidth("referenceTypesMenu", 180);

        var dvThumbnails = panel.attachDataView({
            type: {
                template: '<div class=\'col-1\'>' +
                    '<input type=\'hidden\' value=\'#Annotations#\'>' +
                    '<input type=\'hidden\' value=\'#Longitude#\'><input type=\'hidden\' value=\'#Latitude#\'>' +
                    '<input type=\'checkbox\' class=\'col-12\' name=\'photo-thumbnails\' value=\'#Id#\' /></div><div class=\'col-6\'><span class=\'text-green col-12\'>Location:</span>' +
                     '{common.getLocationName()}<br/>' +
                    '<span class=\'text-green col-12\'>Date Taken:</span>' +
                    '{common.getDate()} <br/> ' +
                    '<span class=\'text-green col-12\'>Taken by: </span>' +
                    '{common.getUserName()}<br/> ' +
                    '</div><div class=\'col-5\'><img class=\'img-block\' src=\'{common.getImage()}\'/><input type=\'button\' id=\'#Id#\' class=\'' + wndSkin + " col-12\' name='btn-photo-view' value='View' onclick='thumbnail.openView(#id#);' /></div>",
                getLocationName: function (obj) {
                    return obj.Location.Name;
                },
                getUserName: function (obj) {
                    return obj.UserProfile.UserName;
                },
                getDate: function (obj) {
                    return window.dhx4.date2str(new Date(obj.RecDate), '%m-%d-%Y %H:%i');
                },
                getImage: function(obj) {
                   return obj.Image ? window.baseUrl + obj.Image:"";
                },
                width: 280,
                height: 130,
                template_edit: '<input class=\'dhx_item_editor\' bind=\'obj.Package\'>'
            }
        });

        dvThumbnails.attachEvent("onItemClick", function (id, ev, html) {
            return false;
        });
        thumbnail.layout = panel;
        thumbnail.dataview = dvThumbnails;
    },

    loadData: function (data) {
        thumbnail.curShipRef = data && data.Reference ? data.Reference : "";
        thumbnail.curShipment = data ? data : "";
        thumbnail.dataview.clearAll();
        thumbnail.dataview.parse(data && data.TransactionPhotos ? data.TransactionPhotos : null, "json");
        thumbnail.dataview.unselectAll();

        var btns = document.querySelectorAll("input[name='btn-photo-view']");

        if (photoViewer.carouselWmd) {
            photoViewer.unloadForm();
        }

        /*update map*/
        
        if (thumbnailsMap.gmap) {
            
            google.maps.Map.prototype.clearOverlays({
                map: thumbnailsMap.gmap,
                markers: thumbnailsMap.markers,
                infos: thumbnailsMap.infos,
                lines: thumbnailsMap.lines
            });
            thumbnailsMap.markers = [];
            thumbnailsMap.infos = [];
            thumbnailsMap.lines = [];
            self.setMarkers({
                map: thumbnailsMap.gmap,
                markers: thumbnailsMap.markers,
                infos: thumbnailsMap.infos
            });
        }

        /*update grid*/
        if (thumbnail.grid) {
            thumbnail.grid.clearAll();
            data.TransactionPhotos.forEach(function (item) { item.Transaction.Reference = data.Reference ? data.Reference : "" });
            thumbnail.grid.setGridDataFromModel(data.TransactionPhotos, {
                "Transaction.Reference": "",
                "Location.Name": "",
                "RecDate": "%m-%d-%Y %H:%i",
                "UserProfile.UserName": "",
                "AnnotationNames": "",
                "Image": ""
            });
        }
       
    },
    clearAll: function () {
        thumbnail.dataview.clearAll();
    },
    grid: null,
    loadGrid: function () {
        var grid = thumbnail.layout.attachGrid(700, 500);
        grid.setHeader("Reference#,Location Picture Taken,Date Taken,Taken By,Annotations,Photos");
        grid.setColTypes("ro,ro,ro,ro,ro,img");
        grid.setInitWidths("150,230,150,160,280,180");
        grid.enableMultiline(true);
        grid.init();
        grid.enableSmartRendering(true);
        grid.enableEditEvents(false, false, false);

        if (photoViewer.carouselWmd) {
            photoViewer.unloadForm();
        }

        grid.attachEvent("onRowDblClicked", function (rId) {
            if (!photoViewer.carouselWmd) {
                photoViewer.initForm(function () {
                    photoViewer.resizeViewer();
                });
            }

            photoViewer.rotate = 0;
            photoViewer.carouselWmd.show();
            photoViewer.resizeViewer();
            photoViewer.selectCell(rId);

        });
        var data = searchResult.dataview.get(searchResult.dataview.getSelected());
        data.TransactionPhotos.forEach(function (item) { item.Transaction.Reference = data.Reference ? data.Reference : "" });
        grid.setGridDataFromModel(data.TransactionPhotos, {
            "Transaction.Reference": "",
            "Location.Name": "",
            "RecDate": "%m-%d-%Y %H:%i",
            "UserProfile.UserName": "",
            "AnnotationNames": "",
            "Image": ""
        });
        thumbnail.grid = grid;
    },
    openView: function (id) {
        thumbnail.currentImg = id;
        //searchResult.getTransactionById(id);
        if (!photoViewer.carouselWmd) {
            photoViewer.initForm(function () {
                photoViewer.resizeViewer();
                photoViewer.rotate = 0;
                photoViewer.selectCell(thumbnail.currentImg);
            });
        }

        photoViewer.rotate = 0;
        photoViewer.carouselWmd.show();
        photoViewer.resizeViewer();

        photoViewer.selectCell(thumbnail.currentImg);
    }
};

var photoViewer = {
    wnd: null,
    rotate: 0,
    infos: [],
    markers: [],
    carouselcells: [],
    gmap: null,
    carousel: null,
    carouselWmd: null,
    currentImg: 0,
    unloadForm: function () {
        photoViewer.carouselWmd.wins.unload();
        photoViewer.carouselWmd = null;
    },
    initForm: function (callback) {
        var wndPhotoView = null;
        if (!photoViewer.carouselWmd) {
            var windows = new dhtmlXWindows();
            wndPhotoView = windows.createWindow('wndSnapshotsView', 0, 0, 900, 650);

            wndPhotoView.centerOnScreen();
            wndPhotoView.button('minmax').show();
            wndPhotoView.button('minmax').enable();

            wndPhotoView.setText("Snapshot Viewer");

            var tbPhotoView = wndPhotoView.attachToolbar({
                items: [
                    { id: "btnShare", type: "button", text: "SHARE", img: window.baseUrl + "Content/imgs/share.png" },
                    { type: "separator" },
                    {
                        id: "btnRotate", type: "button", text: "ROTATE", img: window.baseUrl + "Content/imgs/rotate.png"
                    }
                ]
            });

            tbPhotoView.attachEvent("onClick", function (id) {

                if (id === "btnShare") {
                    wndPhotoView.hide();
                    
                    self.attachFiles();

                    self.share({
                        attached: selectedFiles
                    });
                }
                if (id === "btnRotate") {
                    photoViewer.rotate += 90;

                    var cellid = photoViewer.carousel.getActiveId();
                    var item = photoViewer.carouselcells.filter(function (obj) {
                        return obj.cell == cellid;
                    });

                    var rotatedImage = document.getElementById('img-container' + item[0].item);
                    var container = rotatedImage.parentElement;

                    rotatedImage.style.webkitTransform = "rotate(" + photoViewer.rotate + "deg)";
                    rotatedImage.style.transform = "rotate(" + photoViewer.rotate + "deg)";

                    container.innerHTML = rotatedImage.outerHTML;
                    if (photoViewer.rotate === 360) {
                        photoViewer.rotate = 0;
                    }
                }
            });

            var str = [{ type: "label", label: "<div id='carouselObj'><div/>" }];
            wndPhotoView.attachForm(str);

            wndPhotoView.attachEvent("onResizeFinish", function () {
                photoViewer.resizeViewer();
            });

            wndPhotoView.attachEvent("onMaximize", function () {
                photoViewer.resizeViewer();
            });

            wndPhotoView.attachEvent("onMinimize", function () {
                photoViewer.resizeViewer();
            });

            wndPhotoView.attachEvent("onClose", function () {
                photoViewer.carouselWmd.hide();
            });

            photoViewer.carouselWmd = wndPhotoView;

        } else {
            photoViewer.carouselWmd.show();
            photoViewer.carouselWmd.detachObject(true, "carouselObj");
            photoViewer.carousel = null;

            var str = [{ type: "label", label: "<div id='carouselObj'><div/>" }];
            photoViewer.carouselWmd.attachForm(str);

        }
        wndPhotoView = photoViewer.carouselWmd;

        var frmCarousel = new dhtmlXCarousel({
            parent: "carouselObj",
            item_width: "auto",
            item_height: "auto"

        });
        photoViewer.carouselcells = [];

        var files = thumbnail.curShipment.TransactionPhotos;
        for (var i = 0; i < files.length; i++) {
            var id = frmCarousel.addCell();

            photoViewer.carouselcells[i] = {
                cell: id,
                item: files[i].Id
            }

            var anntList = files[i].AnnotationNames.split(',');
            var annotationslist = "";

            for (var j = 0; j < anntList.length; j++) {
                annotationslist += ("<li>" + anntList[j] + "</li>");
            }

            var str = [
                {
                    type: "block",
                    className: "col-12 photo-viewer text-center",
                    list: [
                        {
                            type: "block",
                            className: "col-3",
                            list: [
                                {
                                    type: "label",
                                    label:
                                        "<span class=\'col-12 border-block text-left\'>" +
                                            "<span class=\'form-block\'>" +
                                            "<span class=\'col-12 text-green\'>Reference#</span>" +
                                            "<span class=\'col-12\' name=\'Reference\'>" + thumbnail.curShipment.Reference + "</span>" +
                                            "</span>" +
                                            "<span class=\'form-block\'>" +
                                            "<span class=\'col-12 text-green\'>Photo Location</span>" +
                                            "<span class=\'form-block\'><span class=\'col-12\' name=\'location\'>" + files[i].Location.Name + "</span>" +
                                            "</span>" +
                                            "<span class=\'form-block\'>" +
                                            "<span class=\'col-12 text-green\'>Photo Taken By</span>" +
                                            "<span class=\'col-12\' name=\'UserName\'>" + files[i].UserProfile.UserName + "</span>" +
                                            "</span>" +
                                            "<span class=\'form-block\'>" +
                                            "<span class=\'col-12 text-green\'>Date Photo Taken</span>" +
                                            "<span class=\'col-12\' name=\'dateTaken\'>" + window.dhx4.date2str(new Date(files[i].RecDate), "%m-%d-%Y %H:%i") + "</span>" +
                                            "</span>" +
                                            "<span class=\'form-block\'>" +
                                            "<span class=\'col-12 text-green\'>Annotations</span>" +
                                            "<div class=\'col-12 border-block\'><ul name=\'annotations\'>" +
                                            annotationslist +
                                            "</ul></span>" +
                                            "</span>" +
                                            "</span>"
                                },
                                { type: "label", label: "<div class=\'col-12  border-block gmaps\' id=\'image-map-" + files[i].Id + "\' style=\'height: 390px;\'></div>" }
                            ]
                        },
                        {
                            type: "block",
                            className: "text-center col-9",
                            list: [
                                { type: "label", label: "<div class=\'image-container col-12\'><input type=\'hidden\' name=\'image-id\'><img id=\'img-container" + files[i].Id + "\'  src=\'" + window.baseUrl + files[i].Image + "\' class=\'shapshots\' style= \'max-width: 100%;height: auto; width: 100%;\'/></div>" }
                            ]
                        }
                    ]
                }
            ];

            frmCarousel.cells(id).attachForm(str);

            var map = self.loadMapView(files[i].Id);
            google.maps.event.trigger(map, 'resize');
            var markers = [];
            var infos = [];
            
            google.maps.Map.prototype.clearOverlays({
                markers: markers,
                infos: infos
            });

            var data = self.setMarker({
                locations: {
                    Latitude: files[i].Location.Latitude,
                    Longitude: files[i].Location.Longitude,
                    LocationName: files[i].Location.Name,
                    UserName: files[i].UserProfile.UserName,
                    RecDate: window.dhx4.date2str(new Date(files[i].RecDate), '%m-%d-%Y %H:%i')
                },
                map: map,
                ma: markers,
                iw: infos
            });

            var latLng = data.markers[0].getPosition();
            map.setCenter(latLng);
        }

        photoViewer.carousel = frmCarousel;
        callback(true);
    },
    resizeViewer: function () {

        var obj = photoViewer.carouselWmd.getDimension();

        var parentObj = document.getElementById("carouselObj");
        parentObj.style.width = (obj[0] - 40) + "px";
        parentObj.style.height = (obj[1] - 120) + "px";
        photoViewer.carousel.setSizes();
        photoViewer.carousel.setCellSize(obj[0], obj[1]);

        var snapshots = document.querySelectorAll(".image-container");
        for (var i = 0; i < snapshots.length; i++) {
            snapshots[i].style.cssText = "height: " + obj[1] * 0.75 + "px;";
        }
        var proc = 0.4;
        if (obj[0] > 1000) {
            proc = 0.5;
        }
        var gmaps = document.querySelectorAll(".gmaps");
        for (var i = 0; i < snapshots.length; i++) {
            gmaps[i].style.cssText = "height: " + obj[1] * proc + "px;position: relative;overflow: hidden;transform: translateZ(0px); background-color: rgb(229, 227, 223);";
        }
    },
    selectCell: function (id) {

        var cells = photoViewer.carouselcells;
        for (var i = 0; i < cells.length; i++) {
            if (cells[i].item == id) {
                photoViewer.carousel.cells(cells[i].cell).setActive();
            }
        }
    }
};

var shareWnd = {
    wnd: null,
    layout: null,
    initForm: function (windows) {
        var wndShare = windows.createWindow('wndShare', 0, 0, 1410, 700);
        var ltShare = wndShare.attachLayout('3W');
        wndShare.maximize();
        adressBook.initForm(ltShare);
        emailDetails.initForm(ltShare);
        attachments.initForm(ltShare);
        wndShare.show();
        wndShare.setText('SHARE');

        wndShare.centerOnScreen();
        wndShare.button('minmax').show();
        wndShare.button('minmax').enable();

        shareWnd.wnd = wndShare;
        shareWnd.layout = ltShare;
    }

};

var emailDetails = {
    wnd: null,
    form: null,
    initForm: function (ltShare) {
        var cell6 = ltShare.cells('b');
        cell6.setText('Email Details:');
        var str = [
            { type: "input", name: "Reference", label: "Reference#:", inputWidth: 300, readonly: true, offsetTop: "13" },
            { type: "input", name: "ConsigneeName", label: "Consignee Name:", inputWidth: 300 },
            //{ type: "input", name: "Email", label: "Sender Email:", required: true, validate: "NotEmpty,ValidEmail", inputWidth: 300},
            //{ type: "checkbox", name: "fcAboveEmail", label: "Always use the above email address as Sender email", labelWidth: 300, labelAlign: "left", inputWidth: 0, offsetLeft: "155", position: "label-right" },
            { type: "input", name: "SendTo", label: "Send To:", rows: "3", required: true, inputWidth: 300, validate: "NotEmpty" },
            { type: "input", name: "Subject", label: "Subject:", required: true, validate: "NotEmpty", inputWidth: 300 },
            { type: "editor", name: "MsgBody", label: "Message Body:", inputWidth: 300, inputHeight: 200 },
            {
                type: "checkbox",
                name: "DeliveryVPOC",
                label: "Request Delivery Visual Proof of Condition(VPOC)",
                labelWidth: 350,
                labelAlign: "left",
                position: "label-right",
                list: [
                    { type: "input", name: "RecepienEmail", label: "Recepient Email Address:", inputWidth: 300, required: true, validate: "NotEmpty,ValidEmail" },
                    { type: "input", name: "EmailNotification", label: "Send Email Notification to:", inputWidth: 300 }
                ]
            },
            { type: "settings", labelWidth: 150, labelAlign: "right", inputWidth: 250, offsetLeft: "5" },
            {
                type: "block",
                name: "form_block_3",
                list: [
                    { type: "newcolumn" },
                    { type: "button", name: "fbtnCancel", value: "CANCEL", inputLeft: 300 },
                    { type: "newcolumn" },
                    { type: "button", name: "fbtnSend", value: "SEND", inputLeft: 300 }
                ]
            }
        ];

        var drmSendEmail = cell6.attachForm(str);

        drmSendEmail.attachEvent("onButtonClick", function (name) {
            switch (name) {
                case 'fbtnSend':
                    {
                        self.sendEmail();
                    }
                    break;
                case 'fbtnCancel':
                    {
                        shareWnd.wnd.hide();
                    }
                    break;
            }
        });

        emailDetails.wnd = drmSendEmail;
    },
    loadTemplate: function () {
        helpers.getDataFromExternalResource("/CargoSnapshotCommon/GetUserDefaultEmailTemplate", null, function(data) {
            emailDetails.wnd.setItemValue("Reference", searchResult.Reference);
            if (data.ConsName) {
                emailDetails.wnd.setItemValue("ConsigneeName", data.ConsigneeName);
            }

            if (data.DeliveryVPOC === true) {
                emailDetails.wnd.checkItem("DeliveryVPOC");
            } else {
                emailDetails.wnd.uncheckItem("DeliveryVPOC");
            }

            if (data.EmailNotification) {
                emailDetails.wnd.setItemValue("EmailNotification", data.EmailNotification);
            }
            if (data.MsgBody) {
                emailDetails.wnd.getEditor("MsgBody").setContent(data.MsgBody);
            }
            if (data.RecepientEmail) {
                emailDetails.wnd.setItemValue("RecepienEmail", data.RecepienEmail);
            }
            if (data.SendTo) {
                emailDetails.wnd.setItemValue("SendTo", data.SendTo);
            }

            if (data.Subject) {
                emailDetails.wnd.setItemValue("Subject", data.Subject + searchResult.Reference);
            }
            if (data.UseEmailSender === true) {
                emailDetails.wnd.checkItem("fcAboveEmail");
            } else {
                emailDetails.wnd.uncheckItem("fcAboveEmail");
            }

            if (data.EmailNotification) {
                emailDetails.wnd.setItemValue("EmailNotification", data.EmailNotification);
            }
        });
    }
};


var adressBook = {
    dataview: null,
    initForm: function (ltShare) {
        var cell_5 = ltShare.cells('a');
        cell_5.setText('Address Book');
        cell_5.setWidth(370);

        var tbContact = cell_5.attachToolbar({
            items: [
                { id: "inCntSearch", type: "buttonInput" },
                //{ type: "separator" },
                //{ id: "btnCntNew", type: "button", text: "New Contact" },
                //{ type: "separator" },
                { id: "btnCntSelect", type: "button", text: "Select Contacts" }
            ]
        });
        tbContact.getInput("inCntSearch").placeholder = "<type to search>";

        //tbContact.setIconsPath('./codebase/imgs/');
        var dvContacts = cell_5.attachDataView({
            type: {
            template: '<input type=\'checkbox\' name=\'contacts\' value=\'#id#\' /><img class=\'col-1 img-contact\' src=\'{common.getImage()}\' style= /> <div class=\'col-12\'> #UserName#<br/><b>#Title#</b><br/>#Email#</div>',
            getImage:function(obj) {
                return window.baseUrl + obj.Photo;
            },
            height: 80,
            width: 315,
            template_edit: '<input class=\'dhx_item_editor\' bind=\'obj.Package\'>'
            },
            drag: true
        });

        //dvContacts.attachEvent("onItemDblClick", function (id, ev, html) {
        //    var data = adressBook.dataview.get(id);

        //    if (pContactInfo) {
        //        pContactInfo.unload();
        //        pContactInfo = null;
        //    }

        //    adressBook.showPopup({
        //        inp: html,
        //        data: data
        //    });
        //});

        dvContacts.attachEvent("onItemClick", function (id, ev, html) {
            /*if checked*/
            var result;
            var contacts = document.querySelectorAll("input[name='contacts']:checked");
            contacts = common.toArray(contacts);

            if (contacts.length > 0) {
                result = contacts.filter(function (obj) {
                    
                    return obj.value == id;
                });
                var data = adressBook.dataview.get(id);

                if (result.length === 0) {
                    /*unchecked*/
                    cntEmails = cntEmails.replace(data.Email + ',', '');

                } else {
                    /*add if not exist*/
                    if (cntEmails.indexOf(data.Email) < 0) {
                        cntEmails += (data.Email + ',');
                    }
                }
            } else {
                cntEmails = '';
            }

            return false;
        });

        var infilterName = tbContact.getInput('inCntSearch');
        infilterName.onfocus = function () {
            tbContact.getInput('inCntSearch').value = '';
        };

        infilterName.oninput = function () {
            var filter = tbContact.getInput('inCntSearch').value;

            adressBook.dataview.loadExternal('/CargoSnapshotCommon/GetUserProfiles', { filter: filter }, function (data) {
                var photos = document.querySelectorAll(".img-contact");
                for (var i = 0; i < photos.length; i++) {
                    photos[i].ondragstart = function () {
                        return false;
                    };
                }
            });
        };


        tbContact.attachEvent("onClick", function (id) {
            if (id === "btnCntNew") {
                var btn = tbContact.getInput('inCntSearch');
                adressBook.showPopup({ inp: btn });

            } else if (id === "btnCntSelect") {
                emailDetails.wnd.setItemValue("SendTo", cntEmails.substring(0, cntEmails.length - 1));

            }
        });

        adressBook.dataview = dvContacts;
    },
    popup: null,
    frm: null,
    showPopup: function (obj) {
        var inp = obj.inp;
        var data = obj.data;

        adressBook.fetchPopup();

        var pContactInfo = new dhtmlXPopup({ mode: "right" });
        pContactInfo.setSkin(wndSkin);
        var str;
        if (!data) {
            str = [
                {
                    type: "block",
                    style: "margin: 10px; width:340px;height:350px;",
                    list: [
                        { type: "hidden", name: "id", value: "0" },
                        { type: "hidden", name: "Img", value: "0" },
                        { type: "label", label: "<img id='Path' class='col-2' src='" + window.baseUrl + "'Content/imgs/user.png' />" },
                        { type: "input", name: "Name", label: "Name:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty", note: { text: "Enter name. This fields is requred.", width: "300" } },
                        { type: "input", name: "Title", label: "Title:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty", note: { text: "Enter title. This fields is requred.", width: "300" } },
                        { type: "input", name: "Email", label: "Email:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty,ValidEmail", note: { text: "Enter email. This fields is requred.", width: "300" } },
                        { type: "label", name: "ctPhoto", label: "<input type='file' class='btn-file " + wndSkin + "' name='photo' />" },
                        {
                            type: "block",
                            name: "fbContactBtnBlock",
                            list: [


                                { type: "button", name: "ctSend", value: "SAVE" },
                                 { type: "newcolumn" },
                                 { type: "button", name: "ctCancel", value: "CANCEL" }

                            ]
                        }
                    ]
                }
            ];
        } else {
            str = [
                {
                    type: "block",
                    style: "margin: 10px; width:340px;height:240px;",
                    list: [
                        { type: "hidden", name: "id", value: "0" },
                         { type: "hidden", name: "Img", value: "0" },
                        { type: "label", label: "<img id='Path' class='col-2' src='/img/unuser.jpg' />" },
                        { type: "input", name: "Name", label: "Name:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty" },
                        { type: "input", name: "Title", label: "Title:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty" },
                        { type: "input", name: "Email", label: "Email:", inputWidth: 230, labelWidth: 70, required: true, validate: "NotEmpty,ValidEmail" },
                       { type: "label", name: "ctPhoto", label: "<input type='file' class='btn-file " + wndSkin + "' name='photo' />" },
                        {
                            type: "block",
                            name: "fbContactBtnBlock",
                            list: [
                                { type: "newcolumn" },
                                { type: "button", name: "ctSend", value: "SAVE" },
                                { type: "newcolumn" },
                                { type: "button", name: "ctCancel", value: "CANCEL" },
                                { type: "newcolumn" },
                                { type: "button", name: "ctDelete", value: "DELETE" }
                            ]
                        }
                    ]
                }
            ];

        }
        adressBook.popup = pContactInfo;


        var frmContactInfo = pContactInfo.attachForm(str);
        if (data) {

            document.getElementById('Path').src = data.Photo;
            frmContactInfo.setItemValue("Img", data.img);
            frmContactInfo.setItemValue("id", data.Id);
            frmContactInfo.setItemValue("Name", data.UserName);
            frmContactInfo.setItemValue("Title", data.Title);
            frmContactInfo.setItemValue("Email", data.Email);
            adressBook.frm = frmContactInfo;

            adressBook.popupEvents(inp, 0);
        } else {
            adressBook.frm = frmContactInfo;
            adressBook.popupEvents(inp, 85);
        }


    },
    fetchPopup: function () {
        if (adressBook.popup) {
            adressBook.popup.unload();
            adressBook.popup = null;
        }
    },
    popupEvents: function (inp, xOffset) {
        adressBook.frm.enableLiveValidation(true);
        adressBook.frm.attachEvent("onButtonClick", function (name) {
            if (name === "ctSend") {
                if (adressBook.frm.validate()) {
                    self.uploadPhoto();
                }
            }
            else if (name === "ctCancel") {
                adressBook.popup.unload();
                adressBook.popup = null;
            }
            else if (name === "ctDelete") {
                dhtmlx.confirm({
                    type: "confirm-error",
                    title: "Delete records",
                    text: "Do you really want to delete this contact?",
                    callback: function (result) {
                        if (result) {
                            var ctId = adressBook.frm.getInput("id").value;
                            window.dhx4.ajax.get("/Contact/Delete?id=" + ctId);
                            adressBook.dataview.remove(ctId);
                            adressBook.popup.unload();
                            adressBook.popup = null;
                        }
                    }
                });


            }
        });

        if (adressBook.popup.isVisible()) {
            adressBook.popup.hide();
        } else {
            var x = dhx4.absLeft(inp) + xOffset;
            var y = dhx4.absTop(inp);
            var w = inp.offsetWidth;
            var h = inp.offsetHeight;
            adressBook.popup.show(x, y, w, h);
        }
    },
    Edit: function (params, callback) {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.open('POST', "/Contact/Edit?" + params, true);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {

                    var data = JSON.parse(xmlhttp.responseText);
                    adressBook.frm.setItemValue("id", data.id);

                    if (adressBook.dataview.get(data.id)) {
                        adressBook.dataview.remove(data.id);
                    }
                    adressBook.dataview.add({
                        id: data.id,
                        img: data.Img,
                        Name: data.Name,
                        Title: data.Title,
                        Email: data.Email
                    }, data.id);

                    callback(data.id);
                }
            }

        };
        xmlhttp.send();
    }
};

var attachments = {
    dataview: null,
    initForm: function (ltShare) {
        var cell_7 = ltShare.cells('c');
        cell_7.setText('Attachments:');
        cell_7.setWidth(400);
        cell_7.fixSize(false, null);
        var tbAttachments = cell_7.attachToolbar({
            items: [
                { id: "btnAttSlct", type: "button", text: "Select All" },
                { type: "separator" },
                { id: "btnAttUnslct", type: "button", text: "Unselect All" }
            ]
        });

        tbAttachments.attachEvent("onClick", function (id) {
            var attached = document.querySelectorAll("input[name='attachments']");
            var checked;
            if (id === "btnAttSlct") {
                checked = true;
            } else if (id === "btnAttUnslct") {
                checked = false;
            }

            for (var i = 0; i < attached.length; i++) {
                attached[i].checked = checked;
            }
        });

        var dtAttachments = cell_7.attachDataView({
            type: {
                template:
                    '<span class=\'col-1\'>' +
                        '<input type=\'checkbox\' class=\'col-12\' name=\'attachments\' value=\'#Id#\' #Checked# />' +
                        '</span>' +
                        '<span class=\'col-6\'>' +
                        "<span class=\'col-12\'>{common.getShipmentReference()}</span>" +
                        "<span class=\'col-12\'><b>{common.getDate()}</b></span>" +
                        "<span class=\'col-12\'>{common.getUserName()}</span>" +
                        "<span class=\'col-12\'>{common.getLocationName()}</span>" +
                        '</span>' +
                        '<span class=\'col-5\'>' +
                        '<img class=\'col-12 attach-block\' src=\'{common.getImage()}\'/>' +
                        '<input type=\'button\' id=\'#Id#\' class=\'' + wndSkin + " col-12\' name='btn-attach-view' value='View' onclick='attachments.openView(#Id#);'/>" +
                        '</span>',
                getLocationName: function (obj) {
                    return obj.Location.Name;
                },
                getShipmentReference: function (obj) {
                    return obj.Transaction.Reference;
                },
                getUserName: function (obj) {
                    return obj.UserProfile.UserName;
                },
                getDate: function (obj) {
                    return window.dhx4.date2str(new Date(obj.RecDate), '%m-%d-%Y %H:%i');
                },
                getImage: function (obj) {
                    if(obj.Image.indexOf("data:") == 0)
                        return obj.Image;
                    else
                        return window.baseUrl + obj.Image;
                },
                height: 90,
                width: 300,
                template_edit: '<input class=\'dhx_item_editor\' bind=\'obj.Package\'>'
            }
        });

        dtAttachments.attachEvent("onItemClick", function (id, ev, html) {

            return false;
        });
        attachments.dataview = dtAttachments;
    },
    openView: function (id) {
        photoViewer.currentImg = id;

        if (!photoViewer.carouselWmd) {
            photoViewer.initForm(function () {
                photoViewer.resizeViewer();
                photoViewer.rotate = 0;
                photoViewer.selectCell(photoViewer.currentImg);
            });
        }
        photoViewer.rotate = 0;
        photoViewer.carouselWmd.show();
        photoViewer.resizeViewer();

        photoViewer.selectCell(id);
    }
};
function doLink(name, value) {
    var f = this.getForm();
    return '<a id="' + name + '" href="#">' + value + '</a>';
}

var account = {
    wnd: null,
    frm: null,
    regWnd: null,
    regFrm: null,
    initForm: function () {
        var windows = new dhtmlXWindows();
        if (!account.wnd) {
            var wnd = windows.createWindow("Login", 0, 0, 800, 600);
            wnd.centerOnScreen();
            wnd.button('minmax').show();
            wnd.button('minmax').enable();
            wnd.setText("Login");

            var layout = wnd.attachLayout("1C");
            var str = [
                {
                    type: "block",
                    list: [
                        { type: "input", name: "email", label: "Enter your email:", inputWidth: 200, labelWidth: 200 },
                        { type: "password", name: "password", label: "Enter your password:", labelWidth: 200, inputWidth: 200 },
                        {
                            type: "checkbox",
                            name: "remeber",
                            label: "Remember me",
                            labelWidth: 300,
                            inputWidth: 0,
                            position: "label-right"
                        },
                        {
                            type: "block",
                            list: [
                                { type: "newcolumn" },
                                { type: "button", name: "Cancel", value: "CANCEL" },
                                { type: "newcolumn" },
                                { type: "button", name: "Send", value: "OK" }
                            ]
                        },
                        {
                            type: "template",
                            name: "register",
                            value: "Register",
                            position: "label-left",
                            format: "doLink"
                        },
                        {
                            type: "template",
                            name: "forgot",
                            value: "Forgot credentials",
                            position: "label-left",
                            format: "doLink"
                        }
                    ]
                }
            ];

            var frm = layout.cells('a').attachForm(str);
            layout.cells('a').hideHeader();
            frm.attachEvent("onButtonClick", function (name) {
                if (name === "Cancel") {
                    account.wnd.hide();
                } else if (name === "Send") {
                    var obj = account.frm.getFormData();
                    account.login(obj.email, obj.password);
                }
            });

            wnd.attachEvent("onClose", function () {
                account.wnd.hide();
            });

            account.wnd = wnd;
            account.frm = frm;

            document.getElementById('register').onclick = function () {

                account.registerInit();
                //close login window
                account.wnd.hide();
                //close forgot if opened
                if (account.forgotWnd) {
                    account.forgotWnd.hide();
                }
            };
            document.getElementById('forgot').onclick = function () {

                account.forgotInit();
                //close login  window
                account.wnd.hide();
                //close register window if opened
                if (account.regWnd) {
                    account.regWnd.hide();
                }
            };
        } else {
            account.wnd.show();
        }

    },
    login: function (email, password) {

        window.dhx4.ajax.query({
            method: "POST",
            url: "Account/Login?email=" + email + "&password=" + password,
            callback: function (data) {
                var email = JSON.parse(data.xmlDoc.response);
                topHeader.curEmail = email;
                account.wnd.hide();
                init();
            },
            headers: {
                "Authorization": "Basic " + btoa(email + ":" + password)
            }

        });
    },
    logout: function () {
        window.dhx4.ajax.get("Account/Logout", function () {
            auth();
        });
    },
    registerInit: function () {
        var windows = new dhtmlXWindows();
        var wnd = windows.createWindow("Login", 0, 0, 800, 600);
        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Registration");

        var layout = wnd.attachLayout("1C");
        layout.cells('a').hideHeader();
        var str = [
                 { type: "hidden", name: "Id", value: "0" },
                 //{ type: "label", label: "<img id='profile-img' src=''>", labelWidth: 50 },
                 { type: "input", name: "Name", label: "Name:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty" },
                 { type: "input", name: "Nickname", label: "Nickname:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty" },
                 { type: "input", name: "EmailAddress", label: "EmailAddress:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty,ValidEmail" },
                 { type: "password", name: "Password", label: "Password:", inputWidth: 200, labelWidth: 120, required: true, validate: "NotEmpty" },
                 { type: "input", name: "Phone", label: "Phone:", inputWidth: 200, labelWidth: 120 },
                 { type: "combo", name: "idTemplate", label: "Default template:", connector: "Template/SerializeList", inputWidth: 200, labelWidth: 200 },

                 //{ type: "label", name: "ctPhoto", label: "<input type='file' class='btn-file " + wndSkin + "' name='photo' />" },
                 {
                     type: "block",
                     name: "form_block_1",
                     list: [
                         { type: "button", name: "Save", value: "SAVE", inputLeft: 200 },
                         { type: "newcolumn" },
                         { type: "button", name: "Cancel", value: "CANCEL", inputLeft: 200 }
                     ]
                 }
        ];

        var frm = layout.cells('a').attachForm(str);
        frm.attachEvent("onButtonClick", function (name) {
            if (name === 'Save') {
                account.registerUser();
            } else {
                account.initForm();
                account.regWnd.hide();
            }
        });
        account.regFrm = frm;
        account.regWnd = wnd;

    },
    registerUser: function () {

        var obj = account.regFrm.getFormData();
        obj.idTemplate = parseInt(obj.idTemplate);
        window.dhx4.ajax.post("Account/Register?" + common.serialize(obj)


            , function (result) {
                var data = JSON.parse(result.xmlDoc.responseText);
                var frm = account.regFrm;
                frm.setItemValue("Id", data.Id);
                frm.setItemValue("Name", data.Name);
                frm.setItemValue("EmailAddress", data.EmailAddress);
                frm.setItemValue("Phone", data.Phone);


                var obj = account.regFrm.getFormData();
                account.login(obj.EmailAddress, obj.Password);
                account.regWnd.hide();

                auth();
            });
    },

    forgotFrm: null,
    forgotWnd: null,
    forgotInit: function () {
        var windows = new dhtmlXWindows();
        var wnd = windows.createWindow("Forgotdata", 0, 0, 800, 600);
        wnd.centerOnScreen();
        wnd.button('minmax').show();
        wnd.button('minmax').enable();
        wnd.setText("Forgot data");

        var layout = wnd.attachLayout("1C");
        layout.cells('a').hideHeader();
        var str = [
            {
                type: "block",
                list: [
                    { type: "input", name: "email", label: "Email", inputWidth: 200, labelWidth: 200 },
                   {
                       type: "radio", name: "forgot", value: "nick", label: "forgot nick"

                   },
                   {
                       type: "radio", name: "forgot", value: "password", label: "forgot password",
                       checked: true
                   },
                    {
                        type: "block",
                        list: [
                             { type: "newcolumn" },
                             { type: "button", name: "Cancel", value: "CANCEL" },
                             { type: "newcolumn" },
                             { type: "button", name: "Send", value: "OK" }
                        ]
                    }
                ]
            }
        ];

        var frm = layout.cells('a').attachForm(str);
        frm.attachEvent("onButtonClick", function (name) {
            if (name === "Cancel") {

                account.forgotWnd.hide();
                account.initForm();
            }
            else if (name === "Send") {

                var obj = account.forgotFrm.getFormData();

                window.dhx4.ajax.post("Account/Forgot?" + common.serialize(obj), function (data) {

                    account.forgotWnd.hide();
                    account.initForm();
                });
            }
        });

        wnd.attachEvent("onClose", function () {
            account.wnd.hide();
        });

        account.forgotFrm = frm;
        account.forgotWnd = wnd;
    }
}





