﻿var Settings = function () {
    this.curEmail = "";
}

Settings.prototype.get = function () {
    window.dhx4.ajax.get("/Settings/load", function (data) {
        var settings = JSON.parse(data.xmlDoc.responseText);

        if (settings.ShowHeader) {
            document.getElementById("header").style.display = "";

            document.getElementById("header").innerHTML =
                "<div class='col-12'>" +
                    "<div class='col-4'><img name='logo' src='" + settings.Logo + "'/></div>" +
                    "<div class='col-4 text-center' name='product-name'>" + settings.ProductName + "</div>" +
                    "<div class='col-4 text-right' name='user-name'><img name='user-logo' src='" + window.baseUrl + "'Content/imgs/cur-user.png'>" + settings.UserName + "</div>" +
                "</div>";

            this.curEmail = settings.UserEmail;
        }
    });
};