﻿var Helper = function () {

};

Helper.prototype.removeChilds = function (myNode) {
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
};

Helper.prototype.toArray = function (coll) {
    for (var i = coll.length, a = []; i--;) {
        a[i] = coll[i];
    }
    return a;
};

/*

var helper = new Helper();
    helper.removeChilds(el);

*/