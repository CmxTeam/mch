﻿function MapWrapper() {
    var count = 0;
}

MapWrapper.prototype.parseCoords = function (location) {
    var p = location.replace(/\ /g, '').split(',');
    p[0] = p[0].replace('°', ' ');
    p[1] = p[1].replace('°', ' ');
    for (var i = 0; i < p.length; i++) {
        var s = p[i].split(' ');
        if (s[1] === 'W' || s[1] === 'S') {
            p[i] = '-' + s[0];
        } else {
            p[i] = s[0];
        }
    }
    return {
        w: p[0],
        l: p[1]
    };
};

MapWrapper.prototype.setMarker = function (obj) {
    var res = google.maps.Map.prototype.clearOverlays(obj.map, obj.ma, obj.iw);
    var data = obj.locations;
    var map = obj.map;
    var ma = obj.ma;
    var iw = obj.iw;
    var coords = this.parseCoords(data.GeoLocation);
    myLatlng = new google.maps.LatLng(coords.w, coords.l);

    ma[count] = new google.maps.Marker({
        map: map, title: "image" + count, position: myLatlng
    });
    map.setCenter(ma[count].getPosition());

    var content =
       "<span class='text-left col-12'><span class='text-green col-12'>Location: </span> <br/>" + data.LocationPictureTaken + '<br/>' +
        "<span class='text-green col-12'>Timestamp: </span> <br/>" + data.TakenBy + '<br/>' +
        "<span class='text-green col-12'>Taken by: </span> <br/>" + data.DateTaken + '<br/></span> ';

    iw[count] = new google.maps.InfoWindow();

    google.maps.event.addListener(ma[count], 'click', (function (marker, content, infowindow) {
        return function () {
            infowindow.setContent(content);
            infowindow.open(map, marker);
        };
    })(ma[count], content, iw[count]));

    count++;

    return res;
};

// mapwrapper.setMarkers(gmap, action_loadMap + ShipmentReference);
MapWrapper.prototype.setMarkers = function (obj) {

    var map = obj.map;
    var action = obj.action;

    var map = obj.map;
    var markers = obj.ma;
    var infos = obj.iw;
    var self = this;
    //"GoogleMap/LoadData?id=" + ShipmentReference
    window.dhx4.ajax.get(action, function (data) {
  
        var locations = JSON.parse(data.xmlDoc.responseText);
        google.maps.Map.prototype.clearOverlays();

        for (var i = 0; i < locations.length; i++) {
            self.setMarker({
                locations: locations[i],
                map: map,
                ma: markersArray,
                iw: infWindowsArray
            });
        }
    });
};


