﻿var PhotoViewer = function (wndPhotoView) {
    this.wndPhotoView = wndPhotoView;
    this.map = null;
    this.markers = {};
    this.infoWindows = {};
    this.zoom = null;

};
PhotoViewer.prototype.setImage = function (data) {
    var photo = document.querySelector('#img-container');
    photo.src = data.img;
    var imgcont = document.querySelector(".image-container");
    var dimn = this.wndPhotoView.getDimension();
    imgcont.style.cssText = "width:" + dimn[0] * 0.5757 + "px; height: " + dimn[1] * 0.7 + "px;";
};

PhotoViewer.prototype.setText = function (obj) {
    var data = obj.data;

    document.querySelector("input[name='image-id']").value = data.id;

    document.querySelector("span[name='shipRef']").innerHTML = data.ShippingReferenceNo;
    document.querySelector("span[name='location']").innerHTML = data.LocationPictureTaken;
    document.querySelector("span[name='takenBy']").innerHTML = data.TakenBy;
    document.querySelector("span[name='dateTaken']").innerHTML = data.DateTaken;

    document.getElementById('current').innerHTML = obj.curIndx;
    document.getElementById('dataCount').innerHTML = obj.count;

    var annotations = document.querySelector("ul[name='annotations']");
    var helper = new Helper();
    helper.removeChilds(annotations);


    var anntList = data.Annotations.split(',');

    for (var j = 0; j < anntList.length; j++) {
        var item = document.createElement('li');
        item.innerHTML = anntList[j];
        annotations.appendChild(item);
    }

};

PhotoViewer.prototype.initMap = function () {
    if (!this.map) {
        var myOptions = {
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(document.getElementById("image-map"), myOptions);

    } else {
        google.maps.Map.prototype.clearOverlays(this.map, this.markers, this.infoWindows);
    }
};

PhotoViewer.prototype.initZoom = function () {
    if (this.zoom) {
        this.zoom.setValue(0);
    } else {
        this.zoom = new dhtmlXSlider({
            parent: "sliderObj",
            value: 0,
            step: 1,
            size: 150,
            min: 0,
            max: 100
        });

        this.zoom.attachEvent("onChange", function (value) {
            var zoom = (100 + value);

            var img = document.querySelector("#img-container");
            img.style.cssText = "width: " + zoom + "%!important;";
        });
    }
};

PhotoViewer.prototype.show = function (obj) {

    var viewer = this.wndPhotoView;

    viewer.show();
    viewer.setDimension(1200, 800);
    viewer.centerOnScreen();
    viewer.bringToTop();

    this.setImage(obj.data);
    this.setText(obj);
    this.initMap();
    this.initZoom();

    var mapwrapper = new MapWrapper();
    var res = mapwrapper.setMarker({
        locations: obj.data,
        map: this.map,
        ma: this.markers,
        iw: this.infoWindows
    });

    this.markers = res.markers;
    this.infoWindows = res.infoWindows;

};

PhotoViewer.prototype.close = function () {
    var viewer = this.wndPhotoView;

    viewer.attachEvent("onClose", function () {
        viewer.hide();
    });
};






