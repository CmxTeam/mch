﻿
var dvSearchResults,
    sZoom, sw, sh,
    ctSearch, tbContact, tbPhotoView,
    vPhoto, pContactInfo, tShare;

var cntEmails = '';
var selectedFiles;

var mainLayout;
var photoMarker;
var myLatlng, markersArray = [], infWindowsArray = [],
    photoMarkers = [], photoInfos = [];
var count = 0;
var curId;

/*email templates*/
var frmTemplate;

/*The current skin*/
var wndSkin = "dhx_web";

function auth() {
    window.dhx4.ajax.get("Account/ValidateUser", function (data) {
        if (data.xmlDoc.responseText.length < 3) {
            if (mainLayout) {
                mainLayout.unload();
                mainLayout = null;

                var node = document.getElementById("header");
                while (node.hasChildNodes()) {
                    node.removeChild(node.lastChild);
                }
            }
            account.initForm();
        } else {
            var email = JSON.parse(data.xmlDoc.response);
            topHeader.curEmail = email;
            if (account.regWnd) {
                account.regWnd.hide();
            }
            init();
        }
    });

}
function init() {
    var self = this;
    var windows = new dhtmlXWindows();

    this.thumnBar = function () {

        if (tShare) {
            tShare = thumbnail.layout.detachToolbar();
            tShare = null;
        }

        if (searchResult.dataview.getSelected() != "") {
            tShare = thumbnail.layout.attachToolbar({
                items: [
                    { id: "btnShare", type: "button", text: "SHARE", img: window.baseUrl + "Content/imgs/share.png" },
                    { type: "separator" },
                    { id: "bsViews", type: "buttonSelect", openAll: true, text: "VIEWS" },
                    { type: "separator" },
                    { id: "btnSnapshot", type: "button", text: "NEW SNAPSHOT" },
                    { type: "separator" },
                    { id: "btnEdit", type: "button", text: "EDIT SNAPSHOT" }
                ]
            });
            
            tShare.setSkin(wndSkin);

            tShare.addListOption("bsViews", "bsLists", 2, "button", "LIST");
            tShare.addListOption("bsViews", "bsThumb", 1, "button", "THUMBNAILS");
            tShare.addListOption("bsViews", "bsMap", 3, "button", "MAP");

            tShare.setListOptionSelected("bsViews", curId);

            tShare.attachEvent("onClick", function (id) {
               
                if (id === "btnShare") {
                    /*Open shareWnd window*/
                    self.attachFiles();

                    self.share({
                        attached: selectedFiles
                    });

                } else if (id === "btnSnapshot") {
                    //thumbnail.dataview.clearAll();
                    //thumbnail.dataview.unselectAll();
                    //searchResult.dataview.unselectAll();
                    //self.thumnBar();
                    transaction.initForm(windows, null);
                } else if (id === "btnEdit") {
                    transaction.initForm(windows, searchResult.dataview.get(searchResult.curId));
                } else {
                    if (thumbnail.layout.getViewName() === "def") {
                        self.attachFiles();
                    }

                    curId = id;

                    if (id === "bsLists") {
                        self.showView('list');
                        tShare.setListOptionSelected("bsViews", id);
                    } else if (id === "bsThumb") {
                        self.showView('def');
                        tShare.setListOptionSelected("bsViews", id);
                    } else if (id === "bsMap") {
                        self.showView('map');
                        tShare.setListOptionSelected("bsViews", id);
                    }

                    if (!tShare) {
                        self.thumnBar();
                    }

                }
            });
        } else {

            tShare = thumbnail.layout.attachToolbar({
                items: [
                    { id: "btnSnapshot", type: "button", text: "NEW SNAPSHOT" }
                ]
            });
            tShare.setSkin(wndSkin);

            tShare.attachEvent("onClick", function (id) {

                if (id === "btnSnapshot") {
                    thumbnail.dataview.clearAll();
                    transaction.initForm(windows, null);
                }
            });
        }


    };

    dhtmlx.image_path = window.baseUrl + '/Content/imgs/';
    //document.getElementById('layoutObj').style.height = window.innerHeight - 50 + "px";


    //mainLayout = new dhtmlXLayoutObject({
    //    parent: document.getElementById('layoutObj'),
    //    pattern: "2U"
    //});
    //mainLayout.setSkin(wndSkin);

    //topHeader.initHeader(windows);
    
    var a = mainLayout.cells('a');
    a.setWidth(350);
    var layout_1 = a.attachLayout('2E');


    helpers.getDataFromExternalResource("/CargoSnapshotCommon/GetReferenceTypes", null, function (data) {
        /*Thumbnails panel*/
        thumbnail.initForm(mainLayout, data);
        
        filterPanel.initForm(layout_1);
        /*search result panel*/
        searchResult.initForm(layout_1);

        var status_1 = mainLayout.attachStatusBar();
        status_1.setText('All Rights Reserved to Cargomatrix Inc. 1999-2015');

        self.thumnBar();
    });
   
    this.resetFormData = function(form) {
        form.forEachItem(function (id) {
            var controlType = form.getItemType(id);
            if (controlType == "combo") {
                var combo = form.getCombo(id);
                combo.unSelectOption();
                combo.checkUncheckAll();
            } else if (controlType == "calendar") {
                document.querySelector("input[name='" + id + "']").value = '';
            } else if (controlType == "input") {
                form.getInput(id).value = "";
            }
        });
    }


    this.setFormData = function (form, data) {
        self.resetFormData(form);
        form.forEachItem(function(id) {
            var controlType = form.getItemType(id);
            var currentUserData = form.getUserData(id, "DynamicControl");
            if (controlType == "combo") {
                var combo = form.getCombo(id);
                if (data && data[id]) {
                    combo.selectOption(combo.getIndexByValue(data[id]));
                } else if (currentUserData && data[currentUserData.Name]) {
                    combo.selectOption(combo.getIndexByValue(data[currentUserData.Name]));
                }
            } else if (controlType == "calendar") {
                if (data && data[id]) {
                    form.setItemValue(id, new Date(data[id]));
                } else if (currentUserData && data[currentUserData.Name]) {
                    form.setItemValue(id, new Date(data[currentUserData.Name]));
                }
            } else if (controlType == "input" || controlType == "hidden") {
                if (data && data[id]) {
                    form.setItemValue(id, data[id])
                } else if (currentUserData && data[currentUserData.Name]) {
                    form.setItemValue(id, data[currentUserData.Name]);
                }
            }
        });
    }

    this.getDynamicFormData = function (form) {
        var formData = [];
        form.forEachItem(function (id) {
            if (form.getUserData(id, "DynamicControl")) {
                var data = { Id: id };
                var controlType = form.getItemType(id);
                if (controlType == "combo") {
                    data.Source = form.getCombo(id).getSelectedValue();
                } else if (controlType == "calendar") {
                    data.Value = document.querySelector("input[name='" + id + "']").value;
                } else if (controlType == "input") {
                    data.Value = form.getInput(id).value;
                }
                if (data.Value || data.Source)
                    formData.push(data);
            }
        });
        return formData;
    }

    this.generateFormDynamicly = function(form, controls, data) {
        if (form && controls) {
            var index = 0;
            controls.forEach(function (control) {
                var newControl = null;

                switch (control.HtmlType) {
                    case "textbox":
                        newControl = {
                            type: "input", required: control.IsMandatory, className: "req-asterisk", name: control.Id, label: control.DisplayName, inputWidth: 180,
                            validate: function (input) {
                                 return control.IsMandatory ? form.getItemValue(control.Id) && form.getItemValue(control.Id).trim() != '' : true;
                            }
                        };
                        break;
                    case "dropdown":
                        newControl = {
                            type: "combo", required: control.IsMandatory, className: "req-asterisk", name: control.Id, label: control.DisplayName, inputWidth: 180,
                            validate: function (input) { return control.IsMandatory ? form.getCombo(control.Id).getSelectedIndex() !== -1 : true; }
                        };
                        break;
                    case "date":
                        newControl = {
                            type: "calendar", required: control.IsMandatory, className: "req-asterisk", name: control.Id, label: control.DisplayName, dateFormat: "%m-%d-%Y %H:%i", inputWidth: 180, readonly: true,
                            validate: function (input) { return control.IsMandatory ? document.querySelector("input[name='" + control.Id + "']").value && document.querySelector("input[name='" + control.Id + "']").value.trim() != '' : true; }
                        }
                        break;
                    default:
                }
                if (newControl) {
                    form.addItem(null, newControl, index);
                    form.setUserData(newControl.name, "DynamicControl", control);
                    index++;
                }
            });
            
            form.forEachItem(function(id) {
                if (form.getItemType(id) == "combo") {
                    var currentUserData = form.getUserData(id, "DynamicControl");
                    if (currentUserData) {
                        var combo = form.getCombo(id);
                        combo.loadExternal("/CargoSnapshotCommon/GetPropertySource", 'Id', 'PropertySourcesValue', function() {
                            if (data && data[currentUserData.Name]) {
                                combo.selectOption(combo.getIndexByValue(data[currentUserData.Name]));
                            }
                        }, { propertySourceTypeId: currentUserData.PropertySourceTypeId });
                        combo.enableFilteringMode("between");
                    }
                } else if (form.getItemType(id) == "calendar") {
                    form.getCalendar(id).showTime();
                }
            });

            if (data) {
                self.setFormData(form, data);
            }
        }
    }

    this.attachFiles = function () {

        var photos = $("input[name='photo-thumbnails']:checked");//document.querySelectorAll("input[name='photo-thumbnails']:checked");

        var str = '';

        for (var i = 0; i < photos.length; i++) {
            str += (photos[i].value + ',');
        }
        /*attached files*/
        str = str.substring(0, str.length - 1);
        selectedFiles = str.split(',');


    };

    this.setText = function (data) {
        document.querySelector("input[name='image-id']").value = data.id;

        document.querySelector("span[name='shipRef']").innerHTML = data.ShippingReferenceNo;
        document.querySelector("span[name='location']").innerHTML = data.LocationPictureTaken;
        document.querySelector("span[name='takenBy']").innerHTML = data.TakenBy;
        document.querySelector("span[name='dateTaken']").innerHTML = data.DateTaken;

        document.getElementById('current').innerHTML = thumbnail.dataview.indexById(data.id) + 1;
        document.getElementById('dataCount').innerHTML = thumbnail.dataview.dataCount();
    };

    this.deleteChilds = function (el) {
        var myNode = document.querySelector(el);
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
    };

    this.share = function (params) {
        shareWnd.initForm(windows);
        adressBook.dataview.loadExternal('/CargoSnapshotCommon/GetUserProfiles', null, function(data) {
            var photos = document.querySelectorAll(".img-contact");
            for (var i = 0; i < photos.length; i++) {
                photos[i].ondragstart = function() {
                    return false;
                };
            }
        });

        if (params) {
            selectedFiles = params.attached;
        }

        /*ids of checked thumbnmails*/
        var checked = common.toValArray($("input[name='photo-thumbnails']:checked"));
        (thumbnail.curShipment.TransactionPhotos).forEach(function(item) {
            if (checked.indexOf(item.Id.toString()) != -1)
                item.Checked = "Checked";
            else {
                item.Checked = "";
            }
        });

        attachments.dataview.clearAll();

        thumbnail.curShipment.TransactionPhotos.forEach(function (item) { item.Transaction.Reference = thumbnail.curShipment.Reference ? thumbnail.curShipment.Reference : "" });

        attachments.dataview.parse(thumbnail.curShipment.TransactionPhotos, "json");

        attachments.dataview.attachEvent("onItemClick", function (id, ev, html) {
            return false;
        });

        shareWnd.layout.cells('b').setWidth(700);
        shareWnd.layout.cells('c').setWidth(350);


        emailDetails.loadTemplate();

        //var send = document.querySelector("textarea[name='fiSendTo']");
        //dhtmlx.DragControl.addDrop(send.id, {
        //    onDrop: function (source, target, d, e) {
        //        var context = dhtmlx.DragControl.getContext();
        //        var item = context.from.get(context.source[0]);
        //        var contacts = target.value;
        //        if (contacts.indexOf(item.Email) < 0) {
        //            if (target.value.length > 0) {
        //                contacts = contacts + ',';
        //            }
        //            contacts += item.Email;
        //        }
        //        target.value = contacts;
        //    }
        //});
    };

    this.setSelectedFiles = function (attached) {

        if (selectedFiles) {
            for (var i = 0; i < attached.length; i++) {

                if (selectedFiles.indexOf(attached[i].value) >= 0) {
                    attached[i].checked = true;
                } else {
                    attached[i].checked = false;
                }
            }
        }
    };

    this.uploadPhoto = function () {
        vPhoto = document.querySelector("input[name='photo']");

        var data = new FormData();
        data.append('files', vPhoto.files[0]);

        var obj = adressBook.frm.getFormData();

        var params = common.serialize(obj);

        adressBook.Edit(params, function (id) {
            common.uploadFile(vPhoto, "/Contact/UploadImage?id=" + id, function (path) {
                if (path.name) {
                    adressBook.frm.setItemValue("Img", path.name);

                    var data = adressBook.frm.getFormData();
                    data.Img = path.name;

                    if (adressBook.dataview.get(data.id)) {
                        adressBook.dataview.remove(data.id);
                    }
                    adressBook.dataview.add({
                        id: data.id,
                        img: data.Img,
                        Name: data.Name,
                        Title: data.Title,
                        Email: data.Email
                    }, data.id);

                }

                adressBook.fetchPopup();
            });
        });

    };

    this.sendEmail = function () {
        if (emailDetails.wnd.validate()) {
            var attached = $("input[name='attachments']:checked");
            var attachmentFileIds = [];
            
            if (attached.length > 0) {
                var notifTo = [];
                for (var i = 0; i < attached.length; i++) {
                    attachmentFileIds.push(attached[i].value);
                }
                notifTo.push(emailDetails.wnd.getInput("RecepienEmail").value);
                notifTo = notifTo.concat(emailDetails.wnd.getInput("EmailNotification").value.split(","));
                
                helpers.executePost("/CargoSnapshotCommon/SendEmail", {
                    Subject: emailDetails.wnd.getInput("Subject").value,
                    NotifTo: notifTo,
                    Recipients: emailDetails.wnd.getInput("SendTo").value,
                    AttachmentFileIds: attachmentFileIds,
                    Body: emailDetails.wnd.getEditor("MsgBody").getContent()
                },function() {
                    dhtmlx.message({ text: "Email was sent.", expire: 3000 });
                });
            }
        }
    };

    this.showView = function (id) {

        var frstShow = thumbnail.layout.showView(id);
        self.thumnBar();
        if (frstShow) {

            if (id === "list") {
                thumbnail.loadGrid();
            } else if (id === "map") {
                thumbnailsMap.loadMap();
            }
        }
        if (id === "def") {
            //if (selectedFiles.length > 0) {
            //    /*select files*/
            //    var attached = document.querySelectorAll("input[name='photo-thumbnails']");
            //    self.setSelectedFiles(attached);
            //}

            var btns = document.querySelectorAll("input[name='btn-photo-view']");

            if (photoViewer.carouselWmd) {
                photoViewer.unloadForm();
            }

            for (var i = 0; i < btns.length; i++) {
                btns[i].onclick = function (e) {

                    if (!photoViewer.carouselWmd) {
                        photoViewer.initForm(function () {
                            photoViewer.resizeViewer();
                        });
                    }

                    photoViewer.rotate = 0;
                    photoViewer.carouselWmd.show();
                    photoViewer.resizeViewer();

                    photoViewer.selectCell(e.currentTarget.id);

                }
            }
        }

    };

    this.loadMapView = function (id) {

        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(33.890542, 151.274856),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("image-map-" + id), myOptions);
        return map;

    };


    this.parseCoords = function (location) {
        var p = location.replace(/\ /g, '').split(',');
        p[0] = p[0].replace('°', ' ');
        p[1] = p[1].replace('°', ' ');
        for (var i = 0; i < p.length; i++) {
            var s = p[i].split(' ');
            var z = '';
            if (s[1] == 'W' || s[1] == 'S') {
                p[i] = '-' + s[0];
            } else {
                p[i] = s[0];
            }
        }
        return {
            w: p[0],
            l: p[1]
        };
    };

    this.setMarker = function (obj) {

        var data = obj.locations;
        var map = obj.map;
        var ma = obj.ma;
        var iw = obj.iw;

        myLatlng = new google.maps.LatLng(data.Latitude, data.Longitude);

        ma[count] = new google.maps.Marker({
            map: map, title: data.RecDate, position: myLatlng
        });
        map.setCenter(ma[count].getPosition());

        var content =
            "<span class='text-left col-12'><span class='text-green col-12'>Location: </span> <br/>" + (data.LocationName || data.PictureTakenLocation) + '<br/>' +
            "<span class='text-green col-12'>Timestamp: </span> <br/>" + (data.RecDate || window.dhx4.date2str(new Date(data.DateTaken), '%m-%d-%Y %H:%i')) + '<br/>' +
            "<span class='text-green col-12'>Taken by: </span> <br/>" + (data.UserName || data.TakenByUser) + '<br/></span> ';

        iw[count] = new google.maps.InfoWindow();

        google.maps.event.addListener(ma[count], 'click', (function (marker, content, infowindow) {
            return function () {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };
        })(ma[count], content, iw[count]));

        count++;
        return {
            markers: ma,
            infos: iw
        }
    };

    this.setMarkers = function(obj) {
        var map = obj.map;
        var mk = obj.markers;
        var infos = obj.infos;
        
        helpers.getDataFromExternalResource("/CargoSnapshotCommon/GetGoogleMapData", { shipmentReference: searchResult.Reference, refType: thumbnail.selectedReferenceType.Id }, function (data) {
            var markers = [];
            var bounds = new google.maps.LatLngBounds();

            for (var i = 0; i < data.length; i++) {

                markers = self.setMarker({
                    locations: data[i],
                    map: map,
                    ma: mk,
                    iw: infos
                });
            }

            if (markers.length > 0) {
                for (var i = 0; i < markers.markers.length; i++) {
                    bounds.extend(markers.markers[i].position);
                }

                map.fitBounds(bounds);

                thumbnailsMap.markers = markers.markers;
                thumbnailsMap.infos = markers.infos;
                thumbnailsMap.gmap = map;

                thumbnailsMap.sortMarkers(markers);
            }
        });
    };


    google.maps.Map.prototype.clearOverlays = function (data) {

        var markers = data.markers;
        var infos = data.infos;
        var lines = data.lines;


        if (markers) {
            markers.forEach(function (item, i, arr) {
                item.setMap(null);
            });
            photoMarkers.length = 0;
            count = 0;
        }
        if (infos) {
            for (var i = 0; i < infos.length; i++) {
                google.maps.event.clearInstanceListeners(infos[i]);
                infos[i].close();
                infos[i] = null;
            }
            infos.length = 0;

        }

        if (lines) {
            for (var i = 0; i < lines.length; i++) {
                lines[i].setMap(null);
            }
            lines.length = 0;

        }
    };

}






