﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    class UpdateDischargeTask : MchDatabaseConnector<bool>
    {
        readonly long _userId;
        readonly StatusEnum _status;
        readonly IEnumerable<long> _tasks;
        readonly DateTime? _updateDate;
        
        public UpdateDischargeTask(IEnumerable<long> tasks,long userId, StatusEnum status,DateTime? updateDate = null)
        {
            _tasks = tasks;
            _status = status;
            _userId = userId;
            _updateDate = updateDate;
        }
        public override bool Execute()
        {
            var tasks = Context.Tasks.Where(t => _tasks.Contains(t.Id));
            foreach (var task in tasks)
            {
                task.StatusId = (int)_status;               
                TaskTransaction transaction = new TaskTransaction
                {
                    RecDate = DateTime.UtcNow,
                    TaskId = task.Id,
                    UserId = _userId,
                    StatusId = (int)_status,
                    StatusTimestamp = (_updateDate.HasValue && _updateDate.Value != DateTime.MinValue) ? _updateDate.Value : DateTime.UtcNow
                };
                Context.TaskTransactions.Add(transaction);
            }
            Context.SaveChanges();
            return true;
        }
    }
}
