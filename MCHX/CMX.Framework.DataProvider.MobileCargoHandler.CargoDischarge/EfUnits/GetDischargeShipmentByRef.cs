﻿using System;
using System.Linq;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits;
using CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.AdoUnits;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities.SearchModels;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class GetDischargeShipmentByRef : MchDatabaseConnector<DischargeShipmentModel>
    {
        private string _shipingRef;
        private long? _dischargeId;
        public GetDischargeShipmentByRef(string shipingRef,long? dischargeId)
        {
            this._shipingRef = shipingRef;
            this._dischargeId = dischargeId;
        }
        public override DischargeShipmentModel Execute()
        {
            var dischargeShipmentModel = new DischargeShipmentModel();
            var shipment = new GetShipmentByFilter(new ShipmentFilterModel { SerialNumber = _shipingRef }).Execute();
            if (shipment == null) return new DischargeShipmentModel();
            dischargeShipmentModel.EntityId = shipment.Id;
            dischargeShipmentModel.EntityType = shipment.EntityType;
            dischargeShipmentModel.ShipingRef = shipment.SerialNumber;

            DischargeShipment dischargeShipment;

            if (shipment.EntityType == CMX.Framework.DataProvider.Entities.Common.EntityTypeEnum.AWB)
            {
                dischargeShipment = Context.DischargeShipments.Where(a => a.AwbId == shipment.Id).OrderBy(s => s.Date).FirstOrDefault();
                var awb = Context.AWBs.Single(a => a.Id == shipment.Id);
                dischargeShipmentModel.ReceivedPieces = awb.ReceivedPieces;
                dischargeShipmentModel.CustomsBroker = awb.Agent.ConvertTo();
                dischargeShipmentModel.Consignee = awb.Customer.ConvertTo();
                dischargeShipmentModel.IATACodeOrig = awb.Port != null ? awb.Port.IATACode : null;
                dischargeShipmentModel.IATACodeDest = awb.Port1 != null ? awb.Port1.IATACode : null;
                dischargeShipmentModel.Carrier3Code = awb.Carrier != null ? awb.Carrier.Carrier3Code : null;

                var overrideCustoms = new GetShipmentOverrideCustomStatuses(awb.Id, EntityTypeEnum.AWB).Execute().FirstOrDefault();
                var initialCustoms = new GetShipmentCustomsStatuses(awb.AWBSerialNumber, EntityTypeEnum.AWB, true).Execute().FirstOrDefault();
                dischargeShipmentModel.LatestCustomsStatus = overrideCustoms ?? initialCustoms;
                dischargeShipmentModel.LatestCustomsStatus = dischargeShipmentModel.LatestCustomsStatus ?? new CustomsHistoryModel();
                dischargeShipmentModel.ShipmentDischargedPieces = GetShipmentDischargedPieces(awb.Id, EntityTypeEnum.AWB);
            }
            else
            {
                dischargeShipment = Context.DischargeShipments.Where(a => a.HawbId == shipment.Id).OrderBy(s => s.Date).FirstOrDefault();
                var hwb = Context.HWBs.Single(a => a.Id == shipment.Id);
                var awb = Context.AWBs_HWBs.FirstOrDefault(a => a.HWBId == shipment.Id);
                if (awb != null)
                {
                    dischargeShipmentModel.CustomsBroker = awb.AWB.Agent.ConvertTo();
                    dischargeShipmentModel.Carrier3Code = awb.AWB.Carrier != null ? awb.AWB.Carrier.Carrier3Code : null;
                }
                dischargeShipmentModel.ReceivedPieces = hwb.ReceivedPieces;
                dischargeShipmentModel.IATACodeOrig = hwb.Port != null ? hwb.Port.IATACode : null;
                dischargeShipmentModel.IATACodeDest = hwb.Port1 != null ? hwb.Port1.IATACode : null;

                dischargeShipmentModel.Consignee = hwb.Customer.ConvertTo();

                var overrideCustoms = new GetShipmentOverrideCustomStatuses(hwb.Id, EntityTypeEnum.HWB).Execute().FirstOrDefault();
                var initialCustoms = new GetShipmentCustomsStatuses(hwb.HWBSerialNumber, EntityTypeEnum.HWB, true).Execute().FirstOrDefault();
                if (initialCustoms != null && overrideCustoms == null)
                    initialCustoms.CustomsCode.IsGreenCode = new GetAmsCodes(code : initialCustoms.StatusCode).Execute().FirstOrDefault().IsGreenCode; 
                dischargeShipmentModel.LatestCustomsStatus = overrideCustoms ?? initialCustoms;
                dischargeShipmentModel.LatestCustomsStatus = dischargeShipmentModel.LatestCustomsStatus ?? new CustomsHistoryModel();
                dischargeShipmentModel.ShipmentDischargedPieces = GetShipmentDischargedPieces(hwb.Id, EntityTypeEnum.HWB);
            }
            //dischargeShipmentModel.Attachments = Context.Attachments.Include("EntityType").Where(a => a.EntityTypeId == (int)shipment.EntityType && a.EntityID == shipment.Id).ToList().Select(a => a.ConvertTo());

            if (dischargeShipment == null) return dischargeShipmentModel;
            dischargeShipmentModel.PickupDocumentDate = dischargeShipment.Id != 0 ? (DateTime?)dischargeShipment.Date : null;
            dischargeShipmentModel.Status = dischargeShipment.Status.ConvertTo();
            dischargeShipmentModel.Carrier = dischargeShipment.Carrier.ConvertTo();
            dischargeShipmentModel.Station = dischargeShipment.Port.ConvertTo();
            dischargeShipmentModel.InbondNumber = dischargeShipment.InbondNumber;
            dischargeShipmentModel.TransferManifest = dischargeShipment.TransferManifest;
            dischargeShipmentModel.TotalReleasedPieces = dischargeShipment.TotalReleasedPieces;           
            dischargeShipmentModel.IsCustomsOverride = dischargeShipment.IsCustomsOverride;
            dischargeShipmentModel.DischargeId = dischargeShipment.DischargeId;
            dischargeShipmentModel.DischargeShipmentType = dischargeShipment.DischargeType.ConvertTo();
            return dischargeShipmentModel;
        }
        private long GetShipmentDischargedPieces(long shipmentId, EntityTypeEnum shipmentType)
        {
            var dispositionedPieces = (from task in Context.Tasks
                                       join disp in Context.Dispositions on task.Id equals disp.TaskId
                                       join dischargeShipment in Context.DischargeShipments on task.EntityId equals dischargeShipment.Id
                                       where (task.EntityTypeId == (int)EntityTypeEnum.DischargeShipment &&
                                             ((shipmentType == EntityTypeEnum.AWB && dischargeShipment.AwbId == shipmentId)
                                             || (shipmentType == EntityTypeEnum.HWB && dischargeShipment.HawbId == shipmentId)))
                                       select disp.Count).Sum();

            var todaysPieces = (from task in Context.Tasks
                                join dischargeShipment in Context.DischargeShipments on task.EntityId equals dischargeShipment.Id
                                where task.EntityTypeId == (int)EntityTypeEnum.DischargeShipment &&
                                ((shipmentType == EntityTypeEnum.AWB && dischargeShipment.AwbId == shipmentId)
                                              || (shipmentType == EntityTypeEnum.HWB && dischargeShipment.HawbId == shipmentId))
                                select (long?)task.Pieces).Sum();

            var piecesForUi = Context.DischargeShipments.Where(d => d.DischargeId == _dischargeId 
                                && ((shipmentType == EntityTypeEnum.AWB && d.AwbId == shipmentId) || 
                                    (shipmentType == EntityTypeEnum.HWB && d.HawbId == shipmentId))
                                && d.DischargeTypeId != (int)DischargeShipmentTypeEnum.PickupDocuments)
                             .Sum(a => (long?)a.TotalReleasedPieces);
            var totalLockedPieces = (dispositionedPieces.HasValue ? dispositionedPieces.Value : 0) + (todaysPieces.HasValue ? todaysPieces.Value : 0) - (piecesForUi.HasValue ? piecesForUi.Value : 0);
            return totalLockedPieces;
        }
    }
}
