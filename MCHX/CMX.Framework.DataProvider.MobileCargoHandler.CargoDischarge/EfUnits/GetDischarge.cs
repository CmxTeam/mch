﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class GetDischarge : MchDatabaseConnector<DischargeExtendedModel>
    {
        private readonly long _dischargeId;
        public GetDischarge(long dischargeId)
        {
            _dischargeId = dischargeId;
        }

        public override DischargeExtendedModel Execute()
        {
            List<DischargeShipmentModel> shipments = new List<DischargeShipmentModel>();
            var discharge = Context.Discharges.Single(d => d.Id == _dischargeId);

            DischargeExtendedModel commonDischarge = discharge.ConvertTo();
            foreach (var shipment in discharge.DischargeShipments)
            {
                DischargeShipmentModel tmpShipment = shipment.ConvertTo();
                var attachemts = Context.Attachments.Where(a => a.EntityID == tmpShipment.EntityId && a.EntityTypeId == (int)tmpShipment.EntityType).ToList();                
                tmpShipment.Attachments = attachemts.Select(a => a.ConvertTo());

                shipments.Add(tmpShipment);
            }

            commonDischarge.DischargeShipments = shipments;
            commonDischarge.DriverLog = discharge.DriverSecurityLog.ConvertTo();
            return commonDischarge;
        }
    }
}
