﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.AdoUnits;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class CreateTaskForDischargeShipment : MchDatabaseConnector<IEnumerable<long>>
    {
        private long _userId;
        private int _warehouseId;
        private DischargeShipmentModel _discharge;
        private DischargeShipment _dbDischarge;

        public CreateTaskForDischargeShipment(long userId, int warehouseId, DischargeShipmentModel discharge, DischargeShipment dbDischarge)
        {
            _userId = userId;
            _warehouseId = warehouseId;
            _discharge = discharge;
            _dbDischarge = dbDischarge;
        }
        public override IEnumerable<long> Execute()
        {
            List<Task> childTasks = new List<Task>();
            List<long> createdTaskIds = new List<long>();
            var userName = Context.UserProfiles.Single(u => u.Id == _userId).UserId;

            if (_discharge.Id == 0 && (_discharge.DischargeShipmentType.Id != (int)DischargeShipmentTypeEnum.PickupDocuments))
            {
                string airline;
                AccountCarrier carrier;
                try
                {
                    if (_discharge.EntityType == EntityTypeEnum.AWB)
                        carrier = Context.AWBs.First(a => a.Id == _discharge.EntityId).Account.AccountCarriers.FirstOrDefault();
                    else
                        carrier = Context.HWBs.First(a => a.Id == _discharge.EntityId).Account.AccountCarriers.FirstOrDefault();
                    airline = carrier.Carrier.CarrierCode;
                }
                catch (Exception ex)
                {
                    airline = null;
                }
                    
                Task childTask = new Task
                {
                    TaskTypeId = (int)TaskTypeEnum.ReleaseFreight,
                    EntityTypeId = (int)EntityTypeEnum.DischargeShipment,
                    EntityId = _dbDischarge.Id,
                    TaskCreator = userName,
                    TaskReference = new GetEntityReference(_discharge.EntityId, _discharge.EntityType).Execute(),
                    ProgressPercent = 0,
                    Airline = airline,
                    Pieces = (int)_discharge.TotalReleasedPieces,
                    TaskDate = DateTime.Now,
                    StatusId = (int)StatusEnum.NotAssigned,
                    StatusTimestamp = DateTime.Now,
                    WarehouseId = _warehouseId,
                    TaskCreationDate = DateTime.Now
                };
                childTasks.Add(childTask);
                Context.Tasks.Add(childTask);
            }

            Context.SaveChanges();
            createdTaskIds.AddRange(childTasks.Select(s => s.Id));
            new UpdateDischargeTask(createdTaskIds, _userId, StatusEnum.NotAssigned, DateTime.Now).Execute();
            return createdTaskIds;

        }
    }
}
