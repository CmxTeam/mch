﻿using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class GetShipmentOverrideCustomStatuses : MchDatabaseConnector<IEnumerable<CustomsHistoryModel>>
    {
        private readonly long _entityId;
        private readonly EntityTypeEnum _entityType;
        public GetShipmentOverrideCustomStatuses(long entityId, EntityTypeEnum entityType)
        {
            _entityId = entityId;
            _entityType = entityType;
        }
        public override IEnumerable<CustomsHistoryModel> Execute()
        {
            var dbCustomsOverrides = Context.UserCustomsOverrides.Include("AmsCode")
                .Where(s => s.EntityId == _entityId && s.EntityTypeId == (int)_entityType).ToList();
            return dbCustomsOverrides.Select(s => s.ConverTo()).OrderByDescending(s => s.Date);
        }
    }
}
