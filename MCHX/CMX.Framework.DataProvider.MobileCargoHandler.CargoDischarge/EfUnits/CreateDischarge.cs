﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    class CreateDischarge : MchDatabaseConnector<bool>
    {
        private long _userId;
        private int _warehouseId;
        private DischargeExtendedModel _dischargeExtendedModel;
        private Discharge _newDischarge;
        private DriverSecurityLog _newDriverLog;

        private readonly string _attachmentPath = ConfigurationManager.AppSettings["DocumentAttachmentsFolder"];
        private readonly string _driverImagesPath = ConfigurationManager.AppSettings["DriverImagesFolder"];

        public CreateDischarge(DischargeExtendedModel discharge, long userId, int warehouseId)
        {
            _userId = userId;
            _warehouseId = warehouseId;
            _dischargeExtendedModel = discharge;
        }
        public override bool Execute()
        {
            return MakeDatabaseTransaction(() => { CoreExecute(); });
        }
        public bool CoreExecute()
        {
            UpdateDriverLicense();
            _dischargeExtendedModel.Warehouse = new WarehouseModel { Id = _warehouseId };
            _dischargeExtendedModel.UserId = _userId;

            if (_dischargeExtendedModel.Id == 0)
            {
                var dbDischarge = CreateNewDischarge();
                dbDischarge.DriverSecurityLog = CreateNewDriverLog();
                Context.Discharges.Add(dbDischarge);
            }
            else
            {
                var dbDischarge = Context.Discharges.First(d => d.Id == _dischargeExtendedModel.Id);
                UpdateDischarge(dbDischarge, _dischargeExtendedModel);
                RemoveDeletedDischargeShipments(dbDischarge);
            }

            if (_dischargeExtendedModel.DriverLog.Id == 0)
            {
                Context.DriverSecurityLogs.Add(CreateNewDriverLog());
            }
            else
            {
                var dbDriverLog = Context.DriverSecurityLogs.First(l => l.Id == _dischargeExtendedModel.DriverLog.Id);
                UpdateDriverLog(dbDriverLog, _dischargeExtendedModel.DriverLog);
            }

            foreach (var shipment in _dischargeExtendedModel.DischargeShipments)
            {
                DischargeShipment dbDischargeShipment;
                List<Attachment> newAttachments = new List<Attachment>();
                if (shipment.Id == 0)
                {
                    dbDischargeShipment = shipment.ConvertTo();
                    if (_dischargeExtendedModel.Id == 0)
                        dbDischargeShipment.Discharge = CreateNewDischarge();
                    else
                        dbDischargeShipment.DischargeId = _dischargeExtendedModel.Id;
                    Context.DischargeShipments.Add(dbDischargeShipment);
                }
                else
                {
                    dbDischargeShipment = Context.DischargeShipments.First(d => d.Id == shipment.Id);
                    UpdateDischargeShipment(dbDischargeShipment, shipment, _dischargeExtendedModel.Id);
                    RemoveDeletedAttachments(shipment);
                }

                if (shipment.Attachments != null)
                {
                    foreach (var attachmentModel in shipment.Attachments)
                    {
                        if (attachmentModel.DbId == 0)
                        {
                            if (attachmentModel.Image != "data:")// check if no empty file is attached
                                newAttachments.Add(CreateNewAttachment(attachmentModel, dbDischargeShipment));
                        }
                        else
                        {
                            var dbAttachment = Context.Attachments.First(a => a.Id == attachmentModel.DbId);
                            UpdateAttachment(dbAttachment, attachmentModel);
                        }
                    }
                }
                Context.SaveChanges();
                if (newAttachments.Count != 0)
                {
                    foreach (var attachment in newAttachments)
                        attachment.EntityID = dbDischargeShipment.Id;
                    Context.Attachments.AddRange(newAttachments);
                    Context.SaveChanges();
                }
                if (shipment.Id == 0)
                {
                    CreateDischargeShipmentTask(shipment, dbDischargeShipment);
                }
            }
            OverrideCustoms();
            return true;
        }
        private void OverrideCustoms()
        {
            if (_dischargeExtendedModel.DischargeShipments == null) return;
            var customsForOverride = _dischargeExtendedModel.DischargeShipments.Where(s => s.LatestCustomsStatus != null && s.LatestCustomsStatus.Id == 0)
                .GroupBy(k => new { EntityId = k.LatestCustomsStatus.EntityId })
                .Select(s =>
                new CustomsHistoryModel
                {
                    CustomsCode = new AmsCodeModel { Id = s.First().LatestCustomsStatus.CustomsCode.Id },
                    EntityId = s.Key.EntityId,
                    EntityType = s.First().EntityType,
                    Comment = s.First().LatestCustomsStatus.Comment,
                    PiecesAffectedCount = s.First().LatestCustomsStatus.PiecesAffectedCount
                });
            var dbOverrides = customsForOverride.Select(s => s.ConverTo());
            Context.UserCustomsOverrides.AddRange(dbOverrides);
            Context.SaveChanges();
        }
        private void RemoveDeletedDischargeShipments(Discharge dbDischarge)
        {
            var dbDischargeShipments = dbDischarge.DischargeShipments;
            var forDelete = dbDischargeShipments.Where(s => _dischargeExtendedModel.DischargeShipments.All(a => a.Id != s.Id));

            foreach (var dbDisShipment in forDelete)
            {
                RemoveShipmentAttachments(dbDisShipment);
                RemoveDischargeShipmentTask(dbDisShipment);
            }
            Context.DischargeShipments.RemoveRange(forDelete);

            if (!_dischargeExtendedModel.DischargeShipments.Any())
                Context.Discharges.Remove(dbDischarge);

        }
        private void RemoveShipmentAttachments(DischargeShipment shipment)
        {
            var shipmentAttachments = Context.Attachments.Where(a => a.EntityID == shipment.Id && a.EntityTypeId == (int)EntityTypeEnum.DischargeShipment).ToList();
            Context.Attachments.RemoveRange(shipmentAttachments);
        }
        private void RemoveDischargeShipmentTask(DischargeShipment dbDischargeShipment)
        {
            var dischargeShipmentModel = dbDischargeShipment.ConvertTo();
            var dbTask = Context.Tasks.FirstOrDefault(t => t.EntityId == dischargeShipmentModel.Id && t.EntityTypeId == (int)EntityTypeEnum.DischargeShipment);
            if (dbTask != null)
            {
                var dbTaskAssignements = Context.TaskAssignments.Where(a => a.TaskId == dbTask.Id);
                var dbTaskTransactions = Context.TaskTransactions.Where(a => a.TaskId == dbTask.Id);
                Context.TaskAssignments.RemoveRange(dbTaskAssignements);
                Context.TaskTransactions.RemoveRange(dbTaskTransactions);
                Context.Tasks.Remove(dbTask);
            }
        }
        private void RemoveDeletedAttachments(DischargeShipmentModel shipment)
        {
            var shipmentAttachments = Context.Attachments.Where(a => a.EntityID == shipment.Id && a.EntityTypeId == (int)EntityTypeEnum.DischargeShipment).ToList();
            var forDelete = shipmentAttachments.Where(a => shipment.Attachments == null || (shipment.Attachments.All(s => s.DbId != a.Id)));
            Context.Attachments.RemoveRange(forDelete);
        }
        private DriverSecurityLog CreateNewDriverLog()
        {
            if (_newDriverLog == null)
            {
                _newDriverLog = _dischargeExtendedModel.DriverLog.ConvertTo();
                _newDriverLog.UserId = _userId;
                var driverPhotoName = FileHelper.SaveImage(_dischargeExtendedModel.DriverLog.DriverPhoto, _driverImagesPath, string.Empty);
                var idPhotoName = FileHelper.SaveImage(_dischargeExtendedModel.DriverLog.IDPhoto, _driverImagesPath, string.Empty);
                _newDriverLog.DriverPhoto = driverPhotoName;
                _newDriverLog.IDPhoto = idPhotoName;
            }
            return _newDriverLog;
        }
        private Discharge CreateNewDischarge()
        {
            if (_newDischarge == null)
            {
                _newDischarge = _dischargeExtendedModel.ConvertTo();
                _newDischarge.WarehouseId = _warehouseId;
                _newDischarge.UserId = _userId;
            }
            return _newDischarge;
        }
        private Attachment CreateNewAttachment(AttachmentModel attachmentModel, DischargeShipment shipment)
        {
            var dbAttachemnt = attachmentModel.ConvertTo();
            if (dbAttachemnt.Name == null)
                dbAttachemnt.Name = attachmentModel.FileName;
            dbAttachemnt.DocumentTypeId = attachmentModel.DocumentTypeId;
            dbAttachemnt.EntityID = shipment.Id;
            dbAttachemnt.EntityTypeId = (int)EntityTypeEnum.DischargeShipment;
            if (dbAttachemnt.FileType.ToUpper() == "SNAPSHOT")
                dbAttachemnt.FileName = FileHelper.SaveImage(attachmentModel.Image, _attachmentPath, attachmentModel.FileName);
            else
                dbAttachemnt.FileName = FileHelper.SaveFile(attachmentModel.Image, _attachmentPath, attachmentModel.FileName);
            return dbAttachemnt;
        }
        private void CreateDischargeShipmentTask(DischargeShipmentModel shipment, DischargeShipment dbDischargeShipment)
        {
            new CreateTaskForDischargeShipment(_userId, _warehouseId, shipment, dbDischargeShipment).Execute();
        }
        private void UpdateDriverLicense()
        {
            var driver = Context.CarrierDrivers.Single(d => d.Id == _dischargeExtendedModel.DriverLog.CarrierDriver.Id);
            driver.LicenseNumber = _dischargeExtendedModel.DriverLog.LicenseNumber;
        }
        private void UpdateDriverLog(DriverSecurityLog dbDriverLog, DriverSecurityLogModel driverLogModel)
        {
            var photoName = _dischargeExtendedModel.DriverLog.DriverPhoto.Contains("/")
                ? _dischargeExtendedModel.DriverLog.DriverPhoto.Split('/').Last()
                : _dischargeExtendedModel.DriverLog.DriverPhoto.Split('\\').Last();
            var idPhotoName = _dischargeExtendedModel.DriverLog.IDPhoto.Contains('/')
                ? _dischargeExtendedModel.DriverLog.IDPhoto.Split('/').Last()
                : _dischargeExtendedModel.DriverLog.IDPhoto.Split('\\').Last();

            if (photoName != dbDriverLog.DriverPhoto)
                dbDriverLog.DriverPhoto = FileHelper.SaveImage(_dischargeExtendedModel.DriverLog.DriverPhoto, _driverImagesPath, string.Empty);
            if (idPhotoName != dbDriverLog.IDPhoto)
                dbDriverLog.IDPhoto = FileHelper.SaveImage(_dischargeExtendedModel.DriverLog.IDPhoto, _driverImagesPath, string.Empty);

            dbDriverLog.PrimaryIDTypeId = driverLogModel.PrimaryIDTypeId;
            dbDriverLog.PrimaryPhotoIdMatch = driverLogModel.PrimaryPhotoIdMatch;
            dbDriverLog.RecDate = driverLogModel.RecDate == DateTime.MinValue ? DateTime.UtcNow : driverLogModel.RecDate;
            dbDriverLog.SecondaryIdTypeId = driverLogModel.SecondaryIdTypeId;
            dbDriverLog.SecondaryPhotoIdMatch = driverLogModel.SecondaryPhotoIdMatch;
            dbDriverLog.TruckingCompanyId = (int?)driverLogModel.TruckingCompany.Id;
            dbDriverLog.CarrierDriverId = driverLogModel.CarrierDriver.Id;
            dbDriverLog.UserId = _userId;
        }

        private void UpdateDischarge(Discharge dbDischarge, DischargeModel dischargeModel)
        {
            dbDischarge.UserId = _userId;
            dbDischarge.WarehouseId = _warehouseId;
            // dbDischarge.StatusId = (int?)dischargeModel.Status.Id;
            //dbDischarge.StatusTimestamp = dischargeModel.StatusTimestamp;
        }

        private void UpdateDischargeShipment(DischargeShipment dbDischargeShipment, DischargeShipmentModel dischargeShipmentModel, long dischargeId)
        {
            dbDischargeShipment.DischargeId = dischargeId;
            dbDischargeShipment.DischargeTypeId = dischargeShipmentModel.DischargeShipmentType.Id;
            if (dischargeShipmentModel.EntityType == EntityTypeEnum.AWB)
                dbDischargeShipment.AwbId = dischargeShipmentModel.EntityId;
            else dbDischargeShipment.HawbId = dischargeShipmentModel.EntityId;
            dbDischargeShipment.CarrierId = dischargeShipmentModel.Carrier.Id != 0 ? (int?)dischargeShipmentModel.Carrier.Id : null;
            dbDischargeShipment.TransferManifest = dischargeShipmentModel.TransferManifest;
            dbDischargeShipment.InbondNumber = dischargeShipmentModel.InbondNumber;
            dbDischargeShipment.PortId = dischargeShipmentModel.Station.Id != 0 ? (long?)dischargeShipmentModel.Station.Id : null;
            dbDischargeShipment.TotalReleasedPieces = (int)dischargeShipmentModel.TotalReleasedPieces;
            dbDischargeShipment.IsCustomsOverride = dischargeShipmentModel.IsCustomsOverride;
            dbDischargeShipment.Date = DateTime.UtcNow;
        }

        private void UpdateAttachment(Attachment dbAttachment, AttachmentModel attachmentModel)
        {
            dbAttachment.RecDate = attachmentModel.RecDate == DateTime.MinValue ? DateTime.UtcNow : attachmentModel.RecDate;
            dbAttachment.UserId = _userId;
            //dbAttachment.Name = attachmentModel.Name;
            //dbAttachment.FileType = attachmentModel.FileType;
            //dbAttachment.Description = attachmentModel.Description;
            //dbAttachment.EntityID = attachmentModel.EntityID;
            //dbAttachment.EntityTypeId = attachmentModel.EntityTypeId;
            //dbAttachment.AttachmentImage = attachmentModel.Image;
            //dbAttachment.DocumentTypeId = attachmentModel.DocumentTypeId;
        }
    }
}