﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.AdoUnits;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class GetDischargeShipmentsByDischarge : MchDatabaseConnector<IEnumerable<DischargeShipmentModel>>
    {
        private readonly long _dischargeId;
        public GetDischargeShipmentsByDischarge(long dischargeId)
        {
            _dischargeId = dischargeId;
        }
        public override IEnumerable<DischargeShipmentModel> Execute()
        {

            var dischargeShipments = Context.DischargeShipments.Include("AWB").Include("AWB.Port").Include("AWB.Carrier").Include("AWB.Port1")
                .Include("AWB.Agent").Include("AWB.Customer").Include("AWB.DischargeShipments")
                .Include("HWB").Include("HWB.Port").Include("HWB.Port").Include("HWB.Port1").Include("HWB.Customer").Include("HWB.DischargeShipments")
                .Include("Status").Include("DischargeType").Include("Carrier").Include("Port")
                .Where(a => a.DischargeId == _dischargeId).ToList();
            return dischargeShipments.Select(d =>
                {
                    var dischargeShipmentModel = new DischargeShipmentModel();
                    if (d.AwbId.HasValue)
                    {
                        dischargeShipmentModel.EntityType = EntityTypeEnum.AWB;
                        dischargeShipmentModel.EntityId = d.AwbId.Value;
                        dischargeShipmentModel.ShipingRef = d.AWB.AWBSerialNumber;
                        //dischargeShipmentModel.AvailablePieces = d.AWB.ReceivedPieces.TryParse<int>() - d.AWB.DischargeShipments.Where(sh => sh.DischargeTypeId != 1).Sum(sh => sh.TotalReleasedPieces);
                        dischargeShipmentModel.CustomsBroker = d.AWB.Agent.ConvertTo();
                        dischargeShipmentModel.Consignee = d.AWB.Customer.ConvertTo();
                        dischargeShipmentModel.IATACodeOrig = d.AWB.Port != null ? d.AWB.Port.IATACode : null;
                        dischargeShipmentModel.IATACodeDest = d.AWB.Port1 != null ? d.AWB.Port1.IATACode : null;
                        dischargeShipmentModel.Carrier3Code = d.AWB.Carrier != null ? d.AWB.Carrier.Carrier3Code : null;
                        dischargeShipmentModel.Attachments = Context.Attachments.Include("EntityType").Where(a => a.EntityTypeId == (int)EntityTypeEnum.AWB && a.EntityID == d.AwbId).ToList().Select(a => a.ConvertTo());
                        var overrideCustoms = new GetShipmentOverrideCustomStatuses(d.AwbId.Value, EntityTypeEnum.AWB).Execute().FirstOrDefault();
                        var initialCustoms = new GetShipmentCustomsStatuses(d.AWB.AWBSerialNumber, EntityTypeEnum.AWB, true).Execute().FirstOrDefault();
                        dischargeShipmentModel.LatestCustomsStatus = overrideCustoms ?? initialCustoms;

                        dischargeShipmentModel.ShipmentDischargedPieces = GetShipmentDischargedPieces(d.AwbId.Value, EntityTypeEnum.AWB);
                    }
                    else
                    {
                        var awb = Context.AWBs_HWBs.FirstOrDefault(a => a.HWBId == d.HawbId);
                        if (awb != null)
                        {
                            dischargeShipmentModel.CustomsBroker = awb.AWB.Agent.ConvertTo();
                            dischargeShipmentModel.Carrier3Code = awb.AWB.Carrier != null ? awb.AWB.Carrier.Carrier3Code : null;
                        }
                        dischargeShipmentModel.IATACodeOrig = d.HWB.Port != null ? d.HWB.Port.IATACode : null;
                        dischargeShipmentModel.IATACodeDest = d.HWB.Port1 != null ? d.HWB.Port1.IATACode : null;
                        dischargeShipmentModel.Consignee = d.HWB.Customer.ConvertTo();
                        dischargeShipmentModel.Attachments = Context.Attachments.Include("EntityType").Where(a => a.EntityTypeId == (int)EntityTypeEnum.HWB && a.EntityID == d.HawbId).ToList().Select(a => a.ConvertTo());
                        var overrideCustoms = new GetShipmentOverrideCustomStatuses(d.HawbId.Value, EntityTypeEnum.HWB).Execute().FirstOrDefault();
                        var initialCustoms = new GetShipmentCustomsStatuses(d.HWB.HWBSerialNumber, EntityTypeEnum.HWB, true).Execute().FirstOrDefault();
                        dischargeShipmentModel.LatestCustomsStatus = overrideCustoms ?? initialCustoms;
                        dischargeShipmentModel.ShipmentDischargedPieces = GetShipmentDischargedPieces(d.HawbId.Value, EntityTypeEnum.HWB);
                    }
                    dischargeShipmentModel.Status = d.Status.ConvertTo();
                    //dischargeShipmentModel.Action = d.DischargeType.ConvertTo();
                    //dischargeShipmentModel.PickupDocumentDate = d.Date;   
                    dischargeShipmentModel.Carrier = d.Carrier.ConvertTo();
                    dischargeShipmentModel.Station = d.Port.ConvertTo();
                    dischargeShipmentModel.InbondNumber = d.InbondNumber;
                    dischargeShipmentModel.TransferManifest = d.TransferManifest;
                    dischargeShipmentModel.TotalReleasedPieces = d.TotalReleasedPieces;                   
                    dischargeShipmentModel.IsCustomsOverride = d.IsCustomsOverride;
                    dischargeShipmentModel.DischargeId = d.DischargeId;
                    dischargeShipmentModel.DischargeShipmentType = d.DischargeType.ConvertTo();
                    dischargeShipmentModel.Id = d.Id;
                    return dischargeShipmentModel;
                });
        }
        private int GetShipmentDischargedPieces(long shipmentId, EntityTypeEnum shipmentType)
        {
            var dispositionedPieces = Context.Dispositions.Where(d => ((shipmentType == EntityTypeEnum.AWB && d.AWBId == shipmentId) ||
                                            (shipmentType == EntityTypeEnum.HWB && d.HWBId == shipmentId))
                                            && d.Task.TaskTypeId == (int)TaskTypeEnum.FreightDischarge
                                            && d.Task.EndDate != null).Sum(s => s.Count);
            var todaysPieces = Context.Tasks.Where(d => d.EntityTypeId == (int)shipmentType && d.EntityId == shipmentId
                                                    && d.TaskTypeId == (int)TaskTypeEnum.FreightDischarge && d.EndDate == null
                                                   ).Sum(s => s.Pieces);
            var totalLockedPieces = (dispositionedPieces.HasValue ? dispositionedPieces.Value : 0) + todaysPieces;
            return totalLockedPieces;
        }
    }
}
