﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class GetShiperVerificationDocuments : MchDatabaseConnector<IEnumerable<ShipperVerificationDocModel>>
    {
        public override IEnumerable<ShipperVerificationDocModel> Execute()
        {
            return Context.ShipperVerificationDocs.Select(s => new ShipperVerificationDocModel { Id = s.Id, Name = s.DocumentName }).ToList();
        }
    }
}
