﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits
{
    public class GetDischargeTasks : MchDatabaseConnector<IEnumerable<AttachmentModel>>
    {
        private long _userId;
        private long _warehouseId;

        public GetDischargeTasks(long userId, int warehouseId)
        {
            _userId = userId;
            _warehouseId = warehouseId;
        }

        public override IEnumerable<AttachmentModel> Execute()
        {
            //var dischargeTasks = Context.Discharges.Where(d => d.DischargeShipments.Any(dsh => dsh.));

            return null;
        }
    }
}
