﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Collections.Generic;
using DhtmlxComponents.Widgets;
using CMX.Framework.DataProvider.Controllers;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CargoDischargeWebSpecificController : CommonController
    {
        private CargoDischargeDatabaseManager _manager = new CargoDischargeDatabaseManager();

        [HttpGet]
        public Grid GetShipmentTransactions(long shipmentId, EntityTypeEnum shipmentType,long pageNumber,long count)
        {
            var shipmentTransactions = _manager.GetShipmentTransactions(ApplicationUser.UserId, shipmentId, shipmentType).ToList();
            return new Grid
            {
                rows = shipmentTransactions.Select(s => new Row
                {
                    id = s.Id.ToString(),
                    data = new List<string> { s.TransactionAction.Code != null ? s.TransactionAction.Code.ToString() : String.Empty, 
                                              s.Reference != null ? s.Reference.ToString() : String.Empty, 
                                              s.StatusTimestamp != null ? s.StatusTimestamp.ToString() : String.Empty }
                })
            };
        }  
    }
}
