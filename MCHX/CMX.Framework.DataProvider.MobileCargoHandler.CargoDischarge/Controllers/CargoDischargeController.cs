﻿using System.Web.Http;
using System.Web.Http.Cors;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Controllers;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities.SearchModels;
using DataExporter.Excel;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CargoDischargeController : MchCommonController
    {
        private CargoDischargeDatabaseManager _manager = new CargoDischargeDatabaseManager();

        [HttpGet]
        public DataContainer<DischargeShipmentModel> GetDischargeShipmentByRef(string shipingRef,long? dischargeId = null)
        {
            return MakeTransactional(() => _manager.GetDischargeShipmentByRef(shipingRef,dischargeId));
        }

        [HttpGet]
        public DataContainer<DischargeExtendedModel> GetDischarge(long dischargeId)
        {
            return MakeTransactional(() => _manager.GetDischarge(dischargeId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<ShipperVerificationDocModel>> GetShiperVerificationDocuments()
        {
            return MakeTransactional(() => _manager.GetShiperVerificationDocuments());
        }

        [HttpPost]
        public DataContainer<bool> CreateDischarge(DischargeExtendedModel discharge)
        {
            return MakeTransactional(() => _manager.CreateDischarge(discharge, ApplicationUser.UserId, ApplicationUser.DefaultWarehouseId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<StatusModel>> GetDischargeStatuses()
        {
            return MakeTransactional(() => _manager.GetStatuses(EntityTypeEnum.DischargeManifest, 50, 51, 52, 53));
        }

        [HttpGet]
        public DataContainer<IEnumerable<DischargeExtendedModel>> GetDischargeByFilter([FromUri]DischargeFilterModel searchModel)
        {
            return MakeTransactional(() => _manager.GetDischargeByFilter(ApplicationUser.UserId, searchModel));
        }

        [HttpGet]
        public DataContainer<byte[]> DischargeTransactionsExcelExport([FromUri]DischargeFilterModel searchModel)
        {

            var manager = new CargoDischargeDatabaseManager();
            var discharges = manager.GetDischargeByFilter(ApplicationUser.UserId, searchModel).ToList();

            var excelData = new ExcelData
            {
                Headers = new List<string> { "Discharge Type", "Shipping Reference", "Discharge Date", "Trucking Company", "Driver", "Total Pieces Released", "Status" },
                SheetName = "EXPLORER",
                DataRows = new List<List<string>>()
            };

            discharges.ForEach(discharge =>
            {
                discharge.DischargeShipments.ToList().ForEach(dischargeShipment =>
                {
                    excelData.DataRows.Add(new List<string>
                    {
                        dischargeShipment.DischargeShipmentType.Name,
                        dischargeShipment.IATACodeOrig + '-' + dischargeShipment.Carrier3Code + '-' + dischargeShipment.ShipingRef + '-' + dischargeShipment.IATACodeDest,
                        discharge.Date.ToShortDateString(),
                        discharge.DriverLog.TruckingCompany.CarrierName ,
                        discharge.DriverLog.CarrierDriver.FullName,
                        dischargeShipment.TotalReleasedPieces.ToString(),
                        dischargeShipment.Status.Name
                    });
                });
            });

            var excelManager = new ExcelManager();
            var excel = excelManager.GenerateExcel(excelData);

            return MakeTransactional(() => excel);
        }

        [HttpGet]
        public DataContainer<IEnumerable<WarehouseDischargeModel>> GetWarehouseDischarges()
        {
            return MakeTransactional(() => _manager.GetWarehouseDischarges(ApplicationUser.UserId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<WarehouseDischargeTaskModel>> GetWarehouseDischargeTasks(long dischargeId)
        {
            return MakeTransactional(() => _manager.GetWarehouseDischargeTasks(ApplicationUser.UserId, dischargeId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<ForkliftItem>> GetForkliftItems(long taskId)
        {
            return MakeTransactional(() => _manager.GetForkliftItems(ApplicationUser.UserId, taskId));
        }

        [HttpPost]
        public DataContainer<bool> AddToForklift(ForkliftFilterModel model)
        {
            return MakeTransactional(() => _manager.AddToForklift(ApplicationUser.UserId, model.TaskId, model.ShipmentType, model.ShipmentId, model.Count.Value));
        }

        [HttpPost]
        public DataContainer<bool> RemoveFromForklift(ForkliftFilterModel model)
        {
            return MakeTransactional(() => _manager.RemoveFromForklift(ApplicationUser.UserId, model.TaskId, model.ShipmentType, model.ShipmentId, model.Count));
        }

        [HttpPost]
        public DataContainer<bool> DropPiecesFromForkliftToLocation(ForkliftToLocationParamModel model)
        {
            return MakeTransactional(() => _manager.DropPiecesFromForkliftToLocation(ApplicationUser.UserId, model.TaskIds, model.LocationId, model.Count));
        }
    }
}