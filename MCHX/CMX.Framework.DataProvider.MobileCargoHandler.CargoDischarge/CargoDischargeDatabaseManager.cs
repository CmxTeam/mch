﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.EfUnits;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities.SearchModels;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge
{
    public class CargoDischargeDatabaseManager : MchCommonDatabaseManager
    {        
        private ConnectorExecutor _connectorExecutor = new ConnectorExecutor();
        
        public DischargeShipmentModel GetDischargeShipmentByRef(string shipingRef,long? dischargeId = null)
        {
            return _connectorExecutor.Execute(() => new GetDischargeShipmentByRef(shipingRef,dischargeId));
        }

        public bool CreateDischarge(DischargeExtendedModel discharge, long userId, int warehouseId)
        {
            return _connectorExecutor.Execute(() => new CreateDischarge(discharge, userId, warehouseId));
        }

        public DischargeExtendedModel GetDischarge(long dischargeId)
        {
            return _connectorExecutor.Execute(() => new GetDischarge(dischargeId));
        }

        public IEnumerable<ShipperVerificationDocModel> GetShiperVerificationDocuments()
        {
            return _connectorExecutor.Execute(() => new GetShiperVerificationDocuments());
        }

        public IEnumerable<DischargeExtendedModel> GetDischargeByFilter(long userId,DischargeFilterModel searchModel)
        {
            return _connectorExecutor.Execute(() => new GetDischargeByFilter(userId,searchModel));
        }

        public IEnumerable<WarehouseDischargeModel> GetWarehouseDischarges(long userId)
        {
            return _connectorExecutor.Execute(() => new GetWarehouseDischarges(userId));
        }

        public IEnumerable<WarehouseDischargeTaskModel> GetWarehouseDischargeTasks(long userId, long dischareId)
        {
            return _connectorExecutor.Execute(() => new GetWarehouseDischargeTasks(userId, dischareId));
        }

        public IEnumerable<ForkliftItem> GetForkliftItems(long userId, long taskId)
        {
            return _connectorExecutor.Execute(() => new GetForkliftItems(userId, taskId));
        }

        public bool AddToForklift(long userId, long taskId, EntityTypeEnum shipmentType, long shipmentId, int count)
        {
            return _connectorExecutor.Execute(() => new AddToForklift(userId, taskId, shipmentType, shipmentId, count));
        }

        public bool RemoveFromForklift(long userId, long taskId, EntityTypeEnum shipmentType, long shipmentId, int? count)
        {
            return _connectorExecutor.Execute(() => new RemoveFromForklift(userId, taskId, shipmentType, shipmentId, count));
        }

        public bool DropPiecesFromForkliftToLocation(long userId, string taskIds, long locationId, int? count)
        {
            return _connectorExecutor.Execute(() => new DropPiecesFromForkliftToLocation(userId, taskIds, locationId, count));
        }
    }
}