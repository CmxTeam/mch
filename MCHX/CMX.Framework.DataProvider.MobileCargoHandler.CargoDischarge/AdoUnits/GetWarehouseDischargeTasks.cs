﻿using System.Data;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class GetWarehouseDischargeTasks : MCHAdoDatabaseConnector<IEnumerable<WarehouseDischargeTaskModel>>     
    {
        public GetWarehouseDischargeTasks(long userId, long dischargeId)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DischargeId", dischargeId))
        { 
        }

        public override IEnumerable<WarehouseDischargeTaskModel> ObjectInitializer(List<DataRow> rows)
        {
            return rows.OfType<DataRow>().Select(r => new WarehouseDischargeTaskModel
            {
                Id = r["TaskId"].TryParse<long>(),
                Piecses = r["Pieces"].TryParse<int>(),
                Locations = r["Locations"].TryParse<string>(),
                ScannedCount = r["ScannedCount"].TryParse<int>(),
                ShipmentId = r["ShipmentId"].TryParse<long>(),
                ShipmentNumber = r["ShipmentNumber"].TryParse<string>(),
                ShipmentType = (EntityTypeEnum)r["ShipmentType"].TryParse<int>()                
            });
        }

        public override string CommandQuery
        {
            get
            {
                return @"SELECT 
	                    Tasks.Id as TaskId,
	                    Case when DischargeShipments.AwbId is null then 3 else 2 end as ShipmentType,
	                    case when DischargeShipments.AwbId is null then HawbId else AwbId end as ShipmentId,
	                    case when DischargeShipments.AwbId is null then Hwbs.HwbSerialNumber else Awbs.AwbSerialNumber end as ShipmentNumber,
	                    Tasks.Pieces,
	                    IsNull(T.ScnnedCount, 0) as ScannedCount,
	                    [dbo].[GetShipmentDischargeLocations](Tasks.Id, 
			                    case when DischargeShipments.AwbId is null then HawbId else AwbId end,
			                    Case when DischargeShipments.AwbId is null then 3 else 2 end) as Locations
                    FROM DischargeShipments 
	                    Inner Join Tasks on DischargeShipments.Id = Tasks.EntityId and Tasks.EntityTypeId = 11 
	                    Inner Join TaskAssignments on TaskAssignments.TaskId = Tasks.Id
	                    Left  Join Awbs on Awbs.Id = DischargeShipments.AwbId
	                    Left  Join Hwbs on Hwbs.Id = DischargeShipments.HawbId
	                    outer apply
	                    (
		                    Select Sum(Dispositions.Count) as ScnnedCount from Dispositions Where Dispositions.TaskId = Tasks.Id
	                    ) as T
                    Where DischargeId = @DischargeId and DischargeShipments.DischargeTypeId != 1 and
                    TaskAssignments.UserId = @UserId and TaskTypeId = 14 And DischargeShipments.DischargeTypeId != 1 and
	                    Tasks.StatusId in (12, 13, 14, 15, 17, 19) and TaskDate = cast(getutcdate() as date)";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}