﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class RemoveFromForklift : MCHAdoDatabaseConnector<bool>
    {
        public RemoveFromForklift(long userId, long taskId, EntityTypeEnum shipmentType, long shipmentId, int? count)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@TaskId", taskId),
                   new CommandParameter("@ShipmentTypeId", (int)shipmentType), new CommandParameter("@ShipmentId", shipmentId),
                   new CommandParameter("@Count", count)
            )
        {
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"If(@Count is null or @Count >= (Select Pieces From ForkliftDetails Where UserId = @UserId and TaskId = @TaskId and 
	                        (case when @ShipmentTypeId = 2 then AwbId else HwbId end) = @ShipmentId))
                        Begin
	                        Delete From ForkliftDetails Where UserId = @UserId and TaskId = @TaskId and 
	                        (case when @ShipmentTypeId = 2 then AwbId else HwbId end) = @ShipmentId	
                        End
                        Else
                        Begin
	                        Update ForkliftDetails Set Pieces = Pieces - @Count Where UserId = @UserId and TaskId = @TaskId and 
	                        (case when @ShipmentTypeId = 2 then AwbId else HwbId end) = @ShipmentId
                        End";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}