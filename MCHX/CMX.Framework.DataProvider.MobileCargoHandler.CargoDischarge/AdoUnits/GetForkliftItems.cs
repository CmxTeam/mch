﻿using System.Data;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class GetForkliftItems : MCHAdoDatabaseConnector<IEnumerable<ForkliftItem>>
    {
        public GetForkliftItems(long userId, long taskId)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@TaskId", taskId))
        {
        }

        public override IEnumerable<ForkliftItem> ObjectInitializer(List<DataRow> rows)
        {
            return rows.OfType<DataRow>().Select(r => new ForkliftItem
            {
                Id = r["TaskId"].TryParse<long>(),
                Pieces = r["Pieces"].TryParse<int>(),
                ShipmentId = r["ShipmentId"].TryParse<long>(),
                ShipmentNumber = r["ShipmentNumber"].TryParse<string>(),
                ShipmentType = (EntityTypeEnum)r["ShipmentType"].TryParse<int>()
            });
        }

        public override string CommandQuery
        {
            get
            {
                return @"Declare @DischargeId bigint

                            Select Top 1 @DischargeId = DischargeShipments.DischargeId 
                            From DischargeShipments 
	                            Inner Join Tasks on DischargeShipments.Id = Tasks.EntityId and Tasks.EntityTypeId = 11 
	                            Inner Join TaskAssignments on TaskAssignments.TaskId = Tasks.Id
                            Where TaskAssignments.UserId = @UserId and Tasks.Id = @TaskId and TaskTypeId = 14 and DischargeShipments.DischargeTypeId != 1
	                            and Tasks.StatusId in (12, 13, 14, 15, 17, 19) and TaskDate = cast(getutcdate() as date)


                            Select 
	                            case when ForkliftDetails.AwbId is null then 3 else 2 end as ShipmentTypeId,
	                            case when ForkliftDetails.AwbId is null then ForkliftDetails.HwbId else ForkliftDetails.AwbId end as ShipmentId,
	                            case when ForkliftDetails.AwbId is null then Hwbs.HwbSerialNumber else Awbs.AwbSerialNumber end as ShipmentNumber,
	                            ForkliftDetails.Pieces,
	                            TaskId
                            from ForkliftDetails 
                            Left  Join Awbs on Awbs.Id = ForkliftDetails.AwbId
                            Left  Join Hwbs on Hwbs.Id = ForkliftDetails.HwbId
                            Where TaskId in (Select Tasks.Id From DischargeShipments 
	                            Inner Join Tasks on DischargeShipments.Id = Tasks.EntityId and Tasks.EntityTypeId = 11 
	                            Inner Join TaskAssignments on TaskAssignments.TaskId = Tasks.Id
                            Where TaskAssignments.UserId = @UserId and DischargeShipments.DischargeId = @DischargeId
	                            and TaskTypeId = 14 and DischargeShipments.DischargeTypeId != 1
	                            and Tasks.StatusId in (12, 13, 14, 15, 17, 19) and TaskDate = cast(getutcdate() as date))";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
