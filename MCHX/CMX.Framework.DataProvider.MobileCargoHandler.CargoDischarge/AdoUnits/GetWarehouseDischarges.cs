﻿using System.Data;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class GetWarehouseDischarges : MCHAdoDatabaseConnector<IEnumerable<WarehouseDischargeModel>>        
    {
        public GetWarehouseDischarges(long userId)
            : base(new CommandParameter("@UserId", userId))
        { }

        public override IEnumerable<WarehouseDischargeModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.OfType<DataRow>().Select(r => new WarehouseDischargeModel
            {
                Id = r["Id"].TryParse<long>(),
                DriverName = r["FullName"].TryParse<string>(),
                DriverPhoto = r["DriverPhoto"].TryParse<string>(),
                CarrierCode = r["Carrier3Code"].TryParse<string>(),
                ShipmentsCount = r["ShipmentsCount"].TryParse<int>(),
                PiecesCount = r["PiecesCount"].TryParse<int>(),
                ScannedCount = r["ScannedCount"].TryParse<int>(),                
            });
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select Discharges.Id, FullName, DriverSecurityLogs.DriverPhoto, Carriers.Carrier3Code,
	                        Count(distinct DischargeShipments.HawbId) + Count(distinct DischargeShipments.AwbId) as ShipmentsCount,
	                        Sum (Tasks.Pieces) as PiecesCount,
	                        Sum(isnull(T.ScnnedCount, 0)) as ScannedCount	
                        from Tasks 
                        Inner Join TaskAssignments on TaskAssignments.TaskId = Tasks.Id
                        Inner Join DischargeShipments On DischargeShipments.Id = Tasks.EntityId and Tasks.EntityTypeId = 11
                        Inner Join Discharges on Discharges.Id = DischargeShipments.DischargeId
                        Inner Join DriverSecurityLogs on DriverSecurityLogs.Id = Discharges.DriverLogId
                        Inner Join CarrierDrivers On CarrierDrivers.Id = DriverSecurityLogs.CarrierDriverId
                        Inner Join Carriers on DriverSecurityLogs.TruckingCompanyId = Carriers.Id
                        outer apply
                        (
	                        Select Sum(Dispositions.Count) as ScnnedCount from Dispositions Where Dispositions.TaskId = Tasks.Id
                        ) as T
                        Where TaskAssignments.UserId = @UserId and TaskTypeId = 14 And DischargeShipments.DischargeTypeId != 1 and
	                        Tasks.StatusId in (12, 13, 14, 15, 17, 19) and TaskDate = cast(getutcdate() as date)
                        Group By Discharges.Id, CarrierDrivers.Id, CarrierDrivers.FullName, DriverSecurityLogs.DriverPhoto, 
	                        Carriers.Id, Carriers.Carrier3Code";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}