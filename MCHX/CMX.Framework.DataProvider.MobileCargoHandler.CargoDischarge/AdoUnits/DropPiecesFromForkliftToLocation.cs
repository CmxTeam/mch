﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class DropPiecesFromForkliftToLocation : MCHAdoDatabaseConnector<bool>
    {
        public DropPiecesFromForkliftToLocation(long userId, string taskIds, long locationId, int? count)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@TaskIds", taskIds),
                   new CommandParameter("@LocationId", locationId), new CommandParameter("@Count", count)
            )
        {
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"SET XACT_ABORT ON
                        Begin Transaction

	                        If(@Count is null)
	                        Begin
		                        Insert Into Dispositions (TaskId, AwbId, HwbId, Count, WarehouseLocationId, Timestamp, UldId, PackageId, IsAdjust, UserId)
		                        Select TaskId, AwbId, HwbId, Pieces, @LocationId, getutcdate(), null, null, 0, @UserId
		                        from ForkliftDetails
		                        Where TaskId in (Select Item from dbo.Split(@TaskIds, ','))

		                        Delete ForkliftDetails Where TaskId in (Select Item from dbo.Split(@TaskIds, ','))
	                        End
	                        Else If ((Select Count(Item) from dbo.Split(@TaskIds, ',')) = 1) -- count can be passed if one task has been selected and maually entered pieces count
	                        Begin
		                        If((Select Pieces From ForkliftDetails Where TaskId = @TaskIds) < @Count)
		                        Begin
			                        Select @Count = Pieces From ForkliftDetails Where TaskId = @TaskIds
		                        End

		                        Insert Into Dispositions (TaskId, AwbId, HwbId, Count, WarehouseLocationId, Timestamp, UldId, PackageId, IsAdjust, UserId)
		                        Select TaskId, AwbId, HwbId, @Count, @LocationId, getutcdate(), null, null, 0, @UserId
		                        from ForkliftDetails
		                        Where TaskId in (Select Item from dbo.Split(@TaskIds, ','))	

		                        Update ForkliftDetails Set Pieces = Pieces - @Count Where TaskId = @TaskIds

		                        If((Select Pieces From ForkliftDetails Where TaskId = @TaskIds) <= 0)
		                        Begin
			                        Delete ForkliftDetails Where TaskId = @TaskIds
		                        End
	                        End

                        Commit Transaction
                        SET XACT_ABORT OFF";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}