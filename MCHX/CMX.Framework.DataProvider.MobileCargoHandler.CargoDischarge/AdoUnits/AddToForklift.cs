﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class AddToForklift : MCHAdoDatabaseConnector<bool>
    {
        public AddToForklift(long userId, long taskId, EntityTypeEnum shipmentType, long shipmentId, int count)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@TaskId", taskId),
                   new CommandParameter("@ShipmentTypeId", (int)shipmentType), new CommandParameter("@ShipmentId", shipmentId),
                   new CommandParameter("@Count", count)
            )
        {
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"If Not Exists (Select top 1 1 From ForkliftDetails Where UserId = @UserId and TaskId = @TaskId and 
	                        (case when @ShipmentTypeId = 2 then AwbId else HwbId end) = @ShipmentId)
                        Begin
	                        Insert Into ForkliftDetails (HwbId, AwbId, Pieces, UldId, TaskId, Timestamp, PackageId, UserId)
	                        Values (case when @ShipmentTypeId = 3 then @ShipmentId else null end, 
		                        case when @ShipmentTypeId = 2 then @ShipmentId else null end,
		                        @Count, null, @TaskId, getutcdate(), null, @UserId)
                        End
                        Else
                        Begin
	                        Update ForkliftDetails Set Pieces = Pieces + @Count 
	                        Where UserId = @UserId and TaskId = @TaskId
                        End";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
