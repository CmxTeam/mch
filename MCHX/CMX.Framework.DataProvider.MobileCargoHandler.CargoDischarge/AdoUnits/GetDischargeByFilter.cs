﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities.SearchModels;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.AdoUnits
{
    public class GetDischargeByFilter : MCHAdoDatabaseConnector<IEnumerable<DischargeExtendedModel>>
    {
        private string[] _rowSpliter = new string[] { "##" };
        private string[] _columnSpliter = new string[] { "$$" };

        public GetDischargeByFilter(long userId, DischargeFilterModel searchModel)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DateFrom", searchModel.DateFrom), new CommandParameter("@DateTo", searchModel.DateTo),
                   new CommandParameter("@StatusId", searchModel.StatusId), new CommandParameter("@DriverId", searchModel.DriverId), new CommandParameter("@TruckingCompanyId", searchModel.TruckingCompanyId),
                   new CommandParameter("@CarrierId", searchModel.CarrierId), new CommandParameter("@DischargeTypeId", searchModel.DischargeTypeId), new CommandParameter("@HwbSerialNumber", searchModel.HwbSerialNumber),
                   new CommandParameter("@AwbSerialNumber", searchModel.AwbSerialNumber))
        { }

        public override IEnumerable<DischargeExtendedModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            var discharges = rows.OfType<DataRow>()
                .GroupBy(row =>
                new
                {
                    Id = row["Id"].TryParse<long>(),
                    Date = row["Date"].TryParse<DateTime>(),
                    DriverLogId = row["DriverLogId"].TryParse<long>(),
                    StatusId = row["StatusId"].TryParse<int>(),
                    StatusName = row["DischargeStatusName"].ToString(),
                    StatusTimestamp = row["StatusTimestamp"].TryParse<DateTime>(),
                    WarehouseId = row["WarehouseId"].TryParse<long>(),
                    WarehouseName = row["DischargeWarehousCode"].ToString()
                })
                .Select(d =>
                {
                    DischargeExtendedModel discharge = new DischargeExtendedModel();
                    discharge.Id = d.Key.Id;
                    discharge.Date = d.Key.Date;
                    discharge.DriverLogId = d.Key.DriverLogId;
                    discharge.Status = new StatusModel { Id = d.Key.StatusId, Name = d.Key.StatusName };
                    discharge.StatusTimestamp = d.Key.StatusTimestamp;
                    discharge.Warehouse = new WarehouseModel { Id = d.Key.WarehouseId, Name = d.Key.WarehouseName };
                    discharge.DischargeShipments = d.Select(r => InitDischargeShipmentModel(r));
                    discharge.DriverLog = d.Select(r => InitDriverSecurityLogModel(r)).FirstOrDefault();
                    return discharge;
                }).ToList();

            return discharges;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Declare @RowSpliter nvarchar(2) = '##'
                         Declare @ColumnSpliter nvarchar(2) = '$$'

        select  case when TBL.AwbId is not null THEN
			[dbo].[Minimum](ISNULL(CustomsStatuses.QuantityAffected,0) - IsNull(TBL.DispositionReleasedPieces,0), ReceivedPieces - (select sum(TotalReleasedPieces) from DischargeShipments where DischargeTypeId = 1 and AwbId = TBL.AwbId))
		else 
			[dbo].[Minimum](ISNULL(CustomsStatuses.QuantityAffected,0) - IsNull(TBL.DispositionReleasedPieces,0),ReceivedPieces - (select sum(TotalReleasedPieces) from DischargeShipments where DischargeTypeId = 1 and HawbId = TBL.HawbId))
		end  as AvailablePiecesForPickupDocument,
		STUFF((    SELECT @RowSpliter + ISNULL(FileName,'') + @ColumnSpliter + isnull(cast(EntityID as NVARCHAR(10)),'') + @ColumnSpliter 
					  + isnull(cast(EntityTypeId as NVARCHAR(10)),'') + @ColumnSpliter 
					  + isnull(FileType,'') + @ColumnSpliter + isnull(cast(ID as NVARCHAR(10)),'') +@ColumnSpliter +  isnull(Name,'') + @ColumnSpliter 
					  + Isnull(cast(RecDate as NVARCHAR(30)),'') +@ColumnSpliter +  isnull(cast(UserId as NVARCHAR(10)),'') +@ColumnSpliter 
					  + Isnull(Description,'') + @ColumnSpliter +  isnull(cast(DocumentTypeId as NVARCHAR(10)),'')  AS [text()]                       
                        FROM dbo.attachments
                        WHERE EntityTypeId = 11 and EntityID = DsichargeShipmentId                      
                        FOR XML PATH('') 
                        ), 1, 2, '' ) as Attachments,
         (select top 1 ds.date 
		  from dbo.DischargeShipments as ds 
		  where (TBL.EntityType = 2 and ds.AwbId = TBL.EntityId) 
		  OR (TBL.EntityType = 3 and ds.HawbId = TBL.EntityId) order by ds.date ) as PickupDocumentDate, 
		 ISNULL((select top 1 0 
		  from dbo.Tasks as T 
		  where T.EntityId = Tbl.DsichargeShipmentId and T.EntityTypeId = 3--Freight Discharge
			and t.StatusId Not in (11,12)
		 ),1) as AllowEdit,ISNULL(CustomsStatuses.QuantityAffected,0) - IsNull(TBL.DispositionReleasedPieces,0) as AvailablePieces,       		
		 
		 TBL.*, 
		 --Complex Types
		 CustomsStatuses.Id as CustomStatusesId,
		 CustomsStatuses.Master as CustomStatusesMaster, CustomsStatuses.House as CustomsStatusesHouse,
		 CustomsStatuses.PartialIdCode as CustomsStatusesPartialIdCode,CustomsStatuses.StatusID as CustomsStatusesStatusId,
		 CustomsStatuses.CustomsCode as CustomsStatusesCustomsCode,CustomsStatuses.StatusCode as CustomsStatusesStatusCode,
		 CustomsStatuses.Code as CustomsStatusesCode,CustomsStatuses.CodeDescription as CustomsStatusesCodeDescription,
		 CustomsStatuses.QuantityAffected as CustomsStatusesQuantityAffected, CustomsStatuses.EntryNo as CustomsStatusesEntryNo,
		 CustomsStatuses.MessageType as CustomsStatusesMessageType,CustomsStatuses.TransactionDateTime as CustomsStatusesTransactionDateTime,
		 CustomsStatuses.CustomsDateTime as CustomsStatusesCustomsDateTime,

		 CustomsOverrides.Id as CustomsOverridesId, CustomsOverrides.CustomsStatusId as CustomsOverridesCustomsStatusId,
		 CustomsOverrides.Date as CustomsOverridesDate,CustomsOverrides.EntityId as CustomsOverridesEntityId,
		 CustomsOverrides.EntityTypeId as CustomsOverridesEntityTypeId,CustomsOverrides.Comment as CustomsOverridesComment,
         CustomsOverrides.PiecesAffectedCount as CustomsOverridesPiecesAffectedCount,
		 CustomsOverrides.Code as CustomsOverridesCode,CustomsOverrides.Description as CustomsOverridesDescription,
		 case when CustomsOverrides.CustomsStatusId is not null then (SELECT top 1 IsGreenCode from AmsCodes where id = CustomsStatusId)
		 ELSE case when EXISTS (select 1 from AmsCodes where CustomsStatuses.StatusCode = CustomsOverrides.Code) 
                   then (select top 1 IsGreenCode from AmsCodes where CustomsStatuses.StatusCode = CustomsOverrides.Code)
		 else 0 end end as IsGreenCode,

		 ISNULL((select sum(D.Count) 
		  from Dispositions as D  join Tasks as T ON D.TaskId = T.Id
		  join DischargeShipments DS on DS.Id = T.EntityId
		  where T.EntityTypeId = 11 AND 
		  ((DS.AwbId = TBL.AwbId and TBL.EntityType = 2) OR(DS.HawbId = Tbl.HawbId and Tbl.EntityType = 3)))
		  ,0) 
		 + ISNULL((select sum(T.Pieces) from Tasks as T 
			join DischargeShipments DS on T.EntityId = DS.Id
			where T.EntityTypeId = 11
			and ((DS.AwbId = TBL.AwbId and TBL.EntityType = 2) OR(DS.HawbId = Tbl.HawbId and Tbl.EntityType = 3))
			AND T.EndDate is NULL),0) 
        -  ISNULL((select sum(ds.TotalReleasedPieces) 
			from dbo.DischargeShipments ds where ds.DischargeId = TBL.DischargeId and ds.DischargeTypeId <> 1 and
				((TBL.EntityType = 2 AND ds.AwbId = TBL.AwbId) OR (TBL.EntityType = 3 and ds.HawbId = TBL.HawbId) )),0)	
		 as ShipmentDischargedPieces		 
		
		 from (

Select Discharges.*, 
DischargeStatuses.Name as DischargeStatusName,
DischargeWarehouses.Code as DischargeWarehousCode,

DriverSecurityLogs.Id as LogId,
DriverSecurityLogs.RecDate as LogRecDate,
DriverSecurityLogs.CarrierDriverId as LogCarrierDriverId,
CarrierDrivers.Fullname as DriverFullName,
DriverSecurityLogs.PrimaryIDTypeId as LogPrimaryIDTypeId,
DriverSecurityLogs.SecondaryIdTypeId as LogSecondaryIdTypeId,
DriverSecurityLogs.PrimaryPhotoIdMatch as LogPrimaryPhotoIdMatch,
DriverSecurityLogs.SecondaryPhotoIdMatch as LogSecondaryPhotoIdMatch,
DriverSecurityLogs.TruckingCompanyId as LogTruckingCompanyId,
TruckingCompany.CarrierName as TruckingCompanyName,
DriverSecurityLogs.DriverPhoto as LogDriverPhoto,
DriverSecurityLogs.IDPhoto as LogIDPhoto,
DriverSecurityLogs.DriverThumbnail as LogDriverThumbnail,
isnull((select sum(Dispositions.Count) 
            from dbo.Dispositions join dbo.Tasks on Dispositions.TaskId = Tasks.Id
            where (ds.AwbId is not null and AWBId = ds.AwbId) OR (ds.AwbId is null  and HWBId = ds.HawbId)
            and Tasks.TaskTypeId = 3        
        ),0)  as DispositionReleasedPieces,

ds.Id as DsichargeShipmentId,
ds.AwbId, 
ds.HawbId, 
ds.DischargeTypeId,
case when ds.AwbId is not null then a.ReceivedPieces else h.ReceivedPieces end as ReceivedPieces,
case when ds.AwbId is not null then ds.AwbId else ds.HawbId end as EntityId,
case when ds.AwbId is not null then 2 else 3 end as EntityType,
case when ds.AwbId is not null then AwbOrigPort.IATACode else HwbOrigPort.IATACode end as IATACodeOrig,
case when ds.AwbId is not null then AwbDestPort.IATACode else HwbDestPort.IATACode end as IATACodeDest,
case when ds.AwbId is not null then AwbCarrier.Carrier3Code else 
			(select top 1 Carriers.Carrier3Code from dbo.AWBs_HWBs  
			 join dbo.AWBs on AWBs.id = AWBs_HWBs.AWBId JOIN Carriers on Carriers.Id = AWBs.CarrierId
			where AWBs_HWBs.HWBId = ds.HawbId ) end as Carrier3Code,
		case when ds.AwbId is not null then a.AWBSerialNumber else h.HWBSerialNumber end as ShipingRef,
		ds.InbondNumber as InbondNumber,
		ds.TransferManifest as TransferManifest,
		ds.IsCustomsOverride as IsCustomsOverride,
		ds.TotalReleasedPieces as TotalReleasedPieces,		
		ds.DischargeId as DischargeId,
		ds.Id as DischargeShipmentId,
		--Complex types
		DischargeTypes.Id as DischargeTypesId,DischargeTypes.Name as DischargeTypesName, --DischargeShipmentType
		Statuses.Id as StatusesId,Statuses.Name as StatusesName,Statuses.Description as StatusesDescription,Statuses.DisplayName as StatusesDisplayName, --Status
		Ports.Id as PortsId,Ports.IATACode as PortsIATACode,Ports.Port as PortsPort, --Station
		DischargeCarrier.Id as DischargeCarrierId,DischargeCarrier.CarrierCode as DischargeCarrierCarrierCode,
		DischargeCarrier.CarrierName  as DischargeCarrierCarrierName,DischargeCarrier.Carrier3Code as DischargeCarrierCarrier3Code, --Carrier
		/*Consignee*/
		case when ds.AwbId is not null then AwbCustomer.Id else HwbCustomer.id end as ConsigneeId,
		case when ds.AwbId is not null then AwbCustomer.Name else HwbCustomer.Name end as ConsigneeName,
		case when ds.AwbId is not null then AwbCustomer.PostalCode else HwbCustomer.PostalCode end as ConsigneePostalCode,
		-------------

		case when ds.AwbId is not null then AwbAgents.Name else 
					(select top 1 Agents.Name from dbo.AWBs_HWBs
						 join dbo.AWBs on AWBs.id = AWBs_HWBs.Awbid JOIN Agents on Agents.Id = AWBs.AgentId
						where AWBs_HWBs.HWBId = ds.HawbId ) 
		end as CustomsBrokerName,
		case when ds.AwbId is not null then AwbAgents.Place else 
					(select top 1 Agents.Place from dbo.AWBs_HWBs
						 join dbo.AWBs on AWBs.id = AWBs_HWBs.Awbid JOIN Agents on Agents.Id = AWBs.AgentId
						where AWBs_HWBs.HWBId = ds.HawbId ) 
		end as CustomsBrokerPlace

from Discharges
	left join Statuses as DischargeStatuses on DischargeStatuses.Id = Discharges.StatusId 
	left join Warehouses as DischargeWarehouses on DischargeWarehouses.Id = Discharges.WarehouseId
	inner join DriverSecurityLogs on DriverSecurityLogs.Id = Discharges.DriverLogId
	left join CarrierDrivers on CarrierDrivers.Id = DriverSecurityLogs.CarrierDriverId
	left join Carriers as TruckingCompany on TruckingCompany.Id = DriverSecurityLogs.TruckingCompanyId
	inner join DischargeShipments as ds on ds.DischargeId = Discharges.Id
	
	left join dbo.AWBs as a on ds.AwbId = a.Id 
	left join dbo.Ports as AwbOrigPort on a.OriginId = AwbOrigPort.id
	left join dbo.Ports as AwbDestPort on a.DestinationId = AwbDestPort.id
	left join dbo.Carriers as AwbCarrier on a.CarrierId = AwbCarrier.Id
	left join dbo.Customers as AwbCustomer on a.ConsigneeId = AwbCustomer.Id
	left join dbo.Agents as AwbAgents on a.AgentId = AwbAgents.Id

	left JOIN dbo.HWBs as h on ds.HawbId = h.Id 
	left JOIN dbo.Ports as HwbOrigPort on h.OriginId = HwbOrigPort.Id
	left join dbo.Ports as HwbDestPort on h.DestinationId = HwbDestPort.Id
	left join dbo.Customers as HwbCustomer on h.ConsigneeId = HwbCustomer.Id

	left join dbo.DischargeTypes on ds.DischargeTypeId = DischargeTypes.Id
	left join dbo.Statuses on ds.StatusId = Statuses.Id
	left join dbo.Ports on ds.PortId = Ports.Id
	left join dbo.Carriers as DischargeCarrier on DischargeCarrier.Id = ds.CarrierId 
	
Where Discharges.UserId = @UserId and
(@DateFrom is null or Discharges.Date >= @DateFrom) and
(@DateTo is null or Discharges.Date < DATEADD(day,1,@DateTo)) and 
(@StatusId is null or Discharges.StatusId = @StatusId) and
(@DriverId is null or DriverSecurityLogs.CarrierDriverId = @DriverId) and
(@TruckingCompanyId is null or DriverSecurityLogs.TruckingCompanyId = @TruckingCompanyId) and
(@CarrierId is null or ds.CarrierId = @CarrierId) and
(@DischargeTypeId is null or ds.DischargeTypeId = @DischargeTypeId) and
(@HwbSerialNumber is null or h.HWBSerialNumber = @HwbSerialNumber) and
(@AwbSerialNumber is null or a.AwbSerialNumber = @AwbSerialNumber)
) as TBL 
outer apply
(
	SELECT top 1 r.RowNumber as Id,r.Master, r.House, r.PartialIdCode, r.StatusID, r.CustomsCode, r.StatusCode, r.Code, r.CodeDescription,  r.QuantityAffected, r.EntryNo, r.MessageType, r.TransactionDateTime, r.CustomsDateTime
		FROM (SELECT ROW_NUMBER() OVER (partition BY d .PartialIdCode
			ORDER BY s.TransactionDateTime DESC) AS RowNumber, w.Master, d .House, d .PartialIdCode, s.StatusID, s.CustomsCode, s.StatusCode, s.Code, s.CodeDescription, 
            s.QuantityAffected, s.EntryNo, s.MessageType, s.TransactionDateTime, s.CustomsDateTime 
			FROM MCHCoreDB.dam.Statuses AS s INNER JOIN
            MCHCoreDB.dam.Details AS d ON d .DetailID = s.DetailID INNER JOIN
            MCHCoreDB.dam.Waybills AS w ON w.WaybillID = d .WaybillID
			where Tbl.EntityType = 2 and w.Master = TBL.ShipingRef and (d.House is null or d.House = '') and s.MessageType not in ('O','E') and s.CustomsCode != 'FSC'
		) AS r where r.RowNumber = 1

	union

	SELECT top 1 r.Rownumber as Id,r.Master, r.House, r.PartialIdCode, r.StatusID, r.CustomsCode, r.StatusCode, r.Code, r.CodeDescription,  r.QuantityAffected, r.EntryNo, r.MessageType, r.TransactionDateTime, r.CustomsDateTime
	  FROM           
	   (SELECT ROW_NUMBER() OVER (partition BY d .PartialIdCode
                          ORDER BY s.CustomsDateTime DESC) AS RowNumber, w.Master, d .House, d .PartialIdCode, s.StatusID, s.CustomsCode, s.StatusCode, s.Code, s.CodeDescription, 
                         s.QuantityAffected, s.EntryNo, s.MessageType, s.TransactionDateTime, s.CustomsDateTime  
						 FROM MCHCoreDB.dam.Statuses AS s INNER JOIN
                         MCHCoreDB.dam.Details AS d ON d .DetailID = s.DetailID INNER JOIN
                         MCHCoreDB.dam.Waybills AS w ON w.WaybillID = d .WaybillID
						 where Tbl.EntityType = 3 and d.House = TBL.ShipingRef  and s.MessageType not in ('O','E') and s.CustomsCode != 'FSC'
	) AS r  where r.RowNumber = 1	

) as CustomsStatuses
outer APPLY
(select TOP(1) UserCustomsOverrides.*,AmsCodes.Code,AmsCodes.Description 
 from dbo.UserCustomsOverrides join AmsCodes on UserCustomsOverrides.CustomsStatusId = AmsCodes.Id
 where  TBL.EntityType = UserCustomsOverrides.EntityTypeId and 
		Tbl.EntityId = UserCustomsOverrides.EntityId order by Date desc) 
 as CustomsOverrides
";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        #region [-- Private Functions --]

        private DriverSecurityLogModel InitDriverSecurityLogModel(DataRow row)
        {
            return new DriverSecurityLogModel
            {
                Id = row["LogId"].TryParse<long>(),
                RecDate = row["LogRecDate"].TryParse<DateTime>(),
                CarrierDriver = new DriverModel { Id = row["LogCarrierDriverId"].TryParse<long>(), FullName = row["DriverFullName"].ToString() },
                PrimaryIDTypeId = row["LogPrimaryIdTypeId"].TryParse<int?>(),
                SecondaryIdTypeId = row["LogSecondaryIdTypeId"].TryParse<int?>(),
                PrimaryPhotoIdMatch = row["LogPrimaryPhotoIdMatch"].TryParse<bool?>(),
                SecondaryPhotoIdMatch = row["LogSecondaryPhotoIdMatch"].TryParse<bool?>(),
                TruckingCompany = new CarrierModel { Id = row["LogTruckingCompanyId"].TryParse<long>(), CarrierName = row["TruckingCompanyName"].ToString() },
                DriverPhoto = FilePathHelper.DriverImagesRelativePath + row["LogDriverPhoto"].TryParse<string>(),
                IDPhoto = FilePathHelper.DriverImagesRelativePath + row["LogIdPhoto"].TryParse<string>()
            };
        }

        private DischargeShipmentModel InitDischargeShipmentModel(DataRow row)
        {
            return new DischargeShipmentModel
            {
                Carrier3Code = row["Carrier3Code"].ToString(),
                EntityId = row["EntityId"].TryParse<int>(),
                EntityType = (EntityTypeEnum)row["EntityType"],
                IATACodeDest = row["IATACodeDest"].ToString(),
                IATACodeOrig = row["IATACodeOrig"].ToString(),
                Id = row["DsichargeShipmentId"].TryParse<long>(),
                InbondNumber = row["InbondNumber"].ToString(),
                ShipingRef = row["ShipingRef"].ToString(),
                AvailablePieces = row["AvailablePieces"].TryParse<int>(),
                IsCustomsOverride = Convert.ToBoolean(row["IsCustomsOverride"].ToString()),
                TotalReleasedPieces = row["TotalReleasedPieces"].TryParse<int>(),
                TransferManifest = row["TransferManifest"].ToString(),
                PickupDocumentDate = row["PickupDocumentDate"].TryParse<DateTime?>(),
                ShipmentDischargedPieces = row["ShipmentDischargedPieces"].TryParse<long>(),
                //UserCustomsOverride = InitCustomsOverrides(row),
                DischargeId = row["DischargeId"].TryParse<long>(),
                ReceivedPieces = row["ReceivedPieces"].TryParse<int>(),
                AllowEdit = row["ReceivedPieces"].TryParse<bool>(),
                LatestCustomsStatus = InitCustomsLatestStatus(row),
                Attachments = InitAttachments(row),
                Carrier = InitCarrier(row),
                Consignee = InitCustomer(row),
                CustomsBroker = InitAgent(row),
                DischargeShipmentType = InitDischargeType(row),
                Station = InitPort(row),
                Status = InitStatus(row)
            };
        }

        private CustomsHistoryModel InitCustomsLatestStatus(DataRow row)
        {
            if (row["CustomsOverridesId"] != DBNull.Value)

                return new CustomsHistoryModel
                {
                    Id = row["CustomsOverridesId"].TryParse<long>(),
                    Comment = row["CustomsOverridesComment"].ToString(),
                    Date = row["CustomsOverridesDate"].TryParse<DateTime>(),
                    EntityId = row["CustomsOverridesEntityId"].TryParse<long>(),
                    PiecesAffectedCount = row["CustomsOverridesPiecesAffectedCount"].TryParse<long>(),
                    EntityType = (EntityTypeEnum)row["CustomsOverridesEntityTypeId"],
                    CustomsCode = new AmsCodeModel
                    {
                        Id = row["CustomsOverridesCustomsStatusId"].TryParse<long>(),
                        Code = row["CustomsOverridesCode"].ToString(),
                        Description = row["CustomsOverridesDescription"].ToString(),
                        IsGreenCode = row["IsGreenCode"].TryParse<bool>()

                    },
                };
            else
                return new CustomsHistoryModel
                {
                    Id = row["CustomStatusesId"].TryParse<long>(),
                    Master = row["CustomStatusesMaster"].ToString(),
                    House = row["CustomsStatusesHouse"].ToString(),
                    EntryNo = row["CustomsStatusesEntryNo"].ToString(),
                    StatusCode = row["CustomsStatusesStatusCode"].ToString(),
                    PartialIdCode = row["CustomsStatusesPartialIdCode"].ToString(),
                    StatusID = row["CustomsStatusesStatusId"].TryParse<int?>(),
                    MessageType = row["CustomsStatusesMessageType"].ToString(),
                    Date = row["CustomsStatusesCustomsDateTime"].TryParse<DateTime>(),
                    PiecesAffectedCount = row["CustomsStatusesQuantityAffected"].TryParse<long>(),
                    TransactionDateTime = row["CustomsStatusesTransactionDateTime"].TryParse<DateTime?>(),
                    CustomsCode = new AmsCodeModel { Code = row["CustomsStatusesCode"].ToString(), Description = row["CustomsStatusesCodeDescription"].ToString() }
                };
        }
        private IEnumerable<AttachmentModel> InitAttachments(DataRow row)
        {
            return row["Attachments"].ToString()
                .Split(_rowSpliter, StringSplitOptions.RemoveEmptyEntries)
                .Select(a =>
                {
                    var attachmentRow = a.Split(_columnSpliter, StringSplitOptions.None);
                    return new AttachmentModel
                    {
                        FileName = attachmentRow[0].ToString(),
                        EntityID = attachmentRow[1].TryParse<long>(),
                        EntityTypeId = attachmentRow[2].TryParse<int>(),
                        FileType = attachmentRow[3].ToString(),
                        Id = attachmentRow[4].TryParse<long>(),
                        Name = attachmentRow[5].ToString(),
                        RecDate = attachmentRow[6].TryParse<DateTime>(),
                        UserId = attachmentRow[7].TryParse<long>(),
                        Description = attachmentRow[8].ToString(),
                        DocumentTypeId = attachmentRow[9].TryParse<long>(),
                        DbId = attachmentRow[4].TryParse<long>(),
                        Image = FilePathHelper.AttachmentRelativePath + attachmentRow[0].ToString(),
                    };

                });
        }

        private CarrierModel InitCarrier(DataRow row)
        {
            return new CarrierModel
            {
                Carrier3Code = row["DischargeCarrierCarrier3Code"].ToString(),
                CarrierCode = row["DischargeCarrierCarrierCode"].ToString(),
                CarrierName = row["DischargeCarrierCarrierName"].ToString(),
                Id = row["DischargeCarrierId"].TryParse<long>(),
            };
        }

        private CustomerModel InitCustomer(DataRow row)
        {
            return new CustomerModel
            {
                Id = row["ConsigneeId"].TryParse<long>(),
                Name = row["ConsigneeName"].ToString(),
                PostalCode = row["ConsigneePostalCode"].ToString()
            };
        }

        private AgentModel InitAgent(DataRow row)
        {
            return new AgentModel
            {
                Name = row["CustomsBrokerName"].ToString(),
                Place = row["CustomsBrokerPlace"].ToString()
            };
        }

        private DischargeTypeModel InitDischargeType(DataRow row)
        {
            return new DischargeTypeModel
            {
                Id = row["DischargeTypesId"].TryParse<long>(),
                Name = row["DischargeTypesName"].ToString()
            };
        }

        private PortModel InitPort(DataRow row)
        {
            return new PortModel
            {
                Id = row["PortsId"].TryParse<long>(),
                Name = row["PortsPort"].ToString()
            };
        }

        private StatusModel InitStatus(DataRow row)
        {
            return new StatusModel
            {
                Id = row["StatusesId"].TryParse<long>(),
                Name = row["StatusesName"].ToString(),
                Description = row["StatusesDescription"].ToString(),
                DisplayName = row["StatusesDisplayName"].ToString()
            };
        }

        #endregion
    }
}