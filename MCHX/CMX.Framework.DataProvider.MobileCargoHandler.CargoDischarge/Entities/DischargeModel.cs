﻿using System;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class DischargeModel : IdEntity
    {        
        public long DriverLogId { get; set; }
        public long UserId { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartDate { get; set; }
        public StatusModel Status { get; set; }
        public DateTime? StatusTimestamp { get; set; }
        public WarehouseModel Warehouse { get; set; }  
       
    }
}
