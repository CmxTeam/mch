﻿using System;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class DischargeShipmentModel : IdEntity
    {
        public long EntityId { get; set; }
        public EntityTypeEnum EntityType { get; set; }
        public string IATACodeOrig { get; set; }
        public string IATACodeDest { get; set; }
        public string Carrier3Code { get; set; }
        public string ShipingRef { get; set; }        
        public string InbondNumber { get; set; }     
        public string TransferManifest { get; set; }
        public bool IsCustomsOverride { get; set; }
        public bool AllowEdit { get; set; }
        public long TotalReleasedPieces { get; set; }
        public long ReceivedPieces { get; set; }
        public long ShipmentDischargedPieces { get; set; }
        public long AvailablePieces { get; set; } 
        public long DischargeId { get; set; }
        public DateTime? PickupDocumentDate { get; set; }
        public StatusModel Status { get; set; }
        public PortModel Station { get; set; }
        public CarrierModel Carrier { get; set; }
        public CustomerModel Consignee { get; set; }
        public AgentModel CustomsBroker { get; set; }
        public DateTime LastCustomsStatusTimestamp { get; set; }
        public DischargeTypeModel DischargeShipmentType { get; set; }
        public CustomsHistoryModel LatestCustomsStatus { get; set; }
        public IEnumerable<AttachmentModel> Attachments {get; set; }
    }
}
