﻿using System;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class DriverSecurityLogModel : IdEntity
    {
        public long? UserId { get; set; } 
        public string LicenseNumber { get; set; }
        public string DriverPhoto { get; set; }
        public string IDPhoto { get; set; }
        public int? PrimaryIDTypeId { get; set; }
        public int? SecondaryIdTypeId { get; set; }
        public bool? PrimaryPhotoIdMatch { get; set; }
        public bool? SecondaryPhotoIdMatch { get; set; }        
        public string DriverThumbnail { get; set; }        
        public DateTime RecDate { get; set; }
        public DriverModel CarrierDriver { get; set; }
        public CarrierModel TruckingCompany { get; set; }        
    }
}
