﻿namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public enum DischargeStatusesEnum
    {
        DischargeRequestStarted = 50,
        DischargeRequestCompleted,
        FreightDischargeStarted,
        FreightDischargeCompleted,
        DischargeDocsPickedUp,
        DischargeRequestEntered,
        FreightDeliveryStarted = 59,
        FreightDelivered
    }
}