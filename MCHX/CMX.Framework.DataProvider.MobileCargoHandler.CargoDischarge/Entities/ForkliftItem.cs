﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class ForkliftItem : IdEntity
    {
        public int Pieces { get; set; }
        public long ShipmentId { get; set; }
        public string ShipmentNumber { get; set; }
        public EntityTypeEnum ShipmentType { get; set; }
    }
}