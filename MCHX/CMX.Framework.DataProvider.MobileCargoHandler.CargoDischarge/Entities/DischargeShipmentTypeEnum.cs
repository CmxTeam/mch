﻿namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public enum DischargeShipmentTypeEnum
    {
        PickupDocuments = 1,
        FreightPickup,
        TransfertoCarrier,
        DomesticTransfer,
    }
}