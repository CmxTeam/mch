﻿namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities.SearchModels
{
    public class ForkliftToLocationParamModel
    {
        public string TaskIds{get; set;} 
        public long LocationId{get; set;}
        public int? Count { get; set; }
    }
}