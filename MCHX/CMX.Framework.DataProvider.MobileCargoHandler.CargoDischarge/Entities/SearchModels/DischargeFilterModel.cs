﻿using System;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities.SearchModels
{
    public class DischargeFilterModel
    {
        public string AwbSerialNumber{ get; set; }
        public string HwbSerialNumber { get; set; }      
        public int? StatusId { get; set; }
        public long? CarrierId { get; set; }              
        public int? TruckingCompanyId { get; set; }
        public long? DriverId { get; set; }
        public long? DischargeTypeId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }  
       
    }
}