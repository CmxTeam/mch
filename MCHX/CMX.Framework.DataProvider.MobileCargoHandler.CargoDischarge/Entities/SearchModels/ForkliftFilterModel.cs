﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities.SearchModels
{
    public class ForkliftFilterModel
    {
        public long TaskId{get; set;}                
        public int? Count { get; set; }
        public long ShipmentId { get; set; }
        public EntityTypeEnum ShipmentType { get; set; } 
    }
}