﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class ShipperVerificationDocModel : IdNameEntity
    {
        ShipperTypeModel ShipperType { get; set; }
    }
}
