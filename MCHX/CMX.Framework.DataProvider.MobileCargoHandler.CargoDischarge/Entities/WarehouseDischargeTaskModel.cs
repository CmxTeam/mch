﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class WarehouseDischargeTaskModel : IdEntity
    {
        public int Piecses { get; set; }
        public int ScannedCount { get; set; }
        public string Locations { get; set; }        
        public long ShipmentId { get; set; }
        public string ShipmentNumber { get; set; }
        public EntityTypeEnum ShipmentType { get; set; }
    }
}