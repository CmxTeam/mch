﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class DischargeExtendedModel : DischargeModel
    {        
        public DriverSecurityLogModel DriverLog { get; set; }
        public IEnumerable<DischargeShipmentModel> DischargeShipments { get; set; }
    }
}