﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class WarehouseDischargeModel : IdEntity
    {
        public string DriverName { get; set; }
        public string DriverPhoto { get; set; }
        public string CarrierCode { get; set; }
        public int ShipmentsCount { get; set; }
        public int PiecesCount { get; set; }
        public int ScannedCount { get; set; }
    }
}