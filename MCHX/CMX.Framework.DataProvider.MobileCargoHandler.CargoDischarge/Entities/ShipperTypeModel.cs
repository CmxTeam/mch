﻿using System;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities
{
    public class ShipperTypeModel : IdNameEntity
    {
        public string Code { get; set; }
        public DateTime RecDate { get; set; }        
    }
}
