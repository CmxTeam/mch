﻿using System;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CargoDischarge.Helpers
{
    public static class Extensions
    {
        public static DischargeExtendedModel ConvertTo(this Discharge discharge)
        {
            if (discharge == null) return new DischargeExtendedModel();
            else
                return new DischargeExtendedModel
                {
                    Date = discharge.Date,
                    DriverLogId = discharge.DriverLogId,
                    Id = discharge.Id,
                    Status = discharge.Status.ConvertTo(),
                    StatusTimestamp = discharge.StatusTimestamp,
                    UserId = discharge.UserId,
                    Warehouse = discharge.Warehouse.ConvertTo()
                };
        }

        public static DriverSecurityLog ConvertTo(this DriverSecurityLogModel driverLogs)
        {
            if (driverLogs == null) return new DriverSecurityLog();
            return new DriverSecurityLog
            {
                CarrierDriverId = driverLogs.CarrierDriver.Id,
                DriverPhoto = driverLogs.DriverPhoto,
                DriverThumbnail = driverLogs.DriverThumbnail,
                Id = driverLogs.Id,
                IDPhoto = driverLogs.IDPhoto,
                PrimaryIDTypeId = driverLogs.PrimaryIDTypeId,
                PrimaryPhotoIdMatch = driverLogs.PrimaryPhotoIdMatch,
                RecDate = DateTime.UtcNow,
                SecondaryIdTypeId = driverLogs.SecondaryIdTypeId,
                SecondaryPhotoIdMatch = driverLogs.SecondaryPhotoIdMatch,
                TruckingCompanyId = (int?)driverLogs.TruckingCompany.Id,
                UserId = driverLogs.UserId
            };
        }

        public static DriverSecurityLogModel ConvertTo(this DriverSecurityLog driverLogs)
        {
            if (driverLogs == null) return new DriverSecurityLogModel();
            return new DriverSecurityLogModel
            {
                CarrierDriver = driverLogs.CarrierDriver.ConvertTo(),
                DriverPhoto = FilePathHelper.DriverImagesRelativePath + driverLogs.DriverPhoto,
                DriverThumbnail = driverLogs.DriverThumbnail,
                Id = driverLogs.Id,
                IDPhoto = FilePathHelper.DriverImagesRelativePath + driverLogs.IDPhoto,
                PrimaryIDTypeId = driverLogs.PrimaryIDTypeId,
                PrimaryPhotoIdMatch = driverLogs.PrimaryPhotoIdMatch,
                RecDate = (driverLogs.RecDate != DateTime.MinValue) ? driverLogs.RecDate : DateTime.UtcNow,
                SecondaryIdTypeId = driverLogs.SecondaryIdTypeId,
                SecondaryPhotoIdMatch = driverLogs.SecondaryPhotoIdMatch,
                TruckingCompany = driverLogs.Carrier.ConvertTo(),
                UserId = driverLogs.UserId
            };
        }

        public static DischargeShipment ConvertTo(this DischargeShipmentModel dischargeShipment)
        {
            if (dischargeShipment == null) return new DischargeShipment();
            return new DischargeShipment
            {
                AwbId = dischargeShipment.EntityType == EntityTypeEnum.AWB ? dischargeShipment.EntityId : (long?)null,
                HawbId = dischargeShipment.EntityType == EntityTypeEnum.HWB ? dischargeShipment.EntityId : (long?)null,
                Id = dischargeShipment.Id,
                Date = DateTime.UtcNow,
                InbondNumber = dischargeShipment.InbondNumber,
                IsCustomsOverride = dischargeShipment.IsCustomsOverride,
                CarrierId = dischargeShipment.Carrier.Id != 0 ? (int?)dischargeShipment.Carrier.Id : null,
                DischargeId = dischargeShipment.DischargeId,
                DischargeTypeId = dischargeShipment.DischargeShipmentType.Id,
                PortId = dischargeShipment.Station.Id != 0 ? (long?)dischargeShipment.Station.Id : null,
                TotalReleasedPieces = (int)dischargeShipment.TotalReleasedPieces,
                TransferManifest = dischargeShipment.TransferManifest,
                StatusId = (int)(((DischargeShipmentTypeEnum)dischargeShipment.DischargeShipmentType.Id) == DischargeShipmentTypeEnum.FreightPickup ? DischargeStatusesEnum.DischargeDocsPickedUp : DischargeStatusesEnum.DischargeRequestEntered)
            };
        }
        public static DischargeShipmentModel ConvertTo(this DischargeShipment dischargeShipment)
        {
            if (dischargeShipment == null) return new DischargeShipmentModel();
            return new DischargeShipmentModel
            {
                EntityId = (long)(dischargeShipment.AwbId != null ? dischargeShipment.AwbId : dischargeShipment.HawbId),
                EntityType = dischargeShipment.AwbId != null ? EntityTypeEnum.AWB : EntityTypeEnum.HWB,
                Id = dischargeShipment.Id,
                InbondNumber = dischargeShipment.InbondNumber,
                IsCustomsOverride = dischargeShipment.IsCustomsOverride,              
                DischargeId = dischargeShipment.DischargeId,
                Carrier = dischargeShipment.Carrier.ConvertTo(),
                DischargeShipmentType = dischargeShipment.DischargeType.ConvertTo(),
                LastCustomsStatusTimestamp = DateTime.UtcNow, ///TODO:;
                Station = dischargeShipment.Port.ConvertTo(),
                TotalReleasedPieces = dischargeShipment.TotalReleasedPieces,
                TransferManifest = dischargeShipment.TransferManifest
            };
        }

        public static Discharge ConvertTo(this DischargeModel discharge)
        {
            if (discharge == null) return new Discharge();
            return new Discharge
            {
                Date = DateTime.UtcNow,
                DriverLogId = discharge.DriverLogId,
                Id = discharge.Id,
                StatusId = (int)DischargeStatusesEnum.DischargeRequestCompleted,
                StatusTimestamp = discharge.Id == 0 ? DateTime.UtcNow : discharge.StatusTimestamp,
                UserId = discharge.UserId,
                WarehouseId = (int)discharge.Warehouse.Id
            };
        }
        public static CustomsHistoryModel ConverTo(this UserCustomsOverride userOverrides)
        {
            if (userOverrides == null)
                return new CustomsHistoryModel();
            return new CustomsHistoryModel
            {
                Id = userOverrides.Id,
                Comment = userOverrides.Comment,
                Date = userOverrides.Date,
                PiecesAffectedCount = userOverrides.PiecesAffectedCount,
                CustomsCode = userOverrides.AmsCode.ConverTo(),
                EntityId = userOverrides.EntityId,
                EntityType = (EntityTypeEnum)userOverrides.EntityTypeId               
            };
        }
        public static UserCustomsOverride ConverTo(this CustomsHistoryModel userOverrides)
        {
            if (userOverrides == null)
                return new UserCustomsOverride();
            return new UserCustomsOverride
            {               
                Comment = userOverrides.Comment,
                Date = DateTime.UtcNow,
                PiecesAffectedCount = userOverrides.PiecesAffectedCount,
                CustomsStatusId = userOverrides.CustomsCode.Id,
                EntityId = userOverrides.EntityId,
                EntityTypeId = (int)userOverrides.EntityType
            };
        }
    }
}
