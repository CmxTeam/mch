﻿using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.DataProviders.AdoUnits;
using System.Collections.Generic;
using System.Linq;

namespace CMX.Framework.DataProvider.Hubs.Entities
{
    public class Users
    {
        private readonly ConnectorExecutor _executor = new ConnectorExecutor();
        
        public IEnumerable<User> AvailableUsers
        {
            get { return _executor.Execute(() => new GetRapiscanUsers()).ToList(); }
        }

        public void Add(string connectionId)
        {
            _executor.Execute(() => new AddRapiscanUser(connectionId));
        }

        public void Remove(string connectionId)
        {
            _executor.Execute(() => new RemoveRapiscanUser(connectionId));
        }

        public void SetDevice(string connectionId, string deviceId)
        {
            _executor.Execute(() => new AssignDeviceToRapiscanUser(connectionId, deviceId));
        }

        public void RemoveDevice(string deviceId)
        {
            _executor.Execute(() => new RemoveDeviceFromRapiscanUser(deviceId));
        }
    }
}