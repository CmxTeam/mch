﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.Hubs.Entities
{
    public class User
    {
        public long Id { get; set; }
        public string ConnectionId { get; set; }
        public string DeviceId { get; set; }
    }
}
