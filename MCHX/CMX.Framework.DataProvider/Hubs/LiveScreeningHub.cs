﻿using CMX.Framework.DataProvider.Hubs.Entities;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.Hubs
{
    public class LiveScreeningHub : Hub
    {
        private Users _users = new Users();
       
        public void AddUser()
        {            
        }

        public void SetDevice(string deviceId)
        {
            _users.SetDevice(Context.ConnectionId, deviceId);
        }

        public void RemoveDevice(string deviceId)
        {
            _users.RemoveDevice(deviceId);
        }

        public override Task OnConnected()
        {
            _users.Add(Context.ConnectionId);            
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _users.Remove(Context.ConnectionId);            
            return base.OnDisconnected(stopCalled);
        }
    }
}
