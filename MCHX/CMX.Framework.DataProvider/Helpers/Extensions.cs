﻿using System;
using System.Linq;
using System.Collections.Generic;
using DhtmlxComponents.Widgets;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.Helpers
{
    public static class Extensions
    {
        public static T TryParse<T>(this object obj)
        {
            try
            {
                if(obj.GetType() == typeof(DBNull))
                    return default(T);

                if (typeof(T).IsValueType && Nullable.GetUnderlyingType(typeof(T)) != null)
                    return (T)Convert.ChangeType(obj, Nullable.GetUnderlyingType(typeof(T)));

                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch
            {
                return default(T);
            }
        }

        public static IEnumerable<MenuItem> ConvertToDhtmlxMenu(this IEnumerable<ApplicationMenu> menus)
        {
            foreach (var menu in menus)
            {
                var menuItem = new MenuItem
                {
                    id = menu.Id.ToString(),
                    text = menu.Name,
                    type = "item",
                    href_link = menu.Path,
                };

                yield return menuItem;

                if (menu.SubMenus != null && menu.SubMenus.Count() > 0)
                    menuItem.items = ConvertToDhtmlxMenu(menu.SubMenus).ToList();
            }
        }

        public static bool IsNullOrEmpty(this string item)
        { 
            return string.IsNullOrEmpty(item);
        }
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null || action == null) return;
            foreach (T element in source)            
                action(element);            
        }
    }
}