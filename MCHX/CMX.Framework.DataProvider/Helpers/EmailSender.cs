﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.Helpers
{
    public static class EmailSender
    {
        private static int _mailerPort = 587;
        private static string _mailerHost = "smtp.gmail.com";
        private static string _fromPass = "AlphaRomeo880";
        private static string _fromAddr = "testromeo00@gmail.com";
        public static void Send(EmailDetailsModel email)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.IsBodyHtml = email.IsBodyHtml;
            mailMessage.Subject = email.Subject;            
            mailMessage.Body = email.Body;
            mailMessage.From = new MailAddress(_fromAddr);
            if (email.Recipients != null && email.Recipients.Any())
                email.Recipients.ForEach(r => mailMessage.To.Add(new MailAddress(r)));
            if (email.NotifTo != null && email.NotifTo.Any())
                email.NotifTo.ForEach(n => mailMessage.CC.Add(new MailAddress(n)));
            if (email.Attachments != null && email.Attachments.Any())
                email.Attachments.ForEach(a => mailMessage.Attachments.Add(new Attachment(a)));

            SmtpClient client = new SmtpClient();
            client.Port = _mailerPort;
            client.Host = _mailerHost;
            client.Credentials = new NetworkCredential(_fromAddr, _fromPass);            
            client.EnableSsl = true;
            client.Send(mailMessage);            
        }
    }
}
