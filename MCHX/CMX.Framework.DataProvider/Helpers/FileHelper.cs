﻿using System;
using System.IO;

namespace CMX.Framework.DataProvider.Helpers
{
    public static class FileHelper
    {
        public static string SaveImage(string base64, string path, string imageName)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            base64 = base64.Contains(";base64,")
                ? base64.Split(new[] { ";base64," }, StringSplitOptions.None)[1]
                : base64;

            byte[] bytes = Convert.FromBase64String(base64);
            var name = imageName.Replace(" ", "") + "_" + Guid.NewGuid().ToString().Replace("-", "") + ".jpg";
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                using (System.Drawing.Bitmap bm2 = new System.Drawing.Bitmap(ms))
                {
                    bm2.Save((path.EndsWith(@"\") ? path : (path + @"\")) + name);
                }
            }

            return name;
        }
        public static string SaveFile(string base64, string path, string fileName)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            string asciiBody;
            if (!base64.Contains(";base64,"))
                asciiBody = base64;
            else asciiBody = base64.Split(new[] { ";base64," }, StringSplitOptions.None)[1];
            byte[] bytes = Convert.FromBase64String(asciiBody);
            var fileParts = fileName.Split('.');
            var majorFileName = fileParts[fileParts.Length - 2 < 0 ? 0 : fileParts.Length - 2];
            var extension = fileParts[fileParts.Length - 1];
            var name = majorFileName.Replace(" ", "") + "_" + Guid.NewGuid().ToString().Replace("-", "") + "." + extension;
            var fileFullPath = (path.EndsWith(@"\") ? path : (path + @"\")) + name;
            File.WriteAllBytes(fileFullPath, bytes);
            return name;
        }
        public static void DeleteFile(string fileName,bool raise = false)
        {
            try
            {
                File.Delete(fileName);
            }
            catch (Exception)
            {
                if (raise)
                    throw;
            }
        }
    }
}
