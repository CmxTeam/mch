﻿using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Entities.Authentication;

namespace CMX.Framework.DataProvider.Controllers
{
    public abstract class UserIdentityController : ApiController
    {
        public ApplicationUser ApplicationUser
        {
            get
            {
                var claims = ((ClaimsIdentity)User.Identity).Claims.ToList();

                return new ApplicationUser
                {
                    UserId = long.Parse(claims[1].Value),
                    UserName = claims[0].Value,
                    DefaultWarehouseId = int.Parse(claims[2].Value),
                    Applications = JsonConvert.DeserializeObject<List<ApplicationMenu>>(claims[3].Value),
                    UserLocations = JsonConvert.DeserializeObject<List<IdNameEntity>>(claims[4].Value),
                    Roles = JsonConvert.DeserializeObject<List<Role>>(claims[5].Value),
                };
            }
        }

        public abstract bool VerifyAuthentikationToken();
    }
}