﻿using System;
using System.Web.Http.Cors;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public abstract class TransactionalController : UserIdentityController
    {
        protected DataContainer<T> MakeTransactional<T>(Func<T> action)
        {
            try
            {
                var data = action();
                return new DataContainer<T> { Data = data, Status = new TransactionStatus { Message = string.Empty, Status = true }, Transaction = new TransactionStatus { Message = string.Empty, Error = string.Empty, Status = true } };
            }
            catch (Exception ex)
            {
                return new DataContainer<T> { Status = new TransactionStatus { Message = ex.Message, Status = false }, Transaction = new TransactionStatus { Message = ex.Message, Error = ex.Message, Status = false } };
            }
        }
    }
}
