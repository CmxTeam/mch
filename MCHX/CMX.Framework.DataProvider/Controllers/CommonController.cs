﻿using System.Linq;
using System.Web.Http;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using DhtmlxComponents.Widgets;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.Controllers
{
    public class CommonController : TransactionalController
    {
        [HttpGet]
        public IEnumerable<IdNameEntity> GetPeriods()
        {
            return new List<IdNameEntity> 
            { 
                new IdNameEntity { Id = 1, Name = "Today" },
                new IdNameEntity { Id = 2, Name = "Yesterday" },
                new IdNameEntity { Id = 3, Name = "Last 3 Days" },
                new IdNameEntity { Id = 7, Name = "Last Week" }, 
                new IdNameEntity { Id = 30, Name = "Last Month" },
                new IdNameEntity { Id = 90, Name = "Last Quarter" } 
            };
        }

        [HttpGet]
        public IEnumerable<IdNameEntity> GetYesNo()
        {
            return new List<IdNameEntity> 
            { 
                new IdNameEntity { Id = 1, Name = "YES" },
                new IdNameEntity { Id = 0, Name = "NO" },                
            };
        }

        [Authorize]
        [HttpGet]
        public IEnumerable<MenuItem> GetUserMenus()
        {
            var menus = ApplicationUser.Applications.ConvertToDhtmlxMenu().ToList();            
            menus.Add(new MenuItem
            {
                id = "logout",
                text = "LOG OUT",
                type = "item",
            });

            return menus;
        }

        [HttpGet]
        [Authorize]
        public override bool VerifyAuthentikationToken()
        {
            return true;
        }

        [HttpGet]
        [Authorize]        
        public IEnumerable<Role> GetRoles(long applicationId)
        {
            return ApplicationUser.Roles.Where(r => r.ApplicationId == applicationId).ToList();
        }
    }
}