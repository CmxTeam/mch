﻿using Owin;
using System;

using log4net;
using Microsoft.Owin;
using System.Web.Http;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Web.Http.Dispatcher;
using Microsoft.Owin.Security.OAuth;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Entities.Authentication;
using CMX.Framework.DataProvider.DataProviders.Common.Units.AdoUnits;
using Owin.Security.AesDataProtectorProvider;
using Swashbuckle.Application;
using System.Configuration;

namespace CMX.Framework.DataProvider
{
    public class WebApiStartup
    {
        /// <summary>
        /// Swagger Static configruation to be used
        /// </summary>
        public static Action<SwaggerDocsConfig> SwaggerConfig { get; set; }

        public void Configuration(IAppBuilder app)
        {
            log4net.Config.XmlConfigurator.Configure();
            var config = new HttpConfiguration();
            ConfigureOAuth(app);
            config.Services.Replace(typeof(IAssembliesResolver), new DynamicAssemblyResolver(System.AppDomain.CurrentDomain.BaseDirectory));
            config.EnableCors();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            //Treat  DateTime format as Universal and serialize as w3 standard in format YYYY-MM-DDThh:mm:ss.sssZ
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

            if (SwaggerConfig != null)
            {
                if (ConfigurationManager.AppSettings["EnableSwagger"] != null && Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSwagger"]))
                {
                    if (ConfigurationManager.AppSettings["EnableSwaggerUI"] != null && Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSwaggerUI"]))
                    {
                        config
                            .EnableSwagger(SwaggerConfig)
                            .EnableSwaggerUi();
                    }
                    else
                    {
                        config
                            .EnableSwagger(SwaggerConfig);
                    }
                }
            }            

            config.Routes.MapHttpRoute("api", "{controller}/{action}/{id}", new { id = RouteParameter.Optional });

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            // Token Generation
            if (ConfigurationManager.AppSettings["AesKeyWebApiToken"] != null)          
                app.UseAesDataProtectorProvider(ConfigurationManager.AppSettings["AesKeyWebApiToken"]);            
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }
    }

    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WebApiStartup));

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }



        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

                var connectorExecutor = new ConnectorExecutor();
                var data = await context.Request.ReadFormAsync();

                IUserInfo userInfo = null;

                if (data != null && data.Get("useShell") != null && bool.Parse(data.Get("useShell")))
                {
                    //Shell endpoint knows how to read the Owin Context
                    var tmpUserInfo = Shell.Security.ShellAuthEndpoint.AuthenticateUser(context.UserName, context.Password);

                    if (tmpUserInfo != null)
                    {
                        //TODO: Aram, this part should be removed because CMX.Framework.DataProvider.Entities.Common.UserInfo class and CMX.Shell.Security.UserInfo don't match
                        userInfo = connectorExecutor.Execute<UserInfo>(() => new ShellUserDataProvider(tmpUserInfo.UserId));
                        //==================
                    }
                }
                else
                    userInfo = connectorExecutor.Execute<AddressbookAccountModel>(() => new AuthenticationAgainstAddressbookCreator(context.UserName, context.Password));

                if (userInfo == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("UserName", context.UserName));
                identity.AddClaim(new Claim("UserId", userInfo.Id.ToString()));
                identity.AddClaim(new Claim("DefaultWarehouseId", userInfo.DefaultWarehouseId.ToString()));
                identity.AddClaim(new Claim("Applications", JsonConvert.SerializeObject(userInfo.Applications, Formatting.Indented)));
                identity.AddClaim(new Claim("UserLocations", JsonConvert.SerializeObject(userInfo.UserLocations, Formatting.Indented)));
                identity.AddClaim(new Claim("Roles", JsonConvert.SerializeObject(userInfo.Roles, Formatting.Indented)));
                context.Validated(identity);
            }
            catch (Exception ex)
            {
                log.Error("Login", ex);
                throw;
            }

        }
    }
}
