﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.DataProviders.AdoUnits
{
    public class RemoveRapiscanUser : FfDatabaseConnector<bool>
    {
        public RemoveRapiscanUser(string connectionId)
            : base(new CommandParameter("@ConnectionId", connectionId))
        { }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get { return @"Delete From DeviceConnections Where ConnectionId = @ConnectionId"; }
        }
    }
}
