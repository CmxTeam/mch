﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders.AdoUnits
{
    public class AddRapiscanUser : FfDatabaseConnector<bool>
    {
        public AddRapiscanUser(string connectionId)
            : base(new CommandParameter("@ConnectionId", connectionId))
        { }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"If not exists (Select top 1 1 from DeviceConnections where ConnectionId = @ConnectionId)
                        Begin
	                        Insert Into DeviceConnections (ConnectionId, DeviceId, Date) Values (@ConnectionId, null, getutcdate())
                        End";
            }
        }        
    }
}