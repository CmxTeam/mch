﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders.AdoUnits
{
    public class RemoveDeviceFromRapiscanUser : FfDatabaseConnector<bool>
    {
        public RemoveDeviceFromRapiscanUser(string deviceId)
            : base(new CommandParameter("@DeviceId", deviceId))
        { }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Update DeviceConnections Set DeviceId = null Where DeviceId = @DeviceId";
            }
        }
    }
}