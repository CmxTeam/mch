﻿using CMX.Framework.DataProvider.Hubs.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.DataProviders.AdoUnits
{
    public class GetRapiscanUsers : FfDatabaseConnector<IEnumerable<User>>
    {
        public override IEnumerable<User> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Cast<DataRow>().Select(r => new User
            {
                Id = r["Id"].TryParse<long>(),
                ConnectionId = r["ConnectionId"].TryParse<string>(),
                DeviceId = r["DeviceId"].TryParse<string>()
            });
        }

        public override string CommandQuery
        {
            get { return @"Select * from DeviceConnections"; }
        }
    }
}
