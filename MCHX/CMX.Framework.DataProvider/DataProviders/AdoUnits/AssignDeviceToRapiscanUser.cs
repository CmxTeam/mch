﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders.AdoUnits
{
    public class AssignDeviceToRapiscanUser : FfDatabaseConnector<bool>
    {
        public AssignDeviceToRapiscanUser(string connectionId, string deviceId)
            : base(new CommandParameter("@ConnectionId", connectionId), new CommandParameter("@DeviceId", deviceId))
        { }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Update DeviceConnections Set DeviceId = null Where DeviceId = @DeviceId
                         Update DeviceConnections Set DeviceId = @DeviceId Where ConnectionId = @ConnectionId";
            }
        }
    }
}