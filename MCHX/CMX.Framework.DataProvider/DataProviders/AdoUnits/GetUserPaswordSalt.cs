﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders.Common.Units.AdoUnits
{
    public class GetUserPaswordSalt : ShellDatabaseConnector<string>
    {
        public GetUserPaswordSalt(string userName) :
            base(new CommandParameter("@UserName", userName))
        { 
        }

        public override string ObjectInitializer(List<System.Data.DataTable> tables)
        {
            if (tables[0].Rows.Count == 0) return string.Empty;
            return tables[0].Rows[0][0].ToString();
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select top 1 PasswordSalt from UserMemeberships Inner Join Users on Users.Id = UserId Where LoweredUserName = @UserName";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
