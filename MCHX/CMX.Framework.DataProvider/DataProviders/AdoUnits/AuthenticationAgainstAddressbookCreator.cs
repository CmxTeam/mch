﻿using System;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Entities.Authentication;

namespace CMX.Framework.DataProvider.DataProviders.Common.Units.AdoUnits
{
    public class AuthenticationAgainstAddressbookCreator : AdoNetMultyTableDatabaseConnector<AddressbookAccountModel>
    {
        private string _commandQuery;

        public AuthenticationAgainstAddressbookCreator(string userName, string password)
            : base(userName == "-1" ? null : new CommandParameter("@UserID", userName), new CommandParameter(userName == "-1" ? "@UserPin" : "@UserPassword", password))
        {
            long pin;
            if (userName == "-1")
            {
                long.TryParse(password, out pin);
                _commandQuery = "spLoginUserByPin";
            }
            else
            {
                _commandQuery = "spLoginUser";
            }
        }

        public override string CommandQuery
        {
            get { return _commandQuery; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override AddressbookAccountModel ObjectInitializer(List<DataTable> tables)
        {
            var userInfoTable = tables[0];
            var userInfoRow = userInfoTable.Rows[0];
            var data = new AddressbookAccountModel
            {
                Id = Convert.ToInt64(userInfoRow["AddressBookId"]),
                FirstName = userInfoRow["FirstName"].ToString(),
                LastName = userInfoRow["LastName"].ToString(),
                MiddleName = userInfoRow["LastName"].ToString(),
                UserID = userInfoRow["UserID"].ToString(),
                UserType = userInfoRow["UserType"].ToString(),
                Login = userInfoRow["Login"].ToString(),
                DefaultWarehouseId = 0
            };
            if (data.Login == "0") return null;

            data.Applications = InitApplications(tables[2], tables[3]);
            data.UserLocations = new List<IdNameEntity>(); //InitGateways(tables[4], userInfoRow["Gateway"].ToString());
            data.Roles = InitRoles(tables[5]);
            return data;
        }

        private static IEnumerable<ApplicationMenu> InitApplications(DataTable applicationsTable, DataTable menusTable)
        {
            return applicationsTable.Rows.OfType<DataRow>().Select(r =>
            {
                var applicationId = r["Id"].TryParse<long>();
                var appRootPath = r["RootScreenURL"].ToString();

                var menus = menusTable.Rows.OfType<DataRow>()
                    .Where(mg => mg["ApplicationId"].TryParse<long>() == applicationId)
                    .Select(mg => new ApplicationMenu
                    {
                        Id = mg["Id"].TryParse<long>(),
                        ImagePath = mg["MenuImage"].ToString(),
                        Name = mg["Name"].ToString(),
                        Path = appRootPath + mg["MenuPath"].ToString(),
                        GroupName = mg["GroupName"].ToString(),
                    }).ToList();

                var menuGroups = menus.Where(m => !m.GroupName.IsNullOrEmpty()).GroupBy(m => m.GroupName).Select(g => new ApplicationMenu
                {
                    Id = g.First().Id * -1,
                    Name = g.Key,
                    ImagePath = g.First().ImagePath,
                    SubMenus = g
                }).ToList();

                return new ApplicationMenu
                {
                    Id = applicationId,
                    Name = r["FriendlyName"].ToString(),
                    Path = menuGroups.Count == 0 ? (menus.Any() ? menus.First().Path : null) : null,
                    SubMenus = menuGroups.Count == 0 && menus.Count > 1 ? menus : menuGroups
                };
            }).ToList();
        }

        private static IEnumerable<IdNameEntity> InitGateways(DataTable gatewaysTable, string defaultGatewayName)
        {
            var gateways = gatewaysTable.Rows.OfType<DataRow>().Select(r => new IdNameEntity
            {
                Id = r["RecId"].TryParse<long>(),
                Name = r["HostPlusLocationName"].TryParse<string>(),
            }).ToList();

            var defaultGateway = gateways.SingleOrDefault(g => g.Name == defaultGatewayName);
            if (defaultGateway == null) return gateways;

            var defaulGatewayIndex = gateways.IndexOf(defaultGateway);
            gateways.RemoveAt(defaulGatewayIndex);
            gateways.Insert(0, defaultGateway);

            return gateways;
        }

        private static IEnumerable<Role> InitRoles(DataTable rolesTable)
        {
            return rolesTable.Rows.OfType<DataRow>().Select(r => new Role
                 {
                     Id = r["RoleId"].TryParse<long>(),
                     Name = r["RoleName"].TryParse<string>(),
                     ApplicationId = r["ApplicationId"].TryParse<long>()
                 }).ToList();
        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }
    }
}