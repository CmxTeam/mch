﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Cryptography;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.Entities.Common;
using System.Data;

namespace CMX.Framework.DataProvider.DataProviders.Common.Units.AdoUnits
{
    public class AuthenticationAgainstShell : ShellDatabaseConnector<UserInfo>
    {
        public AuthenticationAgainstShell(string userName, string password, string passwordSalt)
            : base(new CommandParameter("@UserName", userName))
        {
            base.Parameters.Add(new CommandParameter("@Password", GetPasswordHash(password, passwordSalt)));            
        }

        public override UserInfo ObjectInitializer(List<DataTable> tables)
        {            
            if (tables[0].Rows.Count == 0)
                return null;
            var userRow = tables[0].Rows[0];
            
            var menuTableRows = tables[1].Rows.OfType<DataRow>();           

            var userInfo = new UserInfo
            {
                Id = userRow["ShellUserId"].TryParse<long>(),
                CompanyName = userRow["CompanyName"].ToString(),
                EmailAddress = userRow["EmailAddress"].ToString(),
                ThemeName = userRow["ThemeName"].ToString(),
                Name = userRow["Username"].ToString(),
                Firstname = userRow["FirstName"].ToString(),
                Lastname = userRow["LastName"].ToString(),
                DefaultWarehouseId = userRow["DefaultWarehouseId"].TryParse<long?>(),
                Applications = ParseAndCreateMenu(null, menuTableRows).ToList(),
                UserLocations = new List<IdNameEntity>(),
                Roles = new List<Role>()
            };

            return userInfo;
        }

        private IEnumerable<ApplicationMenu> ParseAndCreateMenu(long? parentId, IEnumerable<DataRow> rows)
        {            
            foreach (var row in rows.Where(r => r["ParentId"].TryParse<long?>() == parentId))
            {
                var menuItem = new ApplicationMenu
                {
                    Id = row["MenuId"].TryParse<long>(),
                    ImagePath = row["MenuImagePath"].TryParse<string>(),
                    Name = row["MenuText"].TryParse<string>(),
                    Path = row["MenuPath"].TryParse<string>()                    
                };

                yield return menuItem;
                menuItem.SubMenus = ParseAndCreateMenu(row["MenuId"].TryParse<long>(), rows).ToList();                
            }
        }        

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override string CommandQuery
        {
            get
            {
                return @"Declare @UserId bigint

                        Select top 1 @UserId = Id from Users Where UserName = @UserName and Password = @Password

                        Select Companies.Name as CompanyName,  
                         ThemeName, Users.Id as ShellUserId, Users.UserName, 
                         (Select top 1 Value from UserProfile 
                           Inner Join ProfileFields on ProfileFields.Id = UserProfile.FieldId
                           Where UserId = Users.Id and FieldName = 'EmailAddress') as EmailAddress,

                         (Select top 1 Value from UserProfile 
                           Inner Join ProfileFields on ProfileFields.Id = UserProfile.FieldId
                           Where UserId = Users.Id and FieldName = 'FirstName') as FirstName,

                         (Select top 1 Value from UserProfile 
                           Inner Join ProfileFields on ProfileFields.Id = UserProfile.FieldId
                           Where UserId = Users.Id and FieldName = 'LastName') as LastName,
                         (Select top 1 WarehouseId from UserWarehouses Where UserId = Users.Id and IsDefault = 1) as DefaultWarehouseId
                        from UserMemeberships 
                        Inner Join Users on Users.Id = UserId
                        Inner Join Companies on Companies.Id = UserMemeberships.CompanyId
                        Where Users.Id = @UserId

                        Select ShellMenus.Id as MenuId, Applications.Id as ApplicationId, MenuText, null as MenuPath, null as ParentId, MenuImagePath
                        from 
	                        ShellMenus 
	                        Inner join RoleMenus on ShellMenus.Id = RoleMenus.MenuId
	                        Inner join UserAppRoles on UserAppRoles.RoleId = RoleMenus.RoleId
	                        Inner Join Applications on Applications.MenuId = ShellMenus.Id
                        where IsApplication = 1 and AppId = 1 and UserId = @UserId

                        union

                        Select Shm.Id as MenuId, Shm.ApplicationId, Shm.MenuText, Shm.MenuPath as MenuPath, 
	                        isnull(Shm.ParentId, ShellMenus.Id) as ParentId, Shm.MenuImagePath
                        From ShellMenus as Shm 
	                        Inner join Applications on Applications.Id = Shm.ApplicationId
	                        Inner Join ShellMenus on Applications.MenuId = ShellMenus.Id
	                        Inner join RoleMenus on ShellMenus.Id = RoleMenus.MenuId
	                        Inner join UserAppRoles on UserAppRoles.RoleId = RoleMenus.RoleId
                        where ShellMenus.IsApplication = 1 and AppId = 1 and UserId = @UserId";
            }
        }
        private string GetPasswordHash(string password, string salt)
        {
            var bytes = Encoding.Unicode.GetBytes(password);
            var src = Encoding.Unicode.GetBytes(salt);
            var dst = new byte[src.Length + bytes.Length];
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            var algorithm = HashAlgorithm.Create("SHA1");
            byte[] inArray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inArray);
        }
    }
}
