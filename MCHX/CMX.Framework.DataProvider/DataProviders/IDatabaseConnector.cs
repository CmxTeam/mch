﻿using System;

namespace CMX.Framework.DataProvider.DataProviders
{
    public interface IDatabaseConnector<T> : IDisposable
    {
        T Execute();
    }
}