﻿using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders
{
    public abstract class AdoNetMultyTableDatabaseConnector<T> : AdoNetBaseDatabaseConnector, IDatabaseConnector<T>
    {
        public AdoNetMultyTableDatabaseConnector()
        {

        }

        public AdoNetMultyTableDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public abstract T ObjectInitializer(List<DataTable> tables);

        public virtual T Execute()
        {
            var dataSet = new DataSet();
            base.Execute((adapter) => adapter.Fill(dataSet));
            return ObjectInitializer(dataSet.Tables.OfType<DataTable>().ToList());
        }
    }
}