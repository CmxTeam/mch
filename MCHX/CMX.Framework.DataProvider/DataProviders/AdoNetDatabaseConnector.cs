﻿using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders
{
    public abstract class AdoNetDatabaseConnector<T> : AdoNetBaseDatabaseConnector, IDatabaseConnector<T>
    {
        public AdoNetDatabaseConnector()
        { 
        
        }

        public AdoNetDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public abstract T ObjectInitializer(List<DataRow> rows);

        public virtual T Execute()
        {
            var dataTable = new DataTable();
            base.Execute((adapter) => adapter.Fill(dataTable));
            return ObjectInitializer(dataTable.Rows.OfType<DataRow>().ToList());
        }
    }
}