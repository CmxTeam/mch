﻿using System.Configuration;

namespace CMX.Framework.DataProvider.DataProviders.Common
{
    public abstract class ShellDatabaseConnector<T> : AdoNetMultyTableDatabaseConnector<T>
    {
        public ShellDatabaseConnector()
        { 
        
        }

        public ShellDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["ShellDB"].ConnectionString; }
        }
    }
}