﻿using System;
using System.Data.Entity;

namespace CMX.Framework.DataProvider.DataProviders
{
    public abstract class EfDatabaseConnector<T, U> : IDatabaseConnector<T> where U : DbContext, new()
    {
        private U _context = new U();
        private DbContextTransaction _dbContextTransaction;
        protected U Context { get { return _context; } }
        protected void BeginTransaction()
        {
            _dbContextTransaction = _context.Database.BeginTransaction();
        }
        protected void RollbackTransaction()
        {
            if (_dbContextTransaction != null)
                _dbContextTransaction.Rollback();            
        }
        protected void CommitTransaction()
        {
            if (_dbContextTransaction != null)
                _dbContextTransaction.Commit();
        }
        protected bool MakeDatabaseTransaction(Action action)
        {
            try
            {
                BeginTransaction();
                action();
                CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw;
            }
        }
        public abstract T Execute();

        #region [ -- IDisposable -- ]

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~EfDatabaseConnector()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_context != null)
                _context.Dispose();
            if (_dbContextTransaction != null)
                _dbContextTransaction.Dispose();
            _context = null;
            _dbContextTransaction = null;
        }

        #endregion
    }
}