﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.DataProviders
{
    public abstract class AdoNetBaseDatabaseConnector : IDisposable
    {
        private SqlDataAdapter _adapter;
        private SqlConnection _connection;
        private List<CommandParameter> _parameters;

        public AdoNetBaseDatabaseConnector()
        {
            
        }

        public AdoNetBaseDatabaseConnector(params CommandParameter[] parameters)
        {
            _parameters = new List<CommandParameter>(parameters);
        }

        private System.Data.CommandType _sqlCommandType
        {
            get { return CommandType == CommandType.Text ? System.Data.CommandType.Text : System.Data.CommandType.StoredProcedure; }
        }

        protected List<CommandParameter> Parameters { get { return _parameters; } }

        public abstract string GlobalConnectionString { get; }
        public abstract string CommandQuery { get; }
        public abstract string ConnectionString { get; }

        public virtual CommandType CommandType
        {
            get { return CommandType.Text; }
        }

        protected virtual void Execute(Action<SqlDataAdapter> setFiller)
        {
            _connection = new SqlConnection(ConnectionString);
            _adapter = new SqlDataAdapter(CommandQuery, _connection)
                       {
                           SelectCommand =
                           {
                               CommandType = _sqlCommandType,
                               CommandTimeout = 500
                           }
                       };
            if (_parameters != null)
            {
                foreach (var param in _parameters)
                {
                    if (param == null) continue;
                    _adapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, param.Value ?? DBNull.Value));
                }                
            }

            setFiller(_adapter);
        }

        #region [ -- IDisposable -- ]

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~AdoNetBaseDatabaseConnector()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_adapter != null)
                _adapter.Dispose();

            if (_connection != null)
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _connection.Close();
                _connection.Dispose();
            }
        }

        #endregion
    }
}
