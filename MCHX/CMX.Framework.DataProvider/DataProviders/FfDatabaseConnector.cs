﻿using System.Configuration;

namespace CMX.Framework.DataProvider.DataProviders
{
    public abstract class FfDatabaseConnector<T> : AdoNetDatabaseConnector<T>
    {
        public FfDatabaseConnector()
        {

        }

        public FfDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}