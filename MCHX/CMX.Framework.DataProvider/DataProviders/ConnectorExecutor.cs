﻿using log4net;
using log4net.Core;
using System;

namespace CMX.Framework.DataProvider.DataProviders
{
    public class ConnectorExecutor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ConnectorExecutor));

        public T Execute<T>(Func<IDatabaseConnector<T>> action)
        {
            try
            {
                using (var instance = action())
                {                    
                    return instance.Execute();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);                
                throw;
            }
        }        
    }
}