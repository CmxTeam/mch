﻿using System.Reflection;
using System.Collections.Generic;
using System.Web.Http.Dispatcher;
using System.IO;
using System;

namespace CMX.Framework.DataProvider
{
    public class DynamicAssemblyResolver : DefaultAssembliesResolver
    {
        private string _path = string.Empty;
        public DynamicAssemblyResolver(string path)
        {
            _path = path;
        }

        public override ICollection<Assembly> GetAssemblies()
        {
            var assemblies = base.GetAssemblies();
            foreach (string dll in Directory.GetFiles(_path, "*.dll", SearchOption.AllDirectories))
            {
                try
                {
                    var assembly = Assembly.LoadFile(dll);
                    if (assemblies.Contains(assembly)) continue;
                    assemblies.Add(assembly);
                }                
                catch
                { }
            }
            return assemblies;
        }
    }
}