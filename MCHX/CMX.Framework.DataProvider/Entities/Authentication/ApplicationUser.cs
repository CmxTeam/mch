﻿using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;
namespace CMX.Framework.DataProvider.Entities.Authentication
{
    public class ApplicationUser
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int DefaultWarehouseId { get; set; }
        public IEnumerable<ApplicationMenu> Applications { get; set; }
        public IEnumerable<IdNameEntity> UserLocations { get; set; }
        public IEnumerable<Role> Roles { get; set; }
    }
}