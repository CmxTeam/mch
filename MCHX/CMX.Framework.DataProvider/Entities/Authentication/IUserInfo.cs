﻿using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;
using System.Security.Principal;

namespace CMX.Framework.DataProvider.Entities.Authentication
{
    interface IUserInfo : IIdentity
    {
        long Id { get; set; }
        long? DefaultWarehouseId { get; set; }
        IEnumerable<ApplicationMenu> Applications { get; set; }
        IEnumerable<IdNameEntity> UserLocations { get; set; }
        IEnumerable<Role> Roles { get; set; }
    }
}