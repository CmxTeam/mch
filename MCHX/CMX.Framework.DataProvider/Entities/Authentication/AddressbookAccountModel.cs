﻿using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;
using System.Security.Principal;

namespace CMX.Framework.DataProvider.Entities.Authentication
{
    public class AddressbookAccountModel : IUserInfo
    {
        private bool _isAuthenticated;

        public AddressbookAccountModel()
        {

        }

        public AddressbookAccountModel(bool isAuthenticated)
        {
            _isAuthenticated = isAuthenticated;
        }

        public long Id { get; set; }                
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }        
        public IEnumerable<ApplicationMenu> Applications { get; set; }
        public IEnumerable<IdNameEntity> Gateways { get; set; }
        public IEnumerable<Role> Roles { get; set; }
        public string UserID { get; set; }
        public string UserType { get; set; }        
        public string Login { get; set; }       
        public long? DefaultWarehouseId { get; set; }
        public IEnumerable<IdNameEntity> UserLocations { get; set; }
        public string AuthenticationType
        {
            get { return "Bearer"; }
        }

        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
        }

        public string Name
        {
            get { return UserID; }
        }
    }
}