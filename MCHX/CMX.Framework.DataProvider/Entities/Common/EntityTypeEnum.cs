﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public enum EntityTypeEnum
    {
        FlightManifest = 1,
        AWB = 2,
        HWB = 3,
        ULD = 4,
        Task = 6,
        Shipper = 7,
        ShipperManifest = 8,
        LoadPlan = 9,
        DischargeManifest = 10,
        DischargeShipment = 11
    }
}
