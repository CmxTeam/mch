﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class RemarkUpdater<T> : IdsModel
    {
        public T Content { get; set; }
        public EntityTypeEnum? Type{ get; set; }
    }
}
