﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.Entities.Common
{
    public class EmailDetailsModel
    {
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
        public List<string> NotifTo { get; set; }
        public List<string> Recipients { get; set; }
        public List<string> Attachments { get; set; }       
    }
}
