﻿using System;

namespace CMX.Framework.DataProvider.Entities.Common
{
    public class AttachmentModel : IdNameEntity
    {
        public long DbId { get; set; }
        public long EntityID { get; set; }
        public string FileName { get; set; }       
        public string FileType { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public long? DocumentTypeId { get; set; }
        public int EntityTypeId { get; set; }
        public long? UserId { get; set; }
        public DateTime RecDate { get; set; }
        public virtual EntityTypeEnum EntityType { get; set; }
    }
}
