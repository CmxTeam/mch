﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class DataContainer<T>
    {
        public TransactionStatus Status { get; set; }
        public TransactionStatus Transaction { get; set; }

        public T Data { get; set; }
    }
}
