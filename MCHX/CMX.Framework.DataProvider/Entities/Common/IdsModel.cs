﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class IdsModel
    {
        public long[] Ids { get; set; }
    }
}
