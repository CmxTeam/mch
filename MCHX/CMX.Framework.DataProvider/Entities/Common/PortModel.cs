﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class PortModel : IdNameEntity
    {
        public string FullName { get; set; }
        public bool? IsGateway { get; set; }
        public int? UTCTimeDifference { get; set; }
        public int? ShellPortId { get; set; }
        public MOTModel MOT { get; set; }
        public StateModel State { get; set; }
        public CountryModel Country { get; set; }      
    }
}