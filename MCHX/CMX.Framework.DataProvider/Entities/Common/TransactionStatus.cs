﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class TransactionStatus
    {
        public bool Status { get; set; }
        public string Message { get; set; }

        // a lot of dummy properties i don't know why....
        public string Content { get; set; }
        public string Error { get; set; }
        public string Url { get; set; }
        public string ErrorCode { get; set; }        
        public string Json { get; set; }
    }
}
