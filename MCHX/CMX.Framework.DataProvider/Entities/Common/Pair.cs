﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class Pair<T, U> : Single<T>
    {
        public U Second { get; set; }
    }
}