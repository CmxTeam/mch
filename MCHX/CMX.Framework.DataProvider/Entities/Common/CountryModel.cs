﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class CountryModel : IdEntity
    {
        public string CountryName { get; set; }
        public string TwoCharacterCode { get; set; }
        public CurrencyModel Currency { get; set; }
    }
}
