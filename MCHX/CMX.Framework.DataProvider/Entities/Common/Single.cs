﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class Single<T>
    {
        public T First { get; set; }
    }
}