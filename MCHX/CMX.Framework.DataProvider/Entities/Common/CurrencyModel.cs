﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class CurrencyModel : IdEntity
    {
        public string Currency3Code { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySign { get; set; }
    }
}
