﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class UserProfileModel :IdEntity
    {
        public long? ShellUserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public int? IsSupervisor { get; set; }
        public int? IsTSACertified { get; set; }
        public string Password { get; set; }
    }
}
