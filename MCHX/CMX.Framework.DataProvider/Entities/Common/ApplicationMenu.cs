﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.Entities.Common
{
    public class ApplicationMenu : IdNameEntity
    {
        public string GroupName { get; set; }
        public string Path { get; set; }
        public string ImagePath { get; set; }        
        public IEnumerable<ApplicationMenu> SubMenus { get; set; }
    }
}