﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.Entities.Common
{
    public class SpCommonResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
