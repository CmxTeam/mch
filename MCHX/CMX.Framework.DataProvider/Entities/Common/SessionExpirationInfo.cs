﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class SessionExpirationInfo
    {
        public int SessionForseClose { get; set; }
        public int SessionWorning { get; set; }
    }
}