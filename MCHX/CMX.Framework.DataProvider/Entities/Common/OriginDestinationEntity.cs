﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class OriginDestinationEntity : IdNameEntity
    {
        public PortModel Origin { get; set; }
        public PortModel Destination { get; set; }
    }
}