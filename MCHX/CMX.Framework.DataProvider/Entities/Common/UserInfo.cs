﻿using CMX.Framework.DataProvider.Entities.Authentication;
using System.Collections.Generic;
using System.Security.Principal;
using System.Linq;
namespace CMX.Framework.DataProvider.Entities.Common
{
    public class UserInfo : IUserInfo
    {
        private bool _isAuthenticated;

        public UserInfo()
        {

        }

        public UserInfo(bool isAuthenticated)
        {
            _isAuthenticated = isAuthenticated;
        }


        public long Id { get; set; }        
        public string Name { get; set; }        
        public string ThemeName { get; set; }
        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string SiteIds { get; set; }
        public string AccountIds { get; set; }
        public long? DefaultSiteId { get; set; }
        public long? DefaultAccountId { get; set; }        
        public int ClientTimeZoneOffset { get; set; }
        public string ClientTimeZone { get; set; }
        public long UserLocalId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string WarehouseIds { get; set; }
        public long? DefaultWarehouseId { get; set; }
        public IEnumerable<ApplicationMenu> Applications { get; set; }
        public string ApiUrl { get; set; }
        public bool IsSecureApi { get; set; }
        public IEnumerable<IdNameEntity> UserLocations { get; set; }
        public IEnumerable<Role> Roles { get; set; }

        public bool CanManageCompanyUsers()
        {
            return !string.IsNullOrWhiteSpace(this.CompanyName) && this.CompanyName == "Cargomatrix";            
        }

        public string AuthenticationType
        {
            get { return "Bearer"; }
        }

        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
        }        
    }
}