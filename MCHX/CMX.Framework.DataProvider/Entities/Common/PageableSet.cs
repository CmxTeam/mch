﻿using System.Collections.Generic;
namespace CMX.Framework.DataProvider.Entities.Common
{
    public class PageableSet<T>
    {
        public int Count { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}