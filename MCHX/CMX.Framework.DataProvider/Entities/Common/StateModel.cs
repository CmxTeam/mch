﻿namespace CMX.Framework.DataProvider.Entities.Common
{
    public class StateModel : IdEntity
    {
        public string StateProvince { get; set; }
        public string TwoCharacterCode { get; set; }
        public CountryModel Country { get; set; }
    }
}
