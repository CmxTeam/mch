﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoSnapshot.Helpers
{
    public static class FilePathHelper
    {
        public static string CarrierImageRelativePath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotCarrierImageRelativePath"];              
            }
        }
        public static string AnnotationImageRelativePath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotAnnotationImageRelativePath"];             
            }
        }
        public static string UserImageRelativePath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotUserImageRelativePath"];               
            }
        }
        public static string SnapshotPhotoRelativePath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotSnapshotPhotoRelativePath"];               
            }
        }
        public static string SettingLogoRelativePath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotSettingLogoRelativePath"];
            }
        }
        public static string CarrierImageFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotCarrierImageFullPath"];              
            }
        }
        public static string AnnotationImageFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotAnnotationImageFullPath"];
            }
        }
        public static string UserImageFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotUserImageFullPath"];             
            }
        }
        public static string SnapshotPhotoFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotSnapshotPhotoFullPath"];
            }
        }
        public static string SettingLogoFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["CargoSnapsotSettingLogoFullPath"];
            }
        }
      
    }
}
