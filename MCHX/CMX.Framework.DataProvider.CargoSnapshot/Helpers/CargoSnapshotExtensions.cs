﻿using CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Linq;

namespace CMX.Framework.DataProvider.CargoSnapshot.Helpers
{
    public static class CargoSnapshotExtensions
    {
        public static AnnotationModel ConvertTo(this Annotation annotation)
        {
            if (annotation == null) return null;
            return new AnnotationModel
            {
                Id = annotation.Id,
                Name = annotation.Name,
                Description = annotation.Description,
                AnnotationImage = FilePathHelper.AnnotationImageRelativePath + annotation.AnnotationImagePath
            };
        }
        public static void Update(this Annotation annotation, AnnotationModel annotationModel)
        {
            if (annotation == null) return;
            annotation.Name = annotationModel.Name;
            annotation.Description = annotationModel.Description;
        }
        public static Annotation ConvertTo(this AnnotationModel annotation)
        {
            if (annotation == null) return null;
            return new Annotation
            {
                Name = annotation.Name,
                Description = annotation.Description
            };
        }
        public static SettingModel ConvertTo(this Setting setting)
        {
            if (setting == null) return null;
            return new SettingModel
            {
                Id = setting.Id,
                Logo = FilePathHelper.SettingLogoRelativePath + setting.Logo,
                ProductName = setting.ProductName,
                ShowHeader = setting.ShowHeader
            };
        }
        public static Setting ConvertTo(this SettingModel setting)
        {
            if (setting == null) return null;
            return new Setting
            {
                ProductName = setting.ProductName,
                ShowHeader = setting.ShowHeader
            };
        }
        public static void Update(this Setting setting, SettingModel settingModel)
        {
            if (setting == null) return;
            setting.ProductName = settingModel.ProductName;
            setting.ShowHeader = settingModel.ShowHeader;

        }

        public static void Update(this UserProfile userProfile, CargoSnapshotUserProfileModel userProfileModel)
        {
            if (userProfile == null) return;
            userProfile.DefaultTemplateId = (userProfileModel.DefaultTemplate != null ? (long?)userProfileModel.DefaultTemplate.Id : null);
            userProfile.Email = userProfileModel.Email;
            userProfile.Fax = userProfileModel.Fax;
            userProfile.FirstName = userProfileModel.FirstName;
            userProfile.IsSupervisor = userProfileModel.IsSupervisor;
            userProfile.IsTSACertified = userProfileModel.IsTSACertified;
            userProfile.JobTitle = userProfileModel.JobTitle;
            userProfile.LastName = userProfileModel.LastName;
            userProfile.MiddleName = userProfileModel.MiddleName;
            userProfile.Mobile = userProfileModel.Mobile;
            userProfile.Phone = userProfileModel.Phone;
            userProfile.SettingId = (userProfileModel.Setting != null ? (long?)userProfileModel.Setting.Id : null);
            userProfile.ShellUserId = userProfileModel.ShellUserId;
            userProfile.Title = userProfileModel.Title;
            userProfile.UserId = userProfileModel.UserName;
        }

        public static CargoSnapshotUserProfileModel ConvertTo(this UserProfile userProfile)
        {
            if (userProfile == null) return new CargoSnapshotUserProfileModel();
            else return new CargoSnapshotUserProfileModel
            {
                //DefaultTemplate = userProfile.EmailTemplate != null ? userProfile.EmailTemplate.ConvertTo() : null,
                Email = userProfile.Email,
                Fax = userProfile.Fax,
                FirstName = userProfile.FirstName,
                Id = userProfile.Id,
                IsSupervisor = userProfile.IsSupervisor,
                IsTSACertified = userProfile.IsTSACertified,
                JobTitle = userProfile.JobTitle,
                LastName = userProfile.LastName,
                MiddleName = userProfile.MiddleName,
                Mobile = userProfile.MiddleName,
                Phone = userProfile.Phone,
                Photo = userProfile.PhotoPath != null ? FilePathHelper.UserImageRelativePath + userProfile.PhotoPath : null,
                Setting = userProfile.Setting != null ? userProfile.Setting.ConvertTo() : null,
                ShellUserId = userProfile.ShellUserId,
                Title = userProfile.Title,
                UserName = userProfile.UserId,
                ImageUpload = userProfile.ImageUploadPath,
                ImageDownload = userProfile.ImageDownloadPath
            };
        }

        public static EmailTemplateModel ConvertTo(this EmailTemplate template)
        {
            if (template == null) return null;
            return new EmailTemplateModel
            {
                Id = template.Id,
                ConsigneeName = template.ConsigneeName,
                DeliveryVPOC = template.DeliveryVPOC,
                EmailNotification = template.EmailNotification,
                MsgBody = template.MsgBody,
                RecepientEmail = template.RecepienEmail,
                SendTo = template.SentTo,
                Shared = template.Shared,
                Subject = template.Subject,
                TemplateTitle = template.TemplateTitle,
                UseEmailSender = template.UseEmailSender,
                UserProfile = template.UserProfile != null ? template.UserProfile.ConvertTo() : null
            };
        }

        public static EmailTemplate ConvertTo(this EmailTemplateModel template)
        {
            if (template == null) return null;
            return new EmailTemplate
            {
                ConsigneeName = template.ConsigneeName,
                DeliveryVPOC = template.DeliveryVPOC,
                EmailNotification = template.EmailNotification,
                MsgBody = template.MsgBody,
                RecepienEmail = template.RecepientEmail,
                SentTo = template.SendTo,
                Shared = template.Shared,
                Subject = template.Subject,
                TemplateTitle = template.TemplateTitle,
                UseEmailSender = template.UseEmailSender,
                UserProfileId = template.UserProfile != null ? (long?)template.UserProfile.Id : null
            };
        }
        public static void Update(this EmailTemplate template, EmailTemplateModel templateModel)
        {
            if (template == null) return;
            template.ConsigneeName = templateModel.ConsigneeName;
            template.DeliveryVPOC = templateModel.DeliveryVPOC;
            template.EmailNotification = templateModel.EmailNotification;
            template.MsgBody = templateModel.MsgBody;
            template.RecepienEmail = templateModel.RecepientEmail;
            template.SentTo = templateModel.SendTo;
            template.Shared = templateModel.Shared;
            template.Subject = templateModel.Subject;
            template.TemplateTitle = templateModel.TemplateTitle;
            template.UseEmailSender = templateModel.UseEmailSender;
            template.UserProfileId = templateModel.UserProfile != null ? (long?)templateModel.UserProfile.Id : null;
        }
        public static TransactionPhotosAnnotationModel ConvertTo(this TransactionPhotosAnnotation tranAnnotation)
        {
            if (tranAnnotation == null) return null;
            return new TransactionPhotosAnnotationModel
            {
                Id = tranAnnotation.Id,
                RecDate = tranAnnotation.RecDate,
                XPosition = tranAnnotation.XPosition,
                YPosition = tranAnnotation.YPosition,
                Annotation = tranAnnotation.Annotation.ConvertTo(),
                TransactionPhoto = new TransactionPhotoModel { Id = tranAnnotation.TransactionPhotoId }
            };
        }
        public static TransactionPhotosAnnotation ConvertTo(this TransactionPhotosAnnotationModel tranAnnotation)
        {
            if (tranAnnotation == null) return null;
            return new TransactionPhotosAnnotation
            {
                Id = tranAnnotation.Id,
                XPosition = tranAnnotation.XPosition,
                YPosition = tranAnnotation.YPosition,
                AnotationId = tranAnnotation.Annotation.Id,
                RecDate = DateTime.UtcNow
            };
        }
        public static void Update(this TransactionPhotosAnnotation transactionPhoto, TransactionPhotosAnnotationModel transactionPhotoModel)
        {
            if (transactionPhoto == null) return;
            transactionPhoto.RecDate = DateTime.UtcNow;
            transactionPhoto.XPosition = transactionPhotoModel.XPosition;
            transactionPhoto.YPosition = transactionPhotoModel.YPosition;
        }

        public static TransactionModel ConvertTo(this Transaction transaction)
        {
            if (transaction == null) return null;
            return new TransactionModel
            {
                Id = transaction.Id,
                RecDate = transaction.RecDate,
                AppicationId = transaction.ApplicationId            
            };
        }
        public static Transaction ConvertTo(this TransactionModel transaction)
        {
            if (transaction == null) return null;
            return new Transaction
            {
                RecDate = DateTime.UtcNow,
                ApplicationId = transaction.AppicationId,
                ReferenceTypeId = (int)transaction.ReferenceType
            };
        }
        public static void Update(this Transaction transaction, TransactionModel transactionModel)
        {
            if (transaction == null) return;
            transaction.RecDate = DateTime.UtcNow;
            transaction.ApplicationId = transactionModel.AppicationId;
        }

        public static TransactionPhotoModel ConvertTo(this TransactionPhoto photo)
        {
            if (photo == null) return null;
            return new TransactionPhotoModel
            {
                Id = photo.Id,
                Image = photo.Path != null ? FilePathHelper.SnapshotPhotoRelativePath + photo.Path : null,
                RecDate = photo.RecDate,
                UserProfile = photo.UserProfile.ConvertTo(),
                Transaction = new TransactionModel { Id = photo.TransactionId }
            };
        }
        public static TransactionPhoto ConvertTo(this TransactionPhotoModel photo)
        {
            if (photo == null) return null;
            return new TransactionPhoto
            {
                RecDate = DateTime.UtcNow,
                LocationId = photo.LocationId,
                Longitude = photo.Longitude,
                Latitude = photo.Latitude,
                UserProfileId = photo.UserProfile.Id
            };
        }
        public static void Update(this TransactionPhoto photo, TransactionPhotoModel photoModel)
        {
            if (photo == null) return;
            photo.RecDate = DateTime.UtcNow;
            photo.LocationId = photoModel.LocationId;
            photo.UserProfileId = photoModel.UserProfile.Id;
        }
        public static ControlModel ConvertTo(this Property control,bool isFilter = false)
        {
            if (control == null) return null;
            return new ControlModel
            {
                Id = control.Id,
                Name = control.Name,
                Type = control.Type,
                DisplayName = control.DisplayName,
                HtmlType = control.HtmlType,
                IsMandatory = !isFilter && control.IsMandatory,
                PropertySourceTypeId = control.PropertySourceTypeId,
                ReferenceTypeId = control.ReferenceTypeId
            };
        }
        public static Property ConvertTo(this ControlModel control)
        {
            if (control == null) return null;
            return new Property
            {
                Id = control.Id,
                Name = control.Name,
                Type = control.Type,
                DisplayName = control.DisplayName,
                HtmlType = control.HtmlType,
                IsMandatory = control.IsMandatory,
                IsFilter =  control.IsFilter,
                IsDetail = control.IsDetails,
                PropertySourceTypeId = control.PropertySourceTypeId,
                ReferenceTypeId = control.ReferenceTypeId
            };
        }
    }
}
