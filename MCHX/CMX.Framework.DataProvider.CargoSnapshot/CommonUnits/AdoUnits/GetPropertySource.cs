﻿using System.Linq;
using System.Data;
using System.Dynamic;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.AdoUnits
{
    public class GetPropertySource : CargoSnapshotAdoDatabaseConnector<IEnumerable<object>>
    {
        public GetPropertySource(long propertySourceTypeId)
            : base(new CommandParameter("@PropertySourceTypeId", propertySourceTypeId))
        {

        }

        public override IEnumerable<object> ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return new List<object>();
            var columns = rows[0].Table.Columns;

            return rows.Select(r =>
                               {
                                   var dyn = new ExpandoObject() as IDictionary<string, object>;
                                   foreach (DataColumn column in columns)
                                       dyn.Add(column.ColumnName, r[column.ColumnName]);
                                   return dyn;
                               });
        }

        public override DataProviders.CommandType CommandType
        {
            get { return DataProviders.CommandType.StoredProcedure; }
        }

        public override string CommandQuery
        {
            get { return "GetPropertySource"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
