﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.SqlServer;
using System.Dynamic;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.Dictionary;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetTransactions : CargoSnapshotEfDatabaseConnector<IEnumerable<object>>
    {
        private long _userId;
        private const string _sep = ";";
        private TransactionFilterSearchModel _filter;
        private List<string> _dynamicFilter = new List<string>();

        public GetTransactions(long userId, TransactionFilterSearchModel filter)
        {
            _userId = userId;
            _filter = filter ?? new TransactionFilterSearchModel();
            ParseFilter(filter.ReferenceType);
            Context.Configuration.AutoDetectChangesEnabled = false;
        }
        public override IEnumerable<object> Execute()
        {
            var query = Context.Transactions.Include("Values")
                .Include("Values.Property").Include("TransactionPhotos")
                .Include("TransactionPhotos.UserProfile")
                .Include("TransactionPhotos.TransactionPhotosAnnotations")
                .Include("TransactionPhotos.TransactionPhotosAnnotations.Annotation")
                .Include("TransactionPhotos.TransactionPhotosAnnotations.TransactionPhoto")
                .Include("TransactionPhotos.PropertySource")
                .Include("TransactionPhotos.PropertySource.PropertySourceDetails")
                .Where(t =>    (t.ReferenceTypeId == (int)_filter.ReferenceType)
                            && (_filter.ApplicationId == 0 || t.ApplicationId == _filter.ApplicationId)
                            && (_filter.DateTo == null || t.RecDate <= _filter.DateTo)
                            && (_filter.DateFrom == null || t.RecDate >= _filter.DateFrom) 
                            && (_filter.TransactionId == null || t.Id == _filter.TransactionId)
                            && (_filter.PhotoLocationId == null || t.TransactionPhotos.Any(photo => photo.LocationId == _filter.PhotoLocationId)) 
                      );
            
            if (_filter.Properties != null &&  _filter.Properties.Count > 0)
                query = query.Where(t =>_dynamicFilter.All(d =>t.Values.Any(v =>SqlFunctions.PatIndex(d,v.PropertyId + _sep + v.Value1 + _sep + v.PropertySourceId) > 0)));
            
            if (_filter.Annotations != null)
                query = query.Where(s => s.TransactionPhotos.Any(p =>(p.TransactionPhotosAnnotations.Select(tp => tp.AnotationId)
                                                                                                    .Intersect(_filter.Annotations))
                                                                                                    .Any()));

            return query.ToList().Select(transaction =>
                                        {
                                            var dyn = new ExpandoObject() as IDictionary<string, object>;
                                            dyn.Add("TransactionId", transaction.Id);
                                            dyn.Add("RecDate", transaction.RecDate);
                                            dyn.Add("ApplicationId", transaction.ApplicationId);

                                            foreach (var value in transaction.Values)
                                                dyn.Add(value.Property.Name, value.Value1 ?? value.PropertySourceId.ToString());

                                            var transactionPhotos = transaction.TransactionPhotos.ToList().Select(transactionPhoto =>
                                               {
                                                   var tranPhotoModel = transactionPhoto.ConvertTo();
                                                   
                                                   var dynLocation = new ExpandoObject() as IDictionary<string, object>;

                                                   dynLocation.Add("Id", transactionPhoto.PropertySource.Id);
                                                   dynLocation.Add("ShortName", transactionPhoto.PropertySource.Value);
                                                   foreach (var photoValue in transactionPhoto.PropertySource.PropertySourceDetails)
                                                        dynLocation.Add(photoValue.Name,photoValue.Value);
                                                   tranPhotoModel.Location = dynLocation;
                                                   
                                                   tranPhotoModel.PhotoAnnotations = transactionPhoto.TransactionPhotosAnnotations.Select(trnAnnot => trnAnnot.ConvertTo());
                                                   tranPhotoModel.AnnotationNames = string.Join(",", tranPhotoModel.PhotoAnnotations.Select(s => s.Annotation.Name));
                                                   return tranPhotoModel;
                                               });
                                            dyn.Add("TransactionPhotos", transactionPhotos);
                                            return dyn;
                                        });
        }

        private void ParseFilter(ReferenceTypeEnum refType)
        {
            if (_filter.Properties == null) return;

            var dbData = Context.Properties.Where(c => c.ReferenceTypeId == (int)refType && (c.Name == Controls.DateFrom || c.Name == Controls.DateTo || c.Name == Controls.PhotoLocation)).ToList();
            var fromDateId = dbData.Where(s => s.Name == Controls.DateFrom).Select(s => s.Id).FirstOrDefault();
            var dateToId = dbData.Where(s => s.Name == Controls.DateTo).Select(s => s.Id).FirstOrDefault();
            var photoLocId = dbData.Where(s => s.Name == Controls.PhotoLocation).Select(s => s.Id).FirstOrDefault();

            var dFrom =_filter.Properties.FirstOrDefault(p => p.Id == fromDateId);
            if (dFrom != null)
            {
                _filter.DateFrom = dFrom.Value.TryParse<DateTime?>();
                _filter.Properties.Remove(dFrom);
            }
            var dTo = _filter.Properties.FirstOrDefault(p => p.Id == dateToId);
            if (dTo != null)
            {
                _filter.DateTo = dTo.Value.TryParse<DateTime?>();
                _filter.Properties.Remove(dTo);
            }
            
            var photo = _filter.Properties.FirstOrDefault(p => p.Id == photoLocId);
            if (photo != null)
            {
                _filter.PhotoLocationId = photo.Source.TryParse<long>();
                _filter.Properties.Remove(photo);
            }
            _dynamicFilter = _filter.Properties.Select(f => f.Id + _sep + "%" + f.Value + "%" + _sep + f.Source).ToList();
        }
    }
}
