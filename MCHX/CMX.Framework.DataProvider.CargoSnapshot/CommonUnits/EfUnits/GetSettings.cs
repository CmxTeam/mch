﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetSettings : CargoSnapshotEfDatabaseConnector<IEnumerable<SettingModel>>
    {
        private readonly long _settingId;
        public GetSettings(long settingId = 0)
        {
            _settingId = settingId;
        }
        public override IEnumerable<SettingModel> Execute()
        {
            var dbSettings = Context.Settings.Where(s => _settingId == 0 || s.Id == _settingId).ToList();
            return dbSettings.Select(s => s.ConvertTo());
        }
    }
}
