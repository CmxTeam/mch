﻿using System;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SaveSetting : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly SettingModel _setting;
        public SaveSetting(SettingModel setting)
        {
            _setting = setting;
        }
        public override bool Execute()
        {
            if (_setting == null) return false;
            if (_setting.Id == 0)
            {
                var dbSetting = _setting.ConvertTo();
                if (!String.IsNullOrEmpty(_setting.Logo))
                    dbSetting.Logo = FileHelper.SaveImage(dbSetting.Logo, FilePathHelper.SettingLogoFullPath, string.Empty);
                Context.Settings.Add(dbSetting);
            }
            else
            {
                var dbSetting = Context.Settings.Single(s => s.Id == _setting.Id);
                dbSetting.Update(_setting);
                if (!String.IsNullOrEmpty(_setting.Logo) && _setting.Logo.Split('/').Last() != dbSetting.Logo)
                    dbSetting.Logo = FileHelper.SaveImage(dbSetting.Logo, FilePathHelper.SettingLogoFullPath, string.Empty);
            }
            Context.SaveChanges();
            return true;
        }
    }
}
