﻿using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel;
using CMX.Framework.DataProvider.Helpers;


namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class CargoSnapShotEmailSender : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly CargoSnapshotEmailModel _emailDetails;
        public CargoSnapShotEmailSender(CargoSnapshotEmailModel email)
        {
            _emailDetails = email;
        }
        public override bool Execute()
        {
            EmailDetailsModel email = _emailDetails;
            if (_emailDetails.AttachmentFileIds != null && _emailDetails.AttachmentFileIds.Any())
            {
                email.Attachments = new List<string>();
                var dbPhotoAttachments = Context.TransactionPhotos.Include("Transaction").Include("Location")
                    .Where(s => _emailDetails.AttachmentFileIds.Contains(s.Id)).ToList();
                var photoAttachments =
                    dbPhotoAttachments.Select(s => new {Image = FilePathHelper.SnapshotPhotoFullPath + s.Path}).ToList();
                photoAttachments.ForEach(a => email.Attachments.Add(a.Image));
            }
            EmailSender.Send(email);
            return true;
        }
    }
}
