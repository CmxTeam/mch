﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetControls : CargoSnapshotEfDatabaseConnector<IEnumerable<ControlModel>>
    {
        private bool? _isFilter;
        private bool? _isDetails;
        private long _referenceTypeId;
        public GetControls(long referenceTypeId,bool? isFilter = null,bool? isDetails = null)
        {
            _isDetails = isDetails;
            _isFilter = isFilter;
            _referenceTypeId = referenceTypeId;
        }
        public override IEnumerable<ControlModel> Execute()
        {
            var controls = Context.Properties.Where(a => a.ReferenceTypeId == _referenceTypeId 
                                                    && (_isFilter == null || a.IsFilter == _isFilter)
                                                    && (_isDetails == null || a.IsDetail == _isDetails)
                                                    ).OrderBy(s => s.Precedence).ToList();
            return controls.Select(s => s.ConvertTo(_isFilter ?? false));
        }
    }
}
