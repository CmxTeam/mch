﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SaveControl : CargoSnapshotEfDatabaseConnector<bool>
    {
        private ControlModel _control;
        public SaveControl(ControlModel control)
        {
            _control = control;
        }
        public override bool Execute()
        {
            if (_control == null) return false;
            var dbControl = _control.ConvertTo();
            Context.Properties.Add(dbControl);
            Context.SaveChanges();
            return true;
        }
    }
}
