﻿using System.Linq;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeleteSetting : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly long _settingId;
        public DeleteSetting(long settingId)
        {
            _settingId = settingId;
        }
        public override bool Execute()
        {
            if (_settingId == 0) return false;
            var dbSetting = Context.Settings.Single(s => s.Id == _settingId);
            Context.Settings.Remove(dbSetting);
            Context.SaveChanges();
            return true;
        }
    }
}
