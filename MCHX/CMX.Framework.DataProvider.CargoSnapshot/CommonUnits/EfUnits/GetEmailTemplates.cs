﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    class GetEmailTemplates : CargoSnapshotEfDatabaseConnector<IEnumerable<EmailTemplateModel>>
    {
        private readonly long _emailTemplateId;
        public GetEmailTemplates(long emailTemplate = 0)
        {
            _emailTemplateId = emailTemplate;
        }
        public override IEnumerable<EmailTemplateModel> Execute()
        {
            var dbEmailTemplates = Context.EmailTemplates.Include("UserProfile").Where(e => _emailTemplateId == 0 || e.Id == _emailTemplateId).ToList();
            return dbEmailTemplates.Select(s => s.ConvertTo());
        }
    }
}
