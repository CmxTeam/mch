﻿using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SendEmailPassword : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly UserProfileModel _userProfile;
        public SendEmailPassword(UserProfileModel user)
        {
            _userProfile = user;
        }
        public override bool Execute()
        {
            EmailDetailsModel emailDetails = new EmailDetailsModel();
            emailDetails.Subject = "Forgot password";
            emailDetails.Body = _userProfile.FirstName + "! You password is " + _userProfile.Password;
            emailDetails.Recipients.Add(_userProfile.Email);
            EmailSender.Send(emailDetails);           
            return true;
        }
    }
}
