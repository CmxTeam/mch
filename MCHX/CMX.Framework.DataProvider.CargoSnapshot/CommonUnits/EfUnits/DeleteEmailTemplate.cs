﻿using System.Linq;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeleteEmailTemplate : CargoSnapshotEfDatabaseConnector<bool>
    {
        private long _templateId;
        public DeleteEmailTemplate(long templateId)
        {
            _templateId = templateId;
        }
        public override bool Execute()
        {
            if (_templateId == 0) return false;
            var dbTemlate = Context.EmailTemplates.Single(s => s.Id == _templateId);
            Context.EmailTemplates.Remove(dbTemlate);
            Context.SaveChanges();
            return true;
        }
    }
}
