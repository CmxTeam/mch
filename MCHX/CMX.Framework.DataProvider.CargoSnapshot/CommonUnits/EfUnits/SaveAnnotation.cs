﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SaveAnnotation : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly AnnotationModel _annotationModel;
        public SaveAnnotation(AnnotationModel annotation)
        {
            _annotationModel = annotation;
        }
        public override bool Execute()
        {
            if (_annotationModel.Id == 0)
            {
                var dbAnnotation = _annotationModel.ConvertTo();
                if (!String.IsNullOrEmpty(_annotationModel.AnnotationImage))
                    dbAnnotation.AnnotationImagePath = FileHelper.SaveImage(_annotationModel.AnnotationImage, FilePathHelper.AnnotationImageFullPath, string.Empty);
                Context.Annotations.Add(dbAnnotation);
            }
            else
            {
                var photoName = _annotationModel.AnnotationImage.Contains("/")
                                   ? _annotationModel.AnnotationImage.Split('/').Last()
                                   : _annotationModel.AnnotationImage.Split('\\').Last();
                var dbAnnotation = Context.Annotations.Single(a => a.Id == _annotationModel.Id);
                if (!String.IsNullOrEmpty(_annotationModel.AnnotationImage) && photoName != dbAnnotation.AnnotationImagePath)
                    dbAnnotation.AnnotationImagePath = FileHelper.SaveImage(_annotationModel.AnnotationImage, FilePathHelper.AnnotationImageFullPath, string.Empty);
                dbAnnotation.Update(_annotationModel);
            }
            Context.SaveChanges();
            return true;
        }
    }
}
