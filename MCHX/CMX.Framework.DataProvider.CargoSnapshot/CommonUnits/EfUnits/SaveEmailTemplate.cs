﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SaveEmailTemplate : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly long _userId;
        private readonly EmailTemplateModel _template;
        public SaveEmailTemplate(long userId,EmailTemplateModel template)
        {
            _userId = userId;
            _template = template;
        }
        public override bool Execute()
        {
            if (_template == null) return false;
            _template.UserProfile = new UserProfileModel(){Id = _userId};
            if(_template.Id == 0)
            {
                var dbTemplate = _template.ConvertTo();
                Context.EmailTemplates.Add(dbTemplate);
            }
            else
            {
                var dbTemplate = Context.EmailTemplates.Single(s => s.Id == _template.Id);
                dbTemplate.Update(_template);
            }
            Context.SaveChanges();
            return true;
        }
    }
}
