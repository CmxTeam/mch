﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SaveUserProfile : CargoSnapshotEfDatabaseConnector<bool>
    {
        readonly CargoSnapshotUserProfileModel _userProfile;
        public SaveUserProfile(CargoSnapshotUserProfileModel userProfile)
        {
            _userProfile = userProfile;
        }
        public override bool Execute()
        {
            if (_userProfile == null || _userProfile.Id == 0) return false;
            var dbUserProfile = Context.UserProfiles.Single(u => u.Id == _userProfile.Id);
            dbUserProfile.Update(_userProfile);
            var photoName = _userProfile.Photo.Contains("/")
                ? _userProfile.Photo.Split('/').Last()
                : _userProfile.Photo.Split('\\').Last();
            if (!String.IsNullOrEmpty(_userProfile.Photo) && photoName != dbUserProfile.PhotoPath)
                dbUserProfile.PhotoPath = FileHelper.SaveImage(_userProfile.Photo, FilePathHelper.UserImageFullPath, string.Empty);
            
            Context.SaveChanges();
            return true;
        }
    }
}
