﻿using System;
using System.Linq;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.Dictionary;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SaveTransaction : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly TransactionModel _transactionModel;

        public SaveTransaction(TransactionModel transaction)
        {
            _transactionModel = transaction;
            ParseFitler(transaction.ReferenceType);
        }
        public override bool Execute()
        {
            return MakeDatabaseTransaction(() => CoreExecute());
        }
        public bool CoreExecute()
        {
            if (_transactionModel == null) return false;
            if (_transactionModel.Id == 0)
                SaveTransactions();
            else
                UpdateTransactions();
            Context.SaveChanges();
            return true;
        }

        private void SaveTransactions()
        {
            var dbTransaction = _transactionModel.ConvertTo();
            
            _transactionModel.Properties.ForEach(s => dbTransaction.Values.Add(new Value()
            {
                PropertyId = s.Id,
                Value1 = s.Value,
                Transaction = dbTransaction,
                PropertySourceId = s.Source
            }));
            
            ManagePhotosTransaction(dbTransaction);
            Context.Transactions.Add(dbTransaction);
        }

        private void UpdateTransactions()
        {
            var dbTransaction = Context.Transactions.Single(s => s.Id == _transactionModel.Id);
            dbTransaction.Update(_transactionModel);
            if (_transactionModel.Properties != null)
                _transactionModel.Properties.ForEach(s =>
                {
                    var dbValue = dbTransaction.Values.Single(a => a.TransactionId == _transactionModel.Id && a.PropertyId == s.Id);
                    dbValue.Value1 = s.Value;
                    dbValue.PropertySourceId = s.Source;
                });

            var trnPhotos = _transactionModel.TransactionPhotos.Select(a => a.Id).ToList();
            var forRemovePhotoTransactions = Context.TransactionPhotos.Where(p => _transactionModel.Id == p.TransactionId && !trnPhotos.Any(a => a == p.Id));
            Context.TransactionPhotos.RemoveRange(forRemovePhotoTransactions);
            ManagePhotosTransaction(dbTransaction);
            Context.SaveChanges();
        }

        private void ManagePhotosTransaction(Transaction dbTransaction)
        {
            foreach (var tranPhoto in _transactionModel.TransactionPhotos)
            {
                if (tranPhoto.Id == 0)
                    AddNewTransactionPhoto(dbTransaction, tranPhoto);
                else
                    UpdateTransactionPhoto(dbTransaction, tranPhoto);
            }
        }

        private void AddNewTransactionPhoto(Transaction dbTransaction, TransactionPhotoModel tranPhoto)
        {
            TransactionPhoto dbTranPhoto = tranPhoto.ConvertTo();
            if (!String.IsNullOrEmpty(tranPhoto.Image))
                dbTranPhoto.Path = FileHelper.SaveImage(tranPhoto.Image, FilePathHelper.SnapshotPhotoFullPath, string.Empty);
            if (tranPhoto.PhotoAnnotations != null)
                foreach (var annotation in tranPhoto.PhotoAnnotations)
                {
                    var dbTranPhotoAnnotation = annotation.ConvertTo();
                    dbTranPhotoAnnotation.AnotationId = annotation.Annotation.Id;
                    dbTranPhotoAnnotation.TransactionPhoto = dbTranPhoto;
                    Context.TransactionPhotosAnnotations.Add(dbTranPhotoAnnotation);
                }
            dbTransaction.TransactionPhotos.Add(dbTranPhoto);
        }

        private void UpdateTransactionPhoto(Transaction dbTransaction, TransactionPhotoModel tranPhoto)
        {
            var dbTranPhoto = Context.TransactionPhotos.Single(s => s.Id == tranPhoto.Id);
            dbTranPhoto.Update(tranPhoto);
            var photoName = tranPhoto.Image.Contains('\\') ? tranPhoto.Image.Split('\\').Last() : tranPhoto.Image.Split('/').Last();
            if (!String.IsNullOrEmpty(tranPhoto.Image) && photoName != dbTranPhoto.Path)
                dbTranPhoto.Path = FileHelper.SaveImage(tranPhoto.Image, FilePathHelper.SnapshotPhotoFullPath, string.Empty);

            var dbAnnotations = dbTranPhoto.TransactionPhotosAnnotations;
            var modelAnnotations = tranPhoto.PhotoAnnotations;
            var forRemove = dbAnnotations.Where(s => modelAnnotations.All(m => m.Annotation.Id != s.AnotationId));
            var forInsert = modelAnnotations != null ? modelAnnotations.Where(m => dbAnnotations.All(a => a.AnotationId != m.Annotation.Id))
                .Select(s => { var dbTranAnnotation = s.ConvertTo(); dbTranAnnotation.TransactionPhoto = dbTranPhoto; return dbTranAnnotation; }) : null;

            Context.TransactionPhotosAnnotations.RemoveRange(forRemove);
            if (forInsert != null)
                Context.TransactionPhotosAnnotations.AddRange(forInsert);
            Context.SaveChanges();
        }
        private void ParseFitler(ReferenceTypeEnum refType)
        {
            if (_transactionModel == null) return;

            var recDateControlId = Context.Properties.Single(s => s.ReferenceTypeId == (int)refType && (s.Name == Controls.CurrentDate)).Id;
            if (_transactionModel.Properties != null)
            {
                var tmpRecDate = _transactionModel.Properties.FirstOrDefault(p => p.Id == recDateControlId);
                if (tmpRecDate != null)
                {
                    _transactionModel.RecDate = tmpRecDate.Value.TryParse<DateTime>();
                    _transactionModel.Properties.Remove(tmpRecDate);
                }
            }
        }
    }
}