﻿using CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetAnnotations : CargoSnapshotEfDatabaseConnector<IEnumerable<AnnotationModel>>
    {
        private readonly long _id;
        public GetAnnotations(long id = 0)
        {
            _id = id;
        }
        public override IEnumerable<AnnotationModel> Execute()
        {
            IEnumerable<Annotation> dbAnnotations = _id == 0 ? Context.Annotations.ToList() : Context.Annotations.Where(a => a.Id == _id).ToList();
            return dbAnnotations.Select(s => s.ConvertTo());
        }
    }
}
