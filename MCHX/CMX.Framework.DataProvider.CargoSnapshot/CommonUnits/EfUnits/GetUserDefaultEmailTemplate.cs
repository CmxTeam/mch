﻿using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetUserDefaultEmailTemplate : CargoSnapshotEfDatabaseConnector<EmailTemplateModel>
    {
        private readonly long _userId; 
        public GetUserDefaultEmailTemplate(long userId)
        {
            _userId = userId;
        }
        public override EmailTemplateModel Execute()
        {
            var dbDefaultTemplate = Context.UserProfiles.Include("EmailTemplate").Where(s => s.Id == _userId).Select(a => a.EmailTemplate).FirstOrDefault();
            var defaultTemplate = dbDefaultTemplate.ConvertTo();
            return defaultTemplate;
        }
    }
}
