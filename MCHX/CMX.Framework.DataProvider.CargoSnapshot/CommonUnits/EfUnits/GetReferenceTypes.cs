﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetReferenceTypes : CargoSnapshotEfDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public override IEnumerable<IdNameEntity> Execute()
        {
            return Context.ReferenceTypes.Select(a => new IdNameEntity() {Id = a.Id, Name = a.Name}).ToList();
        }
    }
}
