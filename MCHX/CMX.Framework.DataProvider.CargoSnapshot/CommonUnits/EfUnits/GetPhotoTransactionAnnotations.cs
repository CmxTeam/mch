﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetPhotoTransactionAnnotations : CargoSnapshotEfDatabaseConnector<IEnumerable<TransactionPhotosAnnotationModel>>
    {
        private readonly long _photoTransactionId;
        public GetPhotoTransactionAnnotations(long photoTransactionId = 0)
        {
            _photoTransactionId = photoTransactionId;
        }
        public override IEnumerable<TransactionPhotosAnnotationModel> Execute()
        {
            var dbTranAnnotations = Context.TransactionPhotosAnnotations.Include("TransactionPhoto").Include("Annotation").Where(s => (_photoTransactionId == 0 || s.TransactionPhotoId == _photoTransactionId)).ToList();
            return dbTranAnnotations.Select(s => new TransactionPhotosAnnotationModel
            {
                Id = s.Id,
                TransactionPhoto = new TransactionPhotoModel
                {
                    Id = s.TransactionPhotoId
                },
                Annotation = s.Annotation.ConvertTo()
            });
        }
    }
}
