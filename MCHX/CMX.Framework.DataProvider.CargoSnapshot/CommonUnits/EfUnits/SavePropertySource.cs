﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SavePropertySource : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly PorpertySourceModel _source;
        public SavePropertySource(PorpertySourceModel source)
        {
            _source = source ?? new PorpertySourceModel();
        }
        public override bool Execute()
        {
            if (_source.PropertySourceType.Id == 0)
                AddNewSource();
            Context.SaveChanges();
            return true;
        }
        private void AddNewSource()
        {
            PropertySourceType sourceType = new PropertySourceType(){Name = _source.PropertySourceType.Name};
            if(_source.Source == null) return;
            foreach (var src in _source.Source)
            {
                PropertySource propSource = new PropertySource() { Value = src.Value};

                if (src.Attributes != null)
                {
                    foreach (var detail in src.Attributes)
                    {
                        propSource.PropertySourceDetails.Add(new PropertySourceDetail()
                        {
                            Name = detail.Name,
                            Type = detail.Type,
                            Value = detail.Value
                        });
                    }
                    sourceType.PropertySources.Add(propSource);
                }
            }
            Context.PropertySourceTypes.Add(sourceType);
        }
    }
}
