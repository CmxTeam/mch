﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeleteControl : CargoSnapshotEfDatabaseConnector<bool>
    {
        private long _controlId;
        public DeleteControl(long controlId)
        {
            _controlId = controlId;
        }
        public override bool Execute()
        {
            var control = Context.Properties.Single(s => s.Id == _controlId);
            Context.Properties.Remove(control);
            Context.SaveChanges();
            return true;
        }
    }
}
