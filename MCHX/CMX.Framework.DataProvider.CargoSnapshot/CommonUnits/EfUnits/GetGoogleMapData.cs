﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.Dictionary;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetGoogleMapData : CargoSnapshotEfDatabaseConnector<IEnumerable<GoogleMapModel>>
    {
        private readonly ReferenceTypeEnum _refType;
        private readonly string _shipmentReference;
        private readonly long _userId;
        public GetGoogleMapData(long userId,string shipmentReference,ReferenceTypeEnum refType)
        {
            _refType = refType;
            this._userId = userId;
            this._shipmentReference = shipmentReference;            
        }
        public override IEnumerable<GoogleMapModel> Execute()
        {
            var transactionId =
                Context.Values.First(s => s.Property.ReferenceTypeId == (int)_refType && (s.Property.Name == Controls.Reference && s.Value1 == _shipmentReference)).TransactionId;
            return Context.TransactionPhotos.Include("UserProfile").Include("PropertySource")
                .Include("PropertySource.PropertySourceDetails")
                .Where(p => p.TransactionId == transactionId && (_userId == 0 || p.UserProfileId == _userId)).ToList()
                .Select(s => new GoogleMapModel
                {
                    DateTaken = s.RecDate,
                    Latitude = s.PropertySource.PropertySourceDetails.First(details => details.Name == "Latitude").Value.TryParse<decimal>(),
                    Longitude = s.PropertySource.PropertySourceDetails.First(details => details.Name == "Longitude").Value.TryParse<decimal>(),
                    PictureTakenLocation = s.PropertySource.PropertySourceDetails.First(details => details.Name == "Name").Value,
                    TakenByUser = s.UserProfile.FirstName
                }).ToList();
        }
    }
}
