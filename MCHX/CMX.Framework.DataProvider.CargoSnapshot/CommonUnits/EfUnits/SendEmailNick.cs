﻿using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class SendEmailNick : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly UserProfileModel _userProfile;
        public SendEmailNick(UserProfileModel user)
        {
            _userProfile = user;
        }
        public override bool Execute()
        {
            EmailDetailsModel emailDetails = new EmailDetailsModel();
            emailDetails.Subject = "Forgot nick";
            emailDetails.Body = _userProfile.FirstName + "! You nick is " + _userProfile.UserName;
            emailDetails.Recipients.Add(_userProfile.Email);
            EmailSender.Send(emailDetails);
            return true;
        }
    }
}
