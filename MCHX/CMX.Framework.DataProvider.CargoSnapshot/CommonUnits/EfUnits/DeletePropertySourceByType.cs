﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeletePropertySourceByType : CargoSnapshotEfDatabaseConnector<bool>
    {
        private long _propertySourceTypeId;
        public DeletePropertySourceByType(long propertySourceTypeId)
        {
            _propertySourceTypeId = propertySourceTypeId;
        }
        public override bool Execute()
        {
            var propSource = Context.PropertySourceTypes.Single(s => s.Id == _propertySourceTypeId);
            Context.PropertySourceTypes.Remove(propSource);
            Context.SaveChanges();
            return true;
        }
    }
}
