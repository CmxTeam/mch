﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetUserEmailTemplates : CargoSnapshotEfDatabaseConnector<IEnumerable<EmailTemplateModel>>
    {
        private readonly long _userId;
        public GetUserEmailTemplates(long userId)
        {
            _userId = userId;
        }
        public override IEnumerable<EmailTemplateModel> Execute()
        {
            var dbEmailTemplates = Context.EmailTemplates.Include("UserProfile").Include("UserProfile.Setting")
                                    .Where(e => e.UserProfileId == _userId || e.Shared)
                                    .ToList();
            return dbEmailTemplates.Select(s => s.ConvertTo());
        }
    }
}
