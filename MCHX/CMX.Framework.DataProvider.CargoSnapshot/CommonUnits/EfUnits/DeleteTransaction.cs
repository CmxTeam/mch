﻿using System;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using System.Linq;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeleteTransaction : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly long _transactionId;
        public DeleteTransaction(long transactionId)
        {
            _transactionId = transactionId;
        }
        public override bool Execute()
        {
            return MakeDatabaseTransaction(() => CoreExecute());
        }
        public bool CoreExecute()
        {
            if (_transactionId == 0) return false;          
            var dbTransaction = Context.Transactions.Single(s => s.Id == _transactionId);
            //Existance of cascade on delete property on foreign keys
            var photosForDelete = dbTransaction.TransactionPhotos.Select(s => s.Path).ToList().Select(a => FilePathHelper.SnapshotPhotoFullPath + a);
            Context.Transactions.Remove(dbTransaction);
            photosForDelete.ForEach(photo => FileHelper.DeleteFile(photo));
            Context.SaveChanges();
            return true;
        }
    }
}
