﻿using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.Helpers;
using System.Linq;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeletePhoto : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly long _photoId;
        public DeletePhoto(long photoId)
        {
            _photoId = photoId;
        }
        public override bool Execute()
        {
            return MakeDatabaseTransaction(() => CoreExecute());
        }
        public bool CoreExecute()
        {
            if (_photoId == 0) return false;
            var dbPhoto = Context.TransactionPhotos.Single(s => s.Id == _photoId);
            var forDelete = FilePathHelper.SnapshotPhotoFullPath + dbPhoto.Path;
            Context.TransactionPhotos.Remove(dbPhoto);
            Context.SaveChanges();
            FileHelper.DeleteFile(forDelete);
            return true;
        }
    }
}
