﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.Dictionary;
using System.Dynamic;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetTransactionPhotos : CargoSnapshotEfDatabaseConnector<IEnumerable<object>>
    {
        private readonly long _applicationId;
        private readonly long _photoId;
        private readonly string _shipmentRef;
        public GetTransactionPhotos(long photoId = 0, string shipment = null,long applicationId = 0)
        {
            _photoId = photoId;
            _shipmentRef = shipment;
            _applicationId = applicationId;
        }
        public override IEnumerable<object> Execute()
        {
            var dbPhotos = Context.TransactionPhotos.Include("Transaction").Include("UserProfile")
                                                    .Include("Transaction.Values")
                                                    .Include("Transaction.Values.PropertySource")
                                                    .Include("Transaction.Values.PropertySource.PropertySourceDetails")
                                                    .Include("Transaction.Values.Property")
                                                    .Include("TransactionPhotosAnnotations")
                                                    .Include("TransactionPhotosAnnotations.Annotation")
                                                    .Include("TransactionPhotosAnnotations.TransactionPhoto")

                .Where(s => (_applicationId == 0 || s.Transaction.ApplicationId == _applicationId)&&
                            (_photoId == 0 || s.Id == _photoId) &&
                            (_shipmentRef == null || s.Transaction.Values.Any(a => a.Property.Name == Controls.Reference && a.Value1 == _shipmentRef))
                ).ToList();
            return dbPhotos.Select(transactionPhoto =>
            {
                var tranPhotoModel = transactionPhoto.ConvertTo();

                var dynLocation = new ExpandoObject() as IDictionary<string, object>;

                dynLocation.Add("Id", transactionPhoto.PropertySource.Id);
                dynLocation.Add("ShortName", transactionPhoto.PropertySource.Value);
                foreach (var photoValue in transactionPhoto.PropertySource.PropertySourceDetails)
                    dynLocation.Add(photoValue.Name, photoValue.Value);
                tranPhotoModel.Location = dynLocation;

                tranPhotoModel.PhotoAnnotations = transactionPhoto.TransactionPhotosAnnotations.Select(trnAnnot => trnAnnot.ConvertTo());
                tranPhotoModel.AnnotationNames = string.Join(",", tranPhotoModel.PhotoAnnotations.Select(s => s.Annotation.Name));
                return tranPhotoModel;
            });
           
        }
    }
}
