﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetPropertySourceTypes : CargoSnapshotEfDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public override IEnumerable<IdNameEntity> Execute()
        {
            return Context.PropertySourceTypes.Select(s => new IdNameEntity() {Id = s.Id, Name = s.Name}).ToList();
        }
    }
}
