﻿using System.Linq;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class DeleteAnnotation : CargoSnapshotEfDatabaseConnector<bool>
    {
        private readonly long _id;
        public DeleteAnnotation(long id)
        {
            _id = id;
        }
        public override bool Execute()
        {
            return MakeDatabaseTransaction(() => CoreExecute());
        }
        public bool CoreExecute()
        {
            if (_id == 0) return false;
            var dbAnnotation = Context.Annotations.Single(s => s.Id == _id);
            Context.Annotations.Remove(dbAnnotation);
            Context.SaveChanges();
            FileHelper.DeleteFile(FilePathHelper.AnnotationImageFullPath + dbAnnotation.AnnotationImagePath);
            return true;
        }
    }
}
