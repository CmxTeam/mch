﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoSnapshot.Helpers;

namespace CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits
{
    public class GetUserProfiles : CargoSnapshotEfDatabaseConnector<IEnumerable<CargoSnapshotUserProfileModel>>
    {
        private readonly string _filter;
        private readonly long _userProfileId;
        public GetUserProfiles(long userProfileId = 0,string filter = null)
        {
            _filter = filter;
            _userProfileId = userProfileId;
        }
        public override IEnumerable<CargoSnapshotUserProfileModel> Execute()
        {
            var dbUserProfiles = Context.UserProfiles.Include("EmailTemplate").Include("Setting")
                                .Where(u => (_userProfileId == 0 || u.Id == _userProfileId)
                                            && (_filter == null || u.FirstName.Contains(_filter)
                                                || u.LastName.Contains(_filter)
                                                || u.MiddleName.Contains(_filter)
                                                || u.UserId.Contains(_filter)))
                                .ToList();
            return dbUserProfiles.Select(s => s.ConvertTo());
        }
    }
}
