﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class EmailTemplateModel : IdEntity
    {
        public string ConsigneeName { get; set; }
        public string SendTo { get; set; }       
        public string Subject { get; set; }
        public string MsgBody { get; set; }
        public bool UseEmailSender { get; set; }
        public bool DeliveryVPOC { get; set; }       
        public string RecepientEmail { get; set; }        
        public string EmailNotification { get; set; }       
        public string TemplateTitle { get; set; }
        public bool Shared { get; set; }
        public UserProfileModel UserProfile { get; set; }
    }
}
