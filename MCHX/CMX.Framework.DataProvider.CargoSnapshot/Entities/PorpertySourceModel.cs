﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class PorpertySourceModel
    {
        public PorpertySourceTypeModel PropertySourceType { get; set; }
        public List<PorpertySourceDetailsModel> Source { get; set; }
    }
}
