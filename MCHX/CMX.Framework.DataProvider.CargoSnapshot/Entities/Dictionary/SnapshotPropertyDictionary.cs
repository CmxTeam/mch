﻿namespace CMX.Framework.DataProvider.CargoSnapshot.Entities.Dictionary
{
    public static class Controls
    {
        public static string CurrentDate = "RecDate";
        public static string DateFrom = "DateFrom";
        public static string DateTo = "DateTo";
        public static string Reference = "Reference";
        public static string PhotoLocation = "PhotoLocation";
    }

    public enum PropertySourceTypesEnum
    {
        Port = 1,
        Carrier = 2
    }
}
