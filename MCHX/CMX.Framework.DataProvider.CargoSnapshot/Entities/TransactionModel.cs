﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class TransactionModel : IdEntity
    {
        public long AppicationId { get; set; }
        public DateTime RecDate { get; set; }
        public string ShipmentReference { get; set; }  
        public ReferenceTypeEnum ReferenceType { get; set; }
        public List<DynamicPropertyModel> Properties { get; set; }
        public IEnumerable<TransactionPhotoModel> TransactionPhotos { get; set; }
    }
}
