﻿using CMX.Framework.DataProvider.Entities.Common;
using System;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class TransactionPhotosAnnotationModel : IdEntity
    {
        public DateTime RecDate { get; set; }
        public decimal? XPosition { get; set; }
        public decimal? YPosition { get; set; }
        public AnnotationModel Annotation { get; set; }
        public TransactionPhotoModel TransactionPhoto { get; set; }
    }
}
