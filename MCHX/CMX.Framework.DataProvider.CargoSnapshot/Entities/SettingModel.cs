﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class SettingModel : IdEntity
    {
        public string Logo { get; set; }
        public string ProductName { get; set; }
        public bool? ShowHeader { get; set; } 
    }
}
