﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class DynamicPropertyModel : IdEntity
    {
        public string Value { get; set; }
        public long? Source { get; set; }
    }
}
