﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class TransactionPhotoModel : IdEntity
    {
        public string Image { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public string AnnotationNames { get; set; }
        public long LocationId { get; set; }
        public object Location { get; set; }
        public DateTime RecDate { get; set; }
        public TransactionModel Transaction { get; set; }
        public CargoSnapshotUserProfileModel UserProfile { get; set; }
        public IEnumerable<TransactionPhotosAnnotationModel> PhotoAnnotations { get; set; }
    }
}
