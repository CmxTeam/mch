﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
        public class CarrierModel : IdEntity
        {
            public string CarrierCode { get; set; }
            public string CarrierName { get; set; }
            public string Carrier3Code { get; set; }
            public MOTModel CarrierMOT { get; set; }
            public string CarrierLogo { get; set; }
        }
}
