﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class AnnotationModel : IdNameEntity
    {
        public string AnnotationImage { get; set; }
        public string Description { get; set; }
    }
}
