﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class ControlModel : IdNameEntity
    {
        public long ReferenceTypeId { get; set; }
        public string Type { get; set; }
        public string HtmlType { get; set; }
        public bool IsFilter { get; set; }
        public bool IsDetails { get; set; }
        public bool IsMandatory { get; set; }
        public string DisplayName { get; set; }
        public long? PropertySourceTypeId { get; set; }
    }
}
