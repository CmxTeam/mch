﻿using System;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel
{
    public class TransactionFilterSearchModel
    {
        public long ApplicationId { get; set; }
        public long? UserId { get; set; }
        public long? TransactionId { get; set; }
        public long? PhotoLocationId { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public List<long> Annotations { get; set; }
        public ReferenceTypeEnum ReferenceType { get; set; }
        public List<DynamicPropertyModel> Properties { get; set; }
    }
}
