﻿using System;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel
{
    public class GoogleMapModel
    {  
        public DateTime DateTaken {get;set;}
        public string TakenByUser {get;set;}
        public decimal Longitude {get; set;}
        public decimal Latitude {get; set;}
        public string PictureTakenLocation { get; set; }        
    }
}
