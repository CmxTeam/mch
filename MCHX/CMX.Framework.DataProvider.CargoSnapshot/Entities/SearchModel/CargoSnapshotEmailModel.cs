﻿using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel
{
    public class CargoSnapshotEmailModel : EmailDetailsModel
    {
        public List<long> AttachmentFileIds { get; set; }
    }
}
