﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoSnapshot.Entities
{
    public class CargoSnapshotUserProfileModel : UserProfileModel
    {
        public string ImageUpload { get; set; }
        public string ImageDownload { get; set; }
        public EmailTemplateModel DefaultTemplate { get; set; }
        public SettingModel Setting { get; set; }       
    }
}
