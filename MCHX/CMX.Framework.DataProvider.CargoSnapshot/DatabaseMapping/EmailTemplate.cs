namespace CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EmailTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EmailTemplate()
        {
            UserProfiles = new HashSet<UserProfile>();
        }

        public long Id { get; set; }

        [StringLength(100)]
        public string ConsigneeName { get; set; }

        public string SentTo { get; set; }

        [StringLength(100)]
        public string Subject { get; set; }

        public string MsgBody { get; set; }

        public bool UseEmailSender { get; set; }

        public bool DeliveryVPOC { get; set; }

        [StringLength(100)]
        public string RecepienEmail { get; set; }

        [StringLength(100)]
        public string EmailNotification { get; set; }

        [StringLength(100)]
        public string TemplateTitle { get; set; }

        public bool Shared { get; set; }

        public long? UserProfileId { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
