namespace CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CargoSnapshotDbContext : DbContext
    {
        public CargoSnapshotDbContext()
            : base("name=CargoSnapshotDbContext")
        {
        }

        public virtual DbSet<Annotation> Annotations { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<PropertySourceDetail> PropertySourceDetails { get; set; }
        public virtual DbSet<PropertySource> PropertySources { get; set; }
        public virtual DbSet<PropertySourceType> PropertySourceTypes { get; set; }
        public virtual DbSet<ReferenceType> ReferenceTypes { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<TransactionPhoto> TransactionPhotos { get; set; }
        public virtual DbSet<TransactionPhotosAnnotation> TransactionPhotosAnnotations { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<Value> Values { get; set; }
        public virtual DbSet<UserProfilesLocation> UserProfilesLocations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Annotation>()
                .HasMany(e => e.TransactionPhotosAnnotations)
                .WithRequired(e => e.Annotation)
                .HasForeignKey(e => e.AnotationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EmailTemplate>()
                .HasMany(e => e.UserProfiles)
                .WithOptional(e => e.EmailTemplate)
                .HasForeignKey(e => e.DefaultTemplateId);

            modelBuilder.Entity<PropertySource>()
                .HasMany(e => e.TransactionPhotos)
                .WithRequired(e => e.PropertySource)
                .HasForeignKey(e => e.LocationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PropertySource>()
                .HasMany(e => e.UserProfilesLocations)
                .WithRequired(e => e.PropertySource)
                .HasForeignKey(e => e.LocationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ReferenceType>()
                .HasMany(e => e.Properties)
                .WithRequired(e => e.ReferenceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ReferenceType>()
                .HasMany(e => e.Transactions)
                .WithRequired(e => e.ReferenceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransactionPhoto>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<TransactionPhoto>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<TransactionPhotosAnnotation>()
                .Property(e => e.XPosition)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TransactionPhotosAnnotation>()
                .Property(e => e.YPosition)
                .HasPrecision(10, 2);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.EmailTemplates)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserProfileId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TransactionPhotos)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserProfilesLocations)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete(false);
        }
    }
}
