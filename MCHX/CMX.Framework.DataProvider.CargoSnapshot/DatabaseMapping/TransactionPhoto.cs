namespace CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransactionPhotos")]
    public partial class TransactionPhoto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TransactionPhoto()
        {
            TransactionPhotosAnnotations = new HashSet<TransactionPhotosAnnotation>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(1000)]
        public string Path { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public long TransactionId { get; set; }

        public long UserProfileId { get; set; }

        public long LocationId { get; set; }

        public virtual PropertySource PropertySource { get; set; }

        public virtual Transaction Transaction { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransactionPhotosAnnotation> TransactionPhotosAnnotations { get; set; }
    }
}
