namespace CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TransactionPhotosAnnotation
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long TransactionPhotoId { get; set; }

        public long AnotationId { get; set; }

        public decimal? XPosition { get; set; }

        public decimal? YPosition { get; set; }

        public virtual Annotation Annotation { get; set; }

        public virtual TransactionPhoto TransactionPhoto { get; set; }
    }
}
