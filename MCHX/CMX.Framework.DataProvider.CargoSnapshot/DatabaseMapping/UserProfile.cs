namespace CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserProfile()
        {
            EmailTemplates = new HashSet<EmailTemplate>();
            TransactionPhotos = new HashSet<TransactionPhoto>();
            UserProfilesLocations = new HashSet<UserProfilesLocation>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public long? ShellUserId { get; set; }

        [StringLength(256)]
        public string UserId { get; set; }

        [StringLength(5)]
        public string Title { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string JobTitle { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(25)]
        public string Mobile { get; set; }

        [StringLength(25)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public string PhotoPath { get; set; }

        public int? IsSupervisor { get; set; }

        public int? IsTSACertified { get; set; }

        public long? DefaultTemplateId { get; set; }

        public long? SettingId { get; set; }

        public string ImageUploadPath { get; set; }

        public string ImageDownloadPath { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }

        public virtual EmailTemplate EmailTemplate { get; set; }

        public virtual Setting Setting { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransactionPhoto> TransactionPhotos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserProfilesLocation> UserProfilesLocations { get; set; }
    }
}
