namespace CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Value
    {
        public long Id { get; set; }

        public long TransactionId { get; set; }

        public long PropertyId { get; set; }

        [Column("Value")]
        public string Value1 { get; set; }

        public long? PropertySourceId { get; set; }

        public virtual Property Property { get; set; }

        public virtual PropertySource PropertySource { get; set; }

        public virtual Transaction Transaction { get; set; }
    }
}
