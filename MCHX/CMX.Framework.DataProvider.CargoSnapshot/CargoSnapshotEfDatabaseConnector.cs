﻿using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoSnapshot.DatabaseMapping;

namespace CMX.Framework.DataProvider.CargoSnapshot
{
    public abstract class CargoSnapshotEfDatabaseConnector<T> : EfDatabaseConnector<T, CargoSnapshotDbContext>
    {
    }
}
