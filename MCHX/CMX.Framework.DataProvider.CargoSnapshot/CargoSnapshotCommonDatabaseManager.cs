﻿using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;
using CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.EfUnits;
using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel;
using CMX.Framework.DataProvider.CargoSnapshot.CommonUnits.AdoUnits;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.Dictionary;

namespace CMX.Framework.DataProvider.CargoSnapshot
{
    public class CargoSnapshotCommonDatabaseManager
    {
        private ConnectorExecutor _connectorExecutor = new ConnectorExecutor();

        public IEnumerable<AnnotationModel> GetAnnotations(long id = 0)
        {
            return _connectorExecutor.Execute(() => new GetAnnotations(id));
        }

        public bool DeleteAnnotation(IdEntity id)
        {
            return _connectorExecutor.Execute(() => new DeleteAnnotation(id.Id));
        }

        public bool SaveAnnotation(AnnotationModel annotation)
        {
            return _connectorExecutor.Execute(() => new SaveAnnotation(annotation));
        }

        public IEnumerable<CargoSnapshotUserProfileModel> GetUserProfiles(long userProfileId = 0, string filter = null)
        {
            return _connectorExecutor.Execute(() => new GetUserProfiles(userProfileId, filter));
        }

        public bool SaveUserProfile(CargoSnapshotUserProfileModel userProfileModel)
        {
            return _connectorExecutor.Execute(() => new SaveUserProfile(userProfileModel));
        }

        public IEnumerable<GoogleMapModel> GetGoogleMapData(long userId, string shipmentReference,
            ReferenceTypeEnum refType)
        {
            return _connectorExecutor.Execute(() => new GetGoogleMapData(userId, shipmentReference, refType));
        }

        public bool DeleteEmailTemplate(IdEntity id)
        {
            return _connectorExecutor.Execute(() => new DeleteEmailTemplate(id.Id));
        }

        public IEnumerable<EmailTemplateModel> GetEmailTemplates(long emailTemplateId = 0)
        {
            return _connectorExecutor.Execute(() => new GetEmailTemplates(emailTemplateId));
        }

        public IEnumerable<EmailTemplateModel> GetUserEmailTemplates(long userId = 0)
        {
            return _connectorExecutor.Execute(() => new GetUserEmailTemplates(userId));
        }

        public EmailTemplateModel GetUserDefaultEmailTemplate(long userId = 0)
        {
            return _connectorExecutor.Execute(() => new GetUserDefaultEmailTemplate(userId));
        }

        public bool SaveEmailTemplate(long userId, EmailTemplateModel emailTemplateModel)
        {
            return _connectorExecutor.Execute(() => new SaveEmailTemplate(userId, emailTemplateModel));
        }

        public bool DeleteTransaction(IdEntity transactionId)
        {
            return _connectorExecutor.Execute(() => new DeleteTransaction(transactionId.Id));
        }

        public bool SaveTransaction(TransactionModel transactionModel)
        {
            return _connectorExecutor.Execute(() => new SaveTransaction(transactionModel));
        }

        public IEnumerable<object> GetTransactions(long userId, TransactionFilterSearchModel filter)
        {
            return _connectorExecutor.Execute(() => new GetTransactions(userId, filter));
        }

        public bool DeletePhoto(IdEntity photoId)
        {
            return _connectorExecutor.Execute(() => new DeletePhoto(photoId.Id));
        }

        public IEnumerable<object> GetTransactionPhotos(long photoId = 0, string shipmentRef = null,long applicationId = 0)
        {
            return _connectorExecutor.Execute(() => new GetTransactionPhotos(photoId, shipmentRef, applicationId));
        }

        public bool SendEmail(CargoSnapshotEmailModel email)
        {
            return _connectorExecutor.Execute(() => new CargoSnapShotEmailSender(email));
        }

        public IEnumerable<TransactionPhotosAnnotationModel> GetTransactionAnnotations(long transactionId = 0)
        {
            return _connectorExecutor.Execute(() => new GetPhotoTransactionAnnotations(transactionId));
        }

        public bool DeleteSetting(IdEntity settingId)
        {
            return _connectorExecutor.Execute(() => new DeleteSetting(settingId.Id));
        }

        public IEnumerable<SettingModel> GetSettings(long settingId = 0)
        {
            return _connectorExecutor.Execute(() => new GetSettings(settingId));
        }

        public bool SaveSetting(SettingModel setting)
        {
            return _connectorExecutor.Execute(() => new SaveSetting(setting));
        }

        public IEnumerable<object> GetPropertySource(long propertySourceTypeId)
        {
            return _connectorExecutor.Execute(() => new GetPropertySource(propertySourceTypeId));
        }

        public bool SavePropertySource(PorpertySourceModel source)
        {
            return _connectorExecutor.Execute(() => new SavePropertySource(source));
        }

        public IEnumerable<IdNameEntity> GetReferenceTypes()
        {
            return _connectorExecutor.Execute(() => new GetReferenceTypes());
        }

        public IEnumerable<IdNameEntity> GetPropertySourceTypes()
        {
            return _connectorExecutor.Execute(() => new GetPropertySourceTypes());
        }

        public bool SaveControl(ControlModel control)
        {
            return _connectorExecutor.Execute(() => new SaveControl(control));
        }

        public IEnumerable<ControlModel> GetControls(long referenceTypeId, bool? isFilter = null, bool? isDetails = null)
        {
            return _connectorExecutor.Execute(() => new GetControls(referenceTypeId, isFilter, isDetails));
        }

        public bool DeleteControl(IdEntity control)
        {
            return _connectorExecutor.Execute(() => new DeleteControl(control.Id));
        }

        public bool DeletePropertySourceByType(IdEntity propertySource)
        {
            return _connectorExecutor.Execute(() => new DeletePropertySourceByType(propertySource.Id));
        }

        public IEnumerable<object> GetLocations()
        {
            return _connectorExecutor.Execute(() => new GetPropertySource((int)PropertySourceTypesEnum.Port));
        }
    }
}
