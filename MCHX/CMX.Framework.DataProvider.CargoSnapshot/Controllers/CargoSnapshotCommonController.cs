﻿using CMX.Framework.DataProvider.CargoSnapshot.Entities;
using CMX.Framework.DataProvider.CargoSnapshot.Entities.SearchModel;
using CMX.Framework.DataProvider.Controllers;
using CMX.Framework.DataProvider.Entities.Common;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json.Linq;

namespace CMX.Framework.DataProvider.CargoSnapshot.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CargoSnapshotCommonController : CommonController
    {
        private readonly CargoSnapshotCommonDatabaseManager _manager = new CargoSnapshotCommonDatabaseManager();
        
        [HttpGet]
        public DataContainer<IEnumerable<AnnotationModel>> GetAnnotations(long annotationId = 0)
        {
            return MakeTransactional(() => _manager.GetAnnotations(annotationId));
        }

        [HttpPost]
        public DataContainer<bool> SaveAnnotation(AnnotationModel annotation)
        {
            return MakeTransactional(() => _manager.SaveAnnotation(annotation));
        }

        [HttpPost]
        public DataContainer<bool> DeleteAnnotation(IdEntity annotationId)
        {
            return MakeTransactional(() => _manager.DeleteAnnotation(annotationId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<CargoSnapshotUserProfileModel>> GetUserProfiles(long userProfileId = 0, string filter = null)
        {
            return MakeTransactional(() => _manager.GetUserProfiles(userProfileId, filter));
        }

        [HttpPost]
        public DataContainer<bool> SaveUserProfile(CargoSnapshotUserProfileModel userProfile)
        {
            return MakeTransactional(() => _manager.SaveUserProfile(userProfile));
        }
        [HttpGet]
        public DataContainer<IEnumerable<GoogleMapModel>> GetGoogleMapData(string shipmentReference,ReferenceTypeEnum refType)
        {
            return MakeTransactional(() => _manager.GetGoogleMapData(ApplicationUser.UserId, shipmentReference,refType));
        }
       
        [HttpPost]
        public DataContainer<bool> DeleteEmailTemplate(IdEntity templateId)
        {
            return MakeTransactional(() => _manager.DeleteEmailTemplate(templateId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<EmailTemplateModel>> GetEmailTemplates(long templateId = 0)
        {
            return MakeTransactional(() => _manager.GetEmailTemplates(templateId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<EmailTemplateModel>> GetUserEmailTemplates()
        {
            return MakeTransactional(() => _manager.GetUserEmailTemplates(ApplicationUser.UserId));
        }

        [HttpGet]
        public DataContainer<EmailTemplateModel> GetUserDefaultEmailTemplate(long userId = 0)
        {
            return MakeTransactional(() => _manager.GetUserDefaultEmailTemplate(userId == 0 ? ApplicationUser.UserId : userId));
        }

        [HttpPost]
        public DataContainer<bool> SaveEmailTemplate(EmailTemplateModel emailTemplate)
        {
            return MakeTransactional(() => _manager.SaveEmailTemplate(ApplicationUser.UserId, emailTemplate));
        }

        [HttpPost]
        public DataContainer<bool> SaveTransaction(TransactionModel transactionModel)
        {
            return MakeTransactional(() => _manager.SaveTransaction(transactionModel));
        }
        [HttpPost]
        public DataContainer<bool> SavePhoto(TransactionModel transactionModel)
        {
            return MakeTransactional(() => _manager.SaveTransaction(transactionModel));
        }

        [HttpPost]
        public DataContainer<bool> DeleteTransaction(IdEntity transactionId)
        {
            return MakeTransactional(() => _manager.DeleteTransaction(transactionId));
        }
        [HttpGet]
        public DataContainer<IEnumerable<object>> GetTransactions([FromUri]TransactionFilterSearchModel filter)
        {
            return MakeTransactional(() => _manager.GetTransactions(ApplicationUser.UserId, filter));
        }
        
        [HttpPost]
        public DataContainer<bool> DeletePhoto(IdEntity photoId)
        {
            return MakeTransactional(() => _manager.DeletePhoto(photoId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<object>> GetTransactionPhotos(long photoId = 0, string shipmentRef = null,long applicationId = 0)
        {
            return MakeTransactional(() => _manager.GetTransactionPhotos(photoId, shipmentRef, applicationId));
        }
        [HttpGet]
        public DataContainer<IEnumerable<object>> GetPhotos(long photoId = 0, string shipmentRef = null, long applicationId = 0)
        {
            return MakeTransactional(() => _manager.GetTransactionPhotos(photoId, shipmentRef, applicationId));
        }

        [HttpPost]
        public DataContainer<bool> SendEmail(CargoSnapshotEmailModel email)
        {
            return MakeTransactional(() => _manager.SendEmail(email));
        }

        [HttpGet]
        public DataContainer<IEnumerable<TransactionPhotosAnnotationModel>> GetTransactionAnnotations(long transactionId = 0)
        {
            return MakeTransactional(() => _manager.GetTransactionAnnotations(transactionId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<SettingModel>> GetSettings(long settingId = 0)
        {
            return MakeTransactional(() => _manager.GetSettings(settingId));
        }
        [HttpPost]
        public DataContainer<bool> SaveSetting(SettingModel setting)
        {
            return MakeTransactional(() => _manager.SaveSetting(setting));
        }

        [HttpPost]
        public DataContainer<bool> DeleteSetting(IdEntity settingId)
        {
            return MakeTransactional(() => _manager.DeleteSetting(settingId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<dynamic>> GetPropertySource(long propertySourceTypeId)
        {
            return MakeTransactional(() => _manager.GetPropertySource(propertySourceTypeId));
        }

        [HttpPost]
        public DataContainer<bool> SavePropertySource(PorpertySourceModel source)
        {
            return MakeTransactional(() => _manager.SavePropertySource(source));
        }

        [HttpGet]
        public DataContainer<IEnumerable<IdNameEntity>> GetReferenceTypes()
        {
            return MakeTransactional(() => _manager.GetReferenceTypes());
        }

        [HttpGet]
        public DataContainer<IEnumerable<IdNameEntity>> GetPropertySourceTypes()
        {
            return MakeTransactional(() => _manager.GetPropertySourceTypes());
        }

        [HttpGet]
        public DataContainer<IEnumerable<ControlModel>> GetControls(long referenceTypeId,bool? isFilter = null,bool? isDetails = null)
        {
            return MakeTransactional(() => _manager.GetControls(referenceTypeId, isFilter,isDetails));
        }

        [HttpPost]
        public DataContainer<bool> SaveControl(ControlModel control)
        {
            return MakeTransactional(() => _manager.SaveControl(control));
        }

        [HttpPost]
        public DataContainer<bool> DeleteControl(IdEntity control)
        {
            return MakeTransactional(() => _manager.DeleteControl(control));
        }

        [HttpPost]
        public DataContainer<bool> DeletePropertySourceByType(IdEntity source)
        {
            return MakeTransactional(() => _manager.DeletePropertySourceByType(source));
        }
        [HttpGet]
        public DataContainer<IEnumerable<object>> GetLocations()
        {
            return MakeTransactional(() => _manager.GetLocations());
        }
    }
}
