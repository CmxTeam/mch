﻿using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoSnapshot
{
    public abstract class CargoSnapshotAdoDatabaseConnector<T> : AdoNetDatabaseConnector<T>
    {
        public CargoSnapshotAdoDatabaseConnector()
        {

        }

        public CargoSnapshotAdoDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CargoSnapshotDb"].ConnectionString; }
        }
    }
}
