﻿var dischargeTransactionsFilter = function (accordion, onFilterClicked) {
    var $this = this;
    this.onFilterClicked = onFilterClicked;

    this._setCalendarDate = function (name, form, date) {
        var calendar = form.getCalendar(name);
        var calendarToKey = Object.keys(calendar.i)[0];
        calendar.i[calendarToKey].input.value = window.dhx4.date2str(date, '%m-%d-%Y');
        calendar.setDate(date);
    }

    this.getFilters = function () {
        var filterData = $this.form.getFormData();
        filterData.DateFrom = $this.form.getCalendar('DateFrom').getFormatedDate();
        filterData.DateTo = $this.form.getCalendar('DateTo').getFormatedDate();
        return filterData;
    }

    var init = function () {
        var dischargeTransactionsContainer = accordion.addItem('dischargeTransactionsFilterContainer', 'FILTER');
        var filterFormStructure = [

            { type: "input", name: "AwbSerialNumber", label: "AWB#:", position: "label-top", inputWidth: 210 },

            { type: "input", name: "HwbSerialNumber", label: "HWB#:", position: "label-top", inputWidth: 210 },

            { type: "combo", name: "DischargeTypeId", label: "Discharge Type:", position: "label-top", inputWidth: 210, filtering: true },

            { type: "combo", name: "CarrierId", label: "Carrier:", position: "label-top", inputWidth: 210, filtering: true },

            { type: "combo", name: "TruckingCompanyId", label: "Trucking Company:", position: "label-top", inputWidth: 210, filtering: true },

            { type: "combo", name: "DriverId", label: "Driver:", position: "label-top", inputWidth: 210, filtering: true },

            { type: "combo", name: "StatusId", label: "Status:", position: "label-top", inputWidth: 210, filtering: true },

            { type: "combo", name: "PeriodId", label: "Period:", position: "label-top", inputWidth: 210, filtering: true },

            {
                type: "calendar", name: "DateFrom", label: "From Date:", position: "label-top", dateFormat: "%m-%d-%Y", inputWidth: 210
            },

            {
                type: "calendar", name: "DateTo", label: "To Date:", position: "label-top", dateFormat: "%m-%d-%Y", inputWidth: 210
            },

            {
                type: "block", list: [
                    { type: "button", name: "filterButton",offsetLeft: 115, value: "Refresh" }
                ]
            }
        ];


        var filterPanelForm = dischargeTransactionsContainer.attachForm(filterFormStructure);
        filterPanelForm.enableLiveValidation(true);
        
        $this._setCalendarDate('DateFrom', filterPanelForm, new Date());
        $this._setCalendarDate('DateTo', filterPanelForm, new Date());

        filterPanelForm.getCombo('DischargeTypeId').loadExternal('/CargoDischarge/GetDischargeTypes', 'Id', 'Name');
        filterPanelForm.getCombo('CarrierId').loadExternal('/CargoDischarge/GetCarriers', 'Id', 'CarrierName');
        filterPanelForm.getCombo('TruckingCompanyId').loadExternal('/CargoDischarge/GetTruckingCompanies', 'Id', 'CarrierName');
        filterPanelForm.getCombo('DriverId').loadExternal('/CargoDischarge/GetDrivers', 'Id', 'FullName');
        filterPanelForm.getCombo('StatusId').loadExternal('/CargoDischarge/GetDischargeStatuses', 'Id', 'Name');

        var period = filterPanelForm.getCombo('PeriodId');
        period.loadExternal('/CargoDischarge/GetPeriods', 'Id', 'Name',function() {
            period.selectOption(0);
            if ($this.onFilterClicked) {
                $this.onFilterClicked($this.getFilters());
            }
        });

        period.attachEvent("onChange", function (value) {
            if (value) {
                var dateNow = new Date();
                var newDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - (value - 1));

                $this._setCalendarDate('DateFrom', filterPanelForm, newDate);
                $this._setCalendarDate('DateTo', filterPanelForm, dateNow);
            }
        });

        filterPanelForm.attachEvent("onButtonClick", function (name) {
            if (name === 'filterButton') {
                if ($this.onFilterClicked) {
                    $this.onFilterClicked($this.getFilters());
                }
            } 
        });

        this.form = filterPanelForm;
    }.call(this);

}