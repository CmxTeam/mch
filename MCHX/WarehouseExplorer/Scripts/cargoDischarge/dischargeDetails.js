﻿var dischargeDetails = function (cell) {
    var $this = this;
    var photoContainer;
    var photoIdContainer;
    
    this.getDetailsFormData = function () {
        var formData = {
            dischargeType: $this.detailsForm.getItemLabel("dischargeType"),
            shippingReference: $this.detailsForm.getItemLabel("shippingReference"),
            dischargeDate: $this.detailsForm.getItemLabel("dischargeDate"),
            truckingCompany: $this.detailsForm.getItemLabel("truckingCompany"),
            driver: $this.detailsForm.getItemLabel("driver"),
            releasedPieces: $this.detailsForm.getItemLabel("releasedPieces"),
            status: $this.detailsForm.getItemLabel("status"),
            photoPath: photoContainer.firstChild.src,
            photoIdPath: photoIdContainer.firstChild.src
        }
        return formData;
    }

    this.setDetailsFormData = function(data) {      
        $this.detailsForm.setItemLabel("dischargeType", data.dischargeType || "");
        $this.detailsForm.setItemLabel("shippingReference", data.shippingReference || "");
        $this.detailsForm.setItemLabel("dischargeDate", data.dischargeDate || "");
        $this.detailsForm.setItemLabel("truckingCompany", data.truckingCompany || "");
        $this.detailsForm.setItemLabel("driver", data.driver || "");
        $this.detailsForm.setItemLabel("releasedPieces", data.releasedPieces || "");
        $this.detailsForm.setItemLabel("status", data.status || "");

        photoContainer.innerHTML = '<img ' + (data.photoPath ? ' src="' + data.photoPath + '"' : '') + ' style="height: 100px; width: 100px;"/>';
        photoIdContainer.innerHTML = '<img ' + (data.photoIdPath ? ' src="' + data.photoIdPath + '"' : '') + ' style="height: 100px; width: 100px;"/>';
    }

    this.clearDischargeDetailsForm = function () {
        $this.detailsForm.setItemLabel("dischargeType",  '');
        $this.detailsForm.setItemLabel("shippingReference", '');
        $this.detailsForm.setItemLabel("dischargeDate", '');
        $this.detailsForm.setItemLabel("truckingCompany", '');
        $this.detailsForm.setItemLabel("driver", '');
        $this.detailsForm.setItemLabel("releasedPieces", '');
        $this.detailsForm.setItemLabel("status", '');

        photoContainer.innerHTML = '<img style="height: 100px; width: 100px;"/>';
        photoIdContainer.innerHTML = '<img style="height: 100px; width: 100px;"/>';
    }

    var init = function () {
        cell.setText('DISCHARGE DETAILS');

        var formData = [
            {
                type: "block",
                list: [
                    { type: "label", label: "Discharge Type:", labelHeight: 14 },
                    { type: "label", label: "Shipping Reference#:", labelHeight: 14 },
                    { type: "label", label: "Discharge Date:", labelHeight: 14 },
                    { type: "label", label: "Trucking Company:", labelHeight: 14 },
                    { type: "label", label: "Driver:", labelHeight: 14 },
                    { type: "label", label: "Released Pieces:", labelHeight: 14 },
                    { type: "label", label: "Status:", labelHeight: 14 }
                ]
            },
            { type: "newcolumn" },
            {
                type: "block",
                width: 200,
                className: 'detailsContainer',
                list: [
                    { type: "label", name: "dischargeType", labelHeight: 14 },
                    { type: "label", name: "shippingReference", labelHeight: 14 },
                    { type: "label", name: "dischargeDate", labelHeight: 14 },
                    { type: "label", name: "truckingCompany", labelHeight: 14 },
                    { type: "label", name: "driver", labelHeight: 14 },
                    { type: "label", name: "releasedPieces", labelHeight: 14 },
                    { type: "label", name: "status", labelHeight: 14 }
                ]
            },
            { type: "newcolumn" },
            {
                type: "fieldset",
                label: "Driver Details",
                width: 400,
                className: "label-bold",
                list: [
                    {
                        type: "block",
                        list: [
                            { type: "label", label: "Photo", labelHeight: 14 },
                            { type: "newcolumn" },
                            { type: "label", offsetLeft: 150, label: "Photo Id", labelHeight: 14 }
                        ]
                    },
                    {
                        type: "block",
                        list: [
                            { type: "container", name: "photoContainer", inputHeight: 150 },
                            { type: "newcolumn" },
                            { type: "container", offsetLeft: 89, name: "photoIdContainer", inputHeight: 150 }
                        ]
                    }
                ]
            }
        ];
        this.detailsForm = cell.attachForm(formData);

        photoContainer = this.detailsForm.getContainer("photoContainer");
        photoIdContainer = this.detailsForm.getContainer("photoIdContainer");

        photoContainer.innerHTML = '<img style="height: 100px; width: 100px;"/>';
        photoIdContainer.innerHTML = '<img style="height: 100px; width: 100px;"/>';

        $(photoContainer).on("dblclick", "img", function () {
            helpers.popupImage(this.src, 330, 310);
        });

        $(photoIdContainer).on("dblclick", "img", function () {
            helpers.popupImage(this.src, 330, 310);
        });


    }.call(this);
}