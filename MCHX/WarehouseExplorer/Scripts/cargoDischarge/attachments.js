﻿var attachments = function (cell, isWithRemoveButton, itemWidth) {
    var $this = this;
    var form = null;
    var dataView = null;
    this.allowEdit = true;
    this.onAttachmentsCountChange = null;

    this.getAttachments = function (attachmentId) {
        var attachment = null;
        dataView.unselectAll();
        if (attachmentId) {
            $(dataView.serialize()).each(function() {
                if (attachmentId == this.id) {
                    attachment = this;
                    return;
                }
            });
        } else
            attachment = dataView.serialize();
        return attachment;
    }

    this.addAttachment = function (data) {
        dataView.add({
            DbId: data.DbId || 0,
            FileType: data.FileType,
            FileName: data.FileName,
            Name: data.Name || null,
            Image: data.Image,
            DocumentTypeId: data.DocumentTypeId
        });
        if ($this.onAttachmentsCountChange) {
            $this.onAttachmentsCountChange();
        }
    }

    this.clearAndAddAttachments = function (data) {
        dataView.clearAll();
        dataView.parse(data, "json");
        dataView.unselectAll();
        $this.allowEdit = true;
        if ($this.onAttachmentsCountChange) {
            $this.onAttachmentsCountChange();
        }
    }

    var getScrollPosition = function() {
        var scrollableDiv = form.base[0].parentElement;
        return scrollableDiv.scrollTop;
    }

    var setScrollPosition = function (scrollPosition) {
        var scrollableDiv = form.base[0].parentElement;
        scrollableDiv.scrollTop = scrollPosition;
    }

    var init = function () {
        if (!cell.getText() || cell.getText() === "c" || cell.getText() === "a" || cell.getText() === "b")
            cell.setText('ATTACHMENTS');

        var formStr = [
            { type: "container", name: "attachmentsContainer"}
        ];

        form = cell.attachForm(formStr);

        dataView = new dhtmlXDataView({
            container: form.getContainer("attachmentsContainer"),
            type: {
                template: "<div style='height:100%;' dbId='#DbId#'>" +
                          "<div style='display:flex'>" +
                          "<div title='{common.getName()}' style='white-space: nowrap;overflow: hidden;width: 100%;text-overflow: ellipsis;'>{common.getName()}</div>" +
                          (isWithRemoveButton === true ? "<div class='deleteIco' ></div>" : "") +
                          "</div>" +
                          "<br/><br/>" +
                          "<img id='#id#' class='attachmentImage' style='height:calc(100% - 39px)' src='{common.getSource()}'/></div>",
                width: itemWidth ? itemWidth : 210,
                getName: function (obj) {
                    return obj.Name ? obj.Name : obj.FileName;
                },
                getSource: function(obj) {
                    if (obj.FileType === "Document") {
                        var imageName = "attachImage";
                        if (obj.FileName && obj.FileName !== "") {
                            var fileExtensions = ["bmp", "cs", "doc", "docx", "jpg", "pdf", "png", "resx", "rtf", "swf", "tif", "tiff", "xaml", "xls", "xlsx", "txt"];
                            var fullName = obj.FileName.toLowerCase();
                            var extension = fullName.substr((fullName.lastIndexOf('.') + 1));
                            if ($.inArray(extension, fileExtensions) > -1) 
                                imageName = extension;
                        }
                        return window.baseUrl + 'Content/fileExtensionsImages/' + imageName + '.png';
                    }
                    else 
                        return obj.Image;
                }
            },
            height: "auto"
        });

        var scrollPosition = 0;
        var removeItemId = null;
        dataView.attachEvent("onItemClick", function (id, ev, html) {
            scrollPosition = getScrollPosition();

            if ($this.allowEdit && ev.target.className === "deleteIco") {
                dataView.unselectAll();
                removeItemId = id;
            } 
        });

        dataView.attachEvent("onItemDblClick", function (id, ev, html) {
            if (ev.target.className === "attachmentImage" && $this.getAttachments(ev.target.id).FileType != "Document") {
                helpers.popupImage(ev.target.src);
            }
        });
        
        dataView.attachEvent("onSelectChange", function () {
            if (removeItemId) {
                dhtmlx.confirm({
                    type: "confirm",
                    text: "Do you want to delete this item?",
                    callback: function (result) {
                        if (result) {
                            dataView.remove(removeItemId);
                            if ($this.onAttachmentsCountChange) {
                                $this.onAttachmentsCountChange();
                            }
                        }
                        removeItemId = null;
                    }
                });
            }
        });

        dataView.attachEvent("onAfterRender", function () {
            setScrollPosition(scrollPosition);
        });
    }();
}