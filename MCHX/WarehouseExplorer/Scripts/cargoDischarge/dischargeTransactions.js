﻿var dischargeTransactions = function (cell) {
    var $this = this;
    this.onRowDoubleClick = null;
    this.onRowSelect = null;
    this.onActionClicked = null;
    this.onExportToExcel = null;
    
    this.filter = function(filterData) {
        cell.progressOn();
        $this.grid.clearAll();
        $this.grid.loadExternal('/CargoDischarge/GetDischargeByFilter', filterData, function () { cell.progressOff(); });
    }

    this.exportToExcel = function (exportExcelFilter) {
        var downloadUrl = "/CargoDischarge/DischargeTransactionsExcelExport";
        cell.progressOn();
        helpers.exportToExcel(downloadUrl, exportExcelFilter, null, function () { cell.progressOff(); });
    }

    var init = function () {
        cell.showView('dischargeTransactionsContainer');
        cell.setText('DISCHARGE TRANSACTIONS');
        var toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>' +
            '<item type="text" id="txtQuickFinder" text="QUICK FINDER:" title="" />' +
            '<item type="buttonInput" id="button_input_1" width="225" title="" />' +
            '<item type="separator" id="button_separator_1" />' +
            '<item type="buttonSelect" openAll="true" id="button_select_4"  text="Actions" title="" >' +
            '<item type="button" id="addNewDischarge" text="Add New Discharge" image="" /></item>' +
            '<item type="separator" id="button_separator_4" />' +
            '<item type="buttonSelect" openAll="true" id="button_select_export" text="EXPORT" title="" >' +
            '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
            '</item></toolbar>', function () { });

        toolbar.attachEvent('onClick', function(name) {
            if (name === 'btnExportExcel') {
                if ($this.onExportToExcel)
                    $this.onExportToExcel();
            } else if (name === 'addNewDischarge') {
                if ($this.onActionClicked)
                    $this.onActionClicked(name);
            }
        }.bind(this));

        toolbar.attachEvent('onEnter', function(name, value) {
            $this.grid.highlightCellsByValue(value);
        });

        this.grid = cell.attachGrid();
        this.grid.setIconsPath(dhtmlx.image_path);
        this.grid.setHeader(["Discharge Type", "Shipping Reference", "Discharge Date", "Trucking Company", "Driver", "Total Pieces Released", "Status"]);
        this.grid.setInitWidths('*,*,*,*,*,*,*');
        this.grid.setEditable(false);
        this.grid.init();
        this.grid.enableSmartRendering(true);

        this.grid.attachEvent("onRowSelect", function(rId, cInd) {
            if ($this.onRowSelect)
                $this.onRowSelect(rId);
        });

        this.grid.attachEvent("onRowDblClicked", function (rId, cInd) {
            if ($this.onRowDoubleClick)
                $this.onRowDoubleClick(rId);
        });
    }.call(this);
}
