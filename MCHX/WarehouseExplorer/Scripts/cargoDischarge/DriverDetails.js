﻿var driverDetails = function (cellA, driverDetails) {
    var $this = this;
    var filterPanelForm = null;
    var _newDriver, _newTruckingCompany;

    this.getData = function () {
        var data = filterPanelForm.getFormData();
        data.DriverPhoto = filterPanelForm.getItemValue('photoContainer');
        data.IDPhoto = filterPanelForm.getItemValue('photoIdContainer');
        data.PrimaryPhotoIdMatch = !!parseInt(data.PrimaryPhotoIdMatch);
        data.SecondaryPhotoIdMatch = !!parseInt(data.SecondaryPhotoIdMatch);
        return data;
    }

    this.clearDriverDetailsForm = function () {
        filterPanelForm.getCombo('TruckingCompanyId').unSelectOption();
        filterPanelForm.getCombo('CarrierDriverId').unSelectOption();
        filterPanelForm.getCombo('PrimaryIDTypeId').unSelectOption();
        filterPanelForm.getCombo('PrimaryPhotoIdMatch').unSelectOption();
        filterPanelForm.getCombo('SecondaryIdTypeId').unSelectOption();
        filterPanelForm.getCombo('SecondaryPhotoIdMatch').unSelectOption();
        filterPanelForm.getCombo('SecondaryIdTypeId').unSelectOption();
        filterPanelForm.setItemLabel('lbDriverLicenseState', "");
        filterPanelForm.setItemValue('LicenseNumber', "");
        filterPanelForm.setItemValue('photoContainer', "");
        filterPanelForm.setItemValue('photoIdContainer', "");
        filterPanelForm.setItemValue('Id', "0");
    }

    var init = function () {
        var toolbar = cellA.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>'
                                  + '<item type="button" id="btnNewDriver" text="New Driver" title="" />'
                                  + '<item id="sep2" type="separator"/>'
                                  + '<item type="button" id="btnnewTruckingCompany" text="New Trucking Company" title="" />'
                                  + '</toolbar>', function () { });

        toolbar.attachEvent('onClick', function (name, value) {
            if (name === 'btnNewDriver') {
                _newDriver = new newDriver();

                _newDriver.onAfterSave = function (data) {
                    filterPanelForm.getCombo('CarrierDriverId').clearAll();
                    filterPanelForm.getCombo('CarrierDriverId').loadExternal('/CargoDischarge/GetDrivers', 'Id', 'FullName');
                    _newDriver.window.close();
                }

            } else if (name === 'btnnewTruckingCompany') {
                _newTruckingCompany = new newTruckingCompany();

                _newTruckingCompany.onAfterSave = function (data) {
                    filterPanelForm.getCombo('TruckingCompanyId').clearAll();
                    filterPanelForm.getCombo('TruckingCompanyId').loadExternal('/CargoDischarge/GetTruckingCompanies', 'Id', 'CarrierName');
                    _newTruckingCompany.window.close();
                }
            }

        }.bind(this));


        var driverformStructure = [
                { type: "hidden", name: "Id", value: "0" },
                {
                    type: "fieldset", label: "1st ID Verification", list: [
                        {
                            type: "combo", name: "TruckingCompanyId", label: "Trucker:", position: "label-top", labelWidth: 70, inputWidth: 212, required: true, className: "req-asterisk",
                            validate: function (input) { return filterPanelForm.getCombo("TruckingCompanyId").getSelectedIndex() !== -1 }
                        },
                        {
                            type: "combo", name: "CarrierDriverId", label: "Driver:", position: "label-top", labelWidth: 70, inputWidth: 212, required: true, className: "req-asterisk",
                            validate: function (input) { return filterPanelForm.getCombo("CarrierDriverId").getSelectedIndex() !== -1 }
                        },

                        {
                            type: "input", name: "LicenseNumber", label: "License Number:", position: "label-top", labelWidth: 100, inputWidth: 212, required: true, className: "req-asterisk",
                            validate: function (input) { return input && input.trim() !== "" }
                        },

                        {
                            type: "block", blockOffset: 0, list: [
                                { type: "label", name: "lbDriverLicense", label: "Driver License State:", position: "label-left", labelWidth: 130 },
                                { type: "newcolumn" },
                                { type: "label", name: "lbDriverLicenseState", label: "", position: "label-left" }
                            ]
                        },

                        { type: "imageCapturer", name: "photoContainer", id: "photoContainer", controlHeight: '120px', buttonText: 'Capture', labelText: 'PHOTO:', labelWidth: '70px', imageHeight: '80px', imageWidth: '100px', required: true, className: "req-asterisk" },

                        { type: "imageCapturer", name: "photoIdContainer", id: "photoIdContainer", controlHeight: '120px', buttonText: 'Capture', labelText: 'PHOTO ID:', labelWidth: '70px', imageHeight: '80px', imageWidth: '100px', required: true, className: "req-asterisk" },

                        {
                            type: "combo", name: "PrimaryIDTypeId", label: "1st ID Type:", position: "label-top", labelWidth: 110, inputWidth: 212, required: true, className: "req-asterisk",
                            validate: function (input) { return filterPanelForm.getCombo("PrimaryIDTypeId").getSelectedIndex() !== -1 }
                        },
                        
                        {
                            type: "combo", name: "PrimaryPhotoIdMatch", label: "1st ID Matched?", position: "label-left", labelWidth: 93, inputWidth: 115, required: true, className: "req-asterisk",
                            validate: function (input) { return filterPanelForm.getCombo("PrimaryPhotoIdMatch").getSelectedIndex() !== -1 }
                        }
                    ]
                },
                {
                    type: "fieldset", name: "secondaryFieldset", disabled: true, label: "2nd ID Verification", list: [
                        {
                            type: "combo", name: "SecondaryIdTypeId", label: "2nd ID Type:", position: "label-top", labelWidth: 110, inputWidth: 212, required: true, className: "req-asterisk",
                            validate: function(input) { return filterPanelForm.getCombo("SecondaryIdTypeId").getSelectedIndex() !== -1 }
                        },
                        {
                            type: "combo", name: "SecondaryPhotoIdMatch", label: "2nd ID Matched?", position: "label-left", labelWidth: 93, inputWidth: 115, required: true, className: "req-asterisk",
                            validate: function(input) { return filterPanelForm.getCombo("SecondaryPhotoIdMatch").getSelectedIndex() === 0 }
                        }
                    ]
                }
        ];

        filterPanelForm = cellA.attachForm(driverformStructure);

        var truckingCompanyId = filterPanelForm.getCombo('TruckingCompanyId');
        truckingCompanyId.loadExternal('/CargoDischarge/GetTruckingCompanies', 'Id', 'CarrierName', function () {
            if(driverDetails)
                truckingCompanyId.selectOption(truckingCompanyId.getIndexByValue(driverDetails.TruckingCompany.Id));
        });

        var driverId = filterPanelForm.getCombo('CarrierDriverId');
        driverId.loadExternal('/CargoDischarge/GetDrivers', 'Id', 'FullName', function() {
            if (driverDetails)
                driverId.selectOption(driverId.getIndexByValue(driverDetails.CarrierDriver.Id));
        });

        driverId.attachEvent("onChange", function (id) {
            var option = this.getOption(id);
            filterPanelForm.setItemLabel('lbDriverLicenseState', option ? option.customData.item.State.TwoCharacterCode : '');
            filterPanelForm.setItemValue('LicenseNumber', option ? option.customData.item.LicenseNumber : '')
        });

        var primaryIdTypeId = filterPanelForm.getCombo('PrimaryIDTypeId');
        primaryIdTypeId.loadExternal('/CargoDischarge/GetCredentialTypes', 'Id', 'Name', function() {
            if (driverDetails)
                primaryIdTypeId.selectOption(primaryIdTypeId.getIndexByValue(driverDetails.PrimaryIDTypeId));
        });

        var secondaryIdTypeId = filterPanelForm.getCombo('SecondaryIdTypeId');
        secondaryIdTypeId.loadExternal('/CargoDischarge/GetCredentialTypes', 'Id', 'Name', function() {
            if (driverDetails)
                secondaryIdTypeId.selectOption(secondaryIdTypeId.getIndexByValue(driverDetails.SecondaryIdTypeId));
        });

        var secondaryPhotoIdMatch = filterPanelForm.getCombo('SecondaryPhotoIdMatch');
        secondaryPhotoIdMatch.loadExternal('/CargoDischarge/GetYesNo', 'Id', 'Name', function () {
            if (driverDetails)
                secondaryPhotoIdMatch.selectOption(secondaryPhotoIdMatch.getIndexByValue(driverDetails.SecondaryPhotoIdMatch));
        });

        var primaryPhotoIdMatch = filterPanelForm.getCombo('PrimaryPhotoIdMatch');
        primaryPhotoIdMatch.loadExternal('/CargoDischarge/GetYesNo', 'Id', 'Name', function() {
            if (driverDetails) {
                primaryPhotoIdMatch.selectOption(primaryPhotoIdMatch.getIndexByValue(driverDetails.PrimaryPhotoIdMatch));
                if (driverDetails.PrimaryPhotoIdMatch === true) {
                    filterPanelForm.disableItem("secondaryFieldset");
                    secondaryIdTypeId.unSelectOption();
                    secondaryPhotoIdMatch.unSelectOption();
                }
            }
        });

        primaryPhotoIdMatch.attachEvent("onChange", function (id) {
            var option = this.getOption(id);
            if (option && option.text === "NO")
                filterPanelForm.enableItem("secondaryFieldset");
            else {
                filterPanelForm.disableItem("secondaryFieldset");
                secondaryIdTypeId.unSelectOption();
                secondaryPhotoIdMatch.unSelectOption();
            }
        });

        if (driverDetails) {
            filterPanelForm.setItemValue('photoContainer', driverDetails.DriverPhoto || "");
            filterPanelForm.setItemValue('photoIdContainer', driverDetails.IDPhoto || "");
            filterPanelForm.setItemValue('Id', driverDetails.Id || "0");
        }


        filterPanelForm.enableLiveValidation(true);
        this.form = filterPanelForm;
    }.call(this);
}