﻿var newDriver = function (cellNewDriver) {
    var $this = this;
    this.onAfterSave = null;
    this.form = null;
    this.window = null;

    var formStructure = [
        {
            type: "block", list: [
                {
                    type: "input", name: "FirstName", label: "First Name:", position: "label-top", labelWidth: 100, inputWidth: 212, required: true, className: "req-asterisk",
                    validate: function (input) { return input && input.trim() !== "" }
                },
                { type: "newcolumn", offset: 20 },
                {
                    type: "input", name: "LastName", label: "Last Name:", position: "label-top", labelWidth: 100, inputWidth: 212, required: true, className: "req-asterisk",
                    validate: function (input) { return input && input.trim() !== "" }
                }
            ]
        },
        {
            type: "block", list: [
               {
                   type: "combo", name: "TruckingCompanyId", label: "Trucking Company:", position: "label-top", inputWidth: 212, required: true, className: "req-asterisk",
                   validate: function (input) { return $this.form.getCombo("TruckingCompanyId").getSelectedIndex() !== -1 }
               },
               { type: "newcolumn", offset: 20 },
               {
                   type: "input", name: "LicenseNumber", label: "License Number:", position: "label-top",inputWidth: 212, required: true, className: "req-asterisk",
                   validate: function (input) { return input && input.trim() !== "" }
               }
            ]
        },
        {
            type: "block", list: [
                { type: "calendar", name: "LicenseExpiry", label: "License Expires On:", position: "label-top", dateFormat: "%m-%d-%Y", inputWidth: 210, required: true, className: "req-asterisk" },
                { type: "newcolumn", offset: 20 },
                {
                    type: "input", name: "STANumber", label: "STA Number:", position: "label-top", inputWidth: 212, required: true, className: "req-asterisk",
                    validate: function (input) { return input && input.trim() !== "" }
                }
            ]
        },
        {
            type: "combo", name: "StateId", label: "State:",offsetLeft:20, position: "label-top", labelWidth: 70, inputWidth: 212, required: true, className: "req-asterisk",
            validate: function (input) { return $this.form.getCombo("StateId").getSelectedIndex() !== -1 }
        },
        {
            type: "block", list: [
                { type: "imageCapturer", name: "DriverPhoto", id: "DriverPhoto", controlHeight: '120px', buttonText: 'Capture', labelText: 'Photo:', labelWidth: '70px', imageHeight: '80px', imageWidth: '100px', required: true, className: "req-asterisk" },
                { type: "newcolumn", offset:80 },
                { type: "imageCapturer", name: "LicenseImage", id: "LicenseImage", controlWidth: '200px', controlHeight: '120px', buttonText: 'Capture', labelText: 'License Image:', labelWidth: '100px', imageHeight: '80px', imageWidth: '100px', required: true, className: "req-asterisk" }
            ]
        }
    ];

    var init = function () {
        var newDriverForm = new submitForm("New Driver", formStructure, function (formData) {
            formData.DriverPhoto = newDriverForm.form.getItemValue('DriverPhoto');
            formData.LicenseImage = newDriverForm.form.getItemValue('LicenseImage');

            formData.LicenseExpiry = $this.form.getCalendar('LicenseExpiry').getFormatedDate();
            formData.TruckingCompany = { Id: formData.TruckingCompanyId }
            formData.State = { Id: formData.StateId }

            helpers.executePost("/CargoDischarge/CreateDriver", formData, function () {
                if ($this.onAfterSave)
                    $this.onAfterSave(formData);
            });

        }, 515, 470, "SAVE");

        $this.form = newDriverForm.form;
        $this.window = newDriverForm.window;
        
        $this.form.getCombo('TruckingCompanyId').loadExternal('/CargoDischarge/GetTruckingCompanies', 'Id', 'CarrierName');
        $this.form.getCombo('StateId').loadExternal('/CargoDischarge/GetStates', 'Id', 'StateProvince');
    }();
}