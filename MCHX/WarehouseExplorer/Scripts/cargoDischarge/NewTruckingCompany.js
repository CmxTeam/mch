﻿var newTruckingCompany = function () {
    var $this = this;
    this.onAfterSave = null;
    this.form = null;
    this.window = null;

    var formStructure = [
        {
            type: "block", list: [
                {
                    type: "input", name: "CarrierCode", label: "Carrier Code:", position: "label-top", labelWidth: 100, inputWidth: 212, required: true, className: "req-asterisk", maxLength:5,
                    validate: function (input) { return input && input.trim() !== "" }
                },
                { type: "newcolumn", offset: 20 },
                {
                    type: "input", name: "CarrierName", label: "Carrier Name:", position: "label-top", labelWidth: 100, inputWidth: 212, required: true, className: "req-asterisk", maxLength:50,
                    validate: function (input) { return input && input.trim() !== "" }
                }
            ]
        },
        {
            type: "block", list: [
                {
                    type: "input", name: "Carrier3Code", label: "Carrier 3Code:", position: "label-top", labelWidth: 100, inputWidth: 212, required: true, className: "req-asterisk", maxLength: 3,
                    validate: function (input) { return input && input.trim() !== "" }
                },
                { type: "newcolumn", offset: 20 },
                {
                    type: "combo", name: "MotId", label: "MOT:", position: "label-top", labelWidth: 70, inputWidth: 212, required: true, className: "req-asterisk",
                    validate: function (input) { return $this.form.getCombo("MotId").getSelectedIndex() !== -1 }
                }
            ]
        },
        {
            type: "block", list: [
                {
                    type: "template", label: "Logo:", inputWidth:444, name: "CarrierLogo", position: "label-top", labelWidth: 70, className: "req-asterisk",
                    format: helpers.uploaderFormTemplate, required: true
                }
            ]
        }
    ];

    var init = function () {

        var newTruckingCompanyForm = new submitForm("New Trucking Company", formStructure, function (formData) {
            
            formData.CarrierMOT = { Id: formData.MotId }

            helpers.executePost("/CargoDischarge/CreateCarrier", formData, function () {
                if ($this.onAfterSave)
                    $this.onAfterSave(formData);
            });

        }, 515, 300, "SAVE");

        
        $this.form = newTruckingCompanyForm.form;
        $this.window = newTruckingCompanyForm.window;

        $this.form.getCombo('MotId').loadExternal('/CargoDischarge/GetMots', 'Id', 'TransportModeName');
    }();
}


