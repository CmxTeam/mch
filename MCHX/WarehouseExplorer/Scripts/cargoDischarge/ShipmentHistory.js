﻿var shipmentHistory = function (cellShipmentHistory) {
    var $this = this;

    this.filter = function (filterData) {
        cellShipmentHistory.progressOn();

        $this.grid.clearAll();
        $this.grid.loadExternalByModel('/CargoDischarge/GetShipmentTransactions', filterData, {
            "Status.DisplayName": "",
            "Description": "",
            "StatusTimestamp": "%m-%d-%Y %H:%i"
        }, function () { cellShipmentHistory.progressOff(); });
    }

    var init = function () {
        cellShipmentHistory.showView('ShipmentHistoryGridContainer');
        var gridShipHistory = cellShipmentHistory.attachGrid();
        gridShipHistory.setIconsPath(dhtmlx.image_path);
        gridShipHistory.setHeader(["Status", "Description", "Timestamp"]);
        gridShipHistory.setColTypes("ro,ro,ro");
        gridShipHistory.setColSorting('str,str,str');
        gridShipHistory.setInitWidths('*,*,*');
        //grid.enableColumnMove(true);
        gridShipHistory.init();
        $this.grid = gridShipHistory;
    }();

    return this;
}