﻿var selectedShipment = function (cellLayoutGrid) {
    var $this = this;
    this.onDeleteClick = null;
    this.onEditClick = null;
    var uniqueId = 0;

    this.clearAllData = function() {
        uniqueId = 0;
        $this.grid.clearAll();
    }

    this.getShipments = function (id) {
        if (id) 
            return $this.grid.getUserData(id, "CustomRowData");

        var tmpArray = [];

        $($this.grid.getAllRowIds().split(",")).each(function () {
            if($this.grid.getUserData(this, "CustomRowData") !== "")
                tmpArray.push($this.grid.getUserData(this, "CustomRowData"));
        });

        return tmpArray;
    }

    this.addEditShimpent = function (id, data, customData) {
        var existingIndex = 0;
        if (id !== null && $this.grid.getRowIndex(id) !== -1) {
            existingIndex = $this.grid.getRowIndex(id);
            $this.grid.deleteRow(id);
        } else {
            uniqueId = uniqueId + 1;
            id = uniqueId;
        }

        $this.grid.addRow(id, data, existingIndex);
        customData.rowId = id;
        $this.grid.setUserData(id, "CustomRowData", customData);
    }

    this.grid = function () {
        cellLayoutGrid.showView('SelectedShipmentGridContainer');
        var gridSelected = cellLayoutGrid.attachGrid();
        gridSelected.setIconsPath(window.baseUrl +"Content/imgs/");
        gridSelected.setHeader(["Shipping Reference#", "Action", "Release Pieces"]);
        gridSelected.setColTypes("ro,ro,ro");
        gridSelected.setColSorting('str,str,str');
        gridSelected.setInitWidths('*,*,*');
        gridSelected.init();


        gridSelected.attachEvent("onRowSelect", function (block, e) {
            
            if ($this.onEditClick)
                $this.onEditClick(block);
           
        });

        return gridSelected;
    }();

}