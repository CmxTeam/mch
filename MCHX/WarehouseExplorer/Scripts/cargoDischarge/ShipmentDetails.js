﻿var shipmentDetails = function (cellShipmentDetails) {
    var $this = this;
    this.onSaveClick = null;
    this.onCancelClick = null;
    this.onDeleteClick = null;
    this.onUpdateClick = null;
    this.isShipmentReadyForUpdate = false;
    this.LatestCustomsStatus = null;
    
    var dischargeShipmentPickupAction = false;
    var addPanelForm;

    var getRemeaningPieces = function () {
        if (addPanelForm.isItemChecked("DischargeTypeId", 1))
            return 99999;
        return parseInt(addPanelForm.getItemLabel("availablePieces"));
    }

    this.headerTemplate = '<div style="background-color: transparent;" class="dhx_toolbar_dhx_web dhxtoolbar_icons_18" dir="ltr">'
            + '<div class="dhxtoolbar_float_left"><div class="dhx_toolbar_text" title="">Shipment Details</div><div class="dhx_toolbar_sep" title=""></div>'
            + '<div id="btnOverrideCustomsStatus" class="dhx_toolbar_btn dhxtoolbar_btn_def div-hover" title="">'
            + '<div class="dhxtoolbar_text">Override Customs Status</div>'
            + '</div></div></div>';

    var disableEnableDeleteButton = function () {
        if (addPanelForm.getItemValue('rowId') !== "") {
            addPanelForm.enableItem("btnDelete");
        } else {
            addPanelForm.disableItem("btnDelete");
        }
    }

    var disableEnablePickupAction = function () {
        if (!dischargeShipmentPickupAction) {
            addPanelForm.disableItem("DischargeTypeId", 1);
        } else {
            addPanelForm.enableItem("DischargeTypeId", 1);
        }
    }

    var disableEnableTotalReleasedPieces = function () {
        if (addPanelForm.isItemChecked("DischargeTypeId", 1))
            addPanelForm.disableItem("TotalReleasedPieces");
        else
            addPanelForm.enableItem("TotalReleasedPieces");
    }

    this.disableEnableForm = function (isEnable) {
        if (addPanelForm.getItemValue("Id") !== "" || isEnable) {
            addPanelForm.unlock();
            if (addPanelForm.isItemHidden("btnUpdate")) {

                $this.isShipmentReadyForUpdate = true;

                disableEnableTotalReleasedPieces();
                disableEnableDeleteButton();
                disableEnablePickupAction();

                addPanelForm.enableItem("DischargeTypeId", 2);
                addPanelForm.enableItem("DischargeTypeId", 3);
                addPanelForm.enableItem("DischargeTypeId", 4);
                addPanelForm.enableItem("btnCancel");

            } else {
                addPanelForm.enableItem("btnUpdate");
                addPanelForm.disableItem("btnDelete");
                addPanelForm.disableItem("btnCancel");
                addPanelForm.disableItem("DischargeTypeId", 1);
                addPanelForm.disableItem("DischargeTypeId", 2);
                addPanelForm.disableItem("DischargeTypeId", 3);
                addPanelForm.disableItem("DischargeTypeId", 4);
                addPanelForm.disableItem("TotalReleasedPieces");
            }
            
        } else {
            $this.isShipmentReadyForUpdate = false;
            addPanelForm.hideItem("btnSave");
            addPanelForm.showItem("btnUpdate");
            addPanelForm.lock();
        }
    }

    var dischargeTypeIdOnChange = function(id) {
        addPanelForm.hideItem("CarrierId");
        addPanelForm.hideItem("TransferManifest");
        addPanelForm.hideItem("InbondNumber");
        addPanelForm.hideItem("StationId");
        if (id == 1) {
            addPanelForm.disableItem("TotalReleasedPieces");
            addPanelForm.setItemValue('TotalReleasedPieces', addPanelForm.getItemLabel("availablePieces"));
        } else if (id == 3) {
            addPanelForm.getCombo('StationId').unSelectOption();
            addPanelForm.setItemValue('InbondNumber', '');
            addPanelForm.showItem("CarrierId");
            addPanelForm.showItem("TransferManifest");
        } else if (id == 4) {
            addPanelForm.showItem("InbondNumber");
            addPanelForm.showItem("StationId");
            addPanelForm.getCombo('CarrierId').unSelectOption();
            addPanelForm.setItemValue('TransferManifest', '');
        } else {
            addPanelForm.enableItem("TotalReleasedPieces");
            addPanelForm.getCombo('CarrierId').unSelectOption();
            addPanelForm.setItemValue('TransferManifest', '');
            addPanelForm.getCombo('StationId').unSelectOption();
            addPanelForm.setItemValue('InbondNumber', '');
        }
    }

    this.setData = function (data) {
        $this.clearShipmentDetailForm();
        addPanelForm.setFormData(data);
        addPanelForm.setItemLabel('shippingReference', data.shippingReference);
        addPanelForm.setItemLabel('consignee', data.consignee);
        addPanelForm.setItemValue('TransferManifest', data.TransferManifest);
        addPanelForm.setItemValue('InbondNumber', data.InbondNumber);
        addPanelForm.setItemLabel('customsBroker', data.customsBroker);
        addPanelForm.setItemLabel('customsStatus', data.customsStatus);
        addPanelForm.setItemLabel('availablePieces', data.availablePieces);
        addPanelForm.setItemLabel('PickupDocumentDate', data.PickupDocumentDate);
        if (addPanelForm.isItemChecked("DischargeTypeId", 1) && addPanelForm.getItemValue('rowId') === "")
            addPanelForm.setItemValue('TotalReleasedPieces', data.availablePieces);

        if (addPanelForm.getItemValue('rowId') === "") {
            addPanelForm.hideItem("btnUpdate");
            addPanelForm.showItem("btnSave");
        } else {
            addPanelForm.showItem("btnUpdate");
            addPanelForm.hideItem("btnSave");
        }
        $this.LatestCustomsStatus = data.LatestCustomsStatus;
        dischargeShipmentPickupAction = data.dischargeShipmentPickupAction;
        dischargeTypeIdOnChange(data.DischargeTypeId);
        $this.disableEnableForm();
    }

    this.clearShipmentDetailForm = function () {
        addPanelForm.getCombo('StationId').unSelectOption();
        addPanelForm.getCombo('CarrierId').unSelectOption();
        addPanelForm.setItemLabel('shippingReference', '');
        addPanelForm.setItemLabel('consignee', '');
        addPanelForm.setItemLabel('customsBroker', '');
        addPanelForm.setItemLabel('customsStatus', '');
        addPanelForm.setItemLabel('availablePieces', '');
        addPanelForm.setItemLabel('PickupDocumentDate', '');
        addPanelForm.setItemValue('TotalReleasedPieces', '');
        addPanelForm.setItemValue('TransferManifest', '');
        addPanelForm.setItemValue('InbondNumber', '');
        addPanelForm.setItemValue('DischargeTypeId', 1);
        addPanelForm.setItemValue('rowId', "");
        addPanelForm.setItemValue('Id', "");
        addPanelForm.setItemValue('EntityId', "");
        addPanelForm.setItemValue('EntityType', "");
        addPanelForm.hideItem("CarrierId");
        addPanelForm.hideItem("TransferManifest");
        addPanelForm.hideItem("InbondNumber");
        addPanelForm.hideItem("StationId");

        dischargeShipmentPickupAction = false;
        $this.LatestCustomsStatus = null;
        $this.disableEnableForm();
        
    }

    this.getShipmentDetailForm = function () {
        var formData = addPanelForm.getFormData();
        formData.shippingReference = addPanelForm.getItemLabel("shippingReference");
        formData.consignee = addPanelForm.getItemLabel("consignee");
        formData.customsBroker = addPanelForm.getItemLabel("customsBroker");
        formData.customsStatus = addPanelForm.getItemLabel("customsStatus");
        formData.availablePieces = addPanelForm.getItemLabel("availablePieces");
        formData.PickupDocumentDate = addPanelForm.getItemLabel("PickupDocumentDate");
        formData.LatestCustomsStatus = $this.LatestCustomsStatus;
        return formData;
    }

    var init = function () {
        cellShipmentDetails.showView('shipmentDetailsContainer');
       
        cellShipmentDetails.setText($this.headerTemplate);
        cellShipmentDetails.setCollapsedText("Shipment Details");

        var formData = [
            {
                type: "block",
                list: [
                    { type: "hidden", name: "rowId"},
                    { type: "hidden", name: "Id" },
                    { type: "hidden", name: "EntityId" },
                    { type: "hidden", name: "EntityType" },
                    {
                        type: "block", blockOffset: 0,
                        list: [
                            {
                                type: "block", blockOffset: 0, width: 150,
                                list: [
                                    { type: "label", label: "Shipping Ref#:", labelHeight: 14 },
                                    { type: "label", label: "Consignee#:", labelHeight: 14 },
                                    { type: "label", label: "Customs Broker:", labelHeight: 14 },
                                    { type: "label", label: "Customs Status:", labelHeight: 14 },
                                    { type: "label", label: "Available Pieces:", labelHeight: 14 },
                                    { type: "label", label: "Pickup Document Date:", labelHeight: 14 }
                                ]
                            },
                            { type: "newcolumn" },
                            {
                                type: "block", blockOffset: 25,
                                className: 'detailsContainer label-text-ellipsis',
                                list: [
                                    { type: "label", name: "shippingReference", labelHeight: 14 },
                                    { type: "label", name: "consignee", labelHeight: 14 },
                                    { type: "label", name: "customsBroker", labelHeight: 14 },
                                    { type: "label", name: "customsStatus", labelHeight: 14 },
                                    { type: "label", name: "availablePieces", labelHeight: 14 },
                                    { type: "label", name: "PickupDocumentDate", labelHeight: 14 }
                                ]
                            }
                        ]
                    },
                    { type: "label", label: "Action:", labelHeight: 14 },
                    { type: "radio", name: "DischargeTypeId", checked: true, value: 1, label: "Pickup Document", labelWidth: "auto", position: "label-right", labelHeight: 14 },
                    { type: "radio", name: "DischargeTypeId", value: 2, label: "Freight Pickup", labelWidth: "auto", position: "label-right", labelHeight: 14 },
                    {
                        type: "radio", name: "DischargeTypeId", value: 3, label: "Transfer to Carrier", labelWidth: "auto", position: "label-right", labelHeight: 14,
                        list: [
                            {
                                type: "combo", name: "CarrierId", label: "Carrier:", hidden:true, position: "label-left", inputWidth: 128, labelWidth: 120, className: "req-asterisk", required: true,
                                validate: function (input) { return addPanelForm.getCombo("CarrierId").getSelectedIndex() !== -1 }
                            },
                            { 
                                type: "input", name: "TransferManifest", label: "Transfer Manifest#", hidden:true, position: "label-left", labelHeight: 14, labelWidth: 120, inputWidth: 128, className: "req-asterisk", required: true,
                                validate: function (input) { return input.trim() !== "" }
                            }
                        ]
                    },
                    {
                        type: "radio", name: "DischargeTypeId", value: 4, label: "Domestic Transfer", labelWidth: "auto", position: "label-right", labelAlign: "right", labelHeight: 14, list:
                        [
                            {
                                type: "input", name: "InbondNumber", label: "Inbound No.", hidden:true, position: "label-left", labelHeight: 14, labelWidth: 120, inputWidth: 128, className: "req-asterisk", required: true,
                                validate: function (input) { return input.trim() !== "" }
                            },
                            {
                                type: "combo", name: "StationId", label: "Station:", hidden:true, position: "label-left", labelWidth: 120, inputWidth: 128, className: "req-asterisk", required: true,
                                validate: function (input) {return addPanelForm.getCombo("StationId").getSelectedIndex() !== -1;}
                            }
                        ]
                    },
                    {
                        type: "input", name: "TotalReleasedPieces", position: "label-left", label: "Total Release Pieces:", className: "req-asterisk keep-label-enabled", labelWidth: 140, inputWith: 200, required: true, maxLength: 4,
                        validate: function(input) {
                            return /^[0-9]+$/.test(input) && input > 0 && input <= getRemeaningPieces();
                        }
                    },
                    {
                        type: "block",  list: [
                                              { type: "button", name: "btnCancel", value: "CANCEL", disabled: !$this.isShipmentReadyForUpdate  },
                                              { type: "newcolumn" },
                                              { type: "button", name: "btnUpdate", value: "UPDATE" },
                                              { type: "button", name: "btnSave", value: "SAVE", hidden:true },
                                              { type: "newcolumn" },
                                              { type: "button", name: "btnDelete", value: "DELETE", disabled: !$this.isShipmentReadyForUpdate }
                        ]
                    }
                ]
            }
        ];
        addPanelForm = cellShipmentDetails.attachForm(formData);
        addPanelForm.getCombo('CarrierId').loadExternal('/CargoDischarge/GetCarriers', 'Id', 'CarrierName');

        //set styles for resposiv
        addPanelForm.cont.firstElementChild.classList.add("form-auto-width");
        $(addPanelForm.cont.children).find(".dhxform_base").each(function () {
            $(this).addClass("form-auto-width");
        });
        $(addPanelForm.cont.children).find(".dhxform_block").each(function () {
            $(this).addClass("form-auto-width");
        });
        var btns = $($(addPanelForm.cont.children).find(".dhxform_base"));
        for (var i = 1; i < 4; i++) {
            $(btns[btns.length - i]).removeClass("form-auto-width");
        }
        $($($(addPanelForm.cont.children).find(".dhxform_block")[1]).find(".dhxform_base")[2]).css("width", "calc(100% - 150px)");//.addClass("form-width");
        $($($(addPanelForm.cont.children).find(".dhxform_block")[1]).find(".dhxform_base")[0]).css("width", "120px");//.addClass("form-width");
        ///////
        

        addPanelForm.getCombo('StationId').loadExternal('/CargoDischarge/GetPorts', 'Id', 'Name');

        addPanelForm.attachEvent("onChange", function (name, value) {
            if (name === "DischargeTypeId") {
                dischargeTypeIdOnChange(value);
            }
        });

        addPanelForm.attachEvent("onButtonClick", function (name) {
            if (name === "btnUpdate") {

                addPanelForm.hideItem("btnUpdate");
                addPanelForm.showItem("btnSave");
                $this.disableEnableForm();
                if ($this.onUpdateClick) {
                    $this.onUpdateClick();
                }

            } else if (name === 'btnSave') {

                var data = $this.getShipmentDetailForm();
                if (data.LatestCustomsStatus.CustomsCode.IsGreenCode) {
                    if (addPanelForm.validate() && $this.onSaveClick) {
                        $this.onSaveClick(data);
                    }
                } else {
                    dhtmlx.alert({
                        title: "Warning",
                        type: "warning",
                        text: "The customs status does not allow you to proceed!"
                    });
                }

            } else if (name === 'btnDelete') {

                if ($this.onDeleteClick)
                    $this.onDeleteClick(addPanelForm.getItemValue('rowId'));

            } else {
                $this.clearShipmentDetailForm();
                if ($this.onCancelClick)
                    $this.onCancelClick();
            }
        });

        $this.form = addPanelForm;
        $this.form.lock();
    }.call(this);
}