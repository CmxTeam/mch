﻿var dischargeTransctionWindow = function (isEdit, selectedShipments, driverDetail) {
    var $this = this;
    this.existingDischargeId = 0;
    var _driverDetails;
    var _shipmentDetails;
    var _attachments;
    var _shipmentHistory;
    var _selectedShipments;
    var _startDate = new Date();
    
    var dischargeTypeEnum = {
        1: "Pickup Document",
        2: "Freight Pickup",
        3: "Transfer to Carrier",
        4: "Domestic Transfer"
    }

    var convertDriverDetailsToApiModel = function(driverLog) {
        return {
            Id: driverLog.Id || 0,
            DriverPhoto: driverLog.DriverPhoto,
            IDPhoto: driverLog.IDPhoto,
            PrimaryIDTypeId: driverLog.PrimaryIDTypeId,
            PrimaryPhotoIdMatch: driverLog.PrimaryPhotoIdMatch,
            SecondaryIdTypeId: driverLog.SecondaryIdTypeId,
            SecondaryPhotoIdMatch: driverLog.SecondaryPhotoIdMatch,
            LicenseNumber: driverLog.LicenseNumber,
            DriverThumbnail: driverLog.DriverThumbnail,
            CarrierDriver: {
                Id: driverLog.CarrierDriverId,
                LicenseNumber: driverLog.LicenseNumber,
                DriverPhoto: driverLog.DriverPhoto,
                DriverThumbnail: driverLog.DriverThumbnail,
                LicenseImage: driverLog.IDPhoto
            },
            TruckingCompany: {
                id: driverLog.TruckingCompanyId
            }
        }
    }

    var convertShipmentsToApiModel = function (shipments) {
        var convertedArray = [];

        shipments.forEach(function (item) {
            item.DischargeShipmentType = { Id: item.DischargeTypeId };
            item.Station = { id: item.StationId }
            item.Carrier = { id: item.CarrierId }
            convertedArray.push(item);
        });
        return convertedArray;
    }

    var convertShipmentFromApiModel = function (item) {
        return {
            rowId: item.rowId || null,
            Id: item.Id.toString(),
            EntityId: item.EntityId.toString(),
            EntityType: item.EntityType.toString(),
            shippingReference: item.IATACodeOrig + '-' + item.Carrier3Code + '-' + item.ShipingRef + '-' + item.IATACodeDest,
            consignee: (item.Consignee.Name?item.Consignee.Name + ', ':"") + (item.Consignee.AddressLine1 ?item.Consignee.AddressLine1 + ', ':"") + (item.Consignee.City||""),
            customsBroker: (item.CustomsBroker.Name ? item.CustomsBroker.Name : 'N/A')  + (item.CustomsBroker.Place ? ', ' + item.CustomsBroker.Place : ''),
            availablePieces: item.AvailablePieces,
            PickupDocumentDate: item.PickupDocumentDate ? window.dhx4.date2str(new Date(item.PickupDocumentDate), '%m-%d-%Y %H:%i') : "",
            ShipmentDischargedPieces: item.ShipmentDischargedPieces,
            ReceivedPieces: item.ReceivedPieces,
            LatestCustomsStatus: item.LatestCustomsStatus,
            customsStatus: item.LatestCustomsStatus && item.LatestCustomsStatus.PiecesAffectedCount ? (item.LatestCustomsStatus.CustomsCode.Code + ' (' + item.LatestCustomsStatus.PiecesAffectedCount + ') ' + item.LatestCustomsStatus.CustomsCode.Description) : '',
            DischargeTypeId: item.DischargeShipmentType ? item.DischargeShipmentType.Id : 1,
            attachments: item.Attachments,
            TotalReleasedPieces: item.TotalReleasedPieces,
            InbondNumber: item.InbondNumber,
            StationId: item.Station ? item.Station.Id : null,
            CarrierId: item.Carrier ? item.Carrier.Id : null,
            TransferManifest: item.TransferManifest,
            AllowEdit: item.AllowEdit
        };
    }

    var calculateRemeaningPieces = function (shipment) {
        var remainingPieces = shipment.LatestCustomsStatus.PiecesAffectedCount - shipment.ShipmentDischargedPieces;
        var dischargeShipmentPickupAction = true;
        $(_selectedShipments.getShipments()).each(function () {
            if (this.EntityId == shipment.EntityId && this.EntityType == shipment.EntityType && this.rowId != shipment.rowId) {
                if (this.DischargeTypeId != 1) {
                    remainingPieces = remainingPieces - parseInt(this.TotalReleasedPieces);
                }
                dischargeShipmentPickupAction = false;
            }
        });

        shipment.availablePieces = remainingPieces;

        if (!dischargeShipmentPickupAction || shipment.PickupDocumentDate != "") {
            shipment.DischargeTypeId = (shipment.rowId == "" || shipment.rowId == null) && shipment.DischargeTypeId === 1 ? 2 : shipment.DischargeTypeId;
            shipment.dischargeShipmentPickupAction = false;
        } else {
            shipment.dischargeShipmentPickupAction = true;
        }
        return shipment;
    }
   
    var init = function () {
        var windows = new dhtmlXWindows();
        var driverWindow = windows.createWindow('driverWindow', 0, 0, 1200, 600);
        driverWindow.show();
        driverWindow.setText("Drivers");
        driverWindow.setModal(1);
        driverWindow.maximize();
        driverWindow.denyResize();
        driverWindow.denyPark();
        driverWindow.centerOnScreen();
        var mainlayout = driverWindow.attachLayout('2U');
        var cellA = mainlayout.cells('a');
        var cellB = mainlayout.cells('b');
        var layoutB = cellB.attachLayout('2E');
        var cellDetail = layoutB.cells('a');
        var cellLayoutB = cellDetail.attachLayout('3L');
        var cellShipmentDetails = cellLayoutB.cells('a');
        var cellShipmentAttachment = cellLayoutB.cells('b');
        var cellShipmentHistory = cellLayoutB.cells('c');
        var cellSelectedShipments = layoutB.cells('b');

        var clearShipmentInfo = function() {
            _shipmentDetails.clearShipmentDetailForm();
            _attachments.clearAndAddAttachments();
            _shipmentHistory.grid.clearAll();
        }

        cellA.setWidth(308);
        cellA.setText("Driver");

        _driverDetails = new driverDetails(cellA, driverDetail);
       
        var layoutBToolbar = cellLayoutB.attachToolbar();
        layoutBToolbar.setIconsPath(dhtmlx.image_path);
        layoutBToolbar.loadStruct('<toolbar>'
                                    + '<item type="text" id="txtSearch" text="Shipping Reference#:" title="" />'
                                    + '<item type="buttonInput" id="txtSearchFilter" width="225" title="" />'
                                    + '<item type="separator" id="button_separator_1" />'
                                    + '<item type="button" id="btnSearch" text="SEARCH" title="" />'
                                    + '<item id="sep2" type="separator"/>'
                                    + '<item type="button" id="btnFinalize" text="FINALIZE" title="" />'
                                    + '</toolbar>', function () { });
        layoutBToolbar.attachEvent('onClick', function (name, value) {
            if (name === 'btnSearch') {
                var searchText = layoutBToolbar.getInput('txtSearchFilter').value;
                if (searchText) {
                    cellShipmentDetails.progressOn();
                    clearShipmentInfo();
                    helpers.getDataFromExternalResource("/CargoDischarge/GetDischargeShipmentByRef", { shipingRef: searchText, dischargeId: $this.existingDischargeId }, function (data) {
                        console.log(data);

                        if (data && data.EntityId !== 0) {
                            var model = convertShipmentFromApiModel(data);

                            $(_selectedShipments.getShipments()).each(function () {
                                if (this.EntityId == data.EntityId && this.EntityType == data.EntityType && this.rowId != data.rowId && this.LatestCustomsStatus) {
                                    model.LatestCustomsStatus = this.LatestCustomsStatus;
                                    model.customsStatus = this.customsStatus;
                                    return false;
                                }
                            });

                            _shipmentDetails.setData(calculateRemeaningPieces(model));

                            _selectedShipments.grid.clearSelection();

                            _shipmentHistory.filter({ shipmentId: data.EntityId, shipmentType: data.EntityType });
                        } else {
                            dhtmlx.alert({
                                title: "Warning",
                                type: "warning",
                                text: "Shipment does not exist!"
                            });
                        }

                        cellShipmentDetails.progressOff();
                    });
                }
            } else if (name === 'btnFinalize') {
                if (_driverDetails.form.validate()) {
                    if (_selectedShipments.getShipments().length > 0) {
                        driverWindow.progressOn();

                        helpers.executePost('/CargoDischarge/CreateDischarge', { Id: $this.existingDischargeId, StartDate: _startDate.toJSON(), DriverLog: convertDriverDetailsToApiModel(_driverDetails.getData()), DischargeShipments: convertShipmentsToApiModel(_selectedShipments.getShipments()) }, function (data) {
                            if (data && data.Status && data.Status.Status == false) {
                                dhtmlx.alert({
                                    title: "Warning",
                                    type: "warning",
                                    text: data.Status.Message || "Something went wrong, please try again later!"
                                });
                                driverWindow.progressOff();
                                return false;
                            }
                                
                            _driverDetails.clearDriverDetailsForm();
                            _selectedShipments.clearAllData();
                            clearShipmentInfo();
                            layoutBToolbar.setValue("txtSearchFilter", "");
                            _startDate = new Date();
                            $this.existingDischargeId = 0;
                            driverWindow.progressOff();
                        });
                    } else {
                        dhtmlx.alert({
                            title: "Warning",
                            type: "warning",
                            text: "No Shipment(s) selected!"
                        });
                    }
                }
            }
        }.bind(this));
        
        _shipmentDetails = new shipmentDetails(cellShipmentDetails);
        _shipmentDetails.onSaveClick = function (data) {

            var rowData = [
                data.shippingReference,
                dischargeTypeEnum[data.DischargeTypeId],
                data.TotalReleasedPieces
            ];
            data.attachments = _attachments.getAttachments();
            if (data.attachments.length === 0 && data.DischargeTypeId !== 1) {
                dhtmlx.alert({
                    title: "Warning",
                    type: "warning",
                    text: "Attachment(s) is required!"
                });
            } else {
                _selectedShipments.addEditShimpent(data.rowId || null, rowData, data);
                if (data.LatestCustomsStatus) {
                    $(_selectedShipments.getShipments()).each(function () {
                        if (this.EntityId == data.EntityId && this.EntityType == data.EntityType && this.rowId != data.rowId) {
                            this.LatestCustomsStatus = data.LatestCustomsStatus;
                            this.customsStatus = data.customsStatus;
                            _selectedShipments.grid.setUserData(this.rowId, "CustomRowData", this);
                        }
                    });
                }
                clearShipmentInfo();
                _selectedShipments.grid.clearSelection();
            }
        }

        _shipmentDetails.onDeleteClick = function(id) {
            dhtmlx.confirm({
                type: "confirm",
                text: "Do you want to delete this item?",
                callback: function(result) {
                    if (result) {
                        _selectedShipments.grid.deleteRow(id);
                        if (_shipmentDetails.form.getInput("rowId").value == id)
                            _shipmentDetails.form.setItemValue('rowId', "");

                        _selectedShipments.grid.clearSelection();
                        clearShipmentInfo();
                    }
                }
            });
        }

        _shipmentDetails.onUpdateClick = function() {
            _attachments.allowEdit = true;
        }

        _shipmentDetails.onCancelClick = function() {
            clearShipmentInfo();
            _selectedShipments.grid.clearSelection();
        }
       
        cellShipmentAttachment.setWidth(500);
        _attachments = new attachments(cellShipmentAttachment, true, 130);

        var attachmentCellHeaderTmp = '<div style="background-color: transparent;" class="dhx_toolbar_dhx_web dhxtoolbar_icons_18" dir="ltr">'
            + '<div class="dhxtoolbar_float_left">'
            + '<div id="btnUploadAttachment" class="dhx_toolbar_btn dhxtoolbar_btn_def div-hover" title="">'
            + '<div class="dhxtoolbar_text">ADD ATTACHMENT</div>'
            + '</div>'
            + '<div class="dhx_toolbar_sep" title=""></div>'
            + '<div id="btnCaptureAttachment" class="dhx_toolbar_btn dhxtoolbar_btn_def div-hover" title="">'
            + '<div class="dhxtoolbar_text">CAPTURE</div>'
            + '</div>'
            + '<div class="dhx_toolbar_btn dhxtoolbar_btn_def" title="">'
            + '<div id="lblAttachmentsCount" class="dhxtoolbar_text"></div>'
            + '</div>'
            + '</div></div>';

        var changeAttachmentsCountHeader  = function() {
            if (document.getElementById("lblAttachmentsCount") != null) {
                if (_attachments.getAttachments().length != 0)
                    document.getElementById("lblAttachmentsCount").innerHTML = "(" + _attachments.getAttachments().length + ")";
                else
                    document.getElementById("lblAttachmentsCount").innerHTML = "";
            }
        }

        cellShipmentAttachment.setText(attachmentCellHeaderTmp);
        cellShipmentAttachment.setCollapsedText("ATTACHMENTS");

        cellLayoutB.attachEvent("onExpand", function (name) {
            if(name == "b")
                changeAttachmentsCountHeader();
        });

        _attachments.onAttachmentsCountChange = function() {
            changeAttachmentsCountHeader();
        }

        $(cellShipmentAttachment.cell).on('click', "#btnCaptureAttachment", function() {
            if (_shipmentDetails.isShipmentReadyForUpdate) {
                new helpers.documentCapturerControl('/CargoDischarge/GetShiperVerificationDocuments', function(data) {
                    var attachmentData = {
                        DbId: 0,
                        FileType: "Snapshot",
                        FileName: data.FileName,
                        Name: null,
                        Image: data.image,
                        DocumentTypeId: data.FileType
                    };

                    _attachments.addAttachment(attachmentData);
                });
            }
        });

        $(cellShipmentAttachment.cell).on('click', "#btnUploadAttachment", function() {
            if (_shipmentDetails.isShipmentReadyForUpdate) {
                var formStructure = [
                    {
                            type: "block", list: [
                                {
                                    type: "template", label: "Attachment File:", inputWidth: 444, name: "AttachmentFile", position: "label-top",
                                    labelWidth: 150, className: "req-asterisk", format: helpers.uploaderFormTemplate, required: true
                                },
                                { type: "input", name: "FileName", label: "File Name", position: "label-top", labelHeight: 14, labelWidth: 120, inputWidth: 444, className: "req-asterisk", required: true }
                            ]
                    }
                ];
                var newFileAttachmentForm = new submitForm("Add Attachment", formStructure, function(formData) {
                    var attachmentData = {
                        DbId: 0,
                        FileType: "Document",
                        FileName: $(".dhxform_textarea.customUploadText").val(),
                        Name:  formData.FileName,
                        Image: formData.AttachmentFile,
                        DocumentTypeId: null
                    };

                    _attachments.addAttachment(attachmentData);
                    newFileAttachmentForm.window.close();
                }, 515, 250, "SAVE");
            }
        });

        $(cellShipmentDetails.cell).on('click', "#btnOverrideCustomsStatus", function () {
            if (_shipmentDetails.isShipmentReadyForUpdate) {
                var overrideCusomsStatusForm = null;
                var model = JSON.parse(JSON.stringify(_shipmentDetails.getShipmentDetailForm()));
                var formStructure = [
                    {
                        type: "combo", name: "CustomsStatusId", offsetLeft:10, label: "Customs Status Code:", position: "label-top", labelWidth: 120, inputWidth: 350, className: "req-asterisk", required: true,
                        validate: function (input) { return overrideCusomsStatusForm.form.getCombo("CustomsStatusId").getSelectedIndex() !== -1; }
                    },
                    {
                        type: "input", name: "Comment", offsetLeft: 10, label: "Comment:", labelAlign: "left", position: "label-top", labelWidth: 100, inputWidth: 350, rows: 5, maxLength: 500
                    },
                    {
                        type: "input", name: "PiecesAffectedCount", offsetLeft: 10, position: "label-top", label: "Pieces Affected Count:", className: "req-asterisk keep-label-enabled", labelWidth: 140, inputWith: 200, required: true, maxLength: 4,
                        validate: function (input) {
                            return /^[0-9]+$/.test(input) && input > 0;
                        }
                    }
                ];

                overrideCusomsStatusForm = new submitForm("Override Customs Status", formStructure, function (formData) {
                    var minCount = model.ShipmentDischargedPieces;

                    $(_selectedShipments.getShipments()).each(function () {
                        if (this.EntityId == model.EntityId && this.EntityType == model.EntityType && this.rowId != model.rowId && this.DischargeTypeId != 1) {
                            minCount = minCount + parseInt(this.TotalReleasedPieces);
                        }
                    });

                    model.LatestCustomsStatus.PiecesAffectedCount = formData.PiecesAffectedCount;
                    model = calculateRemeaningPieces(model);

                    if (model.availablePieces > 0 && formData.PiecesAffectedCount <= model.ReceivedPieces) {
                        var option = overrideCusomsStatusForm.form.getCombo('CustomsStatusId').getOption(formData.CustomsStatusId);
                        var customStatusData = option.customData.item;

                        _shipmentDetails.form.setItemLabel('availablePieces', model.availablePieces);
                        _shipmentDetails.form.setItemLabel('customsStatus', customStatusData.Code + ' (' + formData.PiecesAffectedCount + ') ' + customStatusData.Description);

                        _shipmentDetails.LatestCustomsStatus = {
                            EntityId: model.EntityId,
                            EntityType: model.EntityType,
                            PiecesAffectedCount: formData.PiecesAffectedCount,
                            Comment: formData.Comment,
                            CustomsCode: customStatusData
                        };
                    } else {
                        dhtmlx.alert({
                            title: "Warning",
                            type: "warning",
                            text: "Pieces Affected Count should be between " + minCount + "-" + model.ReceivedPieces + " range!"
                        });
                    }
                    overrideCusomsStatusForm.window.close();

                }, 400, 350, "SAVE");
                overrideCusomsStatusForm.form.getCombo('CustomsStatusId').loadExternal('/CargoDischarge/GetAmsCodes', 'Id', 'FullName');
            }
        });

        cellShipmentHistory.setText("SHIPMENT HISTORY");
        cellShipmentHistory.setHeight(50);

        _shipmentHistory = new shipmentHistory(cellShipmentHistory);


        cellSelectedShipments.setText("SELECTED SHIPMENTS");
        cellSelectedShipments.setHeight(200);

        _selectedShipments = new selectedShipment(cellSelectedShipments);

        var editSelectedShipment = function(id) {
            clearShipmentInfo();
            var shipment = _selectedShipments.getShipments(id);
            _shipmentDetails.setData(calculateRemeaningPieces(shipment));
            _attachments.clearAndAddAttachments(shipment.attachments);
            _shipmentHistory.filter({ shipmentId: shipment.EntityId, shipmentType: shipment.EntityType });
            _attachments.allowEdit = false;
        }

        _selectedShipments.onEditClick = function (id) {
            if (_shipmentDetails.form.getItemValue("rowId") === "" && _shipmentDetails.isShipmentReadyForUpdate) {
                dhtmlx.confirm({
                    type: "confirm",
                    text: "You already have active shipment do you want to override it??",
                    callback: function(result) {
                        if (result) {
                            editSelectedShipment(id);
                        }
                    }
                });
            } else {
                editSelectedShipment(id);
            }
        }

        if (isEdit && selectedShipments) {
            selectedShipments.forEach(function (item) {
                var model = convertShipmentFromApiModel(item);
                var rowData = [
                        model.shippingReference,
                        dischargeTypeEnum[model.DischargeTypeId],
                        model.TotalReleasedPieces
                ]; 
                _selectedShipments.addEditShimpent(null, rowData, model);
            });

        }

    }.apply(this);
}