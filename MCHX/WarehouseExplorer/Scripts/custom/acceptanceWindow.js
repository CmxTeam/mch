﻿var acceptanceWindow = function () {
    var $this = this;
    this.onButtonClicked = null;

    this.show = function (id) {
        this._resetForm();
        this.window.setModal(1);
        this.window.show();
    }.bind(this);

    this.hide = function () {
        this.window.hide();
        this.window.setModal(0);
    }.bind(this);

    this._resetForm = function () {
        this.window.acceptanceForm.getCombo('airlinesCombo').unSelectOption();
        this.window.acceptanceForm.getCombo('originCombo').unSelectOption();
        this.window.acceptanceForm.getCombo('destinationCombo').unSelectOption();
        this.window.acceptanceForm.setItemValue('carrierTextbox', '');
        this.window.acceptanceForm.setItemValue('awbNumberTextbox', '');
        this.window.acceptanceForm.setItemValue('piecesTextbox', '');
        this.window.acceptanceForm.setItemValue('weightTextbox', '');
        this.window.acceptanceForm.setItemValue('shipperNameTextbox', '');
        this.window.acceptanceForm.setItemValue('consigneeNameTextbox', '');
        this.window.acceptanceForm.setItemLabel('statusLable', '');
        this.window.acceptanceForm.setItemValue('awbId', '');
        this._disableControls(); 
        this._disableComplete();
    }

    this._enableControls = function () {               
        this.window.acceptanceForm.enableItem('piecesTextbox');
        this.window.acceptanceForm.enableItem('weightTextbox');
        this.window.acceptanceForm.enableItem('weightTextboxLabel');
        this.window.acceptanceForm.enableItem('shipperNameTextbox');
        this.window.acceptanceForm.enableItem('consigneeNameTextbox');
    }

    this._disableControls = function () {               
        this.window.acceptanceForm.disableItem('piecesTextbox');
        this.window.acceptanceForm.disableItem('weightTextbox');
        this.window.acceptanceForm.disableItem('weightTextboxLabel');
        this.window.acceptanceForm.disableItem('shipperNameTextbox');
        this.window.acceptanceForm.disableItem('consigneeNameTextbox');
    }

    this._enableComplete = function(){
        this.window.acceptanceForm.enableItem('completeButton');
        this.window.acceptanceForm.enableItem('addHwbButton');        
    }

    this._disableComplete = function(){
        this.window.acceptanceForm.disableItem('completeButton');
        this.window.acceptanceForm.disableItem('addHwbButton');        
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('acceptanceWindow', 0, 0, 500, 410);

        var formStructure = [
            {
                type: 'block', list: [
                    { type:"hidden", name:"awbId" },
                    { type: "combo", name: "airlinesCombo", label: "Select Airline", position: "label-top", inputWidth: 222, connector: '../api/Common/GetAccountCarriers' },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "carrierTextbox", label: "Carrier", position: "label-top", labelWidth: 60, inputWidth: 50 },
                    { type: "newcolumn" },
                    { type: "input", name: "awbNumberTextbox", label: "AWB", position: "label-top", labelWidth: 60, inputWidth: 160 }
                ]
            },
            {
                type: 'block', list: [
                    { type: "combo", name: "originCombo", label: "Origin", labelWidth: 122, position: "label-top", inputWidth: 100 },
                    { type: "newcolumn" },
                    { type: "combo", name: "destinationCombo", label: "Destination", position: "label-top", inputWidth: 100 },
                    { type: "newcolumn" },
                    { type: "button", name: "searchButton", value: "SEARCH", offsetLeft: 30, offsetTop: 29 },
                    { type: "newcolumn" },
                    { type: "button", name: "resetButton", value: "RESET", offsetLeft: 25, offsetTop: 29 },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "piecesTextbox", label: "Pieces", position: "label-top", labelWidth: 122, inputWidth: 100 },
                    { type: "newcolumn" },
                    { type: "input", name: "weightTextbox", label: "Weight", position: "label-top", labelWidth: 60, inputWidth: 100 },
                    { type: "newcolumn" },
                    { type: "label", name: "weightTextboxLabel", label: "KGS", className: "control-right-label" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "shipperNameTextbox", label: "Shipper Name (optional)", position: "label-top", inputWidth: 222 },
                    { type: "newcolumn" },
                    { type: "label", name: "statusLable", offsetLeft: 30, offsetTop: 29 }
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "consigneeNameTextbox", label: "Consignee Name (optional)", position: "label-top", inputWidth: 222 },
                    { type: "newcolumn" },
                    { type: "button", name: "addHwbButton", value: "Add HWB", offsetLeft: 30, offsetTop: 29 },
                    { type: "newcolumn" },
                    { type: "button", name: "completeButton", value: "COMPLETE", offsetTop: 29, disabled: true }
                ]
            },
        ];

        window.acceptanceForm = window.attachForm(formStructure);

        var airlinesCombo = window.acceptanceForm.getCombo('airlinesCombo');

        airlinesCombo.attachEvent('onChange', function (id) {
            var option = this.getOption(id);
            window.acceptanceForm.setItemValue('carrierTextbox', option ? option.customData.Carrier3Code : '');
        });

        dhx4.ajax.get('../api/Common/GetPorts', function (a) {
            var response = a.xmlDoc.response || a.xmlDoc.responseText;
            var data = JSON.parse(response);
            window.acceptanceForm.getCombo('originCombo').load(data);
            window.acceptanceForm.getCombo('destinationCombo').load(data);
        });

        window.acceptanceForm.attachEvent('onButtonClick', function (name) {
            if (name == 'addHwbButton') {
                if (!window.acceptanceForm.getItemValue('awbId')) {
                    window.progressOn();
                    var values = window.acceptanceForm.getValues();
                    var accountId = airlinesCombo.getOption(airlinesCombo.getSelectedValue()).customData.AccountId;
                    console.log(accountId);
                    dhx4.ajax.get('../api/Common/AddAwb?accountId=' + accountId +
                        '&carrierNumber=' + values.carrierTextbox + '&awbSerialNumber=' + values.awbNumberTextbox + '&originId=' + values.originCombo
                        + '&destinationId=' + values.destinationCombo + '&totalPieces=' + values.piecesTextbox + '&shipperName=' + values.shipperNameTextbox
                        + '&consigneeName=' + values.consigneeNameTextbox + '&warehouseId=1&isComplete=false', function (a) {
                            var response = a.xmlDoc.response || a.xmlDoc.responseText;
                            window.acceptanceForm.setItemValue('awbId', response);
                            console.log('added awb:', response);
                            console.log(window.acceptanceForm.getItemValue('awbId'));

                            if (response < 0) {
                                window.acceptanceForm.setItemLabel('statusLable', 'AWB already exists.');
                            } else {
                                if ($this.onButtonClicked) {
                                    $this.onButtonClicked(window.acceptanceForm.getItemValue('awbId'));
                                }
                            }
                            window.progressOff();                            
                        });
                }
                else {
                    if (this.onButtonClicked) {
                        this.onButtonClicked(window.acceptanceForm.getItemValue('awbId'));
                    }
                }                                
            } else if (name == 'searchButton') {
                var values = window.acceptanceForm.getValues();
                if (values.airlinesCombo && values.carrierTextbox && values.awbNumberTextbox && values.originCombo && values.destinationCombo) {
                    window.progressOn();
                    dhx4.ajax.get('../api/Common/TryFindAwb?carrier=' + values.carrierTextbox +
                    '&awbNumber=' + values.awbNumberTextbox + '&originId=' + values.originCombo + '&destinationId=' + values.destinationCombo, function (a) {
                        var response = a.xmlDoc.response || a.xmlDoc.responseText;
                        var data = JSON.parse(response);
                        if (data) {
                            window.acceptanceForm.setItemValue('awbId', data.Id);
                            window.acceptanceForm.setItemValue('piecesTextbox', data.TotalPieces);
                            window.acceptanceForm.setItemValue('weightTextbox', data.TotalWeight);
                            window.acceptanceForm.setItemValue('shipperNameTextbox', data.ShipperName);
                            window.acceptanceForm.setItemValue('consigneeNameTextbox', data.ConsigneeName);
                            window.acceptanceForm.setItemLabel('statusLable', 'AWB already exists.');
                            $this._disableControls();
                            $this.window.acceptanceForm.enableItem('addHwbButton');
                        } else {
                            window.acceptanceForm.setItemValue('awbId', '');
                            window.acceptanceForm.setItemValue('piecesTextbox', '');
                            window.acceptanceForm.setItemValue('weightTextbox', '');
                            window.acceptanceForm.setItemValue('shipperNameTextbox', '');
                            window.acceptanceForm.setItemValue('consigneeNameTextbox', '');
                            window.acceptanceForm.setItemLabel('statusLable', '');
                            $this._enableControls();
                        }
                        window.progressOff();
                    });
                }
            } else if (name == 'completeButton') {
                window.progressOn();
                var values = window.acceptanceForm.getValues();
                var accountId = airlinesCombo.getOption(airlinesCombo.getSelectedValue()).customData.AccountId;
                console.log(accountId);
                dhx4.ajax.get('../api/Common/AddAwb?accountId=' + accountId +
                    '&carrierNumber=' + values.carrierTextbox + '&awbSerialNumber=' + values.awbNumberTextbox + '&originId=' + values.originCombo
                    + '&destinationId=' + values.destinationCombo + '&totalPieces=' + values.piecesTextbox + '&shipperName=' + values.shipperNameTextbox
                    + '&consigneeName=' + values.consigneeNameTextbox + '&warehouseId=1&isComplete=true', function (a) {
                        var response = a.xmlDoc.response || a.xmlDoc.responseText;

                        if (response > -1) {
                            console.log(response);
                            window.setModal(0);
                            window.hide();
                        } else {
                            window.acceptanceForm.setItemLabel('statusLable', 'AWB already exists.');
                        }
                        window.progressOff();
                    });
            } else if (name == 'resetButton') {
                $this._resetForm();
            }
        }.bind(this));

        window.acceptanceForm.attachEvent("onBeforeChange", function (name, oldValue, newValue) {                       
            if (!$this._awbId && name == 'piecesTextbox' && newValue) {
                $this._enableComplete();
            } else if (name == 'airlinesCombo' || name == 'carrierTextbox' || name == 'awbNumberTextbox' || name == 'originCombo' || name == 'destinationCombo') {
                $this._disableControls();
                $this._disableComplete();
            }

            return true;
        });

        window.button('close').attachEvent('onClick', function () {
            window.setModal(0);
            window.hide();
        });

        window.setText('ENTER AWB DETAILS');
        window.hide();
        window.denyResize();
        window.centerOnScreen();
        window.button('minmax').show();
        window.button('minmax').enable();

        this.window = window;
    }.apply(this);
};