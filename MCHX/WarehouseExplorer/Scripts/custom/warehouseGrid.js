﻿var warehouseGrid = function (cell, onRowDoubleClick) {
    this.onRowDoubleClick = onRowDoubleClick;
    this.onActionClicked = null;
    this.onExportToExcel = null;
    this.warehouseId = null;

    this.show = function () {
        cell.showView('warehouse');
    }

    this.filter = function (shipmentRef, carriersId, originId, destId,
                        warehouseId, zoneId, locationId, fromDate, toDate) {
        this.warehouseId = warehouseId;
        cell.progressOn();
        this.grid.clearAll();

        this.grid.load('../api/Common/GetWharehouseExplorerView?shipmentRef=' + shipmentRef +
        '&carriersId=' + carriersId +
        '&originId=' + originId +
        '&destId=' + destId +
        '&warehouseId=' + warehouseId +
        '&zoneId=' + zoneId +
        '&locationId=' + locationId +
        '&fromDate=' + fromDate +
        '&toDate=' + toDate,
        function () { cell.progressOff(); }, 'json');
    }

    this.exportToExcel = function (shipmentRef, carriersId, originId, destId,
                        warehouseId, zoneId, locationId, fromDate, toDate) {
        var downloadUrl = window.location.origin + '/api/Common/ExportToExcel?shipmentRef=' + shipmentRef +
                                '&carriersId=' + carriersId +
                                '&originId=' + originId +
                                '&destId=' + destId +
                                '&warehouseId=' + warehouseId +
                                '&zoneId=' + zoneId +
                                '&locationId=' + locationId +
                                '&fromDate=' + fromDate +
                                '&toDate=' + toDate;
        var a = document.createElement("a");
        a.href = downloadUrl;       
        document.body.appendChild(a);
        a.click();

        setTimeout(function () { document.body.removeChild(a); }, 100);
    }

    init = function () {
        cell.showView('warehouse');
        cell.hideHeader();
        var toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar><item type="text" id="button_text_1" text="QUICK FINDER:" title="" />' +
            '<item type="buttonInput" id="button_input_1" width="225" title="" />' +
            '<item type="separator" id="button_separator_1" />' +
            '<item type="buttonSelect" openAll="true" id="button_select_export" text="EXPORT" title="" >' +
            '<item type="button" id="exportToExcelButton" text="Excel" image="" />' +
            '</item><item type="separator" id="button_separator_4" />' +
            '<item type="buttonSelect" id="button_select_4" text="Actions" title="">' +
            '<item type="button" id="enterAcceptanceButton" text="Enter Acceptance" image="" /></item>' +
            '</toolbar>', function () { });

        toolbar.attachEvent('onClick', function (name) {
            if (name == 'exportToExcelButton') {
                if (this.onExportToExcel)
                    this.onExportToExcel();
            } else if (name == 'enterAcceptanceButton') {
                if (this.onActionClicked)
                    this.onActionClicked(name);
            }            
        }.bind(this));

        this.grid = cell.attachGrid();
        this.grid.setIconsPath(dhtmlx.image_path);
        this.grid.setHeader(["Carrier", "Shipment#", "Origin", "Dest", "Pieces", "Weight", "First Scan", "Last Inventory", "Duration", "Overage", "Shotage", "Damages", "Left Behind", "Type", "TaskId"]);
        this.grid.setInitWidths('*,100,*,*,*,*,150,150,90,*,*,*,*,*');        
        this.grid.setEditable(false);
        this.grid.init();
        this.grid.setColumnHidden(13, true);
        this.grid.setColumnHidden(14, true);
        this.grid.enableSmartRendering(true);

        this.grid.attachEvent("onRowSelect", function (rId, cInd) {
            var type = this.grid.cells(this.grid.getSelectedId(), 13).getValue();
            var taskId = this.grid.cells(this.grid.getSelectedId(), 14).getValue();
            
            if (this.onRowDoubleClick) {
                this.onRowDoubleClick(rId, type, taskId, this.warehouseId);
            }
        }.bind(this));        
    }.call(this);

    return this;
};