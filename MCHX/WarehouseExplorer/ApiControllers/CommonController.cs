﻿using DataExporter.Excel;
using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using WarehouseExplorer.DataProviders;
using WarehouseExplorer.Models;

namespace WarehouseExplorer.ApiControllers
{
    public class CommonController : BaseApiController<WarehouseDataProvider>
    {
        public IEnumerable<ValueText> GetPeriods()
        {
            return new List<ValueText> 
            { 
                new ValueText { value = "1", text = "Today" },
                new ValueText { value = "2", text = "Yesterday" },
                new ValueText { value = "3", text = "Last 3 Days" },
                new ValueText { value = "7", text = "Last Week" }, 
                new ValueText { value = "30", text = "Last Month" },
                new ValueText { value = "90", text = "Last Quarter" } 
            };
        }

        public IEnumerable<ValueText> GetCarriers()
        {
            return GetComboData(@"SELECT Id, CarrierName + ' (' + CarrierCode + ')' as Name
                FROM Carriers Where Isnull(CarrierCode, '') != '' and Isnull(CarrierName, '') != '' Order By CarrierName", "Id", "Name");
        }

        public IEnumerable<ValueText> GetPorts()
        {
            return GetComboData(@"SELECT Id, IATACode FROM Ports Order By IATACode", "Id", "IATACode");
        }

        public IEnumerable<ValueText> GetWarehouses(int? portId = 0)
        {
            var query = string.Format(@"Select Distinct Warehouses.Id, Warehouses.Code from UserWarehouses 
	                Inner Join Warehouses On UserWarehouses.WarehouseId = Warehouses.Id
	                Where {0} = 0 or Warehouses.PortId = {0} Order By Warehouses.Code", portId.GetValueOrDefault());
            return GetComboData(query, "Id", "Code");
        }

        public IEnumerable<ValueText> GetWarehouseZones(int? warehouseId = 0)
        {
            var query = string.Format(@"Select Id, Zone from WarehouseZones Where {0} = 0 or WarehouseZones.WarehouseId = {0} Order By Zone", warehouseId.GetValueOrDefault());
            return GetComboData(query, "Id", "Zone");
        }

        public IEnumerable<ValueText> GetWarehouseLocations(long? warehouseId = 0, int? zoneId = 0)
        {
            var query = string.Format(@"Select Id, Location from WarehouseLocations Where ({0} = 0 or ZoneId = {0}) and ({1} = 0 or WarehouseId = {1}) Order By Location", zoneId.GetValueOrDefault(), warehouseId.GetValueOrDefault());
            return GetComboData(query, "Id", "Location");
        }

        public Grid GetWharehouseExplorerView(int? posStart = 0, int? count = 20, string shipmentRef = "", long? carriersId = null, long? originId = null,
            long? destId = null, long? warehouseId = null, long? zoneId = null, long? locationId = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            return MakePageableGrid("MCHNew_WharehouseExplorerView", "ShipmentId", posStart, count.GetValueOrDefault(20), parameters: new List<KeyValuePair<string, object>> 
            {
                new KeyValuePair<string, object>("@ShipmentRef", shipmentRef),
                new KeyValuePair<string, object>("@CarriersId", carriersId),
                new KeyValuePair<string, object>("@OriginId", originId),
                new KeyValuePair<string, object>("@DestId", destId),
                new KeyValuePair<string, object>("@WarehouseId", warehouseId),
                new KeyValuePair<string, object>("@ZoneId", zoneId),
                new KeyValuePair<string, object>("@LocationId", locationId),
                new KeyValuePair<string, object>("@FromDate", fromDate),
                new KeyValuePair<string, object>("@ToDate", toDate)
            });
        }

        public Grid GetWharehouseInventoryView(int? posStart = 0, int? count = 20, long? warehouseId = null, DateTime? inventoryDate = null, long? taskId = null)
        {
            return MakePageableGrid("MCHNew_WharehouseInventoryView", "Key", posStart, count.GetValueOrDefault(20), parameters: new List<KeyValuePair<string, object>> 
            {
                new KeyValuePair<string, object>("@WarehouseId", warehouseId),
                new KeyValuePair<string, object>("@InventoryDate", inventoryDate),
                new KeyValuePair<string, object>("@TaskId", taskId)
            });
        }

        public IEnumerable<ValueText> GetInventoryRuns(long warehouseId, DateTime? date = null)
        {
            DataProvider.CommandType = CommandType.StoredProcedure;
            var a = GetComboData("MCHNew_WharehouseInventoryRuns", "TaskId", "Reference",
                new List<KeyValuePair<string, object>> 
                { 
                    new KeyValuePair<string, object>("@WarehouseId", warehouseId), 
                    new KeyValuePair<string, object>("@InventoryDate", date)                        
                }).ToList();
            return a;
        }

        public Grid GetLocationDetails(string parentId, string entityType, string taskId, string gridType)
        {
            long shipmentId;
            long tId;
            var et = entityType == "AWB" ? true : false;

            if (gridType == "warehouse")
            {
                shipmentId = long.Parse(parentId);
                tId = long.Parse(taskId);
            }
            else
            {
                shipmentId = long.Parse(parentId.Split('_')[0]);
                tId = long.Parse(parentId.Split('_')[2]);
            }

            DataProvider.CommandType = CommandType.StoredProcedure;
            var data = DataProvider.GetDataAsString("MCHNew_GetLocationDetails", "Id",
                new List<KeyValuePair<string, object>> 
                { 
                    new KeyValuePair<string, object>("@ShipmentId", shipmentId), 
                    new KeyValuePair<string, object>("@TaskId", tId),
                    new KeyValuePair<string, object>("@EntityType", et),
                });
            return new Grid
            {
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        public Grid GetHistory(string parentId, string entityType, string warehouseId, string gridType)
        {
            var shipmentId = gridType == "warehouse" ? long.Parse(parentId) : long.Parse(parentId.Split('_')[0]);
            var et = entityType == "AWB" ? 2 : 3;

            var query = string.Format(@"Select Description, 
                dbo.MCH_ConvertToLocalDateTime({0}, StatusTimestamp) As Timestamp,
                UP.UserId As UserName, Transactions.Id
                from Transactions left join UserProfiles UP on Transactions.UserId = UP.Id
                Where EntityId = {1}
                And EntityTypeId = {2}", warehouseId, shipmentId, et);

            var data = DataProvider.GetDataAsString(query, "Id");
            return new Grid
            {
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        public Form<ShipmentDetails> GetDetails(string parentId, string entityType, string warehouseId, string gridType)
        {
            var shipmentId = gridType == "warehouse" ? long.Parse(parentId) : long.Parse(parentId.Split('_')[0]);
            var et = entityType == "AWB" ? 2 : 3;

            DataProvider.CommandType = CommandType.StoredProcedure;

            var details = DataProvider.FillSingleOrDefailt("MCHNew_GetShipmentDetails", row => new ShipmentDetails
            {
                CarrierCode = row["CarrierCode"].ToString(),
                ShipmentReference = row["ShipmentRef"].ToString(),
                Origin = row["Origin"].ToString(),
                Destination = row["Destination"].ToString(),
                TotalPieces = Convert.ToInt32(row["TPieces"]),
                TotalWeight = row["TWeight"].ToString(),
                HandlingCodes = row["HandlingCodes"].ToString(),
                Overpack = row["Overpack"].ToString(),
                Shipper = new Partner
                {
                    Name = row["Shipper"].ToString(),
                    Address = row["ShipperAddress"].ToString(),
                    City = row["ShipperCity"].ToString(),
                    Postal = row["ShipperPostal"].ToString(),
                    Country = row["ShipperCountry"].ToString(),
                    State = row["ShipperState"].ToString()
                },
                Consignee = new Partner
                {
                    Name = row["Consignee"].ToString(),
                    Address = row["ConsigneeAddress"].ToString(),
                    City = row["ConsigneeCity"].ToString(),
                    Postal = row["ConsigneePostal"].ToString(),
                    Country = row["ConsigneeCountry"].ToString(),
                    State = row["ConsigneeState"].ToString()
                }
            },
            new List<KeyValuePair<string, object>> 
                { 
                    new KeyValuePair<string, object>("@EntityId", shipmentId), 
                    new KeyValuePair<string, object>("@EntityTypeId", et),
                    new KeyValuePair<string, object>("@WarehouseId", warehouseId)
                });

            return new Form<ShipmentDetails> { data = details };
        }

        public IEnumerable<MediaItem> GetMediaDetails(string parentId, string entityType, string warehouseId, string gridType)
        {
            var shipmentId = gridType == "warehouse" ? long.Parse(parentId) : long.Parse(parentId.Split('_')[0]);
            var et = entityType == "AWB" ? 2 : 3;

            var query = string.Format(@"Select 
                SnapshotImage As Media,
                DisplayText as Condition,
                Pieces,
                dbo.MCH_ConvertToLocalDateTime({0}, Timestamp) As Timestamp,
                UP.FirstName + ' ' + UP.LastName As UserName
                from Snapshots
                left Join UserProfiles UP On UP.Id = Snapshots.UserId
                Where EntityId = {1}
                And EntityTypeId = {2}", warehouseId, shipmentId, et);

            return DataProvider.Fill<IEnumerable<MediaItem>>(query, rows => rows.Select(r => new MediaItem
            {
                Image = r["Media"].ToString(),
                Condition = r["Condition"].ToString(),
                Pieces = Convert.ToInt32(r["Pieces"]),
                Date = Convert.ToDateTime(r["Timestamp"]).ToString("MM/dd/yyyy HH:mm"),
                UserName = r["UserName"].ToString()
            }).ToList());
        }

        public IEnumerable<AccountCarrier> GetAccountCarriers()
        {
            return DataProvider.Fill<IEnumerable<AccountCarrier>>(string.Format(@"Select Distinct Carriers.Id as CarrierId, CarrierName + ' (' + CarrierCode + ')' as CarrierName, 
                                Carrier3Code, Accounts.Id as AccountId
                                 from Accounts
                                 Inner Join AccountCarriers On AccountCarriers.AccountId = Accounts.Id
                                 Inner Join Carriers on AccountCarriers.CarrierId = Carriers.Id
                                 Where IsOwner = 0 and IsActive = 1
                                group by Carriers.Id, Accounts.Id, CarrierName, CarrierCode, Carrier3Code"), rows => rows.Select(r => new AccountCarrier
                                                               {
                                                                   value = r["CarrierId"].ToString(),
                                                                   text = r["CarrierName"].ToString(),
                                                                   Carrier3Code = r["Carrier3Code"].ToString(),
                                                                   AccountId = Convert.ToInt64(r["AccountId"])
                                                               }));
        }

        [HttpGet]
        public AwbBasicInfo TryFindAwb(string carrier, string awbNumber, long originId, long destinationId)
        {
            var query = string.Format(
                            @"Select Awbs.Id, Awbs.AwbSerialNumber, Carriers.Id as CarrierId, Carriers.Carrier3Code, Awbs.OriginId, Awbs.DestinationId,
                                Awbs.TotalPieces, Awbs.TotalWeight, Shipper.Id as ShipperId, Shipper.Name as ShipperName, 
                                Consignee.Id as ConsigneeId, Consignee.Name as ConsigneeName
                            from Awbs inner join Carriers On Carriers.Id = Awbs.CarrierId
                            Left Join Customers as Shipper On Shipper.Id = Awbs.ShipperId
                            Left Join Customers as Consignee On Consignee.Id = Awbs.ConsigneeId
                            Where Awbs.AwbSerialNumber = '{0}' and Carrier3Code = '{1}'
                            and Awbs.OriginId = {2} and Awbs.DestinationId = {3}", awbNumber, carrier, originId, destinationId);

            return DataProvider.FillSingleOrDefailt<AwbBasicInfo>(query,
            row =>
            {
                if (row == null) return null;
                return new AwbBasicInfo
                {
                    Id = Convert.ToInt64(row["Id"]),
                    SerialNumber = row["AwbSerialNumber"].ToString(),
                    CarrierId = Convert.ToInt64(row["CarrierId"]),
                    Carrier3Code = row["Carrier3Code"].ToString(),
                    OriginId = Convert.ToInt64(row["OriginId"]),
                    DestinationId = Convert.ToInt64(row["DestinationId"]),
                    TotalPieces = Convert.ToInt32(row["TotalPieces"]),
                    TotalWeight = row["TotalWeight"].ToString(),
                    ShipperId = string.IsNullOrEmpty(row["ShipperId"].ToString()) ? null : (long?)Convert.ToInt64(row["ShipperId"]),
                    ShipperName = row["ShipperName"].ToString(),
                    ConsigneeId = string.IsNullOrEmpty(row["ConsigneeId"].ToString()) ? null : (long?)Convert.ToInt64(row["ConsigneeId"]),
                    ConsigneeName = row["ConsigneeName"].ToString(),
                };
            });
        }

        [HttpGet]
        public long AddAwb(long accountId, string carrierNumber, string awbSerialNumber, long originId, long destinationId, long totalPieces, long warehouseId, bool isComplete, string shipperName = null, string consigneeName = null)
        {
            DataProvider.CommandType = CommandType.StoredProcedure;
            var result = DataProvider.ExecuteScalar("MCH_ManualAddAWB", new List<KeyValuePair<string, object>> 
                { 
                    new KeyValuePair<string, object>("@accountId", accountId), 
                    new KeyValuePair<string, object>("@carrierNumber", carrierNumber),
                    new KeyValuePair<string, object>("@awbSerialNumber", awbSerialNumber),
                    new KeyValuePair<string, object>("@originId", originId),
                    new KeyValuePair<string, object>("@DestinationId", destinationId),
                    new KeyValuePair<string, object>("@totalPieces", totalPieces),
                    new KeyValuePair<string, object>("@shipperName", shipperName),
                    new KeyValuePair<string, object>("@consigneeName", consigneeName),
                    new KeyValuePair<string, object>("@userId", 3),
                    new KeyValuePair<string, object>("@warehouseId", 1),
                });

            var awbId = Convert.ToInt64(result);
            if (awbId == -1)
                return -1;
            if (isComplete)
                CompleteManualAccept(awbId, 3, 1);
            return awbId;
        }

        [HttpGet]
        public void CompleteManualAccept(long awbId, long userId, long warehouseId)
        {
            DataProvider.CommandType = CommandType.StoredProcedure;
            DataProvider.ExecuteScalar("MCH_CompleteManualAccept", new List<KeyValuePair<string, object>> 
                {                     
                    new KeyValuePair<string, object>("@awbId", awbId),
                    new KeyValuePair<string, object>("@userId", userId),
                    new KeyValuePair<string, object>("@warehouseId", warehouseId),
                });
        }

        public Grid GetAwbHwbs(long awbId)
        {
            var data = DataProvider.GetDataAsString(string.Format(
                    @"Select Origin.IATACode + '-' + Hwbs.HwbSerialNumber + '-' + Destination.IATACode as HwbSerialNumber, Hwbs.Pieces,
                        Shipper.Name as ShipperName, Consignee.Name as ConsigneeName,                       
                        Hwbs.Id
                    from AWBs_HWBs Inner Join Hwbs On Hwbs.Id = AWBs_HWBs.HwbId
                    Inner Join Ports as Origin on Origin.Id = Hwbs.OriginId
                    Inner Join Ports as Destination on Destination.Id = Hwbs.DestinationId
                    Left Join Customers as Shipper on Shipper.Id = Hwbs.ShipperId
                    Left Join Customers as Consignee on Consignee.Id = Hwbs.ConsigneeId
                    Where AWBs_HWBs.AwbId = {0}", awbId), "Id");
            return new Grid
            {
                rows = data.Select(d => new Row
                {
                    id = d.Key,
                    data = d.Value
                })
            };
        }

        [HttpGet]
        public void AddAwbHwb(long awbId, string hwbSerialNumber, long originId, long destinationId, int totalPieces, string shipperName = null, string consigneeName = null)
        {
            DataProvider.CommandType = CommandType.StoredProcedure;

            DataProvider.ExecuteScalar("MCH_ManualAddHWB", new List<KeyValuePair<string, object>> 
                {                     
                    new KeyValuePair<string, object>("@awbId", awbId),
                    new KeyValuePair<string, object>("@hwbSerialNumber", hwbSerialNumber),
                    new KeyValuePair<string, object>("@originId", originId),
                    new KeyValuePair<string, object>("@DestinationId", destinationId),
                    new KeyValuePair<string, object>("@totalPieces", totalPieces),
                    new KeyValuePair<string, object>("@shipperName", shipperName),
                    new KeyValuePair<string, object>("@consigneeName", consigneeName),
                    new KeyValuePair<string, object>("@userId", 3),
                    new KeyValuePair<string, object>("@warehouseId", 1),
                });
        }

        [HttpGet]
        public HttpResponseMessage ExportToExcel(string shipmentRef = "", long? carriersId = null, long? originId = null,
            long? destId = null, long? warehouseId = null, long? zoneId = null, long? locationId = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            DataProvider.CommandType = CommandType.StoredProcedure;

            var data = DataProvider.Fill("MCHNew_WharehouseExplorerView",
                parameters: new List<KeyValuePair<string, object>> 
                {
                    new KeyValuePair<string, object>("@Start", 0),
                    new KeyValuePair<string, object>("@End", 65000),
                    new KeyValuePair<string, object>("@ShipmentRef", shipmentRef),
                    new KeyValuePair<string, object>("@CarriersId", carriersId),
                    new KeyValuePair<string, object>("@OriginId", originId),
                    new KeyValuePair<string, object>("@DestId", destId),
                    new KeyValuePair<string, object>("@WarehouseId", warehouseId),
                    new KeyValuePair<string, object>("@ZoneId", zoneId),
                    new KeyValuePair<string, object>("@LocationId", locationId),
                    new KeyValuePair<string, object>("@FromDate", fromDate),
                    new KeyValuePair<string, object>("@ToDate", toDate)
                },
                objectInitializer: rows => rows.Select(r =>
                {
                    var list = new List<string>();
                    for (var i = 0; i < r.Table.Columns.Count - 4; ++i)
                        list.Add(r[i].ToString());
                    return list;
                }).ToList());

            var excelData = new ExcelData
            {
                Headers = new List<string> { "Carrier", "Shipment", "Origin", "Dest", "Pieces", "Weight", "First Scan", "Last Inventory", "Duration", "Overage", "Shotage", "Damages", "Left Behind" },
                SheetName = "EXPLORER",
                DataRows = data
            };

            var excelManager = new ExcelManager();
            var excel = excelManager.GenerateExcel(excelData);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(new MemoryStream(excel));
            result.Content.Headers.Add("Content-Disposition", "attachment; filename=download.xlsx");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return result;
        }

        [HttpGet]
        public HttpResponseMessage ExportInventoryView(long? warehouseId = null, DateTime? inventoryDate = null, long? taskId = null)
        {
            DataProvider.CommandType = CommandType.StoredProcedure;

            var data = DataProvider.Fill("MCHNew_WharehouseInventoryView",
                parameters: new List<KeyValuePair<string, object>> 
                {
                    new KeyValuePair<string, object>("@Start", 0),
                    new KeyValuePair<string, object>("@End", 65000),
                    new KeyValuePair<string, object>("@WarehouseId", warehouseId),
                    new KeyValuePair<string, object>("@InventoryDate", inventoryDate),
                    new KeyValuePair<string, object>("@TaskId", taskId)
                },
                objectInitializer: rows => rows.Select(r =>
                {
                    var list = new List<string>();
                    for (var i = 0; i < r.Table.Columns.Count - 5; ++i)
                        list.Add(r[i].ToString());
                    return list;
                }).ToList());

            var excelData = new ExcelData
            {
                Headers = new List<string> { "Carrier", "Shipment", "Origin", "Dest", "Pieces", "Weight", "First Scan", "Last Inventory", "Damage", "Overage", "Shotage", "Left Behind" },
                SheetName = "INVENTORY",
                DataRows = data
            };

            var excelManager = new ExcelManager();
            var excel = excelManager.GenerateExcel(excelData);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(new MemoryStream(excel));
            result.Content.Headers.Add("Content-Disposition", "attachment; filename=download.xlsx");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return result;
        }

        public IEnumerable<MenuItem> GetMenu()
        {
            return new List<MenuItem> 
            { 
                new MenuItem
                {
                    id="warehouseExplorer",
                    text = "Warehouse Expolorer",
                    type = "item", 
                    href_link = "http://localhost:52857"             
                }, 
                new MenuItem
                {
                    id="cargoDischarge",
                    text = "Cargo Discharge",
                    type = "item",                    
                    href_link = "http://localhost:52857/Home/CargoDischarge",
                },
                new MenuItem
                {
                    id="logout",
                    text = "LOG OUT",
                    type = "item",
                }
            };
        }
    }
}