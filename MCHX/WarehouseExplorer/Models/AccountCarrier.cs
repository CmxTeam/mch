﻿using DhtmlxComponents.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WarehouseExplorer.Models
{
    public class AccountCarrier : ValueText
    {
        public long AccountId { get; set; }
        public long CarrierId { get; set; }
        public string CarrierCode { get; set; }
        public string Carrier3Code { get; set; }
        public string CarrierName { get; set; }
    }
}