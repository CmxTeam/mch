﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WarehouseExplorer.Models
{
    public class AwbBasicInfo
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public long CarrierId { get; set; }
        public string Carrier3Code{get; set;}
        public long OriginId { get; set; }
        public long DestinationId { get; set; }
        public int TotalPieces { get; set; }
        public string TotalWeight { get; set; }
        public long? ShipperId{get; set;}
        public string ShipperName{get; set;}
        public long? ConsigneeId{get; set;}
        public string ConsigneeName{get; set;}
    }
}