﻿using System.Web.Mvc;

namespace WarehouseExplorer.Controllers
{
    [Authorize]
    public class AccountController : SharedViews.Controllers.LoginBaseController
    {   
        public override bool UseShell
        {
            get { return true; }
        }        
    }
}