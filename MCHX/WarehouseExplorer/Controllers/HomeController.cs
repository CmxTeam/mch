﻿using SharedViews.Attributes;
using SharedViews.Controllers;
using System.Web.Mvc;

namespace WarehouseExplorer.Controllers
{
    public class HomeController : LayoutBaseController
    {
        [Authorize]
        [LayoutTypeAttribute("3L")]
        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        [LayoutTypeAttribute("2U")]
        public ActionResult CargoDischarge()
        {                       
            return View();
        }        
    }
}