﻿using System.Web;
using System.Web.Optimization;

namespace WarehouseExplorer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/dhtmlx").Include(
                        "~/Scripts/dhtmlx.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageCapturer").Include(
                        "~/Scripts/image-capture.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/custom/warehouseFilters.js",
                        "~/Scripts/custom/warehouseGrid.js",
                        "~/Scripts/custom/inventoryFilters.js",
                        "~/Scripts/custom/inventoryGrid.js",
                        "~/Scripts/custom/detailsView.js",
                        "~/Scripts/custom/acceptanceWindow.js",
                        "~/Scripts/custom/addHwbWindow.js",
                        "~/Scripts/helpers.js",
                        "~/Scripts/submitForm.js",
                        "~/Scripts/cargoDischarge/dischargeTransactionsFilter.js",
                        "~/Scripts/cargoDischarge/dischargeTransactions.js",
                        "~/Scripts/cargoDischarge/dischargeDetails.js",                                    
                        "~/Scripts/cargoDischarge/DriverDetails.js",
                        "~/Scripts/cargoDischarge/NewDriver.js",
                        "~/Scripts/cargoDischarge/newTruckingCompany.js",
                        "~/Scripts/cargoDischarge/SelectedShipments.js",
                        "~/Scripts/cargoDischarge/ShipmentDetails.js",
                        "~/Scripts/cargoDischarge/ShipmentHistory.js",
                        "~/Scripts/cargoDischarge/dischargeTransactionWindow.js",
                        "~/Scripts/cargoDischarge/attachments.js"));
            

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/dhtmlx.css",
                      "~/Content/site.css"));
        }
    }
}
