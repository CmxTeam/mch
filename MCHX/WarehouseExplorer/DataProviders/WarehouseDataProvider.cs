﻿using System;

namespace WarehouseExplorer.DataProviders
{
    [Obsolete("Will be removed soon. Use self hosted Data Providers instead.")]
    public class WarehouseDataProvider : BaseDataProvider, IDataProvider
    {
        public override string DatabaseName
        {
            get { return "WarehouseManagement"; }
        }
    }
}