﻿using System;

namespace WarehouseExplorer.DataProviders
{
    [Obsolete("Will be removed soon. Use self hosted Data Providers instead.")]
    public enum CommandType
    {
        Text = 1,
        StoredProcedure = 2
    }
}