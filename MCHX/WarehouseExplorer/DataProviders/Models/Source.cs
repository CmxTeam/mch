﻿using System.Collections.Generic;

namespace WarehouseExplorer.DataProviders.Models
{
    public class Source
    {
        public int Count { get; set; }
        public IEnumerable<SourceItem> Data { get; set; }
    }

    public class SourceItem 
    {
        public string Id { get; set; }
        public IEnumerable<string> Cells { get; set; }
    }
}
