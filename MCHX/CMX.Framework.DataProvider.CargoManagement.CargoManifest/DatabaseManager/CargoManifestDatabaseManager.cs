﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.RecoveryIssueReasons;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.wspieceScan;
using CMX.Framework.DataProvider.DataProviders;
using RecoveryIssueReason = CMX.Framework.DataProvider.CargoManagement.CargoManifest.RecoveryIssueReasons.RecoveryIssueReason;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.DatabaseManager
{
    public partial class ExportDatabaseManager : CmCommonDatabaseManager
    {
        /// <summary>
        /// Get filter list
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetFilterBy(string department)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new FilterBy(department));
        }

        /// <summary>
        /// Get search list result
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <param name="userId"></param>
        /// <param name="filter"></param>
        /// <param name="selDate"></param>
        /// <returns></returns>
        public DhtmlxTree GetSearchList(string gatewayId, string userId, string filter, string selDate)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new SearchResult(gatewayId, userId, filter, selDate));

        }

        /// <summary>
        /// Get legent 
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetLegend(string mode)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new Legend(mode)));
        }

        /// <summary>
        /// Titles of shipments tab items
        /// </summary>
        /// <param name="gateway"></param>
        /// <param name="destination"></param>
        /// <param name="date"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GeTabItems(string gateway, string destination, string date, string account)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new TabCodeGet(gateway, destination, date, account)));
        }

        /// <summary>
        /// Get shipments by code
        /// </summary>
        /// <param name="code"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public Shimpent<GridUserData> GetShipmentsByCode(string code, string account)
        {
            var connectorExecutor = new ConnectorExecutor();
            var columnNames = new List<Dictionary<string, object>>()
            {
                 new Dictionary<string, object>(){{"FieldBaseColumn","Status"},{"FieldName","Status"}, {"FieldFormat",""},{"FieldOrder","0"}},
            };

            columnNames.AddRange(connectorExecutor.Execute((() => new ShipmentSettingsGet(account))));

            var shipment = new Shimpent<GridUserData>
            {
                ColumnNames = string.Join(",", columnNames.Select(o => o["FieldName"]))
            };

            var data = connectorExecutor.Execute((() => new ShipmentsByCodeGet(code)));

            if (!data.Any()) return shipment;
            var columns = data.First().Keys.Where(o => o.Contains("Image")).ToList();

            var dgrid = new DhtmlxGrid<GridUserData>();

            var totalPcs = 0;
            double totalGrossW = 0;
            double volume = 0;
            double chargeWeight = 0;
            double ftc = 0;

            var lstWidth = "*,";
            var mappingWidth = GetMappingWidth();

            foreach (var column in columnNames)
            {
                var wid = mappingWidth.Where(o => o["FieldBaseColumn"].Equals(column["FieldBaseColumn"])).ToList();
                if (wid.Count > 0)
                {
                    lstWidth += string.Format("{0},", wid[0]["Width"]);
                }
            }
            lstWidth = lstWidth.TrimEnd(',');
            shipment.Widths = lstWidth;


            foreach (var node in data.ToList())
            {
                var icons = string.Empty;

                for (var j = 0; j < columns.Count(); j++)
                {
                    if (!string.IsNullOrEmpty(node[columns[j]].ToString()))
                    {
                        icons += string.Format(" <img src='{0}{1}'/> ", ServerInformation.BaseUrl, node[columns[j]].ToString().Replace(".png", ".jpg"));
                    }
                }


                if (bool.Parse(node["TB"].ToString()))
                {
                    icons = "<img src='" + ServerInformation.BaseUrl + "Images/perishable.gif'/>" + icons;
                }

                if (bool.Parse(node["EX"].ToString()))
                {
                    icons = "<img src='" + ServerInformation.BaseUrl + "Images/exclamation.gif' alt='Expedite Shipment'/>" + icons;
                }
                if (bool.Parse(node["CS"].ToString()))
                {
                    icons = "<img src='" + ServerInformation.BaseUrl + "Images/barscan.gif'/>" + icons;
                }

                if (bool.Parse(node["HZ"].ToString()))
                {
                    icons = "<img src='" + ServerInformation.BaseUrl + "Images/Blinking_Lamps.gif'/>" + icons;
                }

                if (bool.Parse(node["UK"].ToString()))
                {
                    icons = "<img src='" + ServerInformation.BaseUrl + "Images/UKS_new.gif' alt='Ready'/>" + icons;
                }
                if (node["HD"].ToString().Equals("1"))
                {
                    icons = "<img src='" + ServerInformation.BaseUrl + "Images/stop.gif' alt='Not Ready'/>" + icons;
                }
                else
                {
                    if (node["Status"].ToString().Equals("0"))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/Greentick.gif' alt='Ready'/>" + icons;
                    }
                    else
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/stop.gif' alt='Not Ready'/>" + icons;
                    }
                }
                node["Status"] = icons;
                //}
                //griddata.Add(icons);

                var griddata = DhtmlxConverer.ToGridRow(node, columnNames);

                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {

                    data = griddata,
                    id = data.IndexOf(node) + 1,
                    //userdata = new List<UserData>()
                    //        {
                    //            new UserData(){hotdeal = node["Origin"].ToString() , name = "Origin"},
                    //            new UserData(){content = node["AirbillNo"].ToString(), name = "AirbillNo"},
                    //            new UserData(){content = node["Destination"].ToString(), name = "Destination"}
                    //        }
                    userdata = new GridUserData()
                    {
                        AirbillNo = node["AirbillNo"].ToString(),
                        Destination = node["Destination"].ToString(),
                        Origin = node["Origin"].ToString()
                    }
                });

                /*total pieces*/
                var piece = node["NoOfPackages"].ToString().Trim().Replace("Of", " ").Split(' ').Last().Trim();
                totalPcs += int.Parse(piece);
                /*gross weight*/
                var gross = node["GrossWeight"].ToString();
                totalGrossW += double.Parse(gross);
                /*volume*/
                var cf = node["Volume"].ToString();
                volume += double.Parse(cf);
                /*ch wt*/
                var chWeight = node["ChargeableWeight"].ToString();
                chargeWeight += double.Parse(chWeight);
                /*ftc*/
                var wCharge = node["WeightCharge"].ToString();
                ftc += double.Parse(wCharge.Replace(',', '.'));
            }

            shipment.data = dgrid;
            shipment.total_hawb = data.Count;
            shipment.total_pcs = totalPcs;
            shipment.total_gross_w = string.Format("{0:0.##} KG", totalGrossW);
            shipment.total_cf = string.Format("{0}", volume);
            shipment.total_ch_wt = string.Format("{0:0.##} KG", chargeWeight);
            shipment.total_ftc = string.Format("${0:#,###.##}", ftc);

            return shipment;
        }


        /// <summary>
        /// Assign console
        /// </summary>
        /// <param name="cargoManifest"></param>
        /// <param name="gateway"></param>
        /// <param name="destination"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public long AssignConsole(string cargoManifest, string gateway, string destination, string account)
        {
            var connectorExecutor = new ConnectorExecutor();
            var planId = DateTime.Now.ToString("MdyyHHmmss");
            //account ?? 
            var result = connectorExecutor.Execute((() => new GetLoadingPlan(gateway, destination, string.Empty, planId)));

            foreach (var item in cargoManifest.Split(',').ToList())
            {
                var airbilNo = item.Split('-')[1];
                var origin = item.Split('-')[0];

                connectorExecutor.Execute(
                    (() => new CargoManifestPlanInsert(airbilNo, origin, long.Parse(planId), "LOOSE")));
            }
            return result;
        }

        private List<Dictionary<string, object>> GetMappingWidth()
        {
            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","AirbillNo"},{"Width","70"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ChargeableWeight"},{"Width","80"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Destination"},{"Width","45"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","DimWeight"},{"Width","80"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","GrossWeight"},{"Width","80"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","NoOfPackages"},{"Width","70"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Origin"},{"Width","55"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Rate"},{"Width","60"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReceiveDate"},{"Width","120"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReceiveLocation"},{"Width","140"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReceiverName"},{"Width","160"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","SenderName"},{"Width","150"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ServiceType"},{"Width","60"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ShipmentDate"},{"Width","80"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Slac"},{"Width","50"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Volume"},{"Width","60"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","WeightCharge"},{"Width","60"}}
            };
            return columnNames;
        }

        /// <summary>
        /// Get shipment table list
        /// </summary>
        /// <param name="gateway"></param>
        /// <param name="destination"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public List<Shimpent<GridUserData>> GetShipments(string gateway, string destination, string account)
        {
            var connectorExecutor = new ConnectorExecutor();

            var columnNames = new List<Dictionary<string, object>>()
            {
                 new Dictionary<string, object>(){{"FieldBaseColumn","Status"},{"FieldName","Status"}, {"FieldFormat",""},{"FieldOrder","0"}},
            };

            columnNames.AddRange(connectorExecutor.Execute((() => new ShipmentSettingsGet(account))));

            var data = connectorExecutor.Execute((() => new ShipmentList(gateway, destination, string.Empty)));
            var gridNodes = connectorExecutor.Execute(() => new GetGridNodes());

            var shipments = new List<Shimpent<GridUserData>>();

            foreach (var t in gridNodes)
            {
                var item = new Shimpent<GridUserData>()
                {
                    allowSelect = !t["AllowSelect"].ToString().Equals("1"),
                    id = int.Parse(t["ID"].ToString()),
                    nodeText = t["NodeDescription"].ToString(),
                    nodeValue = t["NodeName"].ToString(),
                    ColumnNames = string.Join(",", columnNames.Select(o => o["FieldName"]))
                };

                var lstWidth = "*,";
                var mappingWidth = GetMappingWidth();

                foreach (var column in columnNames)
                {
                    var wid = mappingWidth.Where(o => o["FieldBaseColumn"].Equals(column["FieldBaseColumn"])).ToList();
                    if (wid.Count > 0)
                    {
                        lstWidth += string.Format("{0},", wid[0]["Width"]);
                    }
                }
                lstWidth = lstWidth.TrimEnd(',');
                item.Widths = lstWidth;

                var nodes = data.Where(o => o["NodeName"].Equals(t["NodeName"]) || o["NodeName"].ToString().Contains("Image")).ToList();

                if (nodes.Any())
                {
                    var columns = nodes.First().Keys.Where(o => o.Contains("Image")).ToList();

                    var dgrid = new DhtmlxGrid<GridUserData>();

                    var totalPcs = 0;
                    double totalGrossW = 0;
                    double volume = 0;
                    double chargeWeight = 0;
                    double ftc = 0;

                    foreach (var node in nodes.ToList())
                    {
                        var icons = string.Empty;

                        for (var j = 0; j < columns.Count(); j++)
                        {
                            if (!string.IsNullOrEmpty(node[columns[j]].ToString()))
                            {
                                icons += string.Format(" <img src='{0}{1}'/> ", ServerInformation.BaseUrl, node[columns[j]].ToString().Replace(".png", ".jpg"));
                            }
                        }

                        //var shipment = node.Where(o => o.Key.Equals("TB")).ToList();
                        //if (shipment.Count > 0)
                        //{
                        if (bool.Parse(node["TB"].ToString()))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/perishable.gif'/>" + icons;
                        }
                        if (bool.Parse(node["BN"].ToString()))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/bnshipmentLogins.gif'/>" + icons;
                        }
                        if (bool.Parse(node["EX"].ToString()))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/exclamation.gif' alt='Expedite Shipment'/>" + icons;
                        }
                        if (bool.Parse(node["CS"].ToString()))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/InboundScan.gif'/>" + icons;
                        }

                        if (bool.Parse(node["HZ"].ToString()))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/Blinking_Lamps.gif'/>" + icons;
                        }

                        if (bool.Parse(node["UK"].ToString()))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/UKS_new.gif' alt='Ready'/>" + icons;
                        }
                        if (node["HD"].ToString().Equals("1"))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/stop.gif' alt='Not Ready'/>" + icons;
                        }
                        else
                        {
                            if (node["Status"].ToString().Equals("0"))
                            {
                                icons = "<img src='" + ServerInformation.BaseUrl + "Images/Greentick.gif' alt='Ready'/>" + icons;
                            }
                            else
                            {
                                icons = "<img src='" + ServerInformation.BaseUrl + "Images/stop.gif' alt='Not Ready'/>" + icons;
                            }
                        }
                        node["Status"] = icons;

                        var griddata = DhtmlxConverer.ToGridRow(node, columnNames);

                        dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                        {
                            bgColor = t["NodeColor"].ToString(),
                            data = griddata,
                            id = nodes.IndexOf(node) + 1,

                            userdata = new GridUserData()
                            {
                                AirbillNo = node["AirbillNo"].ToString(),
                                Destination = node["Destination"].ToString(),
                                Origin = node["Origin"].ToString(),
                                ZipCode = node["DomesticPostal"].ToString()
                            }
                        });
                        /*total pieces*/
                        var piece = node["NoOfPackages"].ToString().Trim().Replace("Of", " ").Split(' ').Last().Trim();
                        totalPcs += int.Parse(piece);
                        /*gross weight*/
                        var gross = node["GrossWeight"].ToString();
                        totalGrossW += double.Parse(gross.Replace(',', '.'));
                        /*volume*/
                        var cf = node["Volume"].ToString();
                        volume += double.Parse(cf.Replace(',', '.'));
                        /*ch wt*/
                        var chWeight = node["ChargeableWeight"].ToString();
                        chargeWeight += double.Parse(chWeight.Replace(',', '.'));
                        /*ftc*/
                        var wCharge = node["WeightCharge"].ToString();
                        ftc += double.Parse(wCharge.Replace(',', '.'));
                    }

                    item.data = dgrid;
                    item.total_hawb = nodes.Count;
                    item.total_pcs = totalPcs;
                    item.total_gross_w = string.Format("{0:#,###.##} KG", totalGrossW);
                    item.total_cf = string.Format("{0:#,###.##}", volume);
                    item.total_ch_wt = string.Format("{0:#,###.##} KG", chargeWeight);
                    item.total_ftc = string.Format("${0:#,###.##}", ftc);
                }

                shipments.Add(item);
            }

            return shipments;
        }

        /// <summary>
        /// Get column settings
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetColumnSettings(string user)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentSettingsGet(user)));
        }

        /// <summary>
        /// Grid nodes list
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetGridNodesList()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new GetGridNodes());
        }

        ///
        public bool DeleteLoadingPlan(string cargoManifestId)
        {
            var connectorExecutor = new ConnectorExecutor();
            var delPlan = connectorExecutor.Execute((() => new CargoManifestPlanRemove(cargoManifestId)));
            var delSumPlan = connectorExecutor.Execute((() => new CargoManifestPlanSummaryRemove(cargoManifestId)));

            return delSumPlan & delPlan;
        }

        /// <summary>
        /// Shipments details 
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetDimensions(string airbillNo, string origin)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentsDetailsDimGet(airbillNo, origin)));
        }

        /// <summary>
        /// Details: get remarks list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetRemarksDetails(string airbillNo, string origin)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentsDetailsRemarksGet(airbillNo, origin)));
        }

        /// <summary>
        /// Details: get overview data
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetOverView(string airbillNo, string origin)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentsDetailsOverviewGet(airbillNo, origin)));
        }

        public bool PrintHawb(string userId, string airbill, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new PrintHawb(userId, airbill, gateway)));
        }

        public bool PrintHawbItar(string userId, string airbill, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new PrintHawbItar(userId, airbill, gateway)));
        }

        public bool PrintHawbPo(string userId, string airbill, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new PrintHawbPo(userId, airbill, gateway)));
        }

        /// <summary>
        /// Details: Get charges data
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetCharges(string airbillNo, string origin)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentDetailsChargesGet(airbillNo, origin)));
        }

        /// <summary>
        /// Details: get references list data
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetReferences(string airbillNo, string origin)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentDetailsReferencesGet(airbillNo, origin)));
        }

        public bool InsertReference(string housebill, string origin, string reference)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new InsertReference(housebill, origin, reference)));
        }

        /// <summary>
        /// Details: get damages
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetDamages(string airbillNo)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentDetailsDamagesGet(airbillNo)));
        }

        /// <summary>
        /// Details: get locations data list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> GetLocations(string airbillNo)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ShipmentDetailsLoacationsGet(airbillNo)));
        }

        /// <summary>
        ///  Hold/Release shipment
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> ShipmentHoldReleaseGet(string airbillNo, string origin)
        {
            var connectorExecutor = new ConnectorExecutor();
            var data = connectorExecutor.Execute((() => new HawbGet(airbillNo, origin)));
            if (data.Count < 0) return new List<Dictionary<string, object>>();

            var status = bool.Parse(data[0]["HD"].ToString()) ? "Release" : "Hold";
            data[0].Add("StatusDescription", status);
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <param name="reason"></param>
        /// <param name="gateway"></param>
        /// <param name="userId"></param>
        /// <param name="holdStatus"></param>
        /// <returns></returns>
        public bool ShipmentHoldReleaseInsert(string airbillNo, string origin, string reason, string gateway,
            string userId, string holdStatus)
        {
            var connectorExecutor = new ConnectorExecutor();
            try
            {
                connectorExecutor.Execute(
                (() => new ShipmentHoldRelease(airbillNo, origin, reason, gateway, userId, holdStatus.ToUpper())));
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Expedite YES/ NO
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <returns></returns>
        public bool ShipmentExpediteStatus(string airbillNo)
        {
            try
            {
                var connectorExecutor = new ConnectorExecutor();
                var ex = connectorExecutor.Execute((() => new SipmentExpediteGet(airbillNo)));
                if (ex == 0)
                {
                    connectorExecutor.Execute((() => new ShipmentExpediteStatusUpdate(1, airbillNo)));
                }
                else
                {
                    connectorExecutor.Execute((() => new ShipmentExpediteStatusUpdate(0, airbillNo)));
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Change Masterbill Destination
        /// </summary>
        /// <param name="housebill"></param>
        /// <param name="newDest"></param>
        /// <param name="userId"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        public bool ChangeMasterbillDestination(string housebill, string newDest, string userId, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            var newDestData = connectorExecutor.Execute((() => new MasterbillDestinationOriginGet(newDest)));
            if (newDestData.Count > 0)
            {
                var curDest = housebill.Split('-')[1];
                var airbill = housebill.Split('-')[0];

                var data = connectorExecutor.Execute((() => new HouseAirWaybillGet(curDest, airbill)));
                if (data.Count > 0)
                {
                    connectorExecutor.Execute(
                        (() => new HouseAirWaybillUpdate(curDest, airbill, newDest)));

                    connectorExecutor.Execute((() => new DipositionTableAirbillInsert(
                        airbill,
                        userId,
                        string.Format("Masterbill Destination Changed from {0} to {1} By {2}",
                        curDest,
                        newDest,
                        userId),
                        "61")));

                    connectorExecutor.Execute((() =>
                        new HostPlusRemarksInsert(
                            airbill,
                            "0",
                            string.Format(@"Masterbill Destination Changed from {0} to {1} By {2}",
                                curDest,
                                newDest,
                                userId),
                                gateway)));
                    return true;
                }

            }
            return false;
        }

        /// <summary>
        /// Download Selected Shipments
        /// </summary>
        /// <param name="airbrilno"></param>
        /// <param name="gateway"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DhtmlxGrid<GridUserData> DownloadSelectedShipments(string airbrilno, string gateway, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute((() => new DownloadMawb(airbrilno, gateway, userId)));
            var data = connectorExecutor.Execute((() => new DownloadShipmentQueueGet(gateway)));

            foreach (var item in data.rows)
            {
                var status = item.data[1];
                if (status.Equals("0") || status.Equals("2")) item.data[1] = " <img src='" + ServerInformation.BaseUrl + "Images/pendingdownloadInpro.gif'/> ";
                else if (status.Equals("1")) item.data[1] = " <img src='" + ServerInformation.BaseUrl + "Images/downloaded.gif'/> ";
                else if (status.Equals("3")) item.data[1] = " <img src='" + ServerInformation.BaseUrl + "Images/pendingDownload.gif'/> ";
            }

            return data;
        }


        /// <summary>
        /// Update UPS Zone(s)
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetCarrierZones(string serviceType)
        {
            var connectorExecutor = new ConnectorExecutor();
            var carrierId = connectorExecutor.Execute((() => new GetUpsRecId()));

            return connectorExecutor.Execute((() => new GetCarrierZones(carrierId, serviceType)));
        }


        /// <summary>
        /// Update UPS zones
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="zoneCode"></param>
        /// <param name="gatewayId"></param>
        /// <param name="userId"></param>
        /// <param name="serviceCode"></param>
        /// <returns></returns>
        public bool UpdateUpsZone(string zipCode, string zoneCode, string gatewayId,
            string userId, string serviceCode)
        {
            var connectorExecutor = new ConnectorExecutor();
            var carrierId = connectorExecutor.Execute((() => new GetUpsRecId()));

            connectorExecutor.Execute(
                (() => new UpsZoneUpdate(zipCode, zoneCode, gatewayId, userId, carrierId, serviceCode)));
            return true;
        }

        /*
         * Update screen status
         */



        #region Add delay reason

        public RecoveryIssueReason[] GetReasons()
        {
            var reason = new RecoveryIssueReasonsSoapClient("RecoveryIssueReasonsSoap12");
            return reason.SearchRecoveryIssueReason("", "", "Export");
        }
        /*
         * Add delay reason
         */

        public bool SaveDelayReasonsHawb(string hawb, string gateway, string userId, string description)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new InsertDelayReasonHawb(hawb, gateway, userId, description)));
        }

        public bool SaveDelayReasonsMawb(string consol, string carrier, string gateway, string userId, string description)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new InsertDelayReasonMawb(consol, carrier, gateway, userId, description)));
        }

        #endregion

        /// <summary>
        /// Cancel screening
        /// </summary>
        /// <param name="airbillno"></param>
        public bool CancelScreeningByAirbillNo(string airbillno, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            var isCancelable = connectorExecutor.Execute((() => new GetHawbStatuses(airbillno)));

            if (isCancelable)
            {
                var fsp = new WSPieceScanSoapClient("WSPieceScanSoap12");
                fsp.CancelScreening("JFK", airbillno, userId);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Void selected shipment(s)
        /// </summary>
        /// <param name="shipment"></param>
        /// <param name="shipmentType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool VoidSelectedShipmnts(string shipment, string shipmentType, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute((() => new VoidSelectedShipment(shipment, shipmentType, userId)));
            return true;
        }

        /// <summary>
        /// Make Shipment Knowm/ Unknown
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <param name="userId"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        public bool ShipmentStatus(string airbillNo, string origin, string userId, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();

            var isUkOption = connectorExecutor.Execute((() => new HawbGet(airbillNo, origin)));

            if (isUkOption.First()["UK"].ToString().Equals("True"))
            {
                connectorExecutor.Execute((() => new ShipmentStatusUpdate(airbillNo, origin, "0")));
                connectorExecutor.Execute((() =>
                    new DipositionTableAirbillInsert(
                        airbillNo,
                        userId,
                        string.Format("{0}, Shipment Set to Known", userId),
                        "61")));
                connectorExecutor.Execute((() =>
                    new HostPlusRemarksInsert(
                        airbillNo,
                        "0",
                        string.Format(@"#KNOWN CUSTOMER BY {0}", userId),
                        gateway)));
            }
            else
            {
                connectorExecutor.Execute((() => new ShipmentStatusUpdate(airbillNo, origin, "1")));

                connectorExecutor.Execute((() =>
                    new DipositionTableAirbillInsert(
                        airbillNo,
                        userId,
                        string.Format("{0}, Shipment Set to Unknown", userId),
                        "62")));

                connectorExecutor.Execute((() =>
                    new HostPlusRemarksInsert(
                        airbillNo,
                        "0",
                        string.Format(@"#UNKNOWN CUSTOMER BY {0}", userId),
                        gateway)));
            }
            return true;
        }

        /// <summary>
        /// Make Shipment Hazmat/Non-Hazmat
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="origin"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool HawbToggleHazmat(string hawb, string origin, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute((() => new HawbToggleHazmat(hawb, origin, userId)));
            return true;
        }

        /// <summary>
        /// Make Shipment Dryice/Non-Dryice
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="origin"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool MakeShipmentDryice(string hawb, string origin, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute(
                    (() => new ShipmentDryice(hawb, origin, userId)));
            return true;
        }

        /// <summary>
        /// Override/Apply Dims restrictions
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool DimsRestrictions(string hawb, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute(
                    (() => new ShipmentDimsRestrictions(hawb, userId)));
            return true;
        }

        /// <summary>
        /// Unvoid Housebills
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="gateway"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string UnvoidHousebills(string hawb, string gateway, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(
                    (() => new ShipmentUnvoidHouseBills(hawb, gateway, userId)));
        }

        /// <summary>
        /// Override AMS Hold
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool OverrideAmsHold(string hawb, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute(
                    (() => new ShipmentOverrideAmsHold(hawb, userId)));
            return true;
        }

        /// <summary>
        /// Assign Selected Shipments to
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        public bool AssignSelectedShipment(string airbillNo, string origin, long planId)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute(
                    (() => new CargoManifestPlanInsert(airbillNo, origin, planId)));

            return true;
        }

        /// <summary>
        /// Appearance Settings
        /// </summary>
        /// <returns></returns>
        public List<ShipmentTable> AppearanceSettingList(string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new AppearanceSettings(userId)));
        }

        public bool RemoveUserSettings(string account)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new RemoveShipmentExplorerUserSettings(account)));
        }

        public bool SaveColSettings(string account, string field)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new SaveColSettings(account, field)));
        }


        public DhtmlxGrid<GridUserData> ViewWeightGrigSettings()
        {
            var connectorExecutor = new ConnectorExecutor();
            var data = connectorExecutor.Execute(() => new GetGridNodes());

            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","ID"},{"FieldName","ID"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","NodeDescription"},{"FieldName","Description"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","NodeForeColor"},{"FieldName","Font color"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","NodeColor"},{"FieldName","Color"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","AllowSelect"},{"FieldName","Allow Select"},{"FieldFormat",""}}
            };

            if (!data.Any()) return new DhtmlxGrid<GridUserData>();

            var dgrid = new DhtmlxGrid<GridUserData>();

            foreach (var node in data.ToList())
            {
                var griddata = DhtmlxConverer.ToGridRow(node, columnNames);

                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = int.Parse(node["ID"].ToString())
                });
            }

            return dgrid;

        }

        public bool UpdateGridNodes(string nodeColor, string allowSelect, string nodeForeColor, int id)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new UpdateGridNodes(nodeColor, allowSelect, nodeForeColor, id)));
        }

        public bool UpdateGridOrder(string curOreder, int curId, string order, int id)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new UpdateGridOrder(curOreder, curId, order, id)));
        }

        #region Import shipments region

        public Shimpent<GridImportUserData> ImportShipmentsByDestination(string gateway, string destination, string account)
        {
            var connector = new ConnectorExecutor();
            var data = connector.Execute(() => new Getmbdestshipments(gateway, destination, account));
            var shimpent = new Shimpent<GridImportUserData>();
            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","Status"},{"FieldName","Status"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Origin"},{"FieldName","Origin"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","LoadingPlans"},{"FieldName","Loading Plan"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","AirbillNo"},{"FieldName","HAWB#"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ShipmentDate"},{"FieldName","Shipment Date"},{"FieldFormat","{0:MM/dd/yyyy}"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Destination"},{"FieldName","Destination"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","NoOfPackages"},{"FieldName","Pieces"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","GrossWeight"},{"FieldName","Gross Weight"},{"FieldFormat","{0:0.##} KG"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Volume"},{"FieldName","CU FT"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReceiveDate"},{"FieldName","Last Scanned"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","MasterBillNo"},{"FieldName","MasterBillNo"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","RecID"},{"FieldName","RecID"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","OS"},{"FieldName","OS"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReceiveLocation"},{"FieldName","Location"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","EX"},{"FieldName","EX"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","CS"},{"FieldName","CS"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","NodeName"},{"FieldName","NodeName"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","SenderName"},{"FieldName","Shipper"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ChargeableWeight"},{"FieldName","Charge Weight"},{"FieldFormat","{0:0.##} KG"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Rate"},{"FieldName","Rate"},{"FieldFormat","$ {0:0.##}"}}
                //new Dictionary<string, object>(){{"FieldBaseColumn","Tps"},{"FieldName","OH"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","HZ"},{"FieldName","HZ"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","HD"},{"FieldName","HD"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","UK"},{"FieldName","UK"},{"FieldFormat",""}},
                //new Dictionary<string, object>(){{"FieldBaseColumn","Status"},{"FieldName","HBStatus"},{"FieldFormat",""}}

            };
            var dgrid = new DhtmlxGrid<GridImportUserData>();

            double totalGrossW = 0;
            double volume = 0;
            double chargeWeight = 0;

            foreach (var node in data.ToList())
            {
                var shipment = node.Where(o => o.Key.Equals("TB")).ToList();
                var icons = string.Empty;

                if (shipment.Count > 0)
                {


                    if (bool.Parse(node["TB"].ToString()))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/perishable.gif'/>" + icons;
                    }
                    if (bool.Parse(node["BN"].ToString()))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/bnshipmentLogins.gif'/>" + icons;
                    }
                    if (bool.Parse(node["EX"].ToString()))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/exclamation.gif' alt='Expedite Shipment'/>" + icons;
                    }
                    if (bool.Parse(node["CS"].ToString()))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/InboundScan.gif'/>" + icons;
                    }

                    if (bool.Parse(node["HZ"].ToString()))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/Blinking_Lamps.gif'/>" + icons;
                    }

                    if (bool.Parse(node["UK"].ToString()))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/UKS_new.gif' alt='Ready'/>" + icons;
                    }
                    if (node["HD"].ToString().Equals("1"))
                    {
                        icons = "<img src='" + ServerInformation.BaseUrl + "Images/stop.gif' alt='Not Ready'/>" + icons;
                    }
                    else
                    {
                        if (node["Status"].ToString().Equals("0"))
                        {
                            icons = "<img src='" + ServerInformation.BaseUrl + "Images/Greentick.gif' alt='Ready'/>" + icons;
                        }
                    }

                    if (!node["LoadingPlans"].Equals("N/A"))
                    {
                        icons = icons + "<img src='" + ServerInformation.BaseUrl + "Images/clipicon.gif'/>";
                    }

                }

                var row = new DhtmlxRow<GridImportUserData>()
                {
                    id = data.IndexOf(node) + 1,

                    userdata = new GridImportUserData()
                    {
                        LoadingPlan = node["LoadingPlans"].ToString(),
                        MasterBillNo = node["MasterBillNo"].ToString(),
                        RecId = node["RecID"].ToString(),
                        Os = node["OS"].ToString(),
                        NodeName = node["NodeName"].ToString(),
                        Ex = node["EX"].ToString(),
                        Cs = node["CS"].ToString(),
                        Oh = node["Tps"].ToString(),
                        Hz = node["HZ"].ToString(),
                        Hd = node["HD"].ToString(),
                        Uk = node["UK"].ToString(),
                        HbStatus = node["Status"].ToString(),
                        AirbillNo = node["AirbillNo"].ToString()

                    }


                };

                /*gross weight*/
                var gross = node["GrossWeight"].ToString();
                totalGrossW += double.Parse(gross);
                /*volume*/
                var cf = node["Volume"].ToString();
                volume += double.Parse(cf);
                /*ch wt*/
                var chWeight = node["ChargeableWeight"].ToString();
                chargeWeight += double.Parse(chWeight);

                node["Status"] = icons;

                var griddata = DhtmlxConverer.ToGridRow(node, columnNames);
                row.data = griddata;

                dgrid.rows.Add(row);
            }
            shimpent.data = dgrid;
            shimpent.total_hawb = dgrid.rows.Count;
            shimpent.total_gross_w = string.Format("{0:0.##} KG", totalGrossW);
            shimpent.total_cf = string.Format("{0}", volume);
            shimpent.total_ch_wt = string.Format("{0:0.##} KG", chargeWeight);
            return shimpent;
        }

        public string ImportShipmentConsolbyDestination(string lstAirbillNo, string masterbilNo, string userId, string typeConsol)
        {
            var connector = new ConnectorExecutor();
            var result = string.Empty;
            try
            {
                foreach (var airbillNo in lstAirbillNo.Split(','))
                {
                    result += string.Format("{0};", connector.Execute((() => new ImportShipmentConsol(airbillNo, masterbilNo, userId, typeConsol))));

                    var fsp = new WSPieceScanSoapClient("WSPieceScanSoap12");
                    if (fsp.IsFSPCompletedByHawb("JFK", airbillNo))
                    {
                        fsp.ProcessFSP("JFK", airbillNo);
                    }
                }
            }
            catch (Exception exception)
            {
                result = "Error has occurred. " + exception.Message;
            }
            return result;
        }

        public string ImportShipmentConsolbyHousebill(string lstAirbillNo, string masterbilNo, string userId, string typeConsol)
        {
            var connector = new ConnectorExecutor();
            var result = string.Empty;
            if (!string.IsNullOrEmpty(lstAirbillNo.Trim()))
            {

                foreach (var airbillNo in lstAirbillNo.Trim().Split(','))
                {
                    try
                    {
                        result +=
                            connector.Execute(
                                (() => new ImportShipmentConsol(airbillNo, masterbilNo, userId, typeConsol)));

                        if (typeConsol.ToUpper().Equals("C"))
                        {
                            var fsp = new WSPieceScanSoapClient("WSPieceScanSoap12");
                            if (!fsp.IsFSPCompletedByHawb("JFK",
                                airbillNo))
                            {
                                fsp.ProcessFSP("JFK",
                                    airbillNo);
                            }
                        }
                    }

                    catch (Exception exception)
                    {
                        result = "Housebill already Attached";
                    }
                }

            }
            else
            {
                result = "Please Enter Housebill to import.";
            }
            return result;
        }
        #endregion



        #region Filter Scaned/Not scaned change

        public List<Dictionary<string, object>> GetDomesticConsolData(string consol)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetDomesticConsolData(consol)));
        }
        #endregion

        #region Remove Shipmnet(s)

        public bool RemoveShipment(string airnillNo, string masterbillNo)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute(() => new RemoveShipment(airnillNo, masterbillNo));
        }
        #endregion

        #region Print Loading Plan

        public List<Dictionary<string, object>> GetAllPrinters()
        {
            var connector = new ConnectorExecutor();
            return connector.Execute(() => new GetAllPrinters());
        }

        public bool InsertPrintServerReport(string userId, string loadingPlanId, string gateway, string printer)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute(() => new InsertPrintServerReport(userId, loadingPlanId, gateway, printer));
        }
        #endregion

        #region Create empty master house

        public string CreateNewMasterHouse(
            string loadingPlan, string origin, string masterbill, string destination,
            string accountName, string gateway, string userId, string assemblyMode)
        {
            var opDate = DateTime.Now;
            var intTransitID = 0;
            var carrierCode = "ASM";
            var intCarrierNumber = 0;
            var carrierName = "Assembly";
            var carrierFlight = " ";
            var deparDate = opDate;
            var arrDate = opDate;
            var mawbServiceType = "AS";
            var mawbRateType = "";
            var transportType = "C";
            var loadType = "0";
            var taskParams = "";
            int intDispatchTimerID = 0;
            var account = accountName;

            if (string.IsNullOrEmpty(masterbill))
            {
                masterbill = GenerateMasterHouse(gateway);
            }

            masterbill = masterbill.Trim().ToUpper();

            var valid = InsertConsolNew(masterbill, opDate, gateway, intTransitID, carrierCode, intCarrierNumber,
                carrierName, carrierFlight, origin, destination, deparDate, arrDate, gateway, userId,
                mawbServiceType, mawbRateType, transportType, loadType, account, "", intDispatchTimerID);
            if (valid)
            {
                if (assemblyMode.Trim().ToLower().Equals("c"))
                {
                    AttachLoadingplanConsolNew("", "", masterbill, userId, opDate, gateway, taskParams, false, false,
                        false);
                }
                else if (assemblyMode.Trim().ToLower().Equals("a"))
                {
                    AttachLoadingplanConsolNew(loadingPlan.Split('-')[1], "", masterbill, userId, opDate, gateway, taskParams, false,
                        false, false);
                    CalculateMasterhouseOnAssignment(masterbill);
                }
            }
            else
            {
                return "Masterbill already in Use. Please Contact Supervisor.";
            }
            return "Masterbill has been created.";
        }

        /// <summary>
        /// Generates master gouse if ut doesn't exist
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        string GenerateMasterHouse(string gateway)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GenerateMasterHouse(gateway)));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterbillno"></param>
        /// <param name="opDate"></param>
        /// <param name="executedBy"></param>
        /// <param name="execGateway"></param>
        /// <param name="transitId"></param>
        /// <param name="carrierCode"></param>
        /// <param name="caNum"></param>
        /// <param name="caName"></param>
        /// <param name="caFlight"></param>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <param name="depDate"></param>
        /// <param name="arrDate"></param>
        /// <param name="gateway"></param>
        /// <param name="serviceType"></param>
        /// <param name="transportType"></param>
        /// <param name="loadType"></param>
        /// <param name="aclane"></param>
        /// <param name="vesselName"></param>
        /// <param name="taskDispatchTimer"></param>
        /// <param name="rateType"></param>
        /// <returns></returns>
        bool InsertConsolNew(string masterbillno, DateTime opDate, string execGateway,
            int transitId, string carrierCode, int caNum, string caName, string caFlight, string origin,
            string destination,
            DateTime depDate, DateTime arrDate, string gateway, string executedBy, string serviceType, string rateType,
            string transportType,
            string loadType, string aclane, string vesselName, int taskDispatchTimer)
        {
            var connector = new ConnectorExecutor();
            try
            {
                return
                    connector.Execute(
                        () =>
                            new InsertConsolNew(masterbillno, opDate, executedBy, execGateway, transitId, carrierCode, caNum,
                                caName, caFlight, origin, destination, depDate, arrDate, gateway, serviceType, transportType,
                                loadType, aclane, vesselName, taskDispatchTimer, rateType));
            }
            catch (Exception exception)
            {
                var data = GetMawbbyMasterbill(masterbillno);
                if (data.Count > 0)
                {
                    RemoveMawbByMasterbill(masterbillno);
                    return
                        connector.Execute(
                            () =>
                                new InsertConsolNew(masterbillno, opDate, executedBy, execGateway, transitId,
                                    carrierCode, caNum,
                                    caName, caFlight, origin, destination, depDate, arrDate, gateway, serviceType,
                                    transportType,
                                    loadType, aclane, vesselName, taskDispatchTimer, rateType));
                }
                else
                {
                    return false;
                }
            }
        }

        List<Dictionary<string, object>> GetMawbbyMasterbill(string masterbill)
        {
            var connector = new ConnectorExecutor();
            return
                connector.Execute((() => new GetMawbbyMasterbill(masterbill)));
        }

        bool RemoveMawbByMasterbill(string masterbill)
        {
            var connector = new ConnectorExecutor();
            return
                connector.Execute((() => new RemoveMawbByMasterbill(masterbill)));
        }

        bool AttachLoadingplanConsolNew(string cmid, string carrier, string mawb, string userId, DateTime selDate, string selgateway, string taskParams,
          bool uldfscsscInc, bool carrierConvenience, bool isSpot)
        {
            var connector = new ConnectorExecutor();
            return
                connector.Execute(
                    (() =>
                        new AttachLoadingplanConsolNew(cmid, mawb, userId, selDate, selgateway, taskParams,
                            carrierConvenience, uldfscsscInc, carrier, isSpot)));
        }

        bool CalculateMasterhouseOnAssignment(string consol)
        {
            var connector = new ConnectorExecutor();
            return
                connector.Execute((() => new CalculateMasterhouseOnAssignment(consol)));
        }
        #endregion

        #region Update Screening Status on selected Shipment(s)

        public bool UpdateScreeningStatus(string Hawbs)
        {
            var lstHawb = Hawbs.Split(',');
            var fsp = new WSPieceScanSoapClient("WSPieceScanSoap12");

            for (int i = 0; i < lstHawb.Length; i++)
            {
                if (!fsp.IsFSPCompletedByHawb("JFK", lstHawb[i]))
                {
                    fsp.ProcessFSP("JFK", lstHawb[i]);
                }
            }
            return true;
        }
        #endregion
    }
}
