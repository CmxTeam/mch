﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.wspieceScan;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.DatabaseManager
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ExportDatabaseManager
    {
        /// <summary>
        /// Shipments list for the tree search result
        /// </summary>
        /// <param name="terms">[optional]</param>
        /// <param name="reference">[optional]</param>
        /// <param name="gateway">[optional]</param>
        /// <param name="origin">[optional]</param>
        /// <param name="maindest">[optional]</param>
        /// <param name="airbillNo">[optional]</param>
        /// <param name="masterBilNo">[optional]</param>
        /// <param name="shipperName">[optional]</param>
        /// <param name="shipperNo">[optional]</param>
        /// <param name="consigneeName">[optional]</param>
        /// <param name="consigneeNo">[optional]</param>
        /// <param name="shipFrom">[optional]</param>
        /// <param name="shipTo">[optional]</param>
        /// <param name="scannedFrom"> [optional]</param>
        /// <param name="scannedTo">[optional]</param>
        /// <param name="departureFrom">[optional]</param>
        /// <param name="departureTo">[optional]</param>
        /// <param name="arrivalFrom">[optional]</param>
        /// <param name="arrivalTo">[optional]</param>
        /// <param name="pickupFrom">[optional]</param>
        /// <param name="pickupTo">[optional]</param>
        /// <returns></returns>
        public DhtmlxTree GetAdvSearchResult(string terms = null, string reference = null, string gateway = null, string origin = null,
            string maindest = null, string airbillNo = null, string masterBilNo = null, string shipperName = null,
            string shipperNo = null, string consigneeName = null, string consigneeNo = null, string shipFrom = null,
            string shipTo = null, string scannedFrom = null, string scannedTo = null, string departureFrom = null,
            string departureTo = null, string arrivalFrom = null, string arrivalTo = null, string pickupFrom = null,
            string pickupTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new AdvansedSearchGet(terms, reference, gateway, origin, maindest, airbillNo, masterBilNo,
                shipperName, shipperNo, consigneeName, consigneeNo, shipFrom, shipTo, scannedFrom, scannedTo, departureFrom, departureTo,
                arrivalFrom, arrivalTo, pickupFrom, pickupTo)));
        }

        /// <summary>
        /// Shipments list for the tree search result for the Domestic consolidations
        /// </summary>
        /// <param name="consol">C- or D-</param>
        /// <returns></returns>
        public DhtmlxTree GetDomesticConsolList(string consol)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetDomesticConsolList(consol)));
        }

        Dictionary<string, object> AddSetting(Dictionary<string, object> arr, string field, string val)
        {
            if (arr.ContainsKey(field))
            {
                arr[field] = val;
            }
            else
            {
                arr.Add(field, val);
            }
            return arr;
        }

        /// <summary>
        /// Masterbill details loading to the main view of the search panel
        /// </summary>
        /// <param name="consol">Consolidation code</param>
        /// <param name="account">Current user</param>
        /// <param name="airbillNo">Airbill code</param>
        /// <param name="gateway">Gateway</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<ShipmentTable> GetMasterbillDetails(string consol, string account, string filter, string gateway, string userType)
        {

            var connectorExecutor = new ConnectorExecutor();
            var list = consol.Split('-');
            if (list.Length > 1)
            {
                consol = list[1];
            }

            var tables = connectorExecutor.Execute((() => new GetMasterbillCompleteDetails(consol)));

            tables[0].TableName = "Consol";
            tables[1].TableName = "Housebills";
            tables[2].TableName = "ULD";
            tables[3].TableName = "Alerts";

            /*buttons settings*/
            var settings = new Dictionary<string, object>();
            var status = tables[0].Rows.First()["Status"].ToString();
            var shipmentType = tables[0].Rows.First()["ShipmentType"].ToString();

            if (shipmentType.Trim().ToUpper().Equals("AS"))
            {
                AddSetting(settings, "btnFinalyzeMasterHouse", "True");
                AddSetting(settings, "btnFinalize", "False");
                AddSetting(settings, "btnPrintDocuments", "True"); 
            }
            else
            {
                AddSetting(settings, "btnFinalyzeMasterHouse", "False");
                AddSetting(settings, "btnFinalize", "True");
            }

            var isImport = bool.Parse(tables[0].Rows.First()["IsImport"].ToString());
            if (isImport)
            {
                AddSetting(settings, "btnSpotRate", "False");
                AddSetting(settings, "btnDelCon", "False");
                AddSetting(settings, "btnPrintDocuments", "False");
                AddSetting(settings, "btnMAWB", "False");
                AddSetting(settings, "btnMawb2", "False");
                AddSetting(settings, "btnFinalize", "False");
                AddSetting(settings, "btnDelCon", "False");
                AddSetting(settings, "btnReopenConsol", "False");
                AddSetting(settings, "btnTransmit", "False");
                AddSetting(settings, "btnUld", "False");
                AddSetting(settings, "cmbGo", "False");
                AddSetting(settings, "btnGo", "False");

                AddSetting(settings, "DisplayStatus", "Import consol waiting freight recovery.");
            }
            else
            {
                if (status.Trim().ToUpper().Equals("CLOSED"))
                {
                    AddSetting(settings, "btnFinalyzeMasterHouse", "False");
                    AddSetting(settings, "btnSpotRate", "False");
                    AddSetting(settings, "btnDelCon", "False");
                    AddSetting(settings, "btnPrintDocuments", "True");
                    AddSetting(settings, "btnMAWB", "False");
                    AddSetting(settings, "btnMawb2", "False");
                    AddSetting(settings, "btnFinalize", "False");

                    var trStatus = tables[0].Rows.First()["TransmitionStatus"].ToString();
                    if (!trStatus.Contains("Closed in Logis"))
                    {
                        AddSetting(settings, "btnReopenConsol", "True");
                    }
                    else
                    {
                        AddSetting(settings, "btnDelCon", "True");
                        AddSetting(settings, "btnReopenConsol", "False");
                    }

                    AddSetting(settings, "btnTransmit", "True");
                    AddSetting(settings, "btnUld", "False");
                    AddSetting(settings, "cmbGo", "False");
                    AddSetting(settings, "btnGo", "False");
                }
                else
                {
                    var serviceType = tables[0].Rows.First()["ServiceType"].ToString();
                    if (serviceType.Equals("AS"))
                    {
                        AddSetting(settings, "btnMAWB", "False");
                        AddSetting(settings, "btnMawb2", "False");
                        AddSetting(settings, "btnSpotRate", "False");
                    }
                    else
                    {
                        if (ShowOldMawb(gateway))
                        {
                            AddSetting(settings, "btnMAWB", "True");
                            AddSetting(settings, "btnMawb2", "False");
                        }
                        else
                        {
                            AddSetting(settings, "btnMAWB", "False");
                            AddSetting(settings, "btnMawb2", "True");
                        }
                    }

                    if (!string.IsNullOrEmpty(userType))
                    {
                        if (userType.Trim().ToUpper().Equals("ADMIN"))
                        {
                            var isstackClosed = bool.Parse(tables[0].Rows.First()["ISTASKCLOSED"].ToString());
                            if (isstackClosed)
                            {
                                AddSetting(settings, "btnOpenTask", "True");
                            }
                            else
                            {
                                AddSetting(settings, "btnOpenTask", "False");
                            }
                        }
                    }

                    var dsTimerInfo = GetDispatchTimerInfo(consol);
                    if (dsTimerInfo.Count > 0)
                    {
                        AddSetting(settings, "btnDispatchTime", "True");
                    }
                    else
                    {
                        AddSetting(settings, "btnDispatchTime", "False");
                    }
                    AddSetting(settings, "cmbGo", "True");
                    AddSetting(settings, "btnGo", "True");

                }

                if (!status.Trim().ToUpper().Equals("READY"))
                {
                    AddSetting(settings, "btnFinalize", "False");
                }
            }


            var columnNames = connectorExecutor.Execute((() => new ShipmentSettingsGet(account)));

            var item = new Shimpent<GridUserData>
            {
                ColumnNames = "Status," + string.Join(",", columnNames.Select(o => o["FieldName"])),
                id = int.MaxValue
            };

            var lstWidth = "*,";
            var mappingWidth = GetMappingWidth();
            foreach (var column in columnNames)
            {
                var wid = mappingWidth.Where(o => o["FieldBaseColumn"].Equals(column["FieldBaseColumn"])).ToList();
                if (wid.Count > 0)
                {
                    lstWidth += string.Format("{0},", wid[0]["Width"]);
                }
            }
            lstWidth = lstWidth.TrimEnd(',');
            item.Widths = lstWidth;

            var dgrid = new DhtmlxGrid<GridUserData>();


            var columns =
                tables[1].Rows[0].Where(o => o.Key.Contains("Image")).Select(o => o.Key.ToString()).ToList();

            var nodes = new List<Dictionary<string, object>>();

            if (string.IsNullOrEmpty(filter))
            {
                filter = "all";
            }
            if (filter.ToLower().Equals("scanned"))
            {
                nodes = tables[1].Rows.Where(o => o["CS"].Equals("True")).ToList();
            }
            else if (filter.ToLower().Contains("not"))
            {
                nodes = tables[1].Rows.Where(o => o["CS"].Equals("False")).ToList();
            }
            else if (filter.ToLower().Equals("all"))
            {
                nodes = tables[1].Rows;
            }

            double chargeWeight = 0;
            var totalPcs = 0;
            double cubisFeet = 0;
            double totalGrossW = 0;
            double ftc = 0;

            foreach (var node in nodes)
            {
                var griddata = DhtmlxConverer.ToGridRow(node, columnNames);

                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = nodes.IndexOf(node) + 1
                });
                /*total pieces*/
                var piece = node["NoOfPackages"].ToString().Trim().Replace("Of", " ").Split(' ').Last().Trim();
                totalPcs += int.Parse(piece);
                /*ch wt*/
                var chWeight = node["ChargeableWeight"].ToString();
                chargeWeight += double.Parse(chWeight.Replace(',', '.'));
                /*volume*/
                var cf = node["Volume"].ToString();
                cubisFeet += double.Parse(cf.Replace(',', '.'));
                /*gross weight*/
                var gross = node["GrossWeight"].ToString();
                totalGrossW += double.Parse(gross.Replace(',', '.'));
                /*ftc*/
                var wCharge = node["WeightCharge"].ToString();
                ftc += double.Parse(wCharge.Replace(',', '.'));
            }


            item.data = dgrid;
            item.total_hawb = nodes.Count;
            item.total_ch_wt = string.Format("{0:#,###.##} KG", (cubisFeet * 100) * 1728 / (166 * 2.2046));//chargeWeight);
            item.total_pcs = totalPcs;
            item.total_cf = string.Format("{0:#,###.##} CF", cubisFeet);
            item.total_gross_w = string.Format("{0:#,###.##} KG", totalGrossW);
            item.total_ftc = string.Format("${0:#,###.##}", ftc);

            tables[1].Rows = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>() {{"data", item}}
            };

            tables[0].Rows[0]["ExecutedOnDate"] =
                DateTime.Parse(tables[0].Rows[0]["ExecutedOnDate"].ToString()).ToString("MM/dd/yy HH:mm");
            tables[0].Rows[0]["DepartureDate"] =
                DateTime.Parse(tables[0].Rows[0]["DepartureDate"].ToString()).ToString("MM/dd/yy HH:mm");
            tables[0].Rows[0]["ArrivalDate"] =
                DateTime.Parse(tables[0].Rows[0]["ArrivalDate"].ToString()).ToString("MM/dd/yy HH:mm");

            tables.Add(new ShipmentTable() { TableName = "Settings", Rows = new List<Dictionary<string, object>>() { settings } });

            return tables;
        }


        public List<ShipmentTable> GetMasterbillDetails(string consol, string account, string gateway, string userType)
        {
            return this.GetMasterbillDetails(consol, account, "all", gateway, userType);
        }

        /// <summary>
        /// Update transport type functionality
        /// </summary>
        /// <param name="type">Transport type key: P,C,D</param>
        /// <param name="code">Masterbill code</param>
        /// <param name="user">Current user</param>
        /// <returns></returns>
        public bool UpdateTransportType(string type, string code, string user)
        {
            var upMawb = UpdateMawb(type, code);
            var depInsrt = DepositionTableInsert(code, user, type);
            return upMawb && depInsrt;
        }

        /// <summary>
        /// Get title of masterbill to the main view of the search
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetMasterbillTitle(string code)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new MasterbillDescrGet(code)));
        }


        #region Seal verification
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetDispositionSealTypes()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new DispositionSealTypesGet()));
        }

        public DhtmlxCombo GetTetApplLevel()
        {
            var shipType = new DhtmlxCombo();
            shipType.options.AddRange(
                new List<DhtmlxComboItem>()
                {
                    new DhtmlxComboItem()
                    {
                        text = "PIECE",
                        value = "PIECE"
                    },
                    new DhtmlxComboItem()
                    {
                        text = "SKID",
                        value = "SKID"
                    },
                    new DhtmlxComboItem()
                    {
                        text = "ULD",
                        value = "ULD"
                    }
                });

            return shipType;
        }

        public bool SaveSealInformation(int carrierId, string consol, int sealTypeId, string sealRef, string userId,
            string gateway)
        {
            var sealApproval = 0;
            var remark = string.Empty;


            if (sealTypeId == 2 || sealTypeId == 1)
            {
                sealApproval = 1;
            }

            else if (sealTypeId == 2)
            {
                remark = string.Format("SEAL Verification - TET {0}", sealRef);
            }
            else if (sealTypeId == 1)
            {
                remark = string.Format("SEAL Verification - CHAIN OF CUSTODY {0}", sealRef);
            }
            else if (sealTypeId == 3)
            {
                remark = string.Format("SEAL Verification - SEAL MISSING");
            }
            else if (sealTypeId == 4)
            {
                remark = string.Format("SEAL Verification - SEAL BROKEN");
            }

            var connectorExecutor = new ConnectorExecutor();
            return
                connectorExecutor.Execute(
                    (() =>
                        new DispositionSealInfoSave(carrierId, consol, sealTypeId, sealRef, userId, gateway, remark,
                            sealApproval)));
        }
        #endregion

        #region Dispositions
        public DhtmlxGrid<GridUserData> GetDispositionList(string code)
        {
            var connectorExecutor = new ConnectorExecutor();
            var data = connectorExecutor.Execute((() => new DispositionsList(code)));

            if (!data.Any()) return new DhtmlxGrid<GridUserData>();

            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","RecordDate"},{"FieldName","Disposition Date"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Description"},{"FieldName","Description"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","UserID"},{"FieldName","User"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","AirbillNo"},{"FieldName","Airbill No."},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","Reference"},{"FieldName","Reference"},{"FieldFormat",""}}
            };

            var dgrid = new DhtmlxGrid<GridUserData>();
            foreach (var item in data.ToList())
            {
                var griddata = DhtmlxConverer.ToGridRow(item, columnNames);
                griddata.RemoveAt(0);
                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = data.IndexOf(item) + 1
                });
            }

            return dgrid;
        }
        #endregion

        #region reopen consol
        public bool ReopenConsol(string consol, string userId, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new ReopenConsolidation(consol, userId, gateway)));
        }
        #endregion

        #region delete consolidation
        public bool DeleteConsolidation(string consol, string userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new DeleteConsolidation(consol, userId)));
        }
        #endregion

        #region Transmit Consolidation
        public bool TransmitConsolidation(string consol, string userId, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new TransmitConsol(consol, userId, gateway)));
        }
        #endregion

        #region Finalize
        public DhtmlxGrid<GridUserData> GetReportList(string gateway, string reportType, string processName)
        {
            var connectorExecutor = new ConnectorExecutor();
            var data = connectorExecutor.Execute((() => new GetReportList(gateway, reportType, processName)));

            if (!data.Any()) return new DhtmlxGrid<GridUserData>();

            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","ReportName"},{"FieldName","Report name"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReportPath"},{"FieldName","Report Path"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ReportConnection"},{"FieldName","Report connection"},{"FieldFormat",""}}
            };

            var dgrid = new DhtmlxGrid<GridUserData>();
            foreach (var item in data.ToList())
            {
                var griddata = DhtmlxConverer.ToGridRow(item, columnNames);
                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = data.IndexOf(item) + 1
                });
            }

            return dgrid;

        }

        public List<Dictionary<string, object>> GetApplicationPrinters()
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetApplicationPrinters()));
        }
        public bool CloseConsol(string gateway, string userId, string consol)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new CloseConsol(gateway, userId, consol)));
        }

        public double GetMawbTotalValue(string masterbill)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetMawbTotalValue(masterbill)));
        }

        public int CheckAesInfo(string masterbill)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new CheckAesInfo(masterbill)));

        }

        /*
         * Buttons functionality
         */
        /// <summary>
        /// Print all
        /// </summary>
        /// <param name="airbill"></param>
        /// <param name="origin"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool ConsolPrinterPorts(string airbill, string origin, string user)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new ConsolPrinterPorts(airbill, origin, user)));
        }

        public bool InsertReportServerJobs(string gateway, string reportName, string masterbill, string printer,
            string user)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new InsertReportServerJobs(gateway, reportName, masterbill, printer, user)));
        }

        public bool ReportServerJobLocal(string gateway, string reportName, string masterbill, string printer,
            string user)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new InsertReportServerJobLocal(gateway, reportName, masterbill, printer, user)));
        }

        public bool InsertHostPlusLabels(string gateway, string masterbill, string carrierNo)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new InsertHostPlusLabels(gateway, masterbill, carrierNo)));
        }

        public bool CreateEmail(string mailfrom, string mailTo, string body, string subject,
            string userId, string gateway, string consol, string reps)
        {
            var intRid = SendEmail(mailfrom, mailTo, body, subject, userId, gateway);

            foreach (var rep in reps.Split('$'))
            {
                if (!string.IsNullOrEmpty(rep.Trim()))
                {
                    CreateEmailAttachments("CONSOL", string.Format("CONSOL={0} COPIES=EMAIL COPIES", consol), intRid);
                }
                else
                {
                    CreateEmailAttachments(rep.Trim(), string.Format("CONSOL={0}", consol), intRid);
                }
            }
            return true;
        }

        public int SendEmail(string mailfrom, string mailTo, string body, string subject,
            string userId, string gateway)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new SendEmail(mailfrom, mailTo, body, subject, userId, gateway)));
        }

        public bool CreateEmailAttachments(string report, string parameter, int recId)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new CreateEmailAttachments(report, parameter, recId)));
        }

        public bool CloseMawb(string carrier, string masterbill, string gateway, string user)
        {
            var wspieceScan = new WSPieceScanSoapClient("WSPieceScanSoap12");
            if (wspieceScan.IsFSPCompletedByMawb(
                ConfigurationManager.ConnectionStrings[gateway].ConnectionString, carrier, masterbill))
            {
                var connector = new ConnectorExecutor();
                connector.Execute((() => new CloseConsol(gateway, user, masterbill)));
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion


        #region ULD/LOOSE
        public List<Dictionary<string, object>> GetUldTypes()
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetUldTypes()));
        }

        public List<ShipmentTable> GetMawbUlDsData(string masterbillNo)
        {
            var connector = new ConnectorExecutor();
            var data = connector.Execute((() => new GetMawbUlDsData(masterbillNo)));

            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","RecID"},{"FieldName","RecID"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ULD"},{"FieldName","ULD"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ULDNo"},{"FieldName","ID #"},{"FieldFormat",""}}
            };
            var dgrid = new DhtmlxGrid<GridUserData>();
            var nodes = data[1].Rows;

            foreach (var node in nodes)
            {
                var griddata = DhtmlxConverer.ToGridRow(node, columnNames);
                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = nodes.IndexOf(node) + 1
                });
            }

            var item = new Shimpent<GridUserData>
            {
                ColumnNames = string.Join(",", columnNames.Select(o => o["FieldName"])),
                id = int.MaxValue
            };
            item.data = dgrid;
            data[1].Rows = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>() {{"data", item}}
            };


            columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","AirbillNo"},{"FieldName","HAWB"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","PCS"},{"FieldName","Pieces Scanned"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","FinalUlds"},{"FieldName","FinalUlds"},{"FieldFormat",""}}
            };
            dgrid = new DhtmlxGrid<GridUserData>();
            nodes = data[2].Rows;

            foreach (var node in nodes)
            {
                var griddata = DhtmlxConverer.ToGridRow(node, columnNames);
                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = nodes.ToList().IndexOf(node) + 1
                });
            }

            item = new Shimpent<GridUserData>
            {
                ColumnNames = string.Join(",", columnNames.Select(o => o["FieldName"])),
                id = int.MaxValue
            };
            item.data = dgrid;

            data[1].Rows = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>() {{"data", item}}
            };

            data[0].TableName = "TaskInfo";
            data[1].TableName = "ULDS";
            data[2].TableName = "Airbills";

            var settings = new List<Dictionary<string, object>>();

            if (data[0].Rows.Count > 0)
            {
                var endDate = data[0].Rows.First()["EndDate"].ToString();
                if (!string.IsNullOrEmpty(endDate))
                {
                    settings.Add(new Dictionary<string, object>() { { "btnRename", "False" } });
                    settings.Add(new Dictionary<string, object>() { { "btnAddUld", "False" } });
                    settings.Add(new Dictionary<string, object>() { { "Popts", "False" } });
                    settings.Add(new Dictionary<string, object>() { { "IN", "False" } });
                    settings.Add(new Dictionary<string, object>() { { "PalletType", "False" } });
                    settings.Add(new Dictionary<string, object>() { { "cmbPalletOptions", "False" } });
                    settings.Add(new Dictionary<string, object>() { { "uldIdNumber", "False" } });
                }
                else
                {
                    settings.Add(new Dictionary<string, object>() { { "btnRename", "True" } });
                    settings.Add(new Dictionary<string, object>() { { "btnAddUld", "True" } });
                }

                var startDate = data[0].Rows.First()["StartDate"].ToString();
                if (!string.IsNullOrEmpty(startDate))
                {
                    settings.Add(new Dictionary<string, object>() { { "btnRemoveUlds", "False" } });
                }

            }
            else
            {
                settings.Add(new Dictionary<string, object>() { { "btnRename", "True" } });
                settings.Add(new Dictionary<string, object>() { { "btnAddUld", "True" } });
            }

            data.Add(new ShipmentTable()
            {
                TableName = "Settings",
                Rows = settings
            });
            return data;
        }

        public string InsertMAwbUlds(string carrier, string masterbillNo, int uldTypeId, string uld, string uldNo, int recId, string removeAdd)
        {
            var connector = new ConnectorExecutor();
            return
                connector.Execute(
                    (() => new InsertMAwbUlds(carrier, masterbillNo, uldTypeId, uld, uldNo, removeAdd, recId)));
        }

        #endregion

        #region SPOT RATE
        public List<Dictionary<string, object>> GetMawbSpotInfo(string mawb)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetMawbSpotInfo(mawb)));
        }

        public bool UpdateSpotRateInformation(string rateSpotNo, string contract, string salesRep,
            string salesCommision, string rateSpot, string rateSpotType, string masterbill,
            string gateway, bool isSpot)
        {
            var connector = new ConnectorExecutor();
            return
                connector.Execute(
                    (() =>
                        new UpdateSpotRateInformation(rateSpotNo, contract, salesRep, salesCommision, rateSpot,
                            rateSpotType, masterbill, gateway, isSpot)));
        }
        #endregion

        #region Lookout time
        public string GetConsolCloseOut(string mawb)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetConsolCloseOut(mawb)));
        }

        public bool UpdateConsolCloseOut(string mawb, int id, DateTime time)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new UpdateConsolCloseOut(id, time, mawb)));
        }
        #endregion

        #region Finalyze Master House
        public bool FinalyzeMasterHouse(string user, string masterbillNo, string gateway, string account)
        {
            var connector = new ConnectorExecutor();

            var report = connector.Execute((() => new CloseMasterHouseReport(user, masterbillNo, gateway)));
            var closeMasterHouse = connector.Execute((() => new CloseMasterHouse(gateway, masterbillNo, account, user)));

            return report && closeMasterHouse;
        }
        #endregion

        #region Open Task

        public bool ReopenTask(string masterbillNo, string user)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new ReopenTask(masterbillNo, user)));
        }
        #endregion

        /// <summary>
        /// Checks if utem is exist
        /// </summary>
        /// <param name="masterbillNo">Masterbill code</param>
        /// <param name="airbillNo">Aurbill code</param>
        /// <param name="filter">Filter</param>
        /// <param name="gateway">curreent gateway</param>
        /// <returns></returns>
        public string IsExistShipmentList(string masterbillNo, string airbillNo,
            string filter, string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new IsExistShipmentList(masterbillNo, airbillNo,
                filter, gateway)));
        }

        public bool ShowOldMawb(string gateway)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new ShowOldMawb(gateway)));
        }

        public List<Dictionary<string, object>> GetDispatchTimerInfo(string consol)
        {
            var connector = new ConnectorExecutor();
            return connector.Execute((() => new GetDispatchTimerInfo(consol)));
        }

        public DhtmlxGrid<GridUserData> DownloadShipmentQueueGet(string gateway)
        {
            var connectorExecutor = new ConnectorExecutor();

            var data = connectorExecutor.Execute((() => new DownloadShipmentQueueGet(gateway)));

            foreach (var item in data.rows)
            {
                var status = item.data[1];
                if (status.Equals("0") || status.Equals("2")) item.data[1] = " <img src='" + ServerInformation.BaseUrl + "Images/pendingdownloadInpro.gif'/> ";
                else if (status.Equals("1")) item.data[1] = " <img src='" + ServerInformation.BaseUrl + "Images/downloaded.gif'/> ";
                else if (status.Equals("3")) item.data[1] = " <img src='" + ServerInformation.BaseUrl + "Images/pendingDownload.gif'/> ";
            }

            return data;
        }

        public string FastDownloadShipment(string strtype, string shipment, string gateway, string userId,
            string shipmentType)
        {
            string result = string.Empty;
            var type = 1;
            string error = string.Empty;
            if (strtype.Trim().ToLower().Equals("masterbill"))
            {
                result = "Please enter proper format for masterbill download 'CARRIER-MASTER'(000-00000000). ";
                if (shipment.Trim().IndexOf("-") == -1)
                {
                    return result;
                }
                if (shipment.Trim().Split('-').Length > 2)
                {
                    return result;
                }
                type = 2;
            }


            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new FastDownloadShipment(type, shipment, gateway, userId, shipmentType));

        }

        #region Modify MAWB number

        public string RenameMawb(string consol, string newConsol, string userId, string gateway)
        {

            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new RenameMawb(consol, newConsol, userId, gateway));
        }
        #endregion

        #region Print functionality

        public List<Dictionary<string, string>> GetPrintButtonSettings(string consol)
        {
            var buttonSettings = new List<Dictionary<string, string>>();

            var connector = new ConnectorExecutor();
            var data = connector.Execute(() => new GetMasterbillCompleteDetails(consol));
            var consols = data[0].Rows;
            var status = string.Empty;

            if (data[0].Rows.Count > 0)
            {
                var HasAMSHold = consols.First()["HasAMSHold"].ToString();
                if (bool.Parse(HasAMSHold))
                {
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnAMSHold", "True" } });
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnPrintAll", "False" } });
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnClose", "False" } });
                }
                else
                {
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnAMSHold", "False" } });
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnPrintAll", "True" } });
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnClose", "True" } });
                }

                var masterbill = consols.First()["MasterBillNo"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "MasterBillNo", masterbill } });

                var carrierNum = consols.First()["CarrierName"].ToString();
                if (!string.IsNullOrEmpty(carrierNum))
                {
                    buttonSettings.Add(new Dictionary<string, string>() { { "CarrierName", carrierNum } });
                }

                var airportDest = consols.First()["AirportOfDestination"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "DestAirport", airportDest } });

                status = consols.First()["Status"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "Status", status } });

                var carrierFlight = consols.First()["CarrierFlight"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "FlightVoyage", carrierFlight } });

                var airpOrigin = consols.First()["AirportOfOrigin"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "OriginAirport", airpOrigin } });

                var arrivalDate = consols.First()["ArrivalDate"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "ETA", arrivalDate } });

                var depDate = consols.First()["DepartureDate"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "ETD", depDate } });

                var selULDs = consols.First()["SelectedULDS"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "ULD", selULDs } });

                var lanename = consols.First()["LaneName"].ToString();
                buttonSettings.Add(new Dictionary<string, string>() { { "AcName", lanename } });

                if (string.IsNullOrEmpty(lanename))
                {
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnAcount", "False" } });
                }
                else
                {
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnAcount", "True" } });
                }
                var otherChanges = consols.First()["OtherCharges"].ToString();
                double oc = 0;

                if (!string.IsNullOrEmpty(otherChanges))
                {
                    oc = double.Parse(otherChanges);
                }
                var weightcharges = consols.First()["WeightCharges"].ToString();
                double ft = 0;

                if (!string.IsNullOrEmpty(weightcharges))
                {
                    ft = double.Parse(weightcharges);
                }

                buttonSettings.Add(new Dictionary<string, string>() { { "TotalAmount", (oc + ft).ToString() } });
            }

            if (!string.IsNullOrEmpty(status))
            {
                if (status.Trim().ToLower().Equals("closed"))
                {
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnClose", "False" } });
                    buttonSettings.Add(new Dictionary<string, string>() { { "btnEdit", "False" } });
                    buttonSettings.Add(new Dictionary<string, string>() { { "Heading", "Print/Email Documents" } });
                }
            }
            else
            {
                buttonSettings.Add(new Dictionary<string, string>() { { "Heading", "Finalize" } });
                buttonSettings.Add(new Dictionary<string, string>() { { "btnClose", "True" } });
            }


            return buttonSettings;
        }

        #endregion

        #region Private methods
        /// <summary>
        /// Update transport type
        /// </summary>
        /// <param name="type">Transport type key: P,C,D</param>
        /// <param name="code">Masterbill number</param>
        /// <returns></returns>
        bool UpdateMawb(string type, string code)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new UpdateMawb(type, code)));
        }

        /// <summary>
        /// Leave record in the Deposition table about transport type changes
        /// </summary>
        /// <param name="code">Masterbill code</param>
        /// <param name="user">Current user name</param>
        /// <param name="type">Transport type: P,C,D</param>
        /// <returns></returns>
        bool DepositionTableInsert(string code, string user, string type)
        {
            var connectorExecutor = new ConnectorExecutor();
            var status = 0;

            if (type.ToUpper().Equals("P"))
            {
                status = 56;
            }
            if (type.ToUpper().Equals("C"))
            {
                status = 55;
            }
            if (type.ToUpper().Equals("O"))
            {
                status = 57;
            }

            return status != 0 && connectorExecutor.Execute((() => new DepositionTableMasterbillInsert(code, user, status)));
        }
        #endregion
    }
}
