﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.DataProviders.Common.Units.AdoUnits;
using CMX.Framework.DataProvider.Entities.Authentication;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.DatabaseManager
{
    public partial class ExportDatabaseManager
    {
        /// <summary>
        /// Get list of flight companies
        /// </summary>
        /// <param name="gateway"></param>
        /// <param name="destination"></param>
        /// <param name="execDate"></param>
        /// <param name="userId"></param>
        /// <returns>Returns data about flight company in grid format</returns>
        public DhtmlxGrid<GridUserData> GetTodaysConsolsFromMawb(string gateway, string destination, DateTime execDate, string userId)
        {
            if (string.IsNullOrEmpty(gateway) || string.IsNullOrEmpty(destination) || string.IsNullOrEmpty(userId))
            {
                return new DhtmlxGrid<GridUserData>();
            }

            var connectorExecutor = new ConnectorExecutor();
            var data = connectorExecutor.Execute((() => new GetConsolsFomMawb(gateway, destination, execDate)));
            var columnNames = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>(){{"FieldBaseColumn","CarrierCode"},{"FieldName"," "},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","CarrierNumber"},{"FieldName","Carrier"},{"FieldFormat","{0:0##}"}},
                new Dictionary<string, object>(){{"FieldBaseColumn","CarrierName"},{"FieldName","Carrier name"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","CarrierFlight"},{"FieldName","Flight #"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","MasterBillNo"},{"FieldName","Masterbill No"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","AirportOfOrigin"},{"FieldName","Origin"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","AirportOfDestination"},{"FieldName","Dest"},{"FieldFormat",""}},

                new Dictionary<string, object>(){{"FieldBaseColumn","DepartureDate"},{"FieldName","Dep.Date"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","ArrivalDate"},{"FieldName","Arr.Date"},{"FieldFormat",""}},
                new Dictionary<string, object>(){{"FieldBaseColumn","CutOffTime"},{"FieldName","Cutt-Off"},{"FieldFormat",""}}

            };

            if (!data.Any()) return new DhtmlxGrid<GridUserData>();

            data = data.OrderBy(o => o["MasterBillNo"]).ToList();
            var dgrid = new DhtmlxGrid<GridUserData>();

            foreach (var item in data.ToList())
            {
                item["CarrierCode"] = string.Format("<img src='{0}Images/Airlines/{1}.gif'/>", ServerInformation.BaseUrl, item["CarrierCode"]);
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                item["CutOffTime"] = DateTime.Parse(item["DepartureDate"].ToString()).AddHours(-2).ToString("MM/dd/yy hh:mm:ss tt");
                var griddata = DhtmlxConverer.ToGridRow(item, columnNames);

                dgrid.rows.Add(new DhtmlxRow<GridUserData>()
                {
                    data = griddata,
                    id = data.ToList().IndexOf(item) + 1
                });
            }
            return dgrid;
        }

        /// <summary>
        /// set fidden field to this value
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public int GetUk(string code)
        {
            if (string.IsNullOrEmpty(code)) return 0;
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new CargoManifestPlanJoinHawb(code, "UK")));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public int GetHz(string code)
        {
            if (string.IsNullOrEmpty(code)) return 0;
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new CargoManifestPlanJoinHawb(code, "HZ")));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public int GetOs(string code)
        {
            if (string.IsNullOrEmpty(code)) return 0;
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new CargoManifestPlanJoinHawb(code, "OS")));
        }

        /// <summary>
        /// List of MAWB types
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetMawbTypes()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetMawbTypes()));
        }

        /// <summary>
        /// List of radiobuttons for Close out panel
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetActiveConsolCloseOutTimes()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetActiveConsolCloseOutTimes()));
        }

        public bool IsShipmentOnHoldGet(string code)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new IsShipmentOnHoldGet(code)));
        }

        public List<Dictionary<string, object>> GetConsolInfo(string selMaster)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetUldString(selMaster)));
        }

        public List<Dictionary<string, object>> GetTransitSchedul(string transitId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetTransitSchedule(transitId)));
        }

        public List<Dictionary<string, object>> GetCarrierCode(string carrierNumber1, string transitType)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetCarrierName(carrierNumber1, transitType)));
        }

        /// <summary>
        /// It's for selected index of rdo transport type 
        /// </summary>
        /// <param name="carrierNumber"></param>
        /// <returns></returns>
        public bool GetCarrierProperties(string carrierNumber)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute((() => new GetCarrierProperties(carrierNumber)));
        }

        public string GetUserType(long userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new GetUserType(userId));
        }
    }
}
