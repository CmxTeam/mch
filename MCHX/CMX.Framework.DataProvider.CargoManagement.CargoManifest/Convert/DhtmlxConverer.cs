﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert
{
    public static class DhtmlxConverer
    {
        public static DhtmlxGrid<GridUserData> ToGrid(DataTable table)
        {
            var result = new DhtmlxGrid<GridUserData>();

            var data = new List<DhtmlxRow<GridUserData>>();

            for (var i = 0; i < table.Rows.Count; i++)
            {
                var item = new List<string>();

                for (var j = 0; j < table.Columns.Count; j++)
                {
                    item.Add(table.Rows[i][j].ToString());
                }
                data.Add(new DhtmlxRow<GridUserData>()
                {
                    data = item,
                    id = i
                });
            }

            result.rows.AddRange(data);


            return result;
        }

        public static DhtmlxTree ToAdvSearchTree(List<DataRow> rows)
        {
            var root = new DhtmlxTreeItem();
            root.text = "Search result";
            root.id = 1;

            List<dynamic> items = new List<dynamic>();

            for (int i = 0; i < rows.Count; i++)
            {
                items.Add(new
                {
                    text = string.Format("{0}-{1}-{2}",
                    rows[i]["Origin"],
                    rows[i]["AirbillNo"],
                    rows[i]["MBDest"]),
                    id = i + 100,
                    userdata = new List<TreeUserData>()
                    {
                        new TreeUserData()
                        {
                            name = "MasterBillNo",
                            content =  rows[i].Table.Columns["MasterbillNo"]!=null?
                            rows[i]["MasterbillNo"].ToString() : string.Empty
                        },
                        new TreeUserData()
                        {
                             name = "AirbillNo",
                             content =  rows[i]["AirbillNo"].ToString()
                        },
                        new TreeUserData()
                        {
                            name = "Destination",
                            content = rows[i]["MBDest"].ToString()
                        }
                    }
                });
            }

            root.item.AddRange(items);

            var tree = new DhtmlxTree { id = 0 };

            tree.item.Add(root);
            return tree;
        }

        public static List<string> ToGridRow(Dictionary<string, object> node, List<Dictionary<string, object>> columnNames)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            
            var griddata = new List<string>();
            for (var j = 0; j < columnNames.Count(); j++)
            {
                var pValue = node[columnNames[j]["FieldBaseColumn"].ToString()];
                if (!string.IsNullOrEmpty(columnNames[j]["FieldFormat"].ToString()))
                {
                    pValue = string.Format(columnNames[j]["FieldFormat"].ToString(), pValue);
                }
                griddata.Add(pValue.ToString());
            }
            return griddata;
        }
    }
}
