﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest
{
    public abstract class ExportTableDatabaseConnector : AdoNetExportDatabaseConnector<List<Dictionary<string, object>>>
    {
        public ExportTableDatabaseConnector()
        {

        }

        public ExportTableDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public List<string> TableNameList;

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override List<Dictionary<string, object>> ObjectInitializer(List<DataTable> tables)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            if (tables.Count > 0)
            {
                foreach (DataRow dr in tables[0].Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in tables[0].Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
            }
            return rows;
        }
    }
}
