﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest
{
    public abstract class AdoNetExportDatabaseConnector<T>: AdoNetBaseDatabaseConnector, IDatabaseConnector<T> where T : class, new()
    {
        public AdoNetExportDatabaseConnector()
        {

        }

        public AdoNetExportDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }
        public abstract T ObjectInitializer(List<DataTable> tables);


        public virtual T Execute()
        {
            var dataSet = new DataSet();
            base.Execute((adapter) => adapter.Fill(dataSet));
            return ObjectInitializer(dataSet.Tables.OfType<DataTable>().ToList());
        }
    }
}
