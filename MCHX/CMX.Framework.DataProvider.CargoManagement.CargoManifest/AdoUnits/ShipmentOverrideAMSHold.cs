﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentOverrideAmsHold:CmCoreDatabaseConnector<Boolean>
    {
        public ShipmentOverrideAmsHold(string hawb, string userId)
            :base(
            new CommandParameter("@Hawb", hawb),
            new CommandParameter("@UserID", userId))
        {
            
        }

        public override string CommandQuery
        {
            get { return @"spHAWBTuggleOverLength"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
