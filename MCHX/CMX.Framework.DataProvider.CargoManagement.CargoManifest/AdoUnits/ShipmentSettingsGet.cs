﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentSettingsGet : ExportTableDatabaseConnector
    {
        public ShipmentSettingsGet(string user)
            : base(
            new CommandParameter("@UserID", user))
        {

        }
        public override string CommandQuery
        {
            get { return @"spGetUserSettingsShipExGrid"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
