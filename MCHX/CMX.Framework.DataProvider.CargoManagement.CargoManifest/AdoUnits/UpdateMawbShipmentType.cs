﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpdateMawbShipmentType : CmCoreDatabaseConnector<bool>
    {
        public UpdateMawbShipmentType(string masterbillno)
            : base(new CommandParameter("@Masterbillno", masterbillno))
        {

        }
        public override string CommandQuery
        {
            get { return @"Update Mawb set ShipmentType = 'AS' Where Masterbillno = @Masterbillno"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
