﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CreateEmailAttachments : CmCoreDatabaseConnector<bool>
    {
        public CreateEmailAttachments(string report, string parameter, int recId)
            : base(
                new CommandParameter("@Report", report),
                new CommandParameter("@Parameter", parameter),
                new CommandParameter("@RecID", recId))
        {
        }

        public override string CommandQuery
        {
            get { return @"spCreateEmailAttachments"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
