﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetCarrierName:ExportTableDatabaseConnector
    {
        public GetCarrierName(string carrierNumber1, string transitType)
            :base(
            new CommandParameter("@CarrierNumber", carrierNumber1),
            new CommandParameter("@CarrierType", transitType))
        {
        }

        public override string CommandQuery
        {
            get { return
                    @"SELECT CarrierCode 
                    FROM Carriers 
                    WHERE (CarrierNumber = @CarrierNumber) AND (CarrierType = @CarrierType)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
