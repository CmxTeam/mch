﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertReference : CmCoreDatabaseConnector<bool>
    {
        public InsertReference(string housebill, string origin, string reference)
            : base(
            new CommandParameter("@AirbillNo", housebill),
            new CommandParameter("@Origin", origin),
            new CommandParameter("@Reference", reference),
            new CommandParameter("@ReferenceType", "User Reference"))
        {

        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO Hawb_Reference (AirbillNo, Origin, ReferenceType, Reference)
                    VALUES (@AirbillNo, @Origin, @ReferenceType, @Reference)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
