﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class DispositionSealTypesGet: ExportTableDatabaseConnector
    {
        public override string CommandQuery
        {
            get { return 
                @"SELECT RecId, RecDate, SealType 
                FROM Shipment_SEAL_Types"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
