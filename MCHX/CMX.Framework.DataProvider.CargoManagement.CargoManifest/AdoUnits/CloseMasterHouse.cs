﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CloseMasterHouse : CmCoreDatabaseConnector<bool>
    {
        public CloseMasterHouse(string gateway, string masterhouse, string account, string userId)
            : base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@MasterHouse", masterhouse),
            new CommandParameter("@Account", account),
            new CommandParameter("@UserID", userId))
        {
        }

        public override string CommandQuery
        {
            get { return @"CloseMasterHouse"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
