﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.Linq;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class SearchResult : CmCoreMultyTableDatabaseConnector<DhtmlxTree>
    {
        public SearchResult(string gatewayId, string userId, string filter, string selDate)
            : base(
            new CommandParameter("@GatewayID", gatewayId),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@Filter", filter),
            new CommandParameter("@SelDate", selDate))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"spTreeViewBuildTree";
            }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override DhtmlxTree ObjectInitializer(List<DataTable> tables)
        {
            var routes = new DhtmlxTree();
            //Gateway table
            if (tables[0].Rows.Count == 1)
            {
                routes.id = 0;
                DhtmlxTreeItem root = new DhtmlxTreeItem();
                root.text = tables[0].Rows[0][0].ToString();
                root.img = tables[0].Rows[0][1].ToString();
                root.im1 = tables[0].Rows[0][1].ToString();
                root.im2 = tables[0].Rows[0][1].ToString();

                //Lanes
                List<LaneItem> lanes = new List<LaneItem>();
                if (!tables[2].Rows[0][0].Equals(false))
                {
                    for (int i = 0; i < tables[2].Rows.Count; i++)
                    {
                        lanes.Add(new LaneItem()
                        {
                            Lanes = tables[2].Rows[i]["Lanes"].ToString(),
                            Dest = tables[2].Rows[i]["Dest"].ToString(),
                            Gateway = tables[2].Rows[i]["Gateway"].ToString(),
                            Name = tables[2].Rows[i]["Name"].ToString(),
                            Icon = tables[2].Rows[i]["Icon"].ToString()
                        });
                    }
                }
                //Configuratios table
                for (int i = 0; i < tables[1].Rows.Count; i++)
                {
                    dynamic rootItem = new ExpandoObject();
                    rootItem.id = tables[1].Rows[i]["ID"].ToString();
                    rootItem.img = tables[1].Rows[i]["Icon"].ToString();
                    rootItem.im1 = tables[1].Rows[i]["Icon"].ToString();
                    rootItem.im2 = tables[1].Rows[i]["Icon"].ToString();
                    rootItem.text = tables[1].Rows[i]["Name"].ToString();

                    var name = tables[1].Rows[i]["Name"].ToString();
                    List<ExpandoObject> childs = new List<ExpandoObject>();
                    if (lanes.Count > 0)
                    {
                        var data = lanes.Where(o => o.Name.Equals(name)).ToList();
                        rootItem.item = new List<ExpandoObject>();

                        for (int j = 0; j < data.Count(); j++)
                        {
                            dynamic subitem = new ExpandoObject();

                            subitem.id = data[j].Dest;
                            subitem.img = data[j].Icon;
                            subitem.im1 = data[j].Icon;
                            subitem.im2 = data[j].Icon;
                            subitem.text = data[j].Lanes;



                            /* if (name.Equals("Consols"))
                            {
                                var consols = new List<ExpandoObject>();
                                if (tables[4].Rows.Count > 0)
                                {

                                    for (int k = 0; k < tables[4].Rows.Count; k++)
                                    {

                                        dynamic consol = new ExpandoObject();
                                        consol.id = data[j].Dest + "_" + k.ToString();
                                        consol.text = tables[4].Rows[k]["ReportName"].ToString();
                                        consol.img = tables[4].Rows[k]["ReportIcon"].ToString();
                                        consol.im1 = tables[4].Rows[k]["ReportIcon"].ToString();
                                        consol.im2 = tables[4].Rows[k]["ReportIcon"].ToString();

                                        consol.userdata = new List<TreeUserData>()
                                        {
                                            new TreeUserData()
                                            {
                                                name = "process",
                                                content = tables[4].Rows[k]["ProcessName"].ToString()
                                            }
                                        };
                                        


                                        consols.Add(consol);
                                    }

                                }
                                subitem.item = new List<ExpandoObject>();

                                subitem.item.AddRange(consols);
                            }*/

                            rootItem.item.Add(subitem);
                        }
                    }

                    childs.Add(rootItem);

                    root.item.AddRange(childs);

                }
                routes.item.Add(root);
            }

            return routes;
        }
    }
}
