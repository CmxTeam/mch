﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertPrintServerReport : CmCoreDatabaseConnector<bool>
    {
        public InsertPrintServerReport(string userId, string loadingPlanId, string gateway, string printer)
            : base(
            new CommandParameter("@ReportServer", string.Format("CargoMatrixReports {0}", gateway)),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@Parameters", string.Format("PLAN={0}", loadingPlanId)),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Printer", printer))
        {

        }
        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO ReportServer_Jobs (ReportServer, ReportName, UserID, Copies, Parameters, Printer)
                    VALUES (@ReportServer,N'LoadingPlan', @UserID, N'',@Parameters, @Printer)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
