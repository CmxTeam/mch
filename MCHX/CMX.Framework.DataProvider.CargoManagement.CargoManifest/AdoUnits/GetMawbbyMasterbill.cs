﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetMawbbyMasterbill: ExportTableDatabaseConnector
    {
        public GetMawbbyMasterbill(string masterBillNo)
            : base(new CommandParameter("@MasterBillNo", masterBillNo))
        {
        }

        public override string CommandQuery
        {
            get { return @"Select * from Mawb where MasterBillNo = @MasterBillNo"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
