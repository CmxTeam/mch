﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetTransitSchedule:ExportTableDatabaseConnector
    {
        public GetTransitSchedule(string transitId)
            : base(new CommandParameter("@TransitID", transitId))
        {
            
        }

        public override string CommandQuery
        {
            get { return
                    @"SELECT * 
                    FROM TransitSchedule 
                    WHERE (TransitID = @TransitID)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
