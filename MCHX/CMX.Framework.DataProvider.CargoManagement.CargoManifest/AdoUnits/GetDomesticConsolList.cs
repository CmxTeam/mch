﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetDomesticConsolList : CmCoreDatabaseConnector<DhtmlxTree>
    {
        public GetDomesticConsolList(string consol)
            : base(new CommandParameter("@Consol", consol))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"select Hawb.AirbillNo, Hawb.Origin, Hawb.MBDest, HostPlus_Incoming.Consol 
                        from Hawb inner join HostPlus_Incoming on 
                        Hawb.DomCarrier = HostPlus_Incoming.Carrier and 
                        Hawb.DomMasterBillNo = HostPlus_Incoming.Consol 
                        where HostPlus_Incoming.Type = 'RMB' and HostPlus_Incoming.Consol = @Consol";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override DhtmlxTree ObjectInitializer(List<DataRow> rows)
        {
            return DhtmlxConverer.ToAdvSearchTree(rows);
        }
    }
}
