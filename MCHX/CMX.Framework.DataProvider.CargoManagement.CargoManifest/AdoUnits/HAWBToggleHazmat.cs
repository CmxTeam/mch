﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class HawbToggleHazmat : CmCoreDatabaseConnector<bool>
    {
        public HawbToggleHazmat(string hawb, string origin, string userId)
            : base(
            new CommandParameter("@Hawb", hawb),
            new CommandParameter("@Origin", origin),
            new CommandParameter("@UserID", userId))
        {
        }

        public override string CommandQuery
        {
            get { return @"spHAWBTuggleHazmat"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
