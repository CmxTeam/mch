﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class AttachLoadingplanConsolNew : CmCoreDatabaseConnector<bool>
    {
        public AttachLoadingplanConsolNew(string cmid, string mawb, string userId, DateTime selDate, string gateway, string taskParams,
            bool carrierConvenience, bool uldfscsscInc, string carrier, bool isSpot)
            : base(
            new CommandParameter("@CMID", cmid),
            new CommandParameter("@MAWB", mawb),
            new CommandParameter("@USERID", userId),
            new CommandParameter("@SELDATE", selDate),
            new CommandParameter("@GATEWAY", gateway),
            new CommandParameter("@TaskParams", taskParams),
            new CommandParameter("@CarrierConvenience", carrierConvenience),
            new CommandParameter("@ULDFSCSSCInc", uldfscsscInc),
            new CommandParameter("@Result", 1),
            new CommandParameter("@IsSpot", isSpot),
            new CommandParameter("@Carrier", carrier))
        {
            
        }
        public override string CommandQuery
        {
            get
            {
                var strSql = new StringBuilder();
                strSql.Append("Set @Result = 0     ");
                strSql.Append("DECLARE @MAWBGATEWAY NVARCHAR(50), @CANUM INT, @NOPKS INT, @AIRBILL Nvarchar(100)     ");
                strSql.Append("DECLARE @PartMpallet INT, @TaskTimerID INT     ");
                strSql.Append("DECLARE @GatewayPrinter Nvarchar(100)     ");
                strSql.Append("IF @CMID IS NOT NULL     ");
                strSql.Append("BEGIN     ");
                strSql.Append("UPDATE HAWB Set CS = 0, MasterBillNo = @MAWB, Carrier=RIGHT('000' + CONVERT(nvarchar(3),@Carrier), 3) Where AirbillNo in (select AirbillNo from CargoManifestPlan where CargoManifestID = @CMID)     ");
                strSql.Append("INSERT INTO Remarks (Reference, Description, UserID, Gateway, ActionID)     ");
                strSql.Append("Select AirbillNo, 'Housebill Attached to Consol: ' + @MAWB, @USERID, @GATEWAY, 13      ");
                strSql.Append("FROM CargoManifestPlan WHERE CargoManifestID = @CMID     ");
                strSql.Append("END     ");
                strSql.Append("IF EXISTS (SELECT * FROM MAWB WHERE MasterBillNo = @MAWB AND LaneName = 'IAI')     ");
                strSql.Append("BEGIN     ");
                strSql.Append("UPDATE Mawb SET ExecutedOnDate = @SELDATE,  ExecutedBy = @USERID, Status = N'Ready', DisplayStatus = N'Selected', CarrierConvenience = @CarrierConvenience, ULDFSCSSCInc = @ULDFSCSSCInc, IsSpot = @IsSpot WHERE  MasterBillNo = @MAWB     ");
                strSql.Append("END     ");
                strSql.Append("ELSE     ");
                strSql.Append("BEGIN     ");
                strSql.Append("UPDATE Mawb SET ExecutedOnDate = @SELDATE,  TransmitionStatus = 'Selected', ExecutedBy = @USERID, Status = N'Ready', DisplayStatus = N'Selected', CarrierConvenience = @CarrierConvenience, ULDFSCSSCInc = @ULDFSCSSCInc, IsSpot = @IsSpot WHERE  MasterBillNo = @MAWB     ");
                strSql.Append("END     ");
                strSql.Append("Delete From CargoManifestPlan Where CargoManifestID = @CMID     ");
                strSql.Append("Delete From CargoManifestPlanSummary Where CargoManifestID = @CMID     ");
                strSql.Append("SELECT @TaskTimerID = Mawb.TaskTimerID, @PartMpallet = Mawb.PartMpallet, @MAWBGATEWAY = Mawb.Gateway, @CANUM = RIGHT('000' + CONVERT(nvarchar(3), Mawb.CarrierNumber), 3), @NOPKS = SUM(ISNULL(Hawb.NoOfPackages, 0)) FROM Hawb RIGHT OUTER JOIN Mawb ON Hawb.MasterBillNo = Mawb.MasterBillNo WHERE     (Mawb.MasterBillNo = @MAWB) GROUP BY RIGHT('000' + CONVERT(nvarchar(3), Mawb.CarrierNumber), 3), Mawb.Gateway, Mawb.PartMpallet, Mawb.TaskTimerID      ");
                strSql.Append("Select @GatewayPrinter = WHSEPrint1 FROM GatewaySettings WHERE GatewayID = @MAWBGATEWAY     ");
                strSql.Append("IF @PartMpallet = 0     ");
                strSql.Append("BEGIN     ");
                strSql.Append("IF @TaskTimerID = 0     ");
                strSql.Append("BEGIN     ");
                strSql.Append("IF EXISTS(Select * FROM MAWB Where Masterbillno = @MAWB AND ServiceType='AS')     ");
                strSql.Append("BEGIN     ");
                strSql.Append("IF EXISTS (SELECT * FROM DipositionTable WHERE ProcessID = 7 AND ActionID = 101 AND MasterbillNo = @MAWB)     ");
                strSql.Append("BEGIN     ");
                strSql.Append("UPDATE DipositionTable SET UserID = NULL, EndDate = NULL, StartDate = NULL, Reference = dbo.GetConsolREFRENCE(@MAWB),  DispositionDate = @SELDATE WHERE MasterbillNo = @MAWB AND ActionID=101     ");
                strSql.Append("END     ");
                strSql.Append("ELSE BEGIN     ");
                strSql.Append("INSERT INTO DipositionTable (ProcessID, ActionID, DispositionDate, Gateway, MasterbillNo, Reference) VALUES     (7, 101, dbo.GetDateFormat(GETDATE()), @GATEWAY, @MAWB, dbo.GetConsolREFRENCE(@MAWB))     ");
                strSql.Append("END     ");
                strSql.Append("END     ");
                strSql.Append("ELSE BEGIN     ");
                strSql.Append("IF EXISTS (SELECT * FROM DipositionTable WHERE ProcessID = 7 AND ActionID = 35 AND MasterbillNo = @MAWB)     ");
                strSql.Append("BEGIN     ");
                strSql.Append("UPDATE DipositionTable SET UserID = NULL, EndDate = NULL, StartDate = NULL, CarrierNumber = RIGHT('000' + CONVERT(nvarchar(3),@Carrier), 3), Reference = dbo.GetConsolREFRENCE(@MAWB),  DispositionDate = @SELDATE WHERE MasterbillNo = @MAWB     ");
                strSql.Append("END     ");
                strSql.Append("ELSE     ");
                strSql.Append("BEGIN     ");
                strSql.Append("INSERT INTO DipositionTable (ProcessID, ActionID, DispositionDate, Gateway, MasterbillNo, Reference,CarrierNumber) VALUES     (7, 35, dbo.GetDateFormat(GETDATE()), @GATEWAY, @MAWB, dbo.GetConsolREFRENCE(@MAWB), RIGHT('000' + CONVERT(nvarchar(3),@Carrier), 3))     ");
                strSql.Append("END     ");
                strSql.Append("END     ");
                strSql.Append("IF EXISTS(Select * from Mawb Where MasterBillNo = @MAWB AND Label = 0)     ");
                strSql.Append("BEGIN     ");
                strSql.Append("IF NOT EXISTS (SELECT * FROM MAWB WHERE MasterBillNo = @MAWB AND LaneName = 'IAI')     ");
                strSql.Append("Begin     ");
                strSql.Append("INSERT INTO HostPlus_Labels (Gateway, Consol, Carrier, Status, Printer, Copies) VALUES     (@MAWBGATEWAY, @MAWB, @CANUM, 0, @GatewayPrinter, @NOPKS)     ");
                strSql.Append("END     ");
                strSql.Append("END     ");
                strSql.Append("END     ");
                strSql.Append("IF @TaskTimerID = 1     ");
                strSql.Append("BEGIN     ");
                strSql.Append("IF EXISTS(SELECT * FROM DispatchTaskTimers WHERE Consol = @MAWB)     ");
                strSql.Append("BEGIN     ");
                strSql.Append("UPDATE DispatchTaskTimers SET OpenAt = @TASKPARAMS, Type = 'TIMER' WHERE Consol = @MAWB     ");
                strSql.Append("END     ");
                strSql.Append("ELSE BEGIN     ");
                strSql.Append("INSERT INTO DispatchTaskTimers (Consol, OpenAt, Gateway, Type) VALUES (@MAWB, @TASKPARAMS, @GATEWAY, 'TIMER')     ");
                strSql.Append("END     ");
                strSql.Append("END     ");
                strSql.Append("IF @TaskTimerID = 3     ");
                strSql.Append("BEGIN     ");
                strSql.Append("Update Mawb Set Autorated=0, Status = 'Completed', DisplayStatus = 'Scanning Disabled. Ready For Rating.', PulledBy = @UserID Where Masterbillno = @MAWB     ");
                strSql.Append("END     ");
                strSql.Append("IF @TaskTimerID = 4     ");
                strSql.Append("     			BEGIN     ");
                strSql.Append(
                    "     				Update Mawb Set Autorated=0, Status = 'Completed', DisplayStatus = 'Task Dispatched. Ready For Rating.', PulledBy = @UserID Where Masterbillno = @MAWB     ");
                strSql.Append(
                    "     				IF EXISTS(Select * FROM MAWB Where Masterbillno = @MAWB AND ServiceType='AS')     ");
                strSql.Append("     					BEGIN     ");
                strSql.Append(
                    "     						IF EXISTS (SELECT * FROM DipositionTable WHERE ProcessID = 7 AND ActionID = 101 AND MasterbillNo = @MAWB)     ");
                strSql.Append("     							BEGIN     ");
                strSql.Append(
                    "     								UPDATE DipositionTable SET UserID = NULL, EndDate = NULL, StartDate = NULL, Reference = dbo.GetConsolREFRENCE(@MAWB),  DispositionDate = @SELDATE WHERE MasterbillNo = @MAWB AND ActionID=101     ");
                strSql.Append("     							END     ");
                strSql.Append("     						ELSE BEGIN     ");
                strSql.Append(
                    "     								INSERT INTO DipositionTable (ProcessID, ActionID, DispositionDate, Gateway, MasterbillNo, Reference) VALUES     (7, 101, dbo.GetDateFormat(GETDATE()), @GATEWAY, @MAWB, dbo.GetConsolREFRENCE(@MAWB))     ");
                strSql.Append("     							END     ");
                strSql.Append("     					END     ");
                strSql.Append("     				ELSE     ");
                strSql.Append("     					BEGIN     ");
                strSql.Append(
                    "     						IF EXISTS (SELECT * FROM DipositionTable WHERE ProcessID = 7 AND ActionID = 35 AND MasterbillNo = @MAWB)     ");
                strSql.Append("     							BEGIN     ");
                strSql.Append(
                    "     								UPDATE DipositionTable SET UserID = NULL, EndDate = NULL, StartDate = NULL, CarrierNumber = RIGHT('000' + CONVERT(nvarchar(3),@Carrier), 3), Reference = dbo.GetConsolREFRENCE(@MAWB),  DispositionDate = @SELDATE WHERE MasterbillNo = @MAWB     ");
                strSql.Append("     							END     ");
                strSql.Append("     						ELSE BEGIN     ");
                strSql.Append(
                    "     								INSERT INTO DipositionTable (ProcessID, ActionID, DispositionDate, Gateway, MasterbillNo, Reference, CarrierNumber) VALUES     (7, 35, dbo.GetDateFormat(GETDATE()), @GATEWAY, @MAWB, dbo.GetConsolREFRENCE(@MAWB), RIGHT('000' + CONVERT(nvarchar(3),@Carrier), 3))     ");
                strSql.Append("     							END     ");
                strSql.Append("     					END     ");
                strSql.Append("     				IF EXISTS(Select * from Mawb Where MasterBillNo = @MAWB AND Label = 0)     ");
                strSql.Append("     					BEGIN     ");
                strSql.Append(
                    "     						IF NOT EXISTS (SELECT * FROM MAWB WHERE MasterBillNo = @MAWB AND LaneName = 'IAI')     ");
                strSql.Append("     							Begin     ");
                strSql.Append(
                    "     								INSERT INTO HostPlus_Labels (Gateway, Consol, Carrier, Status, Printer, Copies) VALUES     (@MAWBGATEWAY, @MAWB, @CANUM, 0, @GatewayPrinter, @NOPKS)     ");
                strSql.Append("     							END     ");
                strSql.Append("     					END     ");
                strSql.Append("     		END     ");
                strSql.Append("     		EXEC mgms_scanning_setconsoldescription @MAWB     ");
                strSql.Append("     	END     ");
                strSql.Append("     IF EXISTS(Select * from Mawb Where ServiceType='AS' AND Masterbillno = @MAWB)     ");
                strSql.Append("     	BEGIN     ");
                strSql.Append("     		Update Mawb SET TransmitionStatus = 'IN PROGRESS' Where Masterbillno = @MAWB     ");
                strSql.Append("     	END     ");

                strSql.Append(
                    "     	SELECT @CANUM = RIGHT('000' + CONVERT(nvarchar(3), CarrierNumber), 3) FROM Mawb WHERE     (Mawb.MasterBillNo = @MAWB)      ");
                strSql.Append(
                    "     	 IF EXISTS (SELECT * FROM DipositionTable WHERE ProcessID = 7 AND ActionID = 35 AND MasterbillNo = @MAWB)     ");
                strSql.Append("     	BEGIN     ");
                strSql.Append("     	UPDATE DipositionTable SET CarrierNumber = @CANUM WHERE MasterbillNo = @MAWB     ");
                strSql.Append("     	END     ");


                strSql.Append("     Set @Result = 1     ");
                strSql.Append("     Select @Result     ");
                return strSql.ToString();
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
