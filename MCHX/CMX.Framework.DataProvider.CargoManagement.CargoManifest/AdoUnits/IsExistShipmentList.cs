﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class IsExistShipmentList:CmCoreDatabaseConnector<string>
    {
        public IsExistShipmentList(string masterbillNo, string airbillNo,
            string filter, string gateway)
            : base(
            new CommandParameter("@MasterBillNo", masterbillNo),
            new CommandParameter("@Airbillno", airbillNo),
            new CommandParameter("@SearchFor", filter),
            new CommandParameter("@LoggedInGateway", gateway),
            new CommandParameter("@Message", string.Empty),
            new CommandParameter("@LoadingPlan", string.Empty))
        {
        }

        public override string CommandQuery
        {
            get { return @"spSearchHAWB_MAWB";}
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows[0][0].ToString();
        }
    }
}
