﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpdateMawb : CmCoreDatabaseConnector<bool>
    {
        public UpdateMawb(string type, string code)
            : base(
            new CommandParameter("@TransportType", type),
            new CommandParameter("@MasterBillNo", code))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                  @"UPDATE Mawb 
                    SET TransportType = @TransportType 
                    WHERE (MasterBillNo = @MasterBillNo)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
