﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class HouseAirWaybillGet : ExportTableDatabaseConnector
    {
        public HouseAirWaybillGet(string origin, string airbillNo)
            :base(
            new CommandParameter("@Origin", origin),
            new CommandParameter("@AirbillNo",airbillNo))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return 
                    @"SELECT AirbillNo, MBDest
                    FROM Hawb
                    WHERE (Origin = @Origin) AND (AirbillNo = @AirbillNo)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
