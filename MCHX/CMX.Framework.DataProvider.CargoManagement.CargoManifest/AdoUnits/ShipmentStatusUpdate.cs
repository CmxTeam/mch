﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    public class ShipmentStatusUpdate : CmCoreDatabaseConnector<bool>
    {
        public ShipmentStatusUpdate(string airbillNo, string origin, string uk)
            : base(
            new CommandParameter("@UK", uk),
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@Origin", origin))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                @"UPDATE Hawb 
                SET UK = @UK 
                WHERE (AirbillNo = @AirbillNo) AND (Origin = @Origin)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
