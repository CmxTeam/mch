﻿using System.Collections.Generic;
using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetLoadingPlan : CmCoreDatabaseConnector<long>
    {
        public GetLoadingPlan(string gateway, string destination, string account, string planId)
            : base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Destination", destination),
            new CommandParameter("@Account", account),
            new CommandParameter("@PlanID", planId))
        {
        }

        public override string CommandQuery
        {
            get { return @"GETNEXTLOADINGPLAN"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override long ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return long.Parse(rows[0][0].ToString());
        }
    }
}
