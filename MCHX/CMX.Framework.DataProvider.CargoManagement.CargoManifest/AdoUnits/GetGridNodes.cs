﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetGridNodes : ExportTableDatabaseConnector
    {
        public override string CommandQuery
        {
            get { return @"SELECT * FROM GridNodes ORDER BY OrderBy"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
