﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class RemoveShipmentExplorerUserSettings : CmCoreDatabaseConnector<bool>
    {
        public RemoveShipmentExplorerUserSettings(string account)
            : base(new CommandParameter("@UserId", account))
        {

        }
        public override string CommandQuery
        {
            get { return @"DELETE FROM ShipmentExplorerUserSettings WHERE (UserID = @UserId)"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
