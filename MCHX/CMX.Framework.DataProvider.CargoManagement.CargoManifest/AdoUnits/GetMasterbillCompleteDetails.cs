﻿using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetMasterbillCompleteDetails : ExportMultiTableDatabaseConnector
    {
        public GetMasterbillCompleteDetails(string consol)
            :base(
            new CommandParameter("@Consol", consol))
        {
        }

        public override string CommandQuery
        {
            get { return @"GetMasterbillCompleteDetails"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
