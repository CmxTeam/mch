﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetAllPrinters:ExportTableDatabaseConnector
    {
        public override string CommandQuery
        {
            get { return "SELECT * FROM ReportServer_Printers"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
