﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpdateGridNodes : CmCoreDatabaseConnector<bool>
    {
        public UpdateGridNodes(string nodeColor, string allowSelect, string nodeForeColor, int id)
            : base(
            new CommandParameter("@NodeColor", nodeColor ?? string.Empty),
            new CommandParameter("@AllowSelect", allowSelect),
            new CommandParameter("@NodeForeColor", nodeForeColor ?? string.Empty),
            new CommandParameter("@ID", id))
        {
        }
        public override string CommandQuery
        {
            get
            {
                return
                    @"UPDATE GridNodes 
                    SET NodeColor = @NodeColor, AllowSelect = @AllowSelect, NodeForeColor = @NodeForeColor
                    WHERE ID = @ID";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
