﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class DepositionTableMasterbillInsert : CmCoreDatabaseConnector<bool>
    {
        public DepositionTableMasterbillInsert(string code, string user, int status)
            : base(
            new CommandParameter("@MasterbillNo", code),
            new CommandParameter("@ActionID", status),
            new CommandParameter("@UserID", user))
        {

        }
        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO DipositionTable (MasterbillNo, ActionID, UserID) 
                    VALUES  (@MasterbillNo, @ActionID, @UserID)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
