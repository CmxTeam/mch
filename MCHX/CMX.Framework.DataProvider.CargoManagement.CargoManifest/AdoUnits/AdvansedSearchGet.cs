﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class AdvansedSearchGet : CmCoreDatabaseConnector<DhtmlxTree>
    {
        public AdvansedSearchGet(string terms = null, string reference = null, string gateway = null, string origin = null,
            string maindest = null, string airbillNo = null, string masterBilNo = null, string shipperName = null,
            string shipperNo = null, string consigneeName = null, string consigneeNo = null, string shipFrom = null,
            string shipTo = null, string scannedFrom = null, string scannedTo = null, string departureFrom = null,
            string departureTo = null, string arrivalFrom = null, string arrivalTo = null, string pickupFrom = null,
            string pickupTo = null)
            : base(
            new CommandParameter("@Terms", terms),
            new CommandParameter("@Reference", reference),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Origin", origin),
            new CommandParameter("@MAINDEST", maindest),
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@MasterbillNo", masterBilNo),
            new CommandParameter("@ShipperName", shipperName),
            new CommandParameter("@ShipperNo", shipperNo),
            new CommandParameter("@ConsigneeName", consigneeName),
            new CommandParameter("@ConsigneeNo", consigneeNo),
            new CommandParameter("@ShipFrom", shipFrom),
            new CommandParameter("@ShipTo", shipTo),
            new CommandParameter("@ScannedFrom", scannedFrom),
            new CommandParameter("@ScannedTo", scannedTo),
            new CommandParameter("@DepartureFrom", departureFrom),
            new CommandParameter("@DepartureTo", departureTo),
            new CommandParameter("@ArrivalFrom", arrivalFrom),
            new CommandParameter("@ArrivalTo", arrivalTo),
            new CommandParameter("@PickupFrom", pickupFrom),
            new CommandParameter("@PickupTo", pickupTo)
            )
        {

        }
        public override string CommandQuery
        {
            get { return @"spAdvanceSearchMawb"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override DhtmlxTree ObjectInitializer(List<DataRow> rows)
        {
            return DhtmlxConverer.ToAdvSearchTree(rows);
        }
    }
}
