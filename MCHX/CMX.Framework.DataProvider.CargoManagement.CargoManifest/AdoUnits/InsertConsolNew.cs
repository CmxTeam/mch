﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertConsolNew : CmCoreDatabaseConnector<bool>
    {
        public InsertConsolNew(string masterbillno, DateTime opDate, string executedBy, string execGateway,
            int transitId, string carrierCode, int caNum, string caName, string caFlight, string origin, string destination,
            DateTime depDate, DateTime arrDate, string gateway, string serviceType,  string transportType,
            string loadType, string aclane, string vesselName, int taskDispatchTimer, string rateType) :
            base(
            new CommandParameter("@MasterBillNo", masterbillno),
            new CommandParameter("@ExecutedOnDate", opDate),
            new CommandParameter("@ExecutedBy", executedBy),
            new CommandParameter("@ExecutedAtPlace", execGateway),
            new CommandParameter("@TransitID", transitId),
            new CommandParameter("@CarrierCode", carrierCode),
            new CommandParameter("@CarrierNumber", caNum),
            new CommandParameter("@CarrierName", caName),
            new CommandParameter("@CarrierFlight", caFlight),
            new CommandParameter("@AirportOfOrigin", origin),
            new CommandParameter("@AirportOfDestination", destination),
            new CommandParameter("@DepartureDate", depDate),
            new CommandParameter("@ArrivalDate", arrDate),
            new CommandParameter("@ShipmentType", "FT"),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Status", "Pending"),
            new CommandParameter("@ServiceType", serviceType),
            new CommandParameter("@Lane", destination),
            new CommandParameter("@System", string.Format("Cargomatrix")),
            new CommandParameter("@TransportType", transportType),
            new CommandParameter("@PartMpallet", loadType),
            new CommandParameter("@LaneName", aclane),
            new CommandParameter("@VesselName", vesselName),
            new CommandParameter("@TaskTimerID", taskDispatchTimer),
            new CommandParameter("@RateType", rateType)
            )
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO Mawb (MasterBillNo, ExecutedOnDate, ExecutedBy, ExecutedAtPlace, TransitID, 
                    CarrierCode, CarrierNumber, CarrierName, CarrierFlight, AirportOfOrigin,  
                    AirportOfDestination, DepartureDate, ArrivalDate, ShipmentType, Gateway, 
                    Status, ServiceType, Lane, System, TransportType, 
                    PartMpallet, LaneName, VesselName, TaskTimerID, RateType) 
                    VALUES (@MasterBillNo, @ExecutedOnDate, @ExecutedBy, @ExecutedAtPlace, @TransitID, 
                    @CarrierCode, @CarrierNumber, @CarrierName, @CarrierFlight, @AirportOfOrigin, 
                    @AirportOfDestination, @DepartureDate,@ArrivalDate, @ShipmentType, @Gateway,
                    @Status,@ServiceType, @Lane, @System, @TransportType, @PartMpallet, 
                    @LaneName, @VesselName, @TaskTimerID, @RateType)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
