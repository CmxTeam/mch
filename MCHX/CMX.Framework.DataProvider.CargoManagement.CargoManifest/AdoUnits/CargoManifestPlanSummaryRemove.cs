﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CargoManifestPlanSummaryRemove : CmCoreDatabaseConnector<bool>
    {
        public CargoManifestPlanSummaryRemove(string cargoManifestId)
            : base(new CommandParameter("@CargoManifestID", cargoManifestId))
        {

        }

        public override string CommandQuery
        {
            get
            {
                return @"SET NOCOUNT ON;
                        DECLARE @RowCount1 INTEGER
                        DELETE FROM CargoManifestPlanSummary WHERE (CargoManifestID = @CargoManifestID)
                        SELECT @RowCount1 = @@ROWCOUNT
                        SELECT @RowCount1 AS CargoManifestPlanSummary";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return int.Parse(rows[0][0].ToString()) > 0;
        }
    }
}
