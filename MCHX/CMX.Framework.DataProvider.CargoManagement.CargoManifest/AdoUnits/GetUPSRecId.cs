﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest
{
    class GetUpsRecId:CmCoreDatabaseConnector<int>
    {
        public override string CommandQuery
        {
            get { return 
                    @"select ID 
                    from Addressbook 
                    where CompanyName = 'UPS' and CategoryID = 'CAR'"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override int ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? int.Parse(rows[0][0].ToString()) : 0;
        }
    }
}
