﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class PrintHawbPo:CmCoreDatabaseConnector<bool>
    {
        public PrintHawbPo(string userId, string airbill, string gateway)
            : base(
           new CommandParameter("@ReportServer", string.Format("CargoMatrixReports {0}", gateway)),
           new CommandParameter("@UserID", userId),
           new CommandParameter("@AirBill", airbill),
           new CommandParameter("@Gateway", gateway))
        {
            
        }

        public override string CommandQuery
        {
            get { return @"PrintCO";}
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
