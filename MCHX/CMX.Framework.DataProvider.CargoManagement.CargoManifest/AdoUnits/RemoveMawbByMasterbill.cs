﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class RemoveMawbByMasterbill:CmCoreDatabaseConnector<bool>
    {
        public RemoveMawbByMasterbill(string masterBillNo):
            base(new CommandParameter("@MasterBillNo", masterBillNo))
        {
            
        }
        public override string CommandQuery
        {
            get { return @"DELETE FROM Mawb WHERE (MasterBillNo = @MasterBillNo)"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
