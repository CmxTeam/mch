﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CloseMasterHouseReport : CmCoreDatabaseConnector<bool>
    {
        public CloseMasterHouseReport(string userId, string masterbillNo, string gateway)
            : base(
            new CommandParameter("@ReportServer", string.Format("CargoMatrixReports {0}", gateway)),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@Parameters", string.Format("CONSOL={0}", masterbillNo)),
            new CommandParameter("@Gateway", gateway))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO ReportServer_Jobs (ReportServer, ReportName, UserID, Copies, Parameters, Printer, Gateway)
                    VALUES (@ReportServer, N'Assembly',@UserID,'',@Parameters, '',@Gateway)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
