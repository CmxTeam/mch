﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetDomesticConsolData : ExportTableDatabaseConnector
    {
        public GetDomesticConsolData(string consol)
            : base(new CommandParameter("@Consol", consol))
        {
        }

        public override string CommandQuery
        {
            get { return @"GetDomesticMasterbillCompleteDetails"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
