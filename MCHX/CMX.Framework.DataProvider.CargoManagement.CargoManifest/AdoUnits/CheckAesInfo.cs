﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CheckAesInfo:CmCoreDatabaseConnector<int>
    {
        public CheckAesInfo(string masternillNo)
            : base(new CommandParameter("@Mawb", masternillNo))
        {
        }

        public override string CommandQuery
        {
            get { return
                    @"SELECT count(Hawb.AirbillNo) 
                    FROM HostPlus_Fast_Itn_Xtn INNER JOIN Hawb ON HostPlus_Fast_Itn_Xtn.Consol = Hawb.MasterBillNo 
                    WHERE (Hawb.IsImport = 0) AND (Hawb.ED = 1) AND 
                    (Hawb.AESITN IS NULL OR Hawb.AESXTN IS NULL) AND 
                    (Hawb.MasterBillNo = @Mawb)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override int ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? rows.Count : 0;
        }
    }
}
