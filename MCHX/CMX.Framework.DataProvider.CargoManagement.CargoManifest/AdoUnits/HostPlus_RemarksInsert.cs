﻿using System.Collections.Generic;
using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class HostPlusRemarksInsert : CmCoreDatabaseConnector<bool>
    {

        public HostPlusRemarksInsert(string airbillNo, string status, string remark, string gateway)
            :base(
            new CommandParameter("@AirbillNo",airbillNo),
            new CommandParameter("@Remark", remark),
            new CommandParameter("@Status", status),
            new CommandParameter("@Gateway", gateway))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return 
                @"INSERT INTO HostPlus_Remarks (AirbillNo, Remark, Status, Gateway) 
                VALUES (@AirbillNo, @Remark, @Status, @Gateway)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
