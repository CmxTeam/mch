﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class MasterbillDestinationOriginGet : ExportTableDatabaseConnector
    {
        public MasterbillDestinationOriginGet(string mainDest)
            : base(new CommandParameter("@MainDest",mainDest))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return 
                @"SELECT * 
                FROM DestinationOrigin 
                WHERE (MainDest = @MainDest)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }


    }
}
