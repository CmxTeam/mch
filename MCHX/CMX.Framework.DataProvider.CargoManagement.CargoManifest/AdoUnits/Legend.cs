﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class Legend : CmCoreMultyTableDatabaseConnector<DhtmlxGrid<GridUserData>>
    {
        public Legend(string legendMode)
            : base(new CommandParameter("@LegendMode", legendMode))
        {

        }

        public override string CommandQuery
        {
            get { return @"GetLegend"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override DhtmlxGrid<GridUserData> ObjectInitializer(List<DataTable> tables)
        {
            var result = DhtmlxConverer.ToGrid(tables[0]);
            return result;
        }
    }
}
