﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentHoldRelease : CmCoreDatabaseConnector<string>
    {
        public ShipmentHoldRelease(string airbillNo, string origin, string reason, string gateway, string userID, string holdStatus)
            : base(
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@Origin", origin),
            new CommandParameter("@Reason", reason),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@UserID", userID),
            new CommandParameter("@HoldStatus", holdStatus))
        {
        }

        public override string CommandQuery
        {
            get { return @"spHAWB_HoldTuggle"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return string.Empty;
        }
    }
}
