﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertReportServerJobs : CmCoreDatabaseConnector<bool>
    {
        public InsertReportServerJobs(string gateway, string reportName, string masterbill, string printer, string user)
            : base(
            new CommandParameter("@ReportServer", string.Format("CargoMatrixReports {0}", gateway)),
            new CommandParameter("@ReportName", reportName),
            new CommandParameter("@UserID", user),
            new CommandParameter("@Parameters", string.Format("CONSOL={0}", masterbill)),
            new CommandParameter("@Printer", printer),
            new CommandParameter("@GatewayID", gateway))
        {

        }
        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO ReportServer_Jobs  (ReportServer, ReportName, UserID, Copies, Parameters, Printer, Gateway)
                    VALUES (@ReportServer, @ReportName, @UserID, '', @Parameters, @Printer, @GatewayID)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
