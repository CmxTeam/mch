﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpdateGridOrder : CmCoreDatabaseConnector<bool>
    {
        public UpdateGridOrder(string curOreder, int curId, string order, int id)
            : base(
            new CommandParameter("@curOreder", curOreder),
            new CommandParameter("@curId", curId),
            new CommandParameter("@order", order),
            new CommandParameter("@id", id)
            )
        {

        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"UPDATE GridNodes SET OrderBy = @curOreder WHERE (ID = @curId)
                    UPDATE GridNodes SET OrderBy = @order WHERE (ID = @id);";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
