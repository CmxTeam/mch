﻿using System;
using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class TabCodeGet : ExportTableDatabaseConnector
    {
        public TabCodeGet(string gateway, string destination, string date, string account)
            : base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Destination", destination),
            new CommandParameter("@Date", date),
            new CommandParameter("@Account", account))
        {

        }

        public override string CommandQuery
        {
            get { return @"CargoManifest_GetButtons"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
