﻿using System.Collections.Generic;
using System.Configuration;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentDetailsChargesGet : CmCoreMultyTableDatabaseConnector<DhtmlxGrid<GridUserData>>
    {
        public ShipmentDetailsChargesGet(string airbillNo, string origin)
            : base(
        new CommandParameter("@AirbillNo", airbillNo),
        new CommandParameter("@Origin", origin))
        {

        }
        public override string CommandQuery
        {
            get { return @"CargoManifest_GetCharges"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override DhtmlxGrid<GridUserData> ObjectInitializer(List<System.Data.DataTable> tables)
        {
            var result = DhtmlxConverer.ToGrid(tables[0]);
            return result;
        }
    }
}
