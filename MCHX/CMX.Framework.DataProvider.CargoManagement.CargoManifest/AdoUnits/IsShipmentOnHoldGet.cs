﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class IsShipmentOnHoldGet:CmCoreDatabaseConnector<bool>
    {
        public IsShipmentOnHoldGet(string code)
            : base(new CommandParameter("@CargoManifestID", code))
        {
            
        }

        public override string CommandQuery
        {
            get { return
                    @"SELECT dbo.IsShipmentOnHold(Hawb.Airbillno) AS HD 
                    FROM CargoManifestPlan INNER JOIN Hawb ON 
                    CargoManifestPlan.Origin = Hawb.Origin AND 
                    CargoManifestPlan.AirbillNo = Hawb.AirbillNo 
                    WHERE (CargoManifestPlan.CargoManifestID = @CargoManifestID)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 && bool.Parse(rows[0][0].ToString());
        }
    }
}
