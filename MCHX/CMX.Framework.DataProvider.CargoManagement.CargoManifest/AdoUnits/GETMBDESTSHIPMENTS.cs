﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class Getmbdestshipments : ExportTableDatabaseConnector
    {
        public Getmbdestshipments(string gateway, string destination, string account)
            :base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Destination", destination),
            new CommandParameter("@Account", account))
        {
            
        }
        public override string CommandQuery
        {
            get { return @"ShipExplorerGridViewMBDEST"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

    }
}
