﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class FilterBy : ExportTableDatabaseConnector
    {
        public FilterBy(string department)
            : base(new CommandParameter("@Department", department))
        {
        }
        public override string CommandQuery
        {
            get { return @"spTreeViewFilters"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
