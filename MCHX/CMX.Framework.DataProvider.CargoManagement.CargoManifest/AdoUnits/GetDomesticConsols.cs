﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetDomesticConsols:ExportTableDatabaseConnector
    {
        public GetDomesticConsols(string code)
            : base(new CommandParameter("@CargomanifestID", code))
        {
        }

        public override string CommandQuery
        {
            get { return @"GetDomesticConsols"; }
        }
        
        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

    }
}
