﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetDispatchTimerInfo: ExportTableDatabaseConnector
    {
        public GetDispatchTimerInfo(string consol)
            : base(new CommandParameter("@Consol", consol))
        {
        }

        public override string CommandQuery
        {
            get { return @"Select * from dbo.DispatchTaskTimers Where Consol = @Consol"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
