﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertDelayReasonHawb : CmCoreDatabaseConnector<bool>
    {
        public InsertDelayReasonHawb(string hawb, string gateway,
            string userId, string description)
            : base(
            new CommandParameter("@HAWB", hawb),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@Description", description))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                  @"INSERT INTO Remarks(RecDate, Reference, Gateway, ActionID, UserID, Description) 
                    VALUES(GetDate(), @HAWB, @Gateway, 47, @UserID, @Description);  
                    INSERT INTO dbo.HostPlus_Remarks (AirbillNo, Gateway, Remark, Status) 
                    VALUES (@HAWB, @Gateway, @Description, 0);";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
