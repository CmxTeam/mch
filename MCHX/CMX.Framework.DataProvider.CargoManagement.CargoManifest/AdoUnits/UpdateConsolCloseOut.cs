﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpdateConsolCloseOut : CmCoreDatabaseConnector<bool>
    {
        public UpdateConsolCloseOut(int clId, DateTime time, string masterbillNo)
            : base(
            new CommandParameter("@clID", clId),
            new CommandParameter("@T", time),
            new CommandParameter("@Mawb", masterbillNo))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                  @"UPDATE Mawb 
                    Set CCloseout = @clID, CutOffTime = @T 
                    Where Masterbillno = @Mawb";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
