﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetConsolCloseOut : CmCoreDatabaseConnector<string>
    {
        public GetConsolCloseOut(string mawb)
            : base(new CommandParameter("@Mawb", mawb))
        { }

       
        public override string CommandQuery
        {
            get { return 
                @"Select CCloseout, ISNULL(convert(Nvarchar(100),DATEPART(hh, CutOffTime)),'') + ':' + ISNULL(RIGHT('00' + convert(Nvarchar(100),DATEPART(mi,CutOffTime)), 2),'') 
                From Mawb 
                Where Masterbillno = @Mawb"; }
        }
        
        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return string.Format("{0},{1}", rows[0][0], rows[0][1]);
        }
    }
}
