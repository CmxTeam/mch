﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertHostPlusLabels: CmCoreDatabaseConnector<bool>
    {
        public InsertHostPlusLabels(string gateway, string masterbill, string carrierNo)
            :base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Consol", masterbill),
            new CommandParameter("@Carrier", carrierNo))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return 
                    @"Insert Into HostPlus_Labels (Gateway, Consol, Carrier, Status) 
                    VALUES (@Gateway,@Consol,@Carrier,0)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
