﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GenerateMasterHouse : CmCoreDatabaseConnector<string>
    {
        public GenerateMasterHouse(string gateway)
            : base(new CommandParameter("@Gateway", gateway))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return 
                    @"DECLARE @Result nvarchar(MAX)
                    exec GenerateMasterHouse @Gateway,@Result = @Result output
                    select @Result";
            }
        }
        
        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows[0][0].ToString();
        }
    }
}
