﻿using System;
using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetConsolsFomMawb : ExportTableDatabaseConnector
    {
        public GetConsolsFomMawb(string gateway, string destination, DateTime execDate)
            : base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Destination", destination),
            new CommandParameter("@ExecDate", execDate))
        {

        }

        public override string CommandQuery
        {
            get { return @"GetTodaysConsolsFromMAWB"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
