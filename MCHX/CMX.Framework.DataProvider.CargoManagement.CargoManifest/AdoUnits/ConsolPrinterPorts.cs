﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ConsolPrinterPorts : CmCoreDatabaseConnector<bool>
    {
        public ConsolPrinterPorts(string airbillNo, string origin, string user)
            : base(
            new CommandParameter("@AIRBILL", airbillNo),
            new CommandParameter("@ORIGIN", origin),
            new CommandParameter("@USERID", user))
        {
        }

        public override string CommandQuery
        {
            get { return @"CONSOLPRINTREPORTS"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
