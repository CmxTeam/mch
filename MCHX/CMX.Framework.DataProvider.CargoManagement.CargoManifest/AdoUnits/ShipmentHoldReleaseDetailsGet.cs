﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class HawbGet : ExportTableDatabaseConnector
    {
        public HawbGet(string airbillNo, string origin)
            : base(
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@Origin", origin))
        {
        }
        public override string CommandQuery
        {
            get
            {
                return 
                 @"SELECT *
                 FROM Hawb 
                 WHERE  (AirbillNo = @AirbillNo) AND (Origin = @Origin)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

    }
}
