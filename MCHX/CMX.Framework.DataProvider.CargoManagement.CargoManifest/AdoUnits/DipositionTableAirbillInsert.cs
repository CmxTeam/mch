﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class DipositionTableAirbillInsert : CmCoreDatabaseConnector<bool>
    {
        public DipositionTableAirbillInsert(string airbillNo, string userId, string msg, string status)
            :base(
            new CommandParameter("@AirbillNo",airbillNo),
            new CommandParameter("@UserID",userId),
            new CommandParameter("@Reference", msg),
            new CommandParameter("@ActionID",status))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO DipositionTable (AirbillNo, UserID, Reference, ActionID) 
                VALUES (@AirbillNo, @UserID, @Reference, @ActionID)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
