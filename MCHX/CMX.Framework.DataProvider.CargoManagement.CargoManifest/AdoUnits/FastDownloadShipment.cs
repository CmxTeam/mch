﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class FastDownloadShipment : CmCoreDatabaseConnector<string>
    {
        public FastDownloadShipment(int type, string shipment, string gateway, string userId, string shipmentType)
            : base(
            new CommandParameter("@DownlodType", type),
            new CommandParameter("@Shipment", shipment),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@ShipmentType", shipmentType))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"DECLARE @Result nvarchar(MAX)
                    exec spFastDownloadShipment @DownlodType, @Shipment, @Gateway,@UserID,@ShipmentType, @Result = @Result output
                    select @Result";
            }
        }

       
        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? rows[0][0].ToString() : string.Empty;
        }
    }
}
