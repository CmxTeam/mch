﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetCarrierZones : ExportTableDatabaseConnector
    {
        public GetCarrierZones(int carrierId, string serviceType)
            : base(
            new CommandParameter("@CarrierID", carrierId),
            new CommandParameter("@ServiceType", serviceType))
        {
        }

        public override string CommandQuery
        {
            get { return @"CarrierZones"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
