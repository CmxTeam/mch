﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetActiveConsolCloseOutTimes:ExportTableDatabaseConnector
    {
        public override string CommandQuery
        {
            get { return
                    @"SELECT ID, Name 
                    FROM ConsolCloseOutTimes 
                    Where IsActive = 1";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
