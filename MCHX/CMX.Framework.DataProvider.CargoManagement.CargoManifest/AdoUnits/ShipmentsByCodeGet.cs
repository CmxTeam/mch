﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentsByCodeGet : ExportTableDatabaseConnector
    {
        public ShipmentsByCodeGet(string cargoManifestId)
            : base(
            new CommandParameter("@CargoManifestID", cargoManifestId))
        {
        }
        
        public override string CommandQuery
        {
            get { return @"SELECT * FROM LoadingPlansView WHERE CargoManifestID = @CargoManifestID"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

    }
}
