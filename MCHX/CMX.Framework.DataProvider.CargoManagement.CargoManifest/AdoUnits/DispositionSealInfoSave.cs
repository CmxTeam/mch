﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    public class DispositionSealInfoSave : CmCoreDatabaseConnector<bool>
    {
        public DispositionSealInfoSave(int carrierId, string consol, int sealTypeId, string sealRef, string userId,
            string gateway, string remark, int sealApproval)
            :base(
            new CommandParameter("@Carrier", carrierId),
            new CommandParameter("@Consol", consol),
            new CommandParameter("@SealTypeID", sealTypeId),
            new CommandParameter("@SealReference", sealRef),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Remark",remark),
            new CommandParameter("@SealApproval", sealApproval))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                @"DECLARE @MasterbillID BigInt;
                SET @MasterbillID = (SELECT RecID FROM HostPlus_Incoming WHERE Carrier=@Carrier AND Consol = @Consol);  
                SET @MasterbillID = (SELECT RecID FROM HostPlus_Incoming WHERE Carrier=@Carrier AND Consol = @Consol);
                INSERT INTO Shipment_SEAL_information (SealTypeID, SealReference, UserID, MasterbillID, SEALApproved) VALUES (@SealTypeID, @SealReference, UPPER(@UserID), @Consol, @SealApproval);
                INSERT INTO Remarks (CarrierNumber, Reference, Gateway, UserID, Description) VALUES (@Carrier, @Consol, @Gateway, UPPER(@UserID), @Remark);
                INSERT INTO HostPlus_Remarks (Carrier, Consol, Gateway, Remark) VALUES (@Carrier, @Consol, @Gateway, @Remark + ' BY ' + UPPER(@UserID));";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
