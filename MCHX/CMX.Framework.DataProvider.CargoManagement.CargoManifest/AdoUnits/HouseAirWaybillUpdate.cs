﻿using System.Collections.Generic;
using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class HouseAirWaybillUpdate : CmCoreDatabaseConnector<bool>
    {

        public HouseAirWaybillUpdate(string curDest, string airbillNo, string newDest)        
        :base(
            new CommandParameter("@Origin", curDest),
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@MBDest", newDest))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                @"UPDATE Hawb 
                SET MBDest = @MBDest 
                WHERE (Origin = @Origin) AND (AirbillNo = @AirbillNo)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
