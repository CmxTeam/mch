﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class RenameMawb : CmCoreDatabaseConnector<string>
    {
        public RenameMawb(string consol, string newConsol, string userId, string gateway)
            : base(
            new CommandParameter("@Consol", consol),
            new CommandParameter("@NewConsol", newConsol),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@GatewayID", gateway))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                  @"DECLARE @Result nvarchar(MAX) 
                    exec RenameMawb @Consol, @NewConsol, @UserID, @GatewayID, @Result = output
                    select @Result";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? rows[0][0].ToString() : string.Empty;
        }
    }
}
