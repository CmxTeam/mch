﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CargoManifestPlanJoinHawb : CmCoreDatabaseConnector<int>
    {
        private readonly string _param;
        public CargoManifestPlanJoinHawb(string code, string param)
            : base(new CommandParameter("@CargoManifestID", code))
        {
            _param = param.Trim().ToLower();
        }

        public override string CommandQuery
        {
            get
            {
                switch (_param)
                {
                    case "HZ":
                        return
                 @"SELECT HZ
                    FROM CargoManifestPlan INNER JOIN Hawb 
                    ON CargoManifestPlan.Origin = Hawb.Origin AND 
                    CargoManifestPlan.AirbillNo = Hawb.AirbillNo  
                    WHERE (CargoManifestPlan.CargoManifestID = @CargoManifestID) AND (HZ = 1)";
                    case "OS":
                        return
                @"SELECT OS
                    FROM CargoManifestPlan INNER JOIN Hawb 
                    ON CargoManifestPlan.Origin = Hawb.Origin AND 
                    CargoManifestPlan.AirbillNo = Hawb.AirbillNo  
                    WHERE (CargoManifestPlan.CargoManifestID = @CargoManifestID) AND (OS = 1)";
                }
                return
                 @"SELECT UK
                    FROM CargoManifestPlan INNER JOIN Hawb 
                    ON CargoManifestPlan.Origin = Hawb.Origin AND 
                    CargoManifestPlan.AirbillNo = Hawb.AirbillNo  
                    WHERE (CargoManifestPlan.CargoManifestID = @CargoManifestID) AND (UK = 1)"; ;
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override int ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? int.Parse(rows[0][0].ToString()) : 0;
        }
    }
}
