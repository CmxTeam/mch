﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetUserType:CmCoreDatabaseConnector<string>
    {
        public GetUserType(long userId)
            : base(new CommandParameter("@UserId", userId))
        {
        }

        public override string CommandQuery
        {
            get { return @"Select top 1 * from Addressbook where ID = @UserId"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows[0]["UserType"].ToString();
        }
    }
}
