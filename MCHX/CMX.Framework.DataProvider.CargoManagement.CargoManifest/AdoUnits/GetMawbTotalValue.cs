﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetMawbTotalValue : CmCoreDatabaseConnector<double>
    {
        public GetMawbTotalValue(string masterbill)
            : base(
            new CommandParameter("@Mawb", masterbill),
            new CommandParameter("@Result", ""))
        {
        }

        public override string CommandQuery
        {
            get { return @"GetMawbTotalValue"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override double ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? double.Parse(rows[0][0].ToString().Replace('.',',')) : 0;
        }
    }
}
