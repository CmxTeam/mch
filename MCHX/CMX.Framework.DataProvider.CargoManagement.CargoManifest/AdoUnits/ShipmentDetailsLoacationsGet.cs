﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Convert;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentDetailsLoacationsGet : CmCoreMultyTableDatabaseConnector<DhtmlxGrid<GridUserData>>
    {
        public ShipmentDetailsLoacationsGet(string airbillNo)
            : base(
            new CommandParameter("@AirbillNo", airbillNo)
            )
        {
        }

        public override string CommandQuery
        {
            get { return @"HAWB_GetLocations"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override DhtmlxGrid<GridUserData> ObjectInitializer(List<DataTable> tables)
        {
            var result = DhtmlxConverer.ToGrid(tables[0]);
            return result;
        }
    }
}
