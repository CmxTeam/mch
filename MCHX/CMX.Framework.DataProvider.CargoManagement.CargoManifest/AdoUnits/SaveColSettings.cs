﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class SaveColSettings:CmCoreDatabaseConnector<bool>
    {
        public SaveColSettings(string account, string field)
            :base(
            new CommandParameter("@UserID", account),
            new CommandParameter("@FieldName",field))
        {
            
        }
        public override string CommandQuery
        {
            get { return @"spSaveGridSettings"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
