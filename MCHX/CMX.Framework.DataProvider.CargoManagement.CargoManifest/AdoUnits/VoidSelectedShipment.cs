﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class VoidSelectedShipment : CmCoreDatabaseConnector<bool>
    {
        public VoidSelectedShipment(string shipment, string shipmentType, string userId)
            : base(
            new CommandParameter("@Shipment", shipment),
            new CommandParameter("@ShipmentType", shipmentType),
            new CommandParameter("@UserID", userId))
        {

        }

        public override string CommandQuery
        {
            get { return @"Void_Shipment"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
