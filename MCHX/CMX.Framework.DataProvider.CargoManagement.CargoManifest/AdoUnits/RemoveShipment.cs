﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class RemoveShipment : CmCoreDatabaseConnector<bool>
    {
        public RemoveShipment(string airbillNo, string masterbillNo)
            : base(new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@CargoManifestID", masterbillNo))
        {

        }
        public override string CommandQuery
        {
            get { return "DELETE FROM CargoManifestPlan WHERE     (AirbillNo = @AirbillNo) AND (CargoManifestID = @CargoManifestID)"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
