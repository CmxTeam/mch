﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertMAwbUlds : CmCoreDatabaseConnector<string>
    {
        public InsertMAwbUlds(string carrier, string masterbillNo, int uldTypeId, string uld, string uldNo, string removeAdd, int recId)
            : base(
                new CommandParameter("@Carrier", carrier),
                new CommandParameter("@MasterBillNo", masterbillNo),
                new CommandParameter("@ULDTypeID", uldTypeId),
                new CommandParameter("@ULD", uld),
                new CommandParameter("@ULDNo", uldNo),
                new CommandParameter("@RemoveAdd", removeAdd),
                new CommandParameter("@RecID", recId),
                new CommandParameter("@Result", string.Empty))
        {
        }

        public override string CommandQuery
        {
            get { return @"MAWBULDS"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0? rows[0][0].ToString(): string.Empty;
        }
    }
}
