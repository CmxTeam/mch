﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CargoManifestPlanRemove : CmCoreDatabaseConnector<bool>
    {
        public CargoManifestPlanRemove(string cargoManifestId)
            : base(new CommandParameter("@CargoManifestID",cargoManifestId))
        {
            
        }

        public override string CommandQuery
        {
            get
            {
                return @"SET NOCOUNT ON;
                         DECLARE @RowCount1 INTEGER
                         DELETE FROM CargoManifestPlan WHERE (CargoManifestID = @CargoManifestID)
                         SELECT @RowCount1 = @@ROWCOUNT
                         SELECT @RowCount1 AS CargoManifestPlan";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return int.Parse(rows[0][0].ToString()) > 0;
        }
    }
}
