﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetMawbTypes:ExportTableDatabaseConnector
    {
        public override string CommandQuery
        {
            get { return @"SELECT * from Mawb_Types"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
