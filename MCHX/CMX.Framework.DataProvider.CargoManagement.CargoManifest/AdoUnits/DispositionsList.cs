﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class DispositionsList : ExportTableDatabaseConnector
    {
        public DispositionsList(string code)
            : base(new CommandParameter("@MAWB", code))
        {
        }

        public override string CommandQuery
        {
            get { return @"spMAWBDISPOSITIONS"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
