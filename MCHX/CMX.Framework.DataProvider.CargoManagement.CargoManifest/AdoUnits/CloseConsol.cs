﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CloseConsol : CmCoreDatabaseConnector<bool>
    {
        public CloseConsol(string gateway, string userId, string consol)
            : base(
            new CommandParameter("@CONSOL", consol),
            new CommandParameter("@USERID", userId),
            new CommandParameter("@GATEWAY", gateway))
        {

        }
        public override string CommandQuery
        {
            get { return @"CLOSECONSOL"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
