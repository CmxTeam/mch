﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentDimsRestrictions : CmCoreDatabaseConnector<bool>
    {
        public ShipmentDimsRestrictions(string hawb, string user)
            : base(
            new CommandParameter("@Hawb", hawb),
            new CommandParameter("@UserID", user))
        {

        }
        public override string CommandQuery
        {
            get
            {
                return @"spHAWBTuggleOverLength";
            }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
