﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpdateSpotRateInformation : CmCoreDatabaseConnector<bool>
    {
        public UpdateSpotRateInformation(string rateSpotNo, string contract, string salesRep,
            string salesCommision, string rateSpot, string rateSpotType, string masterbill,
            string gateway, bool isSpot)
            :base(
            new CommandParameter("@RateSpotNo", rateSpotNo),
            new CommandParameter("@Contract", contract),
            new CommandParameter("@SalesRep", salesRep),
            new CommandParameter("@SalesCommision", salesCommision),
            new CommandParameter("@RateSpot", rateSpot),
            new CommandParameter("@RateSpotType", rateSpotType),
            new CommandParameter("@Masterbill", masterbill),
            new CommandParameter("@GatewayID", gateway),
            new CommandParameter("@IsSpot", isSpot))
        {
        }

        public override string CommandQuery
        {
            get { return @"spUpdateSpotRateInformation"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
