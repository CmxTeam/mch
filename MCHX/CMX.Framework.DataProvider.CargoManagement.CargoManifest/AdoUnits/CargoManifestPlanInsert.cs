﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CargoManifestPlanInsert : CmCoreDatabaseConnector<bool>
    {
        public CargoManifestPlanInsert(string airbillNo, string origin, long cargoManifestId,
            string uld)
            : base(
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@Origin", origin),
            new CommandParameter("@CargoManifestID", cargoManifestId),
            new CommandParameter("@ULD", uld))
        {
        }

        public CargoManifestPlanInsert(string airbillNo, string origin, long cargoManifestId)
            : base(
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@Origin", origin),
            new CommandParameter("@CargoManifestID", cargoManifestId),
            new CommandParameter("@ULD", null))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"INSERT INTO CargoManifestPlan  (AirbillNo, Origin, CargoManifestID,ULD)
                    VALUES (@AirbillNo, @Origin, @CargoManifestID, @ULD)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
