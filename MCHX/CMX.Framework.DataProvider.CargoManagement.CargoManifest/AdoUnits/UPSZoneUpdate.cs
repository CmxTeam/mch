﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class UpsZoneUpdate:CmCoreDatabaseConnector<bool>
    {
        public UpsZoneUpdate(string zipCode, string zoneCode, string gatewayId,string userId, 
            int carrierId, string serviceCode)
            :base(
            new CommandParameter("@ZipCode", zipCode),
            new CommandParameter("@ZoneCode", zoneCode),
            new CommandParameter("@GatewayID", gatewayId),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@CarrierID", carrierId),
            new CommandParameter("@ServiceCode", serviceCode))
        {
            
        }

        public override string CommandQuery
        {
            get { return @"SaveUPSZone"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
