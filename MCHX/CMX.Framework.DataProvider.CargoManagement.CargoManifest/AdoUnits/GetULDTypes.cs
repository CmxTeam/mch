﻿using System.Configuration;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetUldTypes: ExportTableDatabaseConnector
    {
        public override string CommandQuery
        {
            get { return @"Select * from ULDTypes"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
