﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    public class ShipmentList : ExportTableDatabaseConnector
    {
        public ShipmentList(string gateway, string destination, string account)
            : base(new CommandParameter("@Gateway", gateway),
            new CommandParameter("@Destination", destination),
            new CommandParameter("@Account", account))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"vIEWwEIGHTFINAL";
            }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

    }
}
