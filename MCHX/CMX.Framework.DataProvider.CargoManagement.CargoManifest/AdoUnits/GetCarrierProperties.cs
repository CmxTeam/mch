﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetCarrierProperties : CmCoreDatabaseConnector<bool>
    {
        public GetCarrierProperties(string carrierNumber)
            : base(new CommandParameter("@CarrierNumber", carrierNumber))
        {

        }
        public override string CommandQuery
        {
            get
            {
                return
                    @"SELECT ISNULL(IsCargoOnly,0) as IsCargoOnly,ISNULL(ULDFSCSSFInc,0) as ULDFSCSSFInc 
                    FROM Carriers 
                    WHERE (CarrierNumber = @CarrierNumber)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? bool.Parse(rows[0]["IsCargoOnly"].ToString()) : false;
        }
    }
}
