﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertReportServerJobLocal:CmCoreDatabaseConnector<bool>
    {
        public InsertReportServerJobLocal(string gateway, string reportName, string masterbill, string printer, string user)
            : base(
            new CommandParameter("@ReportServer", string.Format("CargoMatrixReports {0}", gateway)),
            new CommandParameter("@ReportName", reportName),
            new CommandParameter("@Parameters", string.Format("AIRBILL={0}", masterbill)),
            new CommandParameter("@UserID", user),
            new CommandParameter("@Printer", printer))
        {
        }

        public override string CommandQuery
        {
            get { return
                    @"Insert Into ReportServer_job_Local(ReportServer, ReportName, UserID, Copies, Parameters, Printer, Gateway, Status) 
                    Values (@ReportServer, @ReportName, @Parameters, @Printer, @UserID, 0)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
