﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShowOldMawb : CmCoreDatabaseConnector<bool>
    {
        public ShowOldMawb(string gateway)
            : base(new CommandParameter("@Gateway", gateway))
        {

        }
        public override string CommandQuery
        {
            get
            {
                return
                    @"SELECT ISNULL(MawbOld, 0) AS MawbOld 
                    FROM Configuration 
                    WHERE (Gateway = @Gateway)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return bool.Parse(rows[0][0].ToString());
        }
    }
}
