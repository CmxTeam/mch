﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class DeleteConsolidation : CmCoreDatabaseConnector<bool>
    {
        public DeleteConsolidation(string consol, string userId)
            : base(
            new CommandParameter("@CONSOL", consol),
            new CommandParameter("@USERID", userId),
            new CommandParameter("Result", string.Empty))
        {
        }

        public override string CommandQuery
        {
            get { return @"DELETECONSOL"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? bool.Parse(rows[0][0].ToString()) : false;
        }
    }
}
