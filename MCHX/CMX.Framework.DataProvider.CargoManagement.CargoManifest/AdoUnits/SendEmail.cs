﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class SendEmail : CmCoreDatabaseConnector<int>
    {
        public SendEmail(string mailfrom, string mailTo, string body, string subject,
            string userId, string gateway)
            : base(
            new CommandParameter("@MailForm", mailfrom),
            new CommandParameter("@MailTo", mailTo),
            new CommandParameter("@UserId", userId),
            new CommandParameter("@Body", body),
            new CommandParameter("@Subject", subject),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@MailID", string.Empty))
        {
        }

        public override string CommandQuery
        {
            get { return @"spCreateEmail"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override int ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? int.Parse(rows[0][0].ToString()) : 0;
        }
    }
}
