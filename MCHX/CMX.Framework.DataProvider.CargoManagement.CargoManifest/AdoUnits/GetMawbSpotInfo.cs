﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetMawbSpotInfo:ExportTableDatabaseConnector
    {
        public GetMawbSpotInfo(string mawb)
            : base(new CommandParameter("@MAWB", mawb))
        {
        }

        public override string CommandQuery
        {
            get { return @"spGetMawbSpotInfo"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
