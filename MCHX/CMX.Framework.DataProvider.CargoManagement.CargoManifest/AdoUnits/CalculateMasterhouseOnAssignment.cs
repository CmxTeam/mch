﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class CalculateMasterhouseOnAssignment : CmCoreDatabaseConnector<bool>
    {
        public CalculateMasterhouseOnAssignment(string consol)
            : base(new CommandParameter("@CONSOL", consol))
        {

        }
        public override string CommandQuery
        {
            get { return @"CalculateMasterhouseOnAssignment"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
