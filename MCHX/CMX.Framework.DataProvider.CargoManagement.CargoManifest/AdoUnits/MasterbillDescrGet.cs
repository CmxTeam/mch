﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class MasterbillDescrGet : CmCoreDatabaseConnector<string>
    {
        public MasterbillDescrGet(string code)
            : base(new CommandParameter("@MasterBillNo", code))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"SELECT  AirportOfOrigin + '-' + RIGHT('000' + CONVERT(nvarchar(3), CarrierNumber), 3) + '-' + MasterBillNo + '-' + AirportOfDestination 
                        FROM Mawb 
                        WHERE (MasterBillNo = @MasterBillNo)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0 ? rows[0][0].ToString() : string.Empty;
        }
    }
}
