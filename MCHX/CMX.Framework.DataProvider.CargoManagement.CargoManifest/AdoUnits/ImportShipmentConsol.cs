﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ImportShipmentConsol:CmCoreDatabaseConnector<string>
    {
        public ImportShipmentConsol(string airbillNo, string masterbillNo, string userId, string typeConsol)
            : base(new CommandParameter("@MasterBillNo", masterbillNo),
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@IMPType", typeConsol),
            new CommandParameter("@Result", string.Empty))
        {
        }

        public override string CommandQuery
        {
            get { return "IMPORTSHIPMENTSCONSOL"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows[0][0].ToString();
        }
    }
}
