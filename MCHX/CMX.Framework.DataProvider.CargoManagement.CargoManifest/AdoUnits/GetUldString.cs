﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetUldString:ExportTableDatabaseConnector
    {
        public GetUldString(string selMaster)
            : base(new CommandParameter("@MasterBillNo", selMaster))
        {
            
        }

        public override string CommandQuery
        {
            get
            {
                return
                    @"SELECT  *, ISNULL(dbo.fnMAWBULdsAttachedString('@MasterBillNo'),'LOOSE-1') AS fnULDSTRING  
                    FROM Mawb  
                    WHERE (MasterBillNo = N'@MasterBillNo')";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
