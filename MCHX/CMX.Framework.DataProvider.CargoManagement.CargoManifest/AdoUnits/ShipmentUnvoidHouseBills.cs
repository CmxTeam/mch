﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentUnvoidHouseBills:CmCoreDatabaseConnector<string>
    {
        public ShipmentUnvoidHouseBills(string hawb, string gateway, string user)
            :base(
            new CommandParameter("@Hawb", hawb),
            new CommandParameter("@GATEWAY", gateway),
            new CommandParameter("@USER", user))
        {
            
        }
       public override string CommandQuery
        {
            get { 
                return @"DECLARE @Result nvarchar(MAX)
                        exec UnvoidShipment @Hawb, @GATEWAY, @USER, @Result = @Result output
                        select @Result"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override string ObjectInitializer(List<DataRow> rows)
        {
            return rows[0][0].ToString();
        }
    }
}
