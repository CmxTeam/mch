﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class SipmentExpediteGet : CmCoreDatabaseConnector<int>
    {
        public SipmentExpediteGet(string hawb) :
            base(new CommandParameter("@Hawb", hawb))
        {

        }

        public override string CommandQuery
        {
            get
            {
                return @"select EX From HAWB where Airbillno= @Hawb";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override int ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count < 1 ? 0 : rows[0][0].ToString().ToLower().Equals("true") ? 1 : 0;
        }
    }
}
