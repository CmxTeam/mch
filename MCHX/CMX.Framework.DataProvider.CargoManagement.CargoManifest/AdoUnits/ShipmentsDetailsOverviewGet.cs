﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentsDetailsOverviewGet : ExportTableDatabaseConnector
    {
        public ShipmentsDetailsOverviewGet(string airbillNo, string origin)
            : base(
            new CommandParameter("@AirbillNo", airbillNo),
            new CommandParameter("@Origin", origin))
        {

        }

        public override string CommandQuery
        {
            get { return @"CargoManifest_GetAwb "; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }


    }
}
