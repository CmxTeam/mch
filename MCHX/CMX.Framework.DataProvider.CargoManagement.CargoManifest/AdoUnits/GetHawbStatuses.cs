﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetHawbStatuses:CmCoreDatabaseConnector<bool>
    {
        public GetHawbStatuses(string airbillno)
            : base(
            new CommandParameter("@Airbillno",airbillno))
        {
        }

        public override string CommandQuery
        {
            get { return 
                    @"Select * 
                    from Hawb_statuses 
                    Where Airbillno in (@Airbillno) AND StatusID in (51, 52, 53, 56)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
