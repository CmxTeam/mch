﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class InsertDelayReasonMawb : CmCoreDatabaseConnector<bool>
    {
        public InsertDelayReasonMawb(string consol, string carrier, string gateway,
            string userId, string description)
            : base(
            new CommandParameter("@Consol", consol),
            new CommandParameter("@Carrier", carrier),
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@UserID", userId),
            new CommandParameter("@Description", description))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return
                  @"INSERT INTO Remarks(RecDate, Reference, CarrierNumber, Gateway, ActionID, UserID, Description) 
                    VALUES(GetDate(), @Consol, @Carrier, @Gateway, 47, @UserID, @Description);
                    INSERT INTO dbo.HostPlus_Remarks (Carrier, Consol, Gateway, Remark, Status) 
                    VALUES (@Carrier, @Consol, @Gateway, @Description, 0);";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
