﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetMawbUlDsData : ExportMultiTableDatabaseConnector
    {
        public GetMawbUlDsData(string code)
            : base(new CommandParameter("@MasterBillNo", code))
        {
        }

        public override string CommandQuery
        {
            get { return @"spMawbULDsWeb"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
