﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class AppearanceSettings:ExportMultiTableDatabaseConnector
    {
        public AppearanceSettings(string userId)
            : base(new CommandParameter("@UserID", userId))
        {
            
        }

        public override string CommandQuery
        {
            get { return @"spGridSettings"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
