﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class GetReportList : ExportTableDatabaseConnector
    {
        public GetReportList(string gateway, string reportType, string processName)
            :base(
            new CommandParameter("@Gateway", gateway),
            new CommandParameter("@ReportType", reportType),
            new CommandParameter("@ProcessName", processName))
        {
        }

        public override string CommandQuery
        {
            get { return @"GetReportList"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }
    }
}
