﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.AdoUnits
{
    class ShipmentExpediteStatusUpdate : CmCoreDatabaseConnector<bool>
    {
        public ShipmentExpediteStatusUpdate(int ex, string airbillNo)
            :base(
            new CommandParameter("@EX",ex),
            new CommandParameter("@AirbillNo", airbillNo))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return 
                    @"UPDATE Hawb SET EX = @EX 
                    WHERE (AirbillNo = @AirbillNo)";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
