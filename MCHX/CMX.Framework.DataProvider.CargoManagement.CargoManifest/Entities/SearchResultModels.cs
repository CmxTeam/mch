﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities
{
    public class SearchResultObject
    {
        public SearchResultObject()
        {
            Gateway = new Gateway();
            Configuration = new Configuration();
            Lane = new Lanes();
            FilterSetting = new FilterSettings();
        }
        public Gateway Gateway;
        public Configuration Configuration;
        public Lanes Lane;
        public FilterSettings FilterSetting;
       
    }

    public class Gateway
    {
        public Gateway()
        {
            Items = new List<GatewayItem>();
        }
        public List<GatewayItem> Items;
    }

    public class GatewayItem
    {
        public string Gateway;
        public string Icon;
    }

    public class Configuration
    {
        public Configuration()
        {
            Items = new List<ConfigurationItem>();
        }
        public List<ConfigurationItem> Items;
    }

    public class ConfigurationItem
    {
        public int Id;
        public string Name;
        public string Icon;
    }

    public class Lanes
    {
        public Lanes()
        {
            items = new List<LaneItem>();
        }

        public List<LaneItem> items;
    }

    public class LaneItem
    {
        public string Lanes;
        public string Dest;
        public string Gateway;
        public string Name;
        public string Icon;
    }

    public class FilterSettings
    {
        public FilterSettings()
        {
            item = new List<string>();
        }

        public List<string> item;
    }
}
