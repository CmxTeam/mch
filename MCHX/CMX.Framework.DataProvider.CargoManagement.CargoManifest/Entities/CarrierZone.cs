﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities
{
    public class CarrierZone
    {
        public string serviceType;
        public List<Dictionary<string, object>> data;
    }
}
