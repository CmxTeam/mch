﻿using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities
{
    public class Shimpent<T>
    {
        public int id;
        public string nodeText;
        public string nodeValue;
        public bool allowSelect;
        public int orderBy;
        public string ColumnNames;
        public DhtmlxGrid<T> data;

        public int total_hawb;
        public int total_pcs;
        public string total_gross_w;
        public string total_cf;
        public string total_ch_wt;
        public string Widths;
        public string total_ftc;
    }


}
