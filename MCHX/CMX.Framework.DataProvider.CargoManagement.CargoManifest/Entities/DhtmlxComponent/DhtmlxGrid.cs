﻿using System;
using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent
{
    public class DhtmlxGrid<T>
    {
        public DhtmlxGrid()
        {
            rows = new List<DhtmlxRow<T>>();
        }
        public List<DhtmlxRow<T>> rows;
    }



    public class DhtmlxRow<T>
    {
        public Int32 id;
        public string bgColor;
        public T userdata;
        public List<String> data = new List<string>();
    }

    public class GridUserData
    {
        public string Origin;
        public string AirbillNo;
        public string Destination;
        public string MasterBillNo;
        public string ZipCode;
    }

    public class GridImportUserData
    {
        public string LoadingPlan;
        public string MasterBillNo;
        public string RecId;
        public string Os;
        public string Ex;
        public string Cs;
        public string NodeName;
        public string Oh;
        public string Hz;
        public string Hd;
        public string Uk;
        public string HbStatus;
        public string AirbillNo;
    }
}
