﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent
{
    public class DhtmlxCombo
    {
        public DhtmlxCombo()
        {
            options = new List<DhtmlxComboItem>();
        }
        public List<DhtmlxComboItem> options;
    }

    public class DhtmlxComboItem
    {
        public string value;
        public string text;
    }
}
