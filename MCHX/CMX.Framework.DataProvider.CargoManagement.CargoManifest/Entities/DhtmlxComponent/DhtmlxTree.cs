﻿using System.Collections.Generic;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent
{
    public class DhtmlxTree
    {
        public DhtmlxTree()
        {
            item = new List<dynamic>();
        }

        public int id;
        public List<dynamic> item;
    }

    public class DhtmlxTreeItem
    {
        public int id;
        public string text;
        public string child;
        public string img;

        public DhtmlxTreeItem()
        {
            userdata = new List<TreeUserData>();
            item = new List<dynamic>();
        }


        public List<dynamic> item;
        public List<TreeUserData> userdata;
        public string im1;
        public string im2;
    }

    public class TreeUserData
    {
        public string name;
        public string content;
    }


}
