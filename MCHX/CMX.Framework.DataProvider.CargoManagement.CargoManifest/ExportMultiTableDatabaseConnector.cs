﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Threading;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest
{
    public abstract class ExportMultiTableDatabaseConnector :
        AdoNetMultyTableDatabaseConnector<List<ShipmentTable>>
    {
        public ExportMultiTableDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public ExportMultiTableDatabaseConnector()
        {

        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMJFK"].ConnectionString; }
        }

        public override List<ShipmentTable> ObjectInitializer(
            List<DataTable> tables)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var dicTables = new List<ShipmentTable>();

            foreach (var table in tables)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

                foreach (DataRow dr in table.Rows)
                {
                    var row = new Dictionary<string, object>();
                    foreach (DataColumn col in table.Columns)
                    {
                       row.Add(col.ColumnName, dr[col].ToString());
                    }
                    rows.Add(row);
                }
                var dicTable = new ShipmentTable();
                dicTable.TableName = table.TableName;
                dicTable.Rows = new List<Dictionary<string, object>>();
                dicTable.Rows.AddRange(rows);
                dicTables.Add(dicTable);
            }
            return dicTables;
        }
    }

    public class ShipmentTable
    {
        public string TableName { get; set; }
        public List<Dictionary<string, object>> Rows { get; set; }
    }
}
