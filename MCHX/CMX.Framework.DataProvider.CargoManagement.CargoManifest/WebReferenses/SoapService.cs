﻿using System;
using System.Configuration;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.WebReferenses
{

    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [WebServiceBinding(Name = "WSPieceScanSoap", Namespace = "http://www.CargoMatrix.com/")]
    class SoapService : SoapHttpClientProtocol
    {
        public SoapService()
        {
            this.Url = ConfigurationManager.ConnectionStrings["FSPScreeningWS"].ToString();
        }

        [SoapDocumentMethod(
            Action = "http://www.CargoMatrix.com/CancelScreening",
            RequestNamespace = "http://www.CargoMatrix.com/",
            ResponseNamespace = "http://www.CargoMatrix.com/",
            Use = System.Web.Services.Description.SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public void CancelScreening(string conn, string airbillNo, string userId)
        {
            this.Invoke("CancelScreening", new object[] { conn, airbillNo, userId });
        }

        [SoapDocumentMethod(
            Action = "http://www.CargoMatrix.com/IsFSPCompletedByHawb",
            RequestNamespace = "http://www.CargoMatrix.com/",
            ResponseNamespace = "http://www.CargoMatrix.com/",
            Use = System.Web.Services.Description.SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public void IsFspCompletedByHawb(string airbillNo)
        {
        }

        [SoapDocumentMethod(Action = "http://www.CargoMatrix.com/ProcessFSP",
          RequestNamespace = "http://www.CargoMatrix.com/",
          RequestElementName = "ProcessFSP",
          ResponseNamespace = "http://www.CargoMatrix.com/")]
        [WebMethod(Description = "Process FSP")]
        public void ProcessFsp(string airbillNo)
        {
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(HawbIrregularity))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(MawbULD))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FPCMasterBillReason))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FPCHouseBillReason))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(GatewayLocation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ULDType))]
    public class Base
    {
        public int RecID;
        public DateTime RecDate;
        public Statuses ObjectStatus;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public enum Statuses
    {
        Fresh,
        Clean,
        Dirty,
        Deleted
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public class HawbIrregularity
    {
        public string Status;
        public string AirbillNo;
        public string Pieces;
        public string Description;
        public string Gateway;
        public string AQM;
        public string SuperId;
        public string AQM_Reference;
        public string MasterbillNo;
        public string UserId;
        public DateTime LastDate;
        public int AQM_Closed;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public class MawbULD
    {
        public string Carrier;
        public string MasterBillNo;
        public string ULD;
        public string ULDNo;
        public string Reference;
        public string GroupReference;
        public double Weight;
        public int Pieces;
        public string Condition;
        public int Recoverd;
        public int GatewayLocationID;
        public int MasterBillID;
        public int ULDTypeID;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public class FPCMasterBillReason
    {
        public string Name;
        public bool Display;
        public int Priority;
        public string ReasonCode;
        public bool IsDamage;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public class FPCHouseBillReason
    {
        public string Name;
        public bool Display;
        public int Priority;
        public string ReasonCode;
        public bool IsDamage;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public class GatewayLocation
    {
        public string Lane;
        public string Gateway;
        public string Area;
        public int Order;
        public string Description;
        public int WarehouseID;
        public StorageTypes Type;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public enum StorageTypes
    {
        InTransit,
        Recovered,
        Door,
        Rack,
        Bin,
        TemperatureControl,
        HighValue,
        Hazardous,
        ScreeningArea,
        Forklift,
        Missing
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.CargoMatrix.com/")]
    public class ULDType
    {
        public string ULD;
        public string ULDDescription;
        public string CarrierType;
    }
}
