﻿using System;
using System.Collections.Generic;
using System.Web.Services.Protocols;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.WebReferenses
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.34209"),
     System.Diagnostics.DebuggerStepThroughAttribute(),
     System.ComponentModel.DesignerCategoryAttribute("code"),
     System.Web.Services.WebServiceBindingAttribute(Name = "RecoveryIssueReasonsSoap", Namespace = "http://tempuri.org/"),
     System.Xml.Serialization.XmlIncludeAttribute(typeof(BizObject))]
    public class RecoveryIssueReasons : SoapHttpClientProtocol
    {
        private bool _useDefaultCredentialsSetExplicitly;

        public RecoveryIssueReasons()
        {
            this.Url = "http://10.0.0.235/RecoveryIssueReasonsWebService/RecoveryIssueReasons.asmx";
            if (IsLocalFileSystemWebService(this.Url))
            {
                this.UseDefaultCredentials = true;
                this._useDefaultCredentialsSetExplicitly = false;
            }
            else
            {
                _useDefaultCredentialsSetExplicitly = true;
            }
        }
        [SoapDocumentMethodAttribute("http://tempuri.org/SearchRecoveryIssueReason",
            RequestNamespace = "http://tempuri.org/",
            ResponseNamespace = "http://tempuri.org/",
            Use = System.Web.Services.Description.SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public List<RecoveryIssueReason> SearchRecoveryIssueReason(string RecoveryIssueReasonCode, string RecoveryIssue, string ForSystem)
        {
            var results = this.Invoke("SearchRecoveryIssueReason",
                new object[] { RecoveryIssueReasonCode, RecoveryIssue, ForSystem });
            return (List<RecoveryIssueReason>)results[0];
        }


        private static bool IsLocalFileSystemWebService(string url)
        {
            return !string.IsNullOrEmpty(url);
        }
    }

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RecoveryIssueReason)),
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234"),
     SerializableAttribute,
     System.Diagnostics.DebuggerStepThroughAttribute(),
     System.ComponentModel.DesignerCategoryAttribute("code"),
     System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public class BizObject
    {
        private int idField;
        private string codeField;
        private string reasonField;
        private DateTime addedDateField;
        private string addedByField;
        private int sortField;
        private string forSystemField;
    }

    public class RecoveryIssueReason : BizObject
    {
        private int idField;
        private string codeField;
        private string reasonField;
        private DateTime addedDateField;
        private string addedByField;
        private int sortField;
        private string forSystemField;

        public int ID
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        public string Code
        {
            get { return this.codeField; }
            set { this.codeField = value; }
        }

        public string Reason
        {
            get { return this.reasonField; }
            set { this.reasonField = value; }
        }
        public DateTime AddedDate
        {
            get { return this.addedDateField; }
            set { this.addedDateField = value; }
        }

        public string AddedBy
        {
            get { return this.addedByField; }
            set { this.addedByField = value; }
        }

        public int Sort
        {
            get { return this.sortField; }
            set { this.sortField = value; }
        }

        public string ForSystem
        {
            get { return this.forSystemField; }
            set { this.forSystemField = value; }
        }
    }
}
