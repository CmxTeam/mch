﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.DatabaseManager;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.CargoManagement.Controllers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SearchController : CmCommonController
    {
        private readonly ExportDatabaseManager _manager = new ExportDatabaseManager();

        /// <summary>
        /// Get search result
        /// </summary>
        /// <param name="masterbill"></param>
        /// <param name="housebill"></param>
        /// <param name="reference"></param>
        /// <param name="domesticConsole"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxTree> GetListResult(string masterbill = null, string housebill = null,
            string reference = null, string domesticConsole = null)
        {
            if (!string.IsNullOrEmpty(masterbill))
            {
                return MakeTransactional((() => _manager.GetAdvSearchResult(masterBilNo: masterbill)));
            }
            if (!string.IsNullOrEmpty(housebill))
            {
                return MakeTransactional((() => _manager.GetAdvSearchResult(airbillNo: housebill)));
            }
            if (!string.IsNullOrEmpty(reference))
            {
                return MakeTransactional((() => _manager.GetAdvSearchResult(reference: reference)));
            }
            if (!string.IsNullOrEmpty(domesticConsole))
            {
                return MakeTransactional((() => _manager.GetDomesticConsolList(domesticConsole)));
            }
            return new DataContainer<DhtmlxTree>()
            {
                Status = new TransactionStatus()
                {
                    Message = "Valid parameter",
                    Status = false
                }
            };
        }

        /// <summary>
        /// Gets description of the masterbill by code
        /// </summary>
        /// <param name="masterbill"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<string> GetMasterbillDescr(string masterbill)
        {
            return MakeTransactional((() => _manager.GetMasterbillTitle(masterbill)));
        }

        /// <summary>
        /// Get consolidation information
        /// </summary>
        /// <param name="consol"></param>
        /// <param name="airbillNo"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public DataContainer<List<ShipmentTable>> GetMasterbillDetails(string consol)
        {
            if (string.IsNullOrEmpty(consol))
            {
                return new DataContainer<List<ShipmentTable>>();
            }
            var userType = _manager.GetUserType(ApplicationUser.UserId);
            return MakeTransactional((() => _manager.GetMasterbillDetails(consol, ApplicationUser.UserName, "JFK", userType)));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<List<ShipmentTable>> GetMasterbilldetailsByFilter(string consol, string filter)
        {
            if (string.IsNullOrEmpty(consol))
            {
                return new DataContainer<List<ShipmentTable>>();
            }
            var userType = _manager.GetUserType(ApplicationUser.UserId);
            return MakeTransactional((() => _manager.GetMasterbillDetails(consol, ApplicationUser.UserName, filter, "JFK", userType)));
        }

        [HttpGet]
        public DataContainer<string> AlSearchresultsHawb(string airbillNo)
        {
            return MakeTransactional((() => _manager.IsExistShipmentList("", airbillNo, "HAWB", "JFK")));
        }

        [HttpGet]
        public DataContainer<string> AlSearchresultsMawb(string masterbillNo)
        {
            return MakeTransactional((() => _manager.IsExistShipmentList(masterbillNo, "", "MAWB", "JFK")));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<bool> ShowOldMawb()
        {
            return MakeTransactional((() => _manager.ShowOldMawb("JFK")));
        }

        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetDispatchTimerInfo(string consol)
        {
            return MakeTransactional((() => _manager.GetDispatchTimerInfo(consol)));
        }

        #region Update Transport type
        /// <summary>
        /// HttpPost
        /// </summary>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> UpdateTransportType(string type, string code)
        {
            return MakeTransactional((() => _manager.UpdateTransportType(type, code, ApplicationUser.UserName)));
        }
        #endregion

        #region Dispositions
        /// <summary>
        /// Get list od MAWB dispositions
        /// </summary>
        /// <param name="code">Masterbill No</param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetDispositionsList(string code)
        {
            return MakeTransactional((() => _manager.GetDispositionList(code)));
        }
        #endregion

        #region Seal verification
        /// <summary>
        /// Seal type list for Seal verification foem
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetDispositionSealTypes()
        {
            return MakeTransactional((() => _manager.GetDispositionSealTypes()));
        }

        /// <summary>
        /// TET parameters 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxCombo> GetTetApplLevel()
        {
            return MakeTransactional((() => _manager.GetTetApplLevel()));
        }

        /// <summary>
        /// Seal information: save data
        /// </summary>
        /// <param name="carrierId"></param>
        /// <param name="consol"></param>
        /// <param name="sealTypeId"></param>
        /// <param name="sealRef"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> SaveSealInformation(int carrierId, string consol, int sealTypeId, string sealRef,
            string gateway)
        {
            return
                MakeTransactional(
                    (() => _manager.SaveSealInformation(carrierId, consol, sealTypeId, sealRef, ApplicationUser.UserName, gateway)));
        }
        #endregion

        #region Reopen consolidation
        /// <summary>
        /// Reopen consolidation
        /// </summary>
        /// <param name="consol">Masterbill no</param>
        /// <param name="gateway">Current gateway</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> ReopenConsol(string consol, string gateway)
        {
            return MakeTransactional((() => _manager.ReopenConsol(consol, ApplicationUser.UserName, gateway)));
        }
        #endregion

        #region Delete consoldation
        /// <summary>
        /// Delete consolidation button
        /// </summary>
        /// <param name="consol">Consolidation number</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> DeleteConsolidation(string consol)
        {
            return MakeTransactional((() => _manager.DeleteConsolidation(consol, ApplicationUser.UserName)));
        }
        #endregion

        #region ULD/LOOSE
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetUldTypes()
        {
            return MakeTransactional((() => _manager.GetUldTypes()));
        }
        [HttpGet]
        public DataContainer<List<ShipmentTable>> GetMawbUlDsData(string masterbillNo)
        {
            return MakeTransactional((() => _manager.GetMawbUlDsData(masterbillNo)));
        }
        [HttpPost]
        public DataContainer<string> InsertMAwbUlds(string carrier, string masterbillNo, int uldTypeId, string uld, string uldNo)
        {
            return MakeTransactional((() => _manager.InsertMAwbUlds(carrier, masterbillNo, uldTypeId, uld, uldNo, 0, "Add")));
        }
        #endregion

        #region Lookout time


        public DataContainer<string> GetConsolCloseOut(string masterbillNo)
        {
            return MakeTransactional((() => _manager.GetConsolCloseOut(masterbillNo)));
        }

        public DataContainer<bool> SetConsolCloseOut(string masterbillNo, int id, DateTime time)
        {
            return MakeTransactional((() => _manager.UpdateConsolCloseOut(masterbillNo, id, time)));
        }
        #endregion

        #region Finalize master house
        /// <summary>
        /// Finalize master house
        /// </summary>
        /// <param name="masterbillNo"></param>
        /// <param name="account">table(Consol). field(LaneName)</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> FinalyzeMasterHouse(string masterbillNo, string account)
        {
            return MakeTransactional((() => _manager.FinalyzeMasterHouse(ApplicationUser.UserName, masterbillNo, "JFK", account)));
        }
        #endregion

        #region Open Task
        /// <summary>
        /// Open task
        /// </summary>
        /// <param name="masterbillNo"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> OpenTask(string masterbillNo)
        {
            return MakeTransactional((() => _manager.ReopenTask(masterbillNo, ApplicationUser.UserName)));
        }
        #endregion

        #region Finalize

        /*
         * Load informatiom to the form
         */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shipmentType"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetReportList(string shipmentType)
        {
            if (!string.IsNullOrEmpty(shipmentType))
            {
                if (shipmentType.Trim().ToUpper().Equals("AS"))
                {
                    return MakeTransactional((() => _manager.GetReportList("JFK", "EXPORT", "MASTERHOUSE")));
                }
            }
            return MakeTransactional((() => _manager.GetReportList("JFK", "EXPORT", "CONSOL")));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetApplicationPrinters()
        {
            return MakeTransactional((() => _manager.GetApplicationPrinters()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consol"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> CloseConsol(string consol)
        {
            return MakeTransactional((() => _manager.CloseConsol("JFK", ApplicationUser.UserName, consol)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterbill"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<string> GetMawbTotalValue(string masterbill)
        {
            var result = _manager.GetMawbTotalValue(masterbill);
            return MakeTransactional(() => result.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterbill"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<Dictionary<string, string>> CheckAesInfo(string masterbill)
        {
            var result = _manager.CheckAesInfo(masterbill);
            return MakeTransactional(() =>
               result > 0 ?
               new Dictionary<string, string>() { { "imgAES", "True" } } :
               new Dictionary<string, string>() { { "imgAES", "False" } });
        }

        /*
         * Buttons functionality
         */

        /// <summary>
        /// Print all documents
        /// </summary>
        /// <param name="airbill"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [HttpPost]
        public DataContainer<bool> PrintAll(string airbill, string origin)
        {
            return MakeTransactional((() => _manager.ConsolPrinterPorts(airbill, origin, ApplicationUser.UserName)));
        }

        /*
         * Print button
         */
        [Authorize]
        [HttpPost]
        public DataContainer<bool> Print(string reportName, string masterbill, string printer)
        {
            if (reportName.ToUpper().Equals("ASSEMBLY"))
            {
                return
                    MakeTransactional(
                        (() =>
                            _manager.InsertReportServerJobs("JFK", reportName, masterbill, printer,
                                ApplicationUser.UserName)));
            }

            return
                MakeTransactional(
                    (() =>
                        _manager.InsertReportServerJobs("JFK", reportName, masterbill, printer,
                            ApplicationUser.UserName)));

        }

        [HttpPost]
        public DataContainer<bool> PrintLabels(string masterbill, string carrierNo)
        {
            return MakeTransactional((() => _manager.InsertHostPlusLabels("JFK", masterbill, carrierNo)));
        }

        /*
         * Email
         */
        [Authorize]
        [HttpPost]
        public DataContainer<bool> SendEmail(string mailfrom, string mailTo, string body, string subject,
            string userId, string gateway, string consol, string reps)
        {
            return
                MakeTransactional(
                    (() =>
                        _manager.CreateEmail(mailfrom, mailTo, body, subject, ApplicationUser.UserName, "JFK", consol, reps)));
        }

        /*
         * Close Mawb
         */
        [HttpPost]
        public DataContainer<bool> CloseMawb(string carrier, string masterbill, string gateway, string user)
        {
            return MakeTransactional((() => _manager.CloseMawb(carrier, masterbill, "JFK", ApplicationUser.UserName)));
        }

        #endregion

        #region Spot rate
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetMawbSpotInfo(string masterbill)
        {
            return MakeTransactional((() => _manager.GetMawbSpotInfo(masterbill)));
        }

        [HttpPost]
        public DataContainer<bool> UpdateSpotRateInformation(string rateSpotNo, string contract, string salesRep,
            string salesCommision, string rateSpot, string rateSpotType, string masterbill,
            bool isSpot)
        {
            return
                MakeTransactional(
                    (() => _manager.UpdateSpotRateInformation(rateSpotNo, contract, salesRep, salesCommision, rateSpot,
                        rateSpotType, masterbill, "JFK", isSpot)));
        }

        #endregion

        #region Transmit consolidation
        [Authorize]
        [HttpPost]
        public DataContainer<bool> TransmitConsolidation(string consol, string gateway)
        {
            return MakeTransactional((() => _manager.TransmitConsolidation(consol, ApplicationUser.UserName, gateway)));
        }
        #endregion

        #region Download functionality
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> DownloadShipmentQueueGet()
        {
            return MakeTransactional(() => _manager.DownloadShipmentQueueGet("JFK"));
        }
        [Authorize]
        [HttpGet]
        public DataContainer<string> FastDownloadShipment(string type, string shipment)
        {
            if (!type.Trim().ToLower().Equals("masterbill") && !type.Trim().ToLower().Equals("housebill"))
            {
                return new DataContainer<string>()
                {
                    Data = "Invalid parameter",
                    Status = new TransactionStatus()
                        {
                            Message = "Error",
                            Status = false
                        }
                };
            }
            return
                MakeTransactional(
                    () => _manager.FastDownloadShipment(type, shipment, "JFK", ApplicationUser.UserName, "RMB"));
        }
        #endregion

        #region Modify MAWB number
        [Authorize]
        [HttpPost]
        public DataContainer<string> RenameMawb(string consol, string newConsol)
        {
            try
            {
                int.Parse(newConsol);
                var startDigits = int.Parse(newConsol.Substring(0, newConsol.Length - 1));
                var mod7Digit = startDigits % 7;
                var calculateNumber = startDigits.ToString() + mod7Digit.ToString();

                if (newConsol == calculateNumber.ToString().PadLeft(8, '0'))
                {
                    return
                        MakeTransactional(() => _manager.RenameMawb(consol, newConsol, ApplicationUser.UserName, "JFK"));
                }
                return new DataContainer<string>()
                {
                    Data = string.Empty,
                    Status = new TransactionStatus()
                    {
                        Message = "Check digist Failed. Please Check the Masterbill no and try again.",
                        Status = false
                    }
                };
            }
            catch (Exception)
            {
                return new DataContainer<string>()
                {
                    Data = string.Empty,
                    Status = new TransactionStatus()
                    {
                        Message = "Please enter a valid Masterbillno.",
                        Status = false
                    }
                };
            }
        }
        #endregion

        #region Print documents
        [Authorize]
        [HttpPost]
        public DataContainer<bool> PrintDocument(string description, string shipmentType,
            string reportName, string masterbill, string printer, string carrierNo, string acName)
        {
            if (description.Trim().ToLower().Contains("labels"))
            {
                if (shipmentType.Trim().ToLower().Equals("as"))
                {
                    return MakeTransactional(
                         (() =>
                             _manager.ReportServerJobLocal("JFK", reportName, masterbill, printer,
                                 ApplicationUser.UserName)));
                }
                else
                {
                    return MakeTransactional((() => _manager.InsertHostPlusLabels("JFK", masterbill, carrierNo)));
                }
            }

            if (description.Trim().ToLower().Equals("manifest"))
            {
                if (!string.IsNullOrEmpty(acName))
                {
                    if (acName.Trim().ToUpper().Equals("IAI"))
                    {
                        return
                    MakeTransactional(
                        (() =>
                            _manager.InsertReportServerJobs("JFK", "IAIManifest", masterbill, printer,
                                ApplicationUser.UserName)));
                    }
                    return
                    MakeTransactional(
                        (() =>
                            _manager.InsertReportServerJobs("JFK", reportName, masterbill, printer,
                                ApplicationUser.UserName)));
                }

            }

            return
                MakeTransactional(
                    (() =>
                        _manager.InsertReportServerJobs("JFK", reportName, masterbill, printer,
                            ApplicationUser.UserName)));

        }

        [HttpGet]
        public DataContainer<List<Dictionary<string, string>>> GetPrintButtonSettings(string consol)
        {
            return MakeTransactional(() => _manager.GetPrintButtonSettings(consol));
        }
        #endregion
    }
}
