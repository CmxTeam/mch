﻿using System;
using System.Web.Http.Cors;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.DatabaseManager;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.CargoManagement.Controllers;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ConsolidationController : CmCommonController
    {
        private readonly ExportDatabaseManager _manager = new ExportDatabaseManager();

        /// <summary>
        /// Get list of consolidations from MAWB by datetime
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="execDate"></param>
        /// <returns></returns>
        public DataContainer<DhtmlxGrid<GridUserData>> GetFromMawbByDate(string destination, DateTime execDate)
        {
            return MakeTransactional((() => _manager.GetTodaysConsolsFromMawb("JFK", destination, execDate, ApplicationUser.UserName)));
        }

        public DataContainer<int> GetUk(string masterbill)
        {
            return MakeTransactional((() => _manager.GetUk(masterbill)));
        } 

    }
}
