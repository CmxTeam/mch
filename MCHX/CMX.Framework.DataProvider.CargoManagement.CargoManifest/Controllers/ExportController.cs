﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.DatabaseManager;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.Entities.DhtmlxComponent;
using CMX.Framework.DataProvider.CargoManagement.CargoManifest.RecoveryIssueReasons;
using CMX.Framework.DataProvider.CargoManagement.Controllers;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CargoManifest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ExportController : CmCommonController
    {
        private readonly ExportDatabaseManager _manager = new ExportDatabaseManager();

        [HttpPost]
        public void SetBaseUrl(string url)
        {
            ServerInformation.BaseUrl = url;
        }

        /// <summary>
        /// Gets Filters list in dhtmlx combo format
        /// </summary>
        /// <param name="gatewayId">Gateway key</param>
        /// <param name="department">Department key</param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxCombo> GetFilterBy(string gatewayId, string department)
        {
            if (string.IsNullOrEmpty(gatewayId) || string.IsNullOrEmpty(department))
            {
                return new DataContainer<DhtmlxCombo>();
            }
            var data = _manager.GetFilterBy(department);
            var result = new DhtmlxCombo();

            result.options.AddRange(data.Select(o => new DhtmlxComboItem()
            {
                text = o["FilterName"].ToString(),
                value = o["FilterCode"].ToString()
            }));

            return MakeTransactional((() => result));
        }

        /// <summary>
        /// Gets gateway, table configurations, lanes and filter settings of the table
        /// </summary>
        /// <param name="gatewayId">Current gateway</param>
        /// <param name="filter">Selected filter from filter panel</param>
        /// <param name="selDate">Datetime from filter panel</param>
        /// <returns>Returns SearchResultObject object which consist gateway, table configurations, lanes and filter settings</returns>
        [Authorize]
        [HttpGet]
        public DataContainer<DhtmlxTree> GetSearchResult(string gatewayId, string filter,
            DateTime selDate)
        {
            if (string.IsNullOrEmpty(gatewayId) || string.IsNullOrEmpty(filter))
            {
                return new DataContainer<DhtmlxTree>();
            }
            const string format = "MM/dd/yyyy";
            var convertdate = selDate.ToString(format);

            return MakeTransactional((() => _manager.GetSearchList(gatewayId, ApplicationUser.UserName, filter, convertdate)));
        }

        /// <summary>
        /// Get shipment type for filter panel
        /// </summary>
        /// <returns>Return data in dhtmlx combo format</returns>
        [HttpGet]
        public DataContainer<DhtmlxCombo> GetShipmentType()
        {
            var shipType = new DhtmlxCombo();
            shipType.options.AddRange(
                new List<DhtmlxComboItem>()
                {
                    new DhtmlxComboItem()
                    {
                        text = "Ground",
                        value = "GR"
                    },
                    new DhtmlxComboItem()
                    {
                        text = "Air Express",
                        value = "EX"
                    },
                    new DhtmlxComboItem()
                    {
                        text = "Air Freight",
                        value = "FT"
                    },
                    new DhtmlxComboItem()
                    {
                        text = "Ocean Freight",
                        value = "OC"
                    }
                });

            return MakeTransactional((() => shipType));
        }

        /// <summary>
        /// Gets legend description
        /// </summary>
        /// <param name="mode">EXPORT/IMPORT/TASKMANAGER/PROFILE modes</param>
        /// <returns>Return data in dhtmlx grid format</returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetLegend(string mode)
        {
            return string.IsNullOrEmpty(mode)
                ? new DataContainer<DhtmlxGrid<GridUserData>>()
                : MakeTransactional(() => _manager.GetLegend(mode));
        }

        /// <summary>
        /// Get tab items for shipments
        /// </summary>
        /// <param name="gateway">Gateway</param>
        /// <param name="destination">Destination</param>
        /// <param name="date">Date time</param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetTabItems(string gateway, string destination,
            DateTime date)
        {
            if (string.IsNullOrEmpty(gateway) || string.IsNullOrEmpty(destination))
            {
                return new DataContainer<List<Dictionary<string, object>>>();
            }
            const string format = "MM/dd/yyyy";
            var convertdate = date.ToString(format);

            return MakeTransactional((() => _manager.GeTabItems(gateway, destination, convertdate, string.Empty)));
        }

        /// <summary>
        /// Get shipment list by code
        /// </summary>
        /// <param name="code">Masterbill code</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public DataContainer<Shimpent<GridUserData>> GetShipmentsByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return new DataContainer<Shimpent<GridUserData>>();
            }
            return MakeTransactional((() => _manager.GetShipmentsByCode(code, ApplicationUser.UserName)));
        }

        /// <summary>
        /// Gets advansed search result 
        /// </summary>
        /// <param name="masterbillNo"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxTree> GetAdvSearchResult(string masterbillNo)
        {
            return string.IsNullOrEmpty(masterbillNo)
                ? new DataContainer<DhtmlxTree>()
                : MakeTransactional((() => _manager.GetAdvSearchResult(masterbillNo)));
        }

        /// <summary>
        /// Create new consolidation plan
        /// </summary>
        /// <param name="cargoManifest"></param>
        /// <param name="gateway"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public DataContainer<long> AssignConsol(string cargoManifest, string gateway, string destination)
        {
            if (string.IsNullOrEmpty(cargoManifest) || string.IsNullOrEmpty(gateway) ||
                string.IsNullOrEmpty(destination))
            {
                return new DataContainer<long>();
            }
            return MakeTransactional((() => _manager.AssignConsole(cargoManifest, gateway, destination, ApplicationUser.UserName)));
        }

        /// <summary>
        /// Get shipmen list for wiew weight tab
        /// </summary>
        /// <param name="gateway"></param>
        /// <param name="destination"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<List<Shimpent<GridUserData>>> GetShipments(string gateway, string destination, string account)
        {
            if (string.IsNullOrEmpty(gateway) || string.IsNullOrEmpty(destination))
            {
                return new DataContainer<List<Shimpent<GridUserData>>>();
            }
            return MakeTransactional((() => _manager.GetShipments(gateway, destination, ApplicationUser.UserName)));
        }

        /// <summary>
        /// Delete loading plan
        /// </summary>
        /// <param name="cargoManifestId">Cargo manifest id</param>
        /// <returns></returns>
        [HttpPost]
        public DataContainer<bool> DeleteLoadingPlan(string cargoManifestId)
        {
            return string.IsNullOrEmpty(cargoManifestId)
                ? new DataContainer<bool>()
                : MakeTransactional((() => _manager.DeleteLoadingPlan(cargoManifestId)));
        }
        #region Shipment Details
        /// <summary>
        /// Dimensions list for shipment details
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns>Return data in dhtmlxgrid format</returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetDimensions(string airbillNo, string origin)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<DhtmlxGrid<GridUserData>>();
            }
            return MakeTransactional((() => _manager.GetDimensions(airbillNo, origin)));
        }

        /// <summary>
        /// Details: Get remarks list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetRemarksDetails(string airbillNo, string origin)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<DhtmlxGrid<GridUserData>>();
            }
            return MakeTransactional((() => _manager.GetRemarksDetails(airbillNo, origin)));
        }

        /// <summary>
        /// Details: Overview list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetOverview(string airbillNo, string origin)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<List<Dictionary<string, object>>>();
            }
            return MakeTransactional((() => _manager.GetOverView(airbillNo, origin)));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> PrintHawb(string airbill)
        {
            return MakeTransactional(() => _manager.PrintHawb(ApplicationUser.UserName, airbill, "JFK"));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> PrintHawbItar(string airbill)
        {
            return MakeTransactional(() => _manager.PrintHawbItar(ApplicationUser.UserName, airbill, "JFK"));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> PrintHawbPo(string airbill)
        {
            return MakeTransactional(() => _manager.PrintHawbPo(ApplicationUser.UserName, airbill, "JFK"));
        }

        /// <summary>
        /// Details: Get charges list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetCharges(string airbillNo, string origin)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<DhtmlxGrid<GridUserData>>();
            }
            return MakeTransactional((() => _manager.GetCharges(airbillNo, origin)));
        }

        /// <summary>
        /// Details: Get references list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetReferences(string airbillNo, string origin)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<DhtmlxGrid<GridUserData>>();
            }
            return MakeTransactional((() => _manager.GetReferences(airbillNo, origin)));
        }

        [HttpPost]
        public DataContainer<bool> InsertReference(string housebill, string origin, string reference)
        {
            if (string.IsNullOrEmpty(housebill) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<bool>()
                {
                    Data = false,
                    Status = new TransactionStatus()
                    {
                        Message = "Invalid data",
                        Status = false
                    }
                };
            }
            return MakeTransactional((() => _manager.InsertReference(housebill, origin, reference)));
        }

        /// <summary>
        /// Details: Get damages
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetDamages(string airbillNo)
        {
            return string.IsNullOrEmpty(airbillNo) ? new DataContainer<DhtmlxGrid<GridUserData>>() : MakeTransactional((() => _manager.GetDamages(airbillNo)));
        }
        /// <summary>
        /// Details: Get locations list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> GetLocations(string airbillNo)
        {
            return string.IsNullOrEmpty(airbillNo) ? new DataContainer<DhtmlxGrid<GridUserData>>() : MakeTransactional((() => _manager.GetLocations(airbillNo)));
        }

        #endregion

        #region Hold/Release shipment
        /// <summary>
        /// Hold shipment hold release list
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetShipmentHoldRelease(string airbillNo, string origin)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return new DataContainer<List<Dictionary<string, object>>>();
            }
            return MakeTransactional((() => _manager.ShipmentHoldReleaseGet(airbillNo, origin)));
        }

        /// <summary>
        /// Hold release shipment
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <param name="reason"></param>
        /// <param name="gateway"></param>
        /// <param name="holdStatus"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> HoldReleaseShipment(string airbillNo, string origin,
            string reason, string gateway,
            string holdStatus)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin) || string.IsNullOrEmpty(reason)
                || string.IsNullOrEmpty(gateway) || string.IsNullOrEmpty(holdStatus))
                return new DataContainer<bool>() { Data = false, Status = new TransactionStatus() { Message = "All fields are reqired.", Status = false } };
            var lstAirbillNo = airbillNo.Split(',');

            foreach (var code in lstAirbillNo)
            {
                var completed = _manager.ShipmentHoldReleaseInsert(code, origin, reason, gateway, ApplicationUser.UserName, holdStatus);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return MakeTransactional((() => true));
        }
        #endregion

        #region Expedite YES/NO
        /// <summary>
        /// Change expedite status to YES/ NO
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <returns></returns>
        [HttpPost]
        public DataContainer<bool> ChangeExpediteStatus(string airbillNo)
        {
            if (string.IsNullOrEmpty(airbillNo)) return MakeTransactional((() => false));

            var lstAirbillNo = airbillNo.Split(',');
            foreach (var code in lstAirbillNo)
            {
                var completed = _manager.ShipmentExpediteStatus(code);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return MakeTransactional((() => true));
        }
        #endregion

        #region Change Masterbill destination
        /// <summary>
        /// Change Masterbill Destination
        /// </summary>
        /// <param name="housebill"></param>
        /// <param name="newDest"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> ChangeMasterbillDest(string housebill, string newDest, string gateway)
        {
            if (string.IsNullOrEmpty(housebill) || string.IsNullOrEmpty(newDest) ||
                string.IsNullOrEmpty(gateway))
            {
                return MakeTransactional((() => false));
            }

            var lstHousebill = housebill.Split(',');
            foreach (var code in lstHousebill)
            {
                var completed = _manager.ChangeMasterbillDestination(code, newDest, ApplicationUser.UserName, gateway);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return new DataContainer<bool>() { Data = true };
        }
        #endregion

        #region Download Selected Shipment(s)

        /// <summary>
        /// Download Selected Shipments
        /// </summary>
        /// <param name="airbrilno"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> DownloadSelectedshipments(string airbrilno, string gateway)
        {
            if (string.IsNullOrEmpty(airbrilno) || string.IsNullOrEmpty(gateway))
            {
                return new DataContainer<DhtmlxGrid<GridUserData>>();
            }
            return MakeTransactional((() => _manager.DownloadSelectedShipments(airbrilno, gateway, ApplicationUser.UserName)));
        }
        #endregion

        #region Update UPS Zones(s)
        /// <summary>
        /// Get Carrires zones
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<CarrierZone> GetCarrierZones(string serviceType)
        {
            return string.IsNullOrEmpty(serviceType) ?
                MakeTransactional(() => new CarrierZone()) :
                MakeTransactional((() => new CarrierZone()
                {
                    serviceType = serviceType,
                    data = _manager.GetCarrierZones(serviceType)
                }));
        }

        /// <summary>
        /// Update UPS Zone(s)
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="zoneCode"></param>
        /// <param name="serviceCode"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> UpdateUpsZone(string zipCode, string zoneCode, string serviceCode)
        {
            if (string.IsNullOrEmpty(serviceCode))
            {
                return MakeTransactional((() => false));
            }

            return
                MakeTransactional(
                    (() => _manager.UpdateUpsZone(zipCode, zoneCode, "JFK", ApplicationUser.UserName, serviceCode)));
        }

        #endregion
        /*
         * Update screen status
         */

        #region Update Screen Status
        [HttpPost]
        public DataContainer<bool> UpdateScreeningStatus(string airbillNo)
        {
            return MakeTransactional((() => _manager.UpdateScreeningStatus(airbillNo)));
        }
        #endregion
        #region Add delay reason
        /*
         * Add delay reason
         */

        [HttpGet]
        public DataContainer<RecoveryIssueReason[]> GetReasons()
        {
            return MakeTransactional((() => _manager.GetReasons()));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> AddDelayReason(string reasonFor, string shipments, string reason, string remark, string carrier, string mawb)
        {
            var shipmentList = shipments.Trim().Split(',');
            var strRemark = reason;
            if (!string.IsNullOrEmpty(remark.Trim()))
            {
                strRemark += (" " + reason);
            }

            foreach (var shipment in shipmentList)
            {
                if (reasonFor.ToUpper().Equals("HAWB"))
                {
                    _manager.SaveDelayReasonsHawb(shipment, "JFK", ApplicationUser.UserName, strRemark.Trim());
                }
                else if (reasonFor.ToUpper().Equals("MAWB"))
                {
                    _manager.SaveDelayReasonsMawb(mawb, carrier, "JFK", ApplicationUser.UserName, strRemark.Trim());
                }
            }
            return MakeTransactional((() => true));
        }
        #endregion

        #region Cancel screening
        /*
         * Cancel screening
         */
        [Authorize]
        [HttpPost]
        public DataContainer<bool> CancelScreening(string airbillNo)
        {
            return MakeTransactional((() => _manager.CancelScreeningByAirbillNo(airbillNo, ApplicationUser.UserName)));
        }
        #endregion

        #region Void Selected Shipment(s)

        /// <summary>
        /// Void selected shipment(s)
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> VoidSelectedShipments(string shipment)
        {
            if (string.IsNullOrEmpty(shipment))
            {
                return MakeTransactional((() => false));
            }

            var lstShipment = shipment.Split(',');
            foreach (var code in lstShipment)
            {
                var completed = _manager.VoidSelectedShipmnts(code, "HAWB", ApplicationUser.UserName);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return MakeTransactional((() => true));
        }

        #endregion

        #region Make Shipment Known/Unknown
        /// <summary>
        /// Make Shipment Knowm/ Unknown
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> ShipmentStatus(string airbillNo, string origin, string gateway)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin) ||
                string.IsNullOrEmpty(gateway))
            {
                return MakeTransactional((() => false));
            }

            var lstAirbillNo = airbillNo.Split(',');
            var lstOrigin = origin.Split(',');


            if (lstAirbillNo.Length != lstOrigin.Length)
            {
                return MakeTransactional((() => false));
            }

            for (int i = 0; i < lstAirbillNo.Length; i++)
            {
                var completed = _manager.ShipmentStatus(lstAirbillNo[i], lstOrigin[i], ApplicationUser.UserName, gateway);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return MakeTransactional((() => true));

        }
        #endregion

        #region Make Shipment Hazmat/Non-Hazmat
        /// <summary>
        /// Make Shipment Hazmat/Non-Hazmat
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> HawbToggleHazmat(string hawb, string origin)
        {
            if (string.IsNullOrEmpty(hawb) || string.IsNullOrEmpty(origin))
            {
                return MakeTransactional((() => false));
            }

            var lstHawb = hawb.Split(',');
            var lstOrigin = origin.Split(',');

            if (lstHawb.Length != lstOrigin.Length)
            {
                return MakeTransactional((() => false));
            }

            for (int i = 0; i < lstHawb.Length; i++)
            {
                var completed = _manager.HawbToggleHazmat(lstHawb[i], lstOrigin[i], ApplicationUser.UserName);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return MakeTransactional((() => true));
        }
        #endregion

        #region Make Shipment Dryice/Non-Dryice
        /// <summary>
        /// Make Shipment Dryice/Non-Dryice
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> MakeShipmentDryice(string hawb, string origin)
        {
            if (string.IsNullOrEmpty(hawb) || string.IsNullOrEmpty(origin))
            {
                return MakeTransactional((() => false));
            }

            var lstHawb = hawb.Split(',');
            var lstOrigin = origin.Split(',');
            if (lstHawb.Length != lstOrigin.Length)
            {
                return new DataContainer<bool>() { Data = false };
            }
            for (int i = 0; i < lstHawb.Length; i++)
            {
                var completed = _manager.MakeShipmentDryice(lstHawb[i], lstOrigin[i], ApplicationUser.UserName);
                if (!completed) return new DataContainer<bool>() { Data = false };
            }
            return MakeTransactional((() => true));
        }

        #endregion

        #region Override /Apply Dims Restrictions
        /// <summary>
        /// Override/Apply Dims restrictions
        /// </summary>
        /// <param name="hawb"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> DimsRestructions(string hawb)
        {
            if (string.IsNullOrEmpty(hawb))
            {
                return MakeTransactional((() => false));
            }
            var lstHawb = hawb.Split(',');
            for (int i = 0; i < lstHawb.Length; i++)
            {
                var completed = _manager.DimsRestrictions(lstHawb[i], ApplicationUser.UserName);
                if (!completed) return new DataContainer<bool>() { Data = false };

            }
            return MakeTransactional((() => true));
        }
        #endregion

        #region Unvoid Housebills
        /// <summary>
        /// Unvoid Housebills
        /// </summary>
        /// <param name="hawb"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<string> UnvoidHousebills(string hawb, string gateway)
        {
            if (string.IsNullOrEmpty(hawb) || string.IsNullOrEmpty(gateway))
            {
                return MakeTransactional((() => string.Empty));
            }

            var lstHawb = hawb.Trim();

            var completed = _manager.UnvoidHousebills(lstHawb, gateway, ApplicationUser.UserName);
            completed = string.Format("{0} {1}", lstHawb, completed);


            return MakeTransactional((() => completed));

        }
        #endregion

        #region Override AMS Hold
        /// <summary>
        /// Override AMS Hold
        /// </summary>
        /// <param name="hawb"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public DataContainer<bool> OverrideAsmHold(string hawb)
        {
            if (string.IsNullOrEmpty(hawb))
            {
                return MakeTransactional((() => false));
            }

            var lstHawb = hawb.Split(',');
            for (var i = 0; i < lstHawb.Length; i++)
            {
                var completed = _manager.OverrideAmsHold(lstHawb[i], ApplicationUser.UserName);
                if (!completed) return MakeTransactional((() => false));
            }
            return MakeTransactional((() => true));
        }
        #endregion

        #region Assign Selected Shipments
        /// <summary>
        /// Assign Selected Shipments to
        /// </summary>
        /// <param name="airbillNo"></param>
        /// <param name="origin"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpPost]
        public DataContainer<bool> AssignSelectedShipment(string airbillNo, string origin, long planId)
        {
            if (string.IsNullOrEmpty(airbillNo) || string.IsNullOrEmpty(origin))
            {
                return MakeTransactional((() => false));
            }

            var lstAirbillNo = airbillNo.Split(',');
            var lstOrigin = origin.Split(',');

            for (var i = 0; i < lstAirbillNo.Length; i++)
            {
                var completed = _manager.AssignSelectedShipment(lstAirbillNo[i], lstOrigin[i], planId);
                if (!completed) return MakeTransactional((() => false));
            }

            return MakeTransactional((() => true));
        }
        #endregion

        #region Appearance Settings
        /// <summary>
        /// Appearance Settings
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public DataContainer<List<ShipmentTable>> AppearanceSettingList()
        {
            var data = _manager.AppearanceSettingList(ApplicationUser.UserName);
            for (int i = 0; i < data[1].Rows.Count; i++) // selected
            {
                for (int j = 0; j < data[0].Rows.Count; j++) //avaliable
                {
                    if (data[1].Rows[i]["FieldName"].ToString() == data[0].Rows[j]["FieldName"].ToString())
                    {
                        data[0].Rows.RemoveAt(j);
                    }
                }
            }
            return MakeTransactional((() => data));
        }

        /// <summary>
        /// Appearance settings: View weight settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public DataContainer<DhtmlxGrid<GridUserData>> ViewWeightGrigSettings()
        {
            return MakeTransactional((() => _manager.ViewWeightGrigSettings()));
        }

        [HttpPost]
        public DataContainer<bool> UpdateColorSettings(string nodeColor, string allowSelect, string nodeForeColor, int id)
        {
            return MakeTransactional((() => _manager.UpdateGridNodes(nodeColor, allowSelect, nodeForeColor, id)));
        }

        [HttpPost]
        public DataContainer<bool> UpdateGridOrder(string curOrder, int curId, string order, int id)
        {
            return MakeTransactional((() => _manager.UpdateGridOrder(curOrder, curId, order, id)));
        }
        [Authorize]
        [HttpPost]
        public DataContainer<bool> UpdateWeightSettings(string fieldNames)
        {
            _manager.RemoveUserSettings(ApplicationUser.UserName);

            var fields = fieldNames.Split('$');

            foreach (var field in fields)
            {
                _manager.SaveColSettings(ApplicationUser.UserName, field);
            }
            return MakeTransactional((() => true));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> ResetUserSettings()
        {
            return MakeTransactional((() => _manager.RemoveUserSettings(ApplicationUser.UserName)));
        }

        #endregion

        #region Remove Shipment (s)
        [HttpPost]
        public DataContainer<bool> RemoveShipment(string airbillNo, string masterbillNo)
        {
            return MakeTransactional(() => _manager.RemoveShipment(airbillNo, masterbillNo));
        }
        #endregion

        #region Get All Printers
        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetAllPrinters()
        {
            return MakeTransactional(() => _manager.GetAllPrinters());
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> PrintDocument(string loadingPlanId, string printer)
        {
            return MakeTransactional(() => _manager.InsertPrintServerReport(ApplicationUser.UserName, loadingPlanId, "JFK", printer));
        }

        #endregion

        #region Import Shipments
        /*
         * by destination
         */
        [Authorize]
        [HttpGet]
        public DataContainer<Shimpent<GridImportUserData>> GetImportListByHousebill(string destination)
        {
            return MakeTransactional(() => _manager.ImportShipmentsByDestination("JFK", destination, string.Empty));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<string> ImportShipmentsbyDestination(string airbillNo, string masterbillNo, string typeConsol)
        {
            return MakeTransactional((() => _manager.ImportShipmentConsolbyDestination(airbillNo, masterbillNo, ApplicationUser.UserName, typeConsol)));
        }

        /*
         * by housebill
         */
        [Authorize]
        [HttpPost]
        public DataContainer<string> ImportShipmentsbyHousebill(string airbillNo, string masterbillNo, string typeConsol)
        {
            return MakeTransactional((() => _manager.ImportShipmentConsolbyHousebill(airbillNo, masterbillNo, ApplicationUser.UserName, typeConsol)));
        }
        #endregion



        [HttpGet]
        public DataContainer<List<Dictionary<string, object>>> GetDomesticConsolData(string consol)
        {
            return MakeTransactional((() => _manager.GetDomesticConsolData(consol)));
        }

        #region Create Empty masterhouse
        [Authorize]
        [HttpPost]
        public DataContainer<string> CreateNewMasterHouse(string loadingPlan, string origin, string masterbill,
             string destination, string accountName, string assemblyMode)
        {
            return MakeTransactional((() =>
                _manager.CreateNewMasterHouse(loadingPlan, origin, masterbill, destination,
                    accountName, "JFK", ApplicationUser.UserName, assemblyMode)));
        }
        #endregion
    }
}
