﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Net.Http;

namespace RapiscanBLL
{
    public class ScreeningProcessor : IDisposable
    {
        public ScreeningProcessor(string connectionString, string imageSaveFolder, string apiURL, string pieceScanURL)
        {
            _connectionString = connectionString;
            _imageSaveFolder = imageSaveFolder;
            _apiURL = apiURL;
            _pieceScanURL = pieceScanURL;
        }

        public void ProcessRapiscanScreeningData()
        {
            const string screeningInsertQuery = @"INSERT INTO Fsp.Screening (RecDate, DeviceId,ResultCode,UserName,SampleNumber,SampleMethod,ScreeningDate,NumberOfPieces,HousebillNumber,IsProcessed,Remark,ScreeningMethodId,UserId,LocationId) VALUES (getdate(),@DeviceId,@ResultCode,@UserName,@SampleNumber,@SampleMethod, dbo.GetLocalDateTimeByLocationID((Select Top 1 LocationId from fsp.Devices where RecId = @DeviceId) , @ScreeningDate), @NoOfPieces,@HousebillNumber,@IsProcessed,@Remark, (select top 1 ScreeningMethodId from fsp.Devices D inner join CertifiedDevices CD on D.CertifiedDeviceId = CD.RecId where D.RecId = @DeviceId), @UserId, (Select Top 1 LocationId from fsp.Devices where RecId = @DeviceId)); select cast(Scope_Identity() as bigint);";
            const string imgQuery = @"Select * from TransactionsUploadedImages where ReferenceId = @ReferenceId and ScanId = @ScanId";
            const string imgRectangleQuery = @"select R.X1, R.Y1, R.X2, R.Y2 from TransactionsScansRectangles R Inner Join TransactionsScans S ON R.TransactionScanId = S.TransactionScanId Where S.ReferenceId = @ReferenceId and R.ScanId = @ScanId and ImageIndex = @ImageIndex";
            const string connectionNameQuery = @"select WebConnectionName from HostPlus_Locations WHERE RecId = (Select Top 1 LocationId from fsp.Devices where RecId = @DeviceId)";

            List<Transaction> transactions = GetNotProcessedTransactions();

            string errors = "";

            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Connection.Open();
                    foreach (var screenTrans in transactions)
                    {
                        try
                        {
                            //1. Add Db records.
                            command.CommandText = screeningInsertQuery;
                            command.CommandType = CommandType.Text;
                            command.Parameters.Clear();

                            command.Parameters.Add(new SqlParameter("@DeviceId", screenTrans.MachineId));
                            command.Parameters.Add(new SqlParameter("@ResultCode", screenTrans.Clear ? "PASS" : "ALARM"));
                            command.Parameters.Add(new SqlParameter("@UserName", screenTrans.OperatorId));
                            command.Parameters.Add(new SqlParameter("@SampleNumber", screenTrans.ReferenceId));
                            command.Parameters.Add(new SqlParameter("@ScreeningDate", screenTrans.ScanDate));
                            command.Parameters.Add(new SqlParameter("@NoOfPieces", screenTrans.Slac));
                            command.Parameters.Add(new SqlParameter("@SampleMethod", "XRAY"));
                            command.Parameters.Add(new SqlParameter("@NumberOfPieces", 1));
                            command.Parameters.Add(new SqlParameter("@HousebillNumber", screenTrans.ReferenceName));
                            command.Parameters.Add(new SqlParameter("@IsProcessed", false));
                            command.Parameters.Add(new SqlParameter("@Remark", screenTrans.ScanComments));
                            command.Parameters.Add(new SqlParameter("@UserId", screenTrans.UserId ?? (object)DBNull.Value));

                            var screeningId = (long)command.ExecuteScalar();

                            command.CommandText = imgQuery;
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@ReferenceId", screenTrans.ReferenceId));
                            command.Parameters.Add(new SqlParameter("@ScanId", screenTrans.ScanId));

                            var reader = command.ExecuteReader();

                            //2. Process images and save to Db.
                            var imgs = new Dictionary<int, Image>();

                            while (reader.Read())
                            {
                                byte[] image = (byte[])reader["Bytes"];
                                MemoryStream memstr = new MemoryStream(image);

                                //Leave here for testing. Save received image to see if it was received normally.
                                //FileStream fs = new FileStream(@"C:\Users\maria.topchian\Desktop\aa.png",
                                //    FileMode.OpenOrCreate);
                                //fs.Write(image, 0, image.Length);
                                //fs.Close();


                                memstr.Position = 0;
                                Image img = Image.FromStream(memstr, true, true);

                                var index = (int)reader["ImageIndex"];
                                if (!imgs.ContainsKey(index))
                                    imgs.Add((int)reader["ImageIndex"], img);
                                else
                                    imgs[index] = img;
                            }
                            reader.Close();

                            foreach (int k in imgs.Keys)
                            {
                                command.CommandText = imgRectangleQuery;
                                command.Parameters.Clear();
                                command.Parameters.Add(new SqlParameter("@ReferenceId", screenTrans.ReferenceId));
                                command.Parameters.Add(new SqlParameter("@ScanId", screenTrans.ScanId));
                                command.Parameters.Add(new SqlParameter("@ImageIndex", k));

                                reader = command.ExecuteReader();
                                List<Rectangle> rectangles = new List<Rectangle>();
                                while (reader.Read())
                                {
                                    rectangles.Add(new Rectangle()
                                    {
                                        TopLeftX = (int)reader["X1"],
                                        TopLeftY = (int)reader["Y1"],
                                        BottomRightX = (int)reader["X2"],
                                        BottomRightY = (int)reader["Y2"]
                                    });
                                }
                                reader.Close();

                                string filePath = ImageEditor.ProcessAndSaveImage(imgs[k], screenTrans.ReferenceId + "_" + k.ToString(),
                                    _imageSaveFolder, rectangles);


                                command.CommandText = @"Insert into Fsp.ScreeningImages(RecDate,ScreeningId,ImagePath) values (getdate(),@screeningId,@imagePath)";
                                command.Parameters.Clear();
                                command.Parameters.Add(new SqlParameter("@screeningId", screeningId));
                                command.Parameters.Add(new SqlParameter("@imagePath", filePath));
                                command.ExecuteNonQuery();
                            }

                            //4. Delete ScreeningQueue record.
                            command.CommandText = @"delete from Fsp.ScreeningQueue where Reference = @Reference and UserId = @UserId and DeviceId = @DeviceId";
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@DeviceId", screenTrans.MachineId));
                            command.Parameters.Add(new SqlParameter("@UserId", screenTrans.UserId ?? (object)DBNull.Value));
                            command.Parameters.Add(new SqlParameter("@Reference", screenTrans.ReferenceName));
                            command.ExecuteNonQuery();

                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri(_apiURL);

                            try
                            {
                                var uri = string.Format(@"LiveScreeningHubHelper/CompleteScreening?deviceId={0}&shipment={1}",
                                    screenTrans.MachineId, screenTrans.ReferenceName);
                                var response = client.GetAsync(uri).Result;
                            }
                            catch (Exception ex)
                            {

                            }


                            // Get connection name from DB for web service call.
                            command.CommandText = connectionNameQuery;
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@DeviceId", screenTrans.MachineId));
                            var connectionName = command.ExecuteScalar();


                            if (connectionName != null)
                            {
                                WSPieceScan.WSPieceScan psClient = new WSPieceScan.WSPieceScan { Url = _pieceScanURL };
                                psClient.ProcessFSP(connectionName.ToString(), screenTrans.ReferenceName);
                            }

                            //3. Update status on Fsp.Queue.

                            command.CommandText = "UpdateFspQueueStatus";
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@housebillNo", screenTrans.ReferenceName));
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            errors += Environment.NewLine + ex.Message;
                        }

                    }
                }
            }

            if (!string.IsNullOrEmpty(errors))
                throw new ApplicationException(errors);
        }
        private List<Transaction> GetNotProcessedTransactions()
        {
            List<Transaction> retval = new List<Transaction>();
            const string refQuery =
                @"select distinct T.*,TS.TransactionScanId, TS.Clear,TS.Comments, TS.CreatedDate as ScanDate, Fsp.Screening.RecId
                from Transactions T inner join TransactionsScans TS on T.ReferenceId = TS.ReferenceId 
                inner join TransactionsUploadedImages TUI on TUI.ReferenceId = T.ReferenceId
                left join Fsp.Screening on SampleNumber = T.ReferenceId
                where RecId is null";
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand(refQuery, connection))
                    {
                        command.Connection.Open();
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            retval.Add(
                                        new Transaction()
                                        {
                                            ReferenceId = reader["ReferenceId"].ToString(),
                                            MachineId = (int)reader["MachineId"],
                                            AuthorizationToken = reader["AuthorizationToken"].ToString(),
                                            ScanId = reader["ScanId"].ToString(),
                                            OperatorId = reader["OperatorId"].ToString(),
                                            UserId = reader["UserId"] == DBNull.Value ? null : (long?)reader["UserId"],
                                            ReferenceName = reader["ReferenceName"].ToString(),
                                            CreatedDate = (DateTime)reader["CreatedDate"],
                                            Clear = (bool)reader["Clear"],
                                            ScanComments = reader["Comments"].ToString(),
                                            ScanDate = (DateTime)reader["ScanDate"],
                                            Slac = reader["Slac"] == DBNull.Value ? 0 : (int)reader["Slac"]
                                        }
                                        );
                        }
                        command.Connection.Close();
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed getting Rapiscan records. " + ex.Message);
            }
        }

        private readonly string _connectionString;

        private readonly string _imageSaveFolder;

        private readonly string _apiURL;

        private readonly string _pieceScanURL;

        #region IDisposable Implementation
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.

                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                _disposed = true;

            }
        }

        ~ScreeningProcessor()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion
    }

    internal class Transaction
    {
        public string ReferenceId { get; set; }
        public int MachineId { get; set; }
        public string AuthorizationToken { get; set; }
        public string ScanId { get; set; }
        public string OperatorId { get; set; }
        public long? UserId { get; set; }
        public string ReferenceName { get; set; }
        public DateTime Time { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Clear { get; set; }
        public string ScanComments { get; set; }
        public DateTime ScanDate { get; set; }
        public int Slac { get; set; }

    }

}
