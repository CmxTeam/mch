﻿using System.Web;
using System.Web.Optimization;

namespace CSN_
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/signalr").Include(                       
                        "~/Scripts/jquery.signalR-2.2.0.js",
                        "~/Scripts/CSNScripts/signalingadapter.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageCapturer")); // empty for now

            bundles.Add(new ScriptBundle("~/bundles/dhtmlx").Include(
                        "~/Scripts/dhtmlx.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/helpers.js",
                        "~/Scripts/CSNScripts/infobubble.js",
                        "~/Scripts/CSNScripts/dashboard.js",
                        "~/Scripts/CSNScripts/map.js",
                        "~/Scripts/CSNScripts/screeningTransactions.js",
                        "~/Scripts/CSNScripts/screeningDevices.js",
                        "~/Scripts/CSNScripts/screenerModule.js",
                        "~/Scripts/CSNScripts/facilitieModule.js",
                        "~/Scripts/CSNScripts/submitForm.js",
                        "~/Scripts/CSNScripts/html2canvas.js",
                        "~/Scripts/CSNScripts/shipmentDetailsWindow.js",
                        "~/Scripts/CSNScripts/transactionDetailsWindow.js",
                        "~/Scripts/CSNScripts/enterToLiveScreening.js",
                        "~/Scripts/CSNScripts/liveScreening.js",
                        "~/Scripts/CSNScripts/liveScreeningCreateHawb.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/dhtmlx.css",
                      "~/Content/style.css",
                      "~/Content/common.css"));
        }
    }
}
