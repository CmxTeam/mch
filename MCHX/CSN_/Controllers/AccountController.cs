﻿using System.Web.Mvc;

namespace CSN_.Controllers
{
    [Authorize]    
    public class AccountController : SharedViews.Controllers.LoginBaseController
    {
        public override bool UseShell
        {
            get { return false; }
        }        
    }
}