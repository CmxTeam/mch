﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using SharedViews.Attributes;
using SharedViews.Controllers;

namespace CSN_.Controllers
{
    public class HomeController : LayoutBaseController
    {
        [Authorize]
        [LayoutTypeAttribute("2U")]
        [ApplicationIdAttribute("3")]
        public ActionResult Index()
        {
            var authCookie = string.Format("{0}_{1}:{2}", "selectedRole", HttpContext.Request.Url.Host, HttpContext.Request.Url.Port);
            if (Request.Cookies[authCookie] == null)
            {
                var roles = GetDefaultRole(Session["ApiAuthentikationToken"].ToString());
                var defaultRoleId = JArray.Parse(roles);
                if (defaultRoleId.Count > 0)
                {
                    var selectedRole = new HttpCookie(string.Format("selectedRole_{0}:{1}", HttpContext.Request.Url.Host, HttpContext.Request.Url.Port), defaultRoleId[0].SelectToken("Id").ToString())
                    {
                        Expires = DateTime.Now.AddDays(1)
                    };
                    Response.Cookies.Add(selectedRole);
                }                
            }
            return View();
        }

        private static string GetDefaultRole(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var response = client.GetAsync(ConfigurationManager.AppSettings["WebApiUrl"] + "/Common/GetRoles?applicationId=3").Result;
            var receiveStream = response.Content.ReadAsStringAsync().Result;            
            return receiveStream;
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}