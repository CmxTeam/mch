﻿var enterToLiveScreening = function () {
    var $this = this;
    this.onSubmit = null;

    this._refreshCombo = function (comboName, loadUrl) {
        var combo = this.form.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        combo.loadExternal(loadUrl, 'Id', 'Name');
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('enterToLiveScreeningWindow', 0, 0, 400, 210);

        var formStructure = [
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "screeningLocationCombo", label: "Screening Location:", filtering: true, labelWidth: 140, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return $this.form.getCombo("screeningLocationCombo").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "screeningDeviceIdCombo", label: "Screening Device ID:", filtering: true, labelWidth: 140, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return $this.form.getCombo("screeningDeviceIdCombo").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "operatorTypeCombo", label: "Operator Type:", filtering: true, labelWidth: 140, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return $this.form.getCombo("operatorTypeCombo").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    { type: "button", name: "submitButton", value: "SUBMIT", offsetLeft: 263, offsetTop: 25 }
                ]
            }
        ];

        var form = window.attachForm(formStructure);
        var screeningLocationCombo = form.getCombo('screeningLocationCombo');
        screeningLocationCombo.loadExternal('/LiveScreeningWebSpecific/GetScreeningLocations', 'value', 'text', function(data) {
            if (data.length === 1) {
                screeningLocationCombo.selectOption(0);
            }
        });

        screeningLocationCombo.attachEvent("onChange", function (value) {
            if (value)
                this._refreshCombo('screeningDeviceIdCombo', '/LiveScreening/GetAvailableXRAYDevices?locationId=' + value);
        }.bind(this));

        form.getCombo('operatorTypeCombo').load([{ value: "1", text: "Operator" }]);

        form.attachEvent("onButtonClick", function () {
            if (form.validate()) {                
                var lc = form.getCombo('screeningLocationCombo');
                var dc = form.getCombo('screeningDeviceIdCombo');
                var oc = form.getCombo('operatorTypeCombo');
                if (lc.getSelectedValue() && dc.getSelectedValue() && oc.getSelectedValue()) {
                    window.progressOn();
                    var data = dc.getOption(dc.getSelectedValue()).customData.item;
                    if (data.InUse) {
                        dhtmlx.confirm({
                            type: "confirm",
                            text: 'Selected device is in use. Do you want to disconnect user ' + data.UserHolder + '?',
                            callback: function (result) {
                                if (result) {
                                    helpers.getDataFromExternalResource('/LiveScreening/DisconnectUserFromDevice', { deviceId: dc.getSelectedValue() }, function () {
                                        if (this.onSubmit) {
                                            this.onSubmit({
                                                location: { id: lc.getSelectedValue(), text: lc.getSelectedText() },
                                                device: { id: dc.getSelectedValue(), text: dc.getSelectedText() },
                                                operatorType: { id: oc.getSelectedValue(), text: oc.getSelectedText() }
                                            });
                                        }                                        
                                        helpers.getDataFromExternalResource('/LiveScreeningHubHelper/NotifyUserAboutDisconnectedDevice', { deviceId: dc.getSelectedValue(), userName: helpers.getUserName() });
                                        this.window.close();
                                    }.bind(this));
                                } else {
                                    window.progressOff();
                                }
                            }.bind(this)
                        });
                    } else {
                        if (this.onSubmit) {
                            this.onSubmit({
                                location: { id: lc.getSelectedValue(), text: lc.getSelectedText() },
                                device: { id: dc.getSelectedValue(), text: dc.getSelectedText() },
                                operatorType: { id: oc.getSelectedValue(), text: oc.getSelectedText() }
                            });
                        }
                        this.window.close();
                    }
                }
            }
        }.bind(this));

        window.setText('Welcome to Live Screening');
        window.setModal(1);
        window.denyResize();
        window.centerOnScreen();
        this.form = form;
        this.window = window;
    }.apply(this);
};