﻿var dashboardModule = function (cell, accordion) {
    var $this = this;
    var mainLayout, leftLayout;
    var screeningVolumesChart, hourlyChart, rescreenedVolumesChart,
        technologiesChart, screeningResultsChart, chainOfFactoryChart;
    var dataViewCharts;
    var screeningResultsChartCurrentLocation = "";
    var dynamicToolbar = null;

    var showLayoutView = function(viewName, headerName, backCallback, info) {
        leftLayout.showView(viewName);
        leftLayout.setCollapsedText(headerName);

        leftLayout.setText("<div class='customToolbarInLayout' ><div id='" + leftLayout.getViewName() + "'></div></div>");
        if (dynamicToolbar != null) {
            dynamicToolbar.unload();
            dynamicToolbar = null;
        }

        dynamicToolbar = new dhtmlXToolbarObject({
            parent: leftLayout.getViewName(),
            xml:
                    '<toolbar>' +
                    '<item type="text" id="button_text_1" text="' + headerName + '" />' +
                    (headerName != '' ? '<item type="separator" id="button_separator_1" />' : '') +
                    '<item type="buttonSelect" openAll="true" id="btnDownload" text="DOWNLOAD" title="" >' +
                    '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
                    '</item>' +
                    (backCallback ? '<item type="separator" id="button_separator_2" />' +
                                    '<item type="button" id="btnBack" text="BACK" image="" />' : '') +
                    (info ? '<item type="text"  id="button_text_2" text="' + info + '" />' : '') +
                    '</toolbar>'
        });
        if (backCallback)
            dynamicToolbar.addSpacer("btnBack");

        leftLayout.hideArrow();

        dynamicToolbar.attachEvent("onClick", function (id) {
            if (id == 'btnExportExcel') {
                var filter = { 
                    dateFrom: dashboardAccordionForm.getCalendar('fromDateCalendar').getFormatedDate(), 
                    dateTo: dashboardAccordionForm.getCalendar('toDateCalendar').getFormatedDate(),
                    location: dashboardAccordionForm.getCombo('screeningLocations').getAllCheckedIds().join()
                }

                leftLayout.progressOn();
                var url = "/LiveScreening/";

                switch (leftLayout.getViewName()) {
                    case "ChainOfFactory":
                        url += "ExportShipmentSeals";
                        break;
                    case "HourlyProduction":
                        url += "ExportHourlyProductionChart";
                        break;
                    case "RescreenedVolumes":
                        url += "ExportRescreenedVolumesChart";
                        break;
                    case "ScreeningResults":
                        url += "ExportScreeningResultsChart";
                        break;
                    case "ScreeningTechnologies":
                        url += "ExportScreeningTechUtilChart";
                        break;
                    case "ScreeningVolumes":
                        url += "ExportScreeningVolumesChart";
                        break;
                    case "ScreeningResultsHourly":
                        filter.location = screeningResultsChartCurrentLocation;
                        url += "ExportTimeResultByLocation";
                        break;
                    default:
                        break;
                }

                helpers.exportToExcel(url, filter, leftLayout.getCollapsedText(), function () { leftLayout.progressOff() });
        
            } else if (backCallback) {
                backCallback();
            }
        });
    }

    this.ChartsEnum = {
        ScreeningVolumes: '1',
        HourlyProduction: '2',
        RescreenedVolumes: '3',
        ScreeningTechnologies: '4',
        ScreeningResults: '5',
        ChainOfFactory: '6',
        ScreeningResultsHourly:'7'
    }


    this.config = {
        cell: cell,
        accordion: accordion
    };

    this.show = function () {
        cell.showView('dashboardContainer');
    }

    this.filter = function (id) {
        this._filter(
                dashboardAccordionForm.getCombo('screeningLocations').getAllCheckedIds().join(),
                dashboardAccordionForm.getCalendar('fromDateCalendar').getFormatedDate(),
                dashboardAccordionForm.getCalendar('toDateCalendar').getFormatedDate(),
                id);
    }

    this.resetControls = function () {
        var screeningLocations = dashboardAccordionForm.getCombo('screeningLocations');
        screeningLocations.checkUncheckAll(false);
        screeningLocations.unSelectOption();

        dashboardAccordionForm.getCombo('period').selectOption(0);
        this._setCalendarDate('fromDateCalendar', dashboardAccordionForm, new Date());
        this._setCalendarDate('toDateCalendar', dashboardAccordionForm, new Date());
    }


    this._setCalendarDate = function (name, form, date) {
        var calendar = form.getCalendar(name);
        var calendarToKey = Object.keys(calendar.i)[0];
        calendar.i[calendarToKey].input.value = window.dhx4.date2str(date, '%m-%d-%Y');
        calendar.setDate(date);
    }

    var dynamicChartSeriesGenerate = function(chart, method, filter, xAxisKey, isWithLabel) {
        var legendValues = [];
        var colorStep = 0.5;

        if (chart.dataCount() === 0) {
            leftLayout.progressOn();

            chart.loadExternal('/LiveScreening/' + method, filter, function(data) {
                    if (chart.dataCount() === 0) {
                        chart.define("legend", null);
                        if (data && data.Data && data.Data.length > 0) {
                            chart._series = [];

                            $.each(data.Data[0], function(key, value) {
                                if (key !== xAxisKey) {
                                    chart.addSeries({
                                        alpha: 0.8,
                                        value: '#' + key.replace(/ /g, '') + '#',
                                        label: isWithLabel ? '#' + key.replace(/ /g, '') + '#':"",
                                        color: "#" + Math.floor(colorStep * 16777215).toString(16)
                                    });
                                    legendValues.push({ text: key, color: "#" + Math.floor(colorStep * 16777215).toString(16) });
                                    colorStep = colorStep + 0.02;
                                }
                            });
                            if (legendValues.length > 0) {
                                chart.define("legend", {
                                    values: legendValues,
                                    valign: "middle",
                                    align: "right",
                                    width: 90,
                                    layout: "x"
                                });
                            }


                        }
                    }
                },
                function() {
                    leftLayout.progressOff();
                });
        } else {
            chart.showAllSeries();
        }
    }

    this._filter = function (location, dateFrom, dateTo, id) {

        var filter = { dateFrom: dateFrom, dateTo: dateTo, location: location }

        if (id == undefined) {
            id = dataViewCharts.getSelected();

            if (screeningVolumesChart != undefined) {
                screeningVolumesChart.clearAll();
            }
            if (hourlyChart != undefined) {
                hourlyChart.clearAll();
            }
            if (rescreenedVolumesChart != undefined) {
                rescreenedVolumesChart.clearAll();
            }
            if (technologiesChart != undefined) {
                technologiesChart.clearAll();
            }
            if (screeningResultsChart != undefined) {
                screeningResultsChart.clearAll();
            }
            if (chainOfFactoryChart != undefined) {
                chainOfFactoryChart.clearAll();
            }
        }

        switch (id) {
        case this.ChartsEnum.HourlyProduction:
            showLayoutView("HourlyProduction", "HOURLY PRODUCTION");

            if (hourlyChart == undefined) {
                hourlyChart = leftLayout.attachChart({
                    view: 'area',
                    xAxis: { title: "TIME", template:
                        function (obj) {
                            var lineWidth = 16;
                            if (obj.TimeUnit == "XMID") {
                                lineWidth = 12;
                            }

                            return obj.TimeUnit != '' ? '<div style="display: flex;flex-direction: column;">' +
                                '<svg height="16" width="6" style="margin-top: -5px;margin-left: auto;margin-right: auto;">' +
                                '<line x1="0" y1="0" x2="0" y2="' + lineWidth + '" style="stroke:rgb(0,0,0);stroke-width:2.24" /></svg>' +
                                '<span>' + (obj.TimeUnit == "XMID" ? "" : obj.TimeUnit) + '</span></div>' : "";

                        }, lines: false },
                    yAxis: { title: "VOLUME" }
                });

                hourlyChart.attachEvent("onAfterRender", function () {
                    var line = $(hourlyChart.$view).find(".dhx_canvas_text.dhx_axis_item_x")[0];
                    if (line) {
                        $(line).find("svg").css("margin-left", "");
                        $(line).find("svg").css("margin-right", "");
                    }
                });
            }

            dynamicChartSeriesGenerate(hourlyChart, 'GetHourlyProductionChart', filter, "TimeUnit");
            
            break;
        case this.ChartsEnum.RescreenedVolumes:
            showLayoutView("RescreenedVolumes", "RESCREENED VOLUMES");

            if (rescreenedVolumesChart == undefined) {
                rescreenedVolumesChart = leftLayout.attachChart({
                    view: 'bar',
                    label: '#yValue#',
                    gradient: false,
                    xAxis: { "title": "CCSFs", "template": "#xValue#", "step": "500" },
                    yAxis: { "title": "VOLUME", "start": "0" },
                    value: '#yValue#'
                });
            }

            if (rescreenedVolumesChart.dataCount() === 0) {
                leftLayout.progressOn();
                rescreenedVolumesChart.loadExternal('/LiveScreening/GetRescreenedVolumesChart', filter, null, function() { leftLayout.progressOff(); });
            }
            break;
        case this.ChartsEnum.ScreeningTechnologies:
            showLayoutView("ScreeningTechnologies", "SCREENING TECHNOLOGIES UTILIZATION");

            if (technologiesChart == undefined) {
                technologiesChart = leftLayout.attachChart({
                    view: 'bar',
                    gradient: false,
                    xAxis: { title: "LOCATION", template: "#Location#" },
                    yAxis: { title: "VOLUME" }
                });
            }

            dynamicChartSeriesGenerate(technologiesChart, 'GetScreeningTechUtilChart', filter, "Location", true);
           
            break;
        case this.ChartsEnum.ScreeningResults:
            showLayoutView("ScreeningResults", "SCREENING RESULTS");

            if (screeningResultsChart == undefined) {
                screeningResultsChart = leftLayout.attachChart({
                    view: 'bar',
                    gradient: false,
                    xAxis: { title: "LOCATION", template: "#Location#" },
                    yAxis: { title: "VOLUME" }
                });

                screeningResultsChart.attachEvent("onItemdblclick", function(id, ev, trg) {
                    showLayoutView("ScreeningResultsHourly", "SCREENING RESULTS", function () {
                        screeningResultsChartCurrentLocation = "";
                        leftLayout.unloadView("ScreeningResultsHourly");
                        showLayoutView('ScreeningResults', "SCREENING RESULTS");
                    }, screeningResultsChart.get(id).Location);

                    var screeningResultsHourly = leftLayout.attachChart({
                        view: 'area',
                        xAxis: {
                            title: "TIME", template: function (obj) {
                                if (obj.TimeUnit != "XMID") {
                                    return obj.TimeUnit;
                                }
                                return "";
                            }, lines: false
                        },
                        yAxis: { title: "VOLUME" }
                    });
                    var filter = {
                        dateFrom: dashboardAccordionForm.getCalendar('fromDateCalendar').getFormatedDate(),
                        dateTo: dashboardAccordionForm.getCalendar('toDateCalendar').getFormatedDate(),
                        location: screeningResultsChart.get(id).Location
                    }
                    screeningResultsChartCurrentLocation = screeningResultsChart.get(id).Location;
                    dynamicChartSeriesGenerate(screeningResultsHourly, 'GetTimeResultByLocation', filter, "TimeUnit");
                    
                });

            }
            dynamicChartSeriesGenerate(screeningResultsChart, 'GetScreeningResultsChart', filter, "Location", true);
           
            break;
        case this.ChartsEnum.ChainOfFactory:
            showLayoutView("ChainOfFactory", "CHAIN OF CUSTODY");

            if (chainOfFactoryChart == undefined) {
                chainOfFactoryChart = leftLayout.attachChart({
                    view: 'bar',
                    gradient: false,
                    xAxis: { title: "CCSFs", template: "#StationName#" },
                    yAxis: { title: "VOLUME" }
                });
            }

            dynamicChartSeriesGenerate(chainOfFactoryChart, 'GetShipmentSeals', filter, "StationName", true);

            break;
        case this.ChartsEnum.ScreeningVolumes:
        default:
            showLayoutView("ScreeningVolumes", "SCREENING VOLUMES");

            if (screeningVolumesChart == undefined) {
                screeningVolumesChart = leftLayout.attachChart({
                    view: 'bar',
                    label: '#yValue#',
                    gradient: false,
                    xAxis: { "title": "CCSFs", "template": "#xValue#", "step": "500" },
                    yAxis: { "title": "VOLUME", "start": "0" },
                    value: '#yValue#'
                });
            }

            if (screeningVolumesChart.dataCount() === 0) {
                leftLayout.progressOn();
                screeningVolumesChart.loadExternal('/LiveScreening/GetScreeningVolumesChart', filter, null, function() {
                    leftLayout.progressOff();
                });
            }
        }
    }

    var init = function (config) {
        var self = this;
        cell.showView('dashboardContainer');

        var dashboardAccordionContainer = config.accordion.addItem('dashboardAccordionContainer', 'DASHBOARD');

        var dashboardAccordionFormStructure = [
                { type: "combo", comboType: "checkbox", name: "screeningLocations", label: "CCSF:", position: "label-left", labelWidth: 60, inputWidth: 210 },
                { type: "combo", name: "period", label: "Period:", position: "label-left", labelWidth: 60, inputWidth: 210 },
                { type: "calendar", name: "fromDateCalendar", label: "From Date:", position: "label-left", labelWidth: 60, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                { type: "calendar", name: "toDateCalendar", label: "To Date:", position: "label-left", labelWidth: 60, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                { type: "block", list: [{ type: "button", name: "resetButton", value: "RESET", offsetLeft: 80 }, { type: "newcolumn" }, { type: "button", name: "filterButton", value: "APPLY" }] }
        ];

        dashboardAccordionForm = dashboardAccordionContainer.attachForm(dashboardAccordionFormStructure);

        var screeningLocations = dashboardAccordionForm.getCombo('screeningLocations');
        screeningLocations.makeMultiSelect();

        screeningLocations.loadExternal('/LiveScreeningWebSpecific/GetScreeningLocations', 'value', 'text', function(data) {
            if (data.length === 1) {
                screeningLocations.setComboValue(data[0].value);
                screeningLocations.setComboText(data[0].text);
                screeningLocations.setChecked(0, true);
            }
        });

        this._setCalendarDate('fromDateCalendar', dashboardAccordionForm, new Date());
        this._setCalendarDate('toDateCalendar', dashboardAccordionForm, new Date());
        var period = dashboardAccordionForm.getCombo('period');

        period.clearAll();
        period.loadExternal('/LiveScreening/GetPeriods', 'Id', 'Name', function () {
            period.selectOption(0);
        });

        period.attachEvent("onChange", function (value) {
            var dateNow = new Date();
            var newDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - (value - 1));

            this._setCalendarDate('fromDateCalendar', dashboardAccordionForm, newDate);
        }.bind(this));

        dashboardAccordionForm.attachEvent("onButtonClick", function (name) {
            if (name == 'filterButton') {
                this.filter();
            } else {
                this.resetControls();
            }
        }.bind(this));


        // Charts cell

        mainLayout = config.cell.attachLayout('2U');

        leftLayout = mainLayout.cells('a');

        // Charts Data View

        var rightLayout = mainLayout.cells('b');

        rightLayout.setWidth('230');
        rightLayout.hideHeader();
        dataViewCharts = rightLayout.attachDataView({
            type: {
                template: '<label style=\'color: #007DCA;font-size: 14px;font-weight: bold;\'>#title#<label/>',
                height: 50
            }
        });

        dataViewCharts.attachEvent('onSelectChange', function (sel_arr) {
           this.filter(sel_arr[0]); 
        }.bind(this));

        dataViewCharts.attachEvent('onAfterRender', function () {
            //dataViewCharts.select("1");
        });

        helpers.getDataFromExternalResource('/LiveScreeningWebSpecific/GetChartsList', null, function(data) {
            dataViewCharts.clearAll();
            dataViewCharts.parse(data, "xml");
            dataViewCharts.select(1);
        });

    }.call(this, this.config);
};