﻿var shipmentDetailsWindow = function (imagePath) {
    var $this = this;

    var loadFormData = function (screeningTransactionId, needRemark) {
        $this.viewData.clearAll();

        helpers.getDataFromExternalResource('/LiveScreening/GetTransactionDetails', { id: screeningTransactionId }, function (data) {
            $this.layout.progressOff();
            var formData = data;

            $this.window.screeningDetailsForm.setItemLabel('sampleNumberLabelValue', formData.SampleNumber);
            $this.window.screeningDetailsForm.setItemLabel('methodLabelValue', formData.Method);
            $this.window.screeningDetailsForm.setItemLabel('deviceLabelValue', formData.Device);
            $this.window.screeningDetailsForm.setItemLabel('screenerLabelValue', formData.Screener);
            $this.window.screeningDetailsForm.setItemLabel('piecesLabelValue', formData.ScreenedPieces);
            $this.window.screeningDetailsForm.setItemLabel('resultLabelValue', formData.Result);
            $this.window.screeningDetailsForm.setItemLabel('timestampLabelValue', window.dhx4.date2str(new Date(formData.Timestamp), '%m-%d-%Y %H:%i:%s'));
            $this.window.screeningDetailsForm.setItemLabel('locationLabelValue', formData.Location);
            $this.window.screeningDetailsForm.setItemLabel('commentLabelValue', formData.Comment);

            $this.imagesCell.progressOn();
            $this.viewData.loadExternal('/LiveScreening/GetImagesForTransaction', { id: screeningTransactionId, method: formData.Method }, function () { $this.imagesCell.progressOff(); });
            if (needRemark) {
                $this.remarkGrid.clearAll();
                $this.remarksCell.progressOn();
                $this.remarkGrid.loadExternal('/LiveScreeningWebSpecific/GetShipmentRemarksBySample', { sampleNumber: formData.SampleNumber, reference: formData.ShipmentReference }, function () { $this.remarksCell.progressOff(); });
            }
        });
    }

    this.show = function (shipment) {
        $this.screeningTransactionGrid.clearAll();
        $this.layout.progressOn();

        $this.window.headerInfoForm.setItemLabel('ShipmentReference', shipment.data[0]);
        $this.window.headerInfoForm.setItemLabel('ScreenedPieces', shipment.data[6] + ' of ' + shipment.data[5]);
        $this.window.headerInfoForm.setItemLabel('Validated', shipment.data[8] != "transparent.png" ? "Yes":"No");
        $this.window.headerInfoForm.setItemLabel('Status', parseInt(shipment.data[6]) < parseInt(shipment.data[5]) ? 'Screening In Progress' : 'Screening Completed');

        $this.screeningTransactionGrid.loadExternal('/LiveScreeningWebSpecific/GetScreeningTransactionsByHouseBill', { housebillNumber: shipment.data[0] }, function () {
            var screeningTransactionId = $this.screeningTransactionGrid.getRowId(0);
            loadFormData(screeningTransactionId, true);
        });
        this.window.setModal(1);
        this.window.show();
    }.bind(this);

    this.hide = function () {
        this.window.hide();
        this.window.setModal(0);
    }.bind(this);

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('shipmentDetailsWindow', 0, 0, 900, 650);

        var layout = window.attachLayout('4I');

        var headerInfoCell = layout.cells('a');
        headerInfoCell.setHeight('50');
        headerInfoCell.hideHeader();
        headerInfoCell.fixSize(0, 1);

        var headerInfoFormStructure = [
		    { type: "label", label: "Shipping Reference#:" },
		    { type: "newcolumn" },
		    { type: "label", name: "ShipmentReference", labelWidth: 150 },
            { type: "newcolumn" },
            { type: "label", label: "Screened Pieces#:" },
            { type: "newcolumn" },
            { type: "label", name: "ScreenedPieces", labelWidth: 75 },
            { type: "newcolumn" },
            { type: "label", label: "Validated?" },
            { type: "newcolumn" },
            { type: "label", name: "Validated", labelWidth: 40 },
            { type: "newcolumn" },
            { type: "label", label: "Status:" },
            { type: "newcolumn" },
            { type: "label", name: "Status" }
        ];

        window.headerInfoForm = headerInfoCell.attachForm(headerInfoFormStructure);

        var screeningDetailsCell = layout.cells('b');
        screeningDetailsCell.setText('SCREENING DETAILS');
        screeningDetailsCell.setWidth(500);
        screeningDetailsCell.setHeight(320);
        var screeningDetailsFormStructure = [
		{
		    type: "block", list: [
            { type: "label", label: "Sample#:", labelHeight: 14 },
            { type: "label", label: "Method:", labelHeight: 14 },
            { type: "label", label: "Device#:", labelHeight: 14 },
            { type: "label", label: "Screener:", labelHeight: 14 },
            { type: "label", label: "SLAC Screened:", labelHeight: 14 },
            { type: "label", label: "Result:", labelHeight: 14 },
            { type: "label", label: "Timestamp:", labelHeight: 14 },
            { type: "label", label: "Location:", labelHeight: 14 },
            { type: "label", label: "Comment from Device:", labelHeight: 14 }
		    ]
		},
		{ type: "newcolumn" },
		{
		    type: "block", list: [
            { type: "label", name: "sampleNumberLabelValue", labelHeight: 14 },
            { type: "label", name: "methodLabelValue", labelHeight: 14 },
            { type: "label", name: "deviceLabelValue", labelHeight: 14 },
            { type: "label", name: "screenerLabelValue", labelHeight: 14 },
            { type: "label", name: "piecesLabelValue", labelHeight: 14 },
            { type: "label", name: "resultLabelValue", labelHeight: 14 },
            { type: "label", name: "timestampLabelValue", labelHeight: 14 },
            { type: "label", name: "locationLabelValue", labelHeight: 14 },
            { type: "label", name: "commentLabelValue", labelHeight: 14 }
		    ]
		}
        ];
        window.screeningDetailsForm = screeningDetailsCell.attachForm(screeningDetailsFormStructure);

        this.imagesCell = layout.cells('c');
        this.imagesCell.setText('IMAGES');
        this.imagesCell.setHeight(320);

        var mapItemTemplate = "<div>" +
					    "<img style='max-width:400px; max-height:135px;' src='" + imagePath + "#Name#' />" +
			        "</div>";

        this.viewData = this.imagesCell.attachDataView({
            type: {
                template: mapItemTemplate,
                height: 135,
                width: 400
            }
        });

        this.viewData.attachEvent("onItemClick", function (t, r, u) {

        });

        this.footerLayout = layout.cells('d').attachLayout('2E');
        this.screeningTransactionCell = this.footerLayout.cells('a');
        this.screeningTransactionCell.setText('SCREENING TRANSACTIONS');
        this.screeningTransactionCell.setHeight(115);

        var screeningTransactionGrid = this.screeningTransactionCell.attachGrid();
        screeningTransactionGrid.setIconsPath(dhtmlx.image_path);
        screeningTransactionGrid.setHeader(["Sample#", "Method", "Screener", "Timestamp", "Remark"]);
        screeningTransactionGrid.setColTypes("ro,ro,ro,ro,ro");
        screeningTransactionGrid.setColSorting('str,str,str,str,str');
        screeningTransactionGrid.setInitWidths('200,200,110,110,*');
        screeningTransactionGrid.enableMultiline(true);
        screeningTransactionGrid.init();
        this.screeningTransactionGrid = screeningTransactionGrid;

        this.screeningTransactionGrid.attachEvent("onRowSelect", function (rId, cInd) {
            loadFormData(rId);
        });

        this.remarksCell = this.footerLayout.cells('b');
        this.remarksCell.setText('REMARKS');
        this.remarksCell.setHeight(115);

        var remarkGrid = this.remarksCell.attachGrid();
        remarkGrid.setIconsPath(dhtmlx.image_path);
        remarkGrid.setHeader(["Sample#", "User", "Timestamp", "Remark"]);
        remarkGrid.setColTypes("ro,ro,ro,ro");
        remarkGrid.setColSorting('str,str,str,str');
        remarkGrid.setInitWidths('200,200,200,*');
        remarkGrid.enableMultiline(true);
        remarkGrid.init();
        this.remarkGrid = remarkGrid;

        window.button('close').attachEvent('onClick', function () {
            window.setModal(0);
            window.hide();
        });

        window.setText('SHIPMENT SCREENING DETAILS');
        window.hide();
        window.centerOnScreen();
        window.button('minmax').show();
        window.button('minmax').enable();

        this.window = window;
        this.layout = layout;
    }.apply(this);
};