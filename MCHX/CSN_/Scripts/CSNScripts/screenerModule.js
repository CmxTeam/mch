﻿var screenerModule = function (cell, accordion) {
    var grid, tree;
    var $this = this;
    var treeSelectedId;
    this.show = function () {
        cell.setText('SCREENERS');
        cell.showView('screenersGridContainer');
    }

    this.filter = function (screener) {
        cell.progressOn();
        grid.clearAll();
        grid.loadExternal('/LiveScreeningWebSpecific/GetScreenersGrid', { screener: screener }, function () {
            cell.progressOff();
        });        
    }

    this.isNumeric = function (input) {
        return (input - 0) == input && ('' + input).trim().length > 0;
    }

    grid = function () {
        cell.showView('screenersGridContainer');
        var toolbar = cell.attachToolbar();
        var grid = cell.attachGrid();

        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>' +
            //'<item type="buttonSelect" openAll="true" id="button_select_export" text="Actions" title="" >' +
            //'<item type="button" id="btnAddScreener" text="Add" image="" />' +
            //'<item type="button" id="btnEditScreener" text="Edit" image="" /></item>' +
            '<item type="buttonSelect" openAll="true" id="btnDownload" text="Download" title="" >' +
            '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
            '</item>' +
            '</toolbar>', function () { });

        toolbar.initQuikSearch(grid);

        toolbar.attachEvent('onClick', function(id) {
            if (id === "btnEditScreener") {
                if (selectedScreenerId == null) {
                    dhtmlx.alert({
                        title: "Warning",
                        type: "warning",
                        text: "You need to select Screener"
                    });
                } else {
                    //alert(selectedScreenerId);
                }
            } else if ("btnDownload") {
                cell.progressOn();
                helpers.exportToExcel("/LiveScreeningWebSpecific/ExportScreeners", { screener: treeSelectedId || "rootElement" }, "Screeners", function () { cell.progressOff(); });
            }
        });
        
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Id", "Photo", "Name", "Assigned To CCSF", "Screening Methods", "Work Phone", "Work Email", "Cell Phone"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
        grid.setColSorting('str,str,str,str,str,str,str,str');
        grid.setInitWidths('150,150,150,110,160,110,150,150');
        grid.setColumnHidden(0, true);
        grid.showTotal();
        //grid.enableColumnMove(true);
        grid.init();

        grid.attachEvent("onRowSelect", function (id, ind) {
            
        });

        return grid;
    }();

    tree = function () {
        var screenersContainer = accordion.addItem('screenersContainer', 'SCREENERS');
        var toolbar = screenersContainer.attachToolbar();
        var tree = screenersContainer.attachTree();

        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar><item type="buttonInput" id="button_input_2" value="" /></toolbar>', function () { });
        toolbar.getInput("button_input_2").placeholder = "Find";

        toolbar.attachEvent('onEnter', function (name, value) {            
            tree.loadExternal('/LiveScreeningWebSpecific/GetScreenersTree', {filter: value});            
        });
        
        tree.setImagePath(dhtmlx.tree_image_path);
        tree.loadExternal('/LiveScreeningWebSpecific/GetScreenersTree');        

        tree.attachEvent("onClick", function (id) {
            $this.filter(id);
            treeSelectedId = id;
        });

        tree.attachEvent("onXLE", function () {
            if (tree.getIndexById("rootElement") != -1)
                tree.openItem("rootElement");
            if (tree.getIndexById(treeSelectedId) != -1)
                tree.selectItem(treeSelectedId);
        });

        return tree;
    }();

    this.filter();
    return this;
};