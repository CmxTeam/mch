﻿var screeningDevicesModule = function (cell, accordion, userRole) {
    var grid, tree, formStructure, devSubmitForm, treeSelectedId;
    var $this = this;

    var selectedDeviceId = function () {
        var itemId = grid.getSelectedRowId() || tree.getSelectedItemId() || null;
        return $this.isNumeric(itemId) ? itemId : null;
    };

    var treeToolbar = null;

    this.show = function () {
        cell.setText('SCREENING DEVICES');
        cell.showView('devicesGridContainer');
    }

    this.filter = function (device) {
        cell.progressOn();
        grid.clearAll();
        grid.loadExternal('/LiveScreeningWebSpecific/GetScreeningDevices', {device: device}, function (data) { cell.progressOff(); }, 'json');       
    }

    this.isNumeric = function (input) {
        return (input - 0) == input && ('' + input).trim().length > 0;
    }

    this._refreshCombo = function (form, comboName, loadUrl, callback) {
        var combo = form.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        console.log(combo);
        combo.loadExternal(loadUrl, "value", "text", callback);
    }

    var crateDeviceFormInit = function(id) {
        var selectedLocationId = null;
        var selectedWarehouseLocationId = null;
        devSubmitForm = new submitForm("Add/Edit Device", formStructure,
            function (formData) {
                console.log(formData);
                helpers.executePost('/LiveScreening/CreateDevice', formData, function (data) {
                    if (data.Data.First === true) {
                        devSubmitForm.window.close();

                        var filterValue = treeToolbar.getValue("txtFilterDevices");
                        $this.filter(tree.getSelectedItemId() || "rootElement");
                        tree.loadExternal('/LiveScreeningWebSpecific/GetScreeningDevicesTree', { filter: filterValue });
                        tree.refreshItem(0);
                    } else {
                        devSubmitForm.form.setItemLabel('errorMessage', data.Data.Second);
                        devSubmitForm.window.progressOff();
                    }
                });
            }, 400, 320);

        devSubmitForm.form.getCombo("StatusId").attachEvent("onChange", function () {
            var locationCombo = devSubmitForm.form.getCombo("LocationId");
            var wrhLocCombo = devSubmitForm.form.getCombo("WarehouseLocationId");
            if (this.getSelectedValue() === "513" || this.getSelectedValue() === "511" || this.getSelectedValue() === "512") {
                locationCombo.unSelectOption();
                wrhLocCombo.unSelectOption();

                devSubmitForm.form.disableItem("locationsComboBlock");
            } else {
                devSubmitForm.form.enableItem("locationsComboBlock");
                wrhLocCombo.clearAll();

                if (selectedLocationId) {
                    locationCombo.selectOption(locationCombo.getIndexByValue(selectedLocationId));
                    $this._refreshCombo(devSubmitForm.form, "WarehouseLocationId", '/LiveScreeningWebSpecific/GetWarehouseLocations?locationId=' + selectedLocationId, function () {
                        if (selectedWarehouseLocationId)
                            wrhLocCombo.selectOption(wrhLocCombo.getIndexByValue(selectedWarehouseLocationId));
                    });
                }
            }
        });

        devSubmitForm.form.attachEvent('onChange', function (id, value) {
            if (id === "LocationId") {
                console.log('onChange', value);
                $this._refreshCombo(devSubmitForm.form, "WarehouseLocationId", '/LiveScreeningWebSpecific/GetWarehouseLocations?locationId=' + value);
            }
        });

        devSubmitForm.form.getCombo("LocationId").loadExternal('/LiveScreeningWebSpecific/GetScreeningLocations', 'value', 'text', function (data) {
            if (data.length === 1) {
                devSubmitForm.form.getCombo("LocationId").setComboValue(data[0].value);
                devSubmitForm.form.getCombo("LocationId").setComboText(data[0].text);
                $this._refreshCombo(devSubmitForm.form, "WarehouseLocationId", '/LiveScreeningWebSpecific/GetWarehouseLocations?locationId=' + data[0].value);
            }
        });
        devSubmitForm.form.getCombo("CertifiedDeviceId").loadExternal('/LiveScreeningWebSpecific/GetCertifiedDevices', 'value', 'text');

        if (id === "btnEditDevices") {
            devSubmitForm.form.setReadonly("DeviceBarcode", true);
            devSubmitForm.form.setReadonly("SerialNumber", true);
            var addEditUrl = "/LiveScreening/GetDeviceById"; //need url for editData
            helpers.getDataFromExternalResource(addEditUrl, { id: selectedDeviceId() }, function (data) {
                selectedLocationId = data.LocationId;
                selectedWarehouseLocationId = data.WarehouseLocationId;
                if (data.LocationId) {
                    $this._refreshCombo(devSubmitForm.form, "WarehouseLocationId", '/LiveScreeningWebSpecific/GetWarehouseLocations?locationId=' + data.LocationId, function () {
                        devSubmitForm.form.setFormData(data);
                    });
                } else {
                    devSubmitForm.form.setFormData(data);
                }
            });
        };

        //  formData = { Barcode: "gdfsgfd", Id: "50", LocationId: "5", ScreeningMethodId: "4", SerialNumber: "fgdsgf", StatusId: "510", WarehouseLocationId: "4745" }
        devSubmitForm.form.enableLiveValidation(true);
        return devSubmitForm;
    }

    grid = function () {
        cell.showView('devicesGridContainer');
        var grid = cell.attachGrid();

        var toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>' +
            (userRole == 16 ? '<item type="buttonSelect" id="btnAddEditSelect" openAll="true" text="Actions" title="" >' +
            '<item type="button" id="btnAddDevices" text="Add" title="" />' +
            '<item type="button" id="btnEditDevices" text="Edit" title="" /></item>' : "") +
            '<item type="buttonSelect" openAll="true" id="btnDownload" text="Download" title="" >' +
            '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
            '</item>'+
            '</toolbar>', function () { });
        
        toolbar.initQuikSearch(grid);

        toolbar.attachEvent('onClick', function (id) {
            if (id === "btnAddDevices" || id === "btnEditDevices") {
                if (id === "btnEditDevices" && selectedDeviceId() == null) {
                    dhtmlx.alert({
                        title: "Warning",
                        type: "warning",
                        text: "Please select a Device"
                    });
                } else {
                    crateDeviceFormInit(id);
                }
            }else if ("btnDownload") {
                cell.progressOn();
                helpers.exportToExcel("/LiveScreeningWebSpecific/ExportScreeningDevices", { device: treeSelectedId || "rootElement" }, "Devices", function () { cell.progressOff(); });
            }
        });
       
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Id", "Certified Device", "Serial No", "Model Number", "Technology", "Assigned To CCSF"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro");
        grid.setColSorting('str,str,str,str,str,str');
        grid.setInitWidths('200,200,200,200,200,*');
        grid.setColumnHidden(0, true);
        grid.showTotal();
        //grid.enableColumnMove(true);
        grid.init();

        return grid;
    }();

    formStructure = [
            { type: "hidden", name: "Id" },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "SerialNumber", label: "Serial Number:", labelWidth: 135, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "DeviceBarcode", label: "Barcode:", labelWidth: 135, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "CertifiedDeviceId", label: "Model Number:", labelWidth: 135, position: "label-left", inputWidth: 200, required: true, filtering: true,
                        validate: function (input) { return devSubmitForm.form.getCombo("CertifiedDeviceId").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', name: "locationsComboBlock",
                list: [
                     {
                         type: "combo", name: "LocationId", label: "Assigned to CCSF:", labelWidth: 135, position: "label-left", inputWidth: 200, required: true, filtering: true,
                         validate: function (input) { return devSubmitForm.form.getCombo("LocationId").getSelectedIndex() !== -1 }
                     },
                     {
                         type: "combo", name: "WarehouseLocationId", label: "Warehouse Location:", labelWidth: 135, position: "label-left", inputWidth: 200, filtering: true,
                         validate: function (input) { return devSubmitForm.form.getCombo("WarehouseLocationId").getSelectedIndex() !== -1 }
                     }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "StatusId", readonly: true, label: "Status:", labelWidth: 135, position: "label-left", inputWidth: 200,
                        options: [
                            { value: 510, text: "Active" },
                            { value: 512, text: "Repair" },
                            { value: 513, text: "Deleted" },
                            { value: 511, text: "Unassigned" }
                        ], required: true
                    }
                ]
            },
            { type: "label", name: 'errorMessage', label: "&nbsp", offsetLeft: 16, className: 'label-red' }
    ];

    tree = function () {
        var screeningDevicesContainer = accordion.addItem('screeningDevicesContainer', 'DEVICES');
        treeToolbar = screeningDevicesContainer.attachToolbar();
        var tree = screeningDevicesContainer.attachTree();
        treeToolbar.setIconsPath(dhtmlx.image_path);
        
        treeToolbar.loadStruct(
            '<toolbar><item type="buttonInput" id="txtFilterDevices" value="">' +
            '</item>' +
            '</toolbar>', function () { });

        treeToolbar.getInput("txtFilterDevices").placeholder = "Find";

        treeToolbar.attachEvent('onEnter', function (name, value) {
            tree.loadExternal('/LiveScreeningWebSpecific/GetScreeningDevicesTree', { filter: value });
            tree.refreshItem(0);
        });

        tree.setImagePath(dhtmlx.tree_image_path);
        tree.loadExternal('/LiveScreeningWebSpecific/GetScreeningDevicesTree');

        tree.attachEvent("onClick", function (id) {
            $this.filter(id);
            treeSelectedId = id;
        });

        tree.attachEvent("onXLE", function () {
            if (tree.getIndexById("rootElement") != -1)
                tree.openItem("rootElement");
            if (tree.getIndexById(treeSelectedId) != -1)
                tree.selectItem(treeSelectedId);
        });

        return tree;
    }();

    this.filter();
    return this;
};