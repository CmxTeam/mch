﻿var dashboardMapModule = function (cell, accordion) {
    var gMaps, viewData, mapItemsContainer;
    var infoBubbles = [];
    var markers = [];
    var $this = this;
    this.show = function () {
        cell.setText('CARGO SCREENING NETWORK MAP');
        cell.showView('map');
    }

    this._locationSelected = function (id) {
        var location = viewData.get(id);
        var lat = location.Latitude;
        var lng = location.Longitude;
        var zoom = parseInt(location.Zoom);
        $this.setPosition(lat, lng, zoom);
    }

    this.clearMapPins = function() {
        if (infoBubbles.length > 0) {
            for (var i = 0; i < infoBubbles.length; i++) {
                infoBubbles[i].close();
            }
            infoBubbles = [];
        }
        if (markers.length > 0) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        }
    }

    this.refreshMapLocations = function() {
        mapItemsContainer.progressOn();

        $this.clearMapPins();
        helpers.getDataFromExternalResource('/LiveScreening/GetHostPlusMapItems', null, function (data) {
            viewData.clearAll();
            viewData.parse(data, "json");
            //var id = data[0].id;
            //viewData.select(id);
            //$this._locationSelected(id);

            $this.applyPins(data);
            mapItemsContainer.progressOff();
        });
    }

    this._pinClicked = function (id) {
        console.log(id);
    }

    this.setPosition = function (lat, lng, zoom) {
        var myLatlng = new google.maps.LatLng(lat, lng);
        var myOptions = {
            zoom: zoom,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        gMaps.setOptions(myOptions);
    }

    this.applyPins = function (data) {
        for (var i = 0; i < data.length; i = i + 1) {
            var latlng = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);

            var marker = new google.maps.Marker({
                position: latlng,
                map: gMaps,
                animation: google.maps.Animation.DROP,
                icon: baseUrl + 'Content/appimages/' + data[i].Pin
            });

            markers.push(marker)

            var infowindow = new google.maps.InfoWindow({
                content:
                  '<div class="dhx_popup_dhx_skyblue" style="font-weight:bold;">' +
                      '<div><a class="pin-header" style="padding-right: 20px;" onclick="showGridTab(' + data[i].id + ');">' + data[i].Name + '</a><label>CCSF Type: ' + data[i].Details.CCSFType + '</label></div>' +
                      '<div class="pin-content">' +
                        '' +
                        '<div>Security manager: ' + data[i].Details.SecurityManagerName + '</div>' +
                        '<div>Email: <a href="mailto:' + data[i].Details.Email + '">' + data[i].Details.Email + '</a></div>' +
                        '<div>Office: ' + data[i].Details.Office + '</div>' +
                        '<div>Mobile: ' + data[i].Details.Mobile + '</div><br/>' +
                        '<div>Total Screened Pieces: ' + data[i].Details.TotalScannedPieces + '</div>' +
                        '<div>Total Passed: ' + data[i].Details.TotalPassed + '</div>' +
                        '<div>Total Failed: ' + data[i].Details.TotalFailed + '</div>' +
                        //'<div>Total Screened (Primary): ' + data[i].Details.TotalScreenedPrimary + '</div>' +
                        //'<div>Total Screened (Secondary): ' + data[i].Details.TotalScreenedSecondary + '</div>' +
                      '</div>' +
                  '</div>'
            });

            var infoBubble = new InfoBubble({
                content: '<div class="dhx_popup_dhx_skyblue">' +
                            '<div><a class="pin-header">' + data[i].Name + '</a></div>' +
                        '</div>',
                shadowStyle: 0,
                padding: 5,
                borderRadius: 0,
                arrowSize: 1,
                disableAutoPan: true,
                maxWidth: 20,
                maxHeight: 14,
                hideCloseButton: true
            });
            infoBubbles.push(infoBubble);
            infoBubble.setZIndex(-2000);

            infoBubble.open(gMaps, marker);
            (function (iw, m, bubble) {
                google.maps.event.addListener(m, 'click', function () {
                    iw.open(gMaps, m);
                    bubble.close();
                });

                google.maps.event.addListener(iw, 'closeclick', function () {
                    bubble.open(gMaps, m);
                });

            })(infowindow, marker, infoBubble);
        }
    }

    gMaps = function () {
        cell.showView('map');
        var map = cell.attachMap();
        return map;
    }();

    viewData = function () {
        var mapItemTemplate = "<div id='data_template'>" +
				"<div class='country_info'>" +
					"<div class='country_name'><span class='hint2'>#Name#</span></div>" +
				"</div>" +
			"</div>";

        mapItemsContainer = accordion.addItem('mapItemsContainer', 'CARGO SCREENING NETWORK');
        var viewData = mapItemsContainer.attachDataView({
            type: {
                template: mapItemTemplate,
                height: 20,
                width: 300
            }
        });

        viewData.attachEvent("onItemClick", $this._locationSelected);
        return viewData;
    }();

    //$this.refreshMapLocations();

    $this.setPosition(55.8833, -140.6500523, 4);

    return this;
};