﻿var liveScreeningCreateHawb = function (info) {
    this.info = info;
    this.onSubmit = null;

    this._refreshCombo = function (comboName, loadUrl) {
        var combo = this.form.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        combo.load(loadUrl, 'json');
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        var win = windows.createWindow('enterToLiveScreeningWindow', 0, 0, 340, 300);

        var formStructure = [
            {
                type: 'block', list: [
                    { type: "label", label: 'Shipment Reference#: ' + this.info.shipment},
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "origin", label: "Origin:", labelWidth: 120, maxLength: 3, position: "label-left", inputWidth: 150, required: true },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "destination", label: "Destination:", labelWidth: 120, maxLength: 3, position: "label-left", inputWidth: 150, required: true },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "totalPieces", label: "Total Pieces:", labelWidth: 120, maxLength: 4, position: "label-left", inputWidth: 150, required: true, validate: "NotEmpty,ValidInteger" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "totalSlac", label: "Total SLAC:", labelWidth: 120, maxLength: 4, position: "label-left", inputWidth: 150, required: true, validate: "NotEmpty,ValidInteger" },
                ]
            },
            {
                type: 'block', list: [
                    { type: "label", name: 'errorMessage', label: '', className: 'label-red'}
                ]
            },
            {
                type: 'block', list: [
                    { type: "button", name: "cancelButton", value: "CANCEL", offsetLeft: 112, offsetTop: 15 },
                    { type: "newcolumn" },
                    { type: "button", name: "submitButton", value: "CREATE", offsetTop: 15 }
                ]
            }
        ];        

        form = win.attachForm(formStructure);
        form.attachEvent("onButtonClick", function (name) {
            if (name == 'cancelButton') {
                this.win.close();
            } else if (name == 'submitButton') {
                form.setItemLabel('errorMessage', '');
                if (form.validate()) {
                    this.win.progressOn();
                    var values = form.getValues();
                    dhx4.ajax.get(window.WebApiUrl + "/LiveScreening/CreateShipment?userId=" + '44025' +
                        '&locationId=' + this.info.location.id +
                        '&number=' + this.info.shipment +
                        '&origin=' + values.origin +
                        '&destination=' + values.destination +
                        '&pieces=' + values.totalPieces +
                        '&slac=' + values.totalSlac, function(a) {
                            var data = JSON.parse(a.xmlDoc.response || a.xmlDoc.responseText);
                            this.win.progressOff();
                            console.log(data.Data);
                            if (data.Data === 'OK') {
                                this.win.close();
                                if (this.onSubmit) {
                                    this.onSubmit();
                                }
                            } else {
                                form.setItemLabel('errorMessage', data.Data);
                                var origin = form.getInput("origin");
                                var destination = form.getInput("destination");
                                if (data.Data.trim() === "Invalid Origin") {
                                    origin.parentElement.parentElement.classList.remove("validate_ok");
                                    origin.parentElement.parentElement.classList.add("validate_error");
                                    destination.parentElement.parentElement.classList.add("validate_ok");
                                    destination.parentElement.parentElement.classList.remove("validate_error");
                                } else if (data.Data.trim() === "Invalid Destination") {
                                    destination.parentElement.parentElement.classList.remove("validate_ok");
                                    destination.parentElement.parentElement.classList.add("validate_error");
                                    origin.parentElement.parentElement.classList.add("validate_ok");
                                    origin.parentElement.parentElement.classList.remove("validate_error");
                                } else {
                                    origin.parentElement.parentElement.classList.remove("validate_ok");
                                    origin.parentElement.parentElement.classList.add("validate_error");
                                    destination.parentElement.parentElement.classList.remove("validate_ok");
                                    destination.parentElement.parentElement.classList.add("validate_error");
                                }
                            }
                        }.bind(this));
                }
            }            
        }.bind(this));
        form.setValidation('origin', function (data) {            
            return data.length == 3;
        });
        form.setValidation('destination', function (data) {
            return data.length == 3;
        });

        form.setValidation('totalPieces', function (data) {
            var number = Number(data);
            return number % 1 === 0 && number <= 9999 && number > 0;            
        });

        form.setValidation('totalSlac', function (data) {
            var number = Number(data);
            return number % 1 === 0 && number <= 9999 && number >= parseInt(form.getValues().totalPieces) && number > 0;
        });
        //form.enableLiveValidation(true);
        win.setText('SHIPMENT DOES NOT EXIST');
        win.setModal(1);
        win.denyResize();
        win.centerOnScreen();
        this.form = form;
        this.win = win;
    }.apply(this);
};