﻿var liveScreening = function (info) {
    this.info = info;
    this.onSubmit = null;
    this.onClose = null;
    var _liveScreeningCreateHawb = null;
    var resetMessageBox = null;
    var completeMessageBox = null;

    this._resetShipment = function () {
        this.form.setItemValue('shipmentReference', '');
        this.form.setItemValue('slac', '');
        this.form.setItemValue('shipper', '');
        this.form.setItemValue('consignee', '');
        this.form.setItemValue('origin', '');
        this.form.setItemValue('destination', '');
        this.form.setItemValue('totalPieces', '');
        this.form.setItemValue('totalSlac', '');
        this.form.setItemValue('totalWeight', '');
        this.form.setItemValue('decription', '');
        this.form.setItemValue('screeningStatus', '');
        this.form.disableItem('slac');
        this.form.disableItem('resetButton');
        this.form.disableItem('submitButton');
        this.form.enableItem('searchButton');
        this.form.enableItem('shipmentReference');
    }

    this.completeScreening = function(shipment){
        if (this.form.getItemValue('shipmentReference') !== shipment) {
            return;
        }

        this._resetForm();
        completeMessageBox = dhtmlx.alert({
            title: "Info",
            type: "warning",
            text: "Screening transaction completed!"
        });
    }

    this.disconnectDevice = function (userName) {
        this.window.close();
        resetMessageBox = dhtmlx.alert({
            title: "Info",
            type: "warning",
            text: "You are resetted from your device by " + userName
        });
    }

    this._resetForm = function () {
        this._resetShipment();
        this.form.disableItem('completeButton');
        this.form.setItemValue('transactionId', '');
        this.form.setItemValue('transactionDate', '');
    }

    this.setTransaction = function (transaction, timestamp) {
        this.form.setItemValue('transactionId', transaction);
        this.form.setItemValue('transactionDate', timestamp);
        this.form.enableItem('completeButton');
        this.form.disableItem('resetButton');
        this.form.setItemValue('screeningStatus', 'Item In Screening Chamber');
    }

    this.filter = function () {
        console.log(this);
        this.grid.clearAll();
        var date = new Date();
        var utcDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
        this.grid.loadExternal('/LiveScreeningWebSpecific/GetScreeningTransactions', {
            fromDate: utcDate,
            toDate: utcDate,
        });       
    }

    this.close = function () {
        if (this.window.close) {
            this.window.close();
        }        
    }

    var init = function () {
        var windows = new dhtmlXWindows();
        this.window = windows.createWindow('liveScreeningWindow', 0, 0, window.innerWidth, window.innerHeight);

        var tabbar = this.window.attachTabbar();
        tabbar.setArrowsMode("auto");

        tabbar.addTab('currentTab', 'CURRENT SCREENING TRANSACTION');
        var currentTab = tabbar.cells('currentTab');
        currentTab.setActive();

        tabbar.addTab('historyTab', 'PREVIOUS SCREENING TRANSACTIONS');

        var historyTab = tabbar.cells('historyTab');
        
        tabbar.attachEvent('onTabClick', function (name, previousName) {
            if (name == 'historyTab' && name != previousName) {
                this.filter.apply(this);
            }
        }.bind(this));
      
        var grid = historyTab.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Shipping Reference#", "Sample#", "Method", "Device#", "Screened On", "Screened at", "Total Pieces", "Total SLACs", "Screened Pieces", "Result", "Screener", "Validated?"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img");
        grid.setColSorting('str,str,str,str,str,str,str,str,str,str,str,str');
        grid.setInitWidths('200,245,65,65,115,95,95,100,110,65,80,75');
        grid.showTotal();
        grid.init();

        grid.attachEvent("onMouseOver", function (id, ind) {
            var cell = this.cells(id, ind).cell;
            var title = cell._brval || cell.innerText;

            if (title == "transparent.png")
                title = "Not Validated";
            else if (title == "green_chkmark.png")
                title = "Validated";

            cell.title = title;
        });

        var toolbar = historyTab.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>' +
            '<item type="buttonSelect" openAll="true" id="btnDownload" text="Download" title="" >' +
            '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
            '</item>' +
            '</toolbar>', function () { });

        toolbar.initQuikSearch(grid);

        toolbar.attachEvent('onClick', function (id) {
            if ("btnDownload") {
                tabbar.progressOn();
                var date = new Date();
                var utcDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
                helpers.exportToExcel("/LiveScreeningWebSpecific/ScreeningTransactionsExcelExport", {
                    fromDate: utcDate,
                    toDate: utcDate,
                }, "Previous Screening Transactions", function () { tabbar.progressOff(); });
            }
        });

        this.grid = grid;

        var formStructure = [
            {
                type: "fieldset", name: "step1fieldset", label: "Step 1: Screen Shipment", width: 500, height: 300, list: [
                    {
                        type: "block", list: [
                            {
                                type: "input", name: "shipmentReference", label: "Scan Shipment Reference#:", position: "label-top", inputWidth: 250, maxLength: 20, className: "label-bold label-big req-asterisk", offsetTop: 29, required:true,
                                validate: function (input) {  return input && input.trim() !== '' ? true : false }
                            },
                            { type: "newcolumn" },
                            { type: "button", name: "searchButton", value: "SEARCH", offsetTop: 64, offsetLeft: 30 }
                        ]
                    },

                    {
                        type: "block", list: [
                            { type: "input", name: "slac", label: "Screened SLAC:", disabled: true, position: "label-top", inputWidth: 250, className: "label-bold label-big", offsetTop: 31 },
                        ]
                    },
                    {
                        type: "block", list: [{ type: "button", name: "resetButton", value: "CANCEL", offsetLeft: 270, disabled: true }, { type: "newcolumn" }, { type: "button", name: "submitButton", value: "SUBMIT", disabled: true }]
                    }
                ]
            },
            {
                type: "fieldset", name: "step2fieldset", label: "Step 2: Shipment Details", list: [
                    { type: "input", name: "shipper", label: "Shipper:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "consignee", label: "Consignee:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "origin", label: "Origin:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "destination", label: "Dest:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "totalPieces", label: "Total Pieces:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "totalSlac", label: "Total SLAC:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "totalWeight", label: "Total Weight:", labelWidth: 100, position: "label-left", inputWidth: 300, readonly: true },
                    { type: "input", name: "decription", label: "Goods Description:", labelWidth: 100, position: "label-left", inputWidth: 330, rows: 3, readonly: true },
                ]
            },
            { type: "newcolumn" },
            {
                type: "fieldset", name: "step3fieldset", label: "Step 3: Current Screening Transaction Details", offsetLeft: 50, list: [
                    { type: "input", name: "location", value: this.info.location.text, label: "Screening Location:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "operator", label: "Operator:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true, value: helpers.getUserName() },
                    { type: "input", name: "device", value: this.info.device.text, label: "Device#:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "transactionId", label: "Transaction ID#:", labelWidth: 140, position: "label-left", inputWidth: 320, readonly: true },
                    { type: "input", name: "transactionDate", label: "Transaction Timestamp:", labelWidth: 140, position: "label-left", inputWidth: 200, readonly: true },
                    { type: "input", name: "screeningStatus", label: "Screening Status:", labelWidth: 140, position: "label-left", inputWidth: 320, readonly: true },
                    {
                        type: "block", list: [{ type: "button", name: "completeButton", value: "COMPLETE SCREENING", offsetLeft: 279, disabled: true }]
                    }
                ]
            }
        ];

        var form = currentTab.attachForm(formStructure);

        form.getInput("shipmentReference").onclick = function () {
            if($(this).is(':focus'))
                this.select();
        };

        var pressed = false;
        var chars = [];
        $(form.getInput("shipmentReference")).keypress(function (e) {
            chars.push(String.fromCharCode(e.which));

            if (pressed == false) {
                setTimeout(function () {
                    if (chars.length >= 5) {
                        var barcode = chars.join("");
                        console.log("Barcode Scanned: " + barcode);
                        var tmpBarcode = barcode;
                        barcode = barcode.replace(/ /g, '');
                        if (/(\w{7}|H\w{7})(\s)*\+[Y|S][0-9][0-9][0-9][0-9](\+)*/ig.test(barcode)) {
                            barcode = barcode.split("+")[0];
                            if (barcode.charAt(0).toUpperCase() == "H" && barcode.length > 7) {
                                barcode = barcode.substring(1, barcode.length);
                            }
                        } else if (/^H\w{7}(\+)*/ig.test(barcode)) {
                            barcode = barcode.substring(1, 8);
                        }
                        else if (/^[A-Z][A-Z][A-Z]\w{7}[0-9][0-9][0-9][0-9][0-9]/ig.test(barcode)) {
                            barcode = barcode.substring(3, 10);
                        }
                        else if (/\w{7}/ig.test(barcode)) {
                            barcode = barcode.charAt(0).toUpperCase() == "H" ? barcode.substring(1, 8) : barcode.substring(0, 7);
                        }
                        else {
                            barcode = tmpBarcode;
                            //barcode = "";
                            //dhtmlx.alert({
                            //    title: "Warning",
                            //    type: "warning",
                            //    text: "Unexpected Barcode!"
                            //});
                        }

                        $(form.getInput("shipmentReference")).val(barcode);
                    }
                    chars = [];
                    pressed = false;
                }, 200);
            }
            pressed = true;
        });

        form.getInput("slac").onclick = function () {
            if ($(this).is(':focus'))
                this.select();
        };

        form.attachEvent('onButtonClick', function (name) {
            if (name == 'resetButton') {
                dhx4.ajax.get(window.WebApiUrl + "/LiveScreening/CompleteScreening?deviceId=" + this.info.device.id, function () {
                    this._resetShipment();
                }.bind(this));                
            } else if (name == 'submitButton') {
                if (Number(this.form.getItemValue('slac')) % 1 === 0 && parseInt(this.form.getItemValue('slac')) <= parseInt(this.form.getItemValue('totalSlac')) && this.form.getItemValue('slac') > 0) {
                    this.form.getInput('slac').parentNode.parentNode.className = this.form.getInput('slac').parentNode.parentNode.className.replace('validate_error', '');
                    this.form.getInput('totalSlac').parentNode.parentNode.className = this.form.getInput('totalSlac').parentNode.parentNode.className.replace('validate_error', '');

                    this.form.disableItem('slac');
                    //this.form.disableItem('resetButton');
                    this.form.disableItem('submitButton');

                    this._createQueue.apply(this);
                } else {
                    if (this.form.getInput('slac').parentNode.parentNode.className.indexOf('validate_error') == -1) {
                        this.form.getInput('slac').parentNode.parentNode.className = this.form.getInput('slac').parentNode.parentNode.className + ' ' + 'validate_error';
                        this.form.getInput('totalSlac').parentNode.parentNode.className = this.form.getInput('totalSlac').parentNode.parentNode.className + ' ' + 'validate_error';
                    }
                }
            } else if (name == 'completeButton') {
                dhx4.ajax.get(window.WebApiUrl + "/LiveScreening/CompleteScreening?deviceId=" + this.info.device.id, function () {
                    this._resetForm();
                }.bind(this));
            } else if (name == 'searchButton') {
                if (form.validate()) {
                    this.form.disableItem('searchButton');
                    this._getShipment.apply(this);
                }
            }
        }.bind(this));

        this._getShipment = function () {
            dhx4.ajax.get(window.WebApiUrl + "/LiveScreening/TryGetHawb?locationId=" + this.info.location.id +
                        '&number=' + this.form.getValues().shipmentReference, function (a) {
                            var data = JSON.parse(a.xmlDoc.response || a.xmlDoc.responseText);
                            if (data.Data) {
                                this.form.setItemValue('shipper', data.Data.Shipper.Name);
                                this.form.setItemValue('consignee', data.Data.Consignee.Name);
                                this.form.setItemValue('origin', data.Data.Origin.Name);
                                this.form.setItemValue('destination', data.Data.Destination.Name);
                                this.form.setItemValue('totalPieces', data.Data.TotalPieces);
                                this.form.setItemValue('totalSlac', data.Data.TotalSlac);
                                this.form.setItemValue('totalWeight', data.Data.TotalWeight);
                                this.form.setItemValue('decription', data.Data.GoodsDescription);
                                this.form.setItemValue('slac', data.Data.TotalSlac - data.Data.ScannedSlac);

                                this.form.enableItem('slac');
                                this.form.enableItem('resetButton');
                                this.form.enableItem('submitButton');
                                this.form.disableItem('searchButton');
                                this.form.disableItem('shipmentReference');

                            } else {
                                info.shipment = this.form.getValues().shipmentReference;
                                info.slac = this.form.getValues().slac;
                                _liveScreeningCreateHawb = new liveScreeningCreateHawb(info);
                                _liveScreeningCreateHawb.onSubmit = function () {
                                    this._getShipment.apply(this);
                                }.bind(this);
                                this.form.enableItem('searchButton');
                            }
                        }.bind(this));
        }

        this._createQueue = function () {
            helpers.getDataFromExternalResource("/LiveScreening/CreateScreeningQueue", {
                locationId: this.info.location.id,
                number: this.form.getValues().shipmentReference,
                deviceId: this.info.device.id,
                slac: this.form.getValues().slac
            }, function(data) {
                if (data) {
                    this.form.setItemValue('screeningStatus', 'Item In Screening Queue');
                    this.form.disableItem('slac');
                    //this.form.disableItem('resetButton');
                    this.form.disableItem('submitButton');
                }
            }.bind(this));
        }

        this.window.attachEvent('onClose', function () {
            if (_liveScreeningCreateHawb && _liveScreeningCreateHawb.win.close)
                _liveScreeningCreateHawb.win.close();
            if (completeMessageBox)
                completeMessageBox.getElementsByClassName("dhtmlx_popup_button")[0].click();
            if (resetMessageBox)
                resetMessageBox.getElementsByClassName("dhtmlx_popup_button")[0].click();
            if (this.onClose) {
                this.onClose();
            }
            return true;
        }.bind(this));

        this.window.setText('LIVE SCREENING');
        this.window.setModal(1);
        this.window.denyResize();
        this.window.centerOnScreen();
        this.window.denyResize();
        this.window.denyMove();
        this.window.button('park').hide();
        this.window.button('minmax').hide();
        this.form = form;
    }.apply(this);  
};