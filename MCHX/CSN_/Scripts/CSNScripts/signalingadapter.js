﻿(function () {
    this.CsnSignaling = this.CsnSignaling || {};

    CsnSignaling.SignalAdapter = function (config) {        
        $.connection.hub.url = window.SignalRUrl;        
        var signalingHub = $.connection.liveScreeningHub;

        signalingHub.on('transactionReceived', function (transaction, timestamp) {            
            window._liveScreening.setTransaction(transaction, timestamp);
        });

        signalingHub.on('completeScreening', function (shipment) {
            window._liveScreening.completeScreening(shipment);            
        });

        signalingHub.on('deviceHasBeenDisconnected', function (userName) {
            window._liveScreening.disconnectDevice(userName);
        });

        signalingHub.on('usersCount', function (users) {
            console.log(users);
        });

        this.setDevice = function (deviceId) {
            signalingHub.server.setDevice(deviceId);
        }
        
        $.connection.hub.start().then(function () {
            signalingHub.server.addUser();
        });                

        $.connection.hub.disconnected(function () {
            console.log('disconnected');
        });
    };

})();