﻿var screeningTransactionsModule = function (cell, accordion, transactionRowClickListener, shipmentRowClickListener, userRole) {
    var grid,toolbarWithReference, gridWithReference, searchPanelForm, toolbar;
    var $this = this;
    this.listener = transactionRowClickListener;
    this.shipmentRowClickListener = shipmentRowClickListener;

    var getActiveToolbar = function () {
        if (cell.getViewName() != "normalView")
            return toolbarWithReference;
        else
            return toolbar;
    }

    var getActiveGrid = function () {
        if (cell.getViewName() != "normalView")
            return gridWithReference;
        else
            return grid;
    }

    this.show = function () {
        cell.setText('SCREENING TRANSACTIONS');
        cell.showView('normalView');
    }

    this.filter = function () {
        var isValid = searchPanelForm.validateItem("fromDateCalendar") ||
            searchPanelForm.validateItem("form_input_reference") ||
            searchPanelForm.validateItem("form_input_sample");

        if (isValid) {
            searchPanelForm.resetValidateCss("form_input_reference");
            searchPanelForm.resetValidateCss("form_input_sample");
            searchPanelForm.resetValidateCss("fromDateCalendar");
            this._filter(searchPanelForm.getCalendar('fromDateCalendar').getFormatedDate(),
               searchPanelForm.getCalendar('toDateCalendar').getFormatedDate(),
               searchPanelForm.getItemValue('form_input_reference'),
               searchPanelForm.getItemValue('form_input_sample'),
               searchPanelForm.getCombo('screeningLocations').getAllCheckedIds().join(),
               searchPanelForm.getCombo('screeningAreas').getAllCheckedIds().join(),
               searchPanelForm.getCombo('screeningMethods').getAllCheckedIds().join(),
               searchPanelForm.getCombo('screeningDevices').getAllCheckedIds().join(),
               searchPanelForm.getCombo('screeners').getAllCheckedIds().join(),
               searchPanelForm.getCombo('screeningResults').getAllCheckedIds().join(),
               searchPanelForm.getCombo('cctvCameras').getAllCheckedIds().join(),
               searchPanelForm.getItemValue('isValid') === 1 ? true : false);
        }
    }

    this.resetControls = function () {
        this._clearCalendar('fromDateCalendar', searchPanelForm);
        this._clearCalendar('toDateCalendar', searchPanelForm);

        searchPanelForm.getCombo('screeningLocations').checkUncheckAll(false);
        searchPanelForm.getCombo('screeningMethods').checkUncheckAll(false);
        searchPanelForm.getCombo('screeningDevices').checkUncheckAll(false);
        searchPanelForm.getCombo('screeners').checkUncheckAll(false);
        searchPanelForm.getCombo('screeningResults').checkUncheckAll(false);
        searchPanelForm.getCombo('screeningAreas').checkUncheckAll(false);
        searchPanelForm.getCombo('cctvCameras').checkUncheckAll(false);
        
        searchPanelForm.setItemValue('form_input_reference', '');
        searchPanelForm.setItemValue('form_input_sample', '');
        searchPanelForm.uncheckItem('isValid');
    }

    this.setLocation = function (id) {
        var ind = searchPanelForm.getCombo('screeningLocations').getIndexByValue(id);
        searchPanelForm.getCombo('screeningLocations').selectOption(ind);
    }

    this._filter = function (dateFrom, dateTo, shippingReference, sample, location, area, method, device, screener, screeningResult, cctvCamera, isValid) {
        cell.progressOn();
        var grid = getActiveGrid();
        grid.clearAll();
        grid._loadedPages = [0];
        var url = getActiveToolbar().getItemState("viewByReferenceButton") ? '/LiveScreeningWebSpecific/GetGroupedScreeningTransactions' : '/LiveScreeningWebSpecific/GetScreeningTransactions';
        grid.loadExternal(url, {
            fromDate: dateFrom,
            toDate: dateTo,
            housebillNumber: shippingReference,
            sampleNumber: sample,
            locationId: location,
            warehouseLocationId: area,
            screeningMethodId: method,
            deviceId: device,
            resultCode: screeningResult,
            isValid: isValid
        }, function (data) {
            cell.progressOff();
            getActiveToolbar().disableItem("btnSelActions");
            getActiveToolbar().disableListOption("btnSelActions", "updateReferenceButton");
            getActiveToolbar().disableListOption("btnSelActions", "updateSlacButton");
        });
    }

    this.exportToExcel = function () {
        var downloadUrl = getActiveToolbar().getItemState("viewByReferenceButton") ? "/LiveScreeningWebSpecific/GroupedScreeningTransactionsExcelExport" : "/LiveScreeningWebSpecific/ScreeningTransactionsExcelExport";
        var exportExcelFilter = {
            fromDate: searchPanelForm.getCalendar('fromDateCalendar').getFormatedDate(),
            toDate: searchPanelForm.getCalendar('toDateCalendar').getFormatedDate(),
            housebillNumber: searchPanelForm.getItemValue('form_input_reference'),
            sampleNumber: searchPanelForm.getItemValue('form_input_sample'),
            locationId: searchPanelForm.getCombo('screeningLocations').getAllCheckedIds().join(),
            warehouseLocationId: searchPanelForm.getCombo('screeningAreas').getAllCheckedIds().join(),
            screeningMethodId: searchPanelForm.getCombo('screeningMethods').getAllCheckedIds().join(),
            deviceId: searchPanelForm.getCombo('screeningDevices').getAllCheckedIds().join(),
            resultCode: searchPanelForm.getCombo('screeningResults').getAllCheckedIds().join(),
            isValid: searchPanelForm.getItemValue('isValid') === 1 ? true : false
        }
        cell.progressOn();
        helpers.exportToExcel(downloadUrl, exportExcelFilter, "Screening Transactions", function () { cell.progressOff(); });
    }

    this._fixCalendar = function (name, panel) {
        var calendarTo = panel.getCalendar(name);
        var calendarToKey = Object.keys(calendarTo.i)[0];
        calendarTo.i[calendarToKey].input.value = window.dhx4.date2str(new Date(), '%m-%d-%Y');
        calendarTo.setDate(new Date());
    }

    this._clearCalendar = function (name, panel) {
        var calendarTo = panel.getCalendar(name);
        var calendarToKey = Object.keys(calendarTo.i)[0];
        calendarTo.i[calendarToKey].input.value = '';
        calendarTo.setDate(null);
    }

    this._filterScreeningDevices = function () {
        var screeningLocation = searchPanelForm.getCombo('screeningLocations').getAllCheckedIds().join();
        var screeningArea = searchPanelForm.getCombo('screeningAreas').getAllCheckedIds().join();
        var screeningMethod = searchPanelForm.getCombo('screeningMethods').getAllCheckedIds().join();
        var screeningDevices = searchPanelForm.getCombo('screeningDevices');
        screeningDevices.loadExternal("/LiveScreening/GetScreeningDeviceNames?locationId=" + screeningLocation + "&screeningMethodId=" +screeningMethod + "&screeningAreaId=" + screeningArea, 'Id', 'Name');        
    }

    this._filterScreeners = function () {
        var screeningLocation = searchPanelForm.getCombo('screeningLocations').getAllCheckedIds().join();
        var screeningMethod = searchPanelForm.getCombo('screeningMethods').getAllCheckedIds().join();
        $this._refreshComboExternal('screeners', "/LiveScreeningWebSpecific/GetScreeners?locationId=" + screeningLocation + "&screeningMethodId=" + screeningMethod);
    }

    this._refreshComboExternal = function (comboName, loadUrl) {
        var combo = searchPanelForm.getCombo(comboName);
        combo.clearAll();
        combo.setComboValue(null);
        combo.setComboText("");
        combo.loadExternal(loadUrl, "value", "text");
    }

    this.initGrid = function (grid, isWithReference) {
        //cell.detachObject(true);
        //grid = null;
        var currentToolbar = getActiveToolbar();
        
        grid = cell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        if (isWithReference) {
            grid.setHeader(["Reference#", "Method",  "Screened On", "CCSF", "Pieces", "SLAC", "SLAC Screened", "Result", "Validated?"]);
            grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,img");
            grid.setColSorting('str,str,str,str,str,str,str,str,str');
            grid.setInitWidths('200,180,120,65,52,52,100,90,100');
        } else {
            grid.setHeader(["Reference#", "Sample#", "Method", "Device ID", "Screened On", "CCSF", "Pieces", "SLAC", "SLAC Screened", "Result", "Screener", "Validated?"]);
            grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img");
            grid.setColSorting('str,str,str,str,str,str,str,str,str,str,str,str');
            grid.setInitWidths('130,220,60,65,110,65,52,52,65,52,62,65');
            
        }

        grid.attachEvent("onMouseOver", function (id, ind) {
            var cell = this.cells(id, ind).cell;
            var title = cell._brval || cell.innerText;
            
            if (title == "transparent.png")
                title = "Not Validated";
            else if (title == "green_chkmark.png")
                title = "Validated";
            
            cell.title = title;
        });

        grid.enableMultiselect(true);
        grid.showTotal();
        //grid.enableColumnMove(true);
        grid.init();
        grid.enableSmartRendering(true);
        grid.makePageable();

       
        grid.attachEvent("onRowDblClicked", function (rId, cInd) {
            if (cell.getViewName() == "normalView") {
                if (this.listener && rId) {
                    this.listener.call(null, rId);
                }
            } else {
                if (this.shipmentRowClickListener && rId) {
                     this.shipmentRowClickListener.call(null, grid.getUserData(rId, "CustomRowData"));
                }
            }
        }.bind(this));

        getActiveToolbar().disableItem("btnSelActions");

        grid.attachEvent("onSelectStateChanged", function (id) {
            if (cell.getViewName() == "normalView") {
                var selectedIds = grid.getSelectedIdsAsArray();
                getActiveToolbar().enableItem("btnSelActions");
                getActiveToolbar().enableListOption("btnSelActions", "updateReferenceButton");
                getActiveToolbar().enableListOption("btnSelActions", "updateSlacButton");

                selectedIds.forEach(function(id) {
                    if (grid.getUserData(id, "CustomRowData") !== null && grid.getUserData(id, "CustomRowData").data.indexOf("transparent.png") == -1) {
                        getActiveToolbar().disableListOption("btnSelActions", "updateReferenceButton");
                        getActiveToolbar().disableListOption("btnSelActions", "updateSlacButton");
                        return false;
                    }
                });
            } else {
                getActiveToolbar().disableItem("btnSelActions");
            }
        });

        currentToolbar.initQuikSearch(grid);
        return grid;
    }

    var initToolbar = function(toolbar) {
        toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>' +
            '<item type="buttonSelect" openAll="true" id="button_select_export" text="Download" title="" >' +
            '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
            '</item><item type="separator" id="button_separator_4" />' +
            '<item type="buttonSelect" openAll="true" id="btnSelActions" text="Actions" title="" >' +
            (userRole == 16? '<item type="button" id="clearAlarmButton" text="Clear Alarm" image="" />' : "") +
            (userRole == 16?'<item type="button" id="updateReferenceButton" text="Update Reference" image="" />' : "") +
            '<item type="button" id="addRemarkButton" text="Add Remark" image="" />' +
            (userRole == 16? '<item type="button" id="updateSlacButton" text="Update SLAC" image="" />' : "") +
            '</item>' +
            '<item type="separator" id="button_separator_5" />' +
            '<item type="buttonTwoState" id="viewByReferenceButton" text="VIEW BY REFERENCE" image="" />' +
            '<item type="separator" id="button_separator_4" />' +
            '<item type="button" id="liveScreeningButton" text="LIVE SCREENING" image="" />' +
            '</toolbar>', function () { });

        //toolbar.initQuikSearch(grid);
        if (cell.getViewName() != "normalView")
            toolbar.setItemState("viewByReferenceButton", true);
        else
            toolbar.setItemState("viewByReferenceButton", false);

        var commonFormStructureForActions = [
            {
                type: "block",
                blockOffset: 0,
                list: [
                    { type: "label", label: "Reference#:", labelHeight: 14 },
                    { type: "label", label: "Sample#:", labelHeight: 14 },
                    { type: "label", label: "Method:", labelHeight: 14 },
                    { type: "label", label: "Device ID:", labelHeight: 14 },
                    { type: "label", label: "Location:", labelHeight: 14 },
                    { type: "label", label: "Screener:", labelHeight: 14 },
                    { type: "newcolumn", offset: 10 },
                    { type: "label", name: "referenceValue", labelHeight: 14 },
                    { type: "label", name: "sampleNumberLabelValue", labelHeight: 14 },
                    { type: "label", name: "methodLabelValue", labelHeight: 14 },
                    { type: "label", name: "deviceLabelValue", labelHeight: 14 },
                    { type: "label", name: "locationLabelValue", labelHeight: 14 },
                    { type: "label", name: "screenerLabelValue", labelHeight: 14 }
                ]
            }, { type: "block", offsetTop: 10 }
        ];

        var addRemarkStructure = [
            {
                type: "input", name: "Content", label: "Remark:", labelAlign: "left", position: "label-top", labelWidth: 100, inputWidth: 417, required: true, rows: 5, maxLength: 500,
                validate: function (input) { return input && input.trim() !== '' ? true : false }
            }
        ];

        var clearAlarmStructure = [
            {
                type: "input", name: "Content", label: "Comment:", labelAlign: "left", position: "label-top", labelWidth: 100, inputWidth: 417, required: true, rows: 5, maxLength: 500,
                validate: function (input) { return input && input.trim() !== '' ? true : false }
            }
        ];

        var updateSlacStructure = [
            {
                type: "input", name: "Content", label: "Total SLAC:", labelAlign: "left", position: "label-top", labelWidth: 100, inputWidth: 417, required: true, maxLength: 4,
                validate: function (input) { return /^[0-9]+$/.test(input) && input > 0 && input < 10000; }
            }
        ];

        var updateReferenceStructure = [
            {
                type: "input", name: "Content", label: "Reference#:", labelAlign: "left", position: "label-top", labelWidth: 100, inputWidth: 417, required: true, maxLength: 20,
                validate: function (input) { return input && input.trim() !== '' ? true : false }
            }
        ];

        var confirmPopup;
        var _remiderTimer;

        function forceClose(interval) {
            var timer = setTimeout(function () {
                if (confirmPopup) {
                    confirmPopup.getElementsByClassName('dhtmlx_popup_button')[1].click();
                }
            }, interval * 1000);
            return timer;
        }

        function actionsImpl(dataStructure, type, formHeight, formDefaultHeight, formWidth, caption, postMethodName, successMessage, refreshPage) {
            var grid = getActiveGrid();
            var formStructure = null;
            var height = formDefaultHeight;
            var _submitForm = null;
            var selectedIds = grid.getSelectedIdsAsArray();

            if (selectedIds.length > 0) {
                if (selectedIds.length == 1) {
                    height = formHeight;
                    formStructure = commonFormStructureForActions.slice();
                    formStructure.push(dataStructure[0]);
                }
                _submitForm = new submitForm(caption, formStructure || dataStructure, function (formData) {
                    formData.Ids = selectedIds;
                    if (type) {
                        formData.Type = type;
                    }

                    helpers.executePost('/LiveScreening/' + postMethodName, formData, function (data) {
                        dhtmlx.alert({
                            title: "Success",
                            type: "warning",
                            text: successMessage
                        });
                        if (refreshPage) {
                            if (cell.getViewName() == "normalView")
                                $this.filter(grid);
                            else {
                                $this.filter(gridWithReference);
                            }
                        }
                        _submitForm.window.close();
                    });

                }, formWidth, height);
                _submitForm.form.enableLiveValidation(true);
            } else {
                dhtmlx.alert({
                    title: "Warning",
                    type: "warning",
                    text: "You need to select one or more Transactions!"
                });
            }

            if (_submitForm != null && selectedIds.length == 1) {
                var selectedItemData = grid.getUserData(selectedIds[0], "CustomRowData").data;
                _submitForm.form.setItemLabel("referenceValue", selectedItemData[0]);
                _submitForm.form.setItemLabel("sampleNumberLabelValue", selectedItemData[1]);
                _submitForm.form.setItemLabel("methodLabelValue", selectedItemData[2]);
                _submitForm.form.setItemLabel("deviceLabelValue", selectedItemData[3]);
                _submitForm.form.setItemLabel("locationLabelValue", selectedItemData[5]);
                _submitForm.form.setItemLabel("screenerLabelValue", selectedItemData[10]);
            }
        }

        toolbar.attachEvent("onStateChange", function (id, state) {
            if (id === "viewByReferenceButton") {
                if (state) 
                    cell.showView("normalViewRef");
                else 
                    cell.showView("normalView");
                toolbar.setItemState("viewByReferenceButton", !state);
            }
        });

        toolbar.attachEvent('onClick', function (id) {
            if (id === "addRemarkButton") {
                actionsImpl(addRemarkStructure, 3, 430, 240, 450, 'Add Remark', 'AddRemark', 'Remark has been added!', false);
            } else if (id === "updateSlacButton") {
                actionsImpl(updateSlacStructure, null, 360, 160, 450, 'Update SLAC', 'UpdateSlac', 'SLAC has been updated!', true);
            } else if (id === "clearAlarmButton") {
                actionsImpl(clearAlarmStructure, 3, 430, 240, 450, 'Clear Alarm', 'ClearAlarm', 'Alarm has been cleared!', true);
            } else if (id === "updateReferenceButton") {
                actionsImpl(updateReferenceStructure, null, 360, 160, 450, 'Update Reference', 'UpdateReference', 'Reference has been updated!', true);
            } else if (id === "btnExportExcel") {
                $this.exportToExcel();
            } else if (id === "liveScreeningButton") {
                helpers.getDataFromExternalResource('/LiveScreening/IsUserCertifiedforXray', null, function (data) {
                    if (data) {
                        var _enterToLiveScreening = new enterToLiveScreening();
                        _enterToLiveScreening.onSubmit = function (info) {
                            helpers.getDataFromExternalResource('/LiveScreening/AssignDeviceToUser', { deviceId: info.device.id }, function (data) {
                                console.log(info.device.id);

                                window._liveScreening = new liveScreening(info);
                                window._liveScreening.onClose = function () {
                                    clearTimeout(timer);
                                    clearTimeout(_remiderTimer);
                                }

                                window.signalingAdapter.setDevice(info.device.id);

                                _remiderTimer = window.helpers.reminder(data.SessionWorning, true, 'Your Session is about to expire. Do you want to prolong it?',
                                function () {
                                    clearTimeout(timer);
                                    timer = forceClose(data.SessionWorning + data.SessionForseClose);
                                },
                                function () {
                                    console.log('noCallback');
                                    clearTimeout(timer);
                                    clearTimeout(_remiderTimer);
                                    window._liveScreening.close();
                                    helpers.getDataFromExternalResource('/LiveScreening/DisconnectUserFromDevice', { deviceId: info.device.id }, function () { });
                                },
                                function (popupElement, remiderTimer) {
                                    confirmPopup = popupElement;
                                    _remiderTimer = remiderTimer;
                                });

                                var timer = forceClose(data.SessionWorning + data.SessionForseClose);
                            });
                        }
                    } else {
                        dhtmlx.alert({
                            text: "You don't have permission to use XRAY"
                        });

                    }
                });
            }
        });
        return toolbar;
    }

    var init = function () {
        cell.showView('normalViewRef');
        toolbarWithReference = initToolbar(toolbarWithReference);
        gridWithReference = $this.initGrid(gridWithReference, true);
        cell.showView('normalView');

        toolbar = initToolbar(toolbar);
        grid = $this.initGrid(grid);
        //$this.filter();

    }.apply(this);

    searchPanelForm = function () {
        cell.showView('normalView');
        var searchPanel = accordion.addItem('searchPanel', 'SCREENING TRANSACTIONS');
        var searchFormStructure = [
                {
                    type: "input", name: "form_input_reference", label: "Reference#:", position: "label-left", labelWidth: 65, inputWidth: 210, className: "req-asterisk", required: true,
                    validate: function (input) { return input && input.trim() != ""; }
                },
                {
                    type: "input", name: "form_input_sample", label: "Sample#:", position: "label-left", labelWidth: 65, inputWidth: 210, className: "req-asterisk", required: true,
                    validate: function (input) { return input && input.trim() != ""; }
                },
                {
                    type: "calendar", name: "fromDateCalendar", label: "From Date:", position: "label-left", labelWidth: 65, dateFormat: "%m-%d-%Y", inputWidth: 210, className: "req-asterisk", required: true,
                    validate: function (input) { return input; }
                },
                { type: "calendar", name: "toDateCalendar", label: "To Date:", position: "label-left", labelWidth: 65, dateFormat: "%m-%d-%Y", inputWidth: 210 },
                { type: "combo", name: "screeningLocations", comboType: "checkbox", label: "CCSF:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                { type: "combo", name: "screeningMethods", comboType: "checkbox", label: "Method:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                { type: "combo", name: "screeningDevices", comboType: "checkbox", label: "Device:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                { type: "combo", name: "screeners", comboType: "checkbox", label: "Screener:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                { type: "combo", name: "screeningResults", comboType: "checkbox", label: "Result:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                { type: "combo", name: "screeningAreas", comboType: "checkbox", label: "Area:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                { type: "combo", name: "cctvCameras", comboType: "checkbox", label: "Camera:", position: "label-left", labelWidth: 65, inputWidth: 210 },
                {
                    type: "block", name: "form_block_1", list: [
                    { type: "checkbox", name: "isValid", label: "List only validated shipments", position: "label-right" }]
                },
                { type: "block", list: [{ type: "button", name: "resetButton", value: "RESET", offsetLeft: 80 }, { type: "newcolumn" }, { type: "button", name: "filterButton", value: "SEARCH" }] }
        ];

        var searchPanelForm = searchPanel.attachForm(searchFormStructure);

        $this._fixCalendar('fromDateCalendar', searchPanelForm);
        $this._fixCalendar('toDateCalendar', searchPanelForm);

        var screeningLocations = searchPanelForm.getCombo('screeningLocations');
        screeningLocations.makeMultiSelect();

        var screeningMethods = searchPanelForm.getCombo('screeningMethods');
        screeningMethods.makeMultiSelect();

        var screeningDevices = searchPanelForm.getCombo('screeningDevices');
        screeningDevices.makeMultiSelect();

        var screeners = searchPanelForm.getCombo('screeners');
        screeners.makeMultiSelect();

        var screeningResults = searchPanelForm.getCombo('screeningResults');
        screeningResults.makeMultiSelect();

        var screeningAreas = searchPanelForm.getCombo('screeningAreas');
        screeningAreas.makeMultiSelect();

        var cctvCameras = searchPanelForm.getCombo('cctvCameras');
        cctvCameras.makeMultiSelect();

        screeningDevices.loadExternal('/LiveScreening/GetScreeningDeviceNames', 'Id', 'Name');

        screeningResults.loadExternal('/LiveScreening/GetScreeningResults', 'Id', 'Name');

        screeningMethods.loadExternal('/LiveScreening/GetScreeningMethods', 'Id', 'Name');

        cctvCameras.loadExternal('/LiveScreening/GetCctvCameras', 'Id', 'Name');

        screeningLocations.loadExternal('/LiveScreeningWebSpecific/GetScreeningLocations', 'value', 'text', function (data) {
            if (data.length === 1) {
                screeningLocations.setComboValue(data[0].value);
                screeningLocations.setComboText(data[0].text);
                screeningLocations.setChecked(0, true);
                $this._refreshComboExternal('screeningAreas', "/LiveScreeningWebSpecific/GetWarehouseLocations?locationId=" + data[0].value);
                cctvCameras.loadExternal('/LiveScreening/GetCctvCameras?locationId=' + data[0].value, 'Id', 'Name');
            }
        });

        screeners.loadExternal('/LiveScreeningWebSpecific/GetScreeners', 'value', 'text');
        
        screeningAreas.loadExternal('/LiveScreeningWebSpecific/GetWarehouseLocations', 'value', 'text');

        screeningLocations.attachEvent("onChange", function (value) {
            value = screeningLocations.getAllCheckedIds().join();
            $this._refreshComboExternal('screeningAreas', "/LiveScreeningWebSpecific/GetWarehouseLocations?locationId=" + value);
            cctvCameras.loadExternal('/LiveScreening/GetCctvCameras?locationId=' + value, 'Id', 'Name');
            $this._filterScreeningDevices();
            $this._filterScreeners();
        });

        var customFilter = function (mask, option) {
            var r = false;
            if (mask.length == 0) {
                r = true;
            } else if (option.text.toLowerCase().indexOf(mask.toLowerCase()) !== -1) {
                r = true;
            }
            return r;
        };

        screeningLocations.setFilterHandler(customFilter);
        
        screeningMethods.attachEvent("onChange", function (value) {
            $this._filterScreeningDevices();
        });
        screeningMethods.setFilterHandler(customFilter);
        
        screeningDevices.setFilterHandler(customFilter);
       
        screeners.setFilterHandler(customFilter);

        screeningAreas.attachEvent("onChange", function (value) {
            $this._filterScreeningDevices();
        });

        searchPanelForm.attachEvent("onButtonClick", function (name) {
            if (name == 'filterButton') {
                if(cell.getViewName()=="normalView")
                    $this.filter(grid);
                else {
                    $this.filter(gridWithReference);
                }
            } else {
                $this.resetControls();
                $this._fixCalendar('fromDateCalendar', searchPanelForm);
                $this._fixCalendar('toDateCalendar', searchPanelForm);
            }
        });

        return searchPanelForm;
    }();
    
    this._filter(window.dhx4.date2str(new Date(), '%m-%d-%Y'), window.dhx4.date2str(new Date(), '%m-%d-%Y'));
    return this;
};