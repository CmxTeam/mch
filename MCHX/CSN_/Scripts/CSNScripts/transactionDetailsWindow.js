﻿var transactionDetailsWindow = function (imagePath) {        
    var $this = this;
    
    this.show = function (id) {
        $this.grid.clearAll();
        $this.viewData.clearAll();

        helpers.getDataFromExternalResource('/LiveScreening/GetTransactionDetails', { id:id }, function(data) {
            var formData = data;
            $this.window.headerInfoForm.setItemLabel('ShipmentReference', formData.ShipmentReference);
            $this.window.headerInfoForm.setItemLabel('ScreenedPieces', formData.ScreenedPieces + ' of ' + formData.TotalPieces);
            $this.window.headerInfoForm.setItemLabel('Validated', formData.Validated);
            $this.window.headerInfoForm.setItemLabel('Status', formData.ScreenedPieces < formData.TotalPieces ? 'Screening In Progress' : 'Screening Completed');

            $this.window.screeningDetailsForm.setItemLabel('sampleNumberLabelValue', formData.SampleNumber);
            $this.window.screeningDetailsForm.setItemLabel('methodLabelValue', formData.Method);
            $this.window.screeningDetailsForm.setItemLabel('deviceLabelValue', formData.Device);
            $this.window.screeningDetailsForm.setItemLabel('screenerLabelValue', formData.Screener);
            $this.window.screeningDetailsForm.setItemLabel('piecesLabelValue', formData.ScreenedPieces);
            $this.window.screeningDetailsForm.setItemLabel('resultLabelValue', formData.Result);
            $this.window.screeningDetailsForm.setItemLabel('timestampLabelValue', window.dhx4.date2str(new Date(formData.Timestamp), '%m-%d-%Y %H:%i:%s'));
            $this.window.screeningDetailsForm.setItemLabel('locationLabelValue', formData.Location);
            $this.window.screeningDetailsForm.setItemLabel('commentLabelValue', formData.Comment);

            helpers.getDataFromExternalResource('/LiveScreening/GetImagesForTransaction', { id: id, method: formData.Method }, function (data) {
                $this.viewData.parse(data, "json");
            });

            $this.remarksCell.progressOn();
            $this.grid.loadExternal('/LiveScreeningWebSpecific/GetShipmentRemarks', {id: id}, function () { $this.remarksCell.progressOff(); });
        });
        
        this.window.setModal(1);
        this.window.show();
    }.bind(this);

    this.hide = function () {
        this.window.hide();
        this.window.setModal(0);
    }.bind(this);

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('transactionDetailsWindow', 0, 0, 900, 650);

        var layout = window.attachLayout('4I');

        var headerInfoCell = layout.cells('a');
        headerInfoCell.setHeight('50');
        headerInfoCell.hideHeader();
        headerInfoCell.fixSize(0, 1);

        var headerInfoFormStructure = [
		    { type: "label", label: "Shipping Reference#:" },
		    { type: "newcolumn" },
		    { type: "label", name: "ShipmentReference", labelWidth: 150 },
            { type: "newcolumn" },           
            { type: "label", label: "Screened Pieces#:" },
            { type: "newcolumn" },
            { type: "label", name: "ScreenedPieces", labelWidth: 75 },
            { type: "newcolumn" },           
            { type: "label", label: "Validated?" },
            { type: "newcolumn" },
            { type: "label", name: "Validated", labelWidth: 40 },
            { type: "newcolumn" },
            { type: "label", label: "Status:" },
            { type: "newcolumn" },
            { type: "label", name: "Status" }
        ];
        
        window.headerInfoForm = headerInfoCell.attachForm(headerInfoFormStructure);        
        
        var screeningDetailsCell = layout.cells('b');
        screeningDetailsCell.setText('SCREENING DETAILS');
        screeningDetailsCell.fixSize(2, 0);
        screeningDetailsCell.setWidth(500);
        var screeningDetailsFormStructure = [
		{
		    type: "block", list: [
            { type: "label", label: "Sample#:", labelHeight: 14 },
            { type: "label", label: "Method:", labelHeight: 14 },
            { type: "label", label: "Device#:", labelHeight: 14 },
            { type: "label", label: "Screener:", labelHeight: 14 },
            { type: "label", label: "SLAC Screened:", labelHeight: 14 },
            { type: "label", label: "Result:", labelHeight: 14 },
            { type: "label", label: "Timestamp:", labelHeight: 14 },
            { type: "label", label: "Location:", labelHeight: 14 },
            { type: "label", label: "Comment from Device:", labelHeight: 14 }
		    ]
		},
		{ type: "newcolumn" },
		{
		    type: "block", list: [
            { type: "label", name: "sampleNumberLabelValue", labelHeight: 14},
            { type: "label", name: "methodLabelValue", labelHeight: 14 },
            { type: "label", name: "deviceLabelValue", labelHeight: 14 },
            { type: "label", name: "screenerLabelValue", labelHeight: 14 },
            { type: "label", name: "piecesLabelValue", labelHeight: 14 },
            { type: "label", name: "resultLabelValue", labelHeight: 14 },
            { type: "label", name: "timestampLabelValue", labelHeight: 14 },
            { type: "label", name: "locationLabelValue", labelHeight: 14 },
            { type: "label", name: "commentLabelValue", labelHeight: 14 }
		    ]
		}
        ];
        window.screeningDetailsForm = screeningDetailsCell.attachForm(screeningDetailsFormStructure);

        var imagesCell = layout.cells('c');
        imagesCell.setText('IMAGES');
        imagesCell.fixSize(1, 0);

        var mapItemTemplate = "<div>" +
            "<img style='max-width:400px; max-height:135px;' src='" + imagePath + "#Name#' />" +
            "</div>";

        this.viewData = imagesCell.attachDataView({
            type: {
                template: mapItemTemplate,
                height: 135,
                width: 400
            }
        });
        
        this.viewData.attachEvent("onItemDblClick", function (id, ev, html) {
            var imageSource = imagePath + $this.viewData.get(id).Name;
            var img = new Image();

            img.onload = function () {
                if (this.naturalHeight > document.body.clientHeight || this.naturalWidth > document.body.clientWidth)
                    helpers.popupImage(imageSource, document.body.clientHeight, document.body.clientWidth, true);
                else 
                    helpers.popupImage(imageSource, this.naturalHeight + 60, this.naturalWidth + 60);
            }

            img.src = imageSource;
        });

        this.remarksCell = layout.cells('d');
        this.remarksCell.setText('REMARKS');

        var grid = this.remarksCell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["User", "Timestamp", "Remark"]);
        grid.setColTypes("ro,ro,ro");
        grid.setColSorting('str,str,str');
        grid.setInitWidths('200,200,*');
        grid.enableMultiline(true);
        grid.init();        
        this.grid = grid;

        window.button('close').attachEvent('onClick', function () {
            window.setModal(0);
            window.hide();
        });

        window.setText('TRANSACTION DETAILS');
        window.hide();
        window.centerOnScreen();
        window.button('minmax').show();
        window.button('minmax').enable();

        this.window = window;
    }.apply(this);    
};