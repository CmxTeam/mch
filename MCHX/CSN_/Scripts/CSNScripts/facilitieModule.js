﻿var facilitieModule = function (cell, accordion) {
    var grid, tree, formStructure, isLatitude, isLongitute, fcSubmitForm, treeSelectedId;
    var $this = this;

    var selectedFacilitieId = function() {
        var itemId = grid.getSelectedRowId() || tree.getSelectedItemId() || null;
        return $this.isNumeric(itemId) ? itemId : null;
    };

    var treeToolbar = null;

    this.show = function () {
        cell.setText('FACILITIES');
        cell.showView('facilitiesGridContainer');
    }

    this.filter = function (facilitie) {
        cell.progressOn();
        grid.clearAll();
        grid.loadExternal('/LiveScreeningWebSpecific/GetFacilitiesGrid', { id: facilitie }, function () { cell.progressOff(); })        
    }

    this.isNumeric = function (input) {
        return (input - 0) == input && ('' + input).trim().length > 0;
    }

    isLatitude = function (input) {
        return $this.isNumeric(input) && input !== "" && input > -90 && input < 90;
    }

    isLongitute = function (input) {
        return $this.isNumeric(input) && input !== "" && input > -180 && input < 180;
    }

    var createFacilityFormInit = function (id) {

        fcSubmitForm = new submitForm("Edit Facility", formStructure, function (formData) {
            helpers.executePost('/LiveScreening/CreateFacility', formData, function (data) {
                fcSubmitForm.window.close();

                var filterValue = treeToolbar.getValue("txtFilterFacility");
                $this.filter(tree.getSelectedItemId() || "rootElement");
                tree.loadExternal('/LiveScreeningWebSpecific/GetFacilitiesTree', { filter: filterValue }, function () {
                });
            });
        }, 400, 480);

        var cbxCountry = fcSubmitForm.form.getCombo('CountryId');
        var cbxState = fcSubmitForm.form.getCombo('StateId');
        var cbxManager = fcSubmitForm.form.getCombo('ManagerId');
        var cbxCCSF = fcSubmitForm.form.getCombo('CCSFId');

        fcSubmitForm.form.attachEvent('onChange', function (id, value) {
            if (id === "CountryId") {
                cbxState.unSelectOption();
                cbxState.clearAll();
                if (value)
                    cbxState.loadExternal('/LiveScreening/GetStates?countryId=' + value, 'Id', 'StateProvince');
            }
        });

        if (id === "btnAddFacility") {
            cbxCountry.loadExternal('/LiveScreening/GetCountries', 'Id', 'CountryName');
            cbxManager.loadExternal('/LiveScreening/GetManagers', 'Id', 'Name');
            cbxCCSF.loadExternal('/LiveScreening/GetCertifiedScreeningFacilities', 'Id', 'TSAApprovalNumber');
        } else if (id === "btnEditFacility") {

            fcSubmitForm.form.setReadonly("Location", true);

            helpers.getDataFromExternalResource('/LiveScreening/GetFacility', { id: selectedFacilitieId() }, function (data) {
                console.log(data);
                fcSubmitForm.form.setFormData(data);
                if (data.CountryId) {
                    cbxCountry.loadExternal('/LiveScreening/GetCountries', 'Id', 'CountryName', function () {
                        cbxCountry.selectOption(cbxCountry.getIndexByValue(data.CountryId));
                        cbxState.loadExternal('/LiveScreening/GetStates?countryId=' + data.CountryId, 'Id', 'StateProvince', function () {
                            cbxState.selectOption(cbxState.getIndexByValue(data.StateId));
                        });
                    });
                }

                cbxManager.loadExternal('/LiveScreening/GetManagers', 'Id', 'Name', function () {
                    cbxManager.selectOption(cbxManager.getIndexByValue(data.ManagerId));
                });
                cbxCCSF.loadExternal('/LiveScreening/GetCertifiedScreeningFacilities', 'Id', 'TSAApprovalNumber', function () {
                    cbxCCSF.selectOption(cbxCCSF.getIndexByValue(data.CCSFId));
                });
            });
        } 

        fcSubmitForm.form.enableLiveValidation(true);
        return fcSubmitForm;
    }

    grid = function () {
        cell.showView('facilitiesGridContainer');

        var grid = cell.attachGrid();
        grid.setIconsPath(dhtmlx.image_path);
        grid.setHeader(["Id", "Location", "Address", "CCSF#", "Manager Name", "Email",
            "Work Phone", "Mobile Phone", "Status", "Total Devices", "Total Screeners"]);
        grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
        grid.setColSorting('str,str,str,str,str,str,str,str,str,str,str');
        grid.setInitWidths('70,100,100,100,100,100,100,100,70,100,*');
        grid.setColumnHidden(0, true);
        grid.showTotal();
        grid.init();

        var toolbar = cell.attachToolbar();
        toolbar.setIconsPath(dhtmlx.image_path);
        toolbar.loadStruct('<toolbar>' +
            '<item type="buttonSelect" id="btnAddEditSelect" openAll="true" text="Actions" title="" >' +
            //'<item type="button" id="btnAddFacility" hidden="true" text="Add" title="" />' +
            '<item type="button" id="btnEditFacility" text="Edit" title="" />' +
            //'<item type="button" id="btnUploadTSA" text="Upload TSA Master List" title="" />' +
            '</item>' +
            '<item type="buttonSelect" openAll="true" id="btnDownload" text="Download" title="" >' +
            '<item type="button" id="btnExportExcel" text="Excel" image="" />' +
            '</item>' +
            '</toolbar>', function () { });

        toolbar.initQuikSearch(grid);

        toolbar.attachEvent('onClick', function (id) {
            if (id === "btnAddFacility" || id === "btnEditFacility") {
                if (id === "btnEditFacility" && selectedFacilitieId() == null) {
                    dhtmlx.alert({
                        title: "Warning",
                        type: "warning",
                        text: "You need to select Location"
                    });
                } else {
                    fcSubmitForm = createFacilityFormInit(id);
                }
            }else if (id === "btnUploadTSA") {
                var formStructure = [
                        {
                            type: "template", label: "Attachment File:", inputWidth: 444, name: "AttachmentFile",
                            position: "label-top", labelWidth: 150, className: "req-asterisk", format: helpers.uploaderFormTemplate, required: true
                        }
                     ];
                var uploadTsaForm = new submitForm("Upload TSA Master List", formStructure, function(formData) {
                    helpers.executePost('/LiveScreening/UploadTsaMasterList', { Id: 0, Name: formData.AttachmentFile }, function () {

                    });
                    // formData.AttachmentFile is selected Attachment file base64
                    uploadTsaForm.window.close();
                }, 489, 200, "SAVE");
            } else if ("btnDownload") {
                cell.progressOn();
                helpers.exportToExcel("/LiveScreeningWebSpecific/ExportFacilities", { id: treeSelectedId || "rootElement" }, "Facilities", function () { cell.progressOff(); });
            }
        });

        return grid;
    }();

    formStructure = [
            { type: "hidden", name: "Id" },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "Location", label: "Location:", labelWidth: 120, position: "label-left", maxLength: 3, inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "Name", label: "Name:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "AddressLine", label: "Address Line:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "CountryId", label: "Country:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true, filtering: true,
                        validate: function (input) { return fcSubmitForm.form.getCombo("CountryId").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "StateId", label: "State:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true, filtering: true,
                        validate: function (input) { return fcSubmitForm.form.getCombo("StateId").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "City", label: "City:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "PostalCode", label: "Postal Code:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true,
                        validate: function (input) { return input && input.trim() !== '' ? true : false }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "input", name: "Latitude", label: "Latitude:", labelWidth: 120, position: "label-left", inputWidth: 200, validate: isLatitude, required: true }
                ]
            },
            {
                type: 'block', list: [
                    { type: "input", name: "Longitude", label: "Longitude:", labelWidth: 120, position: "label-left", inputWidth: 200, validate: isLongitute, required: true }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "CCSFId", label: "CCSF:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true, filtering: true,
                        validate: function (input) { return fcSubmitForm.form.getCombo("CCSFId").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "ManagerId", label: "Manager:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true, filtering: true,
                        validate: function (input) { return fcSubmitForm.form.getCombo("ManagerId").getSelectedIndex() !== -1 }
                    }
                ]
            },
            {
                type: 'block', list: [
                    {
                        type: "combo", name: "StatusId", readonly: true, label: "Status:", labelWidth: 120, position: "label-left", inputWidth: 200, required: true,
                        options: [
                            { value: 520, text: "Active" },
                            { value: 521, text: "InActive" }
                        ]
                    }
                ]
            }
    ];

    tree = function () {        
        var facilitiesContainer = accordion.addItem('facilitiesContainer', 'FACILITIES');
        treeToolbar = facilitiesContainer.attachToolbar();
        treeToolbar.setIconsPath(dhtmlx.image_path);
        treeToolbar.loadStruct('<toolbar><item type="buttonInput" id="txtFilterFacility" value="">' +
            '</item>' +
            '</toolbar>', function () { });

        treeToolbar.getInput("txtFilterFacility").placeholder = "Find";
        var tree = facilitiesContainer.attachTree();

        tree.setImagePath(dhtmlx.tree_image_path);
        tree.loadExternal('/LiveScreeningWebSpecific/GetFacilitiesTree');

        tree.attachEvent("onClick", function (id) {
            $this.filter(id);
            treeSelectedId = id;
        });

        treeToolbar.attachEvent('onEnter', function(name, value) {
            tree.loadExternal('/LiveScreeningWebSpecific/GetFacilitiesTree', { filter: value });
        });

        tree.attachEvent("onXLE", function () {
            if (tree.getIndexById("rootElement") != -1)
                tree.openItem("rootElement");
            if (tree.getIndexById(treeSelectedId) != -1)
                tree.selectItem(treeSelectedId);
        });
        
        return tree;
    }();

    this.filter();
    return this;
};