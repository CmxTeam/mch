﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CSN_.Startup))]
namespace CSN_
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
