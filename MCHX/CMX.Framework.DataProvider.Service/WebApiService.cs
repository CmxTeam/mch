﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.Service
{
    public partial class WebApiService : ServiceBase
    {
        private static IDisposable _signalR;
        private static IDisposable _api; 

        public WebApiService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var webApiStartupOptions = new StartOptions();
            var signalRStartupOptions = new StartOptions();

            var webApiAddresses = ConfigurationManager.AppSettings["WebApiUrl"];
            var signalrAddresses = ConfigurationManager.AppSettings["SignalRUrl"];

            foreach (var address in webApiAddresses.Split(';'))
                webApiStartupOptions.Urls.Add(address);

            foreach (var address in signalrAddresses.Split(';'))
                signalRStartupOptions.Urls.Add(address);

            WebApiStartup.SwaggerConfig = c =>
            {
                c.SingleApiVersion("v1", "CMX API");
                c.UseFullTypeNameInSchemaIds();
                c.ResolveConflictingActions(api => api.First());
            };

            _api = WebApp.Start<WebApiStartup>(webApiStartupOptions);
            _signalR = WebApp.Start<Startup>(signalRStartupOptions);
        }

        protected override void OnStop()
        {
            _signalR.Dispose();
            _api.Dispose();
        }
    }
}
