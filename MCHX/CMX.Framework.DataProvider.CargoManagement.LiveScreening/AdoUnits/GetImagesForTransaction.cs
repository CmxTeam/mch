﻿using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    class GetImagesForTransaction : CmCoreDatabaseConnector<IEnumerable<ScreeningImageModel>>
    {
        public GetImagesForTransaction(long? screeningId = null)
            : base(new CommandParameter("@ScreeningId", screeningId))
        {
        }
        public override IEnumerable<ScreeningImageModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(r => new ScreeningImageModel 
            {
                Name = r["ImagePath"].ToString()
            });
        }
        public override string CommandQuery
        {
            get { return @"Select ImagePath from fsp.ScreeningImages where ScreeningId = @ScreeningId or @ScreeningId is null"; }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
