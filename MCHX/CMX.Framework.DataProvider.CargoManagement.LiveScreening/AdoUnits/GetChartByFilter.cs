﻿using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public abstract class GetChartByFilter<T> : CmCoreDatabaseConnector<T>
    {
        protected GetChartByFilter(long? userId = null, string locations = null, DateTime? dateFrom = null,
                                    DateTime? dateTo = null)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DateFrom", dateFrom),
                   new CommandParameter("@DateTo", dateTo), new CommandParameter("@Location", locations))
        {
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }
    }
}
