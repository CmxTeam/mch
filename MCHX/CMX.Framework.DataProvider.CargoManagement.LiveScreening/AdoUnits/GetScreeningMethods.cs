﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetScreeningMethods : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public GetScreeningMethods()
        {
        }
        public override IEnumerable<IdNameEntity> ObjectInitializer(List<System.Data.DataRow> rows)
        {
           if(rows == null || rows.Count == 0) return null;
           return rows.Select(s => new IdNameEntity 
           {
               Id = s["RecId"].TryParse<long>(),
               Name = s["Code"].ToString()
           });
        }
        public override string CommandQuery
        {
            get { return @"Select RecId, Code from ScreeningMethods where IsEnabled = 1 Order By Code"; }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
