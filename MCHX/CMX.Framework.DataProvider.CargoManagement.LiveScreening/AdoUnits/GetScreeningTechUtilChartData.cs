﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetScreeningTechUtilChartData : GetScreeningChartData
    {
        public GetScreeningTechUtilChartData(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(userId, locations, dateFrom, dateTo)
        {
           
        }

        public override string CommandQuery
        {
            get { return "GetScreeningTechUtilChart"; }
        }
    }
}
