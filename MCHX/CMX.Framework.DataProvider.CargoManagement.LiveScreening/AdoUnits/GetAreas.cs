﻿using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetAreas : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public GetAreas(int? locationId = null)
            : base(new CommandParameter("@LocationId", locationId))
        {

        }
        public override IEnumerable<IdNameEntity> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(s => new IdNameEntity 
            {
                Id = s["RecId"].TryParse<int>(),
                Name = s["ScreeningArea"].ToString()
            });
        }
        public override string CommandQuery
        {
            get
            {
                return @"Select Warehouses.Code + ' - ' + WL.Name AS ScreeningArea, WL.RecId from WarehouseLocations WL
                    left Join Warehouses On Warehouses.RecId = WL.WarehouseId
                    where WL.TypeId = 16 AND (@LocationId is null or 
                    Warehouses.GatewayId in (Select EntityId From HostPlus_Locations where RecId = @LocationId))
                    Order By ScreeningArea";
            }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
