﻿using System;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetScreeningTechUtilChart : GetScreeningChart
    {
        public GetScreeningTechUtilChart(long? userId = null, string locations = null,
                                        DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(userId, locations, dateFrom, dateTo)
        {
        }

        public override string CommandQuery
        {
            get { return  "GetScreeningTechUtilChart" ; }
        }
    }
}
