﻿using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetShipmentRemarks : CmCoreDatabaseConnector<IEnumerable<ShipmentRemarkModel>>
    {
        public GetShipmentRemarks(long? id = null,string sampleNumber = null,string reference = null) 
            : base(new CommandParameter("@Id", id), 
                   new CommandParameter("@SampleNumber",sampleNumber),
                   new CommandParameter("@Reference",reference))
        {
        }
        public override IEnumerable<ShipmentRemarkModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(s => new ShipmentRemarkModel 
            {
                Id = s["RecId"].TryParse<long>(),
                Description = s["Description"].ToString(),
                Date = s["RecDate"].TryParse<DateTime>(),
                User = s["UserId"].ToString(),
                SampleNumber = s["SampleNumber"].ToString()
            });
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select Screening.SampleNumber,UPPER(Remarks.UserId) as UserId, Remarks.RecDate, Remarks.Description, Remarks.RecId from Remarks 
                    Inner join fsp.Queue as Q On 
	                    (((Origin + '-' + Q.HousebillNo + '-' + Destination) = Remarks.Reference) 
	                    OR (('???-' + Q.HousebillNo + '-???') = Remarks.Reference))
                    Inner join Fsp.Screening as Screening ON Q.HousebillNo = Screening.HousebillNumber
                    where (Screening.RecId = @Id or @Id is null)
                    and (@SampleNumber is null or Screening.SampleNumber = @SampleNumber)
                    and (@Reference is null or Screening.HousebillNumber = @Reference)
                    Order By Remarks.RecDate desc";
            }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
