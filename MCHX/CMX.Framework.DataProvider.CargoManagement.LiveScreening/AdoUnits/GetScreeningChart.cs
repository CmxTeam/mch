﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public abstract class GetScreeningChart : GetChartByFilter<IEnumerable<Dictionary<string, string>>>
    {
        protected GetScreeningChart(long? userId = null, string locations = null, DateTime? dateFrom = null,
            DateTime? dateTo = null) : base(userId, locations, dateFrom, dateTo)

        {
            
        }
        public override IEnumerable<Dictionary<string, string>> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            List<Dictionary<string, string>> chartResult = new List<Dictionary<string, string>>();
            foreach (var r in rows)
            {
                var hourWithLocation = new Dictionary<string, string>();
                for (int col = 0; col < r.Table.Columns.Count; col++)
                    hourWithLocation.Add(r.Table.Columns[col].ToString(), DBNull.Value.Equals(r[col]) ? "0" : r[col].ToString());
                chartResult.Add(hourWithLocation);
            }
            return chartResult;
        }
    }
}
