﻿using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetCertifiedScreeningFacilities : CmCoreDatabaseConnector<IEnumerable<CertifiedScreeningFacilitieModel>>
    {
        public GetCertifiedScreeningFacilities(long? id = null)
            : base(new CommandParameter("@Id", id))
        {            
        }
        public override string CommandQuery
        {
            get
            {
                return @"select ccsf.RecId, ccsf.Name,ccsf.TSAApprovalNumber from TSA_Approved_Certified_Screening_Facilities as ccsf where (@Id is null OR ccsf.RecId = @Id)";
            }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override IEnumerable<CertifiedScreeningFacilitieModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;

            return rows.Select(s => new CertifiedScreeningFacilitieModel
            {
                Id = s["RecId"].TryParse<long>(),
                Name = s["Name"].ToString(),
                TSAApprovalNumber = s["TSAApprovalNumber"].ToString()
            });
        }
    }
}
