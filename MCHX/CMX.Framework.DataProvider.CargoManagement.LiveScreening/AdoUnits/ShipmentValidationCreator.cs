﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class ShipmentValidationCreator : CmCoreDatabaseConnector<ShipmentValidationResultModel>
	{		
        private string _connectionString;
		public ShipmentValidationCreator(string connectionString, long userId, long locationId, string number, string origin, string destination, int pieces, int slac)
            :base(new CommandParameter("@Origin", origin),
                    new CommandParameter("@Destination", destination))
        {
            _connectionString = connectionString;            
        }

		public override string CommandQuery
		{
			get 
            {				
				return @"SELECT CAST(CASE WHEN (Select COUNT(1) From CMXGLOBALDB.dbo.Ports where IataCode = @Origin)>0 THEN 1 ELSE 0 END AS BIT) AS IsOriginValid, CAST(CASE WHEN (Select COUNT(1) From CMXGLOBALDB.dbo.Ports where IataCode = @Destination)>0 THEN 1 ELSE 0 END AS BIT) AS IsDestinationValid";				
			}
		}

		public override string ConnectionString
		{
			get { return _connectionString; }
		}

		public override ShipmentValidationResultModel ObjectInitializer(List<System.Data.DataRow> rows)
		{
			return rows.Select(r => new ShipmentValidationResultModel
			{
				IsOriginValid = (bool)r["IsOriginValid"],
				IsDestinationValid = (bool)r["IsDestinationValid"]
			}).FirstOrDefault();
		}		
	}
}