﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class CompleteScreeningCreator : CmCoreDatabaseConnector<bool>
    {
        public CompleteScreeningCreator(long deviceId)
            : base(new CommandParameter("@DeviceId", deviceId))
        {         
        }

        public override string CommandQuery
        {
            get { return "Delete from fsp.ScreeningQueue Where DeviceId = @DeviceId"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return true;
        }
    }
}
