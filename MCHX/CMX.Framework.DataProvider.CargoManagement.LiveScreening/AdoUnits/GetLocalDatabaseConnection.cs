﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetLocalDatabaseConnection : CmCoreDatabaseConnector<string>
    {
        public GetLocalDatabaseConnection(long location)
            : base(new CommandParameter("@Location", location))
        {
        }

        public override string CommandQuery
        {
            get { return @"Select top 1 Connection From HostPlus_locations where RecId = @Location"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override string ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0) return null;
            return rows[0][0].ToString();
        }
    }
}