﻿using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetCctvCameras : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public GetCctvCameras(string locationId = null)
            : base(new CommandParameter("@LocationId", locationId))
        {

        }
        public override IEnumerable<IdNameEntity> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(s => new IdNameEntity 
            {
                Id = s["RecId"].TryParse<long>(),
                Name = s["DeviceName"].ToString()
            });
        }
        public override string CommandQuery
        {
            get
            {
                return @"Select ND.RecId, HL.Location + '-' + Name As DeviceName from NetworkDevices ND
                        left join HostPlus_Locations HL On HL.RecId = ND.LocationId
                        Where ND.IsActive = 1 and ND.DeviceTypeId 
                              in (Select RR.TypeId From [References] RR Where RR.ReferenceType = 'Network Devices' and RR.Code = 'CCTV') 
                        and (@LocationId is null or ND.LocationId in (select * from dbo.Split(@LocationId,default)))
                        Order By DeviceName";
            }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
