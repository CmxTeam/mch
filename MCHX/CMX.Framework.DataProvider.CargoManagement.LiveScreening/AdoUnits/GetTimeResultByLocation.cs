﻿using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetTimeResultByLocation : CmCoreDatabaseConnector<IEnumerable<ScreeningChartModel>>
    {
        public GetTimeResultByLocation(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DateFrom", dateFrom),
                   new CommandParameter("@DateTo", dateTo), new CommandParameter("@Location", locations))
        {
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string CommandQuery
        {
            get { return "GetHourlyResultByLocation"; }
        }
        public override IEnumerable<ScreeningChartModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(s => new ScreeningChartModel
            {
                DateCount = s["Pieces"].TryParse<long>(),
                Name = s["LabelName"].ToString(),
                StationName = s["Method"].ToString(),
                Date = s["Date"].TryParse<DateTime>()
            }).ToList();
        }
    }
}
