﻿using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public abstract class GetScreeningChartData : GetChartByFilter<IEnumerable<Chart3dStepModel>> 
    {
        protected GetScreeningChartData(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(userId, locations, dateFrom, dateTo)
        {
        }

        public override IEnumerable<Chart3dStepModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            List<Chart3dStepModel> chartResult = new List<Chart3dStepModel>();
            foreach (var r in rows)
            {
                for (int col = 1; col < r.Table.Columns.Count; col++)
                {
                    var dbRow = new Chart3dStepModel();
                    dbRow.zValue = r[0].ToString();
                    dbRow.xValue = r.Table.Columns[col].ToString();
                    dbRow.yValue = DBNull.Value.Equals(r[col]) ? "0" : r[col].ToString();
                    chartResult.Add(dbRow);
                }
            }
            return chartResult;
        }
    }
}
