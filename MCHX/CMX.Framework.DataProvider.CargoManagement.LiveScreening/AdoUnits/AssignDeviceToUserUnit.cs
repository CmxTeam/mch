﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class AssignDeviceToUserUnit : CmCoreDatabaseConnector<SessionExpirationInfo>
    {
        public AssignDeviceToUserUnit(long userId, long deviceId)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DeviceId", deviceId))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"If Exists(Select top 1 1 From DevicesInUse where DeviceId = @DeviceId or UserId = @UserId)
                        Begin
	                        Delete From DevicesInUse Where DeviceId = @DeviceId or UserId = @UserId
                        End
                        Insert Into DevicesInUse(UserId, DeviceId) Values (@UserId, @DeviceId)

                        Declare @EntityId int
                        Select Top 1 @EntityId = HostPlus_Locations.EntityId from HostPlus_Locations 
	                        Inner Join fsp.Devices On fsp.Devices.LocationId = HostPlus_Locations.RecId
                        where fsp.Devices .RecId = @DeviceId

                        Select Isnull((Select top 1 SwitchCondition From dbo.GatewaySwitchBoard where IsEnabled = 1 and ProcessName = 'XRAYSESSIONWARNING' and EntityId = @EntityId), 1800) as Worning,
                               Isnull((Select top 1 SwitchCondition From dbo.GatewaySwitchBoard where IsEnabled = 1 and ProcessName = 'XRAYSESSIONFORCECLOSE' and EntityId = @EntityId), 1800) as ForceClose
                        ";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override SessionExpirationInfo ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var row = rows[0];

            return new SessionExpirationInfo
            {
                SessionWorning = row["Worning"].TryParse<int>(),
                SessionForseClose = row["ForceClose"].TryParse<int>()
            };
        }
    }
}