﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetRescreenedVolumesChartUnit : CmCoreDatabaseConnector<IEnumerable<ChartStepModel>>
    {       
        public GetRescreenedVolumesChartUnit(long? userId, string locations, DateTime? dateFrom, DateTime? dateTo)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DateFrom", dateFrom), new CommandParameter("@DateTo", dateTo),
                   new CommandParameter("@Location", locations))
        {           
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override IEnumerable<ChartStepModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(r => new ChartStepModel
            {
                id = r["LocId"].ToString(),
                xValue = r["Loc"].ToString(),
                yValue = r["ScreeningVolume"].ToString()
            });
        }
        public override string CommandQuery
        {
            get
            {
                return @"select Final.LocationId as LocId, HLoc.Location as Loc, SUM(Final.NumberOfPieces) as ScreeningVolume from (
                        Select * from(
                        Select Scr.NumberOfPieces,Scr.LocationId,Scr.RecDate,Scr.HousebillNumber,
                            ROW_NUMBER() over(partition by Scr.HousebillNumber,Scr.LocationId order by Scr.RecDate desc) R1,
                            Row_number() over(PARTITION by Scr.HousebillNumber order by Scr.RecDate desc) R2,
                            sum(LocationId) over(PARTITION by Scr.HousebillNumber)/cast(count(0) over(PARTITION By Scr.HousebillNumber) as float) R3
                        from fsp.Screening as Scr WITH(NOLOCK)
                        where  Scr.SampleMethod not in ('VERIFICATION')
                        and (@DateFrom is NULL or Scr.ScreeningDate >= cast(@DateFrom as DATE))
                        and (@DateTo is NULL or Scr.ScreeningDate <= DateAdd(DAY,1,cast(@DateTo as DATE)))                        
                        and (@UserId is null or Scr.LocationId in (Select LocationId from AppUserLocations where UserId = @UserId and ApplicationId = 3))
                        and Scr.LocationId is not NULL
                        ) as Ext
                            where Ext.R1 = Ext.R2 
                            and Ext.LocationId <> Ext.R3
                            and (@Location is null or Ext.LocationId in (select * from dbo.Split(@Location,default))) ) as Final
                        join HostPlus_Locations as HLoc WITH(NOLOCK) on Final.LocationId = HLoc.RecId
                        group by Final.LocationId, HLoc.Location";
            }
        }
    }
}
