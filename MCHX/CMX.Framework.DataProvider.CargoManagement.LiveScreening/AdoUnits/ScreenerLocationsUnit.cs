﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class ScreenerLocationsUnit : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {        
        public ScreenerLocationsUnit(long? userId)
            : base(new CommandParameter("@UserId", userId.HasValue ? userId.Value : (object)DBNull.Value))
        {            
        }

        public override string CommandQuery
        {
            get { return @"Select HostPlus_locations.RecId, HostPlus_locations.Location 
                            From HostPlus_locations Inner Join AppUserLocations
                            on AppUserLocations.LocationId = HostPlus_locations.RecId
                            where PGDBEnabled = 1 and ApplicationId = 3 and (@UserId is null or UserId = @UserId)
                            Order By Location"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override IEnumerable<IdNameEntity> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(r => new IdNameEntity 
            {
                Id = Convert.ToInt64(r["RecId"]),
                Name = r["Location"].ToString()
            });
        }
    }
}
