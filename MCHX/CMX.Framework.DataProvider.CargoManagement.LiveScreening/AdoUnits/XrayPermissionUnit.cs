﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class XrayPermissionUnit : CmCoreDatabaseConnector<bool>
    {
        public XrayPermissionUnit(long userId)
            : base(new CommandParameter("@UserId", userId))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select 1 from AddressBook as Screeners 
                        Inner join ScreenerCertifications on ScreenerCertifications.ScreenerId = Screeners.Id
                        Inner join CertificationTypes on CertificationTypes.RecId = ScreenerCertifications.CertificationTypeId
                        inner join ScreeningMethods on ScreeningMethods.RecId = CertificationTypes.ScreeningMethodId  
                        Where ScreeningMethods.RecId = 2 and ScreenerCertifications.ValidTo >= getutcdate()
                        and Screeners.ID = @UserId";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override bool ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Count > 0;
        }
    }
}
