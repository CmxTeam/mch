﻿using System;
using System.Data;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetAuthorizedDeviceByBarcodeUnit : CmCoreDatabaseConnector<DeviceModel>
    {
        public GetAuthorizedDeviceByBarcodeUnit(long userId, string deviceBarcode)
            : base(new CommandParameter("@UserId", userId),
                    new CommandParameter("@DeviceBarcode", deviceBarcode))
        {
        }

        public override string CommandQuery
        {
            get { return "GetDevice"; }
        }

        public override CMX.Framework.DataProvider.DataProviders.CommandType CommandType
        {
            get { return CMX.Framework.DataProvider.DataProviders.CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override DeviceModel ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var r = rows[0];

            return new DeviceModel()
            {
                Id = Convert.ToInt32(r["RecId"]),
                SerialNumber = r["SerialNumber"].ToString(),
                DeviceType = r["DeviceType"].ToString(),
                DeviceBarcode = r["DeviceBarcode"].ToString(),
                UserHolder = r["HolderName"].ToString(),
                LocationId = Convert.ToInt64(r["LocationId"])
            };
        }
    }
}