﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class CreateShipmentCreator : CmCoreDatabaseConnector<string>
    {
        private string _connectionString;
        public CreateShipmentCreator(string connectionString, long userId, long locationId, string number, string origin, string destination, int pieces, int slac, string carrier, double? weight, string masterbillNo)
            : base(new CommandParameter("@UserId", userId),
                    new CommandParameter("@LocationId", locationId),
                    new CommandParameter("@Origin", origin),
                    new CommandParameter("@Destination", destination),
                    new CommandParameter("@AirbillNo", number),
                    new CommandParameter("@NoOfPackages", pieces),
                    new CommandParameter("@Slac", slac),
                    new CommandParameter("@Carrier", carrier),
                    new CommandParameter("@Weight", weight),
                    new CommandParameter("@MasterBillNo", masterbillNo))
        {
            _connectionString = connectionString;
        }

        public override string CommandQuery
        {
            get { return "MCHNew_CreateHawb"; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }

        public override string ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.First()[0].ToString();
        }
    }
}
