﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetShipmentSealsUnit : CmCoreDatabaseConnector<IEnumerable<ScreeningChartModel>>
    {
        private readonly string _connectionDbName;
        private IEnumerable<string> _locations;
        public GetShipmentSealsUnit(string connectionString, DateTime? dateFrom = null, DateTime? dateTo = null, List<string> locations = null)
            : base(new CommandParameter("@DateFrom", dateFrom), new CommandParameter("@DateTo", dateTo))
        {
            _locations = locations;
            _connectionDbName = connectionString;
        }
        public override string CommandQuery
        {
            get
            {
                if (_connectionDbName == "CMMGMSUSGlobalDB"){
                    var tmp = @"SELECT HL.Gateway, ST.SealType as [SealTypeName],ST.RecId as [SealTypeId],Count(SI.RecDate) [DateCount]
                            FROM Shipment_SEAL_information as SI 
                            Inner join Hostplus_incoming as HL on HL.RecId = MasterbillId 
                            Inner join dbo.Shipment_SEAL_Types as ST on ST.RecId = SI.SealTypeID"
                            + (_locations != null ? " Where HL.Gateway in (" + String.Join(",", _locations.Select(s => @"'" + s + @"'")) + ")" : String.Empty) +
                            " group by HL.Gateway,ST.RecId,ST.SealType";
                    return tmp;
                }
                else
                    return @"select ST.SealType as [SealTypeName],ST.RecId as [SealTypeId],Count(SI.RecDate) [DateCount]
                         from dbo.Shipment_SEAL_information as SI inner join dbo.Shipment_SEAL_Types ST on SI.SealTypeID = ST.RecId
                         where (@DateFrom is null OR SI.RecDate >= cast(@DateFrom as DATE)) 
                           and (@DateTo is null OR SI.RecDate <= DateAdd(DAY,1,cast(@DateTo as DATE)))
                         group by ST.RecId,ST.SealType";
            }
        }
        public override string ConnectionString
        {
            get
            {
                var connection = ConfigurationManager.ConnectionStrings[_connectionDbName].ConnectionString;
                if (string.IsNullOrEmpty(connection))
                    throw new ApplicationException("Database connection is null.");

                return connection;
            }
        }
        public override IEnumerable<ScreeningChartModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            var res = rows.Select(r => new ScreeningChartModel
            {
                Id = r["SealTypeId"].TryParse<long>(),
                Name = r["SealTypeName"].ToString(),
                DateCount = r["DateCount"].TryParse<long>(),
                StationName = (_connectionDbName == "CMMGMSUSGlobalDB" ? r["Gateway"].ToString() : _connectionDbName.Substring(2))
            });           
            return res;
        }
      
    }
}
