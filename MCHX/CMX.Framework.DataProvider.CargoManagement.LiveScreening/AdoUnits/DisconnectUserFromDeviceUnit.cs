﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class DisconnectUserFromDeviceUnit : CmCoreDatabaseConnector<DeviceModel>
    {
        public DisconnectUserFromDeviceUnit(int deviceId)
            :base(new CommandParameter("@DeviceId", deviceId))
        {
            
        }

        public override string CommandQuery
        {
            get { return "DisconnectUserFromDevice"; }
        }

        public override CMX.Framework.DataProvider.DataProviders.CommandType CommandType
        {
            get { return CMX.Framework.DataProvider.DataProviders.CommandType.StoredProcedure; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override DeviceModel ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var r = rows[0];

            return new DeviceModel()
            {
                Id = Convert.ToInt32(r["RecId"]),
                SerialNumber = r["SerialNumber"].ToString(),
                DeviceType = r["DeviceType"].ToString(),
                DeviceBarcode = r["DeviceBarcode"].ToString(),
                LocationId = r["LocationId"].TryParse<long>()
            };
        }
    }
}
