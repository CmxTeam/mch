﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetAvailableXRAYDevicesUnit : CmCoreDatabaseConnector<List<DeviceModel>>
    {        
        public GetAvailableXRAYDevicesUnit(long userId, int locationId)
            : base(new CommandParameter("@LocationId", locationId),
                    new CommandParameter("@UserId", userId))
        {            
        }

        public override string CommandQuery
        {
            get
            {
                return @"select D.RecId, D.SerialNumber, D.DeviceType,D.DeviceBarcode, D.LocationId, SM.Code + '-' + D.SerialNumber as Device,
                        IsNull((select top 1 1 from DevicesInUse where DeviceId = D.RecId and UserId != @UserId), 0) as InUse,
                        IsNull((Select top 1 AddressBook.FirstName + ' ' + AddressBook.LastName 
                            from DevicesInUse Inner Join AddressBook on AddressBook.Id = DevicesInUse.UserId where DeviceId = D.RecId and DevicesInUse.UserId != @UserId), '') as InUseBy
                        from Fsp.Devices D  
                        inner join CertifiedDevices CD on D.CertifiedDeviceId = CD.RecId
                        inner join ScreeningMethods SM on CD.ScreeningMethodId = SM.RecId
                        where D.LocationId = @LocationId And SM.Code = 'XRAY'
                        and exists (select top 1 1 from MachinesLogs L inner join AuthorizationTokens AT on L.AuthorizationToken = AT.AuthorizationToken where AT.MachineId = D.RecId and AT.Active=1)";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override List<DeviceModel> ObjectInitializer(List<DataRow> rows)
        {
            return rows.Select(r => new DeviceModel
                                    {
                                        Id = Convert.ToInt32(r["RecId"]),
                                        Name = r["Device"].ToString(),
                                        SerialNumber = r["SerialNumber"].ToString(),
                                        DeviceType = r["DeviceType"].ToString(),
                                        DeviceBarcode = r["DeviceBarcode"].ToString(),
                                        UserHolder = r["InUseBy"].ToString(),
										LocationId = Convert.ToInt64(r["LocationId"]),
                                        InUse = Convert.ToBoolean(r["InUse"])                                       
                                    }
                ).ToList();
        }        
    }
}