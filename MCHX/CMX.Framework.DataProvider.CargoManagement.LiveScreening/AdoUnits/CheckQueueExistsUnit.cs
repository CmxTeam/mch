﻿using System.Data;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class CheckQueueExistsUnit : CmCoreDatabaseConnector<bool>
    {        
        public CheckQueueExistsUnit(long userId, int deviceId)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DeviceId", deviceId))
        {            
        }
        public override string CommandQuery
        {
            get { return @"select case when exists (select 1 from Fsp.ScreeningQueue where UserId = @userId and DeviceId = @DeviceId) then CAST(1 AS BIT) else CAST(0 AS BIT) end as QueueExists"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return false;
            var r = rows[0];

            return (bool)r["QueueExists"];
        }        
    }
}