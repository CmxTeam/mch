﻿using System;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class ScreeningQueueCreator : CmCoreDatabaseConnector<long>
    {
        private readonly string _connectionString;

        public ScreeningQueueCreator(string connectionString, string hawbNumber, long deviceId, long userId, string userName, int slac)
            : base(new CommandParameter("@Slac", slac),
                    new CommandParameter("@UserId", userId),
                    new CommandParameter("@DeviceId", deviceId),
                    new CommandParameter("@UserName", userName),
                    new CommandParameter("@HawbNumber", hawbNumber))
        {
            _connectionString = connectionString;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Delete from fsp.ScreeningQueue Where DeviceId = DeviceId and UserId = @UserId
                        Insert Into fsp.ScreeningQueue (Reference, DeviceId, UserId, UserName, Timestamp, StatusId, Slac)
                        Values (@HawbNumber, @DeviceId, @UserId, @UserName, getutcdate(), 500, @Slac)
                        Select Scope_Identity() as Id";
            }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override long ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0)
                throw new Exception("No ScreeningQueue has been created");
            return Convert.ToInt64(rows[0][0]);
        }
    }
}
