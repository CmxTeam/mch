﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetChartsList : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public override IEnumerable<IdNameEntity> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(r => new IdNameEntity 
            {
                Id = r["Id"].TryParse<long>(),
                Name = r["Name"].ToString()
            });
        }
        public override string CommandQuery
        {
            get
            {
                return @"select 1 as Id, 'Screening Volumes' as Name union
                        select 2 , 'Hourly Production'  union
                        select 3 , 'Rescreened Volumes'  union
                        select 4 , 'Screening Technologies Utilization' union
                        select 5 , 'Screening Results'  union
                        select 6 , 'Chain of Custody'";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
