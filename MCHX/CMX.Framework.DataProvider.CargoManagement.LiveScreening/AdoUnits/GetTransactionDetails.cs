﻿using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetTransactionDetails : CmCoreDatabaseConnector<TransactionDetailsModel>
    {
        public GetTransactionDetails(long id)
            : base(new CommandParameter("@Id", id))
        {
        }
        public override TransactionDetailsModel ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(row => new TransactionDetailsModel
                                  {
                                      Id = Convert.ToInt64(row["Id"]),
                                      ShipmentReference = row["ShipmentReference"].ToString(),
                                      TotalPieces = Convert.ToInt32(row["TotalPieces"]),
                                      ScreenedPieces = Convert.ToInt32(row["ScreenedPieces"]),
                                      Status = row["Status"].ToString(),
                                      Validated = row["IsValid"].ToString(),
                                      SampleNumber = row["SampleNumber"].ToString(),
                                      Method = row["Method"].ToString(),
                                      Device = row["Device"].ToString(),
                                      Timestamp = Convert.ToDateTime(row["Timestamp"]),
                                      Location = row["Location"].ToString(),
                                      Result = row["ScreeningResult"].ToString(),
                                      Screener = row["Screener"].ToString(),
                                      Comment = row["Comment"].ToString()
                                  }).FirstOrDefault();
        }
        public override string CommandQuery
        {
            get
            {
                return @"Select top 1
                                    Fsp.screening.RecId as Id, 
                                    UPPER(HousebillNumber) as ShipmentReference,
                                    IsNull((Select Sum(isnull(HBSlac, 0)) From fsp.Queue where fsp.Queue.HousebillNo = Fsp.screening.HousebillNumber), 0) as TotalPieces, 
                                    NumberOfPieces as ScreenedPieces, 	
                                    'Screening In Progress' As Status,
                                    case when Isnull((select TOP 1 IsValid from Fsp.Queue Where HouseBillNo = HousebillNumber), 0) = 1 then 'Yes' else 'No' end as IsValid,
                                    SampleNumber, ScreeningMethods.Code as Method, 
                                    Fsp.Devices.SerialNumber AS Device,
                                    ScreeningDate as Timestamp, 
                                    HostPlus_Locations.Location AS Location, 
                                    ResultCode As ScreeningResult,
                                    fsp.Screening.UserName As Screener,
                                    Remark as Comment
                                from Fsp.screening left join HostPlus_Locations on HostPlus_Locations.RecId = Fsp.screening.LocationID
                                left join ScreeningMethods on ScreeningMethods.RecId = Fsp.screening.ScreeningMethodId
                                left join Fsp.Devices on Fsp.Devices.RecId = Fsp.screening.DeviceID
                                left join AddressBook as Screeners on Screeners.Id = Fsp.screening.UserId where Fsp.Screening.RecId = @Id";
            }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
