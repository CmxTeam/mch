﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetHourlyProductionChart : CmCoreDatabaseConnector<IEnumerable<ScreeningChartModel>>
    {
        public GetHourlyProductionChart(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DateFrom", dateFrom),
                   new CommandParameter("@DateTo", dateTo), new CommandParameter("@Location", locations))
        {
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override string CommandQuery
        {
            get { return "GetHourlyProductionChart"; }
        }
        public override IEnumerable<ScreeningChartModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(s => new ScreeningChartModel
            {
                DateCount = s["Pieces"].TryParse<long>(),
                Name = s["LabelName"].ToString(),
                StationName = s["Location"].ToString(),
                Date = s["Date"].TryParse<DateTime>()
            }).ToList();
        }
    }
}
