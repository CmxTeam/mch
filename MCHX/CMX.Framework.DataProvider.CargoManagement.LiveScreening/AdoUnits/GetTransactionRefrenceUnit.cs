﻿using System;
using System.Data;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetTransactionRefrenceUnit : CmCoreDatabaseConnector<ScanTransactionModel>
    {
        public GetTransactionRefrenceUnit(long userId, int deviceId)
            : base(new CommandParameter("@UserId", userId),
                    new CommandParameter("@DeviceId", deviceId))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"select top 1 ReferenceId, dbo.GetLocalDateTimeByLocationID((Select top 1 LocationId from Fsp.Devices where RecId = @DeviceId), CreatedDate) AS CreatedDate from Transactions where MachineId = @DeviceId and UserId = @UserId
                        and ReferenceName = (select top 1 Reference From Fsp.ScreeningQueue where UserId = @UserId and DeviceId = @DeviceId)
                        order by CreatedDate desc";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override ScanTransactionModel ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var r = rows[0];

            return new ScanTransactionModel()
                   {
                       ReferenceId = r["ReferenceId"] == DBNull.Value ? null : r["ReferenceId"].ToString(),
                       CreatedDate = r["CreatedDate"] == DBNull.Value ? null : (DateTime?)r["CreatedDate"]
                   };
        }
    }
}