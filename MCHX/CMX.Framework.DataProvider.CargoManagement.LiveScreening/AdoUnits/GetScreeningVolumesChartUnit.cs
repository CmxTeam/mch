﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetScreeningVolumesChartUnit : CmCoreDatabaseConnector<IEnumerable<ChartStepModel>>
    {       
        public GetScreeningVolumesChartUnit(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(new CommandParameter("@UserId", userId), new CommandParameter("@DateFrom", dateFrom), new CommandParameter("@DateTo", dateTo),
                    new CommandParameter("@Location", locations))
        {           
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override string CommandQuery
        {
            get
            {
                return @"Select FS.LocationId as LocId, HL.Location as Loc, SUM(FS.NumberOfPieces) as ScreeningVolume
                            From Fsp.Screening FS WITh (NOLOCK)
                            left join HostPlus_Locations HL WITh (NOLOCK)  On FS.LocationId = HL.RecId 
                            where (@DateFrom is NULL or ScreeningDate >= cast(@DateFrom as DATE))
                            and (@DateTo is NULL or FS.ScreeningDate <= DateAdd(DAY,1,cast(@DateTo as DATE)))
                            and (@Location is null or FS.LocationId in (select * from dbo.Split(@Location,default)))                     
                            and SampleMethod not in ('VERIFICATION')
                            and HL.Location is not null
                            and (@UserId is null or FS.LocationId in (Select LocationId from AppUserLocations where UserId = @UserId and ApplicationId = 3))
                            group by FS.LocationId, HL.Location";               
            }
        }
        public override IEnumerable<ChartStepModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(r => new ChartStepModel
            {
                id = r["LocId"].ToString(),
                xValue = r["Loc"].ToString(),
                yValue = r["ScreeningVolume"].ToString()
            });
        }
    }
}
