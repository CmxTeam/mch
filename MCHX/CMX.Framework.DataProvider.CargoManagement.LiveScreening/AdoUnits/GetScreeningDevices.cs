﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetScreeningDevices : CmCoreDatabaseConnector<IEnumerable<ExtendedScreeningDeviceModel>>
    {
        public GetScreeningDevices(long? deviceId = null,string hostPlusLocationId = null, long? statusId = null,long? deviceLocationId = null,
                                   string screeningMethodId = null, string warehouseLocationId = null,string serialNumber = null)
            : base(new CommandParameter("@HostPlusLocationId", hostPlusLocationId), new CommandParameter("@StatusId", statusId),
                   new CommandParameter("@DeviceLocationId", deviceLocationId), new CommandParameter("@ScreeningMethodId", screeningMethodId),
                   new CommandParameter("@WarehouseLocationId", warehouseLocationId), new CommandParameter("@SerialNumber", serialNumber),
                   new CommandParameter("@DeviceId",deviceId))
        {
        }
        public override IEnumerable<ExtendedScreeningDeviceModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return new List<ExtendedScreeningDeviceModel>();
            return rows.Select(s => new ExtendedScreeningDeviceModel 
            {
                Id = s["DeviceId"].TryParse<long>(),
                Image = s["Image"].ToString(),
                Location = s["HostPlusLocation"].ToString(),
                SerialNumber = s["SerialNumber"].ToString(),
                CertifiedDeviceModuleNumber = s["ModelNumber"].ToString(),
                ScreeningMethodTechnology = s["Code"].ToString(),
                LocationId = s["LocationId"].TryParse<long?>(),
                StatusId = s["StatusId"].TryParse<int>(),
                StatusName = s["DisplayName"].ToString()
            });
        }
        public override string CommandQuery
        {
            get
            {
                return @"Select  '' as Image, HostPlus_Locations.Location as HostPlusLocation,	 
	                           Devices.SerialNumber, ScreeningMethods.Code, CertifiedDevices.ModelNumber,
	                           Devices.RecId as DeviceId,HostPlus_Locations.RecId as LocationId,
	                           Statuses.RecId as StatusId,Statuses.DisplayName,Devices.RecId as DeviceId	   
                        from fsp.Devices as Devices
                        left join CertifiedDevices On CertifiedDevices.RecId = Devices.CertifiedDeviceId
                        left join ScreeningMethods On ScreeningMethods.RecId = CertifiedDevices.ScreeningMethodId
                        left join HostPlus_Locations on HostPlus_Locations.RecId = Devices.LocationId 
                        left join Statuses on Statuses.RecId = Devices.StatusId
                        where(@HostPlusLocationId is null OR HostPlus_Locations.RecId in (select * from dbo.Split(@HostPlusLocationId,default)) )
                        AND (@StatusId is null OR (Devices.LocationId is null AND Devices.StatusId = @StatusId))
                        AND (@ScreeningMethodId is null OR ScreeningMethods.RecId in (select * from dbo.Split(@ScreeningMethodId,default)))
                        AND (@WarehouseLocationId is null OR Devices.WarehouseLocationId in (select * from dbo.Split(@WarehouseLocationId,default)))
                        AND (@SerialNumber is null OR Devices.SerialNumber like '%' + @SerialNumber +'%')
                        AND (@DeviceId is null or Devices.RecId = @DeviceId)"
                       +
                       //Eliminating database table wrong rows, fix as soon as possible
                       @"                        
                        AND Devices.StatusId is not null
                        AND (Devices.LocationId is null or Devices.LocationId <> 0)
                        AND (Devices.StatusId <> 510 OR (Devices.StatusId = 510 AND Devices.LocationId is not null ) )";
                       /////////////////////////////////////////////////////////////////
            }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
