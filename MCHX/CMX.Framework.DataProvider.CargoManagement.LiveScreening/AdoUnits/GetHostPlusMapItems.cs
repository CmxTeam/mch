﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetHostPlusMapItems : CmCoreDatabaseConnector<IEnumerable<MapItemModel>>
    {
        public GetHostPlusMapItems(long userId) : base(new CommandParameter("@UserId",userId))
        {
        }
        public override IEnumerable<MapItemModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(r => new MapItemModel 
            {
                Id = Convert.ToInt32(r["RecId"]),
                Name = r["Location"].ToString(),
                Image = "flag.jpg",
                Zoom = 6,
                Latitude = r["Latitude"].ToString(),
                Longitude = r["Longitude"].ToString(),
                Pin = r["HasAlarm"].TryParse<bool>() ? "red.png" : "blue.png",
                Details = new MapItemDetailsModel
                {
                    CCSFType = r["CCSF"].ToString(),
                    SecurityManagerName = r["SecurityManagerName"].ToString(),
                    Email = r["Email"].ToString(),
                    Office = r["WorkPhone"].ToString(),
                    Mobile = r["MobilePhone"].ToString(),
                    TotalScannedPieces = r["TotalScannedPieces"].TryParse<int>(),
                    TotalPassed = r["PassedPieces"].TryParse<int>(),
                    TotalFailed = r["FailedPieces"].TryParse<int>(),
                    TotalScreenedPrimary = r["TotalScreenedPrimary"].TryParse<int>(),
                    TotalScreenedSecondary = r["TotalScreenedSecondary"].TryParse<int>()
                }
            });
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select RecId, Location, 
	                        Latitude, Longitude,
	                        CCSF,  SecurityManagerName,
	                        Email, WorkPhone, MobilePhone,													
						    MAX(case when ResultCode = 'ALARM' then 1 else 0 end) as HasAlarm,
							SUM(case when ResultCode = 'PASS' then NumberOfPieces else 0 end) as PassedPieces,
							SUM(case when ResultCode = 'ALARM' then NumberOfPieces else 0 end) as FailedPieces,							
							SUM(case when IsPrimary = 1 then NumberOfPieces else 0 end) as TotalScreenedPrimary,
							SUM(case when IsPrimary <> 1 then NumberOfPieces else 0 end) as TotalScreenedSecondary,
						    SUM(NumberOfPieces) as TotalScannedPieces
				          from (		
				                select HostPlus_Locations.RecId, HostPlus_Locations.Location, 
	                                        LocationDetails.Latitude, LocationDetails.Longitude,
	                                        LocationDetails.CCSF, AddressBook.FirstName + ' ' + AddressBook.LastName as SecurityManagerName,
	                                        AddressBook.Email, AddressBook.WorkPhone, AddressBook.MobilePhone,							
							                case when ResultCode = 'PASS' then 'PASS' else 		 
			                                        case when IsProcessed = 0 and CancelledDate is null then 'ALARM' else 
				                                        case when IsProcessed = 1 and CancelledDate is not null then 'PASS' else '' end 
			                                        end
		                                    end as ResultCode,
							                Screening.ScreeningDate	,Screening.NumberOfPieces,IsProcessed,CancelledDate,IsPrimary
						                From HostPlus_Locations 
						                    Left JOIN fsp.Screening as Screening on Screening.LocationId = HostPlus_Locations.RecId  
								                    and (cast(Screening.ScreeningDate as date) = cast(GetUtcDate() as date))
									                and SampleMethod <> 'VERIFICATION' 
									                and SampleMethod <> 'BACKGROUND'  
									                and SampleMethod <> 'UNKNOWN'
                                            Left Join LocationDetails on LocationDetails.LocationId = HostPlus_Locations.RecId
                                            Left Join AddressBook on LocationDetails.ManagerId = AddressBook.ID
						                    Left Join Fsp.Devices as Devices on Screening.DeviceId = Devices.RecId 
						                    Left Join CertifiedDevices On CertifiedDevices.RecId = Devices.CertifiedDeviceId
						                    Left Join ScreeningMethods On ScreeningMethods.RecId = CertifiedDevices.ScreeningMethodId 
						                Where PGDBEnabled = 1
						                    and (@UserId is null or HostPlus_Locations.RecId in (Select LocationId from AppUserLocations where AppUserLocations.UserId = @UserId))
						                ) as T
                        group by RecId, Location,Latitude, Longitude,
		                        CCSF,SecurityManagerName,
		                        Email, WorkPhone, MobilePhone
                        Order By Location";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
