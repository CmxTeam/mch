﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class ScreenersGetter : CmCoreDatabaseConnector<IEnumerable<ScreenerModel>>
    {
        public ScreenersGetter(long? userId, string locationId, string screeningMethodId, string screenerName, long? screenerId)
            : base(new CommandParameter("@UserId", userId),
                    new CommandParameter("@LocationId", locationId),
                    new CommandParameter("@ScreeningMethodId", screeningMethodId),
                    new CommandParameter("@ScreenerId", screenerId),
                    new CommandParameter("@ScreenerName", string.IsNullOrWhiteSpace(screenerName) ? null : screenerName))
        {
        }

        public override IEnumerable<ScreenerModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(r => new ScreenerModel
            {
                Id = r["ScreenerId"].TryParse<long>(),
                Name = r["Screener"] != System.DBNull.Value ? r["Screener"].TryParse<string>() : string.Empty,
                Photo = r["Photo"] != System.DBNull.Value ?  r["Photo"].TryParse<string>() : string.Empty,
                Phone = r["Phone"] != System.DBNull.Value ? r["Phone"].TryParse<string>() : string.Empty,
                Email = r["Email"] != System.DBNull.Value ? r["Email"].TryParse<string>() : string.Empty,
                CellPhone = r["CellPhone"] != System.DBNull.Value ? r["CellPhone"].TryParse<string>() : string.Empty,
                Location = new IdNameEntity { Id = r["LocationId"].TryParse<long>(), Name = r["Location"].TryParse<string>() },
                Method = new IdNameEntity { Id = r["MethodId"].TryParse<long>(), Name = r["MethodName"].TryParse<string>() }
            });
        }

        public override string CommandQuery
        {
            get
            {
                return @" Select Distinct Screeners.Id as ScreenerId, Screeners.FirstName +  ' ' + Screeners.LastName as Screener, 
	                        PersonalWebSite as Photo, WorkPhone as Phone, Email, MobilePhone as CellPhone,
	                        HostPlus_Locations.RecId as LocationId, HostPlus_Locations.Location,
	                        ScreeningMethods.RecId as MethodId, ScreeningMethods.Code as MethodName
                        from AddressBook as Screeners Inner join ScreenerCertifications on ScreenerCertifications.ScreenerId = Screeners.Id
                         Inner join CertificationTypes on CertificationTypes.RecId = ScreenerCertifications.CertificationTypeId
                         inner join AppUserLocations on AppUserLocations.UserId = Screeners.Id
                         inner join ScreeningMethods on ScreeningMethods.RecId = CertificationTypes.ScreeningMethodId
                         inner join HostPlus_Locations on HostPlus_Locations.RecId = AppUserLocations.LocationId
                        Where (@UserId is null or AppUserLocations.UserId = @UserId) 
	                        and ScreenerCertifications.ValidTo >= getutcdate() 
	                        and (@LocationId is null or AppUserLocations.LocationId in (select * from dbo.Split(@LocationId,default)) ) 
	                        and (@ScreeningMethodId is null or CertificationTypes.ScreeningMethodId in (select * from dbo.Split(@ScreeningMethodId,default)))
                            and (@ScreenerId is null or Screeners.Id = @ScreenerId)
	                        and (@ScreenerName is null or Screeners.FirstName +  '' + Screeners.LastName like '%' + @ScreenerName + '%')
                        Order By Screener";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}