﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities.Dictionary;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class ScreeningTransactionsGetterUnit : CmCoreMultyTableDatabaseConnector<PageableSet<Transaction>>
    {
        public ScreeningTransactionsGetterUnit(DateTime? fromDate, DateTime? toDate, string housebillNumber, string sampleNumber,
            string locationId, string warehouseLocationId, string screeningMethodId, string deviceId, long? userId, string resultCode, bool? isValid, int startIndex, int endIndex,bool isGroup)
            :base(new CommandParameter("@FromDate", fromDate),
                    new CommandParameter("@ToDate", toDate),
                    new CommandParameter("@HousebillNumber", housebillNumber),
                    new CommandParameter("@SampleNumber", sampleNumber),
                    new CommandParameter("@LocationId", locationId),
                    new CommandParameter("@WarehouseLocationId", warehouseLocationId),
                    new CommandParameter("@ScreeningMethodId", screeningMethodId),
                    new CommandParameter("@DeviceId", deviceId),
                    new CommandParameter("@UserId", userId),
                    new CommandParameter("@ResultCode", resultCode != null ? resultCode
                                                        .Replace(((int)ScreeningResultsEnum.Pass).ToString(), "PASS")
                                                        .Replace(((int)ScreeningResultsEnum.Alarm).ToString(), "ALARM")
                                                        .Replace(((int)ScreeningResultsEnum.AlarmCleared).ToString(), "ALARM CLEARED") 
                                                        : null),
                    new CommandParameter("@IsValid", isValid),
                    new CommandParameter("@StartIndex", startIndex),
                    new CommandParameter("@EndIndex", endIndex),
                    new CommandParameter("@IsGroup", isGroup))
           
        {
        }

        public override PageableSet<Transaction> ObjectInitializer(List<System.Data.DataTable> tables)
        {
            return new PageableSet<Transaction>
            {
                Count = tables[1].Rows[0][0].TryParse<int>(),
                Data = tables[0].Rows.OfType<DataRow>().Select(r => new Transaction
                {
                    Id = r["RecId"].TryParse<long>(),
                    AirbillNumber = r["Airbillno"].TryParse<string>(),
                    SampleNumber = r["SampleNumber"].TryParse<string>(),
                    Method = r["Method"].TryParse<string>(),
                    DeviceSerial = r["DeviceSerial"].TryParse<string>(),
                    ScreenedOn = r["ScreenedOn"].TryParse<DateTime>(),
                    ScreeningLocation = r["ScreeningLocation"].TryParse<string>(),
                    TotalPieces = r["TotalPieces"].TryParse<int>(),
                    TotalSlac = r["TotalSlac"].TryParse<int>(),
                    ScreenedSlac = r["ScreenedSlac"].TryParse<int>(),
                    ScreeningResult = r["ScreeningResult"].TryParse<string>(),
                    Screener = r["Screener"].TryParse<string>(),
                    IsValid = r["IsValid"].TryParse<string>(),
                    Remark =  r["Remark"].ToString()
                })
            };
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override string CommandQuery
        {
            get { return "GetScreeningTransactions"; }
        }

        public override CMX.Framework.DataProvider.DataProviders.CommandType CommandType
        {
            get { return CMX.Framework.DataProvider.DataProviders.CommandType.StoredProcedure; }
        }
    }
}
