﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetScreeningResultsChartData : GetScreeningChartData
    {
        public GetScreeningResultsChartData(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
            : base(userId, locations, dateFrom, dateTo)
        {
        }

        public override string CommandQuery
        {
            get { return "GetScreeningResultsChart"; }
        }
    }
}
