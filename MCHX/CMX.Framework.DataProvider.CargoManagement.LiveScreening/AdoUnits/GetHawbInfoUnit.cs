﻿using System;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits
{
    public class GetHawbInfoUnit : CmCoreDatabaseConnector<HawbModel>
    {
        private readonly string _connectionString;

        public GetHawbInfoUnit(string connectionString, long locationId, string hawbNumber)
            : base(new CommandParameter("@AirbillNo", hawbNumber), new CommandParameter("@LocationId", locationId))
        {
            _connectionString = connectionString;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select Top 1 RecId, AirbillNo, SenderName as Shipper, ReceiverName as Consignee, Origin, Destination,
                        NoOfPackages as TotalPieces, isnull(Slac, 0) as TotalSlac,
                        cast(GrossWeight as nvarchar(10)) + ' ' + WeightUom as TotalWeight,
                        Description as GoodsDescription,
						Isnull((Select Sum(Screening.NumberOfPieces) from CMXGLOBALDB.fsp.Screening as Screening 
							where Hawb.AirbillNo = Screening.HousebillNumber and Screening.LocationId = @LocationId), 0) as ScannedSlac
                        from Hawb 
                        Where AirbillNo = @AirbillNo
                        Order By RecDate Desc";
            }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }

        public override HawbModel ObjectInitializer(List<System.Data.DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var r = rows[0];

            return new HawbModel
            {
                Id = Convert.ToInt64(r["RecId"]),
                Name = r["AirbillNo"].ToString(),
                Shipper = new PartnerModel { Name = r["Shipper"].ToString() },
                Consignee = new PartnerModel { Name = r["Consignee"].ToString() },
                Origin = new PortModel { Name = r["Origin"].ToString() },
                Destination = new PortModel { Name = r["Destination"].ToString() },
                TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                TotalSlac = Convert.ToInt32(r["TotalSlac"]),
                ScannedSlac = Convert.ToInt32(r["ScannedSlac"]),
                TotalWeight = r["TotalWeight"].ToString(),
                GoodsDescription = r["GoodsDescription"].ToString()
            };
        }
    }
}
