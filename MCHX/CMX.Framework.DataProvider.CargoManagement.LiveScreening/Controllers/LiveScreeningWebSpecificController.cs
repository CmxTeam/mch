﻿using System;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Cors;
using System.Collections.Generic;
using DataExporter.Excel;
using DhtmlxComponents.Widgets;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.CargoManagement.Controllers;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LiveScreeningWebSpecificController : CmCommonController
    {
        readonly long _excelExportLimit = 10000;

        [HttpGet]
        public IEnumerable<ValueText> GetScreeningLocations()
        {
            var manager = new LiveScreeningDatabaseManager();
            return manager.GetScreeningLocations(ApplicationUser.UserId).Select(l => new ValueText { value = l.Id.ToString(), text = l.Name }).ToList();
        }

        [HttpGet]
        public IEnumerable<ValueText> GetScreeners(string locationId = null, string screeningMethodId = null)
        {
            var manager = new LiveScreeningDatabaseManager();
            return manager.GetScreeners(ApplicationUser.UserId, locationId, screeningMethodId, null, null)
                .GroupBy(s => new { s.Id, s.Name })
                .Select(l => new ValueText { value = l.Key.Id.ToString(), text = l.Key.Name }).ToList();
        }

        [HttpGet]
        public IEnumerable<ValueText> GetWarehouseLocations(string locationId = null)
        {
            var manager = new CmCommonDatabaseManager();
            return manager.GetWarehouseLocations(ApplicationUser.UserId, 3, locationId).Select(l => new ValueText { value = l.Id.ToString(), text = l.Name }).ToList();
        }

        [HttpGet]
        public Grid GetScreeningTransactions(int count, int pageNumber = 0, DateTime? fromDate = null, DateTime? toDate = null, string housebillNumber = null, string sampleNumber = null,
            string locationId = null, string warehouseLocationId = null, string screeningMethodId = null, string deviceId = null, string resultCode = null, bool? isValid = null)
        {
            var manager = new LiveScreeningDatabaseManager();
            var data = manager.GetScreeningTransactions(fromDate, toDate, housebillNumber, sampleNumber,
                locationId, warehouseLocationId, screeningMethodId, deviceId, ApplicationUser.UserId, resultCode, isValid, pageNumber * count, count);

            var tc = data.Count > 10000 ? 10000 : data.Count;

            return new Grid
            {
                total_count = tc.ToString(),
                pos = pageNumber * count,
                rows = data.Data.Select(d => new Row
                {
                    id = d.Id.ToString(),
                    data = new List<string> { d.AirbillNumber, d.SampleNumber, d.Method, d.DeviceSerial, d.ScreenedOn.ToString("yyyy/MM/dd HH:mm"),
                        d.ScreeningLocation, d.TotalPieces.ToString(), d.TotalSlac.ToString(), d.ScreenedSlac.ToString(), d.ScreeningResult, d.Screener, d.IsValid, d.Id.ToString() }
                })
            };
        }
        [HttpGet]
        public Grid GetScreeningTransactionsByHouseBill(string housebillNumber, int count = 10000, int pageNumber = 0)
        {
            var manager = new LiveScreeningDatabaseManager();
            var data = manager.GetScreeningTransactions(null, null, housebillNumber, null,
                null, null, null, null, ApplicationUser.UserId, null, null, pageNumber * count, count);

            var tc = data.Count > 10000 ? 10000 : data.Count;

            return new Grid
            {
                total_count = tc.ToString(),
                pos = pageNumber * count,
                rows = data.Data.Select(d => new Row
                {
                    id = d.Id.ToString(),
                    data = new List<string> { d.SampleNumber, d.Method, d.Screener, d.ScreenedOn.ToString("yyyy/MM/dd HH:mm"), d.Remark, d.Id.ToString() }
                })
            };
        }

        [HttpGet]
        public Grid GetGroupedScreeningTransactions(int count, int pageNumber = 0, DateTime? fromDate = null, DateTime? toDate = null, string housebillNumber = null, string sampleNumber = null,
            string locationId = null, string warehouseLocationId = null, string screeningMethodId = null, string deviceId = null, string resultCode = null, bool? isValid = null)
        {
            var manager = new LiveScreeningDatabaseManager();
            var data = manager.GetGroupedScreeningTransactions(fromDate, toDate, housebillNumber, sampleNumber,
                locationId, warehouseLocationId, screeningMethodId, deviceId, ApplicationUser.UserId, resultCode, isValid, pageNumber * count, count);

            var tc = data.Count > 10000 ? 10000 : data.Count;

            return new Grid
            {
                total_count = tc.ToString(),
                pos = pageNumber * count,
                rows = data.Data.Select(d => new Row
                {
                    id = d.Id.ToString(),
                    data = new List<string> { d.AirbillNumber, d.Method, d.ScreenedOn.ToString("yyyy/MM/dd HH:mm"),d.ScreeningLocation,
                         d.TotalPieces.ToString(), d.TotalSlac.ToString(), d.ScreenedSlac.ToString(), d.ScreeningResult, d.IsValid,d.Id.ToString() }
                })
            };
        }

        [HttpGet]
        public DataContainer<byte[]> GroupedScreeningTransactionsExcelExport(DateTime? fromDate = null,
            DateTime? toDate = null, string housebillNumber = null, string sampleNumber = null,
            string locationId = null, string warehouseLocationId = null, string screeningMethodId = null,
            string deviceId = null, string resultCode = null, bool? isValid = null)
        {
            var exportResult = new DataContainer<byte[]>(){Status = new TransactionStatus()};
            try
            {
                var manager = new LiveScreeningDatabaseManager();
                var data = manager.GetGroupedScreeningTransactions(fromDate, toDate, housebillNumber, sampleNumber,
                    locationId, warehouseLocationId, screeningMethodId, deviceId, ApplicationUser.UserId, resultCode,
                    isValid, 0, 10000);

                var excelData = new ExcelData
                {
                    Headers =
                        new List<string>
                        {
                            "Reference#",
                            "Method",
                            "Screened On",
                            "CCSF",
                            "Pieces",
                            "SLAC",
                            "SLAC Screened",
                            "Result",
                            "Validated?"
                        },
                    SheetName = "EXPLORER",
                    DataRows = data.Data.Select(a => new List<string>
                    {
                        a.AirbillNumber,
                        a.Method,
                        a.ScreenedOn.ToShortDateString(),
                        a.ScreeningLocation.ToString(),
                        a.TotalPieces.ToString(),
                        a.TotalSlac.ToString(),
                        a.ScreenedSlac.ToString(),
                        a.ScreeningResult,
                        a.IsValid == "transparent.png" ? "false" : "true"
                    }).ToList()
                };
                var excelManager = new ExcelManager();
                var excel = excelManager.GenerateExcel(excelData);

                if (data.Count > _excelExportLimit)
                {
                    exportResult.Status.Status = false;
                    exportResult.Status.Message =
                        String.Format("Your search has {0} records. Max records to download to an excel is {1}",
                            data.Count,
                            _excelExportLimit);
                }
                else
                    exportResult.Status.Status = true;
                
                exportResult.Data = excel;
            }
            catch (Exception ex)
            {
                exportResult.Status.Status = false;
                exportResult.Status.Message = ex.Message;
            }

            return exportResult;
        }

        [HttpGet]
        public DataContainer<byte[]> ScreeningTransactionsExcelExport(DateTime? fromDate = null, DateTime? toDate = null, string housebillNumber = null, string sampleNumber = null,
            string locationId = null, string warehouseLocationId = null, string screeningMethodId = null, string deviceId = null, string resultCode = null, bool? isValid = null)
        {
            var exportResult = new DataContainer<byte[]>(){Status = new TransactionStatus()};
            try
            {
                var manager = new LiveScreeningDatabaseManager();
                var data = manager.GetScreeningTransactions(fromDate, toDate, housebillNumber, sampleNumber,
                    locationId, warehouseLocationId, screeningMethodId, deviceId, ApplicationUser.UserId, resultCode,
                    isValid, 0, 10000);

                var excelData = new ExcelData
                {
                    Headers =
                        new List<string>
                        {
                            "Reference#",
                            "Sample#",
                            "Method",
                            "Device ID",
                            "Screened On",
                            "CCSF",
                            "Pieces",
                            "SLAC",
                            "SLAC Screened",
                            "Result",
                            "Screener",
                            "Validated?"
                        },
                    SheetName = "EXPLORER",
                    DataRows = data.Data.Select(a => new List<string>
                    {
                        a.AirbillNumber,
                        a.SampleNumber,
                        a.Method,
                        a.DeviceSerial,
                        a.ScreenedOn.ToShortDateString(),
                        a.ScreeningLocation,
                        a.TotalPieces.ToString(),
                        a.TotalSlac.ToString(),
                        a.ScreenedSlac.ToString(),
                        a.ScreeningResult,
                        a.Screener,
                        a.IsValid == "transparent.png" ? "false" : "true"
                    }).ToList()
                };
                var excelManager = new ExcelManager();
                var excel = excelManager.GenerateExcel(excelData);

                if (data.Count > _excelExportLimit)
                {
                    exportResult.Status.Status = false;
                    exportResult.Status.Message =
                        String.Format("Your search has {0} records. Max records to download to an excel is {1}",
                            data.Count,
                            _excelExportLimit);
                }
                else
                    exportResult.Status.Status = true;

                exportResult.Data = excel;
            }
            catch (Exception ex)
            {
                exportResult.Status.Status = false;
                exportResult.Status.Message = ex.Message;
            }
            return exportResult;
        }

        [HttpGet]
        public IEnumerable<ValueText> GetCertifiedDevices()
        {
            var manager = new CmCommonDatabaseManager();
            return manager.GetCertifiedDevices().Select(l => new ValueText { value = l.Id.ToString(), text = l.Name }).ToList();
        }

        [HttpGet]
        public HttpResponseMessage GetFacilitiesTree(string filter = "")
        {
            var manager = new LiveScreeningDatabaseManager();
            var facilities = manager.GetFacilities(null, ApplicationUser.UserId, filter);

            var data = facilities.Select(r => new
            {
                LocationId = r.Id,
                Location = r.Location
            }).GroupBy(r => new { LocationId = r.LocationId, Location = string.IsNullOrEmpty(r.Location) ? "UNASSIGNED" : r.Location })
              .Select(g => string.Format("<item text=\"{0}\" id=\"{1}\"></item>", g.Key.Location, g.Key.LocationId)).ToList();

            var xml = string.Format("<tree id=\"0\"><item text=\"FACILITIES\" id=\"rootElement\">{0}</item></tree>", string.Join(Environment.NewLine, data));
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        [HttpGet]
        public Grid GetFacilitiesGrid(long? id = null)
        {
            var manager = new LiveScreeningDatabaseManager();
            var facilities = manager.GetFacilities(id, ApplicationUser.UserId, null);

            return new Grid
            {
                rows = facilities.Select(d => new Row
                {
                    id = d.Id.ToString(),
                    data = new List<string> { d.Id.ToString(), d.Location, d.Address, d.CCSF, d.SecurityManagerName, d.Email,
                        d.WorkPhone, d.MobilePhone, d.Status, d.TotalDevices.ToString(), d.ScreenersCount.ToString()}
                })
            };

        }

        [HttpGet]
        public DataContainer<byte[]> ExportFacilities(long? id = null)
        {
            return MakeTransactional(() =>
            {
                var manager = new LiveScreeningDatabaseManager();
                var facilities = manager.GetFacilities(id, ApplicationUser.UserId, null).ToList();
                if (!facilities.Any()) return null;
                if (facilities.Count() > _excelExportLimit)
                    throw new Exception(
                        String.Format("Your search has {0} records. Max records to download to an excel is {1}",
                            facilities.Count(), _excelExportLimit));
                var excelData = new ExcelData
                {
                    Headers =
                        new List<string>
                        {
                            "Location","Address","CCSF#","ManagerName","Email","Work Phone","Mobile Phone",
                            "Status","Total Devices","Total Screeners"
                        },
                    SheetName = "EXPLORER",
                    DataRows = facilities.Select(d => new List<string>
                    {
                        d.Location, d.Address, d.CCSF, d.SecurityManagerName, d.Email,
                        d.WorkPhone, d.MobilePhone, d.Status, d.TotalDevices.ToString(), d.ScreenersCount.ToString()
                    }).ToList()
                };
                var excelManager = new ExcelManager();
                var excel = excelManager.GenerateExcel(excelData);
                return excel;
            });
        }

        [HttpGet]
        public HttpResponseMessage GetScreenersTree(string filter = "")
        {
            var manager = new LiveScreeningDatabaseManager();
            var screeners = manager.GetScreeners(ApplicationUser.UserId, null, null, filter, null);

            var data = screeners.GroupBy(r => new { ParentIdColumn = r.Location.Id.ToString(), ParentTextColumn = r.Location.Name.ToString() }).
                Select(pg => string.Format
                  ("<item text=\"{0}({1})\" id=\"{2}_pp\">{3}</item>",
                                (string.IsNullOrEmpty(pg.Key.ParentTextColumn) ? "UNASSIGNED" : pg.Key.ParentTextColumn),
                                pg.Count(),
                                (pg.Key.ParentIdColumn == "0" ? "-1" : pg.Key.ParentIdColumn),

                            string.Join(Environment.NewLine, pg.GroupBy(g => new { Gid = g.Method.Id.ToString(), Gname = g.Method.Name.ToString() }).
                            Select(g => string.Format("<item text=\"{0}({1})\" id=\"{2}_{3}_p\">{4}</item>",
                                        (string.IsNullOrEmpty(g.Key.Gname) ? "UNASSIGNED" : g.Key.Gname),
                                        g.Count(),
                                        pg.Key.ParentIdColumn,
                                        (g.Key.Gid == "0" ? "-1" : g.Key.Gid),
                                        string.Join(Environment.NewLine, g.Select(i => string.Format("<item text=\"{0}\" id=\"{1}_{2}_{3}_i\"></item>",
                                                                                       string.IsNullOrEmpty(i.Name.ToString()) ? "N/A" : i.Name.ToString(),
                                                                                       pg.Key.ParentIdColumn,
                                                                                       (g.Key.Gid == "0" ? "-1" : g.Key.Gid),
                                                                                       i.Id.ToString())).ToList()
                                                    )
                                    )
                    ).ToList()))).ToList();

            var xml = string.Format("<tree id=\"0\">{0}</tree>", string.Join(Environment.NewLine, data));
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        [HttpGet]
        public Grid GetScreenersGrid(string screener = "")
        {
            int? locationId = null;
            int? screenerId = null;
            int? screeningMethodId = null;
            string filter = string.Empty;

            if (screener.EndsWith("_pp"))
                locationId = screener.Replace("_pp", "").TryParse<int?>();
            else if (screener.EndsWith("_p"))
            {
                filter = screener.Replace("_p", "");
                locationId = filter.Split('_')[0].TryParse<int?>();
                screeningMethodId = filter.Split('_')[1].TryParse<int?>();
            }
            else if (screener.EndsWith("_i"))
            {
                filter = screener.Replace("_i", "");
                locationId = filter.Split('_')[0].TryParse<int?>();
                screeningMethodId = filter.Split('_')[1].TryParse<int?>();
                screenerId = filter.Split('_')[2].TryParse<int?>();
            }

            var manager = new LiveScreeningDatabaseManager();
            var screeners = manager.GetScreeners(ApplicationUser.UserId, locationId != null ? locationId.ToString() : null, screeningMethodId != null ? screeningMethodId.ToString() : null, null, screenerId)
                .GroupBy(s => new {s.Id,s.Name,s.Photo,s.Phone,s.Email,s.CellPhone,LocationId = s.Location.Id,LocationName = s.Location.Name})
                .Select(result => new ScreenerModel
                {
                    Id = result.Key.Id,
                    Name = result.Key.Name,
                    Location = new IdNameEntity() { Id = result.Key.LocationId ,Name = result.Key.LocationName},
                    Photo = result.Key.Photo,
                    CellPhone = result.Key.CellPhone,
                    Phone = result.Key.Phone,
                    Email = result.Key.Email,
                    MethodNames = String.Join(",", result.Select(g => g.Method.Name))
                });

            return new Grid
            {
                rows = screeners.Select(d =>
                {
                    var id = string.Format("{0}-{1}", d.Id, d.Location.Id);
                    return new Row { id = id, data = new List<string> { id, d.Photo, d.Name, d.Location.Name, d.MethodNames , d.Phone, d.Email, d.CellPhone} };
                })
            };
        }

        [HttpGet]
        public DataContainer<byte[]> ExportScreeners(string screener = "")
        {
            return MakeTransactional(() =>
            {
                var gridData = GetScreenersGrid(screener);
                if (gridData == null) return null;
                if (gridData.rows.Count() > _excelExportLimit)
                    throw new Exception(
                        String.Format("Your search has {0} records. Max records to download to an excel is {1}",
                            gridData.rows.Count(), _excelExportLimit));

                var excelData = new ExcelData
                {
                    Headers =
                        new List<string>
                        {
                            "Photo","Name","Assigned To CCSF","Screening Methods","Work Phone","Work Email","Cell Phone"
                        },
                    SheetName = "EXPLORER",
                    DataRows = gridData.rows.Select(a => a.data.Skip(1).ToList()).ToList()
                };
                var excelManager = new ExcelManager();
                var excel = excelManager.GenerateExcel(excelData);
                return excel;
            });
        }

        [HttpGet]
        public HttpResponseMessage GetChartsList()
        {
            var manager = new LiveScreeningDatabaseManager();
            var charts = manager.GetChartsList();
            var xmlItems = String.Join(Environment.NewLine,
                charts.Select(s => string.Format(@"<item id=""{0}""><title>{1}</title></item>", s.Id, s.Name)));
            var xml = String.Format(@"<data>{0}</data>", xmlItems);
            return new HttpResponseMessage { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }

        [HttpGet]
        public Grid GetShipmentRemarks(long id)
        {
            var manager = new LiveScreeningDatabaseManager();
            var remarks = manager.GetShipmentRemarks(id);
            if (remarks == null) return null;
            return new Grid
            {
                rows = remarks.Select(s => new Row
                {
                    id = s.Id.ToString(),
                    data = new List<string> { s.User, s.Date.ToString("yyyy/MM/dd HH:mm"), s.Description, s.Id.ToString() }
                })
            };
        }
        [HttpGet]
        public Grid GetShipmentRemarksBySample(string sampleNumber, string reference)
        {
            var manager = new LiveScreeningDatabaseManager();
            var remarks = manager.GetShipmentRemarks(null, reference, sampleNumber);
            if (remarks == null) return null;
            return new Grid
            {
                rows = remarks.Select(s => new Row
                {
                    id = s.Id.ToString(),
                    data = new List<string> { s.SampleNumber, s.User, s.Date.ToString("yyyy/MM/dd HH:mm"), s.Description, s.Id.ToString() }
                })
            };
        }

        [HttpGet]
        public Grid GetScreeningDevices(string device = "")
        {
            long result;
            long? deviceId = null, statusId = null;
            string locationId = null;
            if (long.TryParse(device, out result))
                deviceId = result;
            else if (long.TryParse(device.Replace("_p_p", ""), out result) && result != 0)
                locationId = result.ToString();
            else if (device.StartsWith("0_") && long.TryParse(device.Replace("_p_p", "").TrimStart('0', '_'), out result))
                statusId = result;

            var manager = new LiveScreeningDatabaseManager();
            var devices = manager.GetScreeningDevices(deviceId: deviceId, hostPlusLocationId: locationId, statusId: statusId);
            if (devices == null) return null;

            return new Grid
            {
                rows = devices.OrderBy(s => s.CertifiedDeviceModuleNumber).ThenBy(l => l.SerialNumber).Select(s => new Row
                {
                    id = s.Id.ToString(),
                    data = new List<string> { s.Id.ToString(), s.Image, s.SerialNumber, s.CertifiedDeviceModuleNumber, s.ScreeningMethodTechnology, s.Location }
                })
            };
        }

        [HttpGet]
        public DataContainer<byte[]> ExportScreeningDevices(string device = "")
        {
            return MakeTransactional(() =>
            {
                var screeningDevices = GetScreeningDevices(device);
                if (!screeningDevices.rows.Any()) return null;
                if (screeningDevices.rows.Count() > _excelExportLimit)
                    throw new Exception(
                        String.Format("Your search has {0} records. Max records to download to an excel is {1}",
                            screeningDevices.rows.Count(), _excelExportLimit));
                var excelData = new ExcelData
                {
                    Headers =
                        new List<string>
                        {
                            "Certified Device","Serial No","Model Number","Technology","Assigned to CCSF"
                        },
                    SheetName = "EXPLORER",
                    DataRows = screeningDevices.rows.Select(s => s.data.Skip(1).ToList()).ToList()
                };
                var excelManager = new ExcelManager();
                var excel = excelManager.GenerateExcel(excelData);
                return excel;
            });
        }

        [HttpGet]
        public HttpResponseMessage GetScreeningDevicesTree(string filter = null)
        {
            //no way to rebing json grid
            var manager = new LiveScreeningDatabaseManager();
            var devices = manager.GetScreeningDevices(serialNumber: filter);
            var treeData = devices.Select(r => new
                    {
                        Device = r.Device,
                        DeviceId = r.Id,
                        LocationId = r.LocationId == null ? "0_" + r.StatusId.ToString() : r.LocationId.ToString(),
                        Location = r.Location,
                        Status = r.StatusName,
                    }).GroupBy(r => new { LocationId = r.LocationId + "_p", Location = string.IsNullOrEmpty(r.Location) ? r.Status : r.Location })
                    .OrderBy(s => s.Key.Location)
                    .Select(g => string.Format(
                                        "<item text=\"{0}({1})\" id=\"{2}_p\">{3}</item>", g.Key.Location, g.Count(), g.Key.LocationId,
                                        string.Join(Environment.NewLine, g.OrderBy(i => i.Device).Select(i => string.Format("<item text=\"{0}\" id=\"{1}\"></item>", i.Device, i.DeviceId)))
                                )
                        ).ToList();
            var xml = string.Format("<tree id=\"0\"><item text=\"SCREENING DEVICES\" id=\"rootElement\">{0}</item></tree>", string.Join(Environment.NewLine, treeData));
            return new HttpResponseMessage() { Content = new StringContent(xml, Encoding.UTF8, "application/xml") };
        }
    }
}
