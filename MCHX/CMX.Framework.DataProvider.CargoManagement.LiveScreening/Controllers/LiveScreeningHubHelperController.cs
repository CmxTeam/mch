﻿using System.Linq;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using CMX.Framework.DataProvider.Hubs;
using CMX.Framework.DataProvider.Hubs.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Controllers
{
    public class LiveScreeningHubHelperController : ApiController
    {
        private readonly Users _users = new Users();
        
        [HttpGet]
        public void UpdateTransaction(string deviceId, string transaction, string timestamp)
        {
            var users = _users.AvailableUsers.Where(u => u.DeviceId == deviceId).Select(u => u.ConnectionId).ToList();
            GlobalHost.ConnectionManager.GetHubContext<LiveScreeningHub>().Clients.Clients(users).transactionReceived(transaction, timestamp);

        }

        [HttpGet]
        public void CompleteScreening(string deviceId, string shipment)
        {
            var users = _users.AvailableUsers.Where(u => u.DeviceId == deviceId).Select(u => u.ConnectionId).ToList();
            GlobalHost.ConnectionManager.GetHubContext<LiveScreeningHub>().Clients.Clients(users).completeScreening(shipment);
        }

        [HttpGet]
        public void NotifyUserAboutDisconnectedDevice(string deviceId, string userName)
        {
            var users = _users.AvailableUsers.Where(u => u.DeviceId == deviceId).Select(u => u.ConnectionId).ToList();
            GlobalHost.ConnectionManager.GetHubContext<LiveScreeningHub>().Clients.Clients(users).deviceHasBeenDisconnected(userName);
        }

        [HttpGet]
        public void Test()
        {
            var users = _users.AvailableUsers.Select(u => u.ConnectionId).ToList();
            GlobalHost.ConnectionManager.GetHubContext<LiveScreeningHub>().Clients.All.usersCount(users);                        
        }
    }
}
