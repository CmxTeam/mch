﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Collections.Generic;
using System.Globalization;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.CargoManagement.Controllers;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using DataExporter.Excel;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities.Dictionary;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LiveScreeningController : CmCommonController
    {
        private readonly LiveScreeningDatabaseManager _manager = new LiveScreeningDatabaseManager();

        [HttpGet]
        public DataContainer<HawbModel> TryGetHawb(long locationId, string number)
        {
            return MakeTransactional(() => _manager.TryGetHawb(locationId, number));
        }

        [HttpGet]
        public DataContainer<HawbModel> GetHawbAndCreateScreeningQueueIfExists(long locationId, string number, long deviceId, long userId, string userName, int slac)
        {
            return MakeTransactional(() => _manager.GetHawbAndCreateScreeningQueueIfExists(locationId, number, deviceId, userId, userName, slac));
        }

        [HttpGet]
        public DataContainer<HawbModel> GetHawbAndCreateScreeningQueueIfExists(long locationId, string number, long deviceId, string userName, int slac)
        {
            return MakeTransactional(() => _manager.GetHawbAndCreateScreeningQueueIfExists(locationId, number, deviceId, userName, slac));
        }

        [HttpGet]
        [Authorize]
        public DataContainer<long> CreateScreeningQueue(long locationId, string number, long deviceId, int slac)
        {
            return MakeTransactional(() => _manager.CreateScreeningQueue(locationId, number, deviceId, ApplicationUser.UserId, ApplicationUser.UserName, slac));
        }

        [HttpGet]
        public DataContainer<string> CreateShipment(long userId, long locationId, string number, string origin, string destination, int pieces, int slac, string carrier = null, double? weight = null, string masterbillNo = null)
        {
            return MakeTransactional(() => _manager.CreateShipment(userId, locationId, number, origin, destination, pieces, slac, carrier, weight, masterbillNo));
        }

        [HttpGet]
        public DataContainer<string> CreateShipment(string userName, long locationId, string number, string origin, string destination, int pieces, int slac, string carrier = null, double? weight = null, string masterbillNo = null)
        {
            return MakeTransactional(() => _manager.CreateShipment(userName, locationId, number, origin, destination, pieces, slac, carrier, weight, masterbillNo));
        }

        [HttpGet]
        public DataContainer<ShipmentValidationResultModel> ValidateShipment(long userId, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            return MakeTransactional(() => _manager.ValidateShipment(userId, locationId, number, origin, destination, pieces, slac));
        }

        [HttpGet]
        public DataContainer<ShipmentValidationResultModel> ValidateShipment(string userName, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            var manager = new LiveScreeningDatabaseManager();
            return MakeTransactional(() => manager.ValidateShipment(userName, locationId, number, origin, destination, pieces, slac));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<List<DeviceModel>> GetAvailableXrayDevices(int locationId)
        {
            return MakeTransactional(() => _manager.GetAvailableXRAYDevices(ApplicationUser.UserId, locationId));
        }

        [HttpGet]
        public DataContainer<DeviceModel> GetDeviceById(long userId, int deviceId)
        {
            return MakeTransactional(() => _manager.GetDeviceById(userId, deviceId));
        }

        [HttpGet]
        public DataContainer<DeviceModel> GetDeviceById(string userName, int deviceId)
        {
            return MakeTransactional(() => _manager.GetDeviceById(userName, deviceId));
        }

        [HttpGet]
        public DataContainer<DeviceModel> GetDeviceByBarcode(long userId, string deviceBarcode)
        {
            return MakeTransactional(() => _manager.GetDeviceByBarcode(userId, deviceBarcode));
        }

        [HttpGet]
        public DataContainer<DeviceModel> GetDeviceByBarcode(string userName, string deviceBarcode)
        {
            return MakeTransactional(() => _manager.GetDeviceByBarcode(userName, deviceBarcode));
        }

        [HttpGet]
        public DataContainer<DeviceModel> DisconnectUserFromDevice(int deviceId)
        {
            return MakeTransactional(() => _manager.DisconnectUserFromDevice(deviceId));
        }

        [HttpGet]
        public DataContainer<bool> CompleteScreening(long deviceId)
        {
            return MakeTransactional(() => _manager.CompleteScreening(deviceId));
        }

        [HttpGet]
        public DataContainer<ScanTransactionModel> GetTransactionRefrence(long userId, int deviceId)
        {
            return MakeTransactional(() => _manager.GetTransactionRefrence(userId, deviceId));
        }

        [HttpGet]
        public DataContainer<ScanTransactionModel> GetTransactionRefrence(string userName, int deviceId)
        {
            return MakeTransactional(() => _manager.GetTransactionRefrence(userName, deviceId));
        }
        [HttpGet]
        public DataContainer<bool> CheckQueueExists(long userId, int deviceId)
        {
            return MakeTransactional(() => _manager.CheckQueueExists(userId, deviceId));
        }
        [HttpGet]
        public DataContainer<bool> CheckQueueExists(string userName, int deviceId)
        {
            return MakeTransactional(() => _manager.CheckQueueExists(userName, deviceId));
        }
        [Authorize]
        [HttpGet]
        public DataContainer<IEnumerable<Dictionary<string, string>>> GetShipmentSeals(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            List<long> locationIds = (location != null ? location.Split(',').Select(long.Parse).ToList() : new List<long>());
            var locationNames = _manager.GetScreeningLocations(ApplicationUser.UserId)
                .Where(l => location == null || locationIds.Exists(p => l.Id == p)).Select(l => l.Name).ToList();
            return MakeTransactional(() =>
            {
                return _manager.GetShipmentSeals(locationNames, dateFrom, dateTo).GroupBy(g => g.StationName).OrderBy(g => g.Key).Select(s =>
                {
                    var locInfo = new Dictionary<string, string> {{"StationName", s.Key}};
                    s.Where(a => a.Name != null).ForEach(l =>
                    {
                        if (locInfo.ContainsKey(l.Name))
                            locInfo[l.Name] = (long.Parse(locInfo[l.Name]) + l.DateCount).ToString();
                        else
                            locInfo.Add(l.Name, l.DateCount.ToString());
                    }
                        );
                    return locInfo;
                });
            });
        }

        [Authorize]
        [HttpGet]
        public DataContainer<byte[]> ExportShipmentSeals(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var locationIds = (location != null ? location.Split(',').Select(long.Parse).ToList() : new List<long>());
            var locationNames = _manager.GetScreeningLocations(ApplicationUser.UserId)
                .Where(l => location == null || locationIds.Exists(p => l.Id == p)).Select(l => l.Name).ToList();
            return MakeTransactional(() =>
            {
                var data = _manager.GetShipmentSeals(locationNames, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData =
                data.GroupBy(r => r.StationName).Select(k =>
                {
                    var sheetData = new ExcelData
                                    {
                                        Headers = new List<string> {"Method", "Pieces"},
                                        SheetName = k.Key.ToString(),
                                        DataRows = k.GroupBy(d => d.Name).Select(s => new List<string> {s.Key, s.Sum(t => t.DateCount).ToString()}).ToList()
                                    };
                    return sheetData;
                }).ToList();
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });

        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<ChartStepModel>> GetScreeningVolumesChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() => _manager.GetScreeningVolumesChart(ApplicationUser.UserId, location, dateFrom, dateTo));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<byte[]> ExportScreeningVolumesChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                var data = _manager.GetScreeningVolumesChart(ApplicationUser.UserId, location, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData = new ExcelData
                                       {
                                           Headers = new List<string> {"Location", "Screening Volume"},
                                           DataRows = data.Select(row => new List<string> {row.xValue, row.yValue}).ToList()
                                       };
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });
        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<ChartStepModel>> GetRescreenedVolumesChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() => _manager.GetRescreenedVolumesChart(ApplicationUser.UserId, location, dateFrom, dateTo));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<byte[]> ExportRescreenedVolumesChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                var data = _manager.GetRescreenedVolumesChart(ApplicationUser.UserId, location, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData = new ExcelData
                                 {
                                     Headers = new List<string> {"Location", "Screening Volume"},
                                     DataRows = data.Select(row => new List<string> {row.xValue, row.yValue}).ToList()
                                 };
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });
        }
        [Authorize]
        [HttpGet]
        public DataContainer<IEnumerable<Dictionary<string, string>>> GetHourlyProductionChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                return _manager.GetHourlyProductionChart(ApplicationUser.UserId, location, dateFrom, dateTo).GroupBy(g => g.Date)
               .Select(s =>
               {
                   var locInfo = new Dictionary<string, string> {{"TimeUnit", s.Max(l => l.Name)}};
                   s.ForEach(l =>
                   {
                       if (locInfo.ContainsKey(l.StationName))
                           locInfo[l.StationName] = (long.Parse(locInfo[l.Name]) + l.DateCount).ToString();
                       else
                           locInfo.Add(l.StationName, l.DateCount.ToString());
                   });
                   return locInfo;
               });
            });
        }
        [Authorize]
        [HttpGet]
        public DataContainer<byte[]> ExportHourlyProductionChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                var data = _manager.GetHourlyProductionChart(ApplicationUser.UserId, location, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData =
                data.GroupBy(r => r.StationName).Select(k =>
                {
                    var sheetData = new ExcelData
                                    {
                                        Headers = new List<string>() {"Date", "Pieces"},
                                        SheetName = k.Key.ToString(),
                                        DataRows = k.Select(s => new List<string> {s.Date.ToString(CultureInfo.InvariantCulture), s.DateCount.ToString()}).ToList()
                                    };
                    return sheetData;
                }).ToList();
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<Dictionary<string, string>>> GetTimeResultByLocation(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                return _manager.GetTimeResultByLocation(ApplicationUser.UserId, location, dateFrom, dateTo).GroupBy(g => g.Date)
                .Select(s =>
                {
                    var locInfo = new Dictionary<string, string> {{"TimeUnit", s.Max(l => l.Name)}};
                    s.ForEach(l =>
                    {
                        if (locInfo.ContainsKey(l.StationName))
                            locInfo[l.StationName] = (long.Parse(locInfo[l.Name]) + l.DateCount).ToString();
                        else
                            locInfo.Add(l.StationName, l.DateCount.ToString());
                    });
                    return locInfo;
                });
            });
        }

        [HttpGet]
        [Authorize]
        public DataContainer<byte[]> ExportTimeResultByLocation(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                var data = _manager.GetTimeResultByLocation(ApplicationUser.UserId, location, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData =
                data.GroupBy(r => r.StationName).Select(k =>
                {
                    var sheetData = new ExcelData
                                    {
                                        Headers = new List<string>() {"Date", "Pieces"},
                                        SheetName = k.Key.ToString(),
                                        DataRows = k.Select(s => new List<string> {s.Date.ToString(CultureInfo.InvariantCulture), s.DateCount.ToString()}).ToList()
                                    };
                    return sheetData;
                }).ToList();
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });
        }

        [Authorize]
        [HttpGet]
        public DataContainer<IEnumerable<Dictionary<string, string>>> GetScreeningTechUtilChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() => _manager.GetScreeningTechUtilChart(ApplicationUser.UserId, location, dateFrom, dateTo));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<byte[]> ExportScreeningTechUtilChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                var data = _manager.GetScreeningTechUtil(ApplicationUser.UserId, location, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData =
                data.GroupBy(r => r.zValue).Select(k =>
                {
                    var sheetData = new ExcelData
                                    {
                                        Headers = new List<string> {"Method", "Pieces"},
                                        SheetName = k.Key.ToString(),
                                        DataRows = k.Select(s => new List<string> {s.xValue, s.yValue}).ToList()
                                    };
                    return sheetData;
                }).ToList();
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });
        }
        [Authorize]
        [HttpGet]
        public DataContainer<IEnumerable<Dictionary<string, string>>> GetScreeningResultsChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() => _manager.GetScreeningResultsChart(ApplicationUser.UserId, location, dateFrom, dateTo));
        }
        [Authorize]
        [HttpGet]
        public DataContainer<byte[]> ExportScreeningResultsChart(string location = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            return MakeTransactional(() =>
            {
                var data = _manager.GetScreeningResults(ApplicationUser.UserId, location, dateFrom, dateTo);
                var exportExcel = new ExcelManager();
                var exportData =
                data.GroupBy(r => r.zValue).Select(k =>
                {
                    var sheetData = new ExcelData
                                    {
                                        Headers = new List<string>() {"Method", "Pieces"},
                                        SheetName = k.Key.ToString(),
                                        DataRows = k.Select(s => new List<string>() {s.xValue, s.yValue}).ToList()
                                    };
                    return sheetData;
                }).ToList();
                var exporResult = exportExcel.GenerateExcel(exportData);
                return exporResult;
            });
        }
        [Authorize]
        [HttpGet]
        public DataContainer<bool> IsUserCertifiedforXray()
        {
            return MakeTransactional(() => _manager.IsUserCertifiedforXray(ApplicationUser.UserId));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<SessionExpirationInfo> AssignDeviceToUser(long deviceId)
        {
            return MakeTransactional(() => _manager.AssignDeviceToUser(ApplicationUser.UserId, deviceId));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> CreateFacility(FacilityModel facility)
        {
            return MakeTransactional(() => _manager.CreateFacility(ApplicationUser.UserId, facility));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<FacilityModel> GetFacility(long id)
        {
            return MakeTransactional(() => _manager.GetFacilities(id, ApplicationUser.UserId, null).FirstOrDefault());
        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<CertifiedScreeningFacilitieModel>> GetCertifiedScreeningFacilities(long? id = null)
        {
            return MakeTransactional(() => _manager.GetCertifiedScreeningFacilities(id));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<MapItemModel>> GetHostPlusMapItems()
        {
            return MakeTransactional(() => _manager.GetHostPlusMapItems(ApplicationUser.UserId));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<IdNameEntity>> GetAreas(int? locationId = null)
        {
            return MakeTransactional(() => _manager.GetAreas(locationId));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<IdNameEntity>> GetScreeningMethods()
        {
            return MakeTransactional(() => _manager.GetScreeningMethods());
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<IdNameEntity>> GetCctvCameras(string locationId = null)
        {
            return MakeTransactional(() => _manager.GetCctvCameras(locationId));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<TransactionDetailsModel> GetTransactionDetails(long id)
        {
            return MakeTransactional(() => _manager.GetTransactionDetails(id));
        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<ScreeningImageModel>> GetImagesForTransaction(long id, string method)
        {
            return MakeTransactional(() => _manager.GetImagesForTransaction(id));
        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<IdNameEntity>> GetScreeningResults()
        {
            return MakeTransactional<IEnumerable<IdNameEntity>>(() => new List<IdNameEntity>
            {
                new IdNameEntity{Id = (int)ScreeningResultsEnum.Pass , Name = "PASS" }, 
                new IdNameEntity{Id = (int)ScreeningResultsEnum.Alarm, Name="ALARM"} ,
                new IdNameEntity{Id = (int)ScreeningResultsEnum.AlarmCleared, Name="ALARM CLEARED"}   
            });
        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<IdNameEntity>> GetScreeningDeviceNames(string locationId = null, string screeningMethodId = null, string screeningAreaId = null)
        {
            var manager = new LiveScreeningDatabaseManager();
            var devices = manager.GetScreeningDevices(hostPlusLocationId: locationId, screeningMethodId: screeningMethodId, warehouseLocationId: screeningAreaId);
            return MakeTransactional(() => devices.Select(s => new IdNameEntity { Id = s.Id, Name = s.Device }));
        }

        [HttpPost]
        [Authorize]
        public void UploadTsaMasterList(IdNameEntity file)
        {
            var dataExporter = new ExcelManager();
            dataExporter.ExtractDataFromExcel(file.Name, new List<ExtractSheetInfo> 
            { 
                new ExtractSheetInfo { SheetName = "Active CCSF IACs", StartFromRow = 2 } 
            });
        }
    }
}