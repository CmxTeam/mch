﻿using System;
using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities;
using System.Configuration;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening
{
    public class LiveScreeningDatabaseManager : CmCommonDatabaseManager
    {
        public HawbModel TryGetHawb(long locationId, string number)
        {
            var connectorExecutor = new ConnectorExecutor();
            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            return connectorExecutor.Execute<HawbModel>(() => new GetHawbInfoUnit(localConnection, locationId, number));
        }

        public HawbModel GetHawbAndCreateScreeningQueueIfExists(long locationId, string number, long deviceId, long userId, string userName, int slac)
        {
            var hawb = TryGetHawb(locationId, number);
            if (hawb != null)
                CreateScreeningQueue(locationId, number, deviceId, userId, userName, slac);
            return hawb;
        }

        public HawbModel GetHawbAndCreateScreeningQueueIfExists(long locationId, string number, long deviceId, string userName, int slac)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));

            var hawb = TryGetHawb(locationId, number);
            if (hawb != null)
                CreateScreeningQueue(locationId, number, deviceId, userId, userName, slac);
            return hawb;
        }

        public long CreateScreeningQueue(long locationId, string number, long deviceId, long userId, string userName, int slac)
        {
            var connectorExecutor = new ConnectorExecutor();
            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            return connectorExecutor.Execute<long>(() => new ScreeningQueueCreator(localConnection, number, deviceId, userId, userName, slac));
        }

        public long CreateScreeningQueue(long locationId, string number, long deviceId, string userName, int slac)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));

            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            return connectorExecutor.Execute<long>(() => new ScreeningQueueCreator(localConnection, number, deviceId, userId, userName, slac));
        }

        public string CreateShipment(long userId, long locationId, string number, string origin, string destination, int pieces, int slac, string carrier = null, double? weight = null, string masterbillNo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            return connectorExecutor.Execute<string>(() => new CreateShipmentCreator(localConnection, userId, locationId, number, origin, destination, pieces, slac, carrier, weight, masterbillNo));
        }

        public string CreateShipment(string userName, long locationId, string number, string origin, string destination, int pieces, int slac, string carrier = null, double? weight = null, string masterbillNo = null)
        {
            var connectorExecutor = new ConnectorExecutor();

            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            return connectorExecutor.Execute<string>(() => new CreateShipmentCreator(localConnection, userId, locationId, number, origin, destination, pieces, slac, carrier, weight, masterbillNo));
        }

        public ShipmentValidationResultModel ValidateShipment(long userId, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            var connectorExecutor = new ConnectorExecutor();
            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            return connectorExecutor.Execute<ShipmentValidationResultModel>(() => new ShipmentValidationCreator(localConnection, userId, locationId, number, origin, destination, pieces, slac));
        }

        public ShipmentValidationResultModel ValidateShipment(string userName, long locationId, string number, string origin, string destination, int pieces, int slac)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            var localConnection = GetLocalConnectionString(connectorExecutor, locationId);
            var retval = connectorExecutor.Execute<ShipmentValidationResultModel>(() => new ShipmentValidationCreator(localConnection, userId, locationId, number, origin, destination, pieces, slac));
            return retval;
        }

        public List<DeviceModel> GetAvailableXRAYDevices(long userId, int locationId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<List<DeviceModel>>(() => new GetAvailableXRAYDevicesUnit(userId, locationId));
        }

        public List<DeviceModel> GetAvailableXRAYDevices(string userName, int locationId)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            return connectorExecutor.Execute<List<DeviceModel>>(() => new GetAvailableXRAYDevicesUnit(userId, locationId));
        }

        public DeviceModel GetDeviceById(long userId, int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<DeviceModel>(() => new GetAuthorizedDeviceByIdUnit(userId, deviceId));
        }

        public DeviceModel GetDeviceById(string userName, int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            return connectorExecutor.Execute<DeviceModel>(() => new GetAuthorizedDeviceByIdUnit(userId, deviceId));
        }

        public DeviceModel GetDeviceByBarcode(long userId, string deviceBarcode)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<DeviceModel>(() => new GetAuthorizedDeviceByBarcodeUnit(userId, deviceBarcode));
        }

        public DeviceModel GetDeviceByBarcode(string userName, string deviceBarcode)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            return connectorExecutor.Execute<DeviceModel>(() => new GetAuthorizedDeviceByBarcodeUnit(userId, deviceBarcode));
        }

        public DeviceModel DisconnectUserFromDevice(int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<DeviceModel>(() => new DisconnectUserFromDeviceUnit(deviceId));
        }

        public ScanTransactionModel GetTransactionRefrence(long userId, int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<ScanTransactionModel>(() => new GetTransactionRefrenceUnit(userId, deviceId));
        }

        public ScanTransactionModel GetTransactionRefrence(string userName, int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            return connectorExecutor.Execute<ScanTransactionModel>(() => new GetTransactionRefrenceUnit(userId, deviceId));
        }

        public bool CompleteScreening(long deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<bool>(() => new CompleteScreeningCreator(deviceId));
        }

        public bool CheckQueueExists(long userId, int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<bool>(() => new CheckQueueExistsUnit(userId, deviceId));
        }

        public bool CheckQueueExists(string userName, int deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            var userId = connectorExecutor.Execute<long>(() => new GetUserIdByLoginUnit(userName));
            return connectorExecutor.Execute<bool>(() => new CheckQueueExistsUnit(userId, deviceId));
        }

        public IEnumerable<IdNameEntity> GetScreeningLocations(long? userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<IdNameEntity>>(() => new ScreenerLocationsUnit(userId));
        }

        public bool IsUserCertifiedforXray(long userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<bool>(() => new XrayPermissionUnit(userId));
        }

        public SessionExpirationInfo AssignDeviceToUser(long userId, long deviceId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<SessionExpirationInfo>(() => new AssignDeviceToUserUnit(userId, deviceId));
        }

        public bool CreateFacility(long userId, FacilityModel facility)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<bool>(() => new FacilityCreatorUnit(userId, facility));
        }

        public IEnumerable<FacilityModel> GetFacilities(long? id, long userId, string filter)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<FacilityModel>>(() => new FacilityGetterUnit(id, userId, filter));
        }

        public PageableSet<Transaction> GetScreeningTransactions(DateTime? fromDate, DateTime? toDate, string housebillNumber, string sampleNumber,
            string locationId, string warehouseLocationId, string screeningMethodId, string deviceId, long? userId, string resultCode, bool? isValid,
            int startIndex, int endIndex)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<PageableSet<Transaction>>(() => new ScreeningTransactionsGetterUnit(fromDate, toDate, housebillNumber, sampleNumber,
                locationId, warehouseLocationId, screeningMethodId, deviceId, userId, resultCode, isValid, startIndex, endIndex,false));
        }
        public PageableSet<Transaction> GetGroupedScreeningTransactions(DateTime? fromDate, DateTime? toDate, string housebillNumber, string sampleNumber,
            string locationId, string warehouseLocationId, string screeningMethodId, string deviceId, long? userId, string resultCode, bool? isValid,
            int startIndex, int endIndex)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<PageableSet<Transaction>>(() => new ScreeningTransactionsGetterUnit(fromDate, toDate, housebillNumber, sampleNumber,
                locationId, warehouseLocationId, screeningMethodId, deviceId, userId, resultCode, isValid, startIndex, endIndex, true));
        }
        public IEnumerable<ScreenerModel> GetScreeners(long? userId, string locationId, string screeningMethodId, string screenerName, long? screenerId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ScreenerModel>>(() => new ScreenersGetter(userId, locationId, screeningMethodId, screenerName, screenerId));
        }
      
        public IEnumerable<ScreeningChartModel> GetShipmentSeals(List<string> locations, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            List<ConnectionStringSettings> configLocations = new List<ConnectionStringSettings>();
            List<ScreeningChartModel> shipmentSeals = new List<ScreeningChartModel>();

            //ToDo Review
            foreach (ConnectionStringSettings conn in ConfigurationManager.ConnectionStrings)
                configLocations.Add(conn);

            // Locations from Configuration
            configLocations.Where(l => locations.Exists(s => "CM" + s == l.Name))
                .ForEach(a => shipmentSeals.AddRange(connectorExecutor.Execute<IEnumerable<ScreeningChartModel>>(() => new GetShipmentSealsUnit(a.Name, dateFrom, dateTo))));

            // Locations from MGMSUSGlobalDB
            var sharedLocations = locations.Where(l => !configLocations.Any(c => "CM" + l == c.Name)).ToList();
            if (sharedLocations.Count() != 0)
                shipmentSeals.AddRange(connectorExecutor.Execute<IEnumerable<ScreeningChartModel>>(() => new GetShipmentSealsUnit("CMMGMSUSGlobalDB", dateFrom, dateTo, sharedLocations)));
            shipmentSeals = NormalizeDataForChart(shipmentSeals.ToList(), locations);
            return shipmentSeals.ToList();
        }
        public IEnumerable<ChartStepModel> GetScreeningVolumesChart(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ChartStepModel>>(() => new GetScreeningVolumesChartUnit(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<ChartStepModel> GetRescreenedVolumesChart(long? userId, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ChartStepModel>>(() => new GetRescreenedVolumesChartUnit(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<ScreeningChartModel> GetHourlyProductionChart(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ScreeningChartModel>>(() => new GetHourlyProductionChart(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<ScreeningChartModel> GetTimeResultByLocation(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ScreeningChartModel>>(() => new GetTimeResultByLocation(userId, locations, dateFrom, dateTo));
        }

        public IEnumerable<Dictionary<string, string>> GetScreeningTechUtilChart(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<Dictionary<string, string>>>(() => new GetScreeningTechUtilChart(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<Chart3dStepModel> ExportScreeningTechUtilChart(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<Chart3dStepModel>>(() => new GetScreeningTechUtilChartData(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<Chart3dStepModel> GetScreeningTechUtil(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute < IEnumerable<Chart3dStepModel>>(() => new GetScreeningTechUtilChartData(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<Dictionary<string, string>> GetScreeningResultsChart(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<Dictionary<string, string>>>(() => new GetScreeningResultsChart(userId, locations, dateFrom, dateTo));
        }
        public IEnumerable<Chart3dStepModel> GetScreeningResults(long? userId = null, string locations = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<Chart3dStepModel>>(() => new GetScreeningResultsChartData(userId, locations, dateFrom, dateTo));
        }
        private string GetLocalConnectionString(ConnectorExecutor executor, long locationId)
        {
            return executor.Execute<string>(() => new GetLocalDatabaseConnection(locationId));
        }
        public IEnumerable<CertifiedScreeningFacilitieModel> GetCertifiedScreeningFacilities(long? id = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<CertifiedScreeningFacilitieModel>>(() => new GetCertifiedScreeningFacilities(id));
        }
        public IEnumerable<IdNameEntity> GetChartsList()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<IdNameEntity>>(() => new GetChartsList());
        }
        public IEnumerable<MapItemModel> GetHostPlusMapItems(long userId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<MapItemModel>>(() => new GetHostPlusMapItems(userId));
        }
        public IEnumerable<IdNameEntity> GetAreas(int? locationId = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<IdNameEntity>>(() => new GetAreas(locationId));
        }
        public IEnumerable<IdNameEntity> GetScreeningMethods()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<IdNameEntity>>(() => new GetScreeningMethods());
        }
        public IEnumerable<IdNameEntity> GetCctvCameras(string locationId = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<IdNameEntity>>(() => new GetCctvCameras(locationId));
        }
        public TransactionDetailsModel GetTransactionDetails(long id)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<TransactionDetailsModel>(() => new GetTransactionDetails(id));
        }
        public IEnumerable<ShipmentRemarkModel> GetShipmentRemarks(long? id = null,string reference = null,string sampleNumber = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ShipmentRemarkModel>>(() => new GetShipmentRemarks(id,sampleNumber, reference));
        }
        public IEnumerable<ScreeningImageModel> GetImagesForTransaction(long screeningId)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ScreeningImageModel>>(() => new GetImagesForTransaction(screeningId));
        }
        public IEnumerable<ExtendedScreeningDeviceModel> GetScreeningDevices(long? deviceId = null, string hostPlusLocationId = null, long? statusId = null, long? deviceLocationId = null,
                                                                             string screeningMethodId = null, string warehouseLocationId = null, string serialNumber = null)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute<IEnumerable<ExtendedScreeningDeviceModel>>(() => new GetScreeningDevices(deviceId, hostPlusLocationId, statusId, deviceLocationId, screeningMethodId, warehouseLocationId, serialNumber));
        }

        #region Private members
        private List<ScreeningChartModel> NormalizeDataForChart(List<ScreeningChartModel> data, List<string> inputList)
        {
            if ((data == null || !data.Any()) && inputList != null)
            {
                return inputList.Select(s => new ScreeningChartModel { StationName = s, DateCount = 0 }).ToList();
            }
            var res = data;
            var sealTypes = data.Select(s => s.Name).Distinct().ToList();
            var locations = data.Select(s => s.StationName).Distinct().ToList();
            if (inputList != null)
            {
                var absentLocations = inputList.Where(l => !locations.Exists(i => i == l)).ToList();
                locations = locations.Concat(absentLocations).ToList();
            }
            locations.ForEach(loc => res.AddRange(sealTypes.Select(s => new ScreeningChartModel
            {
                DateCount = 0,
                Name = s,
                StationName = loc
            })));
            return res;
        }
        #endregion
    }
}
