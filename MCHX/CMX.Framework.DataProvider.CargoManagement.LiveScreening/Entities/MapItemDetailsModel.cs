﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class MapItemDetailsModel
    {
        public string CCSFType { get; set; }
        public string SecurityManagerName { get; set; }
        public string Email { get; set; }
        public string Office { get; set; }
        public string Mobile { get; set; }
        public int TotalScannedPieces { get; set; }
        public int TotalPassed { get; set; }
        public int TotalFailed { get; set; }
        public int TotalScreenedPrimary { get; set; }
        public int TotalScreenedSecondary { get; set; }
    }
}
