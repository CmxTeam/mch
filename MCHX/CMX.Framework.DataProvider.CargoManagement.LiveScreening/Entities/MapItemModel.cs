﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class MapItemModel : IdNameEntity
    {
        public string Image { get; set; }
        public int Zoom { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Pin { get; set; }
        public MapItemDetailsModel Details { get; set; }
    }
}
