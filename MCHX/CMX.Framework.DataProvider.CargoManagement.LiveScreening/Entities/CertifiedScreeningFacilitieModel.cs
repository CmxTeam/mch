﻿using System;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class CertifiedScreeningFacilitieModel : IdNameEntity
    {
        public string TSAApprovalNumber { get; set; }
    }
}
