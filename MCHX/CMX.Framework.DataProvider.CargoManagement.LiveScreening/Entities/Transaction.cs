﻿using System;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class Transaction : IdEntity 
    {
        public string AirbillNumber { get; set; }
        public string SampleNumber { get; set; }
        public string Method { get; set; }
        public string DeviceSerial { get; set; }
        public DateTime ScreenedOn { get; set; }
        public string ScreeningLocation { get; set; }
        public int TotalPieces { get; set; }
        public int TotalSlac { get; set; }
        public int ScreenedSlac { get; set; }
        public string ScreeningResult { get; set; }
        public string Screener { get; set; }
        public string IsValid { get; set; }
        public string Remark { get; set; }
    }
}