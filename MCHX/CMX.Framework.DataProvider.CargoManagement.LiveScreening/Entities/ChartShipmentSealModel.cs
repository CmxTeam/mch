﻿namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class ChartShipmentSealModel
    {
        public string StationName { get; set; }
        public long ChainOfCustody { get; set; }
        public long  Tet { get; set; }
        public long SealMissing { get; set; }
        public long SealBroken { get; set; }
    }
}