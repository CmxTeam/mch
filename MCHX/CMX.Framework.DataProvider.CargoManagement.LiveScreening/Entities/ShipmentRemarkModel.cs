﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class ShipmentRemarkModel : IdEntity
    {
        public string User { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string SampleNumber { get; set; }
    }
}
