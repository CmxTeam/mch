﻿using System;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class ScreeningChartModel : IdNameEntity
    {
        public long DateCount { get; set; }
        public string StationName { get; set; }
        public DateTime Date { get; set; }
    }
}
