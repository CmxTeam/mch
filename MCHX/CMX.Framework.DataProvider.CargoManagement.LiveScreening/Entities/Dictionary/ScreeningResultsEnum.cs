﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities.Dictionary
{
    public enum ScreeningResultsEnum
    {
        Pass = 1,
        Alarm = 2,
        AlarmCleared = 3
    }
}
