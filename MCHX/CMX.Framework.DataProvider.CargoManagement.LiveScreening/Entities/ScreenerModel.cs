﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class ScreenerModel : IdNameEntity
    {
        public string Photo { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string MethodNames { get; set; }
        public IdNameEntity Location { get; set; }
        public IdNameEntity Method { get; set; }
    }
}