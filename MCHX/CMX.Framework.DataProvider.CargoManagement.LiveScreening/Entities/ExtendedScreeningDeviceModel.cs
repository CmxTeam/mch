﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.CargoManagement.Entities;

namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class ExtendedScreeningDeviceModel : DeviceModel
    {
        public string Image { get; set; }
        public string Location { get; set; }       
        public string StatusName { get; set; }
        public string CertifiedDeviceModuleNumber { get; set; }
        public string ScreeningMethodTechnology { get; set; }
        public string Device { get { return ScreeningMethodTechnology + "-" + SerialNumber; } }
    }
}
