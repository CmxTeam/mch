﻿namespace CMX.Framework.DataProvider.CargoManagement.LiveScreening.Entities
{
    public class ChartStepModel
    {
        public string id { get; set; }
        public string xValue { get; set; }
        public string yValue { get; set; }
    }
}