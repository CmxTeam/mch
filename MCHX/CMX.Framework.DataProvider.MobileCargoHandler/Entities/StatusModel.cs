﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class StatusModel : IdNameEntity
    {
        public bool IsIndication { get; set; }

        public bool IsWorkFlowItem { get; set; }
       
        public string DisplayName { get; set; }
        public string StatusImagePath { get; set; }

        public string Description { get; set; }
    }
}