﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class LocationItemModel
    {
        public string Location { get; set; }
        public string LocationBarcode { get; set; }
        public long LocationId { get; set; }
        public string LocationPrefix { get; set; }
    }
}
