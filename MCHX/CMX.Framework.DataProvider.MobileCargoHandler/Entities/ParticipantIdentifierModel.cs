﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class ParticipantIdentifierModel : IdEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
