﻿using System;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class ShipmentModel : ShipmentEntityModel
    {
        public long SLAC { get; set; }
        public long Pieces { get; set; }  
        public int? PercentReceived { get; set; }
        public long ForkliftPieces { get; set; }
        public long ReceivedPieces { get; set; }
        public int? WeightUOMId { get; set; }         
        public string DescriptionOfGoods { get; set; }
        public string SerialNumber { get; set; }       
        public double Weight { get; set; }
        public DateTime? RecDate { get; set; }
        public PortModel Origin { get; set; }
        public PortModel Destination { get; set; }     
        public DateTime? StatusTimeStamp { get; set; }
        public CustomerModel Shipper { get; set; }
        public CustomerModel Consignee { get; set; }
        public StatusModel Status { get; set; }
        public CustomsHistoryModel CustomsClearance { get; set; }
        public IEnumerable<CustomsHistoryModel> CustomsFullHistory { get; set; }
    }
}
