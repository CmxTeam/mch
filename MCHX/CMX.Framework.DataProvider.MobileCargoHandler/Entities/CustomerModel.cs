﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class CustomerModel : IdNameEntity
    {
        public string City { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }        
        public string PostalCode { get; set; }        
        public string AccountNumber { get; set; }
        public bool? IsResidential { get; set; }
        public StateModel State { get; set; }
        public CountryModel Country { get; set; }
        public AccountModel Acount { get; set; }
    }
}
