﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class ShipmentEntityModel : IdEntity
    {
        public virtual EntityTypeEnum EntityType { get; set; }
    }
}
