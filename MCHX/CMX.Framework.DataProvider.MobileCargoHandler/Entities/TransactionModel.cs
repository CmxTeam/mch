﻿using System;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class TransactionModel : IdEntity
    {
        public EntityTypeEnum EntityType { get; set; }
        public long? EntityId { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }        
        public StatusModel Status { get; set; }
        public DateTime? StatusTimestamp { get; set; }
        public TransactionActionModel TransactionAction { get; set; }
    }
}
