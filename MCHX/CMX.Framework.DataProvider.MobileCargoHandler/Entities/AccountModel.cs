﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class AccountModel : IdEntity
    { 
        public bool IsOwner { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }        
        public CustomerModel ShellAccount { get; set; }
    }
}
