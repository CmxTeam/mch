﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class AwbShipmentModel : ShipmentModel
    {
        public override EntityTypeEnum EntityType { get { return EntityTypeEnum.AWB; } }
    }
}
