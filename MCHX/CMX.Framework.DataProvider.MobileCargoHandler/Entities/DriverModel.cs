﻿using System;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class DriverModel : IdEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string DriverPhoto { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseImage { get; set; }        
        public string DriverThumbnail { get; set; }
        public string STANumber { get; set; }
        public DateTime LicenseExpiry { get; set; }
        public CarrierModel TruckingCompany { get; set; } //Trucking Carrier
        public StateModel State { get; set; }
    }
}