﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class TransactionActionModel : IdEntity
    {
        public string Code { get; set; }
    }
}