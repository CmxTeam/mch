﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities.SearchModels
{
    public class ShipmentFilterModel
    {
        public long? ShipmentId{get;set;}
        public string CarrierCode{get;set;}
        public string SerialNumber { get; set; }
        public EntityTypeEnum? EntityType { get; set; }
    }
}