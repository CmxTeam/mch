﻿using CMX.Framework.DataProvider.Entities.Common;
using System;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class CustomsHistoryModel : IdEntity
    {
        public long EntityId { get; set; }
        public string Comment { get; set; }                
        public int? StatusID { get; set; }
        public string Master { get; set; }
        public string House { get; set; }       
        public string EntryNo { get; set; }
        public string StatusCode { get; set; }  
        public string MessageType { get; set; }
        public string PartialIdCode { get; set; }
        public long PiecesAffectedCount { get; set; }
        public DateTime Date { get; set; }
        public AmsCodeModel CustomsCode { get; set; }
        public EntityTypeEnum EntityType { get; set; }      
        public DateTime? TransactionDateTime { get; set; }

    }
}
