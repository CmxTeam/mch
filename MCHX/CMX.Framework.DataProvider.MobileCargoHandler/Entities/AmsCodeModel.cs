﻿using CMX.Framework.DataProvider.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class AmsCodeModel : IdEntity
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public bool IsGreenCode { get; set; }
    }
}
