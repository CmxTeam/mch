﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public class AgentModel : IdNameEntity
    {
        public string Place { get; set; }
        public string NumericCode { get; set; }
        public string CASSAddress { get; set; }
        public string AccountNumber { get; set; }
      //  public Customer Consignee { get; set; }
        public ParticipantIdentifierModel Participant { get; set; }
        
    }
}
