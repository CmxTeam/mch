﻿namespace CMX.Framework.DataProvider.MobileCargoHandler.Entities
{
    public enum TaskTypeEnum
    {
        RecoverFreight = 1,
        Inventory = 2,
        FreightDischarge = 3,
        Snapshot = 4,
        BuildFreight = 5,
        TenderToRamp = 6,
        ScreenFreight = 7,
        DangerousGoodsChecklist = 8,
        AcceptFreight = 9,
        LiveAnimalChecklist = 10,
        HighValueChecklist = 11,
        HumanRemainsChecklist = 12,
        FreightAcceptance = 13,
        ReleaseFreight = 14
    }
}