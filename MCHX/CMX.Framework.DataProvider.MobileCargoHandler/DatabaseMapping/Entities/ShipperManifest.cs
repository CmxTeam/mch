namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ShipperManifest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ShipperManifest()
        {
            ScreeningVerificationLogs = new HashSet<ScreeningVerificationLog>();
            ShipperManifests_AWBs = new HashSet<ShipperManifests_AWBs>();
            ShipperVerificationLogs = new HashSet<ShipperVerificationLog>();
            SpecialHandlingLogs = new HashSet<SpecialHandlingLog>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public int AccountId { get; set; }

        public int AircraftTypeId { get; set; }

        public long ShipperTypeId { get; set; }

        public long? ShipperId { get; set; }

        [Required]
        public string ShipperName { get; set; }

        public bool ShipperStatusVerified { get; set; }

        public int? GovCredTypeId { get; set; }

        public string ShipperConsentSignature { get; set; }

        public long? DriverSecurityLogId { get; set; }

        public long UserId { get; set; }

        public int WarehouseId { get; set; }

        public int StatusId { get; set; }

        public DateTime StatusTimestamp { get; set; }

        public virtual Account Account { get; set; }

        public virtual AircraftType AircraftType { get; set; }

        public virtual CredentialType CredentialType { get; set; }

        public virtual DriverSecurityLog DriverSecurityLog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningVerificationLog> ScreeningVerificationLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperManifests_AWBs> ShipperManifests_AWBs { get; set; }

        public virtual ShipperType ShipperType { get; set; }

        public virtual Status Status { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperVerificationLog> ShipperVerificationLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingLog> SpecialHandlingLogs { get; set; }
    }
}
