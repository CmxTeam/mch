namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningDevice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScreeningDevice()
        {
            ScreeningTransactions = new HashSet<ScreeningTransaction>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string SerialNo { get; set; }

        public long? DeviceTypeId { get; set; }

        public int? WarehouseId { get; set; }

        [StringLength(50)]
        public string CurrentSoftwareVersion { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(255)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(50)]
        public string Manufacturer { get; set; }

        public virtual ScreeningMethod ScreeningMethod { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningTransaction> ScreeningTransactions { get; set; }
    }
}
