namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CheckListItemHeader
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CheckListItemHeader()
        {
            CheckListItems = new HashSet<CheckListItem>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(255)]
        public string ListItemHeader { get; set; }

        public int SortOrder { get; set; }

        public bool IsActive { get; set; }

        public long CheckListId { get; set; }

        public virtual CheckList CheckList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckListItem> CheckListItems { get; set; }
    }
}
