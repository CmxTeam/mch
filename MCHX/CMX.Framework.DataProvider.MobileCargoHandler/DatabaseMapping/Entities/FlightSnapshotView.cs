namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FlightSnapshotView")]
    public partial class FlightSnapshotView
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long FlightSnapshotId { get; set; }

        public int? Count { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public DateTime? Timestamp { get; set; }

        public long? FlightManifestId { get; set; }

        public string Conditions { get; set; }

        public byte[] SnapshotImage { get; set; }

        [StringLength(55)]
        public string EntityNumber { get; set; }
    }
}
