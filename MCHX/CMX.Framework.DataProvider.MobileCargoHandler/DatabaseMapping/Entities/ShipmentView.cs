namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShipmentView")]
    public partial class ShipmentView
    {
        public long? FlightManifestId { get; set; }

        [StringLength(12)]
        public string HWBSerialNumber { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long HwbId { get; set; }

        [StringLength(50)]
        public string TrackingNumber { get; set; }

        [StringLength(100)]
        public string Number { get; set; }

        public int? StatusId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long PackageId { get; set; }
    }
}
