namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningRemark
    {
        public long Id { get; set; }

        [Required]
        public string Remark { get; set; }

        [Required]
        [StringLength(3)]
        public string Station { get; set; }
    }
}
