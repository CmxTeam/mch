namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ULDType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ULDType()
        {
            Overpacks = new HashSet<Overpack>();
            ULDs = new HashSet<ULD>();
        }

        public int Id { get; set; }

        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public int? DimUOMId { get; set; }

        public double? MaxWeight { get; set; }

        public double? TareWeight { get; set; }

        public int? WeightUOMId { get; set; }

        public double? MaxVolume { get; set; }

        public int? VolumeUOMId { get; set; }

        public int? Size { get; set; }

        public bool? IsDefault { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Overpack> Overpacks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ULD> ULDs { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }

        public virtual UOM UOM2 { get; set; }
    }
}
