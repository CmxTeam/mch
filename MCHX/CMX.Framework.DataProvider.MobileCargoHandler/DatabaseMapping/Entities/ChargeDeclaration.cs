namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ChargeDeclaration
    {
        public int Id { get; set; }

        public DateTime RecDate { get; set; }

        public long? AWBId { get; set; }

        public long? HWBId { get; set; }

        public bool IsWeightCollect { get; set; }

        public bool IsOtherChargesCollect { get; set; }

        [Column(TypeName = "money")]
        public decimal? CarriageDeclaredValue { get; set; }

        [Column(TypeName = "money")]
        public decimal? CustomsDeclaredValue { get; set; }

        [Column(TypeName = "money")]
        public decimal? InsuranceDeclaredValue { get; set; }

        public bool IsNoCarriageValue { get; set; }

        public bool IsNoCustomsValue { get; set; }

        public bool IsNoInsuranceValue { get; set; }

        public long CurrencyId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual HWB HWB { get; set; }
    }
}
