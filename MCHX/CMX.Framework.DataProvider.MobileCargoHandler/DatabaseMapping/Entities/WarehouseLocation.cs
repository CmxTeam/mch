namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WarehouseLocation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WarehouseLocation()
        {
            Dispositions = new HashSet<Disposition>();
            FlightManifests = new HashSet<FlightManifest>();
            FlightManifests1 = new HashSet<FlightManifest>();
            HWBs = new HashSet<HWB>();
            Onhands = new HashSet<Onhand>();
            Packages = new HashSet<Package>();
            Tasks = new HashSet<Task>();
            ULDs = new HashSet<ULD>();
            UnloadingPorts = new HashSet<UnloadingPort>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int? WarehouseId { get; set; }

        public int? LocationTypeId { get; set; }

        [StringLength(50)]
        public string Barcode { get; set; }

        public int? ZoneId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disposition> Dispositions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightManifest> FlightManifests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightManifest> FlightManifests1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HWB> HWBs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Onhand> Onhands { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Package> Packages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Task> Tasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ULD> ULDs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UnloadingPort> UnloadingPorts { get; set; }

        public virtual WarehouseLocationType WarehouseLocationType { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        public virtual WarehouseZone WarehouseZone { get; set; }
    }
}
