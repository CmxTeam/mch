namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ITDX_Transactions
    {
        public long Id { get; set; }

        public int ScreeningTransactionId { get; set; }

        public DateTime RecDate { get; set; }

        public long DeviceId { get; set; }

        [Required]
        [StringLength(50)]
        public string SerialNumber { get; set; }

        [StringLength(50)]
        public string HostIP { get; set; }

        [StringLength(50)]
        public string ItdxIP { get; set; }

        [StringLength(50)]
        public string TcpPort { get; set; }

        [StringLength(50)]
        public string UdpPort { get; set; }

        [StringLength(50)]
        public string SoftwareVersion { get; set; }

        [StringLength(50)]
        public string InformationSource { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        [StringLength(50)]
        public string SystemMessage { get; set; }

        [StringLength(50)]
        public string SystemReadiness { get; set; }

        [StringLength(50)]
        public string ScreeningResults { get; set; }

        [StringLength(50)]
        public string SubstancesFound { get; set; }

        [StringLength(50)]
        public string LastSample { get; set; }

        [StringLength(50)]
        public string LastCalibration { get; set; }

        [StringLength(50)]
        public string Warning { get; set; }

        [StringLength(50)]
        public string CurrentUser { get; set; }

        [StringLength(50)]
        public string DetectionMode { get; set; }

        [StringLength(50)]
        public string DetectorFlowAverage { get; set; }

        [StringLength(50)]
        public string DetectorFlowRaw { get; set; }

        [StringLength(50)]
        public string SampleFlowAverage { get; set; }

        [StringLength(50)]
        public string SampleFlowRaw { get; set; }

        [StringLength(50)]
        public string DesorberTemperature { get; set; }

        [StringLength(50)]
        public string DesorberVoltage { get; set; }

        [StringLength(50)]
        public string DetectorTemperature { get; set; }

        [StringLength(50)]
        public string DetectorVoltage { get; set; }

        [StringLength(50)]
        public string AbsolutePressureAverage { get; set; }

        [StringLength(50)]
        public string AbsolutePressureRaw { get; set; }

        public long? UserId { get; set; }

        public bool IsActive { get; set; }

        public DateTime? LastConnected { get; set; }

        public virtual ITDX ITDX { get; set; }

        public virtual ScreeningTransaction ScreeningTransaction { get; set; }
    }
}
