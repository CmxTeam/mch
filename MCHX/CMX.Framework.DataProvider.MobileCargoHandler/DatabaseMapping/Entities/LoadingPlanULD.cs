namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LoadingPlanULD
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long LoadingPlanId { get; set; }

        public int UnitTypeId { get; set; }

        [StringLength(30)]
        public string SerialNumber { get; set; }

        public int? OwnerId { get; set; }

        public int? LoadingIndicatorId { get; set; }

        public int? WeightUOMId { get; set; }

        public double? Weight { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual LoadingIndicator LoadingIndicator { get; set; }

        public virtual LoadingPlan LoadingPlan { get; set; }

        public virtual ShipmentUnitType ShipmentUnitType { get; set; }

        public virtual UOM UOM { get; set; }
    }
}
