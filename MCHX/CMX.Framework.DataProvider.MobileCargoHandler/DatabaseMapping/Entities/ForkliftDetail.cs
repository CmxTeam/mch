namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ForkliftDetail
    {
        public long Id { get; set; }

        public long? HWBId { get; set; }

        public long? AWBId { get; set; }

        public int? Pieces { get; set; }

        public long? ULDId { get; set; }

        public long? TaskId { get; set; }

        public DateTime? Timestamp { get; set; }

        public long? PackageId { get; set; }

        public long? UserId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual Package Package { get; set; }

        public virtual ULD ULD { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
