namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MCHTaskManagerView")]
    public partial class MCHTaskManagerView
    {
        public int? WarehouseId { get; set; }

        [Key]
        [Column(Order = 0, TypeName = "date")]
        public DateTime TaskDate { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(256)]
        public string Department { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartmentId { get; set; }

        [StringLength(50)]
        public string TaskType { get; set; }

        public int? TaskTypeId { get; set; }

        public int? AlertCount { get; set; }

        public string Indicators { get; set; }

        public bool? TaskPriority { get; set; }

        [StringLength(5)]
        public string Airline { get; set; }

        [StringLength(100)]
        public string Reference { get; set; }

        public DateTime? DueOn { get; set; }

        [StringLength(100)]
        public string TaskStatus { get; set; }

        public double? TaskProgress { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool IsReopened { get; set; }

        public string TaskAssignees { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? Ulds { get; set; }

        public int? Pieces { get; set; }

        public double? Weight { get; set; }

        [StringLength(2)]
        public string WeightUOM { get; set; }

        [StringLength(50)]
        public string WarehouseLocation { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long TaskId { get; set; }

        public long? EntityId { get; set; }

        public int? EntityTypeId { get; set; }
    }
}
