namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Manifest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Manifest()
        {
            ManifestAttachments = new HashSet<ManifestAttachment>();
            Packages = new HashSet<Package>();
            Manifests1 = new HashSet<Manifest>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(100)]
        public string Number { get; set; }

        public int? TotalPieces { get; set; }

        public int? TotalStc { get; set; }

        public double? TotalWeight { get; set; }

        public double? TotalVolume { get; set; }

        [StringLength(50)]
        public string WeightUOMCombined { get; set; }

        [StringLength(50)]
        public string VolumeUOMCombined { get; set; }

        public int? TotalUlds { get; set; }

        public DateTime? DepartureDate { get; set; }

        public DateTime? ArrivalDate { get; set; }

        [StringLength(50)]
        public string VoyageFlightTruck { get; set; }

        public int TypeId { get; set; }

        public int StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public long? ParentId { get; set; }

        public int? UomWeightId { get; set; }

        public int? UomVolumeId { get; set; }

        public long OriginId { get; set; }

        public long DestinationId { get; set; }

        public int? MotId { get; set; }

        public int? CarrierId { get; set; }

        public int? ServiceTypeId { get; set; }

        public DateTime? CloseOutTime { get; set; }

        public int? LocationId { get; set; }

        public int? AccountId { get; set; }

        public string LabelZPL { get; set; }

        public DateTime? DeliveredOn { get; set; }

        [StringLength(50)]
        public string DeliveredTo { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual CarrierServiceType CarrierServiceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestAttachment> ManifestAttachments { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Package> Packages { get; set; }

        public virtual Port Port { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Manifest> Manifests1 { get; set; }

        public virtual Manifest Manifest1 { get; set; }

        public virtual ManifestType ManifestType { get; set; }

        public virtual MOT MOT { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }
    }
}
