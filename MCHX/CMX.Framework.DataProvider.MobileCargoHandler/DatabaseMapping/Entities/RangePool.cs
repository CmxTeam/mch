namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RangePool
    {
        public int Id { get; set; }

        public int? AccountId { get; set; }

        public int? RangeTypeId { get; set; }

        [StringLength(20)]
        public string Prefix { get; set; }

        public int? FromSeq { get; set; }

        public int? ToSeq { get; set; }

        [StringLength(20)]
        public string Suffix { get; set; }

        public bool? IsCheckDigitCalculated { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(50)]
        public string AddedBy { get; set; }

        public int? MotId { get; set; }

        public int? SeqLength { get; set; }

        public bool? DateBased { get; set; }

        public int? CurrentCounter { get; set; }

        public int? RangeTotal { get; set; }

        public int? RangeCounter { get; set; }

        public int WarehouseId { get; set; }

        public virtual Account Account { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual MOT MOT { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
