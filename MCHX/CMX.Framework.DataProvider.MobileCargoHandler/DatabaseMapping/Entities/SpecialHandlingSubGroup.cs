namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialHandlingSubGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SpecialHandlingSubGroup()
        {
            SpecialHandlingLogs = new HashSet<SpecialHandlingLog>();
            SpecialHandlingSettings = new HashSet<SpecialHandlingSetting>();
        }

        public long Id { get; set; }

        public long SPHGroupId { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public int SPHCodeId { get; set; }

        [Required]
        [StringLength(100)]
        public string DisplayName { get; set; }

        public virtual SpecialHandlingCode SpecialHandlingCode { get; set; }

        public virtual SpecialHandlingGroup SpecialHandlingGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingLog> SpecialHandlingLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingSetting> SpecialHandlingSettings { get; set; }
    }
}
