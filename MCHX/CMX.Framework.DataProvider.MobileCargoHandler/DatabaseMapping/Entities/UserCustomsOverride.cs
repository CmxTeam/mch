namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserCustomsOverride
    {
        public long Id { get; set; }

        public long CustomsStatusId { get; set; }

        public DateTime Date { get; set; }

        public string Comment { get; set; }

        public long PiecesAffectedCount { get; set; }

        public int EntityTypeId { get; set; }

        public long EntityId { get; set; }

        public virtual AmsCode AmsCode { get; set; }

        public virtual EntityType EntityType { get; set; }
    }
}
