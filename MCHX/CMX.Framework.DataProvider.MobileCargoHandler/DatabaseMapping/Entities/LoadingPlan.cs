namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LoadingPlan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LoadingPlan()
        {
            LoadingPlanULDs = new HashSet<LoadingPlanULD>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long FlightManifestId { get; set; }

        public int UnloadingPortId { get; set; }

        public long AWBId { get; set; }

        [StringLength(1)]
        public string ShipmentCode { get; set; }

        public int? LoadPieces { get; set; }

        public int? WeightUOMId { get; set; }

        public double? LoadWeight { get; set; }

        public int? VolumeUOMId { get; set; }

        public double? LoadVolume { get; set; }

        public bool? DensityIndicator { get; set; }

        public int? DensityGroupId { get; set; }

        public int? MovementPriorityCodeId { get; set; }

        public int? ULDCount { get; set; }

        [StringLength(50)]
        public string InwardFlightInfo { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual DensityGroup DensityGroup { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        public virtual MovementPriorityCode MovementPriorityCode { get; set; }

        public virtual UnloadingPort UnloadingPort { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoadingPlanULD> LoadingPlanULDs { get; set; }
    }
}
