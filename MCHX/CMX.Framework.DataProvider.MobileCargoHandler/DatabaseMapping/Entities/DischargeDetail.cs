namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DischargeDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DischargeDetail()
        {
            DischargeCharges = new HashSet<DischargeCharge>();
        }

        public long Id { get; set; }

        public long? DischargeId { get; set; }

        public long? AWBId { get; set; }

        public long? HWBId { get; set; }

        public string Reference { get; set; }

        public int? AvailablePieces { get; set; }

        public int? ReleasedPieces { get; set; }

        public int? StatusId { get; set; }

        public string DriverSignature { get; set; }

        public DateTime? SignTimestamp { get; set; }

        public virtual AWB AWB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeCharge> DischargeCharges { get; set; }

        public virtual Discharge Discharge { get; set; }

        public virtual HWB HWB { get; set; }
    }
}
