namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Alert
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Alert()
        {
            AlertEntities = new HashSet<AlertEntity>();
            AlertEvents = new HashSet<AlertEvent>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long UserId { get; set; }

        [Required]
        public string AlertMessage { get; set; }

        public bool IsHold { get; set; }

        public bool ShowInWarehouse { get; set; }

        public bool ShowInOffice { get; set; }

        public bool Email { get; set; }

        public string EmailDistributionList { get; set; }

        public int AlertFrequencyTypeId { get; set; }

        public DateTime? LastAlertedTime { get; set; }

        public long? AssignedToUserId { get; set; }

        public int? WarehouseId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlertEntity> AlertEntities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlertEvent> AlertEvents { get; set; }

        public virtual AlertFrequencyType AlertFrequencyType { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
