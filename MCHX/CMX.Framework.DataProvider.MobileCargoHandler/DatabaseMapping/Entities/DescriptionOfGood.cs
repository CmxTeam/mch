namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DescriptionOfGood
    {
        public int Id { get; set; }

        public DateTime? RecDate { get; set; }

        public long? HWBId { get; set; }

        [StringLength(65)]
        public string Description { get; set; }

        public virtual HWB HWB { get; set; }
    }
}
