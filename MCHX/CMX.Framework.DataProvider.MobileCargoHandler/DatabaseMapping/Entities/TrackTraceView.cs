namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TrackTraceView")]
    public partial class TrackTraceView
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Carrier { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(55)]
        public string FlightNumber { get; set; }

        [StringLength(4000)]
        public string ArrivalTime { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(3)]
        public string Type { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId { get; set; }

        [StringLength(24)]
        public string Reference { get; set; }

        [StringLength(2)]
        public string Part { get; set; }

        public int? HBCount { get; set; }

        public int? ReceivedPieces { get; set; }

        [StringLength(34)]
        public string Weight { get; set; }

        public double? Weight1 { get; set; }

        [StringLength(4000)]
        public string StorageStartDate { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(20)]
        public string CustomsCode { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(3)]
        public string GeneralOrder { get; set; }

        [StringLength(4000)]
        public string GeneralOrderDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? ISC { get; set; }

        public double? StorageChargesPerDay { get; set; }

        public double? CurrentStorageCharges { get; set; }

        public double? OutstandingBalance { get; set; }

        [StringLength(3)]
        public string UnloadingPort { get; set; }

        [StringLength(6)]
        public string FirmCode { get; set; }

        [StringLength(3)]
        public string FinalDestination { get; set; }
    }
}
