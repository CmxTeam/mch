namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CIMPTestingTable")]
    public partial class CIMPTestingTable
    {
        public long Id { get; set; }

        [Required]
        [StringLength(10)]
        public string MessageCode { get; set; }

        public bool Status { get; set; }

        [StringLength(8)]
        public string AWB { get; set; }

        [StringLength(3)]
        public string Carrier { get; set; }

        [StringLength(3)]
        public string Org { get; set; }

        [StringLength(3)]
        public string Dest { get; set; }

        [StringLength(4)]
        public string TotalPieces { get; set; }

        [StringLength(1)]
        public string Partial { get; set; }

        [StringLength(4)]
        public string LoadPieces { get; set; }

        [StringLength(10)]
        public string CarrFlightNumber { get; set; }

        [StringLength(2)]
        public string ETADay { get; set; }

        [StringLength(3)]
        public string ETAMonth { get; set; }

        [StringLength(4)]
        public string ETATime { get; set; }

        [StringLength(2)]
        public string RcvDay { get; set; }

        [StringLength(3)]
        public string RcvMonth { get; set; }

        [StringLength(4)]
        public string RcvTime { get; set; }

        [StringLength(35)]
        public string Shipper { get; set; }

        [StringLength(2)]
        public string TransferCarr { get; set; }

        [StringLength(2)]
        public string TransferDay { get; set; }

        [StringLength(3)]
        public string TransferMonth { get; set; }

        [StringLength(4)]
        public string TransferTime { get; set; }

        [StringLength(2)]
        public string ETDDay { get; set; }

        [StringLength(3)]
        public string ETDMonth { get; set; }

        [StringLength(4)]
        public string ETDTime { get; set; }

        [StringLength(2)]
        public string DLVDay { get; set; }

        [StringLength(3)]
        public string DLVMonth { get; set; }

        [StringLength(4)]
        public string DLVTime { get; set; }

        [StringLength(35)]
        public string Consignee { get; set; }

        [StringLength(6)]
        public string TransferManifest { get; set; }

        public int? ShpId { get; set; }

        public int? CustId { get; set; }

        public int? AgtId { get; set; }

        [StringLength(12)]
        public string HWB { get; set; }

        [StringLength(15)]
        public string GoodsDesc { get; set; }

        public long? FMId { get; set; }
    }
}
