namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ULD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ULD()
        {
            AWBs_ULDs = new HashSet<AWBs_ULDs>();
            Dimensions = new HashSet<Dimension>();
            Dispositions = new HashSet<Disposition>();
            FlightNotocDGDetails = new HashSet<FlightNotocDGDetail>();
            FlightNotocOtherCargoDetails = new HashSet<FlightNotocOtherCargoDetail>();
            FlightSnapshots = new HashSet<FlightSnapshot>();
            ForkliftDetails = new HashSet<ForkliftDetail>();
            HostPlus_CIMPOutboundQueue = new HashSet<HostPlus_CIMPOutboundQueue>();
            LegSegments = new HashSet<LegSegment>();
            ManifestAWBDetails = new HashSet<ManifestAWBDetail>();
            ManifestDetails = new HashSet<ManifestDetail>();
            ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            OCIs = new HashSet<OCI>();
            OSDs = new HashSet<OSD>();
            SpecialHandlings = new HashSet<SpecialHandling>();
        }

        public long Id { get; set; }

        [StringLength(30)]
        public string SerialNumber { get; set; }

        public int? OwnerId { get; set; }

        public long? FlightManifestId { get; set; }

        public int? TotalUnitCount { get; set; }

        public int? TotalReceivedCount { get; set; }

        [StringLength(50)]
        public string RecoveredBy { get; set; }

        public DateTime? RecoveredOn { get; set; }

        public int? StatusID { get; set; }

        public int? TotalPieces { get; set; }

        public double? TotalWeight { get; set; }

        public double? TotalVolume { get; set; }

        public int? ReceivedPieces { get; set; }

        public int? PutAwayPieces { get; set; }

        public double? PercentRecovered { get; set; }

        public double? PercentReceived { get; set; }

        public double? PercentPutAway { get; set; }

        public int? UnitTypeId { get; set; }

        public int? LoadingIndicatorId { get; set; }

        [StringLength(53)]
        public string Remarks { get; set; }

        public int? ULDVolumeCodeId { get; set; }

        public int? WeightUOMId { get; set; }

        public int? VolumeUOMId { get; set; }

        public DateTime? RecDate { get; set; }

        public bool? IsBUP { get; set; }

        public int? ForkliftPieces { get; set; }

        public bool? IsOSD { get; set; }

        public int? UnloadingPortId { get; set; }

        public int? LocationId { get; set; }

        public double? TareWeight { get; set; }

        public double? FreightWeight { get; set; }

        public int? UldTypeId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AWBs_ULDs> AWBs_ULDs { get; set; }

        public virtual Carrier Carrier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Dimension> Dimensions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disposition> Dispositions { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocDGDetail> FlightNotocDGDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocOtherCargoDetail> FlightNotocOtherCargoDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightSnapshot> FlightSnapshots { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForkliftDetail> ForkliftDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HostPlus_CIMPOutboundQueue> HostPlus_CIMPOutboundQueue { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LegSegment> LegSegments { get; set; }

        public virtual LoadingIndicator LoadingIndicator { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestAWBDetail> ManifestAWBDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OCI> OCIs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSD> OSDs { get; set; }

        public virtual ShipmentUnitType ShipmentUnitType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandling> SpecialHandlings { get; set; }

        public virtual Status Status { get; set; }

        public virtual ULDType ULDType { get; set; }

        public virtual ULDVolumeCode ULDVolumeCode { get; set; }

        public virtual UnloadingPort UnloadingPort { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }
    }
}
