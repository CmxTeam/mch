namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialHandlingVerificationSetting
    {
        public long Id { get; set; }

        public long SPHGroupId { get; set; }

        public long FieldId { get; set; }

        public bool IsMandatory { get; set; }

        public int SortOrder { get; set; }

        public virtual ShipperVerificationField ShipperVerificationField { get; set; }

        public virtual SpecialHandlingGroup SpecialHandlingGroup { get; set; }
    }
}
