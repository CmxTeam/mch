namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DischargeCharge
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DischargeCharge()
        {
            DischargePayments = new HashSet<DischargePayment>();
        }

        public long Id { get; set; }

        public long? DischargeDetailId { get; set; }

        public int? ChargeTypeId { get; set; }

        public double? ChargeAmount { get; set; }

        public int? PaymentTypeId { get; set; }

        public virtual ChargeType ChargeType { get; set; }

        public virtual DischargeDetail DischargeDetail { get; set; }

        public virtual PaymentType PaymentType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargePayment> DischargePayments { get; set; }
    }
}
