namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Task
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Task()
        {
            CheckListResultsParents = new HashSet<CheckListResultsParent>();
            Dispositions = new HashSet<Disposition>();
            OSDs = new HashSet<OSD>();
            TaskAssignments = new HashSet<TaskAssignment>();
            TaskInstructions = new HashSet<TaskInstruction>();
            Tasks1 = new HashSet<Task>();
            TaskTransactions = new HashSet<TaskTransaction>();
            Transactions = new HashSet<Transaction>();
        }

        public long Id { get; set; }

        public int TaskTypeId { get; set; }

        public DateTime TaskCreationDate { get; set; }

        public int EntityTypeId { get; set; }

        public long EntityId { get; set; }

        [Required]
        [StringLength(50)]
        public string TaskCreator { get; set; }

        [StringLength(100)]
        public string TaskReference { get; set; }

        [Column(TypeName = "date")]
        public DateTime TaskDate { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int StatusId { get; set; }

        public DateTime StatusTimestamp { get; set; }

        public int WarehouseId { get; set; }

        public bool IsPriority { get; set; }

        public long? ParentTaskId { get; set; }

        public double ProgressPercent { get; set; }

        public bool IsReopened { get; set; }

        [StringLength(5)]
        public string Airline { get; set; }

        public int? ULDs { get; set; }

        public int Pieces { get; set; }

        public int? WarehouseLocationId { get; set; }

        public double? Weight { get; set; }

        public int? WeightUOMId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckListResultsParent> CheckListResultsParents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disposition> Dispositions { get; set; }

        public virtual EntityType EntityType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSD> OSDs { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskAssignment> TaskAssignments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskInstruction> TaskInstructions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Task> Tasks1 { get; set; }

        public virtual Task Task1 { get; set; }

        public virtual TaskType TaskType { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskTransaction> TaskTransactions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
