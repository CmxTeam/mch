namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CargoAcceptScreeningLog")]
    public partial class CargoAcceptScreeningLog
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long? TaskId { get; set; }

        public long UserId { get; set; }

        public long SealTypeId { get; set; }

        [StringLength(100)]
        public string SealNumber { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public long? ScreeningFacilityId { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        public long? ScreeningMethodId { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual ScreeningMethod ScreeningMethod { get; set; }

        public virtual ScreeningSealType ScreeningSealType { get; set; }

        public virtual Shipper Shipper { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
