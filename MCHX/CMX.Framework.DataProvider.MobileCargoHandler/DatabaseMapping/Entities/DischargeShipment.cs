namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DischargeShipment
    {
        public long Id { get; set; }

        public long DischargeId { get; set; }

        public long DischargeTypeId { get; set; }

        public long? AwbId { get; set; }

        public long? HawbId { get; set; }

        public int? CarrierId { get; set; }

        [StringLength(1000)]
        public string TransferManifest { get; set; }

        [StringLength(1000)]
        public string InbondNumber { get; set; }

        public long? PortId { get; set; }

        public int TotalReleasedPieces { get; set; }

        public bool IsCustomsOverride { get; set; }

        public int StatusId { get; set; }

        public DateTime Date { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual Discharge Discharge { get; set; }

        public virtual DischargeType DischargeType { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual Port Port { get; set; }

        public virtual Status Status { get; set; }
    }
}
