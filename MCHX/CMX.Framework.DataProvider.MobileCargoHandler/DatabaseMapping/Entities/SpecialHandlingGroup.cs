namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialHandlingGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SpecialHandlingGroup()
        {
            AccountSpecialHandlings = new HashSet<AccountSpecialHandling>();
            AlternateScreeningCases = new HashSet<AlternateScreeningCas>();
            ShipperVerificationDocs = new HashSet<ShipperVerificationDoc>();
            SpecialHandlingSettings = new HashSet<SpecialHandlingSetting>();
            SpecialHandlingLogs = new HashSet<SpecialHandlingLog>();
            SpecialHandlingSubGroups = new HashSet<SpecialHandlingSubGroup>();
            SpecialHandlingVerificationSettings = new HashSet<SpecialHandlingVerificationSetting>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }

        public int? SPHCodeId { get; set; }

        [Required]
        [StringLength(100)]
        public string DisplayName { get; set; }

        [StringLength(3)]
        public string Code { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountSpecialHandling> AccountSpecialHandlings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlternateScreeningCas> AlternateScreeningCases { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperVerificationDoc> ShipperVerificationDocs { get; set; }

        public virtual SpecialHandlingCode SpecialHandlingCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingSetting> SpecialHandlingSettings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingLog> SpecialHandlingLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingSubGroup> SpecialHandlingSubGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingVerificationSetting> SpecialHandlingVerificationSettings { get; set; }
    }
}
