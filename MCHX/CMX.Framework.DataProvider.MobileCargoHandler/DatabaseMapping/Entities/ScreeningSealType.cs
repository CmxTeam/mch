namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningSealType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScreeningSealType()
        {
            CargoAcceptScreeningLogs = new HashSet<CargoAcceptScreeningLog>();
            SealVerificationsLogs = new HashSet<SealVerificationsLog>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        public bool ResetScreening { get; set; }

        public bool RequiresSealNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CargoAcceptScreeningLog> CargoAcceptScreeningLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SealVerificationsLog> SealVerificationsLogs { get; set; }
    }
}
