namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Snapshot
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Snapshot()
        {
            SnapshotConditions = new HashSet<SnapshotCondition>();
        }

        public long Id { get; set; }

        public int EntityTypeId { get; set; }

        public long EntityId { get; set; }

        public int Pieces { get; set; }

        [Required]
        public string SnapshotImage { get; set; }

        public long UserId { get; set; }

        public DateTime Timestamp { get; set; }

        public int WarehouseId { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public string DisplayText { get; set; }

        public virtual EntityType EntityType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SnapshotCondition> SnapshotConditions { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
