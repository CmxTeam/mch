namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DGRCheckListResult
    {
        public long Id { get; set; }

        public int ParentId { get; set; }

        public int? CheckListItemId { get; set; }

        [StringLength(50)]
        public string ValueList { get; set; }

        public string ResultValue { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? LastUpdated { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        public virtual DGRCheckListItem DGRCheckListItem { get; set; }

        public virtual DGRCheckListResultsParent DGRCheckListResultsParent { get; set; }
    }
}
