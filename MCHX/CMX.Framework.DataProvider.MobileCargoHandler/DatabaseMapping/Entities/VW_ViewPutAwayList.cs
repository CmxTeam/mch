namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VW_ViewPutAwayList
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Type { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId { get; set; }

        [StringLength(41)]
        public string RefNumber { get; set; }

        public int? IsBUP { get; set; }

        public int? PutAwayPieces { get; set; }

        public int? TotalPieces { get; set; }

        public double? PercentPutAway { get; set; }

        public int? Status { get; set; }
    }
}
