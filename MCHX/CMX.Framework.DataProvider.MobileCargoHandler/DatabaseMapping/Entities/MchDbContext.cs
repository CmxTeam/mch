namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MchDbContext : DbContext
    {
        public MchDbContext()
            : base("name=MchDbContext")
        {
        }

        public virtual DbSet<AccountAlternateScreening> AccountAlternateScreenings { get; set; }
        public virtual DbSet<AccountCarrier> AccountCarriers { get; set; }
        public virtual DbSet<AccountingInfoIdentifier> AccountingInfoIdentifiers { get; set; }
        public virtual DbSet<AccountingInformation> AccountingInformations { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountSpecialHandling> AccountSpecialHandlings { get; set; }
        public virtual DbSet<Agent> Agents { get; set; }
        public virtual DbSet<AircraftType> AircraftTypes { get; set; }
        public virtual DbSet<AlertEntity> AlertEntities { get; set; }
        public virtual DbSet<AlertEvent> AlertEvents { get; set; }
        public virtual DbSet<AlertFrequencyType> AlertFrequencyTypes { get; set; }
        public virtual DbSet<Alert> Alerts { get; set; }
        public virtual DbSet<AlternateScreeningCas> AlternateScreeningCases { get; set; }
        public virtual DbSet<AlternateScreeningLog> AlternateScreeningLogs { get; set; }
        public virtual DbSet<AmsCode> AmsCodes { get; set; }
        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<AttachmentType> AttachmentTypes { get; set; }
        public virtual DbSet<AttributeItem> AttributeItems { get; set; }
        public virtual DbSet<Attribute> Attributes { get; set; }
        public virtual DbSet<AttributeTaskType> AttributeTaskTypes { get; set; }
        public virtual DbSet<AutoCompleteEmail> AutoCompleteEmails { get; set; }
        public virtual DbSet<AutoCompleteFromEmail> AutoCompleteFromEmails { get; set; }
        public virtual DbSet<AWB> AWBs { get; set; }
        public virtual DbSet<AWBs_HWBs> AWBs_HWBs { get; set; }
        public virtual DbSet<AWBs_ULDs> AWBs_ULDs { get; set; }
        public virtual DbSet<CargoAcceptScreeningLog> CargoAcceptScreeningLogs { get; set; }
        public virtual DbSet<CarrierCIMPMailbox> CarrierCIMPMailboxes { get; set; }
        public virtual DbSet<CarrierDriver> CarrierDrivers { get; set; }
        public virtual DbSet<CarrierRoute> CarrierRoutes { get; set; }
        public virtual DbSet<Carrier> Carriers { get; set; }
        public virtual DbSet<CarrierServiceType> CarrierServiceTypes { get; set; }
        public virtual DbSet<ChargeCode> ChargeCodes { get; set; }
        public virtual DbSet<ChargeDeclaration> ChargeDeclarations { get; set; }
        public virtual DbSet<ChargeSummary> ChargeSummaries { get; set; }
        public virtual DbSet<ChargeType> ChargeTypes { get; set; }
        public virtual DbSet<CheckListItemHeader> CheckListItemHeaders { get; set; }
        public virtual DbSet<CheckListItem> CheckListItems { get; set; }
        public virtual DbSet<CheckListResult> CheckListResults { get; set; }
        public virtual DbSet<CheckListResultsParent> CheckListResultsParents { get; set; }
        public virtual DbSet<CheckList> CheckLists { get; set; }
        public virtual DbSet<CIMPTestingTable> CIMPTestingTables { get; set; }
        public virtual DbSet<Condition> Conditions { get; set; }
        public virtual DbSet<ConsignmentCode> ConsignmentCodes { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CredentialType> CredentialTypes { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomsInfoIdentifier> CustomsInfoIdentifiers { get; set; }
        public virtual DbSet<CustomsNotification> CustomsNotifications { get; set; }
        public virtual DbSet<DeleteMe> DeleteMes { get; set; }
        public virtual DbSet<DensityGroup> DensityGroups { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<DescriptionOfGood> DescriptionOfGoods { get; set; }
        public virtual DbSet<DGChecklist> DGChecklists { get; set; }
        public virtual DbSet<DGRCheckListItemHeader> DGRCheckListItemHeaders { get; set; }
        public virtual DbSet<DGRCheckListItem> DGRCheckListItems { get; set; }
        public virtual DbSet<DGRCheckListResult> DGRCheckListResults { get; set; }
        public virtual DbSet<DGRCheckListResultsParent> DGRCheckListResultsParents { get; set; }
        public virtual DbSet<DGRCheckList> DGRCheckLists { get; set; }
        public virtual DbSet<Dimension> Dimensions { get; set; }
        public virtual DbSet<DischargeCharge> DischargeCharges { get; set; }
        public virtual DbSet<DischargeDetail> DischargeDetails { get; set; }
        public virtual DbSet<DischargePayment> DischargePayments { get; set; }
        public virtual DbSet<Discharge> Discharges { get; set; }
        public virtual DbSet<DischargeShipment> DischargeShipments { get; set; }
        public virtual DbSet<DischargeType> DischargeTypes { get; set; }
        public virtual DbSet<Disposition> Dispositions { get; set; }
        public virtual DbSet<DistributionList> DistributionLists { get; set; }
        public virtual DbSet<DriverSecurityLog> DriverSecurityLogs { get; set; }
        public virtual DbSet<DynamicTask> DynamicTasks { get; set; }
        public virtual DbSet<EmailDistributionList> EmailDistributionLists { get; set; }
        public virtual DbSet<EmailFromDefault> EmailFromDefaults { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<EntityAttribute> EntityAttributes { get; set; }
        public virtual DbSet<EntityBase> EntityBases { get; set; }
        public virtual DbSet<EntityCondition> EntityConditions { get; set; }
        public virtual DbSet<EntityReference> EntityReferences { get; set; }
        public virtual DbSet<EntitySnapshot> EntitySnapshots { get; set; }
        public virtual DbSet<EntityType> EntityTypes { get; set; }
        public virtual DbSet<EventCodeOwner> EventCodeOwners { get; set; }
        public virtual DbSet<EventCode> EventCodes { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<FieldType> FieldTypes { get; set; }
        public virtual DbSet<FlightAttachment> FlightAttachments { get; set; }
        public virtual DbSet<FlightManifest> FlightManifests { get; set; }
        public virtual DbSet<FlightNotocDGDetail> FlightNotocDGDetails { get; set; }
        public virtual DbSet<FlightNotocMaster> FlightNotocMasters { get; set; }
        public virtual DbSet<FlightNotocOtherCargoDetail> FlightNotocOtherCargoDetails { get; set; }
        public virtual DbSet<FlightSnapshot> FlightSnapshots { get; set; }
        public virtual DbSet<FlightTransaction> FlightTransactions { get; set; }
        public virtual DbSet<ForkliftDetail> ForkliftDetails { get; set; }
        public virtual DbSet<Form> Forms { get; set; }
        public virtual DbSet<FormType> FormTypes { get; set; }
        public virtual DbSet<FSN> FSNs { get; set; }
        public virtual DbSet<HostPlus_CIMPOutboundQueue> HostPlus_CIMPOutboundQueue { get; set; }
        public virtual DbSet<HWBHTSSet> HWBHTSSets { get; set; }
        public virtual DbSet<HWB> HWBs { get; set; }
        public virtual DbSet<HWBs_Packages> HWBs_Packages { get; set; }
        public virtual DbSet<ImportExportCode> ImportExportCodes { get; set; }
        public virtual DbSet<ITDX> ITDXes { get; set; }
        public virtual DbSet<ITDX_Transactions> ITDX_Transactions { get; set; }
        public virtual DbSet<LabelTemplatesZPL> LabelTemplatesZPLs { get; set; }
        public virtual DbSet<LARCheckListItemHeader> LARCheckListItemHeaders { get; set; }
        public virtual DbSet<LARCheckListItem> LARCheckListItems { get; set; }
        public virtual DbSet<LARCheckListResult> LARCheckListResults { get; set; }
        public virtual DbSet<LARCheckListResultsParent> LARCheckListResultsParents { get; set; }
        public virtual DbSet<LARCheckList> LARCheckLists { get; set; }
        public virtual DbSet<LegSegment> LegSegments { get; set; }
        public virtual DbSet<LineIdentifier> LineIdentifiers { get; set; }
        public virtual DbSet<LoadingIndicator> LoadingIndicators { get; set; }
        public virtual DbSet<LoadingPlan> LoadingPlans { get; set; }
        public virtual DbSet<LoadingPlanULD> LoadingPlanULDs { get; set; }
        public virtual DbSet<ManifestAttachment> ManifestAttachments { get; set; }
        public virtual DbSet<ManifestAWBDetail> ManifestAWBDetails { get; set; }
        public virtual DbSet<ManifestDetail> ManifestDetails { get; set; }
        public virtual DbSet<ManifestPackageDetail> ManifestPackageDetails { get; set; }
        public virtual DbSet<Manifest> Manifests { get; set; }
        public virtual DbSet<ManifestType> ManifestTypes { get; set; }
        public virtual DbSet<MessageType> MessageTypes { get; set; }
        public virtual DbSet<MOT> MOTs { get; set; }
        public virtual DbSet<MovementPriorityCode> MovementPriorityCodes { get; set; }
        public virtual DbSet<OCI> OCIs { get; set; }
        public virtual DbSet<OnhandAttachment> OnhandAttachments { get; set; }
        public virtual DbSet<OnhandEntityReference> OnhandEntityReferences { get; set; }
        public virtual DbSet<Onhand> Onhands { get; set; }
        public virtual DbSet<OnhandSnapshot> OnhandSnapshots { get; set; }
        public virtual DbSet<OnhandTask> OnhandTasks { get; set; }
        public virtual DbSet<OnhandTransaction> OnhandTransactions { get; set; }
        public virtual DbSet<OSD> OSDs { get; set; }
        public virtual DbSet<OtherCharge> OtherCharges { get; set; }
        public virtual DbSet<Overpack> Overpacks { get; set; }
        public virtual DbSet<PackageGroup> PackageGroups { get; set; }
        public virtual DbSet<Package> Packages { get; set; }
        public virtual DbSet<PackageType> PackageTypes { get; set; }
        public virtual DbSet<ParticipantIdentifier> ParticipantIdentifiers { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<PortAlias> PortAliases { get; set; }
        public virtual DbSet<Port> Ports { get; set; }
        public virtual DbSet<PrinterLabelType> PrinterLabelTypes { get; set; }
        public virtual DbSet<Printer> Printers { get; set; }
        public virtual DbSet<PrinterType> PrinterTypes { get; set; }
        public virtual DbSet<PrintServer> PrintServers { get; set; }
        public virtual DbSet<RangePoolItem> RangePoolItems { get; set; }
        public virtual DbSet<RangePool> RangePools { get; set; }
        public virtual DbSet<RateClassCode> RateClassCodes { get; set; }
        public virtual DbSet<RateDescription> RateDescriptions { get; set; }
        public virtual DbSet<ReferenceType> ReferenceTypes { get; set; }
        public virtual DbSet<Remark> Remarks { get; set; }
        public virtual DbSet<ScreeningDevice> ScreeningDevices { get; set; }
        public virtual DbSet<ScreeningMethod> ScreeningMethods { get; set; }
        public virtual DbSet<ScreeningRemark> ScreeningRemarks { get; set; }
        public virtual DbSet<ScreeningSealLog> ScreeningSealLogs { get; set; }
        public virtual DbSet<ScreeningSealType> ScreeningSealTypes { get; set; }
        public virtual DbSet<ScreeningSummary> ScreeningSummaries { get; set; }
        public virtual DbSet<ScreeningTransaction> ScreeningTransactions { get; set; }
        public virtual DbSet<ScreeningVerificationLog> ScreeningVerificationLogs { get; set; }
        public virtual DbSet<SealVerificationsLog> SealVerificationsLogs { get; set; }
        public virtual DbSet<SenderReference> SenderReferences { get; set; }
        public virtual DbSet<ServiceCode> ServiceCodes { get; set; }
        public virtual DbSet<ShipmentUnitType> ShipmentUnitTypes { get; set; }
        public virtual DbSet<ShipperGroup> ShipperGroups { get; set; }
        public virtual DbSet<ShipperManifest> ShipperManifests { get; set; }
        public virtual DbSet<ShipperManifests_AWBs> ShipperManifests_AWBs { get; set; }
        public virtual DbSet<Shipper> Shippers { get; set; }
        public virtual DbSet<ShipperType> ShipperTypes { get; set; }
        public virtual DbSet<ShipperVerificationDoc> ShipperVerificationDocs { get; set; }
        public virtual DbSet<ShipperVerificationField> ShipperVerificationFields { get; set; }
        public virtual DbSet<ShipperVerificationLog> ShipperVerificationLogs { get; set; }
        public virtual DbSet<SnapshotCondition> SnapshotConditions { get; set; }
        public virtual DbSet<Snapshot> Snapshots { get; set; }
        public virtual DbSet<SpecialHandlingCode> SpecialHandlingCodes { get; set; }
        public virtual DbSet<SpecialHandlingGroup> SpecialHandlingGroups { get; set; }
        public virtual DbSet<SpecialHandlingLog> SpecialHandlingLogs { get; set; }
        public virtual DbSet<SpecialHandling> SpecialHandlings { get; set; }
        public virtual DbSet<SpecialHandlingSetting> SpecialHandlingSettings { get; set; }
        public virtual DbSet<SpecialHandlingSubGroup> SpecialHandlingSubGroups { get; set; }
        public virtual DbSet<SpecialHandlingVerificationSetting> SpecialHandlingVerificationSettings { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<TableList> TableLists { get; set; }
        public virtual DbSet<TaskAssignment> TaskAssignments { get; set; }
        public virtual DbSet<TaskCategory> TaskCategories { get; set; }
        public virtual DbSet<TaskInstruction> TaskInstructions { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskSnapshotReference> TaskSnapshotReferences { get; set; }
        public virtual DbSet<TaskTransaction> TaskTransactions { get; set; }
        public virtual DbSet<TaskType> TaskTypes { get; set; }
        public virtual DbSet<TempDeliveryOrderList> TempDeliveryOrderLists { get; set; }
        public virtual DbSet<TempDischargeCharge> TempDischargeCharges { get; set; }
        public virtual DbSet<TempDischargePayment> TempDischargePayments { get; set; }
        public virtual DbSet<TransactionAction> TransactionActions { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TSA_Approved_Carriers_KnownShippers_List> TSA_Approved_Carriers_KnownShippers_List { get; set; }
        public virtual DbSet<TSA_Approved_Carriers_Master_List> TSA_Approved_Carriers_Master_List { get; set; }
        public virtual DbSet<TSA_Approved_Certified_Screening_Facilities> TSA_Approved_Certified_Screening_Facilities { get; set; }
        public virtual DbSet<TSA_Approved_IAC_Master_List> TSA_Approved_IAC_Master_List { get; set; }
        public virtual DbSet<ULD> ULDs { get; set; }
        public virtual DbSet<ULDType> ULDTypes { get; set; }
        public virtual DbSet<ULDVolumeCode> ULDVolumeCodes { get; set; }
        public virtual DbSet<UNClass> UNClasses { get; set; }
        public virtual DbSet<UnloadingPort> UnloadingPorts { get; set; }
        public virtual DbSet<UOM> UOMs { get; set; }
        public virtual DbSet<UOMSystem> UOMSystems { get; set; }
        public virtual DbSet<UOMType> UOMTypes { get; set; }
        public virtual DbSet<UserCustomsOverride> UserCustomsOverrides { get; set; }
        public virtual DbSet<UserPrinter> UserPrinters { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<UserStation> UserStations { get; set; }
        public virtual DbSet<UserWarehous> UserWarehouses { get; set; }
        public virtual DbSet<WarehouseLocation> WarehouseLocations { get; set; }
        public virtual DbSet<WarehouseLocationType> WarehouseLocationTypes { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<WarehouseZone> WarehouseZones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountingInfoIdentifier>()
                .HasMany(e => e.AccountingInformations)
                .WithRequired(e => e.AccountingInfoIdentifier)
                .HasForeignKey(e => e.AccountInfoId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Customers)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.AccountId);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.EmailFromDefaults)
                .WithOptional(e => e.Account)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Account>()
                .HasMany(e => e.ShipperManifests)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.TSA_Approved_Carriers_KnownShippers_List)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AircraftType>()
                .HasMany(e => e.AlternateScreeningCases)
                .WithRequired(e => e.AircraftType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AircraftType>()
                .HasMany(e => e.ShipperManifests)
                .WithRequired(e => e.AircraftType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AircraftType>()
                .HasMany(e => e.ShipperManifests_AWBs)
                .WithRequired(e => e.AircraftType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AircraftType>()
                .HasMany(e => e.ShipperTypes)
                .WithRequired(e => e.AircraftType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AlertFrequencyType>()
                .HasMany(e => e.Alerts)
                .WithRequired(e => e.AlertFrequencyType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alert>()
                .HasMany(e => e.AlertEntities)
                .WithRequired(e => e.Alert)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alert>()
                .HasMany(e => e.AlertEvents)
                .WithRequired(e => e.Alert)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AlternateScreeningCas>()
                .HasMany(e => e.AccountAlternateScreenings)
                .WithRequired(e => e.AlternateScreeningCas)
                .HasForeignKey(e => e.AlternateScreeningCaseId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AlternateScreeningCas>()
                .HasMany(e => e.AlternateScreeningLogs)
                .WithRequired(e => e.AlternateScreeningCas)
                .HasForeignKey(e => e.AltScreeningCaseId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AmsCode>()
                .HasMany(e => e.UserCustomsOverrides)
                .WithRequired(e => e.AmsCode)
                .HasForeignKey(e => e.CustomsStatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Attribute>()
                .HasMany(e => e.AttributeTaskTypes)
                .WithRequired(e => e.Attribute)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Attribute>()
                .HasMany(e => e.EntityAttributes)
                .WithRequired(e => e.Attribute)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AutoCompleteFromEmail>()
                .HasMany(e => e.EmailTemplates)
                .WithOptional(e => e.AutoCompleteFromEmail)
                .HasForeignKey(e => e.FromEmailId);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.AccountingInformations)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.AWBs_ULDs)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.ChargeSummaries)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.LoadingPlans)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.OtherCharges)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.RateDescriptions)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.ScreeningVerificationLogs)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.SenderReferences)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.ShipperManifests_AWBs)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.ShipperVerificationLogs)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AWB>()
                .HasMany(e => e.SpecialHandlingLogs)
                .WithRequired(e => e.AWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarrierDriver>()
                .HasMany(e => e.DriverSecurityLogs)
                .WithRequired(e => e.CarrierDriver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarrierDriver>()
                .HasMany(e => e.FlightManifests)
                .WithOptional(e => e.CarrierDriver)
                .HasForeignKey(e => e.TenderedDriverId);

            modelBuilder.Entity<CarrierDriver>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.CarrierDriver)
                .HasForeignKey(e => e.DriverId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.CarrierCIMPMailboxes)
                .WithRequired(e => e.Carrier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.CarrierDrivers)
                .WithOptional(e => e.Carrier)
                .HasForeignKey(e => e.TruckingCompanyId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.DriverSecurityLogs)
                .WithOptional(e => e.Carrier)
                .HasForeignKey(e => e.TruckingCompanyId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.FlightManifests)
                .WithOptional(e => e.Carrier)
                .HasForeignKey(e => e.CarrierId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.FlightManifests1)
                .WithOptional(e => e.Carrier1)
                .HasForeignKey(e => e.TenderedCarrierId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.LoadingPlanULDs)
                .WithOptional(e => e.Carrier)
                .HasForeignKey(e => e.OwnerId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.ULDs)
                .WithOptional(e => e.Carrier)
                .HasForeignKey(e => e.OwnerId);

            modelBuilder.Entity<Carrier>()
                .HasMany(e => e.LegSegments)
                .WithRequired(e => e.Carrier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarrierServiceType>()
                .HasMany(e => e.Manifests)
                .WithOptional(e => e.CarrierServiceType)
                .HasForeignKey(e => e.ServiceTypeId);

            modelBuilder.Entity<CarrierServiceType>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.CarrierServiceType)
                .HasForeignKey(e => e.ServiceTypeId);

            modelBuilder.Entity<ChargeCode>()
                .HasMany(e => e.OtherCharges)
                .WithRequired(e => e.ChargeCode)
                .HasForeignKey(e => e.CodeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ChargeDeclaration>()
                .Property(e => e.CarriageDeclaredValue)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeDeclaration>()
                .Property(e => e.CustomsDeclaredValue)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeDeclaration>()
                .Property(e => e.InsuranceDeclaredValue)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeSummary>()
                .Property(e => e.TotalChargesWeight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeSummary>()
                .Property(e => e.TotalChargesValuation)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeSummary>()
                .Property(e => e.TotalChargesTax)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeSummary>()
                .Property(e => e.TotalOtherChargesDueAgent)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeSummary>()
                .Property(e => e.TotalOtherChargesDueCarrier)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeSummary>()
                .Property(e => e.TotalCharges)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeType>()
                .Property(e => e.DefaultChargeAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ChargeType>()
                .Property(e => e.MinChargeAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<CheckListItemHeader>()
                .HasMany(e => e.CheckListItems)
                .WithRequired(e => e.CheckListItemHeader)
                .HasForeignKey(e => e.ListItemHeaderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CheckListItem>()
                .Property(e => e.ValueList)
                .IsUnicode(false);

            modelBuilder.Entity<CheckListItem>()
                .HasMany(e => e.CheckListItems1)
                .WithOptional(e => e.CheckListItem1)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<CheckListResultsParent>()
                .Property(e => e.SignatureData)
                .IsUnicode(false);

            modelBuilder.Entity<CheckListResultsParent>()
                .HasMany(e => e.CheckListResults)
                .WithRequired(e => e.CheckListResultsParent)
                .HasForeignKey(e => e.ParentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CheckList>()
                .HasMany(e => e.CheckListItemHeaders)
                .WithRequired(e => e.CheckList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CheckList>()
                .HasMany(e => e.CheckListItems)
                .WithRequired(e => e.CheckList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Condition>()
                .HasMany(e => e.OSDs)
                .WithRequired(e => e.Condition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Condition>()
                .HasMany(e => e.SnapshotConditions)
                .WithRequired(e => e.Condition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Customers)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.RateDescriptions)
                .WithOptional(e => e.Country)
                .HasForeignKey(e => e.OriginOfGoodsCountryId);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.Country)
                .HasForeignKey(e => e.DestinationCountryId);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Onhands1)
                .WithOptional(e => e.Country1)
                .HasForeignKey(e => e.OriginCountryId);

            modelBuilder.Entity<CredentialType>()
                .HasMany(e => e.DriverSecurityLogs)
                .WithOptional(e => e.CredentialType)
                .HasForeignKey(e => e.PrimaryIDTypeId);

            modelBuilder.Entity<CredentialType>()
                .HasMany(e => e.DriverSecurityLogs1)
                .WithOptional(e => e.CredentialType1)
                .HasForeignKey(e => e.SecondaryIdTypeId);

            modelBuilder.Entity<CredentialType>()
                .HasMany(e => e.ShipperManifests)
                .WithOptional(e => e.CredentialType)
                .HasForeignKey(e => e.GovCredTypeId);

            modelBuilder.Entity<CredentialType>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.CredentialType)
                .HasForeignKey(e => e.DriverCredType1Id);

            modelBuilder.Entity<CredentialType>()
                .HasMany(e => e.Onhands1)
                .WithOptional(e => e.CredentialType1)
                .HasForeignKey(e => e.DriverCredType2Id);

            modelBuilder.Entity<Currency>()
                .HasMany(e => e.ChargeDeclarations)
                .WithRequired(e => e.Currency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.ShellAccountId);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.AWBs)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.ShipperId);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.AWBs1)
                .WithOptional(e => e.Customer1)
                .HasForeignKey(e => e.ConsigneeId);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Contacts)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.HWBs)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.ShipperId);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.HWBs1)
                .WithOptional(e => e.Customer1)
                .HasForeignKey(e => e.ConsigneeId);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.ConsigneeId);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Onhands1)
                .WithOptional(e => e.Customer1)
                .HasForeignKey(e => e.ShipperId);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.TaskTypes)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DGRCheckListItemHeader>()
                .HasMany(e => e.DGRCheckListItems)
                .WithRequired(e => e.DGRCheckListItemHeader)
                .HasForeignKey(e => e.ListItemHeaderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DGRCheckListItem>()
                .Property(e => e.ValueList)
                .IsUnicode(false);

            modelBuilder.Entity<DGRCheckListItem>()
                .HasMany(e => e.DGRCheckListItems1)
                .WithOptional(e => e.DGRCheckListItem1)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<DGRCheckListItem>()
                .HasMany(e => e.DGRCheckListResults)
                .WithOptional(e => e.DGRCheckListItem)
                .HasForeignKey(e => e.CheckListItemId);

            modelBuilder.Entity<DGRCheckListResult>()
                .Property(e => e.ValueList)
                .IsUnicode(false);

            modelBuilder.Entity<DGRCheckListResultsParent>()
                .Property(e => e.SignatureData)
                .IsUnicode(false);

            modelBuilder.Entity<DGRCheckListResultsParent>()
                .HasMany(e => e.DGRCheckListResults)
                .WithRequired(e => e.DGRCheckListResultsParent)
                .HasForeignKey(e => e.ParentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DGRCheckList>()
                .HasMany(e => e.DGRCheckListItems)
                .WithRequired(e => e.DGRCheckList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DischargeCharge>()
                .HasMany(e => e.DischargePayments)
                .WithOptional(e => e.DischargeCharge)
                .WillCascadeOnDelete();

            modelBuilder.Entity<DischargeDetail>()
                .HasMany(e => e.DischargeCharges)
                .WithOptional(e => e.DischargeDetail)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Discharge>()
                .HasMany(e => e.DischargeDetails)
                .WithOptional(e => e.Discharge)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Discharge>()
                .HasMany(e => e.DischargeShipments)
                .WithRequired(e => e.Discharge)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DischargeType>()
                .HasMany(e => e.DischargeShipments)
                .WithRequired(e => e.DischargeType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DistributionList>()
                .HasMany(e => e.EmailTemplates)
                .WithOptional(e => e.DistributionList)
                .HasForeignKey(e => e.ToDistributionListId);

            modelBuilder.Entity<DriverSecurityLog>()
                .HasMany(e => e.Discharges)
                .WithRequired(e => e.DriverSecurityLog)
                .HasForeignKey(e => e.DriverLogId);

            modelBuilder.Entity<EntityBase>()
                .HasOptional(e => e.DeleteMe)
                .WithRequired(e => e.EntityBase);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.AlertEntities)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.Attachments)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.CheckListResultsParents)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.EmailTemplates)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.Events)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.RangePoolItems)
                .WithOptional(e => e.EntityType)
                .HasForeignKey(e => e.RangeTypeId);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.RangePools)
                .WithOptional(e => e.EntityType)
                .HasForeignKey(e => e.RangeTypeId);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.ScreeningSummaries)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.Snapshots)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntityType>()
                .HasMany(e => e.UserCustomsOverrides)
                .WithRequired(e => e.EntityType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EventCodeOwner>()
                .HasMany(e => e.EventCodes)
                .WithRequired(e => e.EventCodeOwner)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FieldType>()
                .HasMany(e => e.ShipperVerificationFields)
                .WithRequired(e => e.FieldType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FlightManifest>()
                .HasMany(e => e.FlightAttachments)
                .WithRequired(e => e.FlightManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FlightManifest>()
                .HasMany(e => e.FlightNotocMasters)
                .WithRequired(e => e.FlightManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FlightManifest>()
                .HasMany(e => e.LoadingPlans)
                .WithRequired(e => e.FlightManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FlightNotocMaster>()
                .HasMany(e => e.FlightNotocDGDetails)
                .WithRequired(e => e.FlightNotocMaster)
                .HasForeignKey(e => e.FlightNotocId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FlightNotocMaster>()
                .HasMany(e => e.FlightNotocOtherCargoDetails)
                .WithOptional(e => e.FlightNotocMaster)
                .HasForeignKey(e => e.FlightNotocId);

            modelBuilder.Entity<FormType>()
                .HasMany(e => e.Forms)
                .WithRequired(e => e.FormType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HWB>()
                .Property(e => e.CollectFee)
                .HasPrecision(19, 4);

            modelBuilder.Entity<HWB>()
                .HasMany(e => e.DischargeShipments)
                .WithOptional(e => e.HWB)
                .HasForeignKey(e => e.HawbId);

            modelBuilder.Entity<HWB>()
                .HasMany(e => e.HWBs_Packages)
                .WithRequired(e => e.HWB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SerialNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.HostIP)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.ItdxIP)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.TcpPort)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.UdpPort)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SoftwareVersion)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.InformationSource)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SystemMessage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SystemReadiness)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.ScreeningResults)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SubstancesFound)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.LastSample)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.LastCalibration)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.Warning)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.CurrentUser)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DetectionMode)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DetectorFlowAverage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DetectorFlowRaw)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SampleFlowAverage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.SampleFlowRaw)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DesorberTemperature)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DesorberVoltage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DetectorTemperature)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.DetectorVoltage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.AbsolutePressureAverage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .Property(e => e.AbsolutePressureRaw)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX>()
                .HasMany(e => e.ITDX_Transactions)
                .WithRequired(e => e.ITDX)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SerialNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.HostIP)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.ItdxIP)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.TcpPort)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.UdpPort)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SoftwareVersion)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.InformationSource)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SystemMessage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SystemReadiness)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.ScreeningResults)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SubstancesFound)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.LastSample)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.LastCalibration)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.Warning)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.CurrentUser)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DetectionMode)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DetectorFlowAverage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DetectorFlowRaw)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SampleFlowAverage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.SampleFlowRaw)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DesorberTemperature)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DesorberVoltage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DetectorTemperature)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.DetectorVoltage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.AbsolutePressureAverage)
                .IsUnicode(false);

            modelBuilder.Entity<ITDX_Transactions>()
                .Property(e => e.AbsolutePressureRaw)
                .IsUnicode(false);

            modelBuilder.Entity<LARCheckListItemHeader>()
                .HasMany(e => e.LARCheckListItems)
                .WithRequired(e => e.LARCheckListItemHeader)
                .HasForeignKey(e => e.ListItemHeaderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LARCheckListItem>()
                .Property(e => e.ValueList)
                .IsUnicode(false);

            modelBuilder.Entity<LARCheckListItem>()
                .HasMany(e => e.LARCheckListItems1)
                .WithOptional(e => e.LARCheckListItem1)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<LARCheckListItem>()
                .HasMany(e => e.LARCheckListResults)
                .WithOptional(e => e.LARCheckListItem)
                .HasForeignKey(e => e.CheckListItemId);

            modelBuilder.Entity<LARCheckListResult>()
                .Property(e => e.ValueList)
                .IsUnicode(false);

            modelBuilder.Entity<LARCheckListResultsParent>()
                .Property(e => e.SignatureData)
                .IsUnicode(false);

            modelBuilder.Entity<LARCheckListResultsParent>()
                .HasMany(e => e.LARCheckListResults)
                .WithRequired(e => e.LARCheckListResultsParent)
                .HasForeignKey(e => e.ParentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LARCheckList>()
                .HasMany(e => e.LARCheckListItems)
                .WithRequired(e => e.LARCheckList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoadingPlan>()
                .HasMany(e => e.LoadingPlanULDs)
                .WithRequired(e => e.LoadingPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Manifest>()
                .HasMany(e => e.ManifestAttachments)
                .WithRequired(e => e.Manifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Manifest>()
                .HasMany(e => e.Manifests1)
                .WithOptional(e => e.Manifest1)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<ManifestType>()
                .HasMany(e => e.Manifests)
                .WithRequired(e => e.ManifestType)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Onhand>()
                .HasMany(e => e.DGChecklists)
                .WithRequired(e => e.Onhand)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Onhand>()
                .HasMany(e => e.OnhandAttachments)
                .WithRequired(e => e.Onhand)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OnhandTask>()
                .HasMany(e => e.OnhandTasks1)
                .WithOptional(e => e.OnhandTask1)
                .HasForeignKey(e => e.ParentTaskId);

            modelBuilder.Entity<OtherCharge>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Package>()
                .HasMany(e => e.HWBs_Packages)
                .WithRequired(e => e.Package)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PackageType>()
                .HasMany(e => e.PackageGroups)
                .WithRequired(e => e.PackageType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PackageType>()
                .HasMany(e => e.Packages)
                .WithRequired(e => e.PackageType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ParticipantIdentifier>()
                .HasMany(e => e.Agents)
                .WithOptional(e => e.ParticipantIdentifier)
                .HasForeignKey(e => e.ParticipantId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.AWBs)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.AWBs1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.DestinationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.CarrierRoutes)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.CarrierRoutes1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.DestinationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.CarrierRoutes2)
                .WithOptional(e => e.Port2)
                .HasForeignKey(e => e.ViaId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.FlightManifests)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.FlightManifests1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.DestinationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.FlightNotocMasters)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.LoadingStationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.HostPlus_CIMPOutboundQueue)
                .WithRequired(e => e.Port)
                .HasForeignKey(e => e.StationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.HWBs)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.HWBs1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.DestinationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.LegSegments)
                .WithRequired(e => e.Port)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.Manifests)
                .WithRequired(e => e.Port)
                .HasForeignKey(e => e.DestinationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.Manifests1)
                .WithRequired(e => e.Port1)
                .HasForeignKey(e => e.OriginId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.DestinationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.Onhands1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.Packages)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.DestinationId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.Packages1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.PortAliases)
                .WithRequired(e => e.Port)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.RateDescriptions)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.Line_RCPPortId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.SenderReferences)
                .WithOptional(e => e.Port)
                .HasForeignKey(e => e.SenderPortId);

            modelBuilder.Entity<Port>()
                .HasMany(e => e.SenderReferences1)
                .WithOptional(e => e.Port1)
                .HasForeignKey(e => e.ParticipantPortId);

            modelBuilder.Entity<PrinterLabelType>()
                .HasMany(e => e.LabelTemplatesZPLs)
                .WithOptional(e => e.PrinterLabelType)
                .HasForeignKey(e => e.LabelTypeId);

            modelBuilder.Entity<Printer>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<PrinterType>()
                .HasMany(e => e.Forms)
                .WithRequired(e => e.PrinterType)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PrinterType>()
                .HasMany(e => e.Printers)
                .WithRequired(e => e.PrinterType)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PrintServer>()
                .HasMany(e => e.Printers)
                .WithOptional(e => e.PrintServer)
                .HasForeignKey(e => e.ServerId);

            modelBuilder.Entity<RateClassCode>()
                .HasMany(e => e.RateDescriptions)
                .WithOptional(e => e.RateClassCode)
                .HasForeignKey(e => e.Line_Commodity_RateClassId);

            modelBuilder.Entity<RateClassCode>()
                .HasMany(e => e.RateDescriptions1)
                .WithOptional(e => e.RateClassCode1)
                .HasForeignKey(e => e.Line_RateClassId);

            modelBuilder.Entity<RateDescription>()
                .Property(e => e.Line_Rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RateDescription>()
                .Property(e => e.Line_TotalCharges)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ScreeningDevice>()
                .HasMany(e => e.ScreeningTransactions)
                .WithOptional(e => e.ScreeningDevice)
                .HasForeignKey(e => e.DeviceId);

            modelBuilder.Entity<ScreeningMethod>()
                .HasMany(e => e.ScreeningDevices)
                .WithOptional(e => e.ScreeningMethod)
                .HasForeignKey(e => e.DeviceTypeId);

            modelBuilder.Entity<ScreeningMethod>()
                .HasMany(e => e.ScreeningTransactions)
                .WithRequired(e => e.ScreeningMethod)
                .HasForeignKey(e => e.MethodId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScreeningSealType>()
                .HasMany(e => e.CargoAcceptScreeningLogs)
                .WithRequired(e => e.ScreeningSealType)
                .HasForeignKey(e => e.SealTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScreeningSealType>()
                .HasMany(e => e.SealVerificationsLogs)
                .WithRequired(e => e.ScreeningSealType)
                .HasForeignKey(e => e.SealTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScreeningTransaction>()
                .HasMany(e => e.ITDX_Transactions)
                .WithRequired(e => e.ScreeningTransaction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScreeningVerificationLog>()
                .HasMany(e => e.AlternateScreeningLogs)
                .WithRequired(e => e.ScreeningVerificationLog)
                .HasForeignKey(e => e.ScreeningLogId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScreeningVerificationLog>()
                .HasMany(e => e.ScreeningSealLogs)
                .WithRequired(e => e.ScreeningVerificationLog)
                .HasForeignKey(e => e.ScreeningLogId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipmentUnitType>()
                .HasMany(e => e.LoadingPlanULDs)
                .WithRequired(e => e.ShipmentUnitType)
                .HasForeignKey(e => e.UnitTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipmentUnitType>()
                .HasMany(e => e.ULDs)
                .WithOptional(e => e.ShipmentUnitType)
                .HasForeignKey(e => e.UnitTypeId);

            modelBuilder.Entity<ShipperGroup>()
                .HasMany(e => e.ShipperTypes)
                .WithRequired(e => e.ShipperGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipperManifest>()
                .HasMany(e => e.ScreeningVerificationLogs)
                .WithRequired(e => e.ShipperManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipperManifest>()
                .HasMany(e => e.ShipperManifests_AWBs)
                .WithRequired(e => e.ShipperManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipperManifest>()
                .HasMany(e => e.ShipperVerificationLogs)
                .WithRequired(e => e.ShipperManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipperManifest>()
                .HasMany(e => e.SpecialHandlingLogs)
                .WithRequired(e => e.ShipperManifest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shipper>()
                .HasMany(e => e.CargoAcceptScreeningLogs)
                .WithOptional(e => e.Shipper)
                .HasForeignKey(e => e.ScreeningFacilityId);

            modelBuilder.Entity<Shipper>()
                .HasMany(e => e.ScreeningTransactions)
                .WithOptional(e => e.Shipper)
                .HasForeignKey(e => e.PrescreenedAtCCSFId);

            modelBuilder.Entity<Shipper>()
                .HasMany(e => e.SealVerificationsLogs)
                .WithOptional(e => e.Shipper)
                .HasForeignKey(e => e.ScreeningFacilityId);

            modelBuilder.Entity<ShipperType>()
                .HasMany(e => e.ShipperManifests)
                .WithRequired(e => e.ShipperType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipperVerificationDoc>()
                .HasMany(e => e.Attachments)
                .WithOptional(e => e.ShipperVerificationDoc)
                .HasForeignKey(e => e.DocumentTypeId);

            modelBuilder.Entity<ShipperVerificationField>()
                .HasMany(e => e.ShipperVerificationLogs)
                .WithRequired(e => e.ShipperVerificationField)
                .HasForeignKey(e => e.FieldId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipperVerificationField>()
                .HasMany(e => e.SpecialHandlingVerificationSettings)
                .WithRequired(e => e.ShipperVerificationField)
                .HasForeignKey(e => e.FieldId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Snapshot>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Snapshot>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Snapshot>()
                .HasMany(e => e.SnapshotConditions)
                .WithRequired(e => e.Snapshot)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingCode>()
                .HasMany(e => e.SpecialHandlingGroups)
                .WithOptional(e => e.SpecialHandlingCode)
                .HasForeignKey(e => e.SPHCodeId);

            modelBuilder.Entity<SpecialHandlingCode>()
                .HasMany(e => e.SpecialHandlings)
                .WithOptional(e => e.SpecialHandlingCode)
                .HasForeignKey(e => e.SHCodeId);

            modelBuilder.Entity<SpecialHandlingCode>()
                .HasMany(e => e.SpecialHandlingSubGroups)
                .WithRequired(e => e.SpecialHandlingCode)
                .HasForeignKey(e => e.SPHCodeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingGroup>()
                .HasMany(e => e.AccountSpecialHandlings)
                .WithRequired(e => e.SpecialHandlingGroup)
                .HasForeignKey(e => e.SPHGroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingGroup>()
                .HasMany(e => e.ShipperVerificationDocs)
                .WithOptional(e => e.SpecialHandlingGroup)
                .HasForeignKey(e => e.SpecialHandlingId);

            modelBuilder.Entity<SpecialHandlingGroup>()
                .HasMany(e => e.SpecialHandlingSettings)
                .WithRequired(e => e.SpecialHandlingGroup)
                .HasForeignKey(e => e.SPHGroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingGroup>()
                .HasMany(e => e.SpecialHandlingLogs)
                .WithRequired(e => e.SpecialHandlingGroup)
                .HasForeignKey(e => e.SPHGroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingGroup>()
                .HasMany(e => e.SpecialHandlingSubGroups)
                .WithRequired(e => e.SpecialHandlingGroup)
                .HasForeignKey(e => e.SPHGroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingGroup>()
                .HasMany(e => e.SpecialHandlingVerificationSettings)
                .WithRequired(e => e.SpecialHandlingGroup)
                .HasForeignKey(e => e.SPHGroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpecialHandlingSubGroup>()
                .HasMany(e => e.SpecialHandlingLogs)
                .WithOptional(e => e.SpecialHandlingSubGroup)
                .HasForeignKey(e => e.SPHSubGroupId);

            modelBuilder.Entity<SpecialHandlingSubGroup>()
                .HasMany(e => e.SpecialHandlingSettings)
                .WithOptional(e => e.SpecialHandlingSubGroup)
                .HasForeignKey(e => e.SPHSubGroupId);

            modelBuilder.Entity<State>()
                .HasMany(e => e.CarrierDrivers)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.DischargeShipments)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Manifests)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Packages)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.ShipperManifests)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.TaskTransactions)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TableList>()
                .Property(e => e.DisplayName)
                .IsFixedLength();

            modelBuilder.Entity<TableList>()
                .Property(e => e.TableName)
                .IsFixedLength();

            modelBuilder.Entity<TableList>()
                .Property(e => e.ScreenName)
                .IsFixedLength();

            modelBuilder.Entity<TaskCategory>()
                .HasMany(e => e.TaskTypes)
                .WithOptional(e => e.TaskCategory)
                .HasForeignKey(e => e.CategoryId);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.CheckListResultsParents)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Dispositions)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.OSDs)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.TaskAssignments)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Tasks1)
                .WithOptional(e => e.Task1)
                .HasForeignKey(e => e.ParentTaskId);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.TaskTransactions)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskType>()
                .HasMany(e => e.AlertEvents)
                .WithRequired(e => e.TaskType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskType>()
                .HasMany(e => e.AttributeTaskTypes)
                .WithRequired(e => e.TaskType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskType>()
                .HasMany(e => e.DynamicTasks)
                .WithRequired(e => e.TaskType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskType>()
                .HasMany(e => e.OnhandTasks)
                .WithRequired(e => e.TaskType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskType>()
                .HasMany(e => e.SpecialHandlingSettings)
                .WithRequired(e => e.TaskType)
                .HasForeignKey(e => e.ChecklistTaskTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskType>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.TaskType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.AWBNumber)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.HWBNumber)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.Origin)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.Shipper)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.Consignee)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.Location)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.CustomsStatus)
                .IsFixedLength();

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.TotalChargeAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TempDeliveryOrderList>()
                .Property(e => e.TotalPaymentAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TempDischargeCharge>()
                .Property(e => e.ChargeAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TempDischargeCharge>()
                .Property(e => e.ChargePaid)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TempDischargePayment>()
                .Property(e => e.PaymentAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TSA_Approved_Certified_Screening_Facilities>()
                .HasMany(e => e.ScreeningVerificationLogs)
                .WithOptional(e => e.TSA_Approved_Certified_Screening_Facilities)
                .HasForeignKey(e => e.CCSFId);

            modelBuilder.Entity<ULD>()
                .HasMany(e => e.AWBs_ULDs)
                .WithRequired(e => e.ULD)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ULDType>()
                .HasMany(e => e.Overpacks)
                .WithRequired(e => e.ULDType)
                .HasForeignKey(e => e.OverpackTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UnloadingPort>()
                .HasMany(e => e.LoadingPlans)
                .WithRequired(e => e.UnloadingPort)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.AWBs)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.AWBs1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Dimensions)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Dimensions1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.DimUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.HWBs)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.LoadingPlans)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.LoadingPlans1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.LoadingPlanULDs)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ManifestAWBDetails)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ManifestAWBDetails1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ManifestDetails)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ManifestDetails1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ManifestPackageDetails)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Manifests)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.UomVolumeId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Manifests1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.UomWeightId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.UomWeightId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.PackageGroups)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.DimUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.PackageGroups1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Packages)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.DimUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Packages1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Packages2)
                .WithOptional(e => e.UOM2)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.RateDescriptions)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.Line_WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.Tasks)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ULDs)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ULDs1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ULDTypes)
                .WithOptional(e => e.UOM)
                .HasForeignKey(e => e.WeightUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ULDTypes1)
                .WithOptional(e => e.UOM1)
                .HasForeignKey(e => e.VolumeUOMId);

            modelBuilder.Entity<UOM>()
                .HasMany(e => e.ULDTypes2)
                .WithOptional(e => e.UOM2)
                .HasForeignKey(e => e.DimUOMId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Alerts)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Alerts1)
                .WithOptional(e => e.UserProfile1)
                .HasForeignKey(e => e.AssignedToUserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.AlternateScreeningLogs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Attachments)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CargoAcceptScreeningLogs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Discharges)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Dispositions)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.DriverSecurityLogs)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ForkliftDetails)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.OSDs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Overpacks)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ScreeningSealLogs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ScreeningSealLogs1)
                .WithOptional(e => e.UserProfile1)
                .HasForeignKey(e => e.VerifiedByUserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ScreeningSealLogs2)
                .WithOptional(e => e.UserProfile2)
                .HasForeignKey(e => e.OverrideByUserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ScreeningTransactions)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.AlarmClearedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ScreeningTransactions1)
                .WithOptional(e => e.UserProfile1)
                .HasForeignKey(e => e.AlarmClearedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.SealVerificationsLogs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ShipperManifests)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ShipperVerificationLogs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Snapshots)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.SpecialHandlingLogs)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.SpecialHandlingSettings)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UpdatedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TaskAssignments)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TaskAssignments1)
                .WithRequired(e => e.UserProfile1)
                .HasForeignKey(e => e.AssignedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TaskInstructions)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TaskTransactions)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Transactions)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TSA_Approved_Carriers_KnownShippers_List)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UpdatedByUserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TSA_Approved_Carriers_Master_List)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UpdatedByUserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TSA_Approved_Certified_Screening_Facilities)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TSA_Approved_Certified_Screening_Facilities1)
                .WithOptional(e => e.UserProfile1)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.TSA_Approved_IAC_Master_List)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UpdatedByUserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserPrinters)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserStations)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserWarehouses)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<WarehouseLocation>()
                .HasMany(e => e.Dispositions)
                .WithRequired(e => e.WarehouseLocation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarehouseLocation>()
                .HasMany(e => e.FlightManifests)
                .WithOptional(e => e.WarehouseLocation)
                .HasForeignKey(e => e.RecoveredLocationId);

            modelBuilder.Entity<WarehouseLocation>()
                .HasMany(e => e.FlightManifests1)
                .WithOptional(e => e.WarehouseLocation1)
                .HasForeignKey(e => e.StagingLocationId);

            modelBuilder.Entity<WarehouseLocation>()
                .HasMany(e => e.Onhands)
                .WithOptional(e => e.WarehouseLocation)
                .HasForeignKey(e => e.StageLocationId);

            modelBuilder.Entity<WarehouseLocation>()
                .HasMany(e => e.ULDs)
                .WithOptional(e => e.WarehouseLocation)
                .HasForeignKey(e => e.LocationId);

            modelBuilder.Entity<WarehouseLocation>()
                .HasMany(e => e.UnloadingPorts)
                .WithOptional(e => e.WarehouseLocation)
                .HasForeignKey(e => e.RecoveredLocationId);

            modelBuilder.Entity<WarehouseLocationType>()
                .HasMany(e => e.WarehouseLocations)
                .WithOptional(e => e.WarehouseLocationType)
                .HasForeignKey(e => e.LocationTypeId);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Discharges)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Overpacks)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Remarks)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.ScreeningTransactions)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.ShipperManifests)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Snapshots)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarehouseZone>()
                .HasMany(e => e.WarehouseLocations)
                .WithOptional(e => e.WarehouseZone)
                .HasForeignKey(e => e.ZoneId);
        }
    }
}
