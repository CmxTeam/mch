namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskTransaction
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long TaskId { get; set; }

        public long UserId { get; set; }

        public int StatusId { get; set; }

        public DateTime StatusTimestamp { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool? IsDisabled { get; set; }

        public virtual Status Status { get; set; }

        public virtual Task Task { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
