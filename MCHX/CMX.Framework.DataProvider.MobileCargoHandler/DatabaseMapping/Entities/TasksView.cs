namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TasksView")]
    public partial class TasksView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [StringLength(100)]
        public string Reference { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "date")]
        public DateTime TaskDate { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(3)]
        public string Warehouse { get; set; }

        [StringLength(100)]
        public string Status { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Assignees { get; set; }

        public double? Progress { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool IsReopened { get; set; }
    }
}
