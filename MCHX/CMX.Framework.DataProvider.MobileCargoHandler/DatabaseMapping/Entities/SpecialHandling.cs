namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialHandling
    {
        public int Id { get; set; }

        public long? AWBId { get; set; }

        public long? ULDId { get; set; }

        public long? HWBId { get; set; }

        public int? SHCodeId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual SpecialHandlingCode SpecialHandlingCode { get; set; }

        public virtual ULD ULD { get; set; }
    }
}
