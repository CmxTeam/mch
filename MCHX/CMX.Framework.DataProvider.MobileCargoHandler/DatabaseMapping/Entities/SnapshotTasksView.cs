namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SnapshotTasksView")]
    public partial class SnapshotTasksView
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Type { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId { get; set; }

        [StringLength(36)]
        public string RefNumber { get; set; }

        [StringLength(12)]
        public string RefNumber1 { get; set; }

        public int? Pieces { get; set; }

        [StringLength(33)]
        public string Weight { get; set; }

        [StringLength(50)]
        public string WarehouseLocation { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(7)]
        public string Condition { get; set; }

        public int? SnapshotCount { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PortId { get; set; }

        [StringLength(50)]
        public string TaskStatus { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TaskStatusId { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(50)]
        public string TaskCreator { get; set; }

        [StringLength(50)]
        public string TaskOwner { get; set; }

        [StringLength(500)]
        public string StatusImagePath { get; set; }

        public string EntityTypeImagePath { get; set; }
    }
}
