namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FlightNotocOtherCargoDetail
    {
        public long Id { get; set; }

        public DateTime? RecDate { get; set; }

        public long? FlightNotocId { get; set; }

        public long? AwbId { get; set; }

        [StringLength(500)]
        public string ContentsDescription { get; set; }

        public int? NoOfPackages { get; set; }

        [StringLength(50)]
        public string Quantity { get; set; }

        [StringLength(500)]
        public string SupplementaryInfo { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public long? UldId { get; set; }

        [StringLength(50)]
        public string AircraftPosition { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual FlightNotocMaster FlightNotocMaster { get; set; }

        public virtual ULD ULD { get; set; }
    }
}
