namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ShipperVerificationDoc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ShipperVerificationDoc()
        {
            Attachments = new HashSet<Attachment>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(200)]
        public string DocumentName { get; set; }

        public long? ShipperTypeId { get; set; }

        public long? SpecialHandlingId { get; set; }

        public int? TaskTypeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attachment> Attachments { get; set; }

        public virtual ShipperType ShipperType { get; set; }

        public virtual SpecialHandlingGroup SpecialHandlingGroup { get; set; }

        public virtual TaskType TaskType { get; set; }
    }
}
