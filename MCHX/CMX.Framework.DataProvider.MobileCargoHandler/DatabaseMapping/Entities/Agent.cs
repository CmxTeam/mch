namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Agent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Agent()
        {
            AWBs = new HashSet<AWB>();
        }

        public int Id { get; set; }

        [StringLength(14)]
        public string AccountNumber { get; set; }

        [Required]
        [StringLength(7)]
        public string NumericCode { get; set; }

        [StringLength(4)]
        public string CASSAddress { get; set; }

        public int? ParticipantId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Place { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AWB> AWBs { get; set; }

        public virtual ParticipantIdentifier ParticipantIdentifier { get; set; }
    }
}
