﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities.Enums
{
    public enum enumCheckListStatusTypes
    {
        Open = 0,
        Pending = 1,
        InProgress = 2,
        Completed = 3,
    }
}
