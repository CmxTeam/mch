﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities.Enums
{
    public enum enumBuildFreightStatusTypes
    {
        All = 1,
        Completed = 2,
        InProgress = 3,
        Open = 4,
        Pending = 5
    }
}
