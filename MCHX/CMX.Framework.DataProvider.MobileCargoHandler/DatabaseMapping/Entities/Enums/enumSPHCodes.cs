﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities.Enums
{
    [Flags]
    public enum enumSPHCodes
    {
        Oversize = 1,
        Human = 2,
        LicenseRequired = 4,
        LineAnimal = 8,
        Expedited = 16,
        HighValue= 32,
        TempControlled = 64,
        Perishable = 128,
        Hazmat = 256
    }
}
