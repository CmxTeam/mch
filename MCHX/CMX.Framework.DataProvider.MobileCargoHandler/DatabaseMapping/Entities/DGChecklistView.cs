namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DGChecklistView")]
    public partial class DGChecklistView
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long OnhandId { get; set; }

        public long? DGChecklistId { get; set; }

        [StringLength(50)]
        public string ShipmentNumber { get; set; }

        [StringLength(10)]
        public string Account { get; set; }

        [StringLength(255)]
        public string Place { get; set; }

        [StringLength(255)]
        public string CheckedBy { get; set; }

        public DateTime? DGDate { get; set; }

        public int? StatusId { get; set; }

        [StringLength(50)]
        public string StatusName { get; set; }

        [StringLength(500)]
        public string StatusImagePath { get; set; }
    }
}
