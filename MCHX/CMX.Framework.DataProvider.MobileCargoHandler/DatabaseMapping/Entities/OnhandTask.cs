namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OnhandTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OnhandTask()
        {
            OnhandTasks1 = new HashSet<OnhandTask>();
        }

        public long Id { get; set; }

        public int TaskTypeId { get; set; }

        public DateTime? TaskCreationDate { get; set; }

        public long? OnhandId { get; set; }

        [StringLength(50)]
        public string TaskCreator { get; set; }

        [StringLength(50)]
        public string TaskOwner { get; set; }

        [StringLength(100)]
        public string TaskReference { get; set; }

        public DateTime? TaskDate { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public int? WarehouseId { get; set; }

        public bool? IsPriority { get; set; }

        public long? ParentTaskId { get; set; }

        public virtual Onhand Onhand { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandTask> OnhandTasks1 { get; set; }

        public virtual OnhandTask OnhandTask1 { get; set; }

        public virtual Status Status { get; set; }

        public virtual TaskType TaskType { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
