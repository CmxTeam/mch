namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OSD
    {
        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        public long TaskId { get; set; }

        public long? AwbId { get; set; }

        public long? HwbId { get; set; }

        public long? ULDId { get; set; }

        public int Count { get; set; }

        public int ConditionId { get; set; }

        public long UserId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual Condition Condition { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual Task Task { get; set; }

        public virtual ULD ULD { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
