namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EntitySnapshot
    {
        public long Id { get; set; }

        public int? Count { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public DateTime? Timestamp { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public string DisplayText { get; set; }

        public string ImagePath { get; set; }

        public virtual EntityType EntityType { get; set; }
    }
}
