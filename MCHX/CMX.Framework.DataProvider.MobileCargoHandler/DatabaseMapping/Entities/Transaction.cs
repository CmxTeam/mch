namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Transaction
    {
        public long Id { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        [StringLength(100)]
        public string Reference { get; set; }

        public string Description { get; set; }

        public int? TaskTypeId { get; set; }

        public int? TransactionActionId { get; set; }

        public long? TaskId { get; set; }

        public long? UserId { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual Status Status { get; set; }

        public virtual Task Task { get; set; }

        public virtual TaskType TaskType { get; set; }

        public virtual TransactionAction TransactionAction { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
