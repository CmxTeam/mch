namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_CIMPOutboundQueue
    {
        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        [StringLength(30)]
        public string MessageType { get; set; }

        [StringLength(10)]
        public string StatusCode { get; set; }

        public long? FlightManifestId { get; set; }

        public long? AWBId { get; set; }

        public long? ULDId { get; set; }

        public long? HWBId { get; set; }

        public bool? Transmitted { get; set; }

        public bool? Emailed { get; set; }

        public string ErrorMessage { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public long StationId { get; set; }

        public DateTime? TransmittedOn { get; set; }

        public DateTime? EmailedOn { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual Port Port { get; set; }

        public virtual ULD ULD { get; set; }
    }
}
