namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CheckList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CheckList()
        {
            CheckListItemHeaders = new HashSet<CheckListItemHeader>();
            CheckListItems = new HashSet<CheckListItem>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(4)]
        public string Year { get; set; }

        [StringLength(255)]
        public string Header { get; set; }

        [StringLength(2000)]
        public string HeaderText { get; set; }

        [StringLength(50)]
        public string AddedBy { get; set; }

        public DateTime RecDate { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(255)]
        public string FooterText { get; set; }

        [StringLength(255)]
        public string PageFooterLeft { get; set; }

        [StringLength(255)]
        public string PageFooterRight { get; set; }

        public long TaskTypeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckListItemHeader> CheckListItemHeaders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckListItem> CheckListItems { get; set; }
    }
}
