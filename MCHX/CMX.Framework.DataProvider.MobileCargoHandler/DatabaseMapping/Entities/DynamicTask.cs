namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DynamicTask
    {
        public long Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Reference { get; set; }

        public DateTime TaskDate { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Assignees { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public double? Progress { get; set; }

        public bool IsReopened { get; set; }

        [StringLength(50)]
        public string Station { get; set; }

        [StringLength(50)]
        public string Warehouse { get; set; }

        [StringLength(50)]
        public string WarehouseLocation { get; set; }

        [StringLength(50)]
        public string OriginAirport { get; set; }

        [StringLength(50)]
        public string DestinationAirport { get; set; }

        [StringLength(50)]
        public string Airline { get; set; }

        [StringLength(50)]
        public string FlightNumber { get; set; }

        public DateTime? ETA { get; set; }

        public DateTime? ETD { get; set; }

        public bool? PAXorCAO { get; set; }

        [StringLength(256)]
        public string Driver { get; set; }

        public string ScreeningResult { get; set; }

        public string Alerts { get; set; }

        public int? SPHCodes { get; set; }

        public int TaskTypeId { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual TaskType TaskType { get; set; }
    }
}
