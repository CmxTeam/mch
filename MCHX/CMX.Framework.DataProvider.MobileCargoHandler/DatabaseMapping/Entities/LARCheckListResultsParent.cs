namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LARCheckListResultsParent")]
    public partial class LARCheckListResultsParent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LARCheckListResultsParent()
        {
            LARCheckListResults = new HashSet<LARCheckListResult>();
        }

        public int Id { get; set; }

        public int? AccountId { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(255)]
        public string Place { get; set; }

        public DateTime? CompletedOn { get; set; }

        public string Comments { get; set; }

        public string SignaturePath { get; set; }

        public DateTime? RecDate { get; set; }

        public long? TaskId { get; set; }

        public string SignatureData { get; set; }

        public virtual Account Account { get; set; }

        public virtual EntityType EntityType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LARCheckListResult> LARCheckListResults { get; set; }
    }
}
