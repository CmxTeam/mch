namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FlightNotocMaster")]
    public partial class FlightNotocMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FlightNotocMaster()
        {
            FlightNotocDGDetails = new HashSet<FlightNotocDGDetail>();
            FlightNotocOtherCargoDetails = new HashSet<FlightNotocOtherCargoDetail>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long FlightManifestId { get; set; }

        public long? PreparedBy { get; set; }

        [StringLength(100)]
        public string CheckedBy { get; set; }

        [StringLength(100)]
        public string LoadingSupervisor { get; set; }

        public string OtherInformation { get; set; }

        public byte[] Signature { get; set; }

        [StringLength(50)]
        public string Comment { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public long? LoadingStationId { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocDGDetail> FlightNotocDGDetails { get; set; }

        public virtual Port Port { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocOtherCargoDetail> FlightNotocOtherCargoDetails { get; set; }
    }
}
