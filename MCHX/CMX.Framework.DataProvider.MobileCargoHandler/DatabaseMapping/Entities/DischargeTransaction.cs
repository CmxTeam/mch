namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DischargeTransaction
    {
        public long Id { get; set; }

        public long? DischargeId { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public long? UserId { get; set; }

        public string Description { get; set; }

        public virtual Discharge Discharge { get; set; }

        public virtual Status Status { get; set; }
    }
}
