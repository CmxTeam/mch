namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AWBs_ULDs
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public long UldId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual ULD ULD { get; set; }
    }
}
