namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CarrierDriver
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CarrierDriver()
        {
            DriverSecurityLogs = new HashSet<DriverSecurityLog>();
            FlightManifests = new HashSet<FlightManifest>();
            Onhands = new HashSet<Onhand>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string FullName { get; set; }

        public int? TruckingCompanyId { get; set; }

        [StringLength(500)]
        public string DriverPhoto { get; set; }

        [Required]
        [StringLength(50)]
        public string LicenseNumber { get; set; }

        [StringLength(500)]
        public string LicenseImage { get; set; }

        public DateTime LicenseExpiry { get; set; }

        [StringLength(500)]
        public string DriverThumbnail { get; set; }

        [StringLength(50)]
        public string STANumber { get; set; }

        public int StateId { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverSecurityLog> DriverSecurityLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightManifest> FlightManifests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Onhand> Onhands { get; set; }
    }
}
