namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Overpack
    {
        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        [Column("Overpack")]
        [StringLength(50)]
        public string Overpack1 { get; set; }

        public int OverpackTypeId { get; set; }

        public int PieceNumber { get; set; }

        public long? AwbId { get; set; }

        public long? HwbId { get; set; }

        public int PiecesContained { get; set; }

        public long UserId { get; set; }

        public int WarehouseId { get; set; }

        public bool IsCustomerBuilt { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual ULDType ULDType { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
