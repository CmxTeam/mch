namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AmsCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AmsCode()
        {
            UserCustomsOverrides = new HashSet<UserCustomsOverride>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(3)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool IsGreenCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCustomsOverride> UserCustomsOverrides { get; set; }
    }
}
