namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSA_Approved_Carriers_KnownShippers_List
    {
        public long Id { get; set; }

        public int AccountId { get; set; }

        [Required]
        [StringLength(50)]
        public string ApprovalNumber { get; set; }

        [Required]
        [StringLength(250)]
        public string ShipperName { get; set; }

        [Required]
        [StringLength(150)]
        public string City { get; set; }

        public int StateId { get; set; }

        [Required]
        [StringLength(50)]
        public string PostalCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime ExpirationDate { get; set; }

        public bool IsActive { get; set; }

        public DateTime LastUpdated { get; set; }

        public long UpdatedByUserId { get; set; }

        public virtual Account Account { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
