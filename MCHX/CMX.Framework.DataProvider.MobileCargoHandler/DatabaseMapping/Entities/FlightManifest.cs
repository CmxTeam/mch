namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FlightManifest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FlightManifest()
        {
            FlightAttachments = new HashSet<FlightAttachment>();
            FlightNotocMasters = new HashSet<FlightNotocMaster>();
            FlightSnapshots = new HashSet<FlightSnapshot>();
            FlightTransactions = new HashSet<FlightTransaction>();
            HostPlus_CIMPOutboundQueue = new HashSet<HostPlus_CIMPOutboundQueue>();
            LoadingPlans = new HashSet<LoadingPlan>();
            ManifestAWBDetails = new HashSet<ManifestAWBDetail>();
            ManifestDetails = new HashSet<ManifestDetail>();
            ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            ULDs = new HashSet<ULD>();
            UnloadingPorts = new HashSet<UnloadingPort>();
        }

        public long Id { get; set; }

        public int? CarrierId { get; set; }

        [StringLength(50)]
        public string FlightNumber { get; set; }

        public DateTime? ETA { get; set; }

        public DateTime? ETD { get; set; }

        public int? TotalPieces { get; set; }

        public int? TotalULDs { get; set; }

        public int? TotalAWBs { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public int? ReceivedPieces { get; set; }

        public bool? IsOSD { get; set; }

        public int? StagingLocationId { get; set; }

        [StringLength(50)]
        public string RecoveredBy { get; set; }

        public DateTime? RecoveredOn { get; set; }

        public int? RecoveredULDs { get; set; }

        public int? PutAwayPieces { get; set; }

        public double? PercentRecovered { get; set; }

        public double? PercentReceived { get; set; }

        public double? PercentPutAway { get; set; }

        [StringLength(10)]
        public string AircraftRegistration { get; set; }

        public DateTime? ArrivalPortETA { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public bool? IsComplete { get; set; }

        public DateTime? RecDate { get; set; }

        public double? PercentOverall { get; set; }

        public int? AccountId { get; set; }

        public int? RecoveredLocationId { get; set; }

        [StringLength(50)]
        public string StagedBy { get; set; }

        public DateTime? StagedOn { get; set; }

        public bool? IsOnhold { get; set; }

        public int? TenderedCarrierId { get; set; }

        public long? TenderedDriverId { get; set; }

        public byte[] TenderedDriverSignature { get; set; }

        public int? TenderedTotalPieces { get; set; }

        [StringLength(50)]
        public string TenderedBy { get; set; }

        public DateTime? TenderedTimestamp { get; set; }

        public bool? IsPassenger { get; set; }

        public virtual Account Account { get; set; }

        public virtual CarrierDriver CarrierDriver { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual Carrier Carrier1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightAttachment> FlightAttachments { get; set; }

        public virtual Port Port { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }

        public virtual Status Status { get; set; }

        public virtual WarehouseLocation WarehouseLocation1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocMaster> FlightNotocMasters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightSnapshot> FlightSnapshots { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightTransaction> FlightTransactions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HostPlus_CIMPOutboundQueue> HostPlus_CIMPOutboundQueue { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoadingPlan> LoadingPlans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestAWBDetail> ManifestAWBDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ULD> ULDs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UnloadingPort> UnloadingPorts { get; set; }
    }
}
