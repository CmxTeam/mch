namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningMethod
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScreeningMethod()
        {
            CargoAcceptScreeningLogs = new HashSet<CargoAcceptScreeningLog>();
            ScreeningDevices = new HashSet<ScreeningDevice>();
            ScreeningTransactions = new HashSet<ScreeningTransaction>();
        }

        public long Id { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public bool? IsPrimary { get; set; }

        [Required]
        [StringLength(50)]
        public string DisplayName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CargoAcceptScreeningLog> CargoAcceptScreeningLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningDevice> ScreeningDevices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningTransaction> ScreeningTransactions { get; set; }
    }
}
