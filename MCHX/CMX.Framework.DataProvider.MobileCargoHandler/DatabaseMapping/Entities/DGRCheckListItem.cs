namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DGRCheckListItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DGRCheckListItem()
        {
            DGRCheckListItems1 = new HashSet<DGRCheckListItem>();
            DGRCheckListResults = new HashSet<DGRCheckListResult>();
        }

        public int ListItemHeaderId { get; set; }

        public int? ListItemNo { get; set; }

        [Required]
        public string ListItemText { get; set; }

        [StringLength(50)]
        public string ValueList { get; set; }

        public int? SortOrder { get; set; }

        public bool IsActive { get; set; }

        public int DGRCheckListId { get; set; }

        public DateTime? RecDate { get; set; }

        public int Id { get; set; }

        public int? ParentId { get; set; }

        public virtual DGRCheckListItemHeader DGRCheckListItemHeader { get; set; }

        public virtual DGRCheckList DGRCheckList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DGRCheckListItem> DGRCheckListItems1 { get; set; }

        public virtual DGRCheckListItem DGRCheckListItem1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DGRCheckListResult> DGRCheckListResults { get; set; }
    }
}
