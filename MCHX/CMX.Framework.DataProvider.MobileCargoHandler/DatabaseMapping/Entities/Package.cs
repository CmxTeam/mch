namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Package
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Package()
        {
            Dispositions = new HashSet<Disposition>();
            FlightSnapshots = new HashSet<FlightSnapshot>();
            ForkliftDetails = new HashSet<ForkliftDetail>();
            HWBs_Packages = new HashSet<HWBs_Packages>();
            ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            OnhandTransactions = new HashSet<OnhandTransaction>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public int PieceNumber { get; set; }

        [StringLength(50)]
        public string TrackingNumber { get; set; }

        public int SaidToContain { get; set; }

        public double Length { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        public double Volume { get; set; }

        public int PackageTypeId { get; set; }

        public int? DimUOMId { get; set; }

        public int? VolumeUOMId { get; set; }

        public int? WeightUOMId { get; set; }

        public int? WarehouseLocationId { get; set; }

        public int StatusId { get; set; }

        public int? ReceivedPieces { get; set; }

        public int? AvailablePieces { get; set; }

        public int? ForkliftPieces { get; set; }

        public int? PutAwayPieces { get; set; }

        public double? PercentReceived { get; set; }

        public double? PercentPutAway { get; set; }

        public bool? IsOSD { get; set; }

        public int? AccountId { get; set; }

        public long? ManifestId { get; set; }

        public long? OnhandId { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public bool? IsHazmat { get; set; }

        public long? PackageGroupId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disposition> Dispositions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightSnapshot> FlightSnapshots { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForkliftDetail> ForkliftDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HWBs_Packages> HWBs_Packages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }

        public virtual Manifest Manifest { get; set; }

        public virtual Onhand Onhand { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandTransaction> OnhandTransactions { get; set; }

        public virtual PackageGroup PackageGroup { get; set; }

        public virtual Port Port { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual PackageType PackageType { get; set; }

        public virtual Status Status { get; set; }

        public virtual UOM UOM1 { get; set; }

        public virtual UOM UOM2 { get; set; }
    }
}
