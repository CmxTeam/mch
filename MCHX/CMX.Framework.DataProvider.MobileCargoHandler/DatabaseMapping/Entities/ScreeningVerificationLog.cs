namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningVerificationLog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScreeningVerificationLog()
        {
            AlternateScreeningLogs = new HashSet<AlternateScreeningLog>();
            ScreeningSealLogs = new HashSet<ScreeningSealLog>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long ShipperManifestId { get; set; }

        public long AwbId { get; set; }

        public bool TenderedAsScreened { get; set; }

        public long? CCSFId { get; set; }

        public bool? EligibleForAltScreening { get; set; }

        public long UserId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlternateScreeningLog> AlternateScreeningLogs { get; set; }

        public virtual AWB AWB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningSealLog> ScreeningSealLogs { get; set; }

        public virtual ShipperManifest ShipperManifest { get; set; }

        public virtual TSA_Approved_Certified_Screening_Facilities TSA_Approved_Certified_Screening_Facilities { get; set; }
    }
}
