namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HWB
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HWB()
        {
            AWBs_HWBs = new HashSet<AWBs_HWBs>();
            ChargeDeclarations = new HashSet<ChargeDeclaration>();
            CustomsNotifications = new HashSet<CustomsNotification>();
            DescriptionOfGoods1 = new HashSet<DescriptionOfGood>();
            DischargeDetails = new HashSet<DischargeDetail>();
            DischargeShipments = new HashSet<DischargeShipment>();
            Dispositions = new HashSet<Disposition>();
            ForkliftDetails = new HashSet<ForkliftDetail>();
            HostPlus_CIMPOutboundQueue = new HashSet<HostPlus_CIMPOutboundQueue>();
            HWBHTSSets = new HashSet<HWBHTSSet>();
            HWBs_Packages = new HashSet<HWBs_Packages>();
            ManifestDetails = new HashSet<ManifestDetail>();
            ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            OCIs = new HashSet<OCI>();
            OSDs = new HashSet<OSD>();
            Overpacks = new HashSet<Overpack>();
            SpecialHandlings = new HashSet<SpecialHandling>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(12)]
        public string HWBSerialNumber { get; set; }

        public int Pieces { get; set; }

        public int SLAC { get; set; }

        public double Weight { get; set; }

        public int? WeightUOMId { get; set; }

        public int ReceivedPieces { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PercentReceived { get; set; }

        public int ForkliftPieces { get; set; }

        [StringLength(15)]
        public string DescriptionOfGoods { get; set; }

        public int? ShipperId { get; set; }

        public int? ConsigneeId { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public int? WarehouseLocationId { get; set; }

        public bool? IsOSD { get; set; }

        [Column(TypeName = "money")]
        public decimal? CollectFee { get; set; }

        public bool? IsCollect { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        [StringLength(20)]
        public string ServiceType { get; set; }

        public int? AccountId { get; set; }

        [StringLength(50)]
        public string ExportControl { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AWBs_HWBs> AWBs_HWBs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChargeDeclaration> ChargeDeclarations { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Customer Customer1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomsNotification> CustomsNotifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DescriptionOfGood> DescriptionOfGoods1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeDetail> DischargeDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeShipment> DischargeShipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disposition> Dispositions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForkliftDetail> ForkliftDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HostPlus_CIMPOutboundQueue> HostPlus_CIMPOutboundQueue { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HWBHTSSet> HWBHTSSets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HWBs_Packages> HWBs_Packages { get; set; }

        public virtual Port Port { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual Status Status { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OCI> OCIs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSD> OSDs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Overpack> Overpacks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandling> SpecialHandlings { get; set; }
    }
}
