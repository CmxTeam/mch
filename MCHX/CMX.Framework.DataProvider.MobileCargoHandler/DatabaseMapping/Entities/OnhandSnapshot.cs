namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OnhandSnapshot
    {
        public long Id { get; set; }

        public int? Count { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public DateTime? Timestamp { get; set; }

        public long? OnhandId { get; set; }

        public string Conditions { get; set; }

        public string DisplayText { get; set; }

        public byte[] SnapshotImage { get; set; }

        public virtual Onhand Onhand { get; set; }
    }
}
