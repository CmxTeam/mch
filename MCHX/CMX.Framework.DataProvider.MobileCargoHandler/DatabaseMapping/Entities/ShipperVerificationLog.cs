namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ShipperVerificationLog
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public long ShipperManifestId { get; set; }

        public long FieldId { get; set; }

        public string Value { get; set; }

        public long UserId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual ShipperManifest ShipperManifest { get; set; }

        public virtual ShipperVerificationField ShipperVerificationField { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
