namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PutedAwayItem
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Type { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId { get; set; }

        [StringLength(36)]
        public string Reference { get; set; }

        [StringLength(12)]
        public string Reference1 { get; set; }

        public int? IsBUP { get; set; }

        public int? WarehouseLocationId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Count { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long DispositionId { get; set; }

        public int? Status { get; set; }

        [StringLength(33)]
        public string PaId { get; set; }
    }
}
