namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LARCheckListItemHeader
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LARCheckListItemHeader()
        {
            LARCheckListItems = new HashSet<LARCheckListItem>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string ListItemHeader { get; set; }

        public int? SortOrder { get; set; }

        public bool? IsActive { get; set; }

        public int? LARCheckListId { get; set; }

        public virtual LARCheckList LARCheckList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LARCheckListItem> LARCheckListItems { get; set; }
    }
}
