namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FlightSnapshot
    {
        public long Id { get; set; }

        public int? Count { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public DateTime? Timestamp { get; set; }

        public long? FlightManifestId { get; set; }

        public string Conditions { get; set; }

        public string DisplayText { get; set; }

        public byte[] SnapshotImage { get; set; }

        public long? ULDId { get; set; }

        public long? PackageId { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        public virtual Package Package { get; set; }

        public virtual ULD ULD { get; set; }
    }
}
