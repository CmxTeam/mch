namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RecoverFlightView")]
    public partial class RecoverFlightView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long FlightManifestId { get; set; }

        public DateTime? ETA { get; set; }

        [StringLength(56)]
        public string Flight { get; set; }

        [StringLength(10)]
        public string Ref { get; set; }

        public int? Ulds { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string Account { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusId { get; set; }
    }
}
