namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CarrierCIMPMailbox
    {
        public int Id { get; set; }

        public int CarrierId { get; set; }

        [Required]
        [StringLength(50)]
        public string SenderMailbox { get; set; }

        [Required]
        [StringLength(50)]
        public string ReceiverMailBox { get; set; }

        public virtual Carrier Carrier { get; set; }
    }
}
