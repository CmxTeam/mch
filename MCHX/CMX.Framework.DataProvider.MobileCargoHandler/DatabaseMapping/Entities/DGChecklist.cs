namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DGChecklist
    {
        public long Id { get; set; }

        [StringLength(255)]
        public string H1 { get; set; }

        [Required]
        [StringLength(1)]
        public string A1 { get; set; }

        [Required]
        [StringLength(1)]
        public string A2 { get; set; }

        [Required]
        [StringLength(1)]
        public string A3 { get; set; }

        [Required]
        [StringLength(1)]
        public string A4 { get; set; }

        [Required]
        [StringLength(1)]
        public string A5 { get; set; }

        [Required]
        [StringLength(1)]
        public string A6 { get; set; }

        [Required]
        [StringLength(1)]
        public string A7 { get; set; }

        [StringLength(255)]
        public string H2 { get; set; }

        [Required]
        [StringLength(1)]
        public string A8 { get; set; }

        [Required]
        [StringLength(1)]
        public string A9 { get; set; }

        [Required]
        [StringLength(1)]
        public string A10 { get; set; }

        [Required]
        [StringLength(1)]
        public string A11 { get; set; }

        [Required]
        [StringLength(1)]
        public string A12 { get; set; }

        [StringLength(255)]
        public string H3 { get; set; }

        [Required]
        [StringLength(1)]
        public string A13 { get; set; }

        [Required]
        [StringLength(1)]
        public string A14 { get; set; }

        [StringLength(255)]
        public string Q15Hdr { get; set; }

        [Required]
        [StringLength(1)]
        public string A151 { get; set; }

        [Required]
        [StringLength(1)]
        public string A152 { get; set; }

        [Required]
        [StringLength(1)]
        public string A153 { get; set; }

        [Required]
        [StringLength(1)]
        public string A154 { get; set; }

        [StringLength(255)]
        public string Q16Hdr { get; set; }

        [Required]
        [StringLength(1)]
        public string A161 { get; set; }

        [Required]
        [StringLength(1)]
        public string A162 { get; set; }

        [Required]
        [StringLength(1)]
        public string A163 { get; set; }

        [StringLength(255)]
        public string H4 { get; set; }

        [Required]
        [StringLength(1)]
        public string A17 { get; set; }

        [Required]
        [StringLength(1)]
        public string A18 { get; set; }

        [StringLength(255)]
        public string H5 { get; set; }

        [Required]
        [StringLength(1)]
        public string A19 { get; set; }

        [Required]
        [StringLength(1)]
        public string A20 { get; set; }

        [StringLength(255)]
        public string H6 { get; set; }

        [Required]
        [StringLength(1)]
        public string A21 { get; set; }

        [Required]
        [StringLength(1)]
        public string A22 { get; set; }

        [Required]
        [StringLength(1)]
        public string A23 { get; set; }

        [Required]
        [StringLength(1)]
        public string A24 { get; set; }

        [StringLength(255)]
        public string H7 { get; set; }

        [Required]
        [StringLength(1)]
        public string A25 { get; set; }

        [Required]
        [StringLength(1)]
        public string A26 { get; set; }

        [Required]
        [StringLength(1)]
        public string A27 { get; set; }

        [StringLength(255)]
        public string H8 { get; set; }

        [Required]
        [StringLength(1)]
        public string A28 { get; set; }

        [Required]
        [StringLength(1)]
        public string A29 { get; set; }

        [StringLength(255)]
        public string H9 { get; set; }

        [StringLength(1)]
        public string Q30Hdr { get; set; }

        [Required]
        [StringLength(1)]
        public string A301 { get; set; }

        [Required]
        [StringLength(1)]
        public string A302 { get; set; }

        [Required]
        [StringLength(1)]
        public string A303 { get; set; }

        [Required]
        [StringLength(1)]
        public string A304 { get; set; }

        [Required]
        [StringLength(1)]
        public string A31 { get; set; }

        [Required]
        [StringLength(1)]
        public string A32 { get; set; }

        [Required]
        [StringLength(1)]
        public string A33 { get; set; }

        [Required]
        [StringLength(1)]
        public string A34 { get; set; }

        [Required]
        [StringLength(1)]
        public string A35 { get; set; }

        [Required]
        [StringLength(1)]
        public string A36 { get; set; }

        [Required]
        [StringLength(1)]
        public string A37 { get; set; }

        [Required]
        [StringLength(1)]
        public string A38 { get; set; }

        [Required]
        [StringLength(1)]
        public string A39 { get; set; }

        [StringLength(255)]
        public string H10 { get; set; }

        [Required]
        [StringLength(1)]
        public string A40 { get; set; }

        [Required]
        [StringLength(1)]
        public string A41 { get; set; }

        [Required]
        [StringLength(1)]
        public string A42 { get; set; }

        [Required]
        [StringLength(1)]
        public string A43 { get; set; }

        [Required]
        [StringLength(1)]
        public string A44 { get; set; }

        [Required]
        [StringLength(1)]
        public string A45 { get; set; }

        [Required]
        [StringLength(1)]
        public string A46 { get; set; }

        [Required]
        [StringLength(1)]
        public string A47 { get; set; }

        [StringLength(255)]
        public string H11 { get; set; }

        [Required]
        [StringLength(1)]
        public string A48 { get; set; }

        [Required]
        [StringLength(1)]
        public string A49 { get; set; }

        [Required]
        [StringLength(1)]
        public string A50 { get; set; }

        [Required]
        [StringLength(1)]
        public string A51 { get; set; }

        [StringLength(255)]
        public string H12 { get; set; }

        [Required]
        [StringLength(1)]
        public string A52 { get; set; }

        [Required]
        [StringLength(1)]
        public string A53 { get; set; }

        [Required]
        [StringLength(1)]
        public string A54 { get; set; }

        [StringLength(500)]
        public string Comments { get; set; }

        [StringLength(255)]
        public string CheckedBy { get; set; }

        [StringLength(255)]
        public string Place { get; set; }

        public byte[] SignatureImage { get; set; }

        public long OnhandId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public virtual Onhand Onhand { get; set; }
    }
}
