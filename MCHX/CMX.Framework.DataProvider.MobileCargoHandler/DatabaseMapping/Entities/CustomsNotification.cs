namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomsNotification
    {
        public long Id { get; set; }

        [StringLength(5)]
        public string CustomsCode { get; set; }

        [StringLength(50)]
        public string CodeDescription { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        [StringLength(1)]
        public string Part { get; set; }

        [StringLength(50)]
        public string FlightNumber { get; set; }

        public int? Slac { get; set; }

        public long? HWBId { get; set; }

        public long? AWBId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual HWB HWB { get; set; }
    }
}
