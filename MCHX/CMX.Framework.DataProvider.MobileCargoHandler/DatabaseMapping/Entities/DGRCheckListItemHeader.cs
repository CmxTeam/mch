namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DGRCheckListItemHeader
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DGRCheckListItemHeader()
        {
            DGRCheckListItems = new HashSet<DGRCheckListItem>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string ListItemHeader { get; set; }

        public int? SortOrder { get; set; }

        public bool? IsActive { get; set; }

        public int? DGRCheckListId { get; set; }

        public virtual DGRCheckList DGRCheckList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DGRCheckListItem> DGRCheckListItems { get; set; }
    }
}
