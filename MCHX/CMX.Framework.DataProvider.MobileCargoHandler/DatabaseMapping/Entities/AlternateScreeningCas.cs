namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AlternateScreeningCases")]
    public partial class AlternateScreeningCas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AlternateScreeningCas()
        {
            AccountAlternateScreenings = new HashSet<AccountAlternateScreening>();
            AlternateScreeningLogs = new HashSet<AlternateScreeningLog>();
        }

        public long Id { get; set; }

        public int AircraftTypeId { get; set; }

        [Required]
        [StringLength(250)]
        public string Requirement { get; set; }

        public long? SpecialHandlingGroupId { get; set; }

        public long? ShipperTypeId { get; set; }

        public double? AllowedWeightInKGS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountAlternateScreening> AccountAlternateScreenings { get; set; }

        public virtual AircraftType AircraftType { get; set; }

        public virtual ShipperType ShipperType { get; set; }

        public virtual SpecialHandlingGroup SpecialHandlingGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlternateScreeningLog> AlternateScreeningLogs { get; set; }
    }
}
