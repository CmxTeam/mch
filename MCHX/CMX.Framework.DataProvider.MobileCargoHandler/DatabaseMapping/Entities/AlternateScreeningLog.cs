namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AlternateScreeningLog
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long ScreeningLogId { get; set; }

        public long AltScreeningCaseId { get; set; }

        public bool Value { get; set; }

        public long UserId { get; set; }

        public virtual AlternateScreeningCas AlternateScreeningCas { get; set; }

        public virtual ScreeningVerificationLog ScreeningVerificationLog { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
