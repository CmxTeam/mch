namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OnhandAttachment
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(255)]
        public string FileType { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public long OnhandID { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public byte[] DocumentImage { get; set; }

        public virtual Onhand Onhand { get; set; }
    }
}
