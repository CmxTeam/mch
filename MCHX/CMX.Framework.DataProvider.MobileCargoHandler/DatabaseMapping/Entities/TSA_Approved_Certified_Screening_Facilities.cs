namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSA_Approved_Certified_Screening_Facilities
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TSA_Approved_Certified_Screening_Facilities()
        {
            ScreeningVerificationLogs = new HashSet<ScreeningVerificationLog>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string TSAApprovalNumber { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(255)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        public int? StateId { get; set; }

        [StringLength(13)]
        public string ZipCode { get; set; }

        public int? CountryId { get; set; }

        public DateTime? ExpirationDate { get; set; }

        [StringLength(50)]
        public string IACAgentNumber { get; set; }

        public DateTime? IACExpirationDate { get; set; }

        public DateTime? SuspensionDate { get; set; }

        [StringLength(100)]
        public string CCSFStatus { get; set; }

        [StringLength(100)]
        public string CCSFStatusUpdate { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual Country Country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningVerificationLog> ScreeningVerificationLogs { get; set; }

        public virtual State State { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }
    }
}
