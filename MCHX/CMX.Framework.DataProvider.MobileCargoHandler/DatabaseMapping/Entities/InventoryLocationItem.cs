namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class InventoryLocationItem
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Type { get; set; }

        public long? EntityId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long DispositionId { get; set; }

        public long? TaskId { get; set; }

        public int? Count { get; set; }

        public int? WarehouseLocationId { get; set; }

        [StringLength(36)]
        public string RefNumber { get; set; }

        [StringLength(12)]
        public string RefNumber1 { get; set; }

        [StringLength(33)]
        public string PaId { get; set; }
    }
}
