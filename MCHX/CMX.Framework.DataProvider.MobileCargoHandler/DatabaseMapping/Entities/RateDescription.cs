namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RateDescription
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public int RateLineNumber { get; set; }

        public int? Line_Pieces { get; set; }

        public long? Line_RCPPortId { get; set; }

        public int? Line_WeightUOMId { get; set; }

        public double? Line_GrossWeight { get; set; }

        public int? Line_RateClassId { get; set; }

        [StringLength(10)]
        public string Line_Commodity_ItemNo { get; set; }

        [StringLength(5)]
        public string Line_Commodity_ULDRateClassType { get; set; }

        public int? Line_Commodity_RateClassId { get; set; }

        public int? Line_Commodity_ClassRatePercent { get; set; }

        public double? Line_ChargeableWeight { get; set; }

        [Column(TypeName = "money")]
        public decimal? Line_Rate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Line_TotalCharges { get; set; }

        public string Line_GoodsDescription { get; set; }

        public int? Slac { get; set; }

        [StringLength(50)]
        public string HarmonizedCommodityCode { get; set; }

        public int? OriginOfGoodsCountryId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual Country Country { get; set; }

        public virtual Port Port { get; set; }

        public virtual RateClassCode RateClassCode { get; set; }

        public virtual RateClassCode RateClassCode1 { get; set; }

        public virtual UOM UOM { get; set; }
    }
}
