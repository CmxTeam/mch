namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialHandlingSetting
    {
        public long Id { get; set; }

        public int? AccountId { get; set; }

        public long SPHGroupId { get; set; }

        public long? SPHSubGroupId { get; set; }

        public int ChecklistTaskTypeId { get; set; }

        public int TaskCount { get; set; }

        public long? UpdatedBy { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual Account Account { get; set; }

        public virtual SpecialHandlingGroup SpecialHandlingGroup { get; set; }

        public virtual SpecialHandlingSubGroup SpecialHandlingSubGroup { get; set; }

        public virtual TaskType TaskType { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
