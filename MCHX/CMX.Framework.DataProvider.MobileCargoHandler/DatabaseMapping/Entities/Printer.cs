namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Printer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Printer()
        {
            UserPrinters = new HashSet<UserPrinter>();
        }

        public int Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Port { get; set; }

        public int TypeId { get; set; }

        public bool IsDefault { get; set; }

        public int? ServerId { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public virtual PrinterType PrinterType { get; set; }

        public virtual PrintServer PrintServer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserPrinter> UserPrinters { get; set; }
    }
}
