namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ForkliftView")]
    public partial class ForkliftView
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Type { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId { get; set; }

        [StringLength(36)]
        public string RefNumber { get; set; }

        [StringLength(12)]
        public string RefNumber1 { get; set; }

        public int? Pieces { get; set; }

        public int? IsBUP { get; set; }

        [StringLength(256)]
        public string UserName { get; set; }

        [StringLength(33)]
        public string PaId { get; set; }

        public long? TaskId { get; set; }
    }
}
