namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WarehouseLocationType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WarehouseLocationType()
        {
            WarehouseLocations = new HashSet<WarehouseLocation>();
        }

        public int Id { get; set; }

        [StringLength(20)]
        public string LocationType { get; set; }

        [StringLength(5)]
        public string Code { get; set; }

        [StringLength(2)]
        public string BarcodeIdentifier { get; set; }

        [StringLength(25)]
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WarehouseLocation> WarehouseLocations { get; set; }
    }
}
