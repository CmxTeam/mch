namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskInstruction
    {
        public long Id { get; set; }

        public long? TaskId { get; set; }

        public long? UserId { get; set; }

        public string InstructionText { get; set; }

        public DateTime? RecDate { get; set; }

        public virtual Task Task { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
