namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ChargeSummary
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public bool IsChargeCollect { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalChargesWeight { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalChargesValuation { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalChargesTax { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalOtherChargesDueAgent { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalOtherChargesDueCarrier { get; set; }

        [Column(TypeName = "money")]
        public decimal TotalCharges { get; set; }

        public virtual AWB AWB { get; set; }
    }
}
