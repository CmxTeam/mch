namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LabelTemplatesZPL")]
    public partial class LabelTemplatesZPL
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string LabelName { get; set; }

        public int? AccountId { get; set; }

        [Required]
        public string LabelTemplateZPL { get; set; }

        public int? LabelTypeId { get; set; }

        public virtual Account Account { get; set; }

        public virtual PrinterLabelType PrinterLabelType { get; set; }
    }
}
