namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningTransaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScreeningTransaction()
        {
            ITDX_Transactions = new HashSet<ITDX_Transactions>();
        }

        public int Id { get; set; }

        public DateTime? Created { get; set; }

        public int ScreenedPcs { get; set; }

        [StringLength(255)]
        public string SampleNumber { get; set; }

        public DateTime ScreeningTimestamp { get; set; }

        [Required]
        [StringLength(255)]
        public string Result { get; set; }

        public long Screener { get; set; }

        public int WarehouseId { get; set; }

        public long? PrescreenedAtCCSFId { get; set; }

        public int? DeviceId { get; set; }

        public long MethodId { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        [StringLength(255)]
        public string ClearAlarmComment { get; set; }

        public DateTime? AlarmClearedTimestamp { get; set; }

        public long? AlarmClearedBy { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public byte? IsProcessed { get; set; }

        public DateTime? ProcessedOn { get; set; }

        public virtual EntityType EntityType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ITDX_Transactions> ITDX_Transactions { get; set; }

        public virtual ScreeningDevice ScreeningDevice { get; set; }

        public virtual ScreeningMethod ScreeningMethod { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        public virtual Shipper Shipper { get; set; }
    }
}
