namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UnloadingPort
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UnloadingPort()
        {
            LoadingPlans = new HashSet<LoadingPlan>();
            ManifestAWBDetails = new HashSet<ManifestAWBDetail>();
            ManifestDetails = new HashSet<ManifestDetail>();
            ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            ULDs = new HashSet<ULD>();
        }

        public int Id { get; set; }

        public DateTime? POU_ETD { get; set; }

        public DateTime? POU_ETA { get; set; }

        public bool? IsNilCargo { get; set; }

        public long? PortId { get; set; }

        public long? FlightManifestId { get; set; }

        public int TotalPieces { get; set; }

        public int ReceivedPieces { get; set; }

        public double PercentReceived { get; set; }

        public int PutAwayPieces { get; set; }

        public double PercentPutAway { get; set; }

        [StringLength(50)]
        public string RecoveredBy { get; set; }

        public DateTime? RecoveredOn { get; set; }

        public int TotalUlds { get; set; }

        public int TotalAwbs { get; set; }

        public int? RecoveredLocationId { get; set; }

        public int RecoveredUlds { get; set; }

        public double PercentOverall { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public double PercentRecovered { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoadingPlan> LoadingPlans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestAWBDetail> ManifestAWBDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }

        public virtual Port Port { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ULD> ULDs { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }
    }
}
