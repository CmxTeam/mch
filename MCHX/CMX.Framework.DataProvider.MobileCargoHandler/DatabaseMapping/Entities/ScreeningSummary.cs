namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScreeningSummary")]
    public partial class ScreeningSummary
    {
        public long Id { get; set; }

        public int EntityTypeId { get; set; }

        public long EntityId { get; set; }

        public int? TotalPcsFromTransactions { get; set; }

        public int ProcessedPcs { get; set; }

        [Required]
        [StringLength(50)]
        public string CurrentStatus { get; set; }

        public DateTime RecDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public int AlarmPcs { get; set; }

        public int ClearedPcs { get; set; }

        public int PassedPcs { get; set; }

        public virtual EntityType EntityType { get; set; }
    }
}
