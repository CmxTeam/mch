namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ManifestDetail
    {
        public long Id { get; set; }

        public long? AWBId { get; set; }

        public long? HWBId { get; set; }

        public long? FlightManifestId { get; set; }

        [StringLength(2)]
        public string Part { get; set; }

        public int? LoadCount { get; set; }

        public int? ReceivedCount { get; set; }

        public double? PercentReceived { get; set; }

        public long? ULDId { get; set; }

        public double? Weight { get; set; }

        public int? WeightUOMId { get; set; }

        public double? Volume { get; set; }

        public int? VolumeUOMId { get; set; }

        public bool? DensityIndicator { get; set; }

        public int? ConsignmentCodeId { get; set; }

        public int? DensityGroupId { get; set; }

        public int? UnloadingPortId { get; set; }

        public DateTime? RecDate { get; set; }

        public int? PutAwayPieces { get; set; }

        public double? PercentPutAway { get; set; }

        public bool? IsBUP { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual ConsignmentCode ConsignmentCode { get; set; }

        public virtual DensityGroup DensityGroup { get; set; }

        public virtual FlightManifest FlightManifest { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual ULD ULD { get; set; }

        public virtual UnloadingPort UnloadingPort { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }
    }
}
