namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AttributeItem
    {
        public long Id { get; set; }

        public DateTime? RecDate { get; set; }

        public long? AttributeId { get; set; }

        [Column("AttributeItem")]
        [StringLength(150)]
        public string AttributeItem1 { get; set; }

        public virtual Attribute Attribute { get; set; }
    }
}
