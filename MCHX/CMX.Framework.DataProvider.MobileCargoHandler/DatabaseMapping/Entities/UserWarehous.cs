namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserWarehouses")]
    public partial class UserWarehous
    {
        public int Id { get; set; }

        public long? UserId { get; set; }

        public int? WarehouseId { get; set; }

        public bool IsDefault { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
