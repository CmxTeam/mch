namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EmailFromDefault
    {
        public int Id { get; set; }

        public long AutoCompleteFromEmailId { get; set; }

        public int? AccountId { get; set; }

        public bool IsDefault { get; set; }

        public virtual Account Account { get; set; }

        public virtual AutoCompleteFromEmail AutoCompleteFromEmail { get; set; }
    }
}
