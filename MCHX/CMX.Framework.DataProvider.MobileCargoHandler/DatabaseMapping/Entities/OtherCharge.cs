namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OtherCharge
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public bool IsCollect { get; set; }

        public long CodeId { get; set; }

        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual ChargeCode ChargeCode { get; set; }
    }
}
