namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccountingInformation")]
    public partial class AccountingInformation
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public int AccountInfoId { get; set; }

        [Required]
        [StringLength(34)]
        public string AccountingInfo { get; set; }

        public virtual AccountingInfoIdentifier AccountingInfoIdentifier { get; set; }

        public virtual AWB AWB { get; set; }
    }
}
