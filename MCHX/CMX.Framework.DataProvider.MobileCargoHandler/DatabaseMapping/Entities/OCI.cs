namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OCI
    {
        public int Id { get; set; }

        public DateTime? RecDate { get; set; }

        public long? AWBId { get; set; }

        public long? HWBId { get; set; }

        [StringLength(35)]
        public string SupCustomsInfo { get; set; }

        public int? CountryId { get; set; }

        public int? CustomsInfoIdentifierId { get; set; }

        public int? LineIdentifierId { get; set; }

        public long? ULDId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual Country Country { get; set; }

        public virtual CustomsInfoIdentifier CustomsInfoIdentifier { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual LineIdentifier LineIdentifier { get; set; }

        public virtual ULD ULD { get; set; }
    }
}
