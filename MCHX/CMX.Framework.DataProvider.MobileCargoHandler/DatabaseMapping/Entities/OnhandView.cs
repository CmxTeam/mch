namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OnhandView")]
    public partial class OnhandView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long OnhandId { get; set; }

        [StringLength(50)]
        public string OnhandNumber { get; set; }

        [StringLength(50)]
        public string TruckerPro { get; set; }

        public int? TotalPieces { get; set; }

        public double? TotalWeight { get; set; }

        public int? ReceivedPieces { get; set; }

        [StringLength(50)]
        public string CarrierName { get; set; }

        [StringLength(500)]
        public string StatusImagePath { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long TaskId { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(10)]
        public string AccountCode { get; set; }

        [StringLength(101)]
        public string ReceivedBy { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string StagedLocation { get; set; }

        [StringLength(4000)]
        public string ReceivedDate { get; set; }
    }
}
