namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HwbsView")]
    public partial class HwbsView
    {
        public long? FlightManifestId { get; set; }

        [StringLength(12)]
        public string HwbSerialNumber { get; set; }

        public int? Pieces { get; set; }

        [StringLength(50)]
        public string Consignee { get; set; }

        [StringLength(150)]
        public string City { get; set; }

        [StringLength(50)]
        public string StateProvince { get; set; }

        public long? OnhandId { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long HwbId { get; set; }

        public int? ConsigneeId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HasError { get; set; }

        public string ErrorMessage { get; set; }

        [StringLength(250)]
        public string ContactName { get; set; }

        [StringLength(25)]
        public string ContactPhone { get; set; }

        public int? ContactId { get; set; }

        public int? StatusId { get; set; }
    }
}
