namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ScreeningSealLog
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long ScreeningLogId { get; set; }

        public int SealTypeId { get; set; }

        [StringLength(50)]
        public string SealNumber { get; set; }

        public long UserId { get; set; }

        public long? VerifiedByUserId { get; set; }

        public DateTime? VerifiedOn { get; set; }

        public bool? IsSealGood { get; set; }

        public long? OverrideByUserId { get; set; }

        public virtual ScreeningVerificationLog ScreeningVerificationLog { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }

        public virtual UserProfile UserProfile2 { get; set; }
    }
}
