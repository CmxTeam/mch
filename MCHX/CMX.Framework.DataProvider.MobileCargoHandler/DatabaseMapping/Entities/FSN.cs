namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FSN
    {
        public int Id { get; set; }

        public DateTime RecDate { get; set; }

        public DateTime? LastViewed { get; set; }

        [Required]
        [StringLength(3)]
        public string CCL_ArrivalAirportCode { get; set; }

        [Required]
        [StringLength(4)]
        public string CCL_CargoTerminalOperator { get; set; }

        [Required]
        [StringLength(3)]
        public string AWB_CarrierCode { get; set; }

        [Required]
        [StringLength(8)]
        public string AWB_MasterbillNumber { get; set; }

        [Required]
        [StringLength(1)]
        public string AWB_ConsolidationIdentifier { get; set; }

        [Required]
        [StringLength(12)]
        public string AWB_HousebillNumber { get; set; }

        [Required]
        [StringLength(35)]
        public string AWB_PackageTrackingIdentifier { get; set; }

        [Required]
        [StringLength(3)]
        public string ARR_ImportingCarrier { get; set; }

        [Required]
        [StringLength(5)]
        public string ARR_FlightNumber { get; set; }

        [Required]
        [StringLength(2)]
        public string ARR_ScheduledArrivalDay { get; set; }

        [Required]
        [StringLength(3)]
        public string ARR_ScheduledArrivalMonth { get; set; }

        [StringLength(1)]
        public string ARR_PartArrivalReference { get; set; }

        [StringLength(2)]
        public string CSN_ActionCode { get; set; }

        [StringLength(5)]
        public string CSN_NumberOfPieces { get; set; }

        [StringLength(2)]
        public string CSN_TransactionDay { get; set; }

        [StringLength(3)]
        public string CSN_TransactionMonth { get; set; }

        [StringLength(2)]
        public string CSN_TransactionHour { get; set; }

        [StringLength(2)]
        public string CSN_TransactionMinute { get; set; }

        [StringLength(2)]
        public string CSN_EntryType { get; set; }

        [StringLength(15)]
        public string CSN_EntryNumber { get; set; }

        [StringLength(20)]
        public string CSN_Remarks { get; set; }

        [StringLength(1)]
        public string ASN_StatusCode { get; set; }

        [StringLength(20)]
        public string ASN_ActionExplanation { get; set; }
    }
}
