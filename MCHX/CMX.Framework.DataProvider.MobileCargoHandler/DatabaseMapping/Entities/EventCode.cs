namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EventCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EventCode()
        {
            Events = new HashSet<Event>();
        }

        public int Id { get; set; }

        public DateTime RecDate { get; set; }

        [Column("EventCode")]
        [Required]
        [StringLength(20)]
        public string EventCode1 { get; set; }

        public int EventCodeOwnerId { get; set; }

        [StringLength(5)]
        public string EventSubCode { get; set; }

        [StringLength(50)]
        public string EventName { get; set; }

        public string Description { get; set; }

        public int? StatusId { get; set; }

        public virtual EventCodeOwner EventCodeOwner { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }
    }
}
