namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Discharge
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Discharge()
        {
            DischargeDetails = new HashSet<DischargeDetail>();
            DischargeShipments = new HashSet<DischargeShipment>();
        }

        public long Id { get; set; }

        public DateTime Date { get; set; }

        public long DriverLogId { get; set; }

        public long UserId { get; set; }

        public int WarehouseId { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeDetail> DischargeDetails { get; set; }

        public virtual DriverSecurityLog DriverSecurityLog { get; set; }

        public virtual Status Status { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeShipment> DischargeShipments { get; set; }
    }
}
