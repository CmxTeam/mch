namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AccountSpecialHandling
    {
        public long Id { get; set; }

        public int? AccountId { get; set; }

        public long SPHGroupId { get; set; }

        public int SortOrder { get; set; }

        public virtual Account Account { get; set; }

        public virtual SpecialHandlingGroup SpecialHandlingGroup { get; set; }
    }
}
