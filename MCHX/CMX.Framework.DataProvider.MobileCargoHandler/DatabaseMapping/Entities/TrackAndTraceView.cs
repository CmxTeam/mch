namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TrackAndTraceView")]
    public partial class TrackAndTraceView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ManifestPackageId { get; set; }

        public long? PackageId { get; set; }

        public long? FlightManifestId { get; set; }

        [StringLength(50)]
        public string ReferenceNumber { get; set; }

        [StringLength(12)]
        public string HWBNumber { get; set; }

        [StringLength(50)]
        public string ConsigneeName { get; set; }

        [StringLength(100)]
        public string AddressLine1 { get; set; }

        [StringLength(206)]
        public string AddressLine2 { get; set; }

        [StringLength(50)]
        public string PackageType { get; set; }

        [StringLength(99)]
        public string Dimensions { get; set; }

        [StringLength(33)]
        public string PackageWeight { get; set; }

        [StringLength(15)]
        public string DescriptionOfGoods { get; set; }

        [StringLength(35)]
        public string TotalPieces { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime StatusTimestamp { get; set; }

        [StringLength(50)]
        public string PackageStatus { get; set; }

        public long? OnhandId { get; set; }

        [StringLength(50)]
        public string OnhandNumber { get; set; }

        [StringLength(50)]
        public string ConsigneeName1 { get; set; }

        [StringLength(100)]
        public string AddressLine11 { get; set; }

        [StringLength(206)]
        public string AddressLine21 { get; set; }

        [StringLength(50)]
        public string PackageType1 { get; set; }

        [StringLength(99)]
        public string Dimensions1 { get; set; }

        [StringLength(33)]
        public string PackageWeight1 { get; set; }

        [StringLength(255)]
        public string DescriptionOfGoods1 { get; set; }

        [StringLength(35)]
        public string TotalPieces1 { get; set; }

        public DateTime? RecoveredOn { get; set; }

        public DateTime? ReceivedOn { get; set; }

        public DateTime? ShippedOn { get; set; }

        public DateTime? DeliveredOn { get; set; }

        [StringLength(50)]
        public string DeliveredTo { get; set; }

        [StringLength(50)]
        public string Carrier { get; set; }

        [StringLength(100)]
        public string TrackingNumber { get; set; }

        [StringLength(50)]
        public string ShipmentNumber { get; set; }
    }
}
