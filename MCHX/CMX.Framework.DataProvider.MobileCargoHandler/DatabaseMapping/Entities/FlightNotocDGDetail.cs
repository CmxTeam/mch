namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FlightNotocDGDetail
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long FlightNotocId { get; set; }

        public long? AWBId { get; set; }

        [StringLength(500)]
        public string ProperShippingName { get; set; }

        public int? UNClassId { get; set; }

        [StringLength(50)]
        public string UNNumber { get; set; }

        [StringLength(50)]
        public string PackingGroup { get; set; }

        public int? NoOfPackages { get; set; }

        [StringLength(500)]
        public string PackingTypeAndQty { get; set; }

        [StringLength(500)]
        public string RadioactiveMaterials { get; set; }

        [StringLength(50)]
        public string IMPCode { get; set; }

        [StringLength(50)]
        public string EmergencyResponseCode { get; set; }

        public long? ULDId { get; set; }

        public int? AircraftTypeId { get; set; }

        [StringLength(50)]
        public string AircraftPosition { get; set; }

        [StringLength(50)]
        public string SubRisk { get; set; }

        public virtual AircraftType AircraftType { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual FlightNotocMaster FlightNotocMaster { get; set; }

        public virtual ULD ULD { get; set; }

        public virtual UNClass UNClass { get; set; }
    }
}
