namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SnapshotCondition
    {
        public long Id { get; set; }

        public long SnapshotId { get; set; }

        public int ConditionId { get; set; }

        public decimal? XPosition { get; set; }

        public decimal? YPosition { get; set; }

        public virtual Condition Condition { get; set; }

        public virtual Snapshot Snapshot { get; set; }
    }
}
