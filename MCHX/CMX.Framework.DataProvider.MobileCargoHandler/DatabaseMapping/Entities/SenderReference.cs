namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SenderReference
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AwbId { get; set; }

        public long? SenderPortId { get; set; }

        [StringLength(2)]
        public string SenderFunctionCode { get; set; }

        [StringLength(2)]
        public string SenderOfficeCode { get; set; }

        [StringLength(15)]
        public string SenderFileReference { get; set; }

        public int? ParticipantIdentifierId { get; set; }

        [StringLength(17)]
        public string ParticipantCode { get; set; }

        public long? ParticipantPortId { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual ParticipantIdentifier ParticipantIdentifier { get; set; }

        public virtual Port Port { get; set; }

        public virtual Port Port1 { get; set; }
    }
}
