namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MessageType
    {
        public int Id { get; set; }

        [Column("MessageType")]
        [Required]
        [StringLength(50)]
        public string MessageType1 { get; set; }

        [Required]
        [StringLength(10)]
        public string MessageVersion { get; set; }

        [StringLength(10)]
        public string PriorityCode { get; set; }
    }
}
