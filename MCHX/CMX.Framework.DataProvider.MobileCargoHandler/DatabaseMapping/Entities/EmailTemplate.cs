namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmailTemplate")]
    public partial class EmailTemplate
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public int EntityTypeId { get; set; }

        public bool? IsDefault { get; set; }

        public int? AccountId { get; set; }

        public long? FromEmailId { get; set; }

        public int? ToDistributionListId { get; set; }

        public virtual Account Account { get; set; }

        public virtual AutoCompleteFromEmail AutoCompleteFromEmail { get; set; }

        public virtual DistributionList DistributionList { get; set; }

        public virtual EntityType EntityType { get; set; }
    }
}
