namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LARCheckListItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LARCheckListItem()
        {
            LARCheckListItems1 = new HashSet<LARCheckListItem>();
            LARCheckListResults = new HashSet<LARCheckListResult>();
        }

        public int ListItemHeaderId { get; set; }

        public int? ListItemNo { get; set; }

        [Required]
        public string ListItemText { get; set; }

        [StringLength(50)]
        public string ValueList { get; set; }

        public int? SortOrder { get; set; }

        public bool IsActive { get; set; }

        public int LARCheckListId { get; set; }

        public DateTime? RecDate { get; set; }

        public int Id { get; set; }

        public int? ParentId { get; set; }

        public virtual LARCheckListItemHeader LARCheckListItemHeader { get; set; }

        public virtual LARCheckList LARCheckList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LARCheckListItem> LARCheckListItems1 { get; set; }

        public virtual LARCheckListItem LARCheckListItem1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LARCheckListResult> LARCheckListResults { get; set; }
    }
}
