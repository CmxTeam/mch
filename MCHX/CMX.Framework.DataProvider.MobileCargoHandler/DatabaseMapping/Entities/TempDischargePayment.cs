namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TempDischargePayment
    {
        public long Id { get; set; }

        public long? TempDischargeChargeId { get; set; }

        public int? PaymentTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal? PaymentAmount { get; set; }

        [StringLength(50)]
        public string Reference { get; set; }

        [StringLength(50)]
        public string VoucherNumber { get; set; }

        [StringLength(50)]
        public string CreditCardNumber { get; set; }

        [StringLength(2)]
        public string CreditCardExpiryMonth { get; set; }

        [StringLength(2)]
        public string CreditCardExpiryYear { get; set; }

        public long? TempDeliveryOrderListId { get; set; }

        public virtual PaymentType PaymentType { get; set; }

        public virtual TempDeliveryOrderList TempDeliveryOrderList { get; set; }

        public virtual TempDischargeCharge TempDischargeCharge { get; set; }
    }
}
