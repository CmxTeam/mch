namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TempDeliveryOrderList")]
    public partial class TempDeliveryOrderList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TempDeliveryOrderList()
        {
            TempDischargeCharges = new HashSet<TempDischargeCharge>();
            TempDischargePayments = new HashSet<TempDischargePayment>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public long? AWBId { get; set; }

        public long? HWBId { get; set; }

        [StringLength(20)]
        public string AWBNumber { get; set; }

        [StringLength(12)]
        public string HWBNumber { get; set; }

        [StringLength(3)]
        public string Origin { get; set; }

        [StringLength(50)]
        public string Shipper { get; set; }

        [StringLength(50)]
        public string Consignee { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(10)]
        public string CustomsStatus { get; set; }

        public int? TotalSLAC { get; set; }

        public int? ReleasedSLAC { get; set; }

        public int? AvailableForDischarge { get; set; }

        public byte[] DOImage { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalChargeAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalPaymentAmount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TempDischargeCharge> TempDischargeCharges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TempDischargePayment> TempDischargePayments { get; set; }
    }
}
