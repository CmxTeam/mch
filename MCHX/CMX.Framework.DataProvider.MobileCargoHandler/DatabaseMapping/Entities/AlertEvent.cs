namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AlertEvent
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AlertId { get; set; }

        public int TaskTypeId { get; set; }

        public virtual Alert Alert { get; set; }

        public virtual TaskType TaskType { get; set; }
    }
}
