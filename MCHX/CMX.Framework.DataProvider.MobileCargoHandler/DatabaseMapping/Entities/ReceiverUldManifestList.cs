namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReceiverUldManifestList")]
    public partial class ReceiverUldManifestList
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public long? FlightManifestId { get; set; }

        [StringLength(36)]
        public string UldNumber { get; set; }

        [StringLength(5)]
        public string CarrierCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ULDId { get; set; }

        public bool? IsBUP { get; set; }

        public int? TotalReceivedCount { get; set; }

        public int? TotalPieces { get; set; }

        public int? ReceivedPieces { get; set; }

        public double? PercentReceived { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusId { get; set; }

        [StringLength(500)]
        public string StatusImagePath { get; set; }
    }
}
