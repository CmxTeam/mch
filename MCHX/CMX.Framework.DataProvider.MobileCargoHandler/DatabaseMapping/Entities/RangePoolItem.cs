namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RangePoolItem
    {
        public long Id { get; set; }

        public int? AccountId { get; set; }

        public int? MOTId { get; set; }

        [StringLength(50)]
        public string Number { get; set; }

        public int? WarehouseId { get; set; }

        public DateTime? RecDate { get; set; }

        public int? RangeTypeId { get; set; }

        public virtual Account Account { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual MOT MOT { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
