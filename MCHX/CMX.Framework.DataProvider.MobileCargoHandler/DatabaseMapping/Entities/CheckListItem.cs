namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CheckListItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CheckListItem()
        {
            CheckListItems1 = new HashSet<CheckListItem>();
            CheckListResults = new HashSet<CheckListResult>();
        }

        public long Id { get; set; }

        public long ListItemHeaderId { get; set; }

        public int? ListItemNo { get; set; }

        [Required]
        public string ListItemText { get; set; }

        [StringLength(50)]
        public string ValueList { get; set; }

        public int? SortOrder { get; set; }

        public bool IsActive { get; set; }

        public long CheckListId { get; set; }

        public DateTime RecDate { get; set; }

        public long? ParentId { get; set; }

        public virtual CheckListItemHeader CheckListItemHeader { get; set; }

        public virtual CheckList CheckList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckListItem> CheckListItems1 { get; set; }

        public virtual CheckListItem CheckListItem1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckListResult> CheckListResults { get; set; }
    }
}
