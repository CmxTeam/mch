namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserStation
    {
        public int Id { get; set; }

        public long? UserId { get; set; }

        public long? PortId { get; set; }

        public bool IsDefault { get; set; }

        public virtual Port Port { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
