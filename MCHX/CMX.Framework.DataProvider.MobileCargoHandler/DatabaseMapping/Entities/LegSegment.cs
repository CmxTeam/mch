namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LegSegment
    {
        public long Id { get; set; }

        [Required]
        [StringLength(5)]
        public string FlightNumber { get; set; }

        public DateTime? ETD { get; set; }

        public int Sequence { get; set; }

        public DateTime RecDate { get; set; }

        public int CarrierId { get; set; }

        public long PortId { get; set; }

        public long? ManifestAWBDetailID { get; set; }

        public long? ULDId { get; set; }

        public long? awbid { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual ManifestAWBDetail ManifestAWBDetail { get; set; }

        public virtual ULD ULD { get; set; }

        public virtual Port Port { get; set; }
    }
}
