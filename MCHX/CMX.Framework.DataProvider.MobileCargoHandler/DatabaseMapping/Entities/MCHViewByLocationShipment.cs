namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MCHViewByLocationShipment
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Expr2 { get; set; }

        public int? WarehouseId { get; set; }

        [StringLength(50)]
        public string WarehouseName { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WarehouseLocationId { get; set; }

        public int? ZoneId { get; set; }

        public long? AWBId { get; set; }

        [StringLength(20)]
        public string AWBSerialNumber { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(3)]
        public string IATACode { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(3)]
        public string Expr1 { get; set; }

        public long? HWBId { get; set; }

        [StringLength(12)]
        public string HWBSerialNumber { get; set; }

        public int? Pieces { get; set; }

        public int? TotalPieces { get; set; }

        public DateTime? Expr3 { get; set; }

        public DateTime? Expr4 { get; set; }

        [StringLength(256)]
        public string Expr5 { get; set; }

        public int? Expr6 { get; set; }

        public string Expr7 { get; set; }

        public string Expr8 { get; set; }

        public bool? IsOSD { get; set; }

        public bool? Expr9 { get; set; }

        public double? Expr10 { get; set; }

        [StringLength(100)]
        public string Expr11 { get; set; }

        [StringLength(100)]
        public string Expr12 { get; set; }

        public double? Expr13 { get; set; }

        [StringLength(100)]
        public string Expr14 { get; set; }

        [StringLength(100)]
        public string Expr15 { get; set; }
    }
}
