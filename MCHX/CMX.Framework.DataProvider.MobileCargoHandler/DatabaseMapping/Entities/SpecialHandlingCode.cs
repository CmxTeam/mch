namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SpecialHandlingCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SpecialHandlingCode()
        {
            SpecialHandlingGroups = new HashSet<SpecialHandlingGroup>();
            SpecialHandlings = new HashSet<SpecialHandling>();
            SpecialHandlingSubGroups = new HashSet<SpecialHandlingSubGroup>();
        }

        public int Id { get; set; }

        [StringLength(3)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool? IsHazmatCode { get; set; }

        public bool? IsPerishable { get; set; }

        public bool? IsTempControlled { get; set; }

        public bool? IsHighValue { get; set; }

        public bool? IsExpedited { get; set; }

        public bool? IsLiveAnimal { get; set; }

        public bool? IsLicenseRequired { get; set; }

        public bool? IsHuman { get; set; }

        public bool? IsOversize { get; set; }

        public bool? IsGovernment { get; set; }

        public bool? IsMedical { get; set; }

        public bool? IsLithium { get; set; }

        public bool? IsDryIce { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingGroup> SpecialHandlingGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandling> SpecialHandlings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingSubGroup> SpecialHandlingSubGroups { get; set; }
    }
}
