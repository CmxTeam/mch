namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Event
    {
        public int Id { get; set; }

        public DateTime RecDate { get; set; }

        public int EntityTypeId { get; set; }

        public long EntityId { get; set; }

        public int? EventCodeId { get; set; }

        [Required]
        [StringLength(200)]
        public string UserName { get; set; }

        [StringLength(250)]
        public string EventReference { get; set; }

        public byte IsTransmitted { get; set; }

        public byte? IsEmailed { get; set; }

        public int? AccountId { get; set; }

        public string ErrorMessage { get; set; }

        public int? ResponseStatusCode { get; set; }

        public string ResponseStatusMessages { get; set; }

        [StringLength(250)]
        public string EventPOD { get; set; }

        [StringLength(250)]
        public string EventLocation { get; set; }

        public virtual Account Account { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual EventCode EventCode { get; set; }
    }
}
