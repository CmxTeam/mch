namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Onhand
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Onhand()
        {
            DGChecklists = new HashSet<DGChecklist>();
            OnhandAttachments = new HashSet<OnhandAttachment>();
            OnhandEntityReferences = new HashSet<OnhandEntityReference>();
            OnhandSnapshots = new HashSet<OnhandSnapshot>();
            OnhandTasks = new HashSet<OnhandTask>();
            OnhandTransactions = new HashSet<OnhandTransaction>();
            PackageGroups = new HashSet<PackageGroup>();
            Packages = new HashSet<Package>();
        }

        public long Id { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(50)]
        public string Number { get; set; }

        public int? AccountId { get; set; }

        public int? ShipperId { get; set; }

        public int? ConsigneeId { get; set; }

        public int? VendorId { get; set; }

        [StringLength(50)]
        public string TruckerPro { get; set; }

        public int? CarrierId { get; set; }

        public DateTime? ReceiveDate { get; set; }

        public DateTime? PickupDate { get; set; }

        public int? ReceivePieces { get; set; }

        public double? ReceiveWeight { get; set; }

        public bool? UnmarkedShipment { get; set; }

        public int? TotalPieces { get; set; }

        public int? TotalStc { get; set; }

        public double? TotalWeight { get; set; }

        public double? TotalVolume { get; set; }

        public int? StatusId { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public int? MotId { get; set; }

        public int? UomWeightId { get; set; }

        public int? ServiceTypeId { get; set; }

        public long? DriverId { get; set; }

        public int? DriverCredType1Id { get; set; }

        public int? DriverCredType2Id { get; set; }

        public bool? MatchingPhoto1 { get; set; }

        public bool? MatchingPhoto2 { get; set; }

        public string SignaturePath { get; set; }

        public int? StageLocationId { get; set; }

        [StringLength(250)]
        public string StatusChangeReason { get; set; }

        public bool? IsHazmat { get; set; }

        public int? WarehouseId { get; set; }

        public bool? IsOSD { get; set; }

        [StringLength(255)]
        public string DescriptionofGoods { get; set; }

        public int? DestinationCountryId { get; set; }

        public int? OriginCountryId { get; set; }

        public byte[] DriverSignature { get; set; }

        [StringLength(100)]
        public string ReceivedBy { get; set; }

        public string MarksAndNumbers { get; set; }

        public virtual Account Account { get; set; }

        public virtual CarrierDriver CarrierDriver { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual CarrierServiceType CarrierServiceType { get; set; }

        public virtual Country Country { get; set; }

        public virtual Country Country1 { get; set; }

        public virtual CredentialType CredentialType { get; set; }

        public virtual CredentialType CredentialType1 { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Customer Customer1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DGChecklist> DGChecklists { get; set; }

        public virtual MOT MOT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandAttachment> OnhandAttachments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandEntityReference> OnhandEntityReferences { get; set; }

        public virtual WarehouseLocation WarehouseLocation { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandSnapshot> OnhandSnapshots { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandTask> OnhandTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnhandTransaction> OnhandTransactions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PackageGroup> PackageGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Package> Packages { get; set; }

        public virtual Port Port { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual Status Status { get; set; }

        public virtual UOM UOM { get; set; }
    }
}
