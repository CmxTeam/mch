namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EntityAttribute
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long AttributeId { get; set; }

        public string Value { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public virtual Attribute Attribute { get; set; }

        public virtual EntityType EntityType { get; set; }
    }
}
