namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SealVerificationsLog")]
    public partial class SealVerificationsLog
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long? TaskId { get; set; }

        public long UserId { get; set; }

        public long SealTypeId { get; set; }

        [StringLength(100)]
        public string SealNumber { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public long? ScreeningFacilityId { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual ScreeningSealType ScreeningSealType { get; set; }

        public virtual Shipper Shipper { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
