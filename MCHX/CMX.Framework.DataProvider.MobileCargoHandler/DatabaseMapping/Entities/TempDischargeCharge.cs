namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TempDischargeCharge
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TempDischargeCharge()
        {
            TempDischargePayments = new HashSet<TempDischargePayment>();
        }

        public long Id { get; set; }

        public long? TempDeliveryOrderListId { get; set; }

        public int? ChargeTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal? ChargeAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal? ChargePaid { get; set; }

        public int? PaymentTypeId { get; set; }

        public virtual ChargeType ChargeType { get; set; }

        public virtual PaymentType PaymentType { get; set; }

        public virtual TempDeliveryOrderList TempDeliveryOrderList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TempDischargePayment> TempDischargePayments { get; set; }
    }
}
