namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ShipperVerificationField
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ShipperVerificationField()
        {
            ShipperVerificationLogs = new HashSet<ShipperVerificationLog>();
            SpecialHandlingVerificationSettings = new HashSet<SpecialHandlingVerificationSetting>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public int FieldTypeId { get; set; }

        [Required]
        [StringLength(250)]
        public string DisplayName { get; set; }

        public virtual FieldType FieldType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperVerificationLog> ShipperVerificationLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingVerificationSetting> SpecialHandlingVerificationSettings { get; set; }
    }
}
