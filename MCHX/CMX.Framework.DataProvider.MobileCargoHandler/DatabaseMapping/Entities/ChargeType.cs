namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ChargeType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ChargeType()
        {
            DischargeCharges = new HashSet<DischargeCharge>();
            TempDischargeCharges = new HashSet<TempDischargeCharge>();
        }

        public int Id { get; set; }

        [Column("ChargeType")]
        [StringLength(150)]
        public string ChargeType1 { get; set; }

        [Column(TypeName = "money")]
        public decimal? DefaultChargeAmount { get; set; }

        public bool? IsPerUnit { get; set; }

        public bool? IsClockStartAtArrival { get; set; }

        public int? ClockStartTime { get; set; }

        public int? FreeTimeDuration { get; set; }

        [Column(TypeName = "money")]
        public decimal? MinChargeAmount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeCharge> DischargeCharges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TempDischargeCharge> TempDischargeCharges { get; set; }
    }
}
