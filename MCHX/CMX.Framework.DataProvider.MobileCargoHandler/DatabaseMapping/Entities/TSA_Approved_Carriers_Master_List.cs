namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSA_Approved_Carriers_Master_List
    {
        public long Id { get; set; }

        [StringLength(5)]
        public string IATACode { get; set; }

        [StringLength(50)]
        public string CarrierDesignator { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [StringLength(250)]
        public string DBA { get; set; }

        public bool IsActive { get; set; }

        public DateTime LastUpdated { get; set; }

        public long UpdatedByUserId { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
