namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Dimension
    {
        public int Id { get; set; }

        public long? AWBId { get; set; }

        public long? ULDId { get; set; }

        public int? Pieces { get; set; }

        public double? Weight { get; set; }

        public double? Length { get; set; }

        public double? Width { get; set; }

        public double? Height { get; set; }

        public int? WeightUOMId { get; set; }

        public int? DimUOMId { get; set; }

        public DateTime? RecDate { get; set; }

        public bool? IsScreened { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual ULD ULD { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }
    }
}
