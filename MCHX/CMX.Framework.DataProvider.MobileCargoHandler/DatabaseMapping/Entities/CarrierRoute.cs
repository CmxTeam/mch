namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CarrierRoute
    {
        public int Id { get; set; }

        public int? CarrierId { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public long? ViaId { get; set; }

        [StringLength(50)]
        public string FlightNumber { get; set; }

        public TimeSpan? DepartureTime { get; set; }

        public TimeSpan? ArrivalTime { get; set; }

        public int? Offset { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual Port Port { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual Port Port2 { get; set; }
    }
}
