namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OnhandTransaction
    {
        public long Id { get; set; }

        public long? OnhandId { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Reference { get; set; }

        public string Description { get; set; }

        public int? TaskTypeId { get; set; }

        public int? TransactionActionId { get; set; }

        public long? TaskId { get; set; }

        public long? PackageId { get; set; }

        public virtual Onhand Onhand { get; set; }

        public virtual Package Package { get; set; }

        public virtual Status Status { get; set; }

        public virtual TaskType TaskType { get; set; }

        public virtual TransactionAction TransactionAction { get; set; }
    }
}
