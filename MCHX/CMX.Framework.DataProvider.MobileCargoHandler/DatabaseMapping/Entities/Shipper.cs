namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Shipper
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Shipper()
        {
            CargoAcceptScreeningLogs = new HashSet<CargoAcceptScreeningLog>();
            ScreeningTransactions = new HashSet<ScreeningTransaction>();
            SealVerificationsLogs = new HashSet<SealVerificationsLog>();
        }

        public long Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string TSAApprovalNumber { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(255)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(30)]
        public string State { get; set; }

        [StringLength(13)]
        public string ZipCode { get; set; }

        [StringLength(60)]
        public string Country { get; set; }

        public DateTime? ExpirationDate { get; set; }

        [StringLength(50)]
        public string IAC_Agent_Number { get; set; }

        public DateTime? IACExpirationDate { get; set; }

        public DateTime? SuspensionDate { get; set; }

        [StringLength(100)]
        public string CCSFStatus { get; set; }

        [StringLength(100)]
        public string CCSFStatusUpdate { get; set; }

        [StringLength(255)]
        public string CreatedBy { get; set; }

        public DateTimeOffset? Created { get; set; }

        [StringLength(255)]
        public string ModifiedBy { get; set; }

        public DateTimeOffset? Modified { get; set; }

        public DateTime RecDate { get; set; }

        public long? ShipperTypeId { get; set; }

        public int? CustomerId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CargoAcceptScreeningLog> CargoAcceptScreeningLogs { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningTransaction> ScreeningTransactions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SealVerificationsLog> SealVerificationsLogs { get; set; }

        public virtual ShipperType ShipperType { get; set; }
    }
}
