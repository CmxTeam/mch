namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HWBs_Packages
    {
        public long Id { get; set; }

        public long HWBId { get; set; }

        public long PackageId { get; set; }

        public DateTime RecDate { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual Package Package { get; set; }
    }
}
