namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InventoryView")]
    public partial class InventoryView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string Type { get; set; }

        [StringLength(36)]
        public string Reference { get; set; }

        [StringLength(12)]
        public string Reference1 { get; set; }

        public int? Count { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(20)]
        public string LocationType { get; set; }

        [StringLength(256)]
        public string UserName { get; set; }

        public DateTime? Timestamp { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WarehouseId { get; set; }

        public long? PortId { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EntityId1 { get; set; }
    }
}
