namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AWBs_HWBs
    {
        public int Id { get; set; }

        public long? AWBId { get; set; }

        public long? HWBId { get; set; }

        public DateTime? RecDate { get; set; }

        public virtual AWB AWB { get; set; }

        public virtual HWB HWB { get; set; }
    }
}
