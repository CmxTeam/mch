namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskSnapshotReference
    {
        public long Id { get; set; }

        public DateTime? RecDate { get; set; }

        public long? TaskId { get; set; }

        [StringLength(100)]
        public string Reference { get; set; }
    }
}
