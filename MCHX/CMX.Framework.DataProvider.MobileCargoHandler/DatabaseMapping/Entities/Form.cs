namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Form
    {
        public int Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int TypeId { get; set; }

        public int? AccountId { get; set; }

        public int FormTypeId { get; set; }

        public bool? AutoPrint { get; set; }

        public int? NumOfCopies { get; set; }

        public virtual Account Account { get; set; }

        public virtual FormType FormType { get; set; }

        public virtual PrinterType PrinterType { get; set; }
    }
}
