namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EntityReference
    {
        public long Id { get; set; }

        public int? EntityTypeId { get; set; }

        public long? EntityId { get; set; }

        public int? ReferenceTypeId { get; set; }

        public string Value { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual ReferenceType ReferenceType { get; set; }
    }
}
