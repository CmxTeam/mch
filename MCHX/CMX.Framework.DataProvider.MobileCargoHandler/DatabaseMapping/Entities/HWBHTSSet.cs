namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HWBHTSSet")]
    public partial class HWBHTSSet
    {
        public int Id { get; set; }

        public DateTime? RecDate { get; set; }

        public long? HWBId { get; set; }

        public int? ImportExportCodeId { get; set; }

        public virtual HWB HWB { get; set; }

        public virtual ImportExportCode ImportExportCode { get; set; }
    }
}
