namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DriverSecurityLog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DriverSecurityLog()
        {
            Discharges = new HashSet<Discharge>();
            ShipperManifests = new HashSet<ShipperManifest>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public long CarrierDriverId { get; set; }

        public long? UserId { get; set; }

        public int? PrimaryIDTypeId { get; set; }

        public int? SecondaryIdTypeId { get; set; }

        public bool? PrimaryPhotoIdMatch { get; set; }

        public bool? SecondaryPhotoIdMatch { get; set; }

        public int? TruckingCompanyId { get; set; }

        public string DriverPhoto { get; set; }

        public string IDPhoto { get; set; }

        public string DriverThumbnail { get; set; }

        public virtual CarrierDriver CarrierDriver { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual CredentialType CredentialType { get; set; }

        public virtual CredentialType CredentialType1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Discharge> Discharges { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperManifest> ShipperManifests { get; set; }
    }
}
