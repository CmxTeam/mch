namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ImportExportCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ImportExportCode()
        {
            HWBHTSSets = new HashSet<HWBHTSSet>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string ShortDescription { get; set; }

        [StringLength(255)]
        public string LongDescription { get; set; }

        [StringLength(255)]
        public string UOM { get; set; }

        [StringLength(255)]
        public string UnitOfQty { get; set; }

        [StringLength(255)]
        public string Sitc { get; set; }

        [StringLength(255)]
        public string EndUseClassification { get; set; }

        [StringLength(255)]
        public string UsdaProductCode { get; set; }

        [StringLength(255)]
        public string NaicsClassification { get; set; }

        [StringLength(255)]
        public string HiTechClassification { get; set; }

        [StringLength(50)]
        public string CodeType { get; set; }

        public bool UsedForAes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HWBHTSSet> HWBHTSSets { get; set; }
    }
}
