namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReceiverFlightView")]
    public partial class ReceiverFlightView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UnloadingId { get; set; }

        public long? FlightManifestId { get; set; }

        [StringLength(60)]
        public string OrgFlightNumber { get; set; }

        public DateTime? ETA { get; set; }

        [StringLength(4000)]
        public string ArrivalTime { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string Origin { get; set; }

        public int? TotalULDs { get; set; }

        public int? TotalAWBs { get; set; }

        public int? TotalPieces { get; set; }

        [StringLength(50)]
        public string CarrierName { get; set; }

        [StringLength(56)]
        public string FlightNumber { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int? RecoveredULDs { get; set; }

        public int? ReceivedPieces { get; set; }

        [StringLength(50)]
        public string RecoveredBy { get; set; }

        [StringLength(4000)]
        public string RecoveredOn { get; set; }

        public bool? IsNilCargo { get; set; }

        public long? PortId { get; set; }

        public bool? IsComplete { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusId { get; set; }

        [StringLength(50)]
        public string StatusName { get; set; }

        [StringLength(500)]
        public string CarrierLogoPath { get; set; }

        [StringLength(500)]
        public string StatusImagePath { get; set; }

        [StringLength(50)]
        public string RecoverLocation { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(256)]
        public string StagedBy { get; set; }

        [StringLength(4000)]
        public string StagedOn { get; set; }
    }
}
