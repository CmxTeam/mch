namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Remark
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Column("Remark")]
        [Required]
        public string Remark1 { get; set; }

        [Required]
        [StringLength(50)]
        public string RemarkType { get; set; }

        public int WarehouseId { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
