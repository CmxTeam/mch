namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CredentialType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CredentialType()
        {
            DriverSecurityLogs = new HashSet<DriverSecurityLog>();
            DriverSecurityLogs1 = new HashSet<DriverSecurityLog>();
            ShipperManifests = new HashSet<ShipperManifest>();
            Onhands = new HashSet<Onhand>();
            Onhands1 = new HashSet<Onhand>();
        }

        public int Id { get; set; }

        public DateTime? RecDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public bool IsGovernmentApproved { get; set; }

        public long? ShipperTypeId { get; set; }

        public virtual ShipperType ShipperType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverSecurityLog> DriverSecurityLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverSecurityLog> DriverSecurityLogs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperManifest> ShipperManifests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Onhand> Onhands { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Onhand> Onhands1 { get; set; }
    }
}
