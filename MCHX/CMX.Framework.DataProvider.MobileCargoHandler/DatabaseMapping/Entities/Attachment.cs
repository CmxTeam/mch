namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Attachment
    {
        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(255)]
        public string FileType { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public long EntityID { get; set; }

        public int EntityTypeId { get; set; }

        public long? UserId { get; set; }

        public string AttachmentImage { get; set; }

        public long? DocumentTypeId { get; set; }

        public virtual EntityType EntityType { get; set; }

        public virtual ShipperVerificationDoc ShipperVerificationDoc { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
