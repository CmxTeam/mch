namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PortAliases")]
    public partial class PortAlias
    {
        public long Id { get; set; }

        public long PortId { get; set; }

        [Required]
        [StringLength(3)]
        public string Alias { get; set; }

        public virtual Port Port { get; set; }
    }
}
