namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AccountCarrier
    {
        public int Id { get; set; }

        public int? AccountId { get; set; }

        public int? CarrierId { get; set; }

        [StringLength(50)]
        public string AccountNo { get; set; }

        [StringLength(50)]
        public string AccountLogin { get; set; }

        [StringLength(50)]
        public string AccountPassword { get; set; }

        [StringLength(100)]
        public string AccessKey { get; set; }

        public bool? IsActive { get; set; }

        [Required]
        [StringLength(20)]
        public string AccountCIMPMailbox { get; set; }

        public virtual Account Account { get; set; }

        public virtual Carrier Carrier { get; set; }
    }
}
