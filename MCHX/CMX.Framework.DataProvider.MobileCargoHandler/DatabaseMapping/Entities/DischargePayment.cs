namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DischargePayment
    {
        public long? DischargeChargeId { get; set; }

        public int? PaymentTypeId { get; set; }

        public double? PaymentAmount { get; set; }

        [StringLength(50)]
        public string Reference { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public virtual DischargeCharge DischargeCharge { get; set; }

        public virtual PaymentType PaymentType { get; set; }
    }
}
