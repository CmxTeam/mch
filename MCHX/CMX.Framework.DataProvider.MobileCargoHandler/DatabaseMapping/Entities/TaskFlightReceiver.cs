namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskFlightReceiver")]
    public partial class TaskFlightReceiver
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long FlightManifestId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long TaskId { get; set; }

        public double? PercentOverall { get; set; }

        [StringLength(5)]
        public string CarrierCode { get; set; }

        [StringLength(50)]
        public string FlightNumber { get; set; }

        public DateTime? ETA { get; set; }

        [StringLength(50)]
        public string RecoveredBy { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? TotalULDs { get; set; }

        public int? TotalAWBs { get; set; }

        public int? TotalPieces { get; set; }

        public bool? IsOSD { get; set; }
    }
}
