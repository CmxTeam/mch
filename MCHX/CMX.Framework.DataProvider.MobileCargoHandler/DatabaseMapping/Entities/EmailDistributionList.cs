namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmailDistributionList")]
    public partial class EmailDistributionList
    {
        public int Id { get; set; }

        public int AutoCompleteEmailId { get; set; }

        public int DistributionListId { get; set; }

        public virtual AutoCompleteEmail AutoCompleteEmail { get; set; }

        public virtual DistributionList DistributionList { get; set; }
    }
}
