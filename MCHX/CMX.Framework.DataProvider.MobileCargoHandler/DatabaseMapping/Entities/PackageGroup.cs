namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PackageGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PackageGroup()
        {
            Packages = new HashSet<Package>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public int NumberOfPieces { get; set; }

        public double? Length { get; set; }

        public double? Width { get; set; }

        public double? Height { get; set; }

        public double? Weight { get; set; }

        public int PackageTypeId { get; set; }

        public int? DimUOMId { get; set; }

        public int? WeightUOMId { get; set; }

        public bool? IsHazmat { get; set; }

        public bool? IsOSD { get; set; }

        public long? OnhandId { get; set; }

        public int? STC { get; set; }

        [StringLength(100)]
        public string RefNumber { get; set; }

        public virtual Onhand Onhand { get; set; }

        public virtual UOM UOM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Package> Packages { get; set; }

        public virtual PackageType PackageType { get; set; }

        public virtual UOM UOM1 { get; set; }
    }
}
