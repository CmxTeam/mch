namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EntityBase")]
    public partial class EntityBase
    {
        public long Id { get; set; }

        public virtual DeleteMe DeleteMe { get; set; }
    }
}
