namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AWB
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AWB()
        {
            AccountingInformations = new HashSet<AccountingInformation>();
            AWBs_HWBs = new HashSet<AWBs_HWBs>();
            AWBs_ULDs = new HashSet<AWBs_ULDs>();
            ChargeDeclarations = new HashSet<ChargeDeclaration>();
            ChargeSummaries = new HashSet<ChargeSummary>();
            CustomsNotifications = new HashSet<CustomsNotification>();
            Dimensions = new HashSet<Dimension>();
            DischargeDetails = new HashSet<DischargeDetail>();
            DischargeShipments = new HashSet<DischargeShipment>();
            Dispositions = new HashSet<Disposition>();
            FlightNotocDGDetails = new HashSet<FlightNotocDGDetail>();
            FlightNotocOtherCargoDetails = new HashSet<FlightNotocOtherCargoDetail>();
            ForkliftDetails = new HashSet<ForkliftDetail>();
            HostPlus_CIMPOutboundQueue = new HashSet<HostPlus_CIMPOutboundQueue>();
            LoadingPlans = new HashSet<LoadingPlan>();
            ManifestAWBDetails = new HashSet<ManifestAWBDetail>();
            ManifestDetails = new HashSet<ManifestDetail>();
            ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            OCIs = new HashSet<OCI>();
            OSDs = new HashSet<OSD>();
            OtherCharges = new HashSet<OtherCharge>();
            Overpacks = new HashSet<Overpack>();
            RateDescriptions = new HashSet<RateDescription>();
            ScreeningVerificationLogs = new HashSet<ScreeningVerificationLog>();
            SenderReferences = new HashSet<SenderReference>();
            ShipperManifests_AWBs = new HashSet<ShipperManifests_AWBs>();
            ShipperVerificationLogs = new HashSet<ShipperVerificationLog>();
            SpecialHandlings = new HashSet<SpecialHandling>();
            SpecialHandlingLogs = new HashSet<SpecialHandlingLog>();
            LegSegments = new HashSet<LegSegment>();
        }

        public long Id { get; set; }

        public DateTime RecDate { get; set; }

        public int? CarrierId { get; set; }

        [Required]
        [StringLength(20)]
        public string AWBSerialNumber { get; set; }

        public int Pieces { get; set; }

        public int SLAC { get; set; }

        public double Weight { get; set; }

        public int? WeightUOMId { get; set; }

        public double Volume { get; set; }

        public int? VolumeUOMId { get; set; }

        public int ReceivedPieces { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PercentReceived { get; set; }

        public int ForkliftPieces { get; set; }

        public int? StatusId { get; set; }

        public DateTime? StatusTimestamp { get; set; }

        public string DescriptionOfGoods { get; set; }

        [StringLength(50)]
        public string LoadType { get; set; }

        public int? ShipperId { get; set; }

        public int? ConsigneeId { get; set; }

        public bool? DensityIndicator { get; set; }

        [StringLength(65)]
        public string OtherServiceInfo1 { get; set; }

        [StringLength(65)]
        public string OtherServiceInfo2 { get; set; }

        [StringLength(2)]
        public string CustomsOrigin { get; set; }

        [StringLength(15)]
        public string CustomsReference { get; set; }

        [StringLength(65)]
        public string SpecialServiceRequest1 { get; set; }

        [StringLength(65)]
        public string SpecialServiceRequest2 { get; set; }

        [StringLength(65)]
        public string SpecialServiceRequest3 { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public int? MovementPriorityCodeId { get; set; }

        public int? AgentId { get; set; }

        public int? IsTransfer { get; set; }

        public int? AccountId { get; set; }

        public bool? IsOSD { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ExecutedOnDate { get; set; }

        [StringLength(100)]
        public string ExecutedAtPlace { get; set; }

        [StringLength(100)]
        public string ExecutedBy { get; set; }

        [StringLength(100)]
        public string ShipperCertification { get; set; }

        public int? AircraftTypeId { get; set; }

        public int? AirwaybillType { get; set; }

        public int? ServiceCodeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountingInformation> AccountingInformations { get; set; }

        public virtual Account Account { get; set; }

        public virtual Agent Agent { get; set; }

        public virtual AircraftType AircraftType { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Customer Customer1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AWBs_HWBs> AWBs_HWBs { get; set; }

        public virtual MovementPriorityCode MovementPriorityCode { get; set; }

        public virtual Port Port { get; set; }

        public virtual Port Port1 { get; set; }

        public virtual ServiceCode ServiceCode { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AWBs_ULDs> AWBs_ULDs { get; set; }

        public virtual UOM UOM { get; set; }

        public virtual UOM UOM1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChargeDeclaration> ChargeDeclarations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChargeSummary> ChargeSummaries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomsNotification> CustomsNotifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Dimension> Dimensions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeDetail> DischargeDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DischargeShipment> DischargeShipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disposition> Dispositions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocDGDetail> FlightNotocDGDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightNotocOtherCargoDetail> FlightNotocOtherCargoDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForkliftDetail> ForkliftDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HostPlus_CIMPOutboundQueue> HostPlus_CIMPOutboundQueue { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoadingPlan> LoadingPlans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestAWBDetail> ManifestAWBDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OCI> OCIs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSD> OSDs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OtherCharge> OtherCharges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Overpack> Overpacks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RateDescription> RateDescriptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScreeningVerificationLog> ScreeningVerificationLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SenderReference> SenderReferences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperManifests_AWBs> ShipperManifests_AWBs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShipperVerificationLog> ShipperVerificationLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandling> SpecialHandlings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialHandlingLog> SpecialHandlingLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LegSegment> LegSegments { get; set; }
    }
}
