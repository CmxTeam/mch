namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.UldDetails")]
    public partial class UldDetail
    {
        public int UldDetailID { get; set; }

        public int UldID { get; set; }

        public int? ItemNo { get; set; }

        [StringLength(35)]
        public string Description { get; set; }

        public int? HazardousMaterialIndicator { get; set; }

        [StringLength(35)]
        public string Marks { get; set; }

        [StringLength(10)]
        public string Hts { get; set; }

        [StringLength(2)]
        public string CustomsNoteContentCode { get; set; }

        public virtual Uld Uld { get; set; }
    }
}
