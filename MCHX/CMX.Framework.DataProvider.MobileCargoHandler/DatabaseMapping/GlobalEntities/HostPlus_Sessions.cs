namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_Sessions
    {
        [Key]
        public int RecId { get; set; }

        public DateTime RecDate { get; set; }

        [StringLength(20)]
        public string UserId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public int SessionTypeId { get; set; }

        public bool IsActive { get; set; }

        public string Server { get; set; }
    }
}
