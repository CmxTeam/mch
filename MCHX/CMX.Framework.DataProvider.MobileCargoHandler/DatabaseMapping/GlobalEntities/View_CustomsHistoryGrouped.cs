namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class View_CustomsHistoryGrouped
    {
        public long? RowNumber { get; set; }

        [StringLength(11)]
        public string Master { get; set; }

        [StringLength(12)]
        public string House { get; set; }

        [StringLength(1)]
        public string PartialIdCode { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusID { get; set; }

        [StringLength(25)]
        public string CustomsCode { get; set; }

        [StringLength(2)]
        public string StatusCode { get; set; }

        [StringLength(2)]
        public string Code { get; set; }

        [StringLength(35)]
        public string CodeDescription { get; set; }

        public long? QuantityAffected { get; set; }

        [StringLength(15)]
        public string EntryNo { get; set; }

        [StringLength(1)]
        public string MessageType { get; set; }
    }
}
