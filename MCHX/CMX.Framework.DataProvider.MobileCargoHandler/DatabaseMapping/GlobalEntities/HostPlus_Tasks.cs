namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_Tasks
    {
        [Key]
        public int RecId { get; set; }

        public DateTime RecDate { get; set; }

        public int SessionTypeId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [StringLength(50)]
        public string Icon { get; set; }

        public int? RetriesOnFail { get; set; }
    }
}
