namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.CustomsActions")]
    public partial class CustomsAction
    {
        public int CustomsActionID { get; set; }

        public int DetailID { get; set; }

        public string MessageId { get; set; }

        [StringLength(11)]
        public string MessageType { get; set; }

        [StringLength(6)]
        public string MessageAction { get; set; }

        [StringLength(2)]
        public string StatusCode { get; set; }

        [StringLength(2)]
        public string ReasonCode { get; set; }

        [StringLength(20)]
        public string Explanation { get; set; }

        public DateTime? LiftoffDateTime { get; set; }

        [StringLength(1)]
        public string SendArrivalData { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
