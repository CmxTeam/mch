namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_SessionTasks
    {
        [Key]
        public int RecID { get; set; }

        public DateTime RecDate { get; set; }

        [StringLength(20)]
        public string UserId { get; set; }

        public int SessionId { get; set; }

        public int TaskId { get; set; }

        public DateTime LastDate { get; set; }

        public DateTime NextDate { get; set; }

        public decimal Timer { get; set; }

        [Required]
        [StringLength(50)]
        public string TimerWeekDays { get; set; }

        [Required]
        [StringLength(50)]
        public string TimerType { get; set; }

        public bool IsActive { get; set; }

        public string Status { get; set; }

        [Column(TypeName = "text")]
        public string Trace { get; set; }

        public bool IsTraceable { get; set; }

        public string Progress { get; set; }

        public string Error { get; set; }

        public int? LocationId { get; set; }

        public string Description { get; set; }

        public int? Retries { get; set; }
    }
}
