namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Waybills")]
    public partial class Waybill
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Waybill()
        {
            Details = new HashSet<Detail>();
        }

        public int WaybillID { get; set; }

        public int ManifestID { get; set; }

        [StringLength(10)]
        public string Company { get; set; }

        [StringLength(10)]
        public string Office { get; set; }

        [StringLength(11)]
        public string Master { get; set; }

        [StringLength(1)]
        public string MasterIndicator { get; set; }

        [StringLength(4)]
        public string Client { get; set; }

        [StringLength(3)]
        public string DepartureAirport { get; set; }

        [StringLength(3)]
        public string ArrivalAirport { get; set; }

        [StringLength(4)]
        public string ArrivalTerminal { get; set; }

        public DateTime? ArrivalDate { get; set; }

        [StringLength(3)]
        public string Carrier { get; set; }

        [StringLength(5)]
        public string FlightNo { get; set; }

        [StringLength(10)]
        public string Agentcode { get; set; }

        [StringLength(4)]
        public string AgentIataCasscode { get; set; }

        [StringLength(35)]
        public string AgentName { get; set; }

        [StringLength(35)]
        public string AgentPlace { get; set; }

        [StringLength(20)]
        public string SignatoryCarrierName { get; set; }

        [StringLength(17)]
        public string SignatoryCarrierLocation { get; set; }

        public DateTime? SignatoryCarrierDateTime { get; set; }

        [StringLength(20)]
        public string SignatoryConsignorName { get; set; }

        [StringLength(3)]
        public string CargoMovementType { get; set; }

        [StringLength(2)]
        public string CargoMovementCountryCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detail> Details { get; set; }

        public virtual Manifest Manifest { get; set; }
    }
}
