namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Addresses")]
    public partial class Address
    {
        public int AddressID { get; set; }

        public int DetailID { get; set; }

        [StringLength(2)]
        public string Type { get; set; }

        [StringLength(6)]
        public string AccountNo { get; set; }

        [StringLength(35)]
        public string EntityIdentifier { get; set; }

        [StringLength(12)]
        public string MidOrIrsNo { get; set; }

        [StringLength(35)]
        public string Name { get; set; }

        [StringLength(35)]
        public string Line1 { get; set; }

        [StringLength(35)]
        public string Line2 { get; set; }

        [StringLength(35)]
        public string Line3 { get; set; }

        [StringLength(35)]
        public string City { get; set; }

        public string SubCountry { get; set; }

        [StringLength(5)]
        public string CityUnlocId { get; set; }

        [StringLength(2)]
        public string Country { get; set; }

        [StringLength(35)]
        public string CountryName { get; set; }

        [StringLength(10)]
        public string PostalCode { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(20)]
        public string Contact1 { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(5)]
        public string PhoneExt1 { get; set; }

        [StringLength(20)]
        public string Cellular1 { get; set; }

        [StringLength(70)]
        public string Email1 { get; set; }

        [StringLength(20)]
        public string Contact2 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(5)]
        public string PhoneExt2 { get; set; }

        [StringLength(20)]
        public string Cellular2 { get; set; }

        [StringLength(70)]
        public string Email2 { get; set; }

        [StringLength(35)]
        public string Salutation { get; set; }

        [StringLength(16)]
        public string BondType { get; set; }

        [StringLength(3)]
        public string SuretyCode { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
