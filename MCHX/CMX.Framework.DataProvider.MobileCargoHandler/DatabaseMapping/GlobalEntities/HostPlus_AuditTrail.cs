namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_AuditTrail
    {
        [Key]
        public long RecId { get; set; }

        public DateTime RecDate { get; set; }

        public long? TransactionId { get; set; }

        [StringLength(20)]
        public string UserId { get; set; }

        [StringLength(50)]
        public string DBUser { get; set; }

        [StringLength(50)]
        public string AppName { get; set; }

        [StringLength(1)]
        public string Operation { get; set; }

        [StringLength(50)]
        public string TableName { get; set; }

        [Column(TypeName = "xml")]
        public string Before { get; set; }

        [Column(TypeName = "xml")]
        public string After { get; set; }
    }
}
