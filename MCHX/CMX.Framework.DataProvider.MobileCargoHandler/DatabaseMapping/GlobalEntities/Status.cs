namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Statuses")]
    public partial class Status
    {
        public int StatusID { get; set; }

        public int DetailID { get; set; }

        [StringLength(25)]
        public string CustomsCode { get; set; }

        [StringLength(2)]
        public string StatusCode { get; set; }

        [StringLength(2)]
        public string Code { get; set; }

        [StringLength(35)]
        public string CodeDescription { get; set; }

        [StringLength(2)]
        public string ErrorType { get; set; }

        public long? QuantityAffected { get; set; }

        public DateTime? TransactionDateTime { get; set; }

        public DateTime? CustomsDateTime { get; set; }

        [StringLength(150)]
        public string Explanation { get; set; }

        [StringLength(2)]
        public string EntryType { get; set; }

        [StringLength(15)]
        public string EntryNo { get; set; }

        public DateTime? MessageFlightDate { get; set; }

        [StringLength(5)]
        public string MessageFlightNo { get; set; }

        public string Text { get; set; }

        [StringLength(1)]
        public string MessageType { get; set; }

        [StringLength(4)]
        public string OriginalMessageType { get; set; }

        [StringLength(8)]
        public string ResponseType { get; set; }

        [StringLength(15)]
        public string Category { get; set; }

        [StringLength(3)]
        public string ReasonCode { get; set; }

        [StringLength(255)]
        public string ReasonText { get; set; }

        [StringLength(255)]
        public string ActualValue { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
