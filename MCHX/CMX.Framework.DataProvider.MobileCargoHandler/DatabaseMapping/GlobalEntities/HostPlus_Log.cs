namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_Log
    {
        [Key]
        public int RecId { get; set; }

        public DateTime? RecDate { get; set; }

        [Column(TypeName = "xml")]
        public string Description { get; set; }

        public string StackTrace { get; set; }
    }
}
