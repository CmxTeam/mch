namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_Parameters
    {
        [Key]
        public long RecId { get; set; }

        public DateTime RecDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string DefaultValue { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public bool? AllowBlank { get; set; }

        [StringLength(50)]
        public string DataType { get; set; }

        public long? TaskId { get; set; }

        public long? SessionTypeId { get; set; }

        public string DataEnumeration { get; set; }
    }
}
