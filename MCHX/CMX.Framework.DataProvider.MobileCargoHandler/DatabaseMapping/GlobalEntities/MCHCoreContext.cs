namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MCHCoreContext : DbContext
    {
        public MCHCoreContext()
            : base("name=MCHCoreContext")
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Charge> Charges { get; set; }
        public virtual DbSet<CustomsAction> CustomsActions { get; set; }
        public virtual DbSet<Detail> Details { get; set; }
        public virtual DbSet<Inbond> Inbonds { get; set; }
        public virtual DbSet<Manifest> Manifests { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<RatingDetail> RatingDetails { get; set; }
        public virtual DbSet<Routing> Routings { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<UldDetail> UldDetails { get; set; }
        public virtual DbSet<Uld> Ulds { get; set; }
        public virtual DbSet<Waybill> Waybills { get; set; }
        public virtual DbSet<CBPMessageLog> CBPMessageLogs { get; set; }
        public virtual DbSet<CIMPMessageLog> CIMPMessageLogs { get; set; }
        public virtual DbSet<HostPlus_AuditTrail> HostPlus_AuditTrail { get; set; }
        public virtual DbSet<HostPlus_AutoEmail> HostPlus_AutoEmail { get; set; }
        public virtual DbSet<HostPlus_AutoEmail_Reports> HostPlus_AutoEmail_Reports { get; set; }
        public virtual DbSet<HostPlus_EmailAttachments> HostPlus_EmailAttachments { get; set; }
        public virtual DbSet<HostPlus_Emails> HostPlus_Emails { get; set; }
        public virtual DbSet<HostPlus_Locations> HostPlus_Locations { get; set; }
        public virtual DbSet<HostPlus_Log> HostPlus_Log { get; set; }
        public virtual DbSet<HostPlus_Parameters> HostPlus_Parameters { get; set; }
        public virtual DbSet<HostPlus_SessionParameters> HostPlus_SessionParameters { get; set; }
        public virtual DbSet<HostPlus_Sessions> HostPlus_Sessions { get; set; }
        public virtual DbSet<HostPlus_SessionTaskParameters> HostPlus_SessionTaskParameters { get; set; }
        public virtual DbSet<HostPlus_SessionTasks> HostPlus_SessionTasks { get; set; }
        public virtual DbSet<HostPlus_SessionTypes> HostPlus_SessionTypes { get; set; }
        public virtual DbSet<HostPlus_Tasks> HostPlus_Tasks { get; set; }
        public virtual DbSet<HostPlus_TimestampsCodes> HostPlus_TimestampsCodes { get; set; }        
        public virtual DbSet<View_CustomsHistoryGrouped> View_CustomsHistoryGrouped { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Detail>()
                .Property(e => e.CurrencyExchangeRate)
                .HasPrecision(19, 6);

            modelBuilder.Entity<Uld>()
                .Property(e => e.Qty)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Uld>()
                .Property(e => e.Wgt)
                .HasPrecision(19, 4);

            modelBuilder.Entity<HostPlus_AuditTrail>()
                .Property(e => e.Operation)
                .IsFixedLength();

            modelBuilder.Entity<HostPlus_Emails>()
                .HasMany(e => e.HostPlus_EmailAttachments)
                .WithRequired(e => e.HostPlus_Emails)
                .HasForeignKey(e => e.EmailId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HostPlus_SessionTasks>()
                .Property(e => e.Trace)
                .IsUnicode(false);
        }
    }
}
