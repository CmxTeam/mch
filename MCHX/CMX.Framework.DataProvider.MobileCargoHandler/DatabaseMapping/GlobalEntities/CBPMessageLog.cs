namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CBPMessageLog
    {
        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        [StringLength(100)]
        public string FileName { get; set; }

        [StringLength(20)]
        public string MessageType { get; set; }

        [StringLength(100)]
        public string MailBoxName { get; set; }

        public string RAWMessage { get; set; }

        public bool ProcessingStatus { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime? ProcessTime { get; set; }

        [StringLength(100)]
        public string SenderMailBox { get; set; }

        [StringLength(10)]
        public string Direction { get; set; }

        [StringLength(10)]
        public string Priority { get; set; }
    }
}
