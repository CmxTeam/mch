namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_EmailAttachments
    {
        [Key]
        public long RecId { get; set; }

        public long EmailId { get; set; }

        [Required]
        public string FileName { get; set; }

        public virtual HostPlus_Emails HostPlus_Emails { get; set; }
    }
}
