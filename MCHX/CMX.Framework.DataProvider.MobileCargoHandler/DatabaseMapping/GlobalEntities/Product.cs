namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Products")]
    public partial class Product
    {
        public int ProductID { get; set; }

        public int DetailID { get; set; }

        public int? DescriptionLineNo { get; set; }

        public long? Hts { get; set; }

        [StringLength(35)]
        public string Description { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
