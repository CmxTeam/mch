namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_SessionTaskParameters
    {
        [Key]
        public long RecId { get; set; }

        public DateTime RecDate { get; set; }

        [StringLength(20)]
        public string UserId { get; set; }

        public long SessionTaskId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(4000)]
        public string Value { get; set; }

        public long? ParameterId { get; set; }
    }
}
