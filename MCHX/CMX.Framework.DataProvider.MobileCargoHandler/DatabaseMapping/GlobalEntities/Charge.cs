namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Charges")]
    public partial class Charge
    {
        public int ChargeID { get; set; }

        public int DetailID { get; set; }

        [StringLength(1)]
        public string ChargesIndicator { get; set; }

        [StringLength(2)]
        public string ChargeCode { get; set; }

        [StringLength(1)]
        public string EntitlementCode { get; set; }

        public double? ChargeAmount { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
