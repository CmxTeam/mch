namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_Emails
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HostPlus_Emails()
        {
            HostPlus_EmailAttachments = new HashSet<HostPlus_EmailAttachments>();
        }

        [Key]
        public long RecId { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(100)]
        public string UserId { get; set; }

        public int? Status { get; set; }

        [StringLength(250)]
        public string MailForm { get; set; }

        [StringLength(1000)]
        public string MailTo { get; set; }

        [StringLength(1000)]
        public string MailCc { get; set; }

        [StringLength(500)]
        public string MailBcc { get; set; }

        [StringLength(4000)]
        public string Body { get; set; }

        [StringLength(250)]
        public string Subject { get; set; }

        [StringLength(3)]
        public string Gateway { get; set; }

        public DateTime? MailDate { get; set; }

        [StringLength(250)]
        public string Attachment { get; set; }

        [StringLength(2000)]
        public string TransmitionError { get; set; }

        public int? IsAutoEmail { get; set; }

        public int? Tries { get; set; }

        public int? IsZip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HostPlus_EmailAttachments> HostPlus_EmailAttachments { get; set; }
    }
}
