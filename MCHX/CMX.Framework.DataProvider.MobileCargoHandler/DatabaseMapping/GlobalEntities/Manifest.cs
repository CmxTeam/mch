namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Manifests")]
    public partial class Manifest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Manifest()
        {
            Waybills = new HashSet<Waybill>();
        }

        public int ManifestID { get; set; }

        [Column(TypeName = "xml")]
        public string XmlFile { get; set; }

        public DateTime? InitializedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? AccessedDate { get; set; }

        public DateTime? ProcessedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Waybill> Waybills { get; set; }
    }
}
