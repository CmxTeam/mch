namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_TimestampsCodes
    {
        [Key]
        public int RecID { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(255)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Description { get; set; }
    }
}
