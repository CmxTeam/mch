namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_Locations
    {
        [Key]
        public int RecId { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(10)]
        public string Location { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public string Connection { get; set; }

        public int? TimeDifference { get; set; }

        [StringLength(30)]
        public string WebConnectionName { get; set; }

        public int? EntityID { get; set; }

        [StringLength(50)]
        public string DBName { get; set; }
    }
}
