namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_AutoEmail_Reports
    {
        [Key]
        public long RecId { get; set; }

        public int? EmailId { get; set; }

        [StringLength(250)]
        public string Report { get; set; }

        [StringLength(250)]
        public string Parameters { get; set; }

        [StringLength(1000)]
        public string SqlQuery { get; set; }

        [StringLength(250)]
        public string SqlParameters { get; set; }

        [StringLength(1000)]
        public string SqlNonQuery { get; set; }

        [StringLength(250)]
        public string FileName { get; set; }

        [StringLength(10)]
        public string ReportType { get; set; }

        public DateTime? RecDate { get; set; }
    }
}
