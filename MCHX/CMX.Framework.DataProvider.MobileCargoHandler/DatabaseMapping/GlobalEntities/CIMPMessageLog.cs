namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CIMPMessageLog
    {
        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        [StringLength(100)]
        public string FileName { get; set; }

        [StringLength(20)]
        public string MessageType { get; set; }

        [StringLength(100)]
        public string MailboxName { get; set; }

        public string RawMessage { get; set; }

        public bool ProcessingStatus { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime? DownloadedTime { get; set; }

        public DateTime? SentTime { get; set; }

        [StringLength(250)]
        public string SenderMailBox { get; set; }

        [Required]
        [StringLength(5)]
        public string Direction { get; set; }

        [StringLength(2)]
        public string Priority { get; set; }
    }
}
