namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.RatingDetails")]
    public partial class RatingDetail
    {
        public int RatingDetailID { get; set; }

        public int DetailID { get; set; }

        [StringLength(1)]
        public string RateClass { get; set; }

        [StringLength(7)]
        public string CommodityItemNo { get; set; }

        public double? ChargeableWgt { get; set; }

        public double? RateOrCharge { get; set; }

        [StringLength(1)]
        public string RateChargeFlag { get; set; }

        [StringLength(1)]
        public string RatingTypeIndicator { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
