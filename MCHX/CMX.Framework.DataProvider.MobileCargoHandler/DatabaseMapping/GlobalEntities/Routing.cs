namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Routings")]
    public partial class Routing
    {
        public int RoutingID { get; set; }

        public int DetailID { get; set; }

        public int? ItemNo { get; set; }

        [StringLength(4)]
        public string DepartAirport { get; set; }

        [StringLength(2)]
        public string DepartAirportCountry { get; set; }

        [StringLength(25)]
        public string DepartAirportCity { get; set; }

        [StringLength(4)]
        public string ArrivalAirport { get; set; }

        [StringLength(2)]
        public string ArrivalAirportCountry { get; set; }

        [StringLength(25)]
        public string ArrivalAirportCity { get; set; }

        [StringLength(10)]
        public string FlightNo { get; set; }

        [StringLength(4)]
        public string CarrierCode { get; set; }

        public DateTime? DepartDateTime { get; set; }

        public DateTime? ArrivalDateTime { get; set; }

        [StringLength(4)]
        public string DepartTerminal { get; set; }

        [StringLength(25)]
        public string DepartTerminalName { get; set; }

        [StringLength(4)]
        public string ArrivalTerminal { get; set; }

        [StringLength(25)]
        public string ArrivalTerminalName { get; set; }

        [StringLength(2)]
        public string TransferType { get; set; }

        [StringLength(3)]
        public string AircraftType { get; set; }

        [StringLength(12)]
        public string AircraftRegistrationNo { get; set; }

        [StringLength(10)]
        public string CodeShareFlightNo { get; set; }

        [StringLength(4)]
        public string CodeShareCarrierCode { get; set; }

        [StringLength(3)]
        public string IataIcaoCode { get; set; }

        [StringLength(17)]
        public string StageCode { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
