namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_SessionTypes
    {
        [Key]
        public int RecId { get; set; }

        public DateTime RecDate { get; set; }

        [StringLength(20)]
        public string UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public bool? AuthRequired { get; set; }

        [StringLength(50)]
        public string GroupName { get; set; }

        [StringLength(50)]
        public string Icon { get; set; }
    }
}
