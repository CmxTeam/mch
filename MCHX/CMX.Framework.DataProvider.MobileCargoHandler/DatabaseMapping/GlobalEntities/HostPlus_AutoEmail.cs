namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HostPlus_AutoEmail
    {
        [Key]
        public long RecId { get; set; }

        [StringLength(250)]
        public string TaskName { get; set; }

        [StringLength(250)]
        public string MailFrom { get; set; }

        [StringLength(1000)]
        public string MailTo { get; set; }

        [StringLength(1000)]
        public string MailCc { get; set; }

        [StringLength(500)]
        public string MailBcc { get; set; }

        [StringLength(250)]
        public string Subject { get; set; }

        [StringLength(250)]
        public string BodyHeader { get; set; }

        [StringLength(250)]
        public string BodyFooter { get; set; }

        [StringLength(2000)]
        public string BodyDetail { get; set; }

        [StringLength(3)]
        public string Gateway { get; set; }

        [StringLength(100)]
        public string MailDays { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? NextDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime? LastDate { get; set; }

        [Required]
        [StringLength(250)]
        public string Frequency { get; set; }

        [StringLength(5)]
        public string FromTime { get; set; }

        [StringLength(5)]
        public string ToTime { get; set; }

        [StringLength(250)]
        public string Attachment { get; set; }

        [StringLength(1000)]
        public string SqlQuery { get; set; }

        [StringLength(250)]
        public string SqlParameters { get; set; }

        [StringLength(1000)]
        public string SqlNonQuery { get; set; }

        [StringLength(250)]
        public string TransmitionError { get; set; }

        public int? IsZip { get; set; }

        public DateTime RecDate { get; set; }
    }
}
