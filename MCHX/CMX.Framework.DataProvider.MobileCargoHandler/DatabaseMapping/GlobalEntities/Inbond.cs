namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Inbonds")]
    public partial class Inbond
    {
        public int InbondID { get; set; }

        public int DetailID { get; set; }

        [StringLength(3)]
        public string TransferDestination { get; set; }

        [StringLength(8)]
        public string DomesticIntl { get; set; }

        [StringLength(4)]
        public string Liability { get; set; }

        [StringLength(4)]
        public string TransferTo { get; set; }

        [StringLength(1)]
        public string Mot { get; set; }

        [StringLength(12)]
        public string BondedCarrierId { get; set; }

        public long? TransitNo { get; set; }

        [StringLength(11)]
        public string InbondStatus { get; set; }

        public virtual Detail Detail { get; set; }
    }
}
