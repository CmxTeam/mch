namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Details")]
    public partial class Detail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Detail()
        {
            Addresses = new HashSet<Address>();
            Charges = new HashSet<Charge>();
            CustomsActions = new HashSet<CustomsAction>();
            Inbonds = new HashSet<Inbond>();
            Products = new HashSet<Product>();
            RatingDetails = new HashSet<RatingDetail>();
            Routings = new HashSet<Routing>();
            Statuses = new HashSet<Status>();
            Ulds = new HashSet<Uld>();
        }

        public int DetailID { get; set; }

        public int WaybillID { get; set; }

        [StringLength(12)]
        public string House { get; set; }

        [StringLength(1)]
        public string PartialIdCode { get; set; }

        [StringLength(20)]
        public string Action { get; set; }

        [StringLength(4)]
        public string CbsaReportingCarrier { get; set; }

        [StringLength(4)]
        public string MxClient { get; set; }

        [StringLength(2)]
        public string CountryOrigin { get; set; }

        [StringLength(1)]
        public string Fda { get; set; }

        [StringLength(1)]
        public string ShipmentDescriptionCode { get; set; }

        public double? GrossWgt { get; set; }

        [StringLength(1)]
        public string Uom { get; set; }

        public int? TotalQty { get; set; }

        [StringLength(3)]
        public string TotalQtyUom { get; set; }

        public int? PartialQty { get; set; }

        [StringLength(3)]
        public string PartialQtyUom { get; set; }

        public double? PartialWgt { get; set; }

        public long? VolWgt { get; set; }

        public long? CustomsValue { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        public decimal? CurrencyExchangeRate { get; set; }

        [StringLength(12)]
        public string DeclaredValueCarriage { get; set; }

        [StringLength(12)]
        public string DeclaredValueCustoms { get; set; }

        [StringLength(11)]
        public string AmountInsurance { get; set; }

        [StringLength(5)]
        public string Nomination { get; set; }

        [StringLength(3)]
        public string PtpDestination { get; set; }

        public DateTime? PtpDate { get; set; }

        [StringLength(35)]
        public string Ucr { get; set; }

        [StringLength(4)]
        public string DischargeAirport { get; set; }

        [StringLength(4)]
        public string DischargeTerminal { get; set; }

        public int? HazardousMaterialIndicator { get; set; }

        [StringLength(21)]
        public string SupplementaryCargoRefNo { get; set; }

        [StringLength(60)]
        public string Instructions { get; set; }

        public int? EntryType { get; set; }

        public long? EntryNo { get; set; }

        [StringLength(4)]
        public string Destination { get; set; }

        [StringLength(1)]
        public string MxSpecialProcessingCode { get; set; }

        [StringLength(10)]
        public string FsnStatus { get; set; }

        [StringLength(5)]
        public string TotalChargePrepaidIndicator { get; set; }

        [StringLength(5)]
        public string TotalDisbursementPrepaidIndicator { get; set; }

        [StringLength(3)]
        public string CargoMovementType { get; set; }

        [StringLength(2)]
        public string CargoMovementCountryCode { get; set; }

        [StringLength(1)]
        public string TotalRatingTypeCode { get; set; }

        [StringLength(5)]
        public string MonetarySummationPrepaidIndicator { get; set; }

        [StringLength(12)]
        public string MonetarySummationGrandTotal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Charge> Charges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomsAction> CustomsActions { get; set; }

        public virtual Waybill Waybill { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inbond> Inbonds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RatingDetail> RatingDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Routing> Routings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Status> Statuses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Uld> Ulds { get; set; }
    }
}
