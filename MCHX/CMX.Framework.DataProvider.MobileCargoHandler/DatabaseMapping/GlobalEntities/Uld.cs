namespace CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dam.Ulds")]
    public partial class Uld
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Uld()
        {
            UldDetails = new HashSet<UldDetail>();
        }

        public int UldID { get; set; }

        public int DetailID { get; set; }

        public int? ItemNo { get; set; }

        [StringLength(2)]
        public string UldOwnerCode { get; set; }

        [StringLength(20)]
        public string UldBagNo { get; set; }

        [StringLength(3)]
        public string UldType { get; set; }

        public long? Volume { get; set; }

        [StringLength(3)]
        public string VolumeUom { get; set; }

        public decimal? Qty { get; set; }

        [StringLength(3)]
        public string QtyUom { get; set; }

        public decimal? Wgt { get; set; }

        [StringLength(3)]
        public string WgtUom { get; set; }

        [StringLength(1)]
        public string Breakdown { get; set; }

        public DateTime? LoadedDateTime { get; set; }

        public DateTime? UnloadedDateTime { get; set; }

        [StringLength(2)]
        public string CustomsNoteContentCode { get; set; }

        [StringLength(1)]
        public string RatingTypeIndicator { get; set; }

        public virtual Detail Detail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UldDetail> UldDetails { get; set; }
    }
}
