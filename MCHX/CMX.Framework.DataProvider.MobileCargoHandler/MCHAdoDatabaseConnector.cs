﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.MobileCargoHandler
{
    public abstract class MCHAdoDatabaseConnector<T> : AdoNetDatabaseConnector<T>
    {
        public MCHAdoDatabaseConnector()
        {                
        }
        public MCHAdoDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }
        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MchDbContext"].ConnectionString; }
        } 

    }
}