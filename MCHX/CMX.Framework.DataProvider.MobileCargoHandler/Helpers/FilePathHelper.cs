﻿using System.Configuration;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Helpers
{
    public static class FilePathHelper
    {
        public static string AttachmentRelativePath
        {
            get
            {
                return @"\Content\uploadedImages\documentAttachmantes\";
            }
        }
        public static string DriverImagesRelativePath
        {
            get
            {
                return @"\Content\uploadedImages\driverImages\";
            }
        }

        public static string AttachmentFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["DocumentAttachmentsFolder"];
            }
        }
        public static string DriverImagesFullPath
        {
            get
            {
                return ConfigurationManager.AppSettings["DriverImagesFolder"];
            }
        }


    }
}