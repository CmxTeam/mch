﻿using System;
using CMX.Framework.DataProvider.Entities.Common;
using Model = CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Helpers
{
    public static class MchExtensions
    {
        public static AccountModel ConvertTo(this Account account)
        {
            if (account == null) return new AccountModel();

            return new AccountModel
            {
                AccountCode = account.AccountCode,
                AccountName = account.AccountName,
                Id = account.Id,
                IsOwner = account.IsOwner,
                ShellAccount = account.Customer.ConvertTo()
            };
        }
        public static CustomerModel ConvertTo(this Customer customer)
        {
            if (customer == null) return new CustomerModel();
            return new CustomerModel
            {
                AccountNumber = customer.AccountNumber,
                Acount = customer.Account.ConvertTo(),
                AddressLine1 = customer.AddressLine1,
                AddressLine2 = customer.AddressLine2,
                City = customer.City,
                Country = customer.Country.ConvertTo(),
                Id = customer.Id,
                IsResidential = customer.IsResidential,
                Name = customer.Name,
                PostalCode = customer.PostalCode,
                State = customer.State.ConvertTo()
            };
        }
        public static Model.CountryModel ConvertTo(this Country country)
        {
            if (country == null) return new Model.CountryModel();

            return new Model.CountryModel
            {
                CountryName = country.Country1,
                Currency = country.Currency.ConvertTo(),
                Id = country.Id,
                TwoCharacterCode = country.TwoCharacterCode
            };
        }
        public static Model.CurrencyModel ConvertTo(this Currency currency)
        {
            if (currency == null) return new Model.CurrencyModel();
            return new Model.CurrencyModel { Currency3Code = currency.Currency3Code, CurrencyName = currency.Currency1, CurrencySign = currency.CurrencySign, Id = currency.Id };
        }

        public static AgentModel ConvertTo(this Agent agent)
        {
            if (agent == null) return new AgentModel();

            return new AgentModel
            {
                AccountNumber = agent.AccountNumber,
                CASSAddress = agent.CASSAddress,
                Id = agent.Id,
                Name = agent.Name,
                NumericCode = agent.NumericCode,
                Participant = agent.ParticipantIdentifier.ConvertTo(),
                Place = agent.Place
            };
        }
        public static ParticipantIdentifierModel ConvertTo(this ParticipantIdentifier identifier)
        {
            if (identifier == null) return new ParticipantIdentifierModel();
            return new ParticipantIdentifierModel { Code = identifier.Code, Description = identifier.Description, Id = identifier.Id };
        }
        public static Model.AttachmentModel ConvertTo(this Attachment attachment)
        {
            if (attachment == null) return new Model.AttachmentModel();
            return new Model.AttachmentModel
            {
                Image = FilePathHelper.AttachmentRelativePath + attachment.FileName,
                EntityID = attachment.EntityID,
                EntityType = attachment.EntityType.ConvertTo(),
                FileName = attachment.FileName,
                FileType = attachment.FileType,
                Id = attachment.Id,
                DbId = attachment.Id,
                Name = attachment.Name,
                RecDate = attachment.RecDate,
                UserId = attachment.UserId,
                Description = attachment.Description,
                DocumentTypeId = attachment.DocumentTypeId,
                EntityTypeId = attachment.EntityTypeId
            };
        }

        public static CarrierModel ConvertTo(this Carrier carrier)
        {
            if (carrier == null) return new CarrierModel();

            return new CarrierModel
            {
                Carrier3Code = carrier.Carrier3Code,
                CarrierCode = carrier.CarrierCode,
                CarrierLogo = carrier.CarrierLogoPath,
                CarrierMOT = carrier.MOT.ConvertTo(),
                CarrierName = carrier.CarrierName,
                Id = carrier.Id
            };
        }
        public static Carrier ConvertTo(this CarrierModel carrier)
        {
            if (carrier == null) return new Carrier();

            return new Carrier
            {
                Carrier3Code = carrier.Carrier3Code,
                CarrierCode = carrier.CarrierCode,              
                CarrierName = carrier.CarrierName,   
                MOTId = (int?)carrier.CarrierMOT.Id,                            
            };
        }
        public static DischargeTypeModel ConvertTo(this DischargeType dischargeType)
        {
            if (dischargeType == null) return new DischargeTypeModel();
            return new DischargeTypeModel { Id = dischargeType.Id, Name = dischargeType.Name };
        }
        public static Model.EntityTypeEnum ConvertTo(this EntityType entityType)
        {
            if (entityType == null) return new Model.EntityTypeEnum();
            return (Model.EntityTypeEnum)entityType.Id;
        }
        public static Model.MOTModel ConvertTo(this MOT mot)
        {
            if (mot == null) return new Model.MOTModel();
            return new Model.MOTModel { Id = mot.Id, TransportModeName = mot.MOT1 };
        }
        public static MOT ConvertTo(this Model.MOTModel mot)
        {
            if (mot == null) return new MOT();
            return new MOT { MOT1 = mot.TransportModeName };
        }
        public static Model.PortModel ConvertTo(this Port port)
        {
            if (port == null) return new Model.PortModel();
            return new Model.PortModel { Id = port.Id, Name = port.Port1 };
        }

        public static Model.StateModel ConvertTo(this State state)
        {
            if (state == null) return new Model.StateModel();
            return new Model.StateModel { Country = state.Country.ConvertTo(), Id = state.Id, StateProvince = state.StateProvince, TwoCharacterCode = state.TwoCharacterCode };
        }
        
        
        
        public static Attachment ConvertTo(this  Model.AttachmentModel attachment)
        {
            if (attachment == null) return new Attachment();
            return new Attachment
            {
               // AttachmentImage = attachment.Image,
                Description = attachment.Description,
                DocumentTypeId = attachment.DocumentTypeId,
                EntityID = attachment.EntityID,
                EntityTypeId = attachment.EntityTypeId,
                FileName = attachment.FileName,
                FileType = attachment.FileType,
                Id = attachment.DbId,
                Name = attachment.Name,
                RecDate = DateTime.UtcNow,
                UserId = attachment.UserId
            };
        }

        public static AwbShipmentModel ConvertTo(this AWB awb)
        {
            if (awb == null) return new AwbShipmentModel();
            else
                return new AwbShipmentModel
                {
                    Id = awb.Id,
                    SLAC = awb.SLAC,
                    Pieces = awb.Pieces,
                    PercentReceived = awb.PercentReceived,
                    ForkliftPieces = awb.ForkliftPieces,
                    ReceivedPieces = awb.ReceivedPieces,
                    WeightUOMId = awb.WeightUOMId,
                    DescriptionOfGoods = awb.DescriptionOfGoods,
                    SerialNumber = awb.AWBSerialNumber,
                    Weight = awb.Weight,
                    RecDate = awb.RecDate
                };
        }
        public static HwbShipmentModel ConvertTo(this HWB hwb)
        {
            if (hwb == null) return new HwbShipmentModel();
            else
                return new HwbShipmentModel
                {
                    Id = hwb.Id,
                    SLAC = hwb.SLAC,
                    Pieces = hwb.Pieces,
                    PercentReceived = hwb.PercentReceived,
                    ForkliftPieces = hwb.ForkliftPieces,
                    ReceivedPieces = hwb.ReceivedPieces,
                    WeightUOMId = hwb.WeightUOMId,
                    DescriptionOfGoods = hwb.DescriptionOfGoods,
                    SerialNumber = hwb.HWBSerialNumber,
                    Weight = hwb.Weight,
                    RecDate = hwb.RecDate
                };
        }
        public static DriverModel ConvertTo(this CarrierDriver driver)
        {
            if (driver == null) return new DriverModel();
            else
                return new DriverModel
                {
                    Id = driver.Id,
                    FullName = driver.FullName,
                    LastName = driver.LastName,
                    FirstName = driver.FirstName,
                    DriverPhoto = driver.DriverPhoto,
                    DriverThumbnail = driver.DriverThumbnail,
                };
        }

        public static CarrierDriver ConvertTo(this DriverModel driver)
        {
            if (driver == null) return new CarrierDriver();
            else
                return new CarrierDriver
                {
                    FullName = driver.FirstName + " " + driver.LastName,
                    LastName = driver.LastName,
                    FirstName = driver.FirstName,                  
                    DriverThumbnail = driver.DriverThumbnail,
                    TruckingCompanyId = (int?)driver.TruckingCompany.Id,
                    LicenseNumber = driver.LicenseNumber,
                    LicenseExpiry = driver.LicenseExpiry,
                    STANumber = driver.STANumber,
                    StateId = (int)driver.State.Id
                };
        }
        public static StatusModel ConvertTo(this Status status)
        {
            if (status == null) return new StatusModel();
            else
                return new StatusModel
                {
                    Id = status.Id,
                    Name = status.Name,
                    Description = status.Description,
                    DisplayName = status.DisplayName
                };
        }
        public static Status ConvertTo(this StatusModel status)
        {
            if (status == null) return new Status();
            else
                return new Status
                {
                    DisplayName = status.DisplayName,
                    Name = status.Name

                };
        }
        public static WarehouseModel ConvertTo(this Warehouse warehouse)
        {
            if (warehouse == null) return new WarehouseModel();
            else
                return new WarehouseModel
                {
                    Id = warehouse.Id,
                    Name = warehouse.WarehouseName
                };
        }
        public static Warehouse ConvertTo(this WarehouseModel warehouse)
        {
            if (warehouse == null) return new Warehouse();
            else
                return new Warehouse
                {
                    WarehouseName = warehouse.Name
                };
        }
        public static TransactionModel ConvertTo(this Transaction transaction)
        {
            if (transaction == null) return new TransactionModel();
            return new TransactionModel {
                Id = transaction.Id,
                EntityType = (EntityTypeEnum)transaction.EntityTypeId,
                EntityId = transaction.EntityId,
                Description = transaction.Description,
                Reference = transaction.Reference,
                StatusTimestamp = transaction.StatusTimestamp,
                Status = transaction.Status.ConvertTo(),
                TransactionAction = transaction.TransactionAction.ConvertTo()
            };
        }
        public static TransactionActionModel ConvertTo(this TransactionAction transactionAction)
        {
            if (transactionAction == null) return new TransactionActionModel();
            return new TransactionActionModel
            {
                Id = transactionAction.Id,
                Code = transactionAction.Code
            };
        }
        public static AmsCodeModel ConverTo(this AmsCode amsCode)
        {
            if (amsCode == null) return new AmsCodeModel();
            return new AmsCodeModel
            {
                Id = amsCode.Id,
                Code = amsCode.Code,                
                Description = amsCode.Description,
                IsGreenCode = amsCode.IsGreenCode,
                FullName = amsCode.Code + (amsCode.Description != null ? " (" + amsCode.Description + ")": string.Empty)
            };
        }
        public static AmsCode ConverTo(this AmsCodeModel amsCode)
        {
            if (amsCode == null) return new AmsCode();
            return new AmsCode
            {               
                Code = amsCode.Code,
                Description = amsCode.Description
            };
        }

    }
}
