﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetPorts : MchDatabaseConnector<IEnumerable<PortModel>>
    {
        public override IEnumerable<PortModel> Execute()
        {
            return Context.Ports.Select(p => new PortModel { Id = p.Id, Name = p.IATACode }).OrderBy(p => p.Name).ToList();
        }
    }
}
