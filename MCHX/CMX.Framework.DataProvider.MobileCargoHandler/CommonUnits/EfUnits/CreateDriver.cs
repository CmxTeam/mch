﻿using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class CreateDriver : MchDatabaseConnector<bool>
    {
        private DriverModel _driver;
        public CreateDriver(DriverModel driver)
        {
            _driver = driver;
        }
        public override bool Execute()
        {
            if (_driver == null) return false;
            CarrierDriver dbDriver = _driver.ConvertTo();
            dbDriver.DriverPhoto = FileHelper.SaveImage(_driver.DriverPhoto, FilePathHelper.DriverImagesFullPath, string.Empty);
            dbDriver.LicenseImage = FileHelper.SaveImage(_driver.LicenseImage, FilePathHelper.DriverImagesFullPath, string.Empty);
            Context.CarrierDrivers.Add(dbDriver);
            Context.SaveChanges();
            return true;
        }
    }
}