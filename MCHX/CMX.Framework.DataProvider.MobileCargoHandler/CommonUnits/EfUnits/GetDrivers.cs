﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetDrivers : MchDatabaseConnector<IEnumerable<DriverModel>>
    {
        private long? _driverId;

        public GetDrivers(long? driverId)
        {
            _driverId = driverId;
        }

        public override IEnumerable<DriverModel> Execute()
        {
            return Context.CarrierDrivers.Include("State")
                .Where(d => !_driverId.HasValue || d.Id == _driverId)
                .Select(d => new DriverModel { Id = d.Id, FullName = d.FullName, LicenseNumber = d.LicenseNumber,
                    State = new StateModel { Id = (long)d.StateId, TwoCharacterCode = d.State.TwoCharacterCode } }).ToList();
        }
    }
}