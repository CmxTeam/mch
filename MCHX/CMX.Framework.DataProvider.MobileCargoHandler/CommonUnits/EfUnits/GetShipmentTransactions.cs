﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetShipmentTransactions : MchDatabaseConnector<IEnumerable<TransactionModel>>
    {
        private long _userId;
        private long _shipmentId;
        private EntityTypeEnum _shipmentType;
        public GetShipmentTransactions(long userId,long shipmentId,EntityTypeEnum shipmentType)
        {
            _userId = userId;
            _shipmentId = shipmentId;
            _shipmentType = shipmentType;
        }
        public override IEnumerable<TransactionModel> Execute()
        {
            var dbTransactions = Context.Transactions.Include("Status").Include("TransactionAction").Where(t => t.EntityId == _shipmentId && t.EntityTypeId == (int)_shipmentType /*&& t.UserId == _userId*/).ToList();
            return dbTransactions.Select(s => s.ConvertTo());
        }
    }
}