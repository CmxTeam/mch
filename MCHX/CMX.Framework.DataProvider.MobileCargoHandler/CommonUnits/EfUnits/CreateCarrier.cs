﻿using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class CreateCarrier : MchDatabaseConnector<bool>
    {
        CarrierModel _carrier;
        public CreateCarrier(CarrierModel carrier)
        {
            _carrier = carrier;
        }
        public override bool Execute()
        {
            if (_carrier == null) return false;
            Carrier dbCarrier = _carrier.ConvertTo();
            dbCarrier.CarrierLogoPath = FileHelper.SaveImage(_carrier.CarrierLogo, FilePathHelper.DriverImagesFullPath, string.Empty);
            Context.Carriers.Add(dbCarrier);
            Context.SaveChanges();
            return true;
        }
    }
}