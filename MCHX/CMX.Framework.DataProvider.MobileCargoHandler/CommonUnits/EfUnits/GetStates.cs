﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetStates : MchDatabaseConnector<IEnumerable<StateModel>>
    {
        public override IEnumerable<StateModel> Execute()
        {
            var states = Context.States.Include("Country").Where(s => s.StateProvince != null && s.TwoCharacterCode != null).ToList();
            return states.Select(a => a.ConvertTo()).ToList();
        }
    }
}
