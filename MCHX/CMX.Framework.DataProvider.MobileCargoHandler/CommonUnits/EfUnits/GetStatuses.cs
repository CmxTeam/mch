﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetStatuses : MchDatabaseConnector<IEnumerable<StatusModel>>
    {
        private EntityTypeEnum? _groupId;
        private List<long> _ids;

        public GetStatuses()
        {         
        }

        public GetStatuses(EntityTypeEnum groupId)
        {
            _groupId = groupId;
        }

        public GetStatuses(EntityTypeEnum groupId, params long[] ids)
            :this(groupId)
        {
            _ids = ids.ToList();            
        }


        public override IEnumerable<StatusModel> Execute()
        {
            var a = Context.Statuses
                .Where(s => !_groupId.HasValue || s.EntityTypeId == (long)_groupId).ToList()
                .Where(s => _ids == null || _ids.Contains(s.Id))
                .Select(s => new StatusModel { Id = s.Id, Name = s.Name }).OrderBy(s => s.Name).ToList();
            return a;
        }
    }
}
