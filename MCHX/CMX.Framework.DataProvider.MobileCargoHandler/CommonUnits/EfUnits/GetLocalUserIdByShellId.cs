﻿using System.Linq;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetLocalUserIdByShellId : MchDatabaseConnector<long>
    {
        private long _shellUserId;
        public GetLocalUserIdByShellId(long shellUserId)
        {
            _shellUserId = shellUserId;
        }

        public override long Execute()
        {
            return Context.UserProfiles.Single(up => up.ShellUserId == _shellUserId).Id;
        }
    }
}