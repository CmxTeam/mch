﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetMots : MchDatabaseConnector<IEnumerable<MOTModel>>
    {
        public override IEnumerable<MOTModel> Execute()
        {
            var mots = Context.MOTs.ToList();
            return mots.Select(m => m.ConvertTo()).ToList();
        }
    }
}