﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities.SearchModels;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetAttachments : MchDatabaseConnector<IEnumerable<AttachmentModel>>
    {
        private string _shipingRef;
        public GetAttachments(string shipingRef)
        {
            this._shipingRef = shipingRef;
        }
        public override IEnumerable<AttachmentModel> Execute()
        {
            var entity = new GetShipmentByFilter(new ShipmentFilterModel{SerialNumber = _shipingRef}).Execute();

            var attachment = Context.Attachments.Include("EntityType").Where(a => a.EntityTypeId == (int)entity.EntityType
                                            && a.EntityID == entity.Id).ToList();
            return attachment.Select(a => a.ConvertTo());                            
        }
    }
}