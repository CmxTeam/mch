﻿using System.Linq;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities.SearchModels;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetShipmentByFilter : MchDatabaseConnector<ShipmentModel>
    {
        private ShipmentFilterModel _searchModel;
        public GetShipmentByFilter(ShipmentFilterModel searchModel)
        {
            if (searchModel != null) _searchModel = searchModel;
            else searchModel = new ShipmentFilterModel();
        }
        public override ShipmentModel Execute()
        {
            ShipmentModel shipmentModel = new ShipmentModel();
            if (_searchModel.ShipmentId != null && _searchModel.EntityType != null)
            {
                if (_searchModel.EntityType == EntityTypeEnum.AWB)
                {
                    shipmentModel = Context.AWBs.Where(a => a.Id == _searchModel.ShipmentId
                                    && (_searchModel.CarrierCode == null || _searchModel.CarrierCode == a.Carrier.Carrier3Code)
                                    ).ToList().Select(a => a.ConvertTo()).FirstOrDefault();
                }
                else
                {
                    shipmentModel = Context.HWBs.Where(a => a.Id == _searchModel.ShipmentId).ToList().Select(a => a.ConvertTo()).FirstOrDefault();
                }
            }
            else if (_searchModel.SerialNumber != null)
            {
                var awbSerialLength = _searchModel.SerialNumber.Length - 8;
                var awbSerial = _searchModel.SerialNumber.Substring(awbSerialLength < 0 ? 0 : awbSerialLength);
                var awbCarrier = _searchModel.SerialNumber.Substring(0, _searchModel.SerialNumber.IndexOf(awbSerial));
                var absentCarrier = string.IsNullOrEmpty(awbCarrier);
                var awb = Context.AWBs.Where(e => e.AWBSerialNumber == awbSerial && (absentCarrier || e.Carrier.Carrier3Code == awbCarrier)).FirstOrDefault();
                if (awb != null) shipmentModel = awb.ConvertTo();
                else
                {
                    var hwb = Context.HWBs.Where(h => h.HWBSerialNumber == _searchModel.SerialNumber).FirstOrDefault();
                    if (hwb != null) shipmentModel = hwb.ConvertTo();
                }
            }
            return shipmentModel;
        }
    }
}
