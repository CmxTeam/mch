﻿using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetAmsCodes : MchDatabaseConnector<IEnumerable<AmsCodeModel>>
    {
        private long _amsId;
        private string _code;
        public GetAmsCodes(long amsId = 0,string code = null)
        {
            _amsId = amsId;
            _code = code;
        }
        public override IEnumerable<AmsCodeModel> Execute()
        {
            var dbAmsCodes = Context.AmsCodes.Where(a => (_amsId == 0 || a.Id == _amsId)
                                                         && (_code == null || a.Code == _code)
                                                         && (_code != null || a.IsGreenCode == true)).ToList();
            return dbAmsCodes.Select(s => s.ConverTo());
        }
    }
}
