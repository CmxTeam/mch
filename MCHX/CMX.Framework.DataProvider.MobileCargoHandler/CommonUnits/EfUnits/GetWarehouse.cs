﻿using System.Linq;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetWarehouse: MchDatabaseConnector<WarehouseModel>
    {
        private readonly string _station;
        public GetWarehouse(string station)
        {
            _station = station;
        }

        public override WarehouseModel Execute()
        {
            return Context.Warehouses.Where(w => w.Port.IATACode == _station).ToList().Select(s => s.ConvertTo()).Single();
        }
    }
}
