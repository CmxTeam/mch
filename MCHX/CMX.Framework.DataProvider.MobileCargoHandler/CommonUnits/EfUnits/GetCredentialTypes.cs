﻿using System.Linq;
using System.Collections.Generic;
using Model = CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetCredentialTypes : MchDatabaseConnector<IEnumerable<Model.IdNameEntity>>
    {
        public override IEnumerable<Model.IdNameEntity> Execute()
        {
            return Context.CredentialTypes.Select(c => new Model.IdNameEntity { Id = c.Id, Name = c.Name }).ToList();
        }
    }
}