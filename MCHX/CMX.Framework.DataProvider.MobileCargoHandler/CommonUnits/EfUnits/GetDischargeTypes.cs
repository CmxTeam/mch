﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetDischargeTypes : MchDatabaseConnector<IEnumerable<DischargeTypeModel>>
    {
        public override IEnumerable<DischargeTypeModel> Execute()
        {
            var dischargeTypes = Context.DischargeTypes.ToList();
            return dischargeTypes.Select(d => d.ConvertTo());
        }
    }
}