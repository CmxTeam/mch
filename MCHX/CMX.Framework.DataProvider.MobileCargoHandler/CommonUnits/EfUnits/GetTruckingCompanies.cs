﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.Helpers;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetTruckingCompanies : MchDatabaseConnector<IEnumerable<CarrierModel>>
    {
        public override IEnumerable<CarrierModel> Execute()
        {
            var carriers = Context.Carriers.Include("MOT").Where(c => c.MOTId == 3).ToList();
            return carriers.Select(r => r.ConvertTo());
        }
    }
}