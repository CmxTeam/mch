﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits
{
    public class GetCarriers : MchDatabaseConnector<IEnumerable<CarrierModel>>
    {
        public override IEnumerable<CarrierModel> Execute()
        {
            return Context.Carriers.Where(c => c.CarrierName != null && c.CarrierCode != null)
                          .Select(c => new CarrierModel { Id = c.Id, CarrierName = c.CarrierName })
                          .OrderBy(c => c.CarrierName).ToList();
        }
    }
}
