﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.AdoUnits
{
    public class GetEntityReference : MCHAdoDatabaseConnector<string>
    {  
        public GetEntityReference(long entityId, EntityTypeEnum entityType)
            : base(new CommandParameter("@EntityId", entityId), new CommandParameter("@EntityTypeId", (int)entityType))
        {}
        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }
        public override string CommandQuery
        {
            get { return "MCH_GetEntityReference"; }
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override string ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows[0][0].ToString(); 
        }
    }
}