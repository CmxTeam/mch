﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Helpers;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.AdoUnits
{   
    public class GetShipmentCustomsStatuses : MCHAdoDatabaseConnector<IEnumerable<CustomsHistoryModel>>
    {
        private EntityTypeEnum _entityType;
        public GetShipmentCustomsStatuses(string shipmentNumber, EntityTypeEnum entityType, bool latest)
            : base(new CommandParameter("@ShipmentNumber", shipmentNumber), new CommandParameter("@Latest", latest))
        {
            _entityType = entityType;
        }
        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MCHCoreContext"].ConnectionString; }
        }
        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }
        public override string CommandQuery
        {
            get { return _entityType == EntityTypeEnum.AWB ? "GetAwbCustomsHistory" : "GetHwbCustomsHistory"; }
        }
        public override IEnumerable<CustomsHistoryModel> ObjectInitializer(List<System.Data.DataRow> rows)
        {
            return rows.Select(r => new CustomsHistoryModel
            {
                Id = r["RowNumber"].TryParse<long>(),                
                EntryNo = r["EntryNo"].ToString(),
                House = r["House"].ToString(),
                Master = r["Master"].ToString(),
                StatusCode = r["StatusCode"].ToString(),
                StatusID = r["StatusID"].TryParse<int>(),
                MessageType = r["MessageType"].ToString(),
                PartialIdCode = r["PartialIdCode"].ToString(),
                Date = r["CustomsDateTime"].TryParse<DateTime>(),
                PiecesAffectedCount = r["QuantityAffected"].TryParse<long>(),                
                TransactionDateTime = r["TransactionDateTime"].TryParse<DateTime>(),
                CustomsCode = new AmsCodeModel{Code = r["Code"].ToString(),Description = r["CodeDescription"].ToString()},
            }
            );
        }

    }
}
