﻿using System.Web.Http;
using System.Web.Http.Cors;
using System.Collections.Generic;
using CMX.Framework.DataProvider.Controllers;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities.SearchModels;

namespace CMX.Framework.DataProvider.MobileCargoHandler.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MchCommonController : CommonController
    {
        private readonly MchCommonDatabaseManager _manager = new MchCommonDatabaseManager();                

        [HttpGet]
        public DataContainer<IEnumerable<DischargeTypeModel>> GetDischargeTypes()
        {            
            return MakeTransactional<IEnumerable<DischargeTypeModel>>(() => _manager.GetDischargeTypes());
        }

        [HttpGet]
        public DataContainer<IEnumerable<PortModel>> GetPorts()
        {
            return MakeTransactional(() => _manager.GetPorts());
        }

        [HttpGet]
        public DataContainer<IEnumerable<DriverModel>> GetDrivers(long? driverId = null)
        {
            return MakeTransactional(() => _manager.GetDrivers(driverId));
        }

        [HttpGet]
        public DataContainer<IEnumerable<CarrierModel>> GetTruckingCompanies()
        {
            return MakeTransactional(() => _manager.GetTruckingCompanies());
        }

        [HttpGet]
        public DataContainer<IEnumerable<IdNameEntity>> GetCredentialTypes()
        {
            return MakeTransactional(() => _manager.GetCredentialTypes());
        }

        [HttpGet]
        public DataContainer<IEnumerable<AttachmentModel>> GetAttachments(string shipingRef)
        {
            return MakeTransactional(() => _manager.GetAttachments(shipingRef));
        }

        [HttpGet]
        public DataContainer<IEnumerable<StatusModel>> GetStatuses()
        {
            return MakeTransactional(() => _manager.GetStatuses());
        }

        [HttpGet]
        public DataContainer<IEnumerable<CarrierModel>> GetCarriers()
        {
            return MakeTransactional(() => _manager.GetCarriers());
        }

        [HttpGet]
        public DataContainer<ShipmentModel> GetShipmentByFilter([FromUri]ShipmentFilterModel searchModel)
        {
            return MakeTransactional(() => _manager.GetShipmentByFilter(searchModel));
        }

        [HttpPost]
        public DataContainer<bool> CreateDriver(DriverModel driver)
        {
            return MakeTransactional(() => _manager.CreateDriver(driver));          
        }

        [HttpGet]
        public DataContainer<IEnumerable<StateModel>> GetStates()
        {
            return MakeTransactional(() => _manager.GetStates());
        }

        [HttpPost]
        public DataContainer<bool> CreateCarrier(CarrierModel carrier)
        {
            return MakeTransactional(() => _manager.CreateCarrier(carrier));          
        }

        [HttpGet]
        public DataContainer<IEnumerable<MOTModel>> GetMots()
        {
            return MakeTransactional(() => _manager.GetMots());
        }
        [HttpGet]
        public DataContainer<IEnumerable<TransactionModel>> GetShipmentTransactions(long shipmentId,EntityTypeEnum shipmentType)
        {
            return MakeTransactional(() => _manager.GetShipmentTransactions(ApplicationUser.UserId,shipmentId,shipmentType));
        }
        [HttpGet]
        public DataContainer<IEnumerable<AmsCodeModel>> GetAmsCodes(long amsCodeId = 0)
        {
            return MakeTransactional(() => _manager.GetAmsCodes(amsCodeId));
        }

        [HttpGet]
        public DataContainer<WarehouseModel> GetWarehouse(string station)
        {
            return MakeTransactional(() => _manager.GetWarehouse(station));
        }
    }
}