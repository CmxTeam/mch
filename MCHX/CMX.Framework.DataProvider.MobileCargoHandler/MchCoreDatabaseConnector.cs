﻿using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.GlobalEntities;

namespace CMX.Framework.DataProvider.MobileCargoHandler
{
    public abstract class MchCoreDatabaseConnector<T> : EfDatabaseConnector<T, MCHCoreContext>
    {
    }
}