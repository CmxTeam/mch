﻿using System;
using System.IO;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.MobileCargoHandler.DatabaseMapping.Entities;

namespace CMX.Framework.DataProvider.MobileCargoHandler
{
    public abstract class MchDatabaseConnector<T> : EfDatabaseConnector<T, MchDbContext>
    {
        public UserProfile GetAppUser(long userExternalId)
        {
            return this.Context.UserProfiles.SingleOrDefault(e => e.ShellUserId == userExternalId);
        }
    }
}