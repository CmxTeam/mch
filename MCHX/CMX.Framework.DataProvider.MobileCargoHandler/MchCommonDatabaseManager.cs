﻿using System.Linq;
using System.Collections.Generic;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities;
using CMX.Framework.DataProvider.MobileCargoHandler.Entities.SearchModels;
using CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.EfUnits;
using CMX.Framework.DataProvider.MobileCargoHandler.CommonUnits.AdoUnits;

namespace CMX.Framework.DataProvider.MobileCargoHandler
{
    public class MchCommonDatabaseManager
    {
        private ConnectorExecutor _connectorExecutor = new ConnectorExecutor();

        public IEnumerable<DriverModel> GetDrivers(long? driverId)
        {
            return _connectorExecutor.Execute(() => new GetDrivers(driverId));
        }

        public IEnumerable<CarrierModel> GetTruckingCompanies()
        {
            return _connectorExecutor.Execute(() => new GetTruckingCompanies());
        }

        public IEnumerable<IdNameEntity> GetCredentialTypes()
        {
            return _connectorExecutor.Execute(() => new GetCredentialTypes());
        }

        public IEnumerable<DischargeTypeModel> GetDischargeTypes()
        {
            return _connectorExecutor.Execute<IEnumerable<DischargeTypeModel>>(() => new GetDischargeTypes());
        }

        public IEnumerable<PortModel> GetPorts()
        {
            return _connectorExecutor.Execute(() => new GetPorts());
        }

        public IEnumerable<AttachmentModel> GetAttachments(string shipingRef)
        {
            return _connectorExecutor.Execute(() => new GetAttachments(shipingRef));
        }

        public IEnumerable<StatusModel> GetStatuses()
        {
            return _connectorExecutor.Execute(() => new GetStatuses());
        }

        public IEnumerable<StatusModel> GetStatuses(EntityTypeEnum groupId)
        {
            return _connectorExecutor.Execute(() => new GetStatuses(groupId));
        }

        public IEnumerable<StatusModel> GetStatuses(EntityTypeEnum groupId, params long[] ids)
        {
            return _connectorExecutor.Execute(() => new GetStatuses(groupId, ids));
        }

        public IEnumerable<CarrierModel> GetCarriers()
        {
            return _connectorExecutor.Execute(() => new GetCarriers());
        }

        public long GetLocalUserIdByShellId(long shellUserId)
        {
            return _connectorExecutor.Execute(() => new GetLocalUserIdByShellId(shellUserId));
        }
        public ShipmentModel GetShipmentByFilter(ShipmentFilterModel searchModel)
        {
            var shipment = _connectorExecutor.Execute(() => new GetShipmentByFilter(searchModel));
            if (shipment != null)
            {
                shipment.CustomsClearance = _connectorExecutor.Execute(() => new GetShipmentCustomsStatuses(shipment.SerialNumber, shipment.EntityType, false)).FirstOrDefault();
                shipment.CustomsFullHistory = _connectorExecutor.Execute(() => new GetShipmentCustomsStatuses(shipment.SerialNumber, shipment.EntityType, false));
            }
            return shipment;
        }
        public bool CreateDriver(DriverModel driver)
        {
            return _connectorExecutor.Execute(() => new CreateDriver(driver));
        }
        public IEnumerable<StateModel> GetStates()
        {
            return _connectorExecutor.Execute(() => new GetStates());
        }
        public bool CreateCarrier(CarrierModel carrier)
        {
            return _connectorExecutor.Execute(() => new CreateCarrier(carrier));
        }
        public IEnumerable<MOTModel> GetMots()
        {
            return _connectorExecutor.Execute(() => new GetMots());
        }
        public IEnumerable<TransactionModel> GetShipmentTransactions(long userId,long shipmentId,EntityTypeEnum shipmentType)
        {
            return _connectorExecutor.Execute(() => new GetShipmentTransactions(userId,shipmentId,shipmentType));
        }
        public IEnumerable<AmsCodeModel> GetAmsCodes(long amsCodeId = 0)
        {
            return _connectorExecutor.Execute(() => new GetAmsCodes(amsCodeId));
        }

        public WarehouseModel GetWarehouse(string station)
        {
            return _connectorExecutor.Execute(() => new GetWarehouse(station));
        }
    }
}
