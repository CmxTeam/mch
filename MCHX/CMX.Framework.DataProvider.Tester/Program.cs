﻿using Microsoft.Owin.Hosting;
using System;
using System.Configuration;
using System.Linq;

namespace CMX.Framework.DataProvider.Tester
{
    class Program
    {
        private static IDisposable _signalR;
        private static IDisposable _api; 

        static void Main(string[] args)
        {
            var webApiStartupOptions = new StartOptions();
            var signalRStartupOptions = new StartOptions();

            var webApiAddresses = ConfigurationManager.AppSettings["WebApiUrl"];
            var signalrAddresses = ConfigurationManager.AppSettings["SignalRUrl"];

            foreach (var address in webApiAddresses.Split(';'))
                webApiStartupOptions.Urls.Add(address);

            foreach (var address in signalrAddresses.Split(';'))
                signalRStartupOptions.Urls.Add(address);

            WebApiStartup.SwaggerConfig = c =>
            {
                c.SingleApiVersion("v1", "DataProvider API");
                c.UseFullTypeNameInSchemaIds();
                c.ResolveConflictingActions(api => api.First());
            };

            _api = WebApp.Start<WebApiStartup>(webApiStartupOptions);
            _signalR = WebApp.Start<Startup>(signalRStartupOptions);
                       
            Console.WriteLine("Server Connected");

            Console.ReadLine();
            _signalR.Dispose();
            _api.Dispose();

        }
    }
}
