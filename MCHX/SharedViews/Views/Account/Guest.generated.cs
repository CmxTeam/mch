﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Account/Guest.cshtml")]
    public partial class _Views_Account_Guest_cshtml : System.Web.Mvc.WebViewPage<dynamic>
    {
        public _Views_Account_Guest_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 1 "..\..\Views\Account\Guest.cshtml"
  
    Layout = null;

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<!DOCTYPE html>\r\n<html>\r\n<head>\r\n    <meta");

WriteLiteral(" charset=\"utf-8\"");

WriteLiteral(" />\r\n    <meta");

WriteLiteral(" name=\"viewport\"");

WriteLiteral(" content=\"width=device-width, initial-scale=1.0\"");

WriteLiteral(">\r\n    <title>Guest View</title>\r\n");

WriteLiteral("    ");

            
            #line 11 "..\..\Views\Account\Guest.cshtml"
Write(System.Web.Optimization.Styles.Render("~/Content/css"));

            
            #line default
            #line hidden
WriteLiteral("\r\n    <script>");

            
            #line 12 "..\..\Views\Account\Guest.cshtml"
       Write(Html.Raw("window.WebApiUrl='" + System.Configuration.ConfigurationManager.AppSettings["WebApiUrl"] + "'"));

            
            #line default
            #line hidden
WriteLiteral("</script>\r\n</head>\r\n<body>\r\n    <div>Welcome Guest!</div>\r\n    <style>\r\n        ." +
"dhxlayout_cont {\r\n            float: right !important;\r\n            width: 20% !" +
"important;\r\n            margin-left: calc(79%);\r\n        }\r\n    </style>\r\n\r\n    " +
"<script>\r\n");

WriteLiteral("        ");

            
            #line 25 "..\..\Views\Account\Guest.cshtml"
   Write(Html.Raw("window.baseUrl='" + Url.Content("~/") + "'"));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </script>\r\n\r\n");

WriteLiteral("    ");

            
            #line 28 "..\..\Views\Account\Guest.cshtml"
Write(System.Web.Optimization.Scripts.Render("~/bundles/jquery"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 29 "..\..\Views\Account\Guest.cshtml"
Write(System.Web.Optimization.Scripts.Render("~/bundles/dhtmlx"));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n    <script>\r\n        dhtmlx.image_path = \'./Content/imgs/\';                 " +
"    \r\n    </script>\r\n</body>\r\n</html>");

        }
    }
}
#pragma warning restore 1591
