﻿using SharedViews.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SharedViews.Controllers
{
    public abstract class LoginBaseController : Controller
    {
        public abstract bool UseShell { get; }

        public abstract long ApplicationId { get; }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.UseShell = UseShell;
            ViewBag.ApplicationId = ApplicationId;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            var result = Login(false, model.UserName, model.Password, model.RememberMe);
            if (result.Status)
                return Content("OK");
            return Content(result.Message);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult LoginSession(SessionLoginModel model)
        {
            Login(true, string.Empty, model.Session, false);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Guest()
        {
            return View();
        }

        [HttpPost]
        public void LogOff()
        {
            FormsAuthentication.SignOut();
            RemoveCookie("WebApiAuth");
            RemoveCookie("UserName");
            RemoveCookie("Firstname");
            RemoveCookie("Lastname");
            RemoveCookie("Session");
        }

        private StatusModel Login(bool useSession, string userName, string password, bool rememberMe)
        {
            var body = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", "password"), 
                new KeyValuePair<string, string>("Username", userName), 
                new KeyValuePair<string, string>("Password", password),
                new KeyValuePair<string, string>("useShell", UseShell ? "True" : "False"),
                new KeyValuePair<string, string>("useSession", useSession ? "True" : "False"),
                new KeyValuePair<string, string>("appId", ApplicationId.ToString())                
            };

            var content = new FormUrlEncodedContent(body);
            var client = new HttpClient();
            var response = client.PostAsync(ConfigurationManager.AppSettings["WebApiUrl"] + "/token", content).Result;
            var jsonString = response.Content.ReadAsStringAsync().Result;
            
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch 
            {
                return new StatusModel { Status = false, Message = jsonString };
            }

            var json = Newtonsoft.Json.Linq.JObject.Parse(jsonString);
            CreateCookies(new List<KeyValuePair<string, string>> 
                {
                    new KeyValuePair<string, string>("WebApiAuth", json["access_token"].ToString()),
                    new KeyValuePair<string, string>("UserName", json["UserName"].ToString()),
                    new KeyValuePair<string, string>("Firstname", json["Firstname"].ToString()),
                    new KeyValuePair<string, string>("Lastname", json["Lastname"].ToString()),
                    new KeyValuePair<string, string>("Session", json["Session"].ToString())
                }, rememberMe);

            FormsAuthentication.SetAuthCookie(json["UserName"].ToString(), rememberMe);
            Session.Add("ApiAuthentikationToken", json["access_token"].ToString());
            return new StatusModel { Status = true };
        }
        
        private void RemoveCookie(string name)
        {
            var authCookie = string.Format("{0}_{1}:{2}", name, HttpContext.Request.Url.Host, HttpContext.Request.Url.Port);
            if (Request.Cookies[authCookie] != null)
            {
                HttpCookie cookie = Request.Cookies[authCookie];
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }
        }

        private void CreateCookies(IEnumerable<KeyValuePair<string, string>> values, bool rememberMe)
        {
            foreach (var value in values)
            {
                var cookie = new HttpCookie(string.Format("{0}_{1}:{2}", value.Key, HttpContext.Request.Url.Host, HttpContext.Request.Url.Port), value.Value);
                cookie.Expires = rememberMe ? DateTime.Now.AddDays(365) : DateTime.Now.AddDays(1);
                Response.Cookies.Add(cookie);
            }
        }                
    }
}
