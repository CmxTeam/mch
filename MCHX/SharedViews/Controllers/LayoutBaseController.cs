﻿using System.Linq;
using System.Web.Mvc;
using SharedViews.Attributes;

namespace SharedViews.Controllers
{
    public abstract class LayoutBaseController : Controller
    {       
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var layouTypeAttribute = filterContext.ActionDescriptor.GetCustomAttributes(typeof(LayoutTypeAttribute), false).SingleOrDefault();
            if (layouTypeAttribute != null)
                ViewBag.LayoutType = ((LayoutTypeAttribute)layouTypeAttribute).LayoutType;
            
            ViewBag.ApplicationName = ApplicationName;
            
            base.OnActionExecuting(filterContext);
        }
       
        public abstract string ApplicationName { get; }
    }
}
