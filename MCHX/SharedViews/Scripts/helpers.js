﻿if (typeof (window.helpers) == "undefined") {
    window.helpers = {
        _getCookie: function (cname) {
            var name = cname + '_' + window.location.host + ((window.location.port == "80" || window.location.port == "") ? ":80" : "") + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        },

        _deleteAuthCookie: function () {
            if (this._getCookie('WebApiAuth')) {
                var name = 'WebApiAuth' + '_' + window.location.host + ((window.location.port == "80" || window.location.port == "") ? ":80" : "");
                document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT";
            }
        },

        getUserName: function () {
            return this._getCookie('UserName');
        },

        downloadFile: function (base64, fileName) {
            var a = document.createElement("a");
            a.href = base64;
            a.download = fileName || 'ExportedFile';
            document.body.appendChild(a);
            a.click();
            setTimeout(function () { document.body.removeChild(a); }, 100);
        },

        exportDivAsImage: function (divForExport, fileName) {
            html2canvas($(divForExport), {
                onrendered: function (canvas) {
                    helpers.downloadFile(canvas.toDataURL("image/png", 1.0), "ImageFile.jpg");
                }
            });
        },

        exportToExcel: function (url, filter, fileName, callback, method) {
            $.ajax({
                method: method || 'GET',
                url: WebApiUrl + url,
                data: filter,
                headers: {
                    'Authorization': "Bearer " + this._getCookie('WebApiAuth')
                },
                success: function (data) {
                    var dataType = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,";
                    if (data.Status != null && data.Status.Status == false) {
                        if (data.Data) {
                            dhtmlx.confirm({
                                title: "Warning",
                                ok: "FILTER MORE",
                                type: "exportconfirmation",
                                cancel: "DOWNLOAD ANYWAY",
                                text: "Your Search has exceeded the size limit for downloading an excel. You will receive only part of your result",
                                callback: function (result) {
                                    if (result == false) {
                                        if (fileName && fileName.trim().lastIndexOf(".xlsx") != fileName.trim().length - 5)
                                            fileName += ".xlsx";
                                        helpers.downloadFile(dataType + data.Data, fileName || "ExcelFile.xlsx");
                                    }
                                }
                            });
                        } else {
                            dhtmlx.alert({
                                title: "Warning",
                                type: "warning",
                                text: data.Status.Message || "Something went wrong, please try again later!"
                            });
                        }
                    } else {
                        if (fileName && fileName.trim().lastIndexOf(".xlsx") != fileName.trim().length - 5)
                            fileName += ".xlsx";
                        helpers.downloadFile(dataType + (data.Data || data), fileName || "ExcelFile.xlsx");
                    }
                    if (callback) {
                        callback(data);
                    }
                },
                error: function (error) {
                    helpers.showAjaxLoadingError(error);
                    if (callback) {
                        callback(error);
                    }
                }
            });
        },

        executePost: function (url, data, callback, authToken) {
            $.ajax({
                type: 'POST',
                url: WebApiUrl + url,
                data: data,
                dataType: 'json',
                headers: {
                    'Authorization': "Bearer " + (authToken || this._getCookie('WebApiAuth'))
                },
                success: function (data) {
                    if (callback) {
                        callback(data);
                    }
                },
                error: function (error) {
                    helpers.showAjaxLoadingError(error);
                    if (callback) {
                        callback(error);
                    }
                }
            });
        },

        reminder: function (interval, repeat, text, yesCallback, noCallback, timerElapsed) {
            var timer = setTimeout(function () {
                var element = dhtmlx.confirm({
                    type: "confirm",
                    text: text,
                    callback: function (result) {
                        if (result) {
                            clearTimeout(timer);

                            if (yesCallback) {
                                yesCallback();
                            }

                            if (repeat) {
                                console.log('repeat');
                                this.reminder(interval, repeat, text, yesCallback, noCallback, timerElapsed);
                            }
                        } else {
                            if (noCallback) {
                                noCallback();
                            }
                        }
                    }.bind(this)
                });

                if (timerElapsed) {
                    timerElapsed(element, timer);
                }
            }.bind(this), interval * 1000);
            return timer;
        },

        documentCapturerControl: function (documentTypesUrl, callback) {
            var windows = new dhtmlXWindows();
            var captureWindow = windows.createWindow('captureWindow', 0, 0, 600, 380);
            var form = null;
            var formStructure = [
                {
                    type: "input", name: "FileName", label: "File Name", position: "label-top", inputWidth: 170, required: true, className: "req-asterisk",
                    validate: function (input) { return input && input.trim() !== "" }
                },

                { type: "newcolumn" },

                {
                    type: "combo", name: "FileType", label: "File Type", position: "label-top", inputWidth: 200, offsetLeft: 10, required: true, className: "req-asterisk",
                    validate: function (input) { return form.getCombo("FileType").getSelectedIndex() !== -1 }
                }
            ];

            form = captureWindow.attachForm(formStructure);
            form.enableLiveValidation(true);
            var fileType = form.getCombo('FileType');
            fileType.loadExternal(documentTypesUrl, 'Id', 'Name');

            fileType.attachEvent("onChange", function (id) {
                var option = this.getOption(id);
                form.setItemValue('FileName', option ? option.customData.item.Name : '')
            });

            captureWindow.show();
            captureWindow.setText("Capture");
            captureWindow.setModal(1);
            captureWindow.denyResize();
            captureWindow.denyPark();
            captureWindow.centerOnScreen();

            var container = document.createElement('DIV');
            container.setAttribute('style', 'height: 80%; width: 100%;');

            var video = document.createElement('VIDEO');
            video.setAttribute('style', 'max-width: 48%; height: 95%; float: left;');

            var image = document.createElement('IMG');
            image.setAttribute('style', 'width: 274px; height: 206px; margin-top: 21px; float: right; border-style: solid; border-width: 0px; border-color: red;');

            container.appendChild(video);
            container.appendChild(image);

            var controlsContainer = document.createElement('DIV');
            controlsContainer.setAttribute('style', 'display:inline-block; float: right; padding-top: 27px;');

            var captureBtn = document.createElement('DIV');
            captureBtn.innerHTML = 'Capture';
            captureBtn.setAttribute('style', 'margin-right:10px; color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            var acceptBtn = document.createElement('DIV');
            acceptBtn.innerHTML = 'Accept';
            acceptBtn.setAttribute('style', 'color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            controlsContainer.appendChild(acceptBtn);
            controlsContainer.appendChild(captureBtn);

            captureWindow.cell.firstChild.firstChild.insertBefore(controlsContainer, captureWindow.cell.firstChild.firstChild.firstChild);
            captureWindow.cell.firstChild.firstChild.insertBefore(container, captureWindow.cell.firstChild.firstChild.firstChild);

            var mediaHandler = new MediaHandler();
            mediaHandler.start(video, image);

            captureWindow.attachEvent('onClose', function () {
                mediaHandler.stop();
                return true;
            });

            captureBtn.onclick = function () {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");
                var cropHeight = video.clientHeight;
                var cropWidth = video.clientWidth;
                canvas.width = cropWidth;
                canvas.height = cropHeight;
                context.drawImage(video, 0, 0, cropWidth, cropHeight);
                image.src = canvas.toDataURL("image/jpg");
                image.style.borderWidth = "0px";
            }

            acceptBtn.onclick = function () {
                if (form.validate() && image.src) {
                    if (callback) {
                        var data = form.getFormData();
                        data.image = image.src;
                        callback.call(this, data);
                    }
                    captureWindow.close();
                }
                if (image.src === "")
                    image.style.borderWidth = "1px";
            }.bind(this);
        },

        deepValue: function (obj, path) {
            if (obj != null) {
                for (var i = 0, path = path.split('.'), len = path.length; i < len; i++) {
                    obj = obj[path[i]];
                };
            }
            return obj;
        },

        getDataFromExternalResource: function (url, params, callback) {
            $.ajax({
                type: 'GET',
                url: WebApiUrl + url,
                data: params,
                headers: {
                    'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
                },
                success: function (data) {
                    if (data){
                        data = data.Data || data;
                    }
                    if (callback) {
                        callback(data);
                    }
                },
                error: function (message) {
                    helpers.showAjaxLoadingError(message);
                }
            });
        },

        showAjaxLoadingError: function (error) {
            if (error.status == 401) {
                dhx4.ajax.post('../Account/LogOff', function (a) {
                    helpers._deleteAuthCookie();
                    window.location.href = window.location.origin + window.baseUrl + 'Account/Login';
                });
                return;
            }

            dhtmlx.alert({
                title: "Warning",
                type: "warning",
                text: "Something went wrong, please try again later!"
            });
        },

        popupImage: function (imageSource, height, width, imageAutosize) {
            if (imageSource && imageSource != "") {
                var windows = new dhtmlXWindows();
                var imageWindow = windows.createWindow('imagePopup', 0, 0, width || 310, height || 310);
                imageWindow.show();
                imageWindow.setText("Image");
                imageWindow.setModal(1);
                imageWindow.denyResize();
                imageWindow.denyPark();
                imageWindow.centerOnScreen();

                var formStructure = [
                    { type: "container", name: "imageContainer", offsetLeft: 0, offsetTop: 0 }
                ];

                var form = imageWindow.attachForm(formStructure);

                var imageContainer = form.getContainer("imageContainer");
                if (imageAutosize)
                    imageContainer.innerHTML = '<img style="width:auto;max-height:100%;max-width:100%" src="' + imageSource + '">';
                else
                    imageContainer.innerHTML = '<img style="width:auto;height:auto" src="' + imageSource + '">';
            }
        },

        uploaderFormTemplate: function (name, value) {
            var form = this.getForm();
            $(this).on("change", ".customUpload", (function (fileEv) {
                if (!fileEv)
                    return;

                if (fileEv.target.files[0]) {

                    var reader = new FileReader();

                    reader.onload = (function (a) {
                        console.log(a.target.result);

                        form.setItemValue(name, a.target.result);
                        $($(form._getItemByName(name)).find("input")[0]).attr('value', fileEv.target.files[0].name);
                    });

                    reader.readAsDataURL(fileEv.target.files[0]);
                }
            }));

            $(this).on("keypress", ".customFileUploadBtn", function (ev) {
                if (ev && ev.keyCode === 13 && $(".customUpload"))
                    $(".customUpload").click();
                else
                    return;
            });

            return '<div class="customFileUpload btn btn-primary">' +
                        '<input class="dhxform_textarea customUploadText" placeholder="Select File" readonly="true">' +
                        '<div class="dhxform_btn customFileUploadBtn" role="link" tabindex="0">' +
                            '<div class="dhxform_btn_txt">Select File</div>' +
                            '<div class="dhxform_btn_filler" disabled="true"></div>' +
                        '</div>' +
                        '<input type="file" class="customUpload">' +
                    '</div>';
        }
    }
}

dhtmlXToolbarObject.prototype.initQuikSearch = function (grid) {
    this.addInput("quickSearchInput", 0, "", 225);
    this.getInput("quickSearchInput").placeholder = "Find";

    var searchInput = $(this.getInput('quickSearchInput'));
    var wrapper = $('<div class="toolbar-search-input-wrapper"><span></span><span class="up"></span><span class="down"></span></div>');
    searchInput.wrap(wrapper);

    searchInput.on('keyup', function (e) {
        var text = $(this).val();
        grid.highlightCellsByValue(text);
    });

    grid.attachEvent("onClearAll", function () {
        searchInput.val("");
    });

    $(this.cont).on('click', ".toolbar-search-input-wrapper > .up", function () {
        grid.selectPreviouseHighlightedRow();
    });

    $(this.cont).on('click', ".toolbar-search-input-wrapper > .down", function () {
        grid.selectNextHighlightedRow();
    });
}

dhtmlXGridObject.prototype.getSelectedIdsAsArray = function () {
    var selectedIdArray = [];
    var selectedIdsString = this.getSelectedRowId();
    if (selectedIdsString) {
        selectedIdsString.split(",").forEach(function (value) {
            if (!isNaN(value))
                selectedIdArray.push(new Number(value).valueOf());
        });
    }
    return selectedIdArray;
}

dhtmlXGridObject.prototype.highlightCellsByValue = function (value) {
    var grid = this;
    this.quickSearchedCount = 0;
    $.each(this.rowsBuffer, function (i, currentRow) {
        $(currentRow).find('td').css({ backgroundColor: '' });
        if (value && currentRow) {
            $(currentRow.cells).each(function (index) {
                if (this.textContent.toUpperCase().indexOf(value.toUpperCase()) !== -1 && this.style.display !== 'none') {
                    $(this).css({
                        backgroundColor: 'deepskyblue'
                    });
                    grid.quickSearchedCount = grid.quickSearchedCount + 1;
                }
            });
        }
    });
}

dhtmlXGridObject.prototype.selectNextHighlightedRow = function (value) {
    var grid = this;
    var selectedIndex = grid.getRowIndex(grid.getSelectedRowId());
    $.each(grid.rowsBuffer, function (i, currentRow) {
        if (i > selectedIndex && currentRow) {
            $(currentRow.cells).each(function (index) {
                if (this.style.backgroundColor !== '') {
                    grid.selectRowById(currentRow.idd);
                    return false;
                }
            });

            if (grid.getRowIndex(grid.getSelectedRowId()) != selectedIndex)
                return false;
        }
    });
}

dhtmlXGridObject.prototype.selectPreviouseHighlightedRow = function (value) {
    var grid = this;
    var selectedIndex = grid.getRowIndex(grid.getSelectedRowId());
    var currentRow = null;
    for (var j = selectedIndex - 1; j >= 0; j--) {
        currentRow = grid.rowsBuffer[j];
        if (currentRow) {
            $(currentRow.cells).each(function (index) {
                if (this.style.backgroundColor !== '') {
                    grid.selectRowById(currentRow.idd);
                    return false;
                }
            });
        }

        if (grid.getRowIndex(grid.getSelectedRowId()) != selectedIndex)
            return false;
    }
}

dhtmlXGridObject.prototype.makePageable = function (pageSize) {
    this._loadedPages = [0];
    this._pageSize = pageSize || 30;

    var _paging = function (pageNumber, count) {
        this.filters = this.filters || {};
        this.filters.pageNumber = pageNumber;
        this.filters.count = count;
        this.loadExternal(this._externalUrl, this.filters);
    }

    this.attachEvent("onScroll", function (sLeft, sTop) {
        var gridPosition = this.getStateOfView();
        var count = gridPosition[1] > this._pageSize ? gridPosition[1] : this._pageSize;
        var pageNumber = parseInt(gridPosition[0] / count) + 1;
        if (this._loadedPages.indexOf(pageNumber) === -1) {
            _paging.call(this, pageNumber, count);

            $("#" + this.entBox.id).find('.smartRenderProgress').remove();
            $("#" + this.entBox.id).append("<div class='smartRenderProgress dhx_cell_progress_img' style='width:35px;height:35px; top:calc(100% - 53px); display:block; position:absolute; left: calc(100% - 53px);'></div>");

            this._loadedPages.push(pageNumber);
        }
    }.bind(this));

    this.attachEvent("onXLE", function () {
        $("#" + this.entBox.id).find('.smartRenderProgress').remove();
    }.bind(this));
}

dhtmlXTreeObject.prototype.loadExternal = function (url, filters, callback) {
    this._externalUrl = url;
    this.filters = filters || {};

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
        },
        success: function (result) {
            this.deleteChildItems(0);
            this.parse(result, "xml");
            if (callback) {
                callback();
            }
        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXGridObject.prototype.setGridDataFromModel = function (data, modelType) {
    var grid = this;
    var gridData = {
        rows: []
    };
    if (data != null) {
        data.forEach(function (item) {
            var gridItem = {
                id: item.Id || new Date().toString(),
                data: []
            };
            var columnIndex = 0;
            $.each(modelType, function (key, dateTimeFormat) {
                var value = helpers.deepValue(item, key);
                if (dateTimeFormat !== "" && Date.parse(value))
                    value = window.dhx4.date2str(new Date(value), dateTimeFormat);
                if (grid.getColType(columnIndex) == "img")
                    gridItem.data.push(window.baseUrl + value || "");
                else
                    gridItem.data.push(value || "");
                columnIndex += 1;
            });
            gridData.rows.push(gridItem);
        });

        grid.parse(gridData, 'json');
        data.forEach(function (item) {
            grid.setUserData(item.Id, "CustomRowData", item);
        });
    }
}
dhtmlXGridObject.prototype.loadExternalByModel = function (url, filters, modelType, callback) {
    this._externalUrl = url;
    this.filters = filters || {};
    this.filters.pageNumber = this.filters.pageNumber || 0;
    this.filters.count = this.filters.count || this._pageSize || 10000;

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
        },
        success: function (result) {
            var gridData = {
                rows: []
            };
            if (result != null && result.Data) {
                this.setGridDataFromModel(result.Data, modelType);
            }

            if (callback) {
                callback();
            }
        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXDataView.prototype.loadExternal = function (url, filters, callback) {
    this._externalUrl = url;
    this.filters = filters || {};
    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
        },
        success: function (result) {
            this.clearAll();
            this.parse(result.Data || result, 'json');
            if (callback) {
                callback(result.Data || result)
            }
        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXGridObject.prototype.showTotal = function () {
    var grid = this;
    grid.attachFooter(
    [
        "<div id='grid_recinfoArea' style='padding-left: 10px;color: white;font-weight: bold;width:100%;height:100%'>0 of 0</div>", "#cspan", "#cspan", "#cspan",
        "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan",
        "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan", "#cspan"
    ],
    ['height:25px;width:100%;text-align:left;background:#3da0e3;border-color:white;padding:0px;']);

    grid.attachEvent("onXLE", function () {
        var renderedTotal = grid.getAllRowIds() != "" ? $.unique($(grid.getAllRowIds().split(','))).length : 0;
        var serverTotal = parseInt(renderedTotal) > parseInt(grid.serverTotalCount) ? renderedTotal : grid.serverTotalCount;
        grid.setFooterLabel(0, "<div id='grid_recinfoArea' style='padding-left: 10px;color: white;font-weight: bold;width:100%;height:100%'> " +
            renderedTotal + " " +
            "of " + serverTotal + " </div>");
    });
}

dhtmlXGridObject.prototype.loadExternal = function (url, filters, callback) {
    this._externalUrl = url;
    this.filters = filters || {};
    this.filters.pageNumber = this.filters.pageNumber || 0;
    this.filters.count = this.filters.count || this._pageSize || 10000;

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
        },
        success: function (data) {
            if (data) {
                data = data.Data || data;
                this.serverTotalCount = parseInt(data.total_count) || 0;
                this.forEachRow(function (id) {
                    data.rows.forEach(function (item) {
                        if (id == item.id) {
                            this.deleteRow(id);
                        }
                    }.bind(this));
                }.bind(this));

                this.parse(data, 'json');
                data.rows.forEach(function (item) {
                    var rowData = this.getUserData(item.id, "CustomRowData");
                    if (rowData == null || rowData == "") {
                        this.setUserData(item.id, "CustomRowData", item);
                    }
                }.bind(this));
            }

            if (callback) {
                callback(data);
            }
        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXChart.prototype.showAllSeries = function (isShow) {
    if (this.config.legend || isShow) {
        try {
            for (var i = 0; i < this._series.length; i++) {
                this.showSeries(i);
            }
        } catch (e) { }
    }
}

dhtmlXChart.prototype.loadExternal = function (url, filters, onBeforeRender, onAfterRender) {
    this._externalUrl = url;
    this.filters = filters || {};

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
        },
        success: function (result) {
            if (onBeforeRender) {
                onBeforeRender(result);
            }

            var convertedData = [];
            if (result != null && result.Data) {
                for (var i = 0; i < result.Data.length; i++) {
                    var tmpObj = {};
                    $.each(result.Data[i], function (key, value) {
                        tmpObj[key.replace(/ /g, '')] = value;
                    });
                    convertedData.push(tmpObj);
                }
            }
            this.clearAll();
            this.parse(convertedData, 'json');
            this.showAllSeries();

            if (onAfterRender) {
                onAfterRender(result);
            }

        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXDataView.prototype.getByDbId = function (Id) {
    var foundedItem = null;
    this.serialize().forEach(function (item) {
        if (item.Id == Id) {
            foundedItem = item;
            return false;
        }
    });
    return foundedItem;
}

dhtmlXCombo.prototype.getAllCheckedIds = function () {
    var checkedIdList = this.getChecked();
    var index = checkedIdList.indexOf("UncheckAll");
    if (index > -1) {
        checkedIdList.splice(index, 1);
    }
    return checkedIdList;
}

dhtmlXCombo.prototype.checkUncheckAll = function (isChecked) {
    this.forEachOption(function (optId) {
        this.setChecked(this.getIndexByValue(optId.value), isChecked);
    }.bind(this));
    this.unSelectOption();
}

dhtmlXCombo.prototype.makeMultiSelect = function () {
    var combo = this;

    var generateMultiSelectComboInputText = function () {
        var inputText = "";
        combo.forEachOption(function (option) {
            if (option.checked && option.value != "UncheckAll") {
                inputText = inputText + (inputText !== "" ? ", " : "") + option.text;
            }
        });

        combo.setChecked(combo.getIndexByValue("UncheckAll"), combo.getOptionsCount() - combo.getAllCheckedIds().length == 1);
        combo.setComboText(inputText);
    }
    combo.readonly(true);

    combo.attachEvent("onOpen", function () {
        if (combo.getOptionsCount() > 1 && combo.getIndexByValue("UncheckAll") == -1) {
            combo.addOption([
                ["UncheckAll", "(Select All)"]
            ]);
            combo.setOptionIndex("UncheckAll", 0);
        }
    });

    combo.attachEvent("onCheck", function (value, state) {
        var uncheckIndex = combo.getIndexByValue("UncheckAll");
        if (value == "UncheckAll" && uncheckIndex != -1) {
            combo.checkUncheckAll(combo.isChecked(uncheckIndex));
        }

        combo.setChecked(uncheckIndex, combo.getOptionsCount() - combo.getAllCheckedIds().length == 1);
        generateMultiSelectComboInputText();
        combo.openSelect();
    });

    combo.attachEvent("onClose", function () {
        if (combo.getSelectedValue() === "UncheckAll") {
            var uncheckIndex = combo.getIndexByValue("UncheckAll");
            if (uncheckIndex != -1) {
                combo.setChecked(uncheckIndex, !combo.isChecked(uncheckIndex));
                combo.checkUncheckAll(combo.isChecked(uncheckIndex));
            }
        } else {
            var index = combo.getIndexByValue(combo.getSelectedValue());
            combo.setChecked(index, !combo.isChecked(index));
        }

        generateMultiSelectComboInputText();
    });
}

dhtmlXCombo.prototype.loadExternal = function (url, dataKeyField, dataTextField, callback, params) {
    this.unSelectOption();
    this.clearAll();
    this._externalUrl = url
    this.filters = params || {};
    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + helpers._getCookie('WebApiAuth')
        },
        success: function (data) {
            if (data.Status && (!data.Status.Status || (data.Status.Status && data.Data === null))) {
                //show error message. not sure for combo
                return;
            }

            var comboData = $.map(data.Data || data, function (item) {
                return { value: helpers.deepValue(item, dataKeyField), text: helpers.deepValue(item, dataTextField), item: item }
            });

            this.load(comboData, function () {
                if (callback)
                    callback(comboData);
            });

        }.bind(this)
    });
}

dhtmlXMenuObject.prototype.loadExternal = function (url, params, callback) {
    helpers.getDataFromExternalResource.call(this, url, params, function (data) {
        this.loadStruct(data);
        if (callback) {
            callback();
        }
    }.bind(this));
}

dhtmlXForm.prototype.items.imageCapturer = {
    render: function (item, data) {
        item._type = "imageCapturer";
        item._enabled = true;
        item._id = data.id;
        item._useExternalButton = data.useExternalButton || false;

        item.setAttribute('style', 'height: ' + data.controlHeight);

        var image = document.createElement("IMG");
        image.setAttribute('id', data.id);
        image.setAttribute('style', 'height: ' + data.imageHeight + '; width: ' + data.imageWidth + ';');

        var button = this._createButton.call(item, data.buttonText, item._useExternalButton);
        var label = this._createLabel.call(item, data.labelText, data.labelWidth);

        var controlsContainer = document.createElement("DIV");

        controlsContainer.appendChild(label);
        controlsContainer.appendChild(button);
        item.appendChild(controlsContainer);
        item.appendChild(image);
        item.image = image;

        image.ondblclick = function () {
            helpers.popupImage(image.src, 330, 310);
        }

        return this;
    },

    destruct: function (item) { }, //destructor
    setValue: function (item, value) {
        if (value == "")
            $(item.image).removeAttr("src");
        else
            item.image.src = value;
    },
    getValue: function (item) {
        return item.image.src;
    },
    enable: function (item) { },
    disable: function (item) { },

    _setImageSrc: function () {
        console.log(this.id);
    },

    _createLabel: function (text, width) {
        var labelContainer = document.createElement('DIV');
        labelContainer.setAttribute('style', 'display: inline-block; float:left;');
        labelContainer.setAttribute('class', 'dhxform_item_label_top');

        var labelText = document.createElement('DIV');
        labelText.setAttribute('class', 'dhxform_txt_label2 topmost');
        labelText.setAttribute('style', 'width: ' + width);
        labelText.innerHTML = text;
        labelContainer.appendChild(labelText);
        return labelContainer;
    },

    _createButton: function (text) {
        var buttonContainer = document.createElement('DIV');
        buttonContainer.setAttribute('dir', 'ltr');
        buttonContainer.setAttribute('role', 'link');
        buttonContainer.setAttribute('class', 'dhxform_btn');
        buttonContainer.setAttribute('style', 'display: inline-block; float: none; cursor: pointer;');


        var buttonText = document.createElement('DIV');
        buttonText.setAttribute('class', 'dhxform_btn_txt');
        buttonText.innerHTML = text;

        var buttonFilter = document.createElement('DIV');
        buttonFilter.setAttribute('disabled', 'true');
        buttonFilter.setAttribute('class', 'dhxform_btn_filler');

        buttonContainer.appendChild(buttonText);
        buttonContainer.appendChild(buttonFilter);

        buttonContainer.onclick = function () {
            var windows = new dhtmlXWindows();
            var captureWindow = windows.createWindow('captureWindow', 0, 0, 600, 380);
            captureWindow.show();
            captureWindow.setText("Capture");
            captureWindow.setModal(1);
            captureWindow.denyResize();
            captureWindow.denyPark();
            captureWindow.centerOnScreen();

            var container = document.createElement('DIV');
            container.setAttribute('style', 'height: 88%; width: 100%;');

            var video = document.createElement('VIDEO');
            video.setAttribute('style', 'max-width: 48%; height: 95%; float: left;');

            var image = document.createElement('IMG');
            image.setAttribute('style', 'width: 274px; height: 206px; margin-top: 34px; float: right;');

            container.appendChild(video);
            container.appendChild(image);

            var controlsContainer = document.createElement('DIV');
            controlsContainer.setAttribute('style', 'height: 12%; width: 100%;');

            var captureBtn = document.createElement('DIV');
            captureBtn.innerHTML = 'Capture';
            captureBtn.setAttribute('style', 'margin-right:10px; color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            var acceptBtn = document.createElement('DIV');
            acceptBtn.innerHTML = 'Accept';
            acceptBtn.setAttribute('style', 'color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            controlsContainer.appendChild(acceptBtn);
            controlsContainer.appendChild(captureBtn);

            captureWindow.cell.firstChild.appendChild(container);
            captureWindow.cell.firstChild.appendChild(controlsContainer);

            var mediaHandler = new MediaHandler();
            mediaHandler.start(video, image);

            captureWindow.attachEvent('onClose', function () {
                mediaHandler.stop();
                return true;
            });

            captureBtn.onclick = function () {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");
                var cropHeight = video.clientHeight;
                var cropWidth = video.clientWidth;
                canvas.width = cropWidth;
                canvas.height = cropHeight
                context.drawImage(video, 0, 0, cropWidth, cropHeight);
                image.src = canvas.toDataURL("image/jpg");
            }

            acceptBtn.onclick = function () {
                this.image.src = image.src;
                captureWindow.close();
            }.bind(this);
        }.bind(this);

        return buttonContainer;
    }
}

function submitForm(title, formStructure, submitCallback, width, height, buttonName) {
    var wndWidth = width ? width : 400;
    var wndHeight = height ? height : 400;
    this.onSubmit = submitCallback;

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('submitFormWindow', 0, 0, wndWidth, wndHeight);

        var tmpFormStructure = formStructure.slice();
        tmpFormStructure.push({
            type: 'block',
            list: [
                { type: "button", name: "submitButton", value: buttonName || "SUBMIT", offsetLeft: wndWidth - 132, offsetTop: 15 }
            ]
        });
        var form = window.attachForm(tmpFormStructure);

        form.attachEvent("onButtonClick", function () {
            var formData = form.getFormData();
            if (form.validate()) {
                window.progressOn();
                if (this.onSubmit) {
                    this.onSubmit(formData);
                }
            }
        }.bind(this));

        window.setText(title);
        window.setModal(1);
        window.denyResize();
        window.centerOnScreen();
        this.form = form;
        this.window = window;
    }.apply(this);
};