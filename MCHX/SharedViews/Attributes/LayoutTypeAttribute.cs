﻿using System;

namespace SharedViews.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Method, AllowMultiple = false)]
    public class LayoutTypeAttribute : Attribute
    {
        public LayoutTypeAttribute(string layoutType)
        {
            LayoutType = layoutType;
        }

        public string LayoutType { get; private set; }
    }
}