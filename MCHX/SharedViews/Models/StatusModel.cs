﻿namespace SharedViews.Models
{
    public class StatusModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}