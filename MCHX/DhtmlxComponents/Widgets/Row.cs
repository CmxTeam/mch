﻿using System.Collections.Generic;

namespace DhtmlxComponents.Widgets
{
    public class Row
    {
        public string id { get; set; }
        public IEnumerable<string> data { get; set; }
    }
}