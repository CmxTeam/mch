﻿namespace DhtmlxComponents.Widgets
{
    public class ExtendedMenuItem<T> : MenuItem
    {
        private T _userdata;

        public T userdata
        {
            get { return _userdata; }
            set { _userdata = value; }
        }
    }
}
