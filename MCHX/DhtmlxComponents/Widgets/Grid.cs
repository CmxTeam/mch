﻿using System.Collections.Generic;

namespace DhtmlxComponents.Widgets
{
    public class Grid
    {
        public string total_count { get; set; }
        public int pos { get; set; }
        public IEnumerable<Row> rows { get; set; }
    }

    public class DxGrid<T> where T : class
    {
        public string total_count { get; set; }
        public int pos { get; set; }
        public IEnumerable<T> rows { get; set; }
    }
}