﻿using System.Collections.Generic;

namespace DhtmlxComponents.Widgets
{
    public class TreeItem
    {
        public string id { get; set; }
        public string text { get; set; }
        public IEnumerable<TreeItem> item { get; set; }
    }
}