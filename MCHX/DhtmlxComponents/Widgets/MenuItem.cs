﻿using System.Collections.Generic;

namespace DhtmlxComponents.Widgets
{
    public class MenuItem
    {
        private bool _enabled = true;

        public string id { get; set; }
        public string text { get; set; }
        public string type { get; set; }
        public string img { get; set; }
        public string imgdis { get; set; }
        public string href_link { get; set; }
        public string href_target { get; set; }
        public bool enabled { get { return _enabled; } set { _enabled = value; } }
        public IEnumerable<MenuItem> items { get; set; }

        public class MenuItemTypes
        {
            public const string Item = "item";
            public const string Checkbox = "checkbox";
            public const string Radio = "radio";
        }
    }    
}