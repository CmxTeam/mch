﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApiTester.Controllers
{
    [Authorize]
    public class AccountController : SharedViews.Controllers.LoginBaseController
    {
        public override bool UseShell
        {
            get { return true; }
        }   
    }
}