﻿using SharedViews.Attributes;
using SharedViews.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApiTester.Controllers
{
    public class HomeController : LayoutBaseController
    {
        [Authorize]
        [LayoutTypeAttribute("1C")]
        public ActionResult Index()
        {
            return View();
        }        
    }
}