﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Export.CargoManifest
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Script/jquery-{version}.js"));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/dhtmlx.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/custom")
                        .Include("~/Script/helpers.js")
                        .Include("~/Script/common.js")
                        .Include("~/Script/functions.js")
                        .Include("~/Script/main_tab.js")
                        .Include("~/Script/nav_tree.js")
                        .Include("~/Script/temporary_tab.js")
                        .Include("~/Script/assign_consolidation.js")
                        .Include("~/Script/work_area.js")
                        .Include("~/Script/change_destination.js")
                        .Include("~/Script/download_shipments.js")
                        .Include("~/Script/hold_shipment.js")
                        .Include("~/Script/import_shipments.js")
                        .Include("~/Script/master_house.js")
                        .Include("~/Script/print.js")

                        .Include("~/Script/shipment_details/charges.js")
                        .Include("~/Script/shipment_details/damages.js")
                        .Include("~/Script/shipment_details/hb_overview.js")
                        .Include("~/Script/shipment_details/index.js")
                        .Include("~/Script/shipment_details/locations.js")
                        .Include("~/Script/shipment_details/pcs_dims.js")
                        .Include("~/Script/shipment_details/references.js")
                        .Include("~/Script/shipment_details/remarks.js")

                        .Include("~/Script/appearance_settings/index.js")
                        .Include("~/Script/appearance_settings/left_tree_view_settings.js")
                        .Include("~/Script/appearance_settings/view_weight_grid_settings.js")
                        .Include("~/Script/appearance_settings/view_weight_settings.js")

                        .Include("~/Script/masterbill_details.js")
                        );

            bundles.Add(new ScriptBundle("~/bundles/dhtmlx").Include(
                        "~/Script/dhtmlx.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageCapturer").Include(
                        "~/Script/image-capture.js"));

        }
    }
}