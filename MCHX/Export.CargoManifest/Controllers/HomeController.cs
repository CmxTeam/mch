﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SharedViews.Attributes;
using SharedViews.Controllers;

namespace Export.CargoManifest.Controllers
{
    public class HomeController : LayoutBaseController
    {
        [Authorize]
        [LayoutType("2U")]
        public ActionResult Index()
        {
            return View();
        }
    }
}