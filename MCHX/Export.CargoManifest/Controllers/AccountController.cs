﻿using System.Web.Mvc;
using SharedViews.Controllers;

namespace Export.CargoManifest.Controllers
{
    [Authorize]
    public class AccountController : LoginBaseController
    {
        public override bool UseShell
        {
            get { return false; }
        }
    }
}