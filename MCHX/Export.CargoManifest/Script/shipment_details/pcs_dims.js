var pcsTabs = {};

var pcsTab = function (tab, data) {

	var _this = this;
	this.tab = tab;
	this.tabData = data;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{id: 'housebill_dims', type: 'text', text: '<span class="bold">Housebill Dims</span>', width: 200},
					{type: 'separator'},
					{id: 'housebill_no', type: 'text', text: '<span class="bold">Housebill No: ' + this.tabData.origin + '-' + this.tabData.airbillNo + '</span>'}
				]
			}
		);

		//init grid

		this.grid = this.tab.attachGrid();
		this.grid.setIconPath(dhtmlx.image_path);
		this.grid.setHeader(['Item No.', 'Length', 'Width', 'Height', 'UOM']);
		this.grid.setColSorting('int,int,int,int,str');
		this.grid.setColAlign('center,center,center,center,center');
		this.grid.setColTypes('ro,ro,ro,ro,ro');
		this.grid.setInitWidths('*,*,*,*,*');
		this.grid.init();
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/GetDimensions?airbillNo=' + this.tabData.airbillNo + '&origin=' + this.tabData.origin, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			_this.grid.clearAll();
			if (data && data.Status && data.Status.Status) {
				_this.grid.parse(data.Data, 'json');
			}
			_this.tab.progressOff();
		});
	};

	this.clearAll = function () {
		_this = null;
	};

	this.init();
	return this;
};

dhx4.attachEvent('loadPCSTabData', function (tab, data) {
	if (tab && !pcsTabs[tab.getId()]) {
		pcsTabs[tab.getId()] = new pcsTab(tab, data);
	}
	pcsTabs[tab.getId()].load(data);
});
dhx4.attachEvent('clearPCSData', function (tab) {
	if (tab && pcsTabs && pcsTabs[tab.getId()]) {
		pcsTabs[tab.getId()].clearAll();
		pcsTabs[tab.getId()] = null;
		delete pcsTabs[tab.getId()];
	}
});