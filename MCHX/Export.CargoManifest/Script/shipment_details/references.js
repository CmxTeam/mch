var referencesTabs = {};

var referencesTab = function (tab, data) {

	var _this = this;
	this.tab = tab;
	this.tabData = data;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{id: 'housebill_references', type: 'text', text: '<span class="bold">Housebill References</span>'},
					{type: 'separator'},
					{id: 'housebill_no', type: 'text', text: '<span class="bold">Housebill No: ' + this.tabData.origin + '-' + this.tabData.airbillNo + '</span>'},
					{type: 'separator'},
					{id: 'add_reference', type: 'text',	text: 'Add Reference:'},
					{id: 'reference', type: 'buttonInput', width: 150},
					{id: 'add_reference', type: 'button', text: 'Add Reference'}
				]
			}
		);

		this.toolBar.attachEvent('onClick', function(id) {
			var ref = this.getValue('reference');
			if(id === 'add_reference' && ref){
				_this.tab.progressOn();
				sendAjaxRequest('Export/InsertReference?housebill='+_this.tabData.airbillNo+'&origin='+_this.tabData.origin+'&reference=' + ref, 'POST', null, function(response) {
					response = dhx4.s2j(response.xmlDoc.responseText);
					if(response && response.Data) {
						_this.load();
					}else {
						showAlert('References', response.Status.Message);
						_this.tab.progressOff();
					}
				});
			}
			else {
				showAlert('References', 'Please fill field before continue');
			}
		});

		//init grid

		this.grid = this.tab.attachGrid();
		this.grid.setIconPath(dhtmlx.image_path);
		this.grid.setHeader(['Reference Type', 'Reference']);
		this.grid.setColSorting('str,str');
		this.grid.setColAlign('center,center');
		this.grid.setColTypes('ro,ro');
		this.grid.setInitWidths('*,*');
		this.grid.init();
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/GetReferences?airbillNo=' + this.tabData.airbillNo + '&origin=' + this.tabData.origin, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			_this.grid.clearAll();
			if (data && data.Status && data.Status.Status) {
				_this.grid.parse(data.Data, 'json');
			}
			_this.tab.progressOff();
		});
	};

	this.clearAll = function(){
		_this = null;
	};

	this.init();
	return this;
};

dhx4.attachEvent('loadReferencesTabData', function (tab, data) {
	if (tab && !referencesTabs[tab.getId()]) {
		referencesTabs[tab.getId()] = new referencesTab(tab, data);
	}
	referencesTabs[tab.getId()].load(data);
});
dhx4.attachEvent('clearReferencesData', function(tab){
	if(tab && referencesTabs && referencesTabs[tab.getId()]) {
		referencesTabs[tab.getId()].clearAll();
		referencesTabs[tab.getId()] = null;
		delete referencesTabs[tab.getId()];
	}
});