var remarksTabs = {};

var remarksTab = function (tab, data) {

	var _this = this;
	this.tab = tab;
	this.tabData = data;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{id: 'housebill_remarks', type: 'text',	text: '<span class="bold">Housebill Remarks</span>'},
					{type: 'separator'},
					{id: 'housebill_no', type: 'text', text: '<span class="bold">Housebill No: ' + this.tabData.origin + '-' + this.tabData.airbillNo + '</span>'}
				]
			}
		);

		//init grid

		this.grid = this.tab.attachGrid();
		this.grid.setIconPath(dhtmlx.image_path);
		this.grid.setHeader(['Executed On', 'User', 'Reference', 'Description']);
		this.grid.setColSorting('str,str,str,str');
		this.grid.setColAlign('center,center,center,center');
		this.grid.setColTypes('ro,ro,ro,ro');
		this.grid.setInitWidths('*,*,*,*');
		this.grid.init();
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/GetRemarksDetails?airbillNo=' + this.tabData.airbillNo + '&origin=' + this.tabData.origin, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			_this.grid.clearAll();
			if (data && data.Status && data.Status.Status) {
				_this.grid.parse(data.Data, 'json');
			}
			_this.tab.progressOff();
		});
	};

	this.clearAll = function(){
		_this = null;
	};

	this.init();
	return this;
};

dhx4.attachEvent('loadRemarksTabData', function (tab, data) {
	if (tab && !remarksTabs[tab.getId()]) {
		remarksTabs[tab.getId()] = new remarksTab(tab, data);
	}
	remarksTabs[tab.getId()].load(data);
});
dhx4.attachEvent('clearRemarksData', function(tab){
	if(tab && remarksTabs && remarksTabs[tab.getId()]) {
		remarksTabs[tab.getId()].clearAll();
		remarksTabs[tab.getId()] = null;
		delete remarksTabs[tab.getId()];
	}
});