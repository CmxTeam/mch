var locationsTabs = {};

var locationTab = function (tab, data) {

	var _this = this;
	this.tab = tab;
	this.tabData = data;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{id: 'housebill_locations',	type: 'text', text: '<span class="bold">Housebill Locations</span>'},
					{type: 'separator'},
					{id: 'housebill_no', type: 'text', text: '<span class="bold">Housebill No: ' + this.tabData.origin + '-' + this.tabData.airbillNo + '</span>'}
				]
			}
		);

		//init grid

		this.grid = this.tab.attachGrid();
		this.grid.setIconPath(dhtmlx.image_path);
		this.grid.setHeader(['Pieces', 'Location', 'User', 'Description']);
		this.grid.setColSorting('str,str,str,str');
		this.grid.setColAlign('center,center,center,center');
		this.grid.setColTypes('ro,ro,ro,ro');
		this.grid.setInitWidths('*,*,*,*');
		this.grid.init();
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/GetLocations?airbillNo=' + this.tabData.airbillNo, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			_this.grid.clearAll();
			if (data && data.Status && data.Status.Status) {
				_this.grid.parse(data.Data, 'json');
			}
			_this.tab.progressOff();
		});
	};

	this.clearAll = function(){
		_this = null;
	};

	this.init();
	return this;
};

dhx4.attachEvent('loadLocationsTabData', function (tab, data) {
	if (tab && !locationsTabs[tab.getId()]) {
		locationsTabs[tab.getId()] = new locationTab(tab, data);
	}
	locationsTabs[tab.getId()].load(data);
});
dhx4.attachEvent('clearLocationsData', function(tab){
	if(tab && locationsTabs && locationsTabs[tab.getId()]) {
		locationsTabs[tab.getId()].clearAll();
		locationsTabs[tab.getId()] = null;
		delete locationsTabs[tab.getId()];
	}
});