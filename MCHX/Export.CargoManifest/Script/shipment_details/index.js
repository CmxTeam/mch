var shipmentDetails;
var shipmentDetailsWindow = function (shipmentsData) {

	var _this = this;
	this.shipmentsData = shipmentsData;

	this.init = function () {

		//init window
		this.shipWin = CM.W.createWindow("shipment_details", 0, 0, 900, 500);
		this.shipWin.denyPark();
		this.shipWin.denyResize();
		this.shipWin.setText('Shipment Details');
		this.shipWin.center();

		this.shipWin.attachEvent('onClose', function () {
			_this.tabBar.forEachTab(function (tab) {
				var id = tab.getId();
				if (id === 'pcs_dims') {
					dhx4.callEvent('clearPCSData', [tab]);
				}
				else if (id === 'remarks') {
					dhx4.callEvent('clearRemarksData', [tab])
				}
				else if (id === 'hb_overview') {
					dhx4.callEvent('clearOverviewData', [tab])
				}
				else if (id === 'charges') {
					dhx4.callEvent('clearChargesData', [tab])
				}
				else if (id === 'references') {
					dhx4.callEvent('clearReferencesData', [tab])
				}
				else if (id === 'damages') {
					dhx4.callEvent('clearDamagesData', [tab])
				}
				else if (id === 'locations') {
					dhx4.callEvent('clearLocationsData', [tab])
				}
			});
			return true;
		});

		//init tabbar

		this.tabBar = this.shipWin.attachTabbar({
				tabs: [
					{id: "pcs_dims", text: "PCS./DIMS",	active: false, close: false},
					{id: "remarks",	text: "Remarks", active: false,	close: false},
					{id: "hb_overview",	text: "HB Overview", active: false,	close: false},
					{id: "charges",	text: "Charges", active: false,	close: false},
					{id: "references", text: "References", active: false, close: false},
					{id: "damages",	text: "Damages", active: false,	close: false},
					{id: "locations", text: "Locations", active: false, close: false}
				]
			}
		);

		this.tabBar.attachEvent('onSelect', function (id) {
			var event;
			if (id === 'pcs_dims') {
				event = 'loadPCSTabData';
			}
			else if (id === 'remarks') {
				event = 'loadRemarksTabData';
			}
			else if (id === 'hb_overview') {
				event = 'loadOverviewTabData';
			}
			else if (id === 'charges') {
				event = 'loadChargesTabData';
			}
			else if (id === 'references') {
				event = 'loadReferencesTabData';
			}
			else if (id === 'damages') {
				event = 'loadDamagesTabData';
			}
			else if (id === 'locations') {
				event = 'loadLocationsTabData';
			}
			if(event){
				dhx4.callEvent(event, [this.tabs(id), _this.shipmentsData[0]]);
			}
			return true;
		});

		this.tabBar.tabs('pcs_dims').setActive();

	};

	this.init();
	return this;
};

dhx4.attachEvent('showShipmentDetailsWindow', function (shipmentsData) {
	shipmentDetails = new shipmentDetailsWindow(shipmentsData)
});