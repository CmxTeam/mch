var overviewTabs = {};

var overviewTab = function (tab, data) {

	var _this = this;
	this.tab = tab;
	this.tabData = data;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{id: 'housebill_ditails', type: 'text', text: '<span class="bold">Housebill Details</span>'},
					{type: 'separator'},
					{id: 'housebill_no', type: 'text', text: '<span class="bold">Housebill No: ' + this.tabData.origin + '-' + this.tabData.airbillNo + '</span>'}
				]
			}
		);

		//init form

		this.form = this.tab.attachForm([
			{type: "template", name: "housebillNo", label: 'Housebill #:', format: CM.f.textTemplate},
			{type: "template", name: "AESITN", label: 'Autority #:', format: CM.f.textTemplate},
			{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, widthP: '60%', offsetTop: 10, list: [
					{type: "template", name: "NoOfPackages", label: '# of Pieces:', format: CM.f.textTemplate},
					{type: "template", name: "GrossWeight", label: 'Gross Weight:', format: function (n, t) { return  dhx4.template("#value|number_format:0,000.00 KG.#", {value: t});}},
					{type: "template", name: "DimWeight", label: 'Dim. Weight:', format: function (n, t) {	return dhx4.template("#value|number_format:0,000.00 KG.#", {value: t});}},
					{type: "template", name: "DeclareCustomsValue", label: 'Shipment Value:', format: function (n, t) { return dhx4.template("#value|number_format:$0,000.00#", {value: t});}},
					{type: "template", name: "otherCharges", label: 'Other Charges:', format: function (n, t) { return dhx4.template("#value|number_format:$0,000.00#", {value: t});}},
					{type: "newcolumn"},
					{type: "template", name: "OnHand", offsetLeft: 20, label: 'On Hand Pieces:', format: CM.f.textTemplate},
					{type: "template", name: "ChargeableWeight", offsetLeft: 20, label: 'Change. Weight:', format: function (n, t) { return dhx4.template("#value|number_format:0,000.00 KG.#", {value: t});}},
					{type: "template", className: "bold", offsetLeft: 20, name: "ShipmentType", label: 'Shipment Type:', format: CM.f.textTemplate},
					{type: "template", name: "WeightCharge", offsetLeft: 20, format: function (n, t) { return "Freight Charges:" + dhx4.template("#value|number_format:$0,000.00#", {value: t});}},
					{type: "template", className: "bold", offsetLeft: 20, name: "TotalCharges", format: function (n, t) { return "Total Charges:" + dhx4.template("#value|number_format:$0,000.00#", {value: t});}}
				]
			},
			{type: "input", name: "Notes", label: "Description:", position: 'label-top', offsetLeft: 5,	width: 250,	rows: 3},
			{type: "block", name: "button_block", blockOffset: 0, offsetLeft: 0, offsetTop: 50, list: [
				{type: "button", name: "print", value: "Print"},
				{type: "newcolumn"},
				{type: "button", name: "preview", value: "Preview"},
				{type: "newcolumn"},
				{type: "button", name: "print_ltar_form", value: "Print ltar Form"},
				{type: "newcolumn"},
				{type: "button", name: "print_co", value: "Print C.O."}
			]
			},
			{type: "newcolumn"},
			{type: "block", name: "info_block_sec_part", blockOffset: 0, offsetLeft: 40, widthP: '40%', offsetTop: 10, list: [
					{type: "template", name: "SenderAccountNo", label: 'Sender:', format: CM.f.textTemplate},
					{type: "input", name: "sender_info", width: 250, rows: 3},
					{type: "template", name: "ReceiverAccountNo", label: 'Receiver:', format: CM.f.textTemplate},
					{type: "input", name: "receiver_info", width: 250, rows: 3},
					{type: "template", name: "third_party_info", label: 'Third party:', format: CM.f.textTemplate},
					{type: "input", name: "third_party", width: 250, rows: 3}
				]
			}
		]);

		this.form.attachEvent('onButtonClick', function(name) {
			var url;
			if(name === 'print') {
				url = 'Export/PrintHawb';
			}
			else if(name === 'preview') {
				return false;
			}
			else if(name === 'print_ltar_form') {
				url = 'Export/PrintHawbItar';
			}
			else if(name === 'print_co') {
				url = 'Export/PrintHawbPo';
			}
			url += '?airbill=' + _this.tabData.airbillNo;
			_this.tab.progressOn();
			sendAjaxRequest(url, 'POST', null, function(response) {
				response = dhx4.s2j(response.xmlDoc.responseText);
				if(response && !response.Data) {
					showAlert('Print', response.Status.Message);
				}
				_this.tab.progressOff();
			})
		});
		this.form.setFormData({});
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/GetOverview?airbillNo=' + this.tabData.airbillNo + '&origin=' + this.tabData.origin, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			data.Data[0].AESITN = data.Data[0].AESITN ? data.Data[0].AESITN : '';
			data.Data[0].housebillNo = data.Data[0].Origin + '-' + data.Data[0].AirbillNo + '-' + data.Data[0].Destination;
			data.Data[0].sender_info = data.Data[0].SenderName + ' ' + data.Data[0].SenderAddress + ' ' + data.Data[0].SenderCity + ' ' + data.Data[0].SenderState + ' ' + data.Data[0].SenderPostalCode
			+ ' ' + data.Data[0].SenderCountry + ' ' + data.Data[0].SenderAttn;
			data.Data[0].receiver_info = data.Data[0].ReceiverName + ' ' + data.Data[0].ReceiverAddress + ' ' + data.Data[0].ReceiverCity + ' ' + data.Data[0].ReceiverState + ' ' + data.Data[0].ReceiverPostalCode
				+ ' ' + data.Data[0].ReceiverCountry + ' ' + data.Data[0].ReceiverAttn;
			data.Data[0].third_party = data.Data[0].receiver_info;
			data.Data[0].otherCharges = data.Data[0].TotalCharges - data.Data[0].WeightCharge;
			_this.form.setFormData(data.Data[0]);
			_this.tab.progressOff();
		});
	};

	this.clearAll = function () {
		_this = null;
	};

	this.init();
	return this;
};

dhx4.attachEvent('loadOverviewTabData', function (tab, data) {
	if (tab && !overviewTabs[tab.getId()]) {
		overviewTabs[tab.getId()] = new overviewTab(tab, data);
	}
	overviewTabs[tab.getId()].load(data);
});
dhx4.attachEvent('clearOverviewData', function (tab) {
	if (tab && overviewTabs && overviewTabs[tab.getId()]) {
		overviewTabs[tab.getId()].clearAll();
		overviewTabs[tab.getId()] = null;
		delete overviewTabs[tab.getId()];
	}
});