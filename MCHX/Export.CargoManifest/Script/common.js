﻿var CM = {
	localhost: false,
	server: 'http://192.168.3.36:3333/' || window.WebApiUrl.split(';')[0],
	dateFormat: "MM/DD/YYYY",
	calendarDateFormat: "%n/%d/%Y",
    gateway: 'JFK'
};
function doOnLoad() {
	mainLayout.cells('b').hideHeader();
	dhx4.callEvent('onLeftSideInit', [mainLayout.cells('a')]);
	mainLayout.cells('a').setWidth(280);
	dhx4.callEvent('onRightSideInit', [mainLayout.cells('b')]);
	CM.W = new dhtmlXWindows();
    sendAjaxRequest('Export/SetBaseUrl?url=' + window.baseUrl, 'POST', null, null);
}

function doOnUnload() {
	if (mainLayout) {
		mainLayout.unload();
		mainLayout = null;
	}
}

dhtmlxEvent(window, 'load', doOnLoad);
dhtmlxEvent(window, 'unload', doOnUnload);