﻿var print;
var printLoadingPlan = function (loadingPlan) {

	var _this = this;
	this.loadingPlan = loadingPlan;

	this.init = function () {

		//init window

		this.printWin = CM.W.createWindow("print", 0, 0, 300, 200);
		this.printWin.denyPark();
		this.printWin.denyResize();
		this.printWin.setText('Print Loading Plan');
		this.printWin.center();

		this.printWin.attachEvent('onClose', function () {
			_this.loadingPlan = _this.printWin = _this = null; 
			return true;
		});

		//init toolbar

		this.toolBar = this.printWin.attachToolbar({
			json: [
				{id: 'print', type: 'text', text: '<span class="bold">Print Loading Plan</span>'}
			]
		});

		this.form = this.printWin.attachForm([
			{type: "template", label: "Please select print", offsetLeft: 80},
			{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 25, offsetTop: 30,list: [
					{type: "combo", name: "printers", width: 140, filtering: true},
					{type: "newcolumn"},
					{type: "button", name: "print", value: "Print", offsetLeft: 10}
				]
			}
		]);

		this.form.attachEvent('onButtonClick', function(id) {
			var t = this.getItemValue('printers');
			if(id === 'print') {
				if (t) {
					_this.printWin.progressOn();
					sendAjaxRequest('Export/PrintDocument?loadingPlanId=' + _this.loadingPlan + '&printer=' + this.getItemValue('printers'), 'POST', null, function(response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if(response && response.Data) {
							showAlert('Print Loading Plan', 'Print Loading Plan. Please wait');
							_this.printWin.close();
						}
						else {
							showAlert('error', 'Something went wrong');
							_this.printWin.progressOff();
						}
					})
				}
				else {
					showAlert('Print', 'Please select print before continue');
				}
			}
		});
		
		
		this.fillForm();
	};

	this.fillForm = function() {
		this.printWin.progressOn();
		this.form.getCombo('printers').loadExternal('Export/GetAllPrinters', 'Printer', 'Description', function() {
			_this.printWin.progressOff();
		});
	};


	this.init();
	return this;
};

dhx4.attachEvent('showPrintLoadingPlan', function (loadingPlan) {
	print = new printLoadingPlan(loadingPlan)
});