﻿function MediaHandler() {
    this.VideoStreamOptions = null;
}


MediaHandler.prototype.stop = function () {
    try {
        this.videoElement.src = '';
        this.videoElement = '';
        this.stream.stop();
    } catch (e) {
        console.log(e);
    }
}

MediaHandler.prototype.init = function (videoElemId, canvasElemId, sourceSelectorId) {
    this.VideoStreamOptions = null;
}

MediaHandler.prototype.start = function (videoElement, source) {
    $this = this;
    window.ImageCaptureSource = 1;

    if (source == 'doc') {
        window.ImageCaptureSource = 0;
    }
    else if (source == 'img') {
        window.ImageCaptureSource = 1;
    }

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

    if (typeof MediaStreamTrack === 'undefined') {
        alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
    } else {
        if (MediaStreamTrack.getSources) {
            navigator.getUserMedia({ 'audio': false, video: true }, function () {
                MediaStreamTrack.getSources(function (sources) {
                    for (var i in sources) {
                        if (sources[i].kind != 'video') continue;
                        console.log(sources[i]);
                        navigator.getUserMedia({ 'audio': false, video: { optional: [{ sourceId: sources[i].id }] } },
                        function (stream) {
                            console.log('stream');
                            $this.stream = stream;
                            $this.videoElement = videoElement;
                            videoElement.src = window.URL.createObjectURL(stream);
                            videoElement.play();
                        }, function (error) {
                            console.log(error);
                            alert('Cannot access to media device.');
                        });
                    }
                });
            }, function () { });
        } else {
            navigator.getUserMedia({ 'audio': false, video: true },
                    function (stream) {
                        console.log('stream');
                        $this.stream = stream;
                        $this.videoElement = videoElement;
                        videoElement.src = window.URL.createObjectURL(stream);
                        videoElement.play();
                    }, function (error) {
                        console.log(error);
                        alert('Cannot access to media device.');
                    });
        }
    }
}

MediaHandler.prototype.getMediaSources = function (sourceInfos) {
    var videoSources = [];
    var audioSources = [];
    for (var i = 0; i != sourceInfos.length; ++i) {
        var sourceInfo = sourceInfos[i];
        //sourceInfo.id, sourceInfo.label
        if (sourceInfo.kind === 'audio') {
            audioSources.push(sourceInfo.id);
        } else if (sourceInfo.kind === 'video') {
            videoSources.push(sourceInfo.id);
        } else {
            console.log('Some other kind of source: ', sourceInfo);
        }
    }

    if (!!window.stream) {
        try {
            window.DocumentVideoElement.src = '';
            window.stream.stop();
        } catch (e) {
            console.log(e);
        }
    }
    var constraints = {
        audio: false,
        video: {
            optional: [{ sourceId: videoSources[window.ImageCaptureSource] }]
        }
    };


    navigator.getUserMedia(constraints,
        function (stream) {
            try {
                window.stream = stream; // make stream available to console
                window.DocumentVideoElement.src = window.URL.createObjectURL(stream);
                window.DocumentVideoElement.play();
            } catch (e) {
                console.log(e);
            }
        },
        function () {
            console.log("navigator.getUserMedia error: ", error);
        });
}