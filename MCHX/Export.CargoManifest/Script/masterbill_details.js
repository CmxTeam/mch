﻿var masterbillTabs = {};

var masterbillTab = function (tab, configData) {
	var _this = this;
	this.tab = tab;
	this.consol = configData.consol;
	this.airbillNo = configData.airbillNo;
	this.gateway = configData.gateway;
	this.grids = {};

	this.init = function () {

		//init toolbar
		this.toolBar = this.tab.attachToolbar({
			json: [
				{id: 'profitability', type: 'button', text: 'Profitability'},
				{type: 'separator', id: 'profitability_sep'},
				{id: 'dispositions', type: 'button', text: 'Dispositions'},
				{type: 'separator', id: 'dispositions_sep'},
				{id: 'delete_consol', type: 'button', text: 'Delete Consol'},
				{type: 'separator', id: 'delete_consol_sep'},
				{id: 'spot_rate', type: 'button', text: 'Spot Rate'},
				{type: 'separator', id: 'spot_rate_sep'},
				{id: 'uld_loose', type: 'button', text: 'ULD / Loose'},
				{type: 'separator', id: 'uld_loose_sep'},
				{id: 'finalize', type: 'button', text: 'Finalize'},
				{type: 'separator', id: 'finalize_sep'},
				{id: 'finalyze_master_house', type: 'button', text: 'Finalyze Master House'},
				{type: 'separator', id: 'finalyze_master_house_sep'},
				{id: 'reopen_consol', type: 'button', text: 'Reopen Consol'},
				{type: 'separator', id: 'reopen_consol_sep'},
				{id: 'mawb', type: 'button', text: 'MAWB'},
				{type: 'separator', id: 'mawb_sep'},
				{id: 'mawb_2', type: 'button', text: 'MAWB'},
				{type: 'separator', id: 'mawb_2_sep'},
				{id: 'transmit', type: 'button', text: 'Transmit'},
				{type: 'separator', id: 'transmit_sep'},
				{id: 'print_documents', type: 'button', text: 'Print Documents'},
				{type: 'separator', id: 'print_documents_sep'},
				{id: 'open_task', type: 'button', text: 'Open Task'},
				{type: 'separator', id: 'open_task_sep'},
				{id: 'dispatch_time', type: 'button', text: 'Dispatch Time'},
				{type: 'separator', id: 'dispatch_time_sep'}
			]
		});

		this.toolBar.attachEvent('onClick', function (id) {
			if(id === 'spot_rate') {
				_this.showStopRate()
			}
			else if(id === 'uld_loose') {
				_this.showULDWin();
			}
			else if(id === 'dispositions') {
				_this.showDisposWin();
			}
			return true;
		});

		//init accorion section

		this.accordion = this.tab.attachAccordion();
		this.accordion.enableMultiMode(true); // enable to add few opened sections
		this.accordion.cont.style.overflowY = "auto"; // add scroll (if needed) to the top section of accordion
	};

	this.load = function (tab) {
		//this.tab = tab;
		this.tab.progressOn();
		sendAjaxRequest('Search/GetMasterbillDetails?consol=' + this.consol, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			if (data && data.Status && data.Status.Status) {
				_this.fillTab(data.Data);
			}
			_this.tab.progressOff();
		});
	};


	this.fillTab = function (data) {
		var i, q, t, formData = {}, gridData = {}, alerts = {}, formSection, gridSection,
			grid, headerData, types = 'ch,', sortType = 'na,', colAlign = 'center,', sectionHeight, totalHawb,
			totalPcs, totalGross, totalCF, totalCH, totalFTC;
		for(i = 0; i < data.length; i++) {
			for(q = 0; q < data[i].Rows.length; q++) {
				if(data[i].TableName === 'Consol') {
					if(data[i].Rows[q].ULDLIST) {
						t = data[i].Rows[q].ULDLIST;
					}
					formData = data[i].Rows[q];
					if(t) {
						formData.ULDLIST = t;
					}
				}
				else if(data[i].TableName === 'Housebills'){
					gridData = data[i].Rows[q].data;
				}
				else if(data[i].TableName === 'ULD'){
					formData.ULDLIST = data[i].Rows[q].ULDLIST;
				}
				else if(data[i].TableName === 'Alerts'){
					alerts = data[i].Rows[q];
				}
			}
		}

		formData.Carrier = formData.CarrierNumber + ' - ' + formData.CarrierName + ' - ' + formData.VesselName;

		// init form

		formSection = this.accordion.addItem('form', '');
		this.form = formSection.attachForm([
			{
				type: "block", name: "left_block", blockOffset: 0, offsetTop: 5,
				list: [
					{
						type: "template", name: "MasterBillNo", labelWidth: 110, label: 'Masterbill #:', format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "Carrier", label: 'Carrier:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "CarrierFlight", label: 'Flight #:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "AirportOfOrigin", label: 'Origin Airport:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "AirportOfDestination", label: 'Dest. Airport:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "TransportType", label: 'Transporter Type:', labelWidth: 110, format: function (n, t) {
						if (t === 'C') {
							t = 'Cargo';
						}
						else if (t === 'P') {
							t = 'Passenger';
						}
						else if (t === 'O') {
							t = 'Combi';
						}
						return '<div class="template" id="changeTransportType">' + t + '</div>';
					}
					},
					{
						type: "template", name: "TransmitionStatus", label: 'Transmission Status:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + (t ? t: 'TRANSMITTED') + '</div>';
					}
					}
				]
			},
			{type: "newcolumn"},
			{
				type: "block", name: "right_block", blockOffset: 0, offsetTop: 5,
				list: [
					{
						type: "template", name: "SelectedULDS", label: 'ULD[s]:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "ServiceType", label: 'Service Code:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "ExecutedOnDate", label: 'Operation Date:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "DepartureDate", label: 'Departure:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "template", name: "ArrivalDate", label: 'Arrival:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					},
					{
						type: "block", name: "inherit", blockOffset: 0, list: [
						{
							type: "template", name: "", label: 'Lockout:', labelWidth: 110
						},
						{type: "newcolumn"},
						{type: "button", name: "CloseOUTInfo", value: ""}
					]
					},
					{
						type: "template", name: "DisplayStatus", label: 'Status:', labelWidth: 110, format: function (n, t) {
						return '<div class="template">' + t + '</div>';
					}
					}
				]
			}
		]);
		window.formaaa = this.form;
		// configure toolbar and formData
		//profitability, dispositions, spot_rate, delete_consol, uld_loose, finalize, finalyze_master_house, reopen_consol, mawb, mawb_2, transmit,
		// print_documents, open_task, dispatch_time
		if(formData.TransmitionStatus.toLowerCase().indexOf('error') > -1) {
			formData.TransmitionStatus = 'ERROR: ' + formData.TransmitionStatus;
		}
		if(formData.IsImport === 'True') {
			this.toolBar.hideItem('spot_rate');
			this.toolBar.hideItem('spot_rate_sep');
			this.toolBar.hideItem('delete_consol');
			this.toolBar.hideItem('delete_consol_sep');
			this.toolBar.hideItem('print_documents');
			this.toolBar.hideItem('print_documents_sep');
			this.toolBar.hideItem('mawb');
			this.toolBar.hideItem('mawb_sep');
			this.toolBar.hideItem('mawb_2');
			this.toolBar.hideItem('mawb_2_sep');
			this.toolBar.hideItem('finalize');
			this.toolBar.hideItem('finalize_sep');
			this.toolBar.hideItem('reopen_consol');
			this.toolBar.hideItem('reopen_consol_sep');
			this.toolBar.hideItem('transmit');
			this.toolBar.hideItem('transmit_sep');
			this.toolBar.hideItem('uld_loose');
			this.toolBar.hideItem('uld_loose_sep');
			this.toolBar.hideItem('options');
			this.toolBar.hideItem('options_sep');
		}
		else {
			if(formData.Status === 'Closed') {
				this.toolBar.hideItem('finalyze_master_house');
				this.toolBar.hideItem('finalyze_master_house_sep');
				this.toolBar.hideItem('spot_rate');
				this.toolBar.hideItem('spot_rate_sep');
				this.toolBar.hideItem('delete_consol');
				this.toolBar.hideItem('delete_consol_sep');
				this.toolBar.hideItem('print_documents');
				this.toolBar.hideItem('print_documents_sep');
				this.toolBar.hideItem('mawb');
				this.toolBar.hideItem('mawb_sep');
				this.toolBar.hideItem('mawb_2');
				this.toolBar.hideItem('mawb_2_sep');
				this.toolBar.hideItem('finalize');
				this.toolBar.hideItem('finalize_sep');
				this.toolBar.hideItem('transmit');
				this.toolBar.hideItem('transmit_sep');
				this.toolBar.hideItem('uld_loose');
				this.toolBar.hideItem('uld_loose_sep');
				this.toolBar.hideItem('options');
				this.toolBar.hideItem('options_sep');
				if(formData.TransmitionStatus.toLowerCase().indexOf('closed in logis') > -1) {
					this.toolBar.showItem('reopen_consol');
					this.toolBar.showItem('reopen_consol_sep');
				}
				else {
					this.toolBar.showItem('delete_consol');
					this.toolBar.showItem('delete_consol_sep');
					this.toolBar.hideItem('reopen_consol');
					this.toolBar.hideItem('reopen_consol_sep');
				}
			}
			else {
				if(formData.ServiceType === "AS") {
					this.toolBar.hideItem('spot_rate');
					this.toolBar.hideItem('spot_rate_sep');
					this.toolBar.hideItem('mawb');
					this.toolBar.hideItem('mawb_sep');
					this.toolBar.hideItem('mawb_2');
					this.toolBar.hideItem('mawb_2_sep');
				}
				else{
					this.showOldMawb()
				}

				//if(Session.item('usertype') == 'admin') {
				// 	if(formData.ISTASKCLOSED === 'True') {
				//		this.toolBar.showItem('open_task');
				//		this.toolBar.showItem('open_task_sep');
				//	}
				//	else {
				//		this.toolBar.hideItem('open_task');
				//		this.toolBar.hideItem('open_task_sep');
				//	}
				//}

				this.getDispatchTimerInfo();
			}
		}
		//if(Session.item('usertype') !== 'admin') {
		// hide reopen button
		//	this.toolBar.hideItem('reopen_consol');
		//	this.toolBar.hideItem('reopen_consol_sep');
		//}
		if(formData.System.toLowerCase() == 'logis') {
			this.toolBar.hideItem('reopen_consol');
			this.toolBar.hideItem('reopen_consol_sep');
			this.toolBar.hideItem('transmit');
			this.toolBar.hideItem('transmit_sep');
			this.toolBar.hideItem('print_documents');
			this.toolBar.hideItem('print_documents_sep');
		}
		this.form.cont.className += ' flex-center';
		this.form.setFormData(formData);
		this.form.setItemLabel('CloseOUTInfo', formData.CloseOUTInfo);
		this.form.attachEvent('onButtonClick', function(id) {
			var t;
			if(id === 'CloseOUTInfo') {
				t = _this.formData.CloseOUTInfo.split(' ')
				t = t[t.length - 1]
				_this.changeTime(t);
			}
		});
		formSection.attachStatusBar({
			text: '<div class="inline_childs"><div>Driver: ' + formData.TASKUSER + '</div><div>Initiated: ' + formData.TASKINITDATE + '</div><div>Started:' + formData.TASKSTARTDATE + '</div>' +
			'<div>Ended: ' + formData.TASKENDDATE + '</div><div>Task Progress: ' + formData.TASKPROGRESS + '</div></div>', // status bar init text
			height: 20     // status bar init height
		});
		formSection.setHeight(265);
		document.getElementById('changeTransportType').onclick = this.changeTransportType;

		//init grid
		gridSection = this.accordion.addItem('gridSection', '');
		grid = gridSection.attachGrid();
		totalHawb = gridData.total_hawb ? gridData.total_hawb : 0;
		totalPcs = gridData.total_pcs ? gridData.total_pcs : 0;
		totalGross = gridData.total_gross_w ? gridData.total_gross_w : 0;
		totalCF = gridData.total_cf ? gridData.total_cf : 0;
		totalCH = gridData.total_ch_wt ? gridData.total_ch_wt : 0;
		totalFTC = gridData.total_ftc ? gridData.total_ftc : 0;
		headerData = gridData.ColumnNames.split(',');
		headerData.unshift('#master_checkbox');
		for (i = 1; i < headerData.length; i++) {
			types += 'ro,';
			sortType += 'str,';
			colAlign += 'center,';
		}
		types = types.substr(0, types.length - 1);
		colAlign = colAlign.substr(0, types.length - 1);
		sortType = sortType.substr(0, sortType.length - 1);
		grid.setHeader(headerData);
		grid.setIconPath(baseUrl);
		grid.setImagesPath(dhtmlx.image_path);

		grid.setColSorting(sortType);
		grid.setColTypes(types);
		grid.setColAlign(colAlign);
		grid.setInitWidths('40,' + gridData.Widths);
		grid.init();
		gridSection.attachStatusBar({
			text: '<div class="inline_childs"><div>Total Airbills: ' + totalHawb + '</div><div>Total Pieces: ' + totalPcs + '</div><div>Total Gross Weight.: ' + totalGross + '</div>' +
			'<div>Total Chargeable Weight: ' + totalCH + '</div><div>Total Cubic Feet: ' + totalCF + '</div><div>Total FTC: ' + totalFTC + '</div></div>', // status bar init text
			height: 20     // status bar init height
		});
		if (gridData.data) {
			for (i = 0; i < gridData.data.rows.length; i++) {
				gridData.data.rows[i].data.unshift('0');
			}
			grid.parse(gridData.data, 'json');
			sectionHeight = (gridData.data ? 26 * gridData.data.rows.length : 0) + gridSection.cell.childNodes[0].offsetHeight + 20 + gridSection.cell.childNodes[0].offsetHeight + 16 + 25; //31 - row height, 35 * 2 - header and footer height 16 - paddings
			gridSection.setHeight(sectionHeight);
		}
		this.formData = formData;
	};

	this.showOldMawb = function(){
		sendAjaxRequest('Search/ShowOldMawb', 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response.Data) {
				_this.toolBar.showItem('mawb');
				_this.toolBar.showItem('mawb_sep');
				_this.toolBar.hideItem('mawb_2');
				_this.toolBar.hideItem('mawb_2_sep');
			}
			else {
				_this.toolBar.hideItem('mawb');
				_this.toolBar.hideItem('mawb_sep');
				_this.toolBar.showItem('mawb_2');
				_this.toolBar.showItem('mawb_2_sep');
			}
		});
	};

	this.getDispatchTimerInfo = function() {
		sendAjaxRequest('Search/GetDispatchTimerInfo?consol=' + this.consol, 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data && response.Data.Rows && response.Data.Rows.length > 0) {
				_this.toolBar.showItem('dispatch_time');
				_this.toolBar.showItem('dispatch_time_sep');
			}
			else {
				_this.toolBar.hideItem('dispatch_time');
				_this.toolBar.hideItem('dispatch_time_sep');
			}
		});
	};

	this.clearAll = function (unload) {
		var key;
		for (key in this.grids) {
			if (this.grids.hasOwnProperty(key)) {
				this.grids[key].destructor();
				this.accordion.removeItem(key);
			}
		}
		if(this.accordion.unload) {
			this.accordion.unload();
		}
		this.grids = {};
		if(unload) {
			this.tab = _this = null;
		}

	};

	this.changeTransportType = function() {
		// init window

		this.transWin = CM.W.createWindow("transport_type", 0, 0, 260, 195);
		this.transWin.denyPark();
		this.transWin.denyResize();
		this.transWin.setText('Transport Type');
		this.transWin.center();

		//init toolbar

		this.toolBar = this.transWin.attachToolbar({
			json: [
				{ id: 'transport_type', type: 'text', text: '<span class="bold">Transport Type</span>' }
			]
		});

		//init form

		this.transForm = this.transWin.attachForm([
			{
				type: "combo",
				name: "transport_type",
				label: _this.tab.getId(),
				position: "label-top",
				width: 215,
				options: [
					{value: '', text: 'Please Select One'},
					{value: 'P', text: 'Passenger'},
					{value: 'C', text: 'Cargo'},
					{value: 'O', text: 'Combi'}
				]
			},
			{
				type: "block", name: "info_block", blockOffset: 0, offsetLeft: 30, offsetTop: 5,
				list: [
					{type: "button", name: "update", value: "Update"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		this.transForm.setItemValue('transport_type', this.formData.TransportType);

		this.transForm.attachEvent('onButtonClick', function (id) {
			var transportType;
			if (id === 'cancel') {
				_this.transWin.close();
			}
			else if (id === 'update') {
				transportType = this.getItemValue('transport_type');
				if (transportType) {
					_this.transWin.progressOn();
					sendAjaxRequest('Search/UpdateTransportType?type=' + transportType + '&code=' + _this.consol, 'POST', null, function(response){
						response = dhx4.s2j(response.xmlDoc.responseText);
						response.Data = true; //remove this later
						if(response && response.Data) {
							_this.form.setItemValue('TransportType', _this.transForm.getCombo('transport_type').getSelectedText());
						}
						_this.transWin.progressOff();
						_this.transWin.close();
					});
				}
				else {
					showAlert('Transport Type', 'Please select Transport Type')
				}
			}
		});
	}.bind(this);

	this.changeTime = function(defaultTime) {
		this.closeoutWin = CM.W.createWindow("closeout_window", 0, 0, 260, 200);
		this.closeoutWin.denyPark();
		this.closeoutWin.denyResize();
		this.closeoutWin.setText('Consol Closeout');
		this.closeoutWin.center();
		this.closeoutTb = this.closeoutWin.attachToolbar({
			json: [
				{ id: 'closeout', type: 'text', text: '<span class="bold">Consol Closeout</span>' }
			]
		});

		this.closeoutForm = this.closeoutWin.attachForm([
			{name: 'closeout', type: 'inputClock', width: 215, value: defaultTime,  label: "Lockout Time:", position: "label-top"},
			{
				type: "block", name: "info_block", blockOffset: 0, offsetLeft: 40, offsetTop: 5,
				list: [
					{type: "button", name: "save", value: "Save"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		this.closeoutForm.attachEvent('onButtonClick', function (id) {
			var time;
			if (id === 'cancel') {
				_this.closeoutWin.close();
			}
			else if (id === 'save') {
				time = this.getItemValue('closeout');
				if (time) {
					sendAjaxRequest('/Export/UnvoidHousebills?hawb=' + housebill + '&gateway=' + CM.gateway + '&userId=' + helpers.getUserName(), 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {

						}
					})
				}
				else {
					//showAlert('Unvoid Shipment', 'Please enter a valid Shipment number')
				}
			}
		});
	};

	this.showStopRate = function() {
		this.spotRateWin = CM.W.createWindow("spotRate_win", 0, 0, 400, 340);
		this.spotRateWin.denyPark();
		this.spotRateWin.denyResize();
		this.spotRateWin.setText('Spot Rate Modification');
		this.spotRateWin.center();
		this.spotRateWin.progressOn();
		this.spotRateWin.attachToolbar({
			json: [
				{ id: 'spotrate', type: 'text', text: '<span class="bold">Spot Rate Modification</span>' }
			]
		});

		this.spotRateForm = this.spotRateWin.attachForm([
			{type: "block", name: "info_block_1", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
				{name: 'IsSpot', type: 'checkbox', offsetLeft: 100, label: "Enable Spot Rate", labelWidth: 100, position: "label-right"},
				{name: 'RateSpotNo', label: 'Spot Number:', type: 'input', labelWidth: 100, position: 'label-left', width: 200},
				{name: 'Contract', label: 'Contract Number:', type: 'input', labelWidth: 100, position: 'label-left', width: 200},
				{name: 'SalesRep', label: 'Sales Rep:', type: 'input', labelWidth: 100, position: 'label-left', width: 200},
				{name: 'SalesCommision', label: 'Sales Comm.:', type: 'input', labelWidth: 100, position: 'label-left', width: 200},
				{
					type: "block", name: "info_block_2", blockOffset: 0, offsetLeft: 0, offsetTop: 5,
					list: [
						{name: 'RateSpot', label: 'Net-Net Rate:', type: 'input', labelWidth: 100, position: 'label-left', width: 127},
						{type: "newcolumn"},
						{name: 'RateSpotType', type: 'combo', width: 70, options: [
							{value: "Per Kg.", text: "Per Kg.", selected: true},
							{value: "Flat", text: "Flat"}
						]}
					]
				}

			]},
			{
				type: "block", name: "info_block_2", blockOffset: 0, offsetLeft: 100, offsetTop: 5,
				list: [
					{type: "button", name: "update", value: "Update"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		sendAjaxRequest('Search/GetMawbSpotInfo?masterbill=' + this.consol, 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data && response.Data.length > 0) {
				_this.spotRateForm.setFormData(response.Data[0]);
			}
			_this.spotRateWin.progressOff();
		});

		this.spotRateForm.attachEvent('onButtonClick', function (id) {
			var formData;
			if (id === 'cancel') {
				_this.spotRateWin.close();
			}
			else if (id === 'update') {
				formData = this.getFormData();
				_this.spotRateWin.progressOn();
				sendAjaxRequest('Search/UpdateSpotRateInformation?rateSpotNo=' + formData.RateSpotNo + '&contract=' + formData.Contract + '&salesRep=' + formData.SalesRep + '&salesCommision=' + formData.SalesCommision + '&rateSpot=' + formData.RateSpot + '&rateSpotType=' + formData.RateSpotType + '&masterbill=' + _this.consol + '&isSpot=' + !!formData.IsSpot, 'POST', null, function (response) {
					response = dhx4.s2j(response.xmlDoc.responseText);
					if(response && response.Data) {
						_this.spotRateWin.close();
						_this.spotRateWin = this.spotRateForm = null;
					}
					else{
						_this.spotRateWin.progressOff();
						showAlert('Error', 'something went wrong');
					}
				});
			}
		});
	};

	this.showULDWin = function() {
		this.uldWin = CM.W.createWindow("uld_win", 0, 0, 800, 900);
		this.uldWin.denyPark();
		this.uldWin.denyResize();
		this.uldWin.setText('Spot Rate Modification');
		this.uldWin.center();
		this.uldWin.progressOn();
		this.uldWin.attachToolbar({
			json: [
				{ id: 'spotrate', type: 'text', text: '<span class="bold">Spot Rate Modification</span>' }
			]
		});

		sendAjaxRequest('Search/GetUldTypes', 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data && response.Data.length > 0) {
			}
			_this.uldWin.progressOff();
		});
		sendAjaxRequest('Search/GetMawbUlDsData?masterbillNo=' + this.consol, 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			console.log(response);
			if(response && response.Data && response.Data.length > 0) {
			}
			_this.uldWin.progressOff();
		});


	};

	this.showDisposWin = function() {
		var t;
		this.disposWin = CM.W.createWindow("dis_win", 0, 0, 700, 600);
		this.disposWin.denyPark();
		this.disposWin.denyResize();
		this.disposWin.setText('Dispositions');
		this.disposWin.center();
		this.disposWin.progressOn();
		this.disposWin.attachToolbar({
			json: [
				{ id: 'spotrate', type: 'text', text: '<span class="bold">MAWB Dispositions</span>' }
			]
		});
		this.disposWin.attachEvent('onClose', function() {
			_this.disposWin = true;
			return true;
		});

		t = this.disposWin.attachGrid();
		t.setHeader(['Disposition Date', 'Description', 'User', 'Airbill No.', 'Reference']);
		t.setColSorting('date,str,str,int,int');
		t.setColTypes('ro,ro,ro,ro,ro');
		t.setColAlign('left,left,left,left,left');
		t.setInitWidths('110,*,100,100,100');
		t.init();
		sendAjaxRequest('Search/GetDispositionsList?code=' + this.consol, 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data && response.Data.rows && response.Data.rows.length > 0) {
				t.parse(response.Data, 'json');
			}
			_this.disposWin.progressOff();
		});
	};

	this.init();

	return this;
};

dhx4.attachEvent("onConsolTabCreated", function (tab, configData) {
	if (tab && !masterbillTabs[tab.getId()]) {
		masterbillTabs[tab.getId()] = new masterbillTab(tab, configData);
	}
	else {
		masterbillTabs[tab.getId()].clearAll();
	}
	masterbillTabs[tab.getId()].load();
});

dhx4.attachEvent("unloadConsolTabData", function (code) {
	if (code && masterbillTabs[code]) {
		masterbillTabs[code].clearAll(true);
		masterbillTabs[code] = null;
		delete masterbillTabs[code];
	}
});