﻿var masterHouse;
var masterHouseWin = function (gateway, destination, type, tabData, total) {
	var _this = this;
	this.gateway = gateway;
	this.destination = destination;
	this.type = type;
	this.tabData = tabData;
	this.total = total;

	this.init = function () {

		//init window

		this.masWin = CM.W.createWindow("assign_master_house", 0, 0, 697, 350);
		this.masWin.denyPark();
		this.masWin.setText('Assign Master House');
		this.masWin.center();
		this.masWin.denyResize();

		this.masWin.attachEvent('onClose', function() {
			_this.masWin = _this = null;
			return true;
		});


		//init toolbar

		this.toolbar = this.masWin.attachToolbar({
			json: [
				{id: 'assign', type: 'text', text: '<span class="bold">Assign Master House</span>'}
			]
		});

		this.form = this.masWin.attachForm([
			{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 198, offsetTop: 5, list: [
					{type: 'input', name: 'origin', label: 'Origin:', position: 'label-left', value: _this.gateway, labelWidth: 90},
					{type: 'input', name: 'destination', label: 'Destination:', position: 'label-left', value: _this.destination, labelWidth: 90},
					{type: 'input', name: 'masterbill', label: 'Master-House#:', position: 'label-left', labelWidth: 90}
				]
			},
			{type: "block", name: "button_section", blockOffset: 0, offsetLeft: 258, offsetTop: 5, list: [
					{type: "button", name: "create", value: "Create & Assign"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			},
			{type: "container", name: "summary", inputWidth: 660, inputHeight: 40, offsetTop: 50},
			{type: "block", name: "bottom_section", blockOffset: 0, offsetLeft: 0, offsetTop: 2,
				list: [
					{type: 'input', name: 'loadingPlan', width: 154, offsetLeft: 9, value: (_this.tabData && _this.tabData.TabCode ? _this.tabData.TabCode: '')},
					{type: 'newcolumn'},
					{type: 'input', name: 'account', width: 154, offsetLeft: 5},
					{type: 'newcolumn'},
					{type: 'input', name: 'gatewayId', value: _this.gateway, width: 154, offsetLeft: 5},
					{type: 'newcolumn'},
					{type: 'input', name: 'userId', value: helpers.getUserName(), width: 154, offsetLeft: 5}
				]
			}
		]);
		if(this.total) {
			this.form.getContainer('summary').innerHTML = this.getSummaryTemplate();
		}

		this.form.attachEvent('onButtonClick', function(name) {
			var data = this.getFormData();
			if(name === 'cancel') {
				_this.masWin.close();
			}
			else if (name === 'create') {
				_this.masWin.progressOn();
				sendAjaxRequest('Export/CreateNewMasterHouse?loadingPlan='+data.loadingPlan+'&origin='+data.origin+'&masterbill='+data.masterbill+'&destination='+data.destination+'&accountName='+data.account+'&assemblyMode=' + _this.type, 'POST', null, function (response) {
					response = dhx4.s2j(response.xmlDoc.responseText);
					if(response && response.Data && response.Data.indexOf('already') > -1) {
						showAlert('Create Master House', response.Data);
					}
					else if(response && !response.Status) {
						showAlert('Create Master House', response.Status.Message);
					}
					else {
						_this.masWin.close();
					}
					_this.masWin.progressOff();
				});
			}
		})
	};

	this.getSummaryTemplate = function() {
		return '<table class="footerTbl">' +
			'<tbody>' +
			'<tr>' +
			'<td class="title">Total Airbills:</td>' +
			'<td class="value">'+this.total.totalHawb+'</td>' +
			'<td class="title">Total Pieces:</td>' +
			'<td class="value">'+this.total.totalPcs+'</td>' +
			'<td class="title">Total Gross Weight:</td>' +
			'<td class="value">'+this.total.totalGross+'</td>' +
			'</tr>' +
			'<tr>' +
			'<td class="title">Total Changeable Weight:</td>' +
			'<td class="value">'+this.total.totalCH+'</td>' +
			'<td class="title">Total Cubic Feet:</td>' +
			'<td class="value">'+this.total.totalCF+'</td>' +
			'<td class="title">Total FTC:</td>' +
			'<td class="value">'+this.total.totalFTC+'</td>' +
			'</tr>' +
			'</tbody>' +
			'</table>'
	};


	this.init();
	return this;
};

dhx4.attachEvent('showMasterHouse', function (gateway, destination, type, tabData, total) {
	masterHouse = new masterHouseWin(gateway, destination, type, tabData, total);
});