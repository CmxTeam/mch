﻿var mainTabs = {};

var mainTab = function (tab, gateway, destination) {
	var _this = this;
	this.tab = tab;
	this.gateway = gateway;
	this.destination = destination;
	this.pageData = {};
	this.grids = {};
	this.displayedGrids = {};

	this.init = function () {
		var typing;
		//init toolbar
		this.toolBar = this.tab.attachToolbar({
			json: [
				{id: 'quick_finder', type: 'text', text: 'QUICK FINDER:'},
				{id: 'search', type: 'buttonInput', width: 200},
				{type: 'separator'},
				{id: 'actions', type: 'buttonSelect', text: 'Actions', options: [
					{id: 'createNewPlan', type: 'button', text: 'Create New Loading Plan'},
					{type: 'separator'},
					{id: 'assignConsolidation', type: 'button', text: 'Assign Consolidation'},
					{id: 'assignEmptyConsolidation', type: 'button', text: 'Assign Empty Consolidation'},
					{id: 'createEmptyMasterHouse', type: 'button', text: 'Create Empty Master House'}
				]
				},
				{type: 'separator'},
				{id: 'groups', type: 'buttonSelect', text: 'Groups', options: [
					{id: 'check_all', type: 'button', text: 'Check All'},
					{id: 'uncheck_all', type: 'button', text: 'Uncheck All'},
					{type: 'separator', id: 'check_sep'}
				]
				},
				{type: 'separator'},
				{id: 'options', type: 'buttonSelect', text: 'Please Select One', options: [
					{id: 'view_shipment', type: 'button', text: 'View Shipment Details'},
					{id: 'hold_release', type: 'button', text: 'Hold / Release Shipment'},
					{id: 'expedite', type: 'button', text: 'Expedite YES / NO'},
					{id: 'change_masterbill', type: 'button', text: 'Change Masterbill Destination'},
					{id: 'download_selected', type: 'button', text: 'Download Selected Shipment(s)'},
					{id: 'update_ups', type: 'button', text: 'Update UPS Zone(s)'},
					{id: 'update_screening', type: 'button', text: 'Update Screening Status'},
					{id: 'add_delay', type: 'button', text: 'Add Delay Reason'},
					{id: 'cancel_screening', type: 'button', text: 'Cancel Screening'},
					{id: 'void_selected', type: 'button', text: 'Void Selected Shipment(s)'},
					{id: 'make_known', type: 'button', text: 'Make Shipment Known / Unknown'},
					{id: 'make_hazmat', type: 'button', text: 'Make Shipment Hazmat / Non-Hazmat'},
					{id: 'make_dryice', type: 'button', text: 'Make Shipment Dryice / Non-Dryice'},
					{id: 'override', type: 'button', text: 'Override / Apply Dims Restrictions'},
					{id: 'unvoid_housebill', type: 'button', text: 'Unvoid Housebills'},
					{id: 'override_ams', type: 'button', text: 'Override AMS Hold'}
				]},
				{type: 'separator'},
				{id: 'collapseAll', type: 'button', text: 'Collapse All', img: baseUrl + 'Images/Expandall.jpg'},
				{type: 'separator'},
				{id: 'expandAll', type: 'button', text: 'Expand All', img: baseUrl + 'Images/Collapseall.jpg'},
				{type: 'separator'},
				{id: 'appearance_settings', type: 'button', text: 'Appearance Settings'}

			]
		});

		this.toolBar.getInput('search').onkeyup = function (ev) {
			clearTimeout(typing);
			typing = setTimeout(function() {
				CM.f.filterGrids(_this.grids, _this.toolBar.getValue('search'), _this.changeSectionDataAndSize);
			}, 500);
		};

		this.toolBar.getInput('search').onkeydown = function (ev) {
			clearTimeout(typing)
		};

		this.toolBar.attachEvent('onClick', function (id) {
			var parentId = this.getParentId(id), imagePath, empty = false, data = [], eventName, i, t = '';
			_this.showConsolWindow = false;
			if (parentId === 'groups') {
				if (id === 'check_all') {
					_this.checkUncheckAll(true)
				} else if (id === 'uncheck_all') {
					_this.checkUncheckAll(false)
				}
				else {
					imagePath = this.getListOptionImage(parentId, id);
					if (imagePath.indexOf('item_chk0') > -1) {
						_this.addGroup(_this.pageData[id]);
						this.setListOptionImage(parentId, id, dhtmlx.image_path + 'dhxgrid_web/item_chk1.gif');
					}
					else {
						_this.removeGroup(id);
						this.setListOptionImage(parentId, id, dhtmlx.image_path + 'dhxgrid_web/item_chk0.gif');
					}
				}
			}
			else if (parentId === 'actions') {
				if (id === 'assignConsolidation' || id === 'createNewPlan' || id === 'assignEmptyConsolidation') {
					if (id === 'assignEmptyConsolidation') {
						empty = true;
					}
					if (id === 'assignConsolidation' || id === 'assignEmptyConsolidation') {
						_this.showConsolWindow = true;
					}
					_this.createNewPlan(empty, id === 'createNewPlan' ? true : false);
				}
				else if(id === 'createEmptyMasterHouse') {
					dhx4.callEvent('showMasterHouse', [_this.gateway, _this.destination, 'c'])
				}
			}
			else if (parentId === 'options') {
				data = CM.f.getSelectedShipmentData(_this.grids);
				data = data.selectedRows;
				if (data.length === 0 && id !== 'unvoid_housebill') {
					showAlert('Shipment Details', 'No Shipment selected');
					return;
				}
				if (id === 'view_shipment') {
					eventName = 'showShipmentDetailsWindow';
				}
				else if (id === 'expedite') {
					CM.f.expediteShipments(data, 'reloadSelectedTab');
				}
				else if (id === 'change_masterbill') {
					eventName = 'showChangeDestinationWindow';
				}
				else if (id === 'download_selected') {
					eventName = 'showDownloadShipmentsWindow';
				}
				else if (id === 'update_screening') {
					for(i = 0; i < data.length; i++) {
						t += data[i].airbillNo + ','
					}
					t = t.substr(0, t.length - 1);
					_this.tab.progressOn();
					CM.f.updateScreeningStatus(t, 'reloadSelectedTab');
				}
				else if (id === 'void_selected') {
					_this.voidSelectedShipments(data);
				}
				else if (id === 'make_known') {
					_this.makeShipmentsKnownUnknown(data);
				}
				else if (id === 'make_hazmat') {
					_this.makeHazmat(data);
				}
				else if (id === 'make_dryice') {
					_this.makeDryice(data);
				}
				else if (id === 'override') {
					_this.overrideDims(data);
				}
				else if (id === 'unvoid_housebill') {
					_this.unvoidShipment();
				}
				else if (id.indexOf('assign_to_plan_') > -1) {
					id = id.split('_');
					id = id[id.length - 1];
					_this.assignSelected(data, id);
				}
				else if(id.indexOf('assign_to_consol_') > -1) {
					id = id.split('_');
					id = id[id.length - 1];
					id = id.split('-');
					CM.f.importShipmentsByHousebill(data[0].airbillNo, id[1], id[0], function(response) {
						if(response && response.Data) {
							_this.reloadMainTab();
						}
					})
				}
				else if (id === 'add_delay') {
					_this.addDelayReason(data);
				}
				else if (id === 'hold_release') {
					eventName = 'showHoldReasonWindow';
				}
				else if (id === 'update_ups') {
					_this.updateUPSZones(data);
				}
				else if (id === 'cancel_screening') {
					showConfirm('Are you sure you want to cancel screening on Selected Shipment(s)', function() {
						for(i = 0; i < data.length; i++) {
							t += data[i].airbillNo + ','
						}
						t = t.substr(0, t.length - 1);
						CM.f.cancelScreeningStatus(t, 'reloadSelectedTab');
					})
				}
				else if (id === 'override_ams') {
					data = CM.f.getSelectedShipmentData(_this.grids, 'ams hold');
					if (data.notInGrid) {
						showAlert('AMS Hold', 'One or more selected shipments do not have AMS Hold, Unable to continue');
					}
					else {
						CM.f.overrideAMS(data.selectedRows, 'reloadSelectedTab');
					}
				}
				if (eventName) {
					dhx4.callEvent(eventName, [data, _this.routes]);
				}
			}
			else if (id === 'appearance_settings') {
				dhx4.callEvent('showAppearanceSettingsWindow', []);
			}
			else if (id === 'expandAll') {
				_this.expandCollapseAll(true);
			}
			else if (id === 'collapseAll') {
				_this.expandCollapseAll(false);
			}
			return true;
		});

		//init accorion section

		this.accordion = this.tab.attachAccordion();
		this.accordion.enableMultiMode(true); // enable to add few opened sections
		this.accordion.cont.style.overflowY = "auto"; // add scroll (if needed) to the top section of accordion
	};

	this.checkUncheckAll = function (flag) {
		this.toolBar.forEachListOption('groups', function (id) {
			var isShowed;
			if (id === 'check_all' || id === 'uncheck_all' || id === 'check_sep') {
				return true;
			}
			var imagePath = this.getListOptionImage('groups', id);
			isShowed = (imagePath.indexOf('item_chk1') > -1) ? true : false;
			if (flag) {
				if (isShowed) {
					return true;
				}
				else {
					_this.addGroup(_this.pageData[id]);
					this.setListOptionImage('groups', id, dhtmlx.image_path + 'dhxgrid_web/item_chk1.gif');
				}
			}
			else {
				if (!isShowed) {
					return true;
				}
				else {
					_this.removeGroup(id);
					this.setListOptionImage('groups', id, dhtmlx.image_path + 'dhxgrid_web/item_chk0.gif');
				}
			}
		}.bind(this.toolBar));
	};

	this.assignSelected = function (shipments, tabCode) {
		var i, t = '', temp = '';
		for(i = 0; i < shipments.length; i++) {
			t += shipments[i].airbillNo + ',';
			temp += shipments[i].origin + ',';
		}
		t = t.substr(0, t.length - 1);
		temp = temp.substr(0, temp.length - 1);
		this.tab.progressOn();
		sendAjaxRequest('Export/AssignSelectedShipment?airbillNo=' + t + '&origin=' + temp + '&planId=' + tabCode, 'POST', null, function (response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if (response && response.Data) {
				_this.reloadMainTab();
			}
			else {
				showAlert('Error', 'something went wrong');
			}
			_this.tab.progressOff();
		});
	};

	this.unvoidShipment = function () {
		this.unvoidWin = CM.W.createWindow("unvoid_housebills", 0, 0, 260, 150);
		this.unvoidWin.denyPark();
		this.unvoidWin.denyResize();
		this.unvoidWin.setText('Unvoid Housebills');
		this.unvoidWin.center();

		this.unvoidForm = this.unvoidWin.attachForm([
			{type: "input", name: "housebill_no", label: "Please Enter Housebill No:", position: "label-top", width: 215},
			{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
					{type: "button", name: "unvoid", value: "Unvoid"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		this.unvoidForm.attachEvent('onButtonClick', function (id) {
			var housebill;
			if (id === 'cancel') {
				_this.unvoidWin.close();
			}
			else if (id === 'unvoid') {
				housebill = this.getItemValue('housebill_no');
				if (housebill) {
					sendAjaxRequest('/Export/UnvoidHousebills?hawb=' + housebill + '&gateway=' + CM.gateway + '&userId=' + helpers.getUserName(), 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {
							if (response.Data.toLowerCase().indexOf('unable') > -1) {
								showAlert('Unvoid Housebil', response.Data);
							}
							else {
								_this.unvoidWin.close();
								_this.unvoidWin = null;
								_this.reloadMainTab();
							}
						}
					})
				}
				else {
					showAlert('Unvoid Shipment', 'Please enter a valid Shipment number')
				}
			}
		});
	};

	this.makeShipmentsKnownUnknown = function (shipments) {
		var i, promise = [];
		for (i = 0; i < shipments.length; i++) {
			promise.push(
				new Promise(function (resolve, reject) {
					sendAjaxRequest('Export/ShipmentStatus?airbillNo=' + shipments[i].airbillNo + '&origin=' + shipments[i].origin + '&userId=' + helpers.getUserName() + '&gateway=' + CM.gateway, 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && ((response.Status && response.Status.Status) || response.Data )) {
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function () {
			_this.reloadMainTab();
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
		});
	};

	this.makeHazmat = function (shipments) {
		var i, promise = [];
		for (i = 0; i < shipments.length; i++) {
			promise.push(
				new Promise(function (resolve, reject) {
					sendAjaxRequest('Export/HawbToggleHazmat?hawb=' + shipments[i].airbillNo + '&origin=' + shipments[i].origin + '&userId=' + helpers.getUserName(), 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && ((response.Status && response.Status.Status) || response.Data )) {
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function () {
			_this.reloadMainTab();
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
		});
	};

	this.overrideDims = function (shipments) {
		var i, promise = [];
		showConfirm('Are you sure you want to Override / Apply Dims Restriction on the Selected Shipment(s)?', function () {
			for (i = 0; i < shipments.length; i++) {
				promise.push(
					new Promise(function (resolve, reject) {
						sendAjaxRequest('Export/DimsRestructions?hawb=' + shipments[i].airbillNo + '&userId=' + helpers.getUserName(), 'POST', null, function (response) {
							response = dhx4.s2j(response.xmlDoc.responseText);
							if (response && ((response.Status && response.Status.Status) || response.Data )) {
								resolve();
							}
							else {
								reject();
							}
						});
					})
				);
			}
			Promise.all(promise).then(function () {
				_this.reloadMainTab();
			}).catch(function (err) {
				showAlert('Error', 'something went wrong');
			});
		});
	};

	this.makeDryice = function (shipments) {
		var i, promise = [];
		for (i = 0; i < shipments.length; i++) {
			promise.push(
				new Promise(function (resolve, reject) {
					sendAjaxRequest('Export/MakeShipmentDryice?hawb=' + shipments[i].airbillNo + '&origin=' + shipments[i].origin + '&userId=' + helpers.getUserName(), 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function (response) {
			_this.reloadMainTab();
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
		});
	};

	this.voidSelectedShipments = function (shipments) {
		var i, promise = [];
		showConfirm('Are you sure you want to VOID Selected Shipment(s)?', function () {
			for (i = 0; i < shipments.length; i++) {
				promise.push(
					new Promise(function (resolve, reject) {
						sendAjaxRequest('Export/VoidSelectedShipments?shipment=' + shipments[i].airbillNo + '&shipmentType=HAWB&userId=' + helpers.getUserName(), 'POST', null, function (response) {
							response = dhx4.s2j(response.xmlDoc.responseText);
							if (response && ((response.Status && response.Status.Status) || response.Data )) {
								resolve();
							}
							else {
								reject();
							}
						});
					})
				);
			}
			Promise.all(promise).then(function (response) {
				_this.reloadMainTab();
			}).catch(function (err) {
				showAlert('Error', 'something went wrong');
			});
		});
	};

	this.reloadMainTab = function () {
		dhx4.callEvent('selectTabById', [this.tab.getId()]);
	};


	/**
	 *
	 * @param isEmpty - parameter that show is consolidation will have data, false - get data from grid, true - empty consolidation
	 * @param isPlan
	 * @returns {boolean}
	 */

	this.createNewPlan = function (isEmpty, isPlan) {
		var key, selectedRows = '', ids = '', url = 'Export/AssignConsol?gateway=' + this.gateway + '&destination=' + this.destination + '&account=' + helpers.getUserName(), i;
		if (!isEmpty) {
			for (key in this.grids) {
				if (this.grids.hasOwnProperty(key)) {
					ids = this.grids[key].getCheckedIdsAsArray(0);
					if (ids.length > 0) {
						for (i = 0; i < ids.length; i++) {
							selectedRows += this.grids[key].getUserData(ids[i], 'Origin') + '-' + this.grids[key].getUserData(ids[i], 'AirbillNo') + ','
						}
					}
				}
			}
			if (selectedRows.length === 0 && isPlan) {
				showAlert('Loading Plan', 'Before creating Loading Plan please select shipment(s)');
				return false;
			}
			else {
				dhx4.callEvent('showAssignConsolOnLastTab', [])
			}
			selectedRows = selectedRows.substr(0, selectedRows.length - 1);
			url += '&cargoManifest=' + selectedRows;
			sendAjaxRequest(url, 'POST', null, this.afterCreatedConsolidation);
		}
		else if (this.showConsolWindow) {
			dhx4.callEvent('callAssignWindow', []);
		}
	};

	this.afterCreatedConsolidation = function (response) {
		var key, ids, i;
		response = dhx4.s2j(response.xmlDoc.responseText);
		if (response && response.Status && response.Status.Status) {
			for (key in this.grids) {
				if (this.grids.hasOwnProperty(key)) {
					ids = this.grids[key].getCheckedIdsAsArray(0);
					if (ids.length > 0) {
						for (i = 0; i < ids.length; i++) {
							this.grids[key].deleteRow(ids[i]);
							this.changeSectionDataAndSize(this.grids[key], key)
						}
					}
				}
			}
			dhx4.callEvent('onNewLoadingPlanCreate', [response.Data, this.showConsolWindow]);
			this.showAssignConsolWindow = false;
		}
	}.bind(this);

	this.changeSectionDataAndSize = function (grid, key, rowsNum) {
		var section = this.accordion.cells(key),
			rowsCount = (rowsNum || rowsNum === 0) ? rowsNum : grid.getRowsNum(),
			sectionHeight = (rowsCount ? 26 * rowsCount : 0) + section.cell.childNodes[0].offsetHeight + section.cell.childNodes[2].offsetHeight + 16 + grid.hdrBox.offsetHeight; // 16 - paddings
		section.setHeight(sectionHeight);
	}.bind(this);

	this.load = function (tabsData, routes) {
		var key, i = 1, j = 1;
		this.routes = routes;
		this.tab.progressOn();
		for (key in tabsData) {
			if (tabsData.hasOwnProperty(key) && tabsData[key].TabOrder === 1) {
				if(i === 1) {
					this.toolBar.addListOption('options', 'assign_to_plan_sep', 17 + i, 'separator');
					i++;
				}
				this.toolBar.addListOption('options', 'assign_to_plan_' + tabsData[key].Code, 17 + i, 'button', 'Assign Selected Shipments to: ' + tabsData[key].TabCaption);
				i++;
			}
			else if(tabsData.hasOwnProperty(key) && (tabsData[key].TabOrder === 2 || tabsData[key].TabOrder === 'consol')) {
				if(j === 1) {
					this.toolBar.addListOption('options', 'assign_to_consol_sep', 200 + j, 'separator');
					j++;
				}
				this.toolBar.addListOption('options', 'assign_to_consol_' + tabsData[key].TabCode, 200 + j, 'button', 'Assign Selected Shipments to: ' + tabsData[key].TabCaption);
				j++;
			}
		}
		if (CM.localhost) {
			_this.fillTab(dhx4._copyObj(viewWeightTab));
			_this.tab.progressOff();
		}
		else {
			sendAjaxRequest('Export/GetShipments?gateway=' + this.gateway + '&destination=' + this.destination + '&account=' + helpers.getUserName(), 'GET', null, this.onAfterLoad);
		}
	};

	this.onAfterLoad = function (response) {
		var data = dhx4.s2j(response.xmlDoc.responseText);
		if (data && data.Status && data.Status.Status) {
			_this.fillTab(data);
			_this.tab.progressOff();
		}
	};

	this.fillTab = function (data) {
		var i;
		for (i = 0; i < data.Data.length; i++) {
			this.pageData[data.Data[i].id] = data.Data[i];
			this.toolBar.addListOption('groups', data.Data[i].id, data.Data[i].id + 3, 'button', data.Data[i].nodeText, data.Data[i].data || this.displayedGrids[data.Data[i].id] ? dhtmlx.image_path + 'dhxgrid_web/item_chk1.gif' : dhtmlx.image_path + 'dhxgrid_web/item_chk0.gif');
			if (data.Data[i].data || this.displayedGrids[data.Data[i].id]) {
				this.addGroup(data.Data[i]);
			}
		}
		this.displayedGrids = {};
	};

	this.clearAll = function () {
		var key;
		for (key in this.grids) {
			if (this.grids.hasOwnProperty(key)) {
				this.removeGroup(key);
			}
		}
		this.toolBar.forEachListOption('groups', function (id) {
			if (id === 'check_all' || id === 'uncheck_all' || id === 'check_sep') {
				return true;
			}
			var imagePath = this.getListOptionImage('groups', id);
			_this.displayedGrids[id] = (imagePath.indexOf('item_chk1') > -1) ? true : false;
			this.removeListOption('groups', id);
		}.bind(this.toolBar));
		this.toolBar.forEachListOption('options', function (id) {
			if (id.indexOf('assign_to_') > -1) {
				this.removeListOption('options', id);
			}
		}.bind(this.toolBar));
		this.pageData = {};
	};

	this.addGroup = function (data) {
		var section = this.accordion.addItem(data.id, data.nodeText),
			grid, types = 'ch,', i,
			sortType = 'na,', colAlign = 'center,', sectionHeight,
			totalHawb = data.total_hawb ? data.total_hawb : 0,
			totalPcs = data.total_pcs ? data.total_pcs : 0,
			totalGross = data.total_gross_w ? data.total_gross_w : 0,
			totalCF = data.total_cf ? data.total_cf : 0,
			totalCH = data.total_ch_wt ? data.total_ch_wt : 0,
			headerData = data.ColumnNames.split(',');
		headerData.unshift('#master_checkbox');
		for (i = 1; i < headerData.length; i++) {
			types += 'ro,';
			sortType += 'str,';
			colAlign += 'center,';
		}
		if (data.data) {
			for (i = 0; i < data.data.rows.length; i++) {
				data.data.rows[i].data.unshift('0');
			}
		}
		section.attachStatusBar({
			text: '<div class="inline_childs"><div>Total HAWB#: ' + totalHawb + '</div><div>Total Pcs: ' + totalPcs + '</div><div>Total Gross Wt.: ' + totalGross + '</div>' +
			'<div>Total CF: ' + totalCF + '</div><div>Total Ch. Wt.: ' + totalCH + '</div></div>', // status bar init text
			height: 20     // status bar init height
		});
		grid = section.attachGrid();
		types = types.substr(0, types.length - 1);
		colAlign = colAlign.substr(0, types.length - 1);
		sortType = sortType.substr(0, sortType.length - 1);
		grid.setHeader(headerData);
		grid.setIconPath(baseUrl);
		grid.setImagesPath(dhtmlx.image_path);

		grid.setColSorting(sortType);
		grid.setColTypes(types);
		grid.setColAlign(colAlign);
		grid.setInitWidths('40,' + data.Widths);
		grid.init();
		if (data.data) {
			grid.parse(data.data, 'json');
		}
		grid.attachEvent('onRowDblClicked', function(rowId) {
			var data = [{
				'airbillNo': this.getUserData(rowId, 'AirbillNo'),
				'origin': this.getUserData(rowId, 'Origin')
			}];
			dhx4.callEvent('showShipmentDetailsWindow', [data]);
		});
		sectionHeight = (data.data ? 26 * data.data.rows.length : 0) + section.cell.childNodes[0].offsetHeight + section.cell.childNodes[2].offsetHeight + 16 + grid.hdrBox.offsetHeight; //31 - row height, 35 * 2 - header and footer height 16 - paddings
		section.setHeight(sectionHeight);
		grid.gridName = data.nodeText;
		this.grids[data.id] = grid;
	};

	this.removeGroup = function (id) {
		if (_this.grids[id]) {
			this.grids[id].destructor();
			delete this.grids[id];
		}
		this.accordion.removeItem(id);
	};

	this.addDelayReason = function (shipments) {
		var temp = '', i;
		for (i = 0; i < shipments.length; i++) {
			temp += shipments[i].airbillNo + '-' + shipments[i].origin + ',';
		}
		temp = temp.substr(0, temp.length - 1);
		this.reasonShipments = shipments;
		//init window

		this.delWin = CM.W.createWindow("delay_reason", 0, 0, 300, 210);
		this.delWin.denyPark();
		this.delWin.denyResize();
		this.delWin.setText('Add Delay Reason');
		this.delWin.center();

		this.delWin.attachEvent('onClose', function () {
			_this.delWin = null;
			return true;
		});

		this.delWin.progressOn();

		//init form

		this.form = this.delWin.attachForm([
			{type: "template", name: "shipment", label: 'Shipment:', offsetTop: 3, format: CM.f.textTemplate},
			{type: "combo", name: "reason", label: "Reason:", width: 215, options: []},
			{type: 'input', name: 'remark', label: "Remark:", rows: 3, width: 215},
			{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 44, offsetTop: 5, list: [
					{type: "button", name: "save_reason", value: "Save"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		this.form.attachEvent('onButtonClick', function (id) {
			var reason, reasonText, remark, temp = '';
			if (id === 'cancel') {
				_this.delWin.close();
			}
			else if (id === 'save_reason') {
				reason = this.getCombo('reason');
				remark = this.getItemValue('remark');
				if (reason.getSelectedValue() !== 'Select One' && remark) {
					reasonText = reason.getSelectedValue() + '-' + reason.getSelectedText();
					for (i = 0; i < _this.reasonShipments.length; i++) {
						temp += shipments[i].airbillNo + ','
					}
					temp = temp.substr(0, temp.length - 1);
					sendAjaxRequest('/Export/AddDelayReason?reasonFor=HAWB&shipments=' + temp + '&reason=' +reasonText + '&remark=' + remark + '&carrier=&mawb', 'POST', null, function(response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if(response && response.Data){
							_this.reasonShipments = null;
							_this.delWin.close();
						}
					});
				}
				else {
					showAlert('Delay Reason', 'Please selected recovery delay reason.');
				}
			}
		});

		sendAjaxRequest('Export/GetReasons', 'GET', null, function (response) {
			var i, opt = [{text: 'Select One', value: 'Select One', selected: true}];
			response = dhx4.s2j(response.xmlDoc.responseText);
			if (response && response.Data) {
				for (i = 0; i < response.Data.length; i++) {
					opt.push({
						value: response.Data[i].Code,
						text: response.Data[i].Reason
					});
					_this.form.reloadOptions('reason', opt)
				}
			}
			_this.delWin.progressOff();
			opt = null;
		});
	};


	this.expandCollapseAll = function (state) {
		var action = state ? 'open' : 'close';
		_this.accordion.forEachItem(function (cell) {
			cell[action]();
		});
	};

	this.updateUPSZones = function(data){
		var i, promise = [], type = ['GROUND', 'SECONDDAY', 'NEXTDAY'], zipCodes = '', shipments = '';
		this.upsWin = CM.W.createWindow("update_ups_zone", 0, 0, 400, 320);
		this.upsWin.denyPark();
		this.upsWin.denyResize();
		this.upsWin.setText('UPS Zone Update');
		this.upsWin.center();
		for(i = 0; i < data.length; i++) {
			zipCodes += data[i].zipcode ? data[i].zipcode + ',': '';
			shipments += data[i].airbillNo + ',';
		}
		zipCodes = zipCodes.substr(0, zipCodes.length - 1);
		shipments = shipments.substr(0, shipments.length - 1);

		this.upsWin.attachToolbar({
			json: [
				{id: 'ups', type: 'text', text: '<span class="bold">UPS Zone Update</span>'}
			]
		});

		this.upsForm = this.upsWin.attachForm([
			{type: "template", name: "shipments", label: 'Shipment(s):', labelWidth: 110, format: CM.f.textTemplate},
			{type: "template", name: "zipCode", label: 'Zip Code:', labelWidth: 110, format: CM.f.textTemplate},
			{type: "template", label: 'Please select zone information from the following dropdowns list for individual service type.', labelWidth: 320},
			{name: 'GROUND', type: 'combo', label: 'Ground:', labelWidth: 100, width: 200, options: []},
			{name: 'SECONDDAY', type: 'combo', label: 'Second Day:', labelWidth: 100, width: 200, options: []},
			{name: 'NEXTDAY', type: 'combo', label: 'Next Day:', labelWidth: 100, width: 200, options: []},
			{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
					{type: "button", name: "save", value: "Save"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		this.upsForm.setItemValue('shipments', shipments);
		this.upsForm.setItemValue('zipCode', zipCodes);

		this.upsForm.attachEvent('onButtonClick', function (id) {
			var housebill, formData = this.getFormData();
			if (id === 'cancel') {
				_this.upsWin.close();
			}
			else if (id === 'save') {
				if(!formData.GROUND) {
					showAlert('UPS Zone Update', 'Please select Zone for ground service type');
					return false;
				}
				else if(!formData.SECONDDAY) {
					showAlert('UPS Zone Update', 'Please select Zone for second day service type');
					return false;
				}
				else if(!formData.NEXTDAY) {
					showAlert('UPS Zone Update', 'Please select Zone for next day service type');
					return false;
				}
				for(i = 0; i < 3; i++) {
					promise.push(
						new Promise(function (resolve, reject) {
							sendAjaxRequest('/Export/UpdateUpsZone?zipCode='+formData.zipCode+'&zoneCode='+formData[type[i]]+'&serviceCode='+type[i]+'&gatewayId=' + CM.gateway, 'POST', null, function (response) {
								var data = dhx4.s2j(response.xmlDoc.responseText);
								console.log(data);
								if (data && data.Data) {
									resolve();
								}
								else {
									reject();
								}
							});
						})
					);
				}
				Promise.all(promise).then(function () {
					_this.upsWin.progressOff();
					_this.upsWin.close();
				}).catch(function (err) {
					showAlert('Error', 'something went wrong');
				});
			}
		});

		//fill UPS win
		this.upsWin.progressOn();
		for(i = 0; i < type.length; i++) {
			promise.push(
				new Promise(function (resolve, reject) {
					sendAjaxRequest('Export/GetCarrierZones?serviceType=' + type[i], 'GET', null, function (response) {
						var data = dhx4.s2j(response.xmlDoc.responseText);
						if (data && data.Data) {
							_this.fillUPSCCombos(data.Data);
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function () {
			_this.upsWin.progressOff();
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
		});
	};

	this.fillUPSCCombos = function(data) {
		var i, opt = [];
		if(data.data && data.data.length > 0) {
			for (i = 0; i < data.data.length; i++) {
				opt.push({
					text: data.data[i].label,
					value: data.data[i].ZoneCode
				})
			}
			this.upsForm.reloadOptions(data.serviceType, opt);
		}
		opt = null;
	}.bind(this);


	this.init();

	return this;
};

dhx4.attachEvent('onMainTabSelect', function (tab, gateway, destination, tabsData, routes) {
	if (tab && !mainTabs[tab.getId()]) {
		mainTabs[tab.getId()] = new mainTab(tab, gateway, destination);
	}
	else {
		mainTabs[tab.getId()].clearAll();
	}
	mainTabs[tab.getId()].load(tabsData, routes);
});

dhx4.attachEvent('unloadMainTabArea', function (tab) {
	if (tab && mainTabs[tab.getId()]) {
		delete mainTabs[tab.getId()];
	}
});
