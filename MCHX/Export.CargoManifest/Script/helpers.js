﻿if (typeof (window.helpers) == "undefined") {
    window.helpers = {
        _getCookie: function (cname) {
            var name = cname + '_' + window.location.host + ((window.location.port == "80" || window.location.port == "") ? ":80" : "") + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        },

        _deleteAuthCookie: function () {
            if (this._getCookie('WebApiAuth')) {
                document.cookie = 'WebApiAuth_' + window.location.host + "=" + "domain= " + window.location.hostname + ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
            }
        },

        getUserName: function () {
            return this._getCookie('UserName');
        },

        exportToExcel: function (url, filter, fileName, callback, method) {
            $.ajax({
                method: method || 'GET',
                url: WebApiUrl + url,
                data: filter,
                headers: {
                    'Authorization': "Bearer " + this._getCookie('WebApiAuth')
                },
                success: function (data) {
                    var dataType = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,";
                    var dataForExport = dataType + data;

                    var a = document.createElement("a");
                    a.href = dataForExport;
                    a.download = fileName || 'ExcelFile.xlsx';
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function () { document.body.removeChild(a); }, 100);
                    if (callback) {
                        callback(data);
                    }
                },
                error: function (error) {
                    helpers.showAjaxLoadingError(error);
                }
            });
        },

        executePost: function (url, data, callback, authToken) {
            $.ajax({
                type: 'POST',
                url: WebApiUrl + url,
                data: data,
                dataType: 'json',
                headers: {
                    'Authorization': "Bearer " + (authToken || this._getCookie('WebApiAuth'))
                },
                success: function (data) {
                    if (callback) {
                        callback(data);
                    }
                },
                error: function (error) {
                    helpers.showAjaxLoadingError(error);
                }
            });
        },

        reminder: function (interval, repeat, text, yesCallback, noCallback, timerElapsed) {
            var timer = setTimeout(function () {
                var element = dhtmlx.confirm({
                    type: "confirm",
                    text: text,
                    callback: function (result) {                        
                        if (result) {
                            clearTimeout(timer);

                            if (yesCallback) {                                
                                yesCallback();
                            }

                            if (repeat) {
                                console.log('repeat');
                                this.reminder(interval, repeat, text, yesCallback, noCallback, timerElapsed);
                            }
                        } else {
                            if (noCallback) {                                
                                noCallback();
                            }
                        }
                    }.bind(this)
                });

                if (timerElapsed) {                    
                    timerElapsed(element, timer);
                }
            }.bind(this), interval * 1000);
            return timer;
        },

        documentCapturerControl: function (documentTypesUrl, callback) {
            var windows = new dhtmlXWindows();
            var captureWindow = windows.createWindow('captureWindow', 0, 0, 600, 380);
            var form = null;
            var formStructure = [
                {
                    type: "input", name: "FileName", label: "File Name", position: "label-top", inputWidth: 170, required: true, className: "req-asterisk",
                    validate: function (input) { return input && input.trim() !== "" }
                },

                { type: "newcolumn" },

                {
                    type: "combo", name: "FileType", label: "File Type", position: "label-top", inputWidth: 200, offsetLeft: 10, required: true, className: "req-asterisk",
                    validate: function (input) { return form.getCombo("FileType").getSelectedIndex() !== -1 }
                }
            ];

            form = captureWindow.attachForm(formStructure);
            form.enableLiveValidation(true);
            var fileType = form.getCombo('FileType');
            fileType.loadExternal(documentTypesUrl, 'Id', 'Name');

            fileType.attachEvent("onChange", function (id) {
                var option = this.getOption(id);                
                form.setItemValue('FileName', option ? option.customData.item.Name : '')
            });

            captureWindow.show();
            captureWindow.setText("Capture");
            captureWindow.setModal(1);
            captureWindow.denyResize();
            captureWindow.denyPark();
            captureWindow.centerOnScreen();

            var container = document.createElement('DIV');
            container.setAttribute('style', 'height: 80%; width: 100%;');

            var video = document.createElement('VIDEO');
            video.setAttribute('style', 'max-width: 48%; height: 95%; float: left;');

            var image = document.createElement('IMG');
            image.setAttribute('style', 'width: 274px; height: 206px; margin-top: 21px; float: right; border-style: solid; border-width: 0px; border-color: red;');

            container.appendChild(video);
            container.appendChild(image);

            var controlsContainer = document.createElement('DIV');
            controlsContainer.setAttribute('style', 'display:inline-block; float: right; padding-top: 27px;');

            var captureBtn = document.createElement('DIV');
            captureBtn.innerHTML = 'Capture';
            captureBtn.setAttribute('style', 'margin-right:10px; color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            var acceptBtn = document.createElement('DIV');
            acceptBtn.innerHTML = 'Accept';
            acceptBtn.setAttribute('style', 'color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            controlsContainer.appendChild(acceptBtn);
            controlsContainer.appendChild(captureBtn);
            
            captureWindow.cell.firstChild.firstChild.insertBefore(controlsContainer, captureWindow.cell.firstChild.firstChild.firstChild);
            captureWindow.cell.firstChild.firstChild.insertBefore(container, captureWindow.cell.firstChild.firstChild.firstChild);
            
            var mediaHandler = new MediaHandler();
            mediaHandler.start(video, image);

            captureWindow.attachEvent('onClose', function () {
                mediaHandler.stop();                
                return true;
            });

            captureBtn.onclick = function () {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");
                var cropHeight = video.clientHeight;
                var cropWidth = video.clientWidth;
                canvas.width = cropWidth;
                canvas.height = cropHeight;
                context.drawImage(video, 0, 0, cropWidth, cropHeight);
                image.src = canvas.toDataURL("image/jpg");
                image.style.borderWidth = "0px";
            }

            acceptBtn.onclick = function () {
                if (form.validate() && image.src) {
                    if (callback) {
                        var data = form.getFormData();
                        data.image = image.src;
                        callback.call(this, data);
                    }
                    captureWindow.close();
                }
                if (image.src === "")
                    image.style.borderWidth = "1px";
            }.bind(this);
        },

        deepValue: function (obj, path) {
            for (var i = 0, path = path.split('.'), len = path.length; i < len; i++) {
                obj = obj[path[i]];
            };
            return obj;
        },

        getDataFromExternalResource: function (url, params, callback) {
            $.ajax({
                type: 'GET',
                url: WebApiUrl + url,
                data: params,
                headers: {
                    'Authorization': "Bearer " + getCookie('WebApiAuth')
                },
                success: function (data) {
                    if (callback) {
                        callback(data);
                    }
                },
                error: function (message) {                    
                    helpers.showAjaxLoadingError(message);
                }
            });
        },

        showAjaxLoadingError: function (error) {            
            if (error.status == 401) {                
                dhx4.ajax.post('../Account/LogOff', function (a) {
                    helpers._deleteAuthCookie();
                    window.location.href = window.location.origin + window.baseUrl + 'Account/Login';
                });
                return;
            }

            dhtmlx.alert({
                title: "Warning",
                type: "warning",
                text: "Something went wrong, please try again later!"
            });
        },

        uploaderFormTemplate: function(name, value) {
            var form = this.getForm();
            $(this).on("change", ".customUpload", (function (fileEv) {
                if (!fileEv)
                    return;

                if (fileEv.target.files[0]) {

                    var reader = new FileReader();

                    reader.onload = (function(a) {
                        console.log(a.target.result);

                        form.setItemValue(name, a.target.result);
                        $($(form._getItemByName(name)).find("input")[0]).attr('value', fileEv.target.files[0].name);
                    });

                    reader.readAsDataURL(fileEv.target.files[0]);
                }
            }));

            $(this).on("keypress", ".customFileUploadBtn", function (ev) {
                if (ev && ev.keyCode === 13 && $(".customUpload"))
                    $(".customUpload").click();
                else
                    return;
            });

            return  '<div class="customFileUpload btn btn-primary">' +
                        '<input class="dhxform_textarea customUploadText" placeholder="Select File" readonly="true">' +
                        '<div class="dhxform_btn customFileUploadBtn" role="link" tabindex="0">' +
                            '<div class="dhxform_btn_txt">Select File</div>' +
                            '<div class="dhxform_btn_filler" disabled="true"></div>' +
                        '</div>' +
                        '<input type="file" accept="image/*" class="customUpload">' +
                    '</div>';
        }
    }
}

dhtmlXGridObject.prototype.getSelectedIdsAsArray = function () {
    var selectedIdArray = [];
    var selectedIdsString = this.getSelectedRowId();
    if (selectedIdsString) {
        selectedIdsString.split(",").forEach(function (value) {
            if (!isNaN(value))
                selectedIdArray.push(new Number(value).valueOf());
        });
    }
    return selectedIdArray;
}

dhtmlXGridObject.prototype.highlightCellsByValue = function (value) {
    $.each(this.rowsBuffer, function (i, currentRow) {
        $(currentRow).find('td').css({ backgroundColor: '' });
        if (value) {
            $(currentRow.cells).each(function (index) {
                if (this.textContent.toUpperCase().indexOf(value.toUpperCase()) !== -1) {
                    $(this).css({ backgroundColor: 'darkkhaki' });
                }
            });
        }
    });
}

dhtmlXGridObject.prototype.makePageable = function (pageSize) {
    this._loadedPages = [0];
    this._pageSize = pageSize || 30;

    var _paging = function (pageNumber, count) {
        this.filters = this.filters || {};
        this.filters.pageNumber = pageNumber;
        this.filters.count = count;
        this.loadExternal(this._externalUrl, this.filters);
    }

    this.attachEvent("onScroll", function (sLeft, sTop) {
        var gridPosition = this.getStateOfView();
        var count = gridPosition[1] > this._pageSize ? gridPosition[1] : this._pageSize;
        var pageNumber = parseInt(gridPosition[0] / count) + 1;
        if (this._loadedPages.indexOf(pageNumber) === -1) {
            _paging.call(this, pageNumber, count);

            $("#" + this.entBox.id).find('.smartRenderProgress').remove();
            $("#" + this.entBox.id).append("<div class='smartRenderProgress dhx_cell_progress_img' style='width:35px;height:35px; top:calc(100% - 53px); display:block; position:absolute; left: calc(100% - 53px);'></div>");

            this._loadedPages.push(pageNumber);
        }
    }.bind(this));

    this.attachEvent("onXLE", function () {
        $("#" + this.entBox.id).find('.smartRenderProgress').remove();
    }.bind(this));
}

dhtmlXTreeObject.prototype.loadExternal = function (url, filters, callback) {
    this._externalUrl = url;
    this.filters = filters || {};

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (result) {
            this.deleteChildItems(0);
            this.parse(result, "xml");
            if (callback) {
                callback();
            }
        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXGridObject.prototype.loadExternal = function (url, filters, callback) {
    this._externalUrl = url;
    this.filters = filters || {};
    this.filters.pageNumber = this.filters.pageNumber || 0;
    this.filters.count = this.filters.count || this._pageSize || 10000;

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (result) {
            this.parse(result, 'json');
            if (callback) {
                callback();
            }
        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXChart.prototype.loadExternal = function (url, filters, onBeforeRender, onAfterRender) {
    this._externalUrl = url;
    this.filters = filters || {};

    $.ajax({
        type: 'GET',
        url: WebApiUrl + this._externalUrl,
        data: this.filters,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (result) {
            if (onBeforeRender) {
                onBeforeRender(result);
            }

            var convertedData = [];
            for (var i = 0; i < result.Data.length; i++) {
                var tmpObj = {};
                $.each(result.Data[i], function (key, value) {
                    tmpObj[key.replace(/ /g, '')] = value;
                });
                convertedData.push(tmpObj);
            }

            this.parse(convertedData, 'json');
            if (onAfterRender) {
                onAfterRender(result);
            }

        }.bind(this),
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXCombo.prototype.loadExternal = function (url, dataKeyField, dataTextField, callback) {
    $.ajax({
        type: 'GET',
        url: WebApiUrl + url,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (data) {
            if (data.Status && !data.Status.Status) {
                //show error message. not sure for combo
                return;
            }
            var comboData = $.map(data.Data || data, function (item) {
                return { value: helpers.deepValue(item, dataKeyField), text: helpers.deepValue(item, dataTextField), item: item }
            });
            
            this.load(comboData, function() {
                if (callback)
                    callback();
            });

        }.bind(this)
    });
}

dhtmlXMenuObject.prototype.loadExternal = function (url, params, callback) {
    helpers.getDataFromExternalResource.call(this, url, params, function (data) {        
        this.loadStruct(data);
    }.bind(this));
}

function getCookie(cname) {    
    var name = cname + '_' + window.location.host + ((window.location.port == "80" || window.location.port == "") ? ":80" : "") + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function loadGridDataFromExternalResource(url, data, control, type, callback) {
    $.ajax({
        type: type || 'GET',
        url: WebApiUrl + url,
        data: data,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (result) {
            control.parse(result, 'json');
            if (callback) {
                callback();
            }
        }
    });
}

function loadDataFromExternalResource(url, control, method, callback) {
    $.ajax({
        method: method || 'GET',
        url: WebApiUrl + url,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (data) {
            control.load(data, function () { if (callback) callback(data) });
        }
    });
}

function getDataFromExternalResource(url, callback, method) {
    $.ajax({
        method: method || 'GET',
        url: WebApiUrl + url,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (data) {
            if (callback) {
                callback(data);
            }
        },
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

function executePostOnExternalResource(url, data, callback) {
    $.ajax({
        type: 'POST',
        url: WebApiUrl + url,
        data: data,
        dataType: 'json',
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        },
        success: function (data) {
            if (callback) {
                callback(data);
            }
        },
        error: function (error) {
            helpers.showAjaxLoadingError(error);
        }
    });
}

dhtmlXForm.prototype.items.imageCapturer = {
    render: function (item, data) {
        item._type = "imageCapturer";
        item._enabled = true;
        item._id = data.id;
        item._useExternalButton = data.useExternalButton || false;

        item.setAttribute('style', 'height: ' + data.controlHeight);

        var image = document.createElement("IMG");
        image.setAttribute('id', data.id);
        image.setAttribute('style', 'height: ' + data.imageHeight + '; width: ' + data.imageWidth + ';');

        var button = this._createButton.call(item, data.buttonText, item._useExternalButton);
        var label = this._createLabel.call(item, data.labelText, data.labelWidth);

        var controlsContainer = document.createElement("DIV");

        controlsContainer.appendChild(label);
        controlsContainer.appendChild(button);
        item.appendChild(controlsContainer);
        item.appendChild(image);
        item.image = image;
        return this;
    },

    destruct: function (item) { }, //destructor
    setValue: function (item, value) {
        item.image.src = value;
    },
    getValue: function (item) {
        return item.image.src;
    },
    enable: function (item) { },
    disable: function (item) { },

    _setImageSrc: function () {
        console.log(this.id);
    },

    _createLabel: function (text, width) {
        var labelContainer = document.createElement('DIV');
        labelContainer.setAttribute('style', 'display: inline-block; float:left;');
        labelContainer.setAttribute('class', 'dhxform_item_label_top');

        var labelText = document.createElement('DIV');
        labelText.setAttribute('class', 'dhxform_txt_label2 topmost');
        labelText.setAttribute('style', 'width: ' + width);
        labelText.innerHTML = text;
        labelContainer.appendChild(labelText);
        return labelContainer;
    },

    _createButton: function (text) {
        var buttonContainer = document.createElement('DIV');
        buttonContainer.setAttribute('dir', 'ltr');
        buttonContainer.setAttribute('role', 'link');
        buttonContainer.setAttribute('class', 'dhxform_btn');
        buttonContainer.setAttribute('style', 'display: inline-block; float: none; cursor: pointer;');


        var buttonText = document.createElement('DIV');
        buttonText.setAttribute('class', 'dhxform_btn_txt');
        buttonText.innerHTML = text;

        var buttonFilter = document.createElement('DIV');
        buttonFilter.setAttribute('disabled', 'true');
        buttonFilter.setAttribute('class', 'dhxform_btn_filler');

        buttonContainer.appendChild(buttonText);
        buttonContainer.appendChild(buttonFilter);

        buttonContainer.onclick = function () {
            var windows = new dhtmlXWindows();
            var captureWindow = windows.createWindow('captureWindow', 0, 0, 600, 380);
            captureWindow.show();
            captureWindow.setText("Capture");
            captureWindow.setModal(1);
            captureWindow.denyResize();
            captureWindow.denyPark();
            captureWindow.centerOnScreen();

            var container = document.createElement('DIV');
            container.setAttribute('style', 'height: 88%; width: 100%;');

            var video = document.createElement('VIDEO');
            video.setAttribute('style', 'max-width: 48%; height: 95%; float: left;');

            var image = document.createElement('IMG');
            image.setAttribute('style', 'width: 274px; height: 206px; margin-top: 34px; float: right;');

            container.appendChild(video);
            container.appendChild(image);

            var controlsContainer = document.createElement('DIV');
            controlsContainer.setAttribute('style', 'height: 12%; width: 100%;');

            var captureBtn = document.createElement('DIV');
            captureBtn.innerHTML = 'Capture';
            captureBtn.setAttribute('style', 'margin-right:10px; color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            var acceptBtn = document.createElement('DIV');
            acceptBtn.innerHTML = 'Accept';
            acceptBtn.setAttribute('style', 'color: white; background-color: #3da0e3; display: inline-block; float: right; text-align: center; vertical-align: middle; white-space: nowrap; line-height: 23px; height: 24px; padding: 1px 14px; cursor: pointer; font-family: Tahoma,Helvetica;font-size:12px;');

            controlsContainer.appendChild(acceptBtn);
            controlsContainer.appendChild(captureBtn);

            captureWindow.cell.firstChild.appendChild(container);
            captureWindow.cell.firstChild.appendChild(controlsContainer);

            var mediaHandler = new MediaHandler();
            mediaHandler.start(video, image);

            captureWindow.attachEvent('onClose', function () {
                mediaHandler.stop();                
                return true;
            });

            captureBtn.onclick = function () {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");
                var cropHeight = video.clientHeight;
                var cropWidth = video.clientWidth;
                canvas.width = cropWidth;
                canvas.height = cropHeight
                context.drawImage(video, 0, 0, cropWidth, cropHeight);
                image.src = canvas.toDataURL("image/jpg");
            }

            acceptBtn.onclick = function () {
                this.image.src = image.src;
                captureWindow.close();
            }.bind(this);
        }.bind(this);

        return buttonContainer;
    }
}

function submitForm(title, formStructure, submitCallback, width, height, buttonName) {
    var wndWidth = width ? width : 400;
    var wndHeight = height ? height : 400;
    this.onSubmit = submitCallback;

    var init = function () {
        var windows = new dhtmlXWindows();
        var window = windows.createWindow('submitFormWindow', 0, 0, wndWidth, wndHeight);

        var tmpFormStructure = formStructure.slice();
        tmpFormStructure.push({
            type: 'block',
            list: [
                { type: "button", name: "submitButton", value: buttonName || "SUBMIT", offsetLeft: wndWidth - 132, offsetTop: 15 }
            ]
        });
        var form = window.attachForm(tmpFormStructure);

        form.attachEvent("onButtonClick", function () {
            var formData = form.getFormData();
            if (form.validate()) {
                window.progressOn();
                if (this.onSubmit) {
                    this.onSubmit(formData);
                }
            }
        }.bind(this));

        window.setText(title);
        window.setModal(1);
        window.denyResize();
        window.centerOnScreen();
        this.form = form;
        this.window = window;
    }.apply(this);
};


dhtmlXGridObject.prototype.getCheckedIdsAsArray = function (colIndex) {
    var t = new Array();
    this.forEachRowA(function (id) {
        var cell = this.cells(id, colIndex);
        if (cell.changeState && cell.getValue() != 0)
            t.push(id);
    }, true);
    return t;
};

function sendAjaxRequest(url, method, data, callBack) {
    dhx4.ajax.query({
        method: method,
        url: window.WebApiUrl + url,
        data: data,
        dataType: 'json',
        async: true,
        callback: callBack,
        headers: {
            'Authorization': "Bearer " + getCookie('WebApiAuth')
        }
    });
}

function showAlert(header, text) {
    dhtmlx.alert({
        title: header,
        type: 'alert',
        text: text
    });
}

function showConfirm(text, callBack) {
    dhtmlx.confirm({
        title: 'Confirm',
        type: 'confirm',
        text: text,
        callback: function (result) {
            if (result && callBack) {
                callBack();
            }
        }
    });
}

function eXcell_button(cell) {
    if (cell) {
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.edit = function () {
    };
    this._doOnClick = function(item) {
        this.grid.callEvent('onRowButtonClick', [this.cell.parentNode.idd, this.cell._cellIndex, item.childNodes[0].innerHTML]);
    };
    this.isDisabled = function () {
        return true;
    };
    this.setValue = function (val) {
        this.setCValue('<div class="dhxform_btn" role="link" tabindex="0" dir="ltr" onclick="new eXcell_button(this.parentNode)._doOnClick(this); (arguments[0]||event).cancelBubble=true;"><div class="dhxform_btn_txt">' + val + '</div><div class="dhxform_btn_filler" disabled="true"></div></div>', val);
    }
}
eXcell_button.prototype = new eXcell;// nests all other methods from the base class

dhtmlXForm.prototype.items.inputClock = {

    render: function(item, data) {
        var t = document.createElement('div'),
            d = document.createElement('div'),
            u = document.createElement('div'),
            y = document.createElement('div'),
            self = this;
        t.className = 'arrows_container';
        u.className = 'arrow_up';
        d.className = 'arrow_down';
        y.className = 'space';
        u.onclick = function () {
            var val = item._value;
            val = val.split(':');
            val[0] = parseInt(val[0], 10);
            val[1] = parseInt(val[1], 10);
            if(val[1] === 59) {
                val[1] = '00';
                if(val[0] !== 23) {
                    val[0] += 1;
                }
                else {
                    val[0] = '00';
                }
            }
            else {
                val[1] += 1;
            }
            self.setValue(item, (val[0] < 10 && val[0] !== '00'? '0' +val[0]: val[0])  + ":" + (val[1] < 10 && val[1] !== '00' ? '0' + val[1]: val[1]));
        };
        d.onclick = function () {
            var val = item._value;
            val = val.split(':');
            val[0] = parseInt(val[0], 10);
            val[1] = parseInt(val[1], 10);
            if(val[1] === 0) {
                val[1] = '59';
                if(val[0] !== 0) {
                    val[0] -= 1;
                }
                else {
                    val[0] ='23';
                }
            }
            else {
                val[1] -= 1;
            }
            self.setValue(item, (val[0] < 10 && val[0] !== '00'? '0' +val[0]: val[0])  + ":" + (val[1] < 10 && val[1] !== '00' ? '0' + val[1]: val[1]));
        };
        t.appendChild(d);
        t.appendChild(y);
        t.appendChild(u);
        item._type = "ta";
        item._enabled = true;
        this.doAddLabel(item, data);
        this.doAddInput(item, data, "INPUT", "TEXT", true, true, "dhxform_textarea");
        this.doAttachEvents(item);
        t.style.top = item.childNodes[1].childNodes[0].offsetTop + 3 + 'px';
        //t.style.left = item.childNodes[1].childNodes[0].offsetWidth - 18 + 'px';
        item.childNodes[1].appendChild(t);

        if (typeof(data.numberFormat) != "undefined") {
            var a,b=null,c=null;
            if (typeof(data.numberFormat) != "string") {
                a = data.numberFormat[0];
                b = data.numberFormat[1]||null;
                c = data.numberFormat[2]||null;
            } else {
                a = data.numberFormat;
                if (typeof(data.groupSep) == "string") b = data.groupSep;
                if (typeof(data.decSep) == "string") c = data.decSep;
            }
            this.setNumberFormat(item, a, b, c, false);
        }
        this.setValue(item, data.value);
        return this;
    },

    doAttachEvents: function(item) {

        var that = this;

        if (item._type == "ta" || item._type == "se" || item._type == "pw") {
            item.childNodes[item._ll?1:0].childNodes[0].onfocus = function() {
                var i = this.parentNode.parentNode;
                if (i._df != null) this.value = i._value||"";
                i.getForm()._ccActivate(i._idd, this, this.value);
                i.getForm().callEvent("onFocus",[i._idd]);
                i = null;
            }
        }

        item.childNodes[item._ll?1:0].childNodes[0].onblur = function() {
            var i = this.parentNode.parentNode;
            i.getForm()._ccDeactivate(i._idd);
            that.updateValue(i, true);
            if (i.getForm().live_validate) that._validate(i);
            i.getForm().callEvent("onBlur",[i._idd]);
            i = null;
        }
    },

    updateValue: function(item, foc) {
        var value = item.childNodes[item._ll?1:0].childNodes[0].value;
        var form = item.getForm();
        var in_focus = (form._ccActive == true && form._formLS != null && form._formLS[item._idd] != null);
        form = null;
        if (!in_focus && item._df != null && value == window.dhx4.template._getFmtValue(item._value, item._df)) return; // if item not in focus
        if (!foc && item._df != null && item._value == value && value == window.dhx4.template._getFmtValue(value, item._df)) return;
        var t = this;
        if (item._value != value) {
            if (item.checkEvent("onBeforeChange")) if (item.callEvent("onBeforeChange",[item._idd, item._value, value]) !== true) {
                // restore
                if (item._df != null) t.setValue(item, item._value); else item.childNodes[item._ll?1:0].childNodes[0].value = item._value;
                return;
            }
            // accepted
            if (item._df != null && foc) t.setValue(item, value); else item._value = value;
            item.callEvent("onChange",[item._idd, value]);
            return;
        }
        if (item._df != null && foc) this.setValue(item, item._value);
    },

    setValue: function(item, value) {

        // str only
        item._value = (typeof(value) != "undefined" && value != null ? value : "");

        var v = (String(item._value)||"");
        var k = item.childNodes[item._ll?1:0].childNodes[0];

        // check if formatting available
        if (item._df != null) v = window.dhx4.template._getFmtValue(v, item._df);

        if (k.value != v) {
            k.value = v;
            item.getForm()._ccReload(item._idd, v);
        }

        k = null;
    },

    getValue: function(item) {
        // update value if item have focus
        var f = item.getForm();
        if (f._formLS && f._formLS[item._idd] != null) this.updateValue(item);
        f = null;
        // str only
        return (typeof(item._value) != "undefined" && item._value != null ? item._value : "");
    },

    setReadonly: function(item, state) {
        item._ro = (state===true);
        if (item._ro) {
            item.childNodes[item._ll?1:0].childNodes[0].setAttribute("readOnly", "true");
        } else {
            item.childNodes[item._ll?1:0].childNodes[0].removeAttribute("readOnly");
        }
    },

    isReadonly: function(item) {
        if (!item._ro) item._ro = false;
        return item._ro;
    },

    getInput: function(item) {
        return item.childNodes[item._ll?1:0].childNodes[0];
    },

    setNumberFormat: function(item, format, g_sep, d_sep, refresh) {

        if (typeof(refresh) != "boolean") refresh = true;

        if (format == "") {
            item._df = null;
            if (refresh) this.setValue(item, item._value);
            return true;
        }

        if (typeof(format) != "string") return;

        var fmt = window.dhx4.template._parseFmt(format, g_sep, d_sep);
        if (fmt == false) return false; else item._df = fmt;

        if (refresh) this.setValue(item, item._value);

        return true;

    }
};

(function(){
    for (var a in {doAddLabel:1,doAddInput:1,destruct:1,doUnloadNestedLists:1,setText:1,getText:1,enable:1,disable:1,isEnabled:1,setWidth:1,setFocus:1})
        dhtmlXForm.prototype.items.inputClock[a] = dhtmlXForm.prototype.items.select[a];
})();