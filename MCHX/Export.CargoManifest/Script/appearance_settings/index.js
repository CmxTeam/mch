﻿var appearanceSettings;
var appearanceSettingsWindow = function (shipmentsData) {

	var _this = this;
	this.shipmentsData = shipmentsData;

	this.init = function () {

		//init window
		this.settWin = CM.W.createWindow("appearance_settings", 0, 0, 900, 500);
		this.settWin.denyPark();
		this.settWin.denyResize();
		this.settWin.setText('Appearance Settings');
		this.settWin.center();

		this.settWin.attachEvent('onClose', function () {
			_this.tabBar.forEachTab(function (tab) {
				var id = tab.getId();
				if (id === 'view_weight_settings') {
					dhx4.callEvent('clearViewWeightSettingsTab', [tab]);
				}
				else if (id === 'view_weight_grid_settings') {
					dhx4.callEvent('clearViewWeightGridSettingsTab', [tab])
				}
				else if (id === 'left_tree_view_settings') {
					dhx4.callEvent('clearLeftTreeViewSettingsTab', [tab])
				}
			});
			_this.settWin = null;
			return true;
		});

		//init tabbar

		this.tabBar = this.settWin.attachTabbar({
				tabs: [
					{
						id: "view_weight_settings",
						text: "View Weight Settings",
						active: false,
						close: false
					},
					{
						id: "view_weight_grid_settings",
						text: "View Weight Grid Settings",
						active: false,
						close: false
					},
					{
						id: "left_tree_view_settings",
						text: "Left Tree View Settings",
						active: false,
						close: false
					}
				]
			}
		);

		this.closeWindow = function() {
			this.settWin.close();
			dhx4.callEvent('reloadSelectedTab', [])
		};

		this.tabBar.attachEvent('onSelect', function (id) {
			if (id === 'view_weight_settings') {
				dhx4.callEvent('loadViewWeightSettings', [this.tabs(id)]);
			}
			else if (id === 'view_weight_grid_settings') {
				dhx4.callEvent('loadViewWeightGridSettings', [this.tabs(id)]);
			}
			else if (id === 'left_tree_view_settings') {
				dhx4.callEvent('loadLeftTreeViewSettings', [this.tabs(id)]);
			}
			return true;
		});

		this.tabBar.tabs('view_weight_settings').setActive();

	};

	this.init();
	return this;
};

dhx4.attachEvent('showAppearanceSettingsWindow', function () {
	appearanceSettings = new appearanceSettingsWindow()
});

dhx4.attachEvent('closeAppearanceSettingWindow', function() {
	appearanceSettings.closeWindow();
});