﻿var leftTreeViewTab = {};

var leftTreeViewSettings = function (tab) {

	var _this = this;
	this.tab = tab;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{
						id: 'housebill_charges',
						type: 'text',
						text: '<span class="bold">Left Tree View Settings</span>',
						width: 200
					}
				]
			}
		);

	};


	this.clearAll = function () {
		_this = null;
	};

	this.load = function () {
		//this.tab.progressOn();
	};

	this.fillTab = function (response) {
		_this.tab.progressOff();
	};


	this.init();
	return this;
};

dhx4.attachEvent('loadLeftTreeViewSettings', function (tab) {
	if (tab && !leftTreeViewTab[tab.getId()]) {
		leftTreeViewTab[tab.getId()] = new leftTreeViewSettings(tab);
	}
	leftTreeViewTab[tab.getId()].load();
});
dhx4.attachEvent('clearLeftTreeViewSettingsTab', function (tab) {
	if (tab && leftTreeViewTab && leftTreeViewTab[tab.getId()]) {
		leftTreeViewTab[tab.getId()].clearAll();
		leftTreeViewTab[tab.getId()] = null;
		delete leftTreeViewTab[tab.getId()];
	}
});