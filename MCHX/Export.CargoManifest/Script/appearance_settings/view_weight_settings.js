﻿var viewWeightTab = {};

var viewWeightSettings = function (tab) {

	var _this = this;
	this.tab = tab;
	this.changed = false;

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{
						id: 'view_weight_settings',
						type: 'text',
						text: '<span class="bold">View Weight Settings</span>',
						width: 200
					}
				]
			}
		);

		//init form

		this.form = this.tab.attachForm(
			[
				{
					type: "block", name: "data", offsetTop: 15, offsetLeft: 140, list: [
					{
						type: "container",
						name: "available_columns",
						label: "Available Columns",
						inputWidth: 180,
						inputHeight: 260,
						position: "label-top",
						className: 'border'
					},
					{type: "newcolumn"},
					{
						type: "button",
						name: "remove",
						value: "",
						className: 'arrowLeftBtn',
						offsetTop: 110,
						offsetLeft: 20
					},
					{type: "button", name: "add", value: "", className: "arrowRightBtn", offsetTop: 20, offsetLeft: 20},
					{type: "newcolumn"},
					{
						type: "container",
						name: "selected_columns",
						label: "Selected Columns",
						inputWidth: 180,
						inputHeight: 260,
						position: "label-top",
						offsetLeft: 20,
						className: 'border'
					},
					{type: "newcolumn"},
					{type: "button", name: "up", value: "", className: "arrowUpBtn", offsetTop: 110, offsetLeft: 20},
					{type: "button", name: "down", value: "", className: "arrowDownBtn", offsetTop: 20, offsetLeft: 20}
				]
				},
				{
					type: "block", name: "buttons", offsetLeft: 245, offsetTop: 15, list: [
					{type: "button", name: "save", value: "Save"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"},
					{type: "newcolumn"},
					{type: "button", name: "reset_to_default", value: "Reset to Default"}
				]
				}
			]
		);

		this.form.attachEvent('onButtonClick', function(id){
			var avId = _this.avColView.getSelected(),
				selId = _this.selColView.getSelected(),
				item;
			if(id === 'up' && selId){
				_this.selColView.moveUp(selId);
				_this.changed = true;
			}
			else if(id === 'down' && selId) {
				_this.selColView.moveDown(selId);
				_this.changed = true;
			}
			else if(id === 'add' && avId) {
				item = _this.avColView.get(avId);
				_this.selColView.add({id: item.id, FieldName: item.FieldName});
				_this.avColView.remove(avId);
				_this.changed = true;
			}
			else if(id === 'remove' && selId) {
				item = _this.selColView.get(selId);
				_this.avColView.add({id: item.id, FieldName: item.FieldName});
				_this.selColView.remove(selId);
				_this.changed = true;
			}
			else if(id === 'cancel') {
				dhx4.callEvent('closeAppearanceSettingWindow', []);
			}
			else if(id === 'save') {
				if(_this.changed) {
					_this.saveChanges();
				}
			}
			else if(id === 'reset_to_default') {
				_this.resetToDefaultFields();
			}
		});

		//init dataviews on the form
		this.avColView = new dhtmlXDataView(
			{
				container: this.form.getContainer('available_columns'),
				type: {
					template: "#FieldName#",
					height: 15
				}
			});
		this.selColView = new dhtmlXDataView(
			{
				container: this.form.getContainer('selected_columns'),
				type: {
					template: "#FieldName#",
					height: 15
				}
			});
	};

	this.saveChanges = function(){
		this.tab.progressOn();
		var i, id, count, fields = '', item;
		count = this.selColView.dataCount();
		for(i = 0; i < count; i++) {
			id = this.selColView.idByIndex(i);
			item = this.selColView.get(id);
			fields += item.FieldName + "$"
		}
		fields = fields.substr(0, fields.length - 1);
		sendAjaxRequest('Export/UpdateWeightSettings?account=' + helpers.getUserName() + '&fieldNames=' + encodeURIComponent(fields), 'POST', null, function(response){
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Status && response.Status.Status) {
				dhx4.callEvent('closeAppearanceSettingWindow', [true]); //true for reload main tab
			}
			_this.tab.progressOff();
		})
	};

	this.resetToDefaultFields = function(){
		this.tab.progressOn();
		sendAjaxRequest('Export/ResetUserSettings', 'POST', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Status && response.Status.Status) {
				dhx4.callEvent('closeAppearanceSettingWindow', [true]); //true for reload main tab
			}
			_this.tab.progressOff();
		});
	};

	this.clearAll = function(){
		if(this.avColView) {
			this.avColView.destructor();
		}
		if(this.selColView){
			this.selColView.destructor();
		}
		this.selColView = _this =  this.avColView = null;
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/AppearanceSettingList?userId=' + helpers.getUserName(), 'GET', null, this.fillTab);
	};

	this.fillTab = function (response) {
		var data = dhx4.s2j(response.xmlDoc.responseText),
			available, selected, i;
		if (data && data.Data && data.Data.length > 0) {
			for (i = 0; i < data.Data[0].Rows.length; i++) {
				_this.avColView.add({id: data.Data[0].Rows[i].RecID, FieldName: data.Data[0].Rows[i].FieldName})
			}
			for (i = 0; i < data.Data[1].Rows.length; i++) {
				_this.selColView.add({id: data.Data[1].Rows[i].RecID, FieldName: data.Data[1].Rows[i].FieldName})
			}
		}
		_this.tab.progressOff();
	};

	this.init();

	return this;
};

dhx4.attachEvent('loadViewWeightSettings', function (tab) {
	if (tab && !viewWeightTab[tab.getId()]) {
		viewWeightTab[tab.getId()] = new viewWeightSettings(tab);
	}
	viewWeightTab[tab.getId()].load();
});
dhx4.attachEvent('clearViewWeightSettingsTab', function (tab) {
	if (tab && viewWeightTab && viewWeightTab[tab.getId()]) {
		viewWeightTab[tab.getId()].clearAll();
		viewWeightTab[tab.getId()] = null;
		delete viewWeightTab[tab.getId()];
	}
});