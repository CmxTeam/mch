﻿var viewWeightGridTab = {};

var viewWeightGridSettings = function (tab) {

	var _this = this;
	this.tab = tab;
	this.changed = false;
	this.curOrder = {};

	this.colors = ['AliceBlue', 'AntiqueWhite', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque', 'Black', 'BlanchedAlmond', 'Blue', 'BlueViolet', 'Brown',
		'BurlyWood', 'CadetBlue', 'Chartreuse', 'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson', 'Cyan', 'DarkBlue', 'DarkCyan', 'DarkGoldenrod',
		'DarkGray', 'DarkGreen', 'DarkKhaki', 'DarkMagenta', 'DarkOliveGreen', 'DarkOrange', 'DarkOrchid', 'DarkRed', 'DarkSalmon', 'DarkSeaGreen', 'DarkSlateBlue', 'DarkSlateGray',
		'DarkTurquoise', 'DarkViolet', 'DeepPink', 'DeepSkyBlue', 'DimGray', 'DodgerBlue', 'Firebrick', 'FloralWhite', 'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'Gold',
		'Goldenrod', 'Gray', 'Green', 'GreenYellow', 'Honeydew', 'HotPink', 'IndianRed', 'Indigo', 'Ivory', 'Khaki', 'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon',
		'LightBlue', 'LightCoral', 'LightCyan', 'LightGoldenrodYellow', 'LightGray', 'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue', 'LightSlateGray',
		'LightSteelBlue', 'LightYellow', 'Lime', 'LimeGreen', 'Linen', 'Magenta', 'Maroon', 'MediumAquamarine', 'MediumBlue', 'MediumOrchid', 'MediumPurple', 'MediumSeaGreen',
		'MediumSlateBlue', 'MediumSpringGreen', 'MediumTurquoise', 'MediumVioletRed', 'MidnightBlue', 'MintCream', 'MistyRose', 'Moccasin', 'NavajoWhite', 'Navy', 'OldLace',
		'Olive', 'OliveDrab', 'Orange', 'OrangeRed', 'Orchid', 'PaleGoldenrod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed', 'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum',
		'PowderBlue', 'Purple', 'Red', 'RosyBrown', 'RoyalBlue', 'SaddleBrown', 'Salmon', 'SandyBrown', 'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
		'SlateGray', 'Snow', 'SpringGreen', 'SteelBlue', 'Tan', 'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White', 'WhiteSmoke', 'Yellow', 'YellowGreen'
	];

	this.init = function () {

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
				json: [
					{id: 'housebill_charges', type: 'text',	text: '<span class="bold">View Weight Grid Settings</span>'}
				]
			}
		);

		//init form

		this.form = this.tab.attachForm(
			[
				{type: "container",	name: "view_weight_grid", inputWidth: 800, inputHeight: 300, offsetLeft: 20},
				{type: "block", name: "buttons", offsetLeft: 300, offsetTop: 15, list: [
					{type: "button", name: "save", value: "Save"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
				}
			]
		);

		this.form.attachEvent('onButtonClick', function (id) {
			if(id === 'cancel') {
				dhx4.callEvent('closeAppearanceSettingWindow', []);
			}
			else if(id === 'save') {
				_this.saveChanges();
			}
		});

		this.grid = new dhtmlXGridObject(this.form.getContainer('view_weight_grid'));
		this.grid.setHeader(['id','Description', 'Font Color', 'Color', 'Allow Selection', '', '']);
		this.grid.setImagesPath(dhtmlx.image_path);
		this.grid.setColSorting('na,str,str,str,na,na,na');
		this.grid.setColTypes('ro,ro,cp,cp,ch,button,button');
		this.grid.setColAlign('left,left,left,left,center,center,center');
		this.grid.setInitWidths('0,*,130,130,130,60,70');
		this.grid.init();
		this.grid.attachEvent('onRowButtonClick', function (rowId, colId, name) {
			var id = _this.grid.cells(rowId, 0).getValue(), order = _this.grid.getRowIndex(rowId), newOrder, newId;
			if (name === 'Up') {
				if(order === 0) {
					return;
				}
				this.moveRowUp(rowId);
				newOrder = order ? order - 1 : 0;
			}
			else if (name === 'Down') {
				if(!this.getRowId(order + 1)) {
					return;
				}
				this.moveRowDown(rowId);
				newOrder = order + 1;
			}
			newId = this.getRowId(order);
			var url = 'Export/UpdateGridOrder?curOrder='+order+'&curId='+newId+'&order='+newOrder+'&id=' + id;
			_this.tab.progressOn();
			sendAjaxRequest(url, 'POST', null, function (response) {
				response = dhx4.s2j(response.xmlDoc.responseText);
				if(response && !response.Data){
					showAlert('Weight Grid Settings', response.Status.Message)
				}
				_this.tab.progressOff();
			});
		});
	};

	this.saveChanges = function() {
		var promise = [];
		this.tab.progressOn();
		this.grid.forEachRow(function(rowId) {
			promise.push(
				new Promise(function (resolve, reject) {
					var url = 'Export/UpdateColorSettings?nodeColor=' + encodeURIComponent(_this.grid.cells(rowId, 3).getValue()) + '&allowSelect=' + _this.grid.cells(rowId, 4).getValue() + '&nodeForeColor=' + encodeURIComponent(_this.grid.cells(rowId, 2).getValue()) + '&id=' +  _this.grid.cells(rowId, 0).getValue();
					sendAjaxRequest(url, 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && ((response.Status && response.Status.Status) || response.Data )) {
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		});
		Promise.all(promise).then(function () {
			_this.tab.progressOff();
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
			_this.tab.progressOff();
		});
	};


	this.clearAll = function () {
		if (this.grid) {
			this.grid.destructor();
		}
		_this = null;
	};

	this.load = function () {
		this.tab.progressOn();
		sendAjaxRequest('Export/ViewWeightGrigSettings', 'GET', null, this.fillTab);
	};

	this.fillTab = function (response) {
		var i, t, temp; //t - text color combo, temp - background color combo
		response = dhx4.s2j(response.xmlDoc.responseText);
		if (response && response.Data.rows) {
			for (i = 0; i < response.Data.rows.length; i++) {
				response.Data.rows[i].data.push('Up');
				response.Data.rows[i].data.push('Down');
			}
			_this.grid.parse(response.Data, 'json');
			_this.curOrder = {};
			_this.grid.forEachRow(function(rowId) {
				_this.curOrder[this.cells(rowId, 0).getValue()] = this.getRowIndex(rowId);
			});
		}
		t = temp = null;
		_this.tab.progressOff();
	};

	this.init();
	return this;
};

dhx4.attachEvent('loadViewWeightGridSettings', function (tab) {
	if (tab && !viewWeightGridTab[tab.getId()]) {
		viewWeightGridTab[tab.getId()] = new viewWeightGridSettings(tab);
	}
	viewWeightGridTab[tab.getId()].load();
});
dhx4.attachEvent('clearViewWeightGridSettingsTab', function (tab) {
	if (tab && viewWeightGridTab && viewWeightGridTab[tab.getId()]) {
		viewWeightGridTab[tab.getId()].clearAll();
		viewWeightGridTab[tab.getId()] = null;
		delete viewWeightGridTab[tab.getId()];
	}
});