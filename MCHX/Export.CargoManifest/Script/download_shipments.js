﻿var downloadShipments;
var downloadShipmentsData = function (data) {

	var _this = this;
	this.data = data;

	this.init = function (data) {

		//init window

		this.downWin = CM.W.createWindow("download_shipments", 0, 0, 600, 800);
		this.downWin.denyPark();
		this.downWin.setText('Download selected Shipment(s)');
		this.downWin.center();
		this.downWin.denyResize();

		this.downWin.attachEvent('onClose', function(){
			if(_this.int) {
				clearInterval(_this.int);
			}
			_this.int = _this.downWin = null;
			return true;
		});


		//init toolbar

		this.toolbar = this.downWin.attachToolbar({
			json: [
				{id: 'route', type: 'text',	text: 'Housebills Download Requests'},
				{type: 'separator'},
				{id: 'route', type: 'text',	text: 'Pending'},
				{id: 'route', type: 'text',	text: '<img src="' + baseUrl + 'Images/pendingdownloadInpro.gif" class="size20"/>'},
				{type: 'separator'},
				{id: 'route', type: 'text',	text: 'In Progress'},
				{id: 'route', type: 'text', text: '<img src="' + baseUrl + 'Images/pendingDownload.gif" class="size20"/>'},
				{type: 'separator'},
				{id: 'route', type: 'text',	text: 'Downloaded'},
				{id: 'route', type: 'text',	text: '<img src="' + baseUrl + 'Images/downloaded.gif" class="size20"/>'},
				{type: 'separator'}
			]
		});

		//init grid

		this.grid = this.downWin.attachGrid();
		this.grid.setIconPath(dhtmlx.image_path);
		this.grid.setHeader(['Shipment', 'Status']);
		this.grid.setColSorting('str,str');
		this.grid.setColAlign('left,center');
		this.grid.setColTypes('ro,ro');
		this.grid.setInitWidths('*,100');
		this.grid.init();
		this.grid.parse(data, 'json');
		this.int = setInterval(function(){
			_this.grid.clearAll();
			_this.loadDownloadData(true);
		}, 5000)
	};

    this.loadDownloadData = function(loading) {
		var url;
		if (loading) {
			this.downWin.progressOn();
		}
		if(this.data && this.data.url){
			url = this.data.url;
		}
		else {
			url = 'Export/DownloadSelectedshipments?airbrilno=' + this.data[0].airbillNo + '&gateway=' + CM.gateway;
		}
        sendAjaxRequest(url, 'GET', null, function (response) {
            response = dhx4.s2j(response.xmlDoc.responseText);
            if (response && response.Data && typeof response.Data === 'object') {
				if(!_this.grid) {
					_this.init(response.Data);
				}
				else {
					_this.grid.parse(response.Data, 'json')
				}
            }
			else {
				showAlert('Download', response.Data);
			}
			if (loading) {
				_this.downWin.progressOff();
			}
        })
    };

	this.loadDownloadData(false);
	return this;
};

dhx4.attachEvent('showDownloadShipmentsWindow', function (data) {
	downloadShipments = new downloadShipmentsData(data)
});