﻿var temporaryTabs = {};

var temporaryTab = function (tab, tabData, routes, gateway, destination) {
	var _this = this;
	this.tab = tab;
	this.tabData = tabData;
	this.showAssignConsolidationWindow = tabData.showAssignConsolWindow;
	this.grids = {};
	this.routes = routes;
	this.gateway = gateway;
	this.destination = destination;

	this.init = function () {

		var typing;

		//init toolbar

		this.toolBar = this.tab.attachToolbar({
			json: [
				{id: 'quick_finder', type: 'text', text: 'QUICK FINDER:'},
				{id: 'search', type: 'buttonInput', width: 200},
				{id: 'separator'},
				{id: 'actions', type: 'buttonSelect', text: 'Actions', options: [
					{id: 'assignConsolidation', type: 'button', text: 'Assign Consolidation'},
					{type: 'separator'},
					{id: 'deletePlan', type: 'button', text: 'Delete Plan'},
					{type: 'separator'},
					{id: 'assignMasterHouse', type: 'button', text: 'Assign Master House'},
					{id: 'createEmptyMasterHouse', type: 'button', text: 'Create Empty Master House'},
					{type: 'separator'},
					{id: 'import', type: 'button', text: 'Import Shipments to this Loading Plan'}
				]
				},
				{type: 'separator'},
				{id: 'options', type: 'buttonSelect', text: 'Please Select One', options: [
					{id: 'view_shipment', type: 'button', text: 'View Shipment Details'},
					{id: 'hold_release', type: 'button', text: 'Hold / Release Shipment'},
					{id: 'expedite', type: 'button', text: 'Expedite YES / NO'},
					{id: 'remove_shipment', type: 'button', text: 'Remove Shipment(s)'},
					{id: 'update_screening_status',	type: 'button',	text: 'Update Screening Status on selected Shipment(s)'},
					{id: 'override_ams', type: 'button', text: 'Override AMS Hold on selected Shipment(s)'},
					{id: 'cancel_screening', type: 'button', text: 'Cancel Screening on selected Shipment(s)'},
					{id: 'print_plan', type: 'button', text: 'Print Loading Plan'},
					{type: 'separator'}
				]
				},
				{type: 'separator'},
				{id: 'appearance_settings', type: 'button', text: 'Appearance Settings'}
			]
		});

		//init filter
		this.toolBar.getInput('search').onkeyup = function (ev) {
			clearTimeout(typing);
			typing = setTimeout(function() {
				CM.f.filterGrids(_this.grids, _this.toolBar.getValue('search'), _this.changeSectionDataAndSize);
			}, 500);
		};

		this.toolBar.getInput('search').onkeydown = function (ev) {
			clearTimeout(typing)
		};

		this.toolBar.attachEvent('onClick', function (id) {
			var data, eventName, parentId = this.getParentId(id), i, t = '';
			if(id === 'appearance_settings') {
				dhx4.callEvent('showAppearanceSettingsWindow', []);
			}
			else if(parentId === 'actions') {
				if (id === 'deletePlan') {
					if (parentId === 'actions') {
						if (id === 'deletePlan') {
							dhtmlx.confirm({
								title: "Delete Plan",
								type: "confirm",
								text: "Are you sure you want to delete plan?",
								callback: _this.deletePlan
							});
						}
					}
				}
				else if (id === 'assignConsolidation') {
					dhx4.callEvent('callAssignWindow', [_this.tabData.Code]);
				}
				else if(id === 'import') {
					dhx4.callEvent('showImportWindow', [_this.tabData.TabCode, _this.routes])
				}
                else if(id === 'createEmptyMasterHouse') {
                    dhx4.callEvent('showMasterHouse', [_this.gateway, _this.destination, 'c', _this.tabData, _this.total])
                }
				else if(id === 'assignMasterHouse') {
					dhx4.callEvent('showMasterHouse', [_this.gateway, _this.destination, 'a', _this.tabData, _this.total])
				}
			}
			else if (parentId === 'options') {
				data = CM.f.getSelectedShipmentData(_this.grids);
				data = data.selectedRows;
				if (data.length === 0 && id !== 'print_plan') {
					showAlert('Shipment Details', 'No Shipment selected');
					return;
				}
				if (id === 'view_shipment') {
					eventName = 'showShipmentDetailsWindow';
				}
				else if (id === 'expedite') {
					CM.f.expediteShipments(data, 'reloadSelectedTab');
				}
				else if (id === 'hold_release') {
					eventName = 'showHoldReasonWindow';
				}
				else if(id === 'remove_shipment') {
					dhtmlx.confirm({
						title: "Delete Selected Shipments",
						type: "confirm",
						text: "Are you sure you want to remove selected shipment(s)",
						callback: function(res) {
							if(res) {
								_this.removeSelectedShipments(data);
							}
						}
					});
				}
				else if(id === 'override_ams') {
					data = CM.f.getSelectedShipmentData(_this.grids);
					if (data.notInGrid) {
						showAlert('AMS Hold', 'One or more selected shipments do not have AMS Hold, Unable to continue');
					}
					else {
						CM.f.overrideAMS(data.selectedRows, 'reloadSelectedTab');
					}
				}
                else if(id === 'update_screening_status') {
                    for(i = 0; i < data.length; i++) {
                        t += data[i].airbillNo + ','
                    }
                    t = t.substr(0, t.length - 1);
                    _this.tab.progressOn();
                    CM.f.updateScreeningStatus(t, 'reloadSelectedTab');
                }
                else if(id === 'cancel_screening') {
                    showConfirm('Are you sure you want to cancel screening on Selected Shipment(s)', function() {
                        for(i = 0; i < data.length; i++) {
                            t += data[i].airbillNo + ','
                        }
                        t = t.substr(0, t.length - 1);
                        CM.f.cancelScreeningStatus(t, 'reloadSelectedTab');
                    })
                }
                else if(id === 'print_plan') {
                    dhx4.callEvent('showPrintLoadingPlan', [_this.tabData.Code]);
                }
				if (eventName) {
					dhx4.callEvent(eventName, [data, _this.routes]);
				}
			}
		});

		//init accorion section

		this.accordion = this.tab.attachAccordion();
		this.accordion.enableMultiMode(true); // enable to add few opened sections
		this.accordion.cont.style.overflowY = "auto"; // add scroll (if needed) to the top section of accordion
	};

	this.deletePlan = function (result) {
		if (result === true) {
			sendAjaxRequest('Export/deleteLoadingPlan?cargoManifestId=' + this.tabData.Code, 'POST', null, this.afterDeletePlan)
		}
	}.bind(this);

	this.removeSelectedShipments = function(data) {
		var i, promise = [];
		for (i = 0; i < data.length; i++) {
			promise.push(
				new Promise(function (resolve, reject) {
					sendAjaxRequest('Export/RemoveShipment?airbillNo=' + data[i].airbillNo + '&masterbillNo=' + _this.tabData.Code , 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function () {
			dhx4.callEvent('reloadSelectedTab', []);
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
		});
	};

	this.afterDeletePlan = function (result) {
		result = dhx4.s2j(result.xmlDoc.responseText);
		if (result && result.Status && result.Status.Status) {
			dhx4.callEvent('onDeleteTab', [this.tabData.Code, 1]);
		}
	}.bind(this);

	this.load = function () {
		this.tab.progressOn();
		if (CM.localhost) {
			this.fillTab(dhx.copy(otherShipments));
		}
		else {
			sendAjaxRequest('Export/GetShipmentsByCode?code=' + this.tabData.Code + '&account=' + helpers.getUserName(), 'GET', null, this.fillTab);
		}
	};


	this.fillTab = function (response) {
		var data = dhx4.s2j(response.xmlDoc.responseText),
			section, grid, headerData, types = 'ch,', i,
			sortType = 'na,', colAlign = 'center,', sectionHeight,
			totalHawb = data.Data.total_hawb ? data.Data.total_hawb : 0,
			totalPcs = data.Data.total_pcs ? data.Data.total_pcs : 0,
			totalGross = data.Data.total_gross_w ? data.Data.total_gross_w : 0,
			totalCF = data.Data.total_cf ? data.Data.total_cf : 0,
			totalCH = data.Data.total_ch_wt ? data.Data.total_ch_wt : 0,
			totalFTC = data.Data.total_ftc ? data.Data.total_ftc : 0;
		if (!data || !data.Status || !data.Status.Status) {
			return false;
		}
		this.total = {
			totalHawb: totalHawb,
			totalPcs: totalPcs,
			totalGross: totalGross,
			totalCF: totalCF,
			totalCH: totalCH,
			totalFTC: totalFTC
		};
		data = data.Data;
		section = this.accordion.addItem(data.id, data.nodeText ? data.nodeText : '');
		headerData = data.ColumnNames.split(',');
		for (i = 0; i < headerData.length; i++) {
			types += 'ro,';
			sortType += 'str,';
			colAlign += 'center,';
		}
		section.attachStatusBar({
			text: '<div class="inline_childs"><div>Total Airbills: ' + totalHawb + '</div><div>Total Pieces: ' + totalPcs + '</div><div>Total Gross Weight: ' + totalGross + '</div>' +
			'<div>Total Chargeable Weight: ' + totalCH + '</div><div>Total Cubic Feet: ' + totalCF + '</div><div>Total FTC: ' + totalFTC + '</div></div>', // status bar init text
			height: 20     // status bar init height
		});
		grid = section.attachGrid();
		types = types.substr(0, types.length - 1);
		colAlign = colAlign.substr(0, types.length - 1);
		sortType = sortType.substr(0, sortType.length - 1);
		headerData.unshift('#master_checkbox');
		grid.setHeader(headerData);
		grid.setIconPath(dhtmlx.image_path);
		//grid.setImagesPath(baseUrl);
		grid.setColSorting(sortType);
		grid.setColTypes(types);
		grid.setInitWidths('40,' + data.Widths);
		grid.setColAlign(colAlign);
		grid.init();
		grid.attachEvent('onRowDblClicked', function(rowId) {
			var data = [{
				'airbillNo': this.getUserData(rowId, 'AirbillNo'),
				'origin': this.getUserData(rowId, 'Origin')
			}];
			dhx4.callEvent('showShipmentDetailsWindow', [data]);
		});
		if (data.data) {
			for (i = 0; i < data.data.rows.length; i++) {
				data.data.rows[i].data.unshift('0');
			}
			grid.parse(data.data, 'json');
		}
		sectionHeight = (data.data ? 26 * data.data.rows.length : 0) + section.cell.childNodes[0].offsetHeight + section.cell.childNodes[2].offsetHeight + 16 + grid.hdrBox.offsetHeight; //16 - paddings
		section.setHeight(sectionHeight);
		grid.gridName = data.nodeText;
		this.grids[data.id] = grid;
		this.tab.progressOff();
		if (this.showAssignConsolidationWindow) {
			dhx4.callEvent('callAssignWindow', [this.tabData.Code]);
		}
		this.showAssignConsolidationWindow = false;
	}.bind(this);

	this.changeSectionDataAndSize = function (grid, key, rowsNum) {
		var section = this.accordion.cells(key),
			rowsCount = (rowsNum || rowsNum === 0) ? rowsNum : grid.getRowsNum(),
			sectionHeight = (rowsCount ? 26 * rowsCount : 0) + section.cell.childNodes[0].offsetHeight + section.cell.childNodes[2].offsetHeight + 16 + grid.hdrBox.offsetHeight; // 16 - paddings
		section.setHeight(sectionHeight);
	}.bind(this);

	this.clearAll = function () {
		var key;
		for (key in this.grids) {
			if (this.grids.hasOwnProperty(key)) {
				this.grids[key].destructor();
				this.accordion.removeItem(key);
			}
		}
		this.grids = {};
	};

	this.init();

	return this;
};

dhx4.attachEvent('onTemporaryTabSelect', function (tab, tabData, routes, gateway, destination) {
	if (tab && !temporaryTabs[tab.getId()]) {
		temporaryTabs[tab.getId()] = new temporaryTab(tab, tabData, routes, gateway, destination);
	}
	else {
		temporaryTabs[tab.getId()].clearAll();
	}
	temporaryTabs[tab.getId()].load();
});

dhx4.attachEvent('clearTemporaryTabData', function (code) {
	if (code && temporaryTabs[code]) {
		temporaryTabs[code].clearAll();
		temporaryTabs[code] = null;
		delete temporaryTabs[code];
	}
});