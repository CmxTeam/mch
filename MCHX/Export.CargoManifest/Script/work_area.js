﻿var WorkArea;

var workAreaFunc = function (cell) {
	var _this = this;
	this.cell = cell;
	this.tabs = {};
	this.tabsData = {};

	this.init = function () {

		// init tabBar

		this.tabBar = this.cell.attachTabbar();
		this.tabBar.setArrowsMode("auto");
		this.tabBar.attachEvent('onSelect', function (id) {
			var t;
			if (_this.tabsData[id] && _this.tabsData[id].TabOrder === 0) {
				dhx4.callEvent('onMainTabSelect', [this.tabs(id), _this.gateway, _this.destination, _this.tabsData, _this.routes]);
			}
			else if (_this.tabsData[id] && _this.tabsData[id].TabOrder === 1) {
				dhx4.callEvent('onTemporaryTabSelect', [this.tabs(id), _this.tabsData[id], _this.routes, _this.gateway, _this.destination]);
                _this.tabsData[id].showAssignConsolWindow = false;
			}
			else if (_this.tabsData[id] && (_this.tabsData[id].TabOrder === 2 || _this.tabsData[id].TabOrder === 'consol')) {
				if(_this.tabsData[id].TabOrder === 2) {
					t = _this.convertDataForConsol(_this.tabsData[id]);
				}
				dhx4.callEvent('onConsolTabCreated', [this.tabs(id), t ? t : _this.tabsData[id]]);
			}
			return true;
		});
	};

	this.convertDataForConsol = function(data) {
		var t = data.TabCaption ? data.TabCaption.split('-') : data.Code.split('-');
		return {
			consol: t[t.length - 1],
			gateway: t[t.length - 2],
			airbillNo: null
		}
	};

	this.clearPage = function () {
		var key;
		for (key in this.tabsData) {
			if (this.tabsData.hasOwnProperty(key)) {
				if (this.tabsData[key].TabOrder === 0) {
					dhx4.callEvent('unloadMainTabArea', [this.tabBar.tabs(this.tabsData[key].Code)]);
				}
				else if (this.tabsData[key].TabOrder === 1) {
					dhx4.callEvent('clearTemporaryTabData', [this.tabsData[key].Code]);
				}
				else if (this.tabsData[key].TabOrder === 2 || this.tabsData[key].TabOrder === 'consol') {
					dhx4.callEvent('unloadConsolTabData', [key]);
				}
			}
		}
		this.tabBar.clearAll();
		this.tabBar.conf.lastActive = null;
		this.gateway = null;
		this.destination = null;
		this.date = null;
		this.tabs = {};
		this.tabsData = {};
	};


	this.getTabBarData = function (gateway, destination, date, routes) {
		this.gateway = gateway;
		this.destination = destination;
		this.date = date;
		this.routes = routes;
		if (CM.localhost) {
			this.onAfterLoadTabs(JSON.stringify(tabContent));
		}
		else if (this.gateway && this.destination && this.date) {
			sendAjaxRequest('Export/GetTabItems?gateway=' + this.gateway + '&destination=' + this.destination + '&date=' + this.date, 'GET', null, this.onAfterLoadTabs);
		}
	};

	this.onAfterLoadTabs = function (response) {
		var data = dhx4.s2j(response.xmlDoc.responseText), i;
		if (data && data.Status && data.Status.Status) {
			if (data && data.Data) {
				for (i = 0; i < data.Data.length; i++) {
					data.Data[i].showAssignConsolWindow = false;
					this.addTab(data.Data[i]);
				}
				if (this.tabBar.tabs('viewWeight')) {
					this.tabBar.tabs('viewWeight').setActive();
				}
			}
		}
	}.bind(this);

	this.addTab = function (data) {
		data.Code = data.Code ? data.Code : 'viewWeight';
		this.tabsData[data.Code] = data;
		this.tabBar.addTab(data.Code, data.TabCaption, null, null, false);
	};

	this.deleteTabByCode = function (code, type) {
		if (this.tabBar.tabs(code)) {
			this.tabBar.tabs(code).close();
			this.tabsData[code] = null;
			delete this.tabsData[code];
			if (type === 1) {
				dhx4.callEvent('clearTemporaryTabData', [code]);
			}
			else if (type === 2) {

			}
		}
	};
	/**
	 *
	 * @param code - tab Code
	 * @param showAssignConsol - true for show consol window
	 */

	this.updateTabsData = function (code, showAssignConsolWindow) {
		this.cell.progressOn();
		sendAjaxRequest('Export/GetTabItems?gateway=' + this.gateway + '&destination=' + this.destination + '&date=' + this.date + '&account=' + helpers.getUserName(), 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText), i, createdTabId;
			if (data && data.Status && data.Status.Status) {
				for (i = 0; i < data.Data.length; i++) {
					if (!_this.tabsData[data.Data[i].Code] && data.Data[i].Code == code) {
						createdTabId = code;
						data.Data[i].showAssignConsolWindow = showAssignConsolWindow;
						_this.addTab(data.Data[i]);
					}
				}
				if (createdTabId) {
					_this.tabBar.tabs(createdTabId).setActive();
				}
			}
			_this.cell.progressOff();
		});
	};

	/**
	 *
	 * @returns array of [0]-destination, [1] - gateway, [2] - date
	 */

	this.getConfigAsArray = function () {
		return [
			this.destination,
			this.gateway,
			this.date
		];
	};

	this.createConsolTab = function(consol, airbillNo, gateway){
		sendAjaxRequest('Search/GetMasterbillDescr?masterbill=' + consol, 'GET', null, function(response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data) {
				_this.tabBar.addTab(response.Data, response.Data, null, null, false);
				_this.tabsData[response.Data] = {
					consol: consol,
					airbillNo: airbillNo,
					gateway: gateway,
					TabOrder: 'consol'
				};
				_this.tabBar.tabs(response.Data).setActive();
			}
		});
	};

	this.reloadSelectedTab = function() {
		var id = this.tabBar.getActiveTab();
		if(id) {
			this.tabBar.callEvent('onSelect', [id])
		}
	};

    this.showConsolWindowOnLastTab = function() {
        var lastCode, key;
        for(key in this.tabsData){
            if(this.tabsData.hasOwnProperty(key) && this.tabsData[key].TabOrder === 1) {
                lastCode = key;
            }
        }
        if(lastCode) {
			this.tabsData[lastCode].showAssignConsolWindow = true;
            this.tabBar.tabs(lastCode).setActive();
        }
    };

	this.init();

	return this;
};

dhx4.attachEvent('onRightSideInit', function (cell, gateway, destination, date, routes) {
	if (cell && !WorkArea) {
		WorkArea = new workAreaFunc(cell);
	}
	else {
		WorkArea.clearPage();
	}
	WorkArea.getTabBarData(gateway, destination, date, routes);
});
dhx4.attachEvent('onDeleteTab', function (code, type) {
	if (WorkArea && code) {
		WorkArea.deleteTabByCode(code, type);
	}
});

dhx4.attachEvent('onNewLoadingPlanCreate', function (code, showAssignConsol) {
	if (WorkArea && code) {
		WorkArea.updateTabsData(code, showAssignConsol);
	}
});

dhx4.attachEvent('callAssignWindow', function (code) {
	var data = WorkArea.getConfigAsArray();
	data.push(code); //add last parameter tab code
	dhx4.callEvent('showAssignConsolidation', data)
});

dhx4.attachEvent('selectTabById', function (id) {
	if (id) {
		WorkArea.tabBar.callEvent('onSelect', [id])
	}
});

dhx4.attachEvent('onConsolTabCreate', function(consol, airbillNo, gateway){
	if(WorkArea) {
		WorkArea.clearPage();
	}
	if(consol && gateway) {
		WorkArea.createConsolTab(consol, airbillNo, gateway);
	}
});

dhx4.attachEvent('reloadSelectedTab', function() {
	WorkArea.reloadSelectedTab();
});

dhx4.attachEvent('showAssignConsolOnLastTab', function() {
    WorkArea.showConsolWindowOnLastTab();
});