﻿var importShipments;
var importShipmentsWin = function (tabCode, routes) {
	var temp = tabCode.split('-');
	var _this = this;
	this.masterbillNo = temp[1];
	this.typeConsol = temp[0];
	this.routes = routes;
	this.changed = false; //flag to check changes

	this.init = function () {

		//init window

		this.impWin = CM.W.createWindow("import_shipments", 0, 0, 1000, 600);
		this.impWin.denyPark();
		this.impWin.setText('Import Shipments');
		this.impWin.center();
		this.impWin.denyResize();

		this.impWin.attachEvent('onClose', function() {
			if(_this.destGrid) {
				_this.destGrid.destructor();
			}
			if(_this.houseGrid) {
				_this.houseGrid.destructor();
			}
			if(_this.changed) {
				dhx4.callEvent('reloadSelectedTab', []);
			}
			_this.impWin = _this = _this.data = _this.routes = null;
			return true;
		});


		//init toolbar

		this.toolbar = this.impWin.attachToolbar({
			json: [
				{id: 'ship', type: 'text', text: '<span class="bold">Import Shipments</span>'}
			]
		});

		this.form = this.impWin.attachForm([
			{
				type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5,	list: [
					{type: 'template', label: 'Import Shipments by:', offsetLeft: 300},
					{type: 'newcolumn'},
					{type: 'radio', name: 'filter_by', value: 'housebill', position: 'label-right', label: 'By Housebill'},
					{type: 'newcolumn'},
					{type: 'radio', name: 'filter_by', value: 'destination', position: 'label-right', label: 'By Destination'}
				]
			},
			{type: 'block', name: 'list_1', hidden: true, list: [
				{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
					{type: 'input', name: 'hawb', label: 'Enter Housebill Number to import:', labelWidth: 190, offsetLeft: 210},
					{type: 'newcolumn'},
					{type: 'checkbox', name: 'multiple_import', position: 'label-right', label: 'Import Multiple', offsetLeft: 10}
				]},
				{type: "block", name: "buttons_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
					{type: "button", name: "import", value: "Import", offsetLeft: 400},
					{type: "newcolumn"},
					{type: "button", name: "close", value: "Close"}
				]},
				{type: "block", name: "grid_1_block", blockOffset: 0, hidden: true, offsetLeft: 0, offsetTop: 5, list: [
					{ type:"container",	name: "grid_1", inputWidth: 925, inputHeight: 375}
				]}
			]},
			{type: 'block', name: 'list_2', hidden: true, list: [
				{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 20, offsetTop: 5, list: [
					{type: 'combo', name: 'destination', label: 'Select Destination from The list:', labelWidth: 180, offsetLeft: 200, filtering: true, options: []},
					{type: "newcolumn"},
					{type: "button", name: "import_selected", value: "Import Selected Shipment(s)", offsetLeft: 10, offsetTop: 5}
				]},
				{type: "block", name: "grid_2_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
					{type: "container", name: "grid_2", inputWidth: 925, inputHeight: 400}
				]}
			]}
		]);

		this.form.attachEvent('onChange', function(name, value, state) {
			if(name === 'filter_by') {
				if(value === 'destination') {
					this.hideItem('list_1');
					this.showItem('list_2');
					if(!_this.destGrid) {
						//init grid
						_this.initDestinationGrid();
						//load options to the destination dropdown
						this.reloadOptions('destination', _this.routes);
					}
				}
				else if(value === 'housebill') {
					this.hideItem('list_2');
					this.showItem('list_1');
				}
			}
			else if(name === 'destination' && value) {
				_this.loadDestinationData();
			}
		});
		this.form.attachEvent('onButtonClick', function(name) {
			var t, i, temp = '';
			if(name === 'close') {
				_this.impWin.close();
			}
			else if(name === 'import') {
				t = this.getItemValue('hawb');
				if(t) {
					_this.impWin.progressOn();
					CM.f.importShipmentsByHousebill(t, _this.masterbillNo, _this.typeConsol, function(response) {
							_this.changed = true;
							if(response && response.Data.toLowerCase() === 'done') {
								if(_this.form.isItemChecked('multiple_import')) {
									_this.storeToGrid(t);
								}
								else {
									_this.impWin.close();
								}
							}
							else {
								showAlert('Import Shipments', response.Data);
							}
							_this.impWin.progressOff();
					}.bind(_this))
				}
			}
			else if(name === 'import_selected') {
				t = _this.destGrid.getCheckedIdsAsArray(0);
				if(t.length > 0) {
					_this.impWin.progressOn();
					for (i = 0; i < t.length; i++) {
						temp += _this.destGrid.getUserData(t[i], 'AirbillNo') + ',';
					}
					temp = temp.substr(0, temp.length - 1);
					sendAjaxRequest('Export/ImportShipmentsbyDestination?airbillNo=' + temp + '&masterbillNo=' + _this.masterbillNo + '&typeConsol=' + _this.typeConsol, 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						_this.changed = true;
						if(response && response.Data) {
							_this.loadDestinationData();
						}
						_this.impWin.progressOff();
					});
				}
				else {
					showAlert('Import Shipments', 'Before continue please select shipment(s)');
				}
			}
		})
	};

	this.loadDestinationData = function() {
		this.impWin.progressOn();
		sendAjaxRequest('Export/GetImportListByHousebill?destination=' + this.form.getItemValue('destination'), 'GET', null, function(response) {
			var i;
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data && response.Data.data){
				for(i = 0; i < response.Data.data.rows.length; i++) {
					response.Data.data.rows[i].data.unshift('0');
				}
				_this.destGrid.clearAll();
				_this.destGrid.parse(response.Data.data, 'json');
			}
			_this.impWin.progressOff();
		});
	};

	this.initDestinationGrid = function() {
		this.destGrid = new dhtmlXGridObject(this.form.getContainer('grid_2'));
		this.destGrid.setHeader(['#master_checkbox', 'Status', 'Origin', 'HAWB#', 'Shipment Date', 'Dest', 'Pieces', 'Gross Weight', 'CU FT', 'Last Scanned', 'Location', 'Shipper', 'Change Weight', 'Rate']);
		this.destGrid.setIconPath(baseUrl);
		this.destGrid.setImagesPath(dhtmlx.image_path);
		this.destGrid.setColSorting('na,na,str,str,str,str,str,str,str,str,str,str,str,str');
		this.destGrid.setColTypes('ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro');
		this.destGrid.setColAlign('center,left,left,left,left,left,left,left,left,left,left,left,left,left');
		this.destGrid.setInitWidths('40,60,55,70,80,45,70,80,60,120,140,160,80,70');
		this.destGrid.init();
	};

	this.storeToGrid = function(name) {
		if(!this.houseGrid) {
			this.initHousebillGrid();
		}
		this.houseGrid.addRow(this.houseGrid.uid(), [name]);
	}.bind(this);

	this.initHousebillGrid = function() {
		this.form.showItem('grid_1_block');
		this.houseGrid = new dhtmlXGridObject(this.form.getContainer('grid_1'));
		this.houseGrid.setHeader(['Imported Shipments']);
		this.houseGrid.setIconPath(baseUrl);
		this.houseGrid.setImagesPath(dhtmlx.image_path);
		this.houseGrid.setColSorting('str');
		this.houseGrid.setColTypes('ro');
		this.houseGrid.setColAlign('left');
		this.houseGrid.setInitWidths('*');
		this.houseGrid.init();
	};

	this.init();
	return this;
};

dhx4.attachEvent('showImportWindow', function (tabCode, routes) {
	importShipments = new importShipmentsWin(tabCode, routes)
});