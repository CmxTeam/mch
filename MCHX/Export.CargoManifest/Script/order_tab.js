﻿'use strict';

var orderTabs = {};

var orderTab = function (tab, tabCode) {
    var _this = this;
    this.tab = tab;
    this.tabCode = tabCode;
    this.grids = {};

    this.init = function () {

        //init toolbar

        this.toolBar = this.tab.attachToolbar({
            json: [
                { id: 'quick_finder', type: 'text', text: 'QUICK FINDER:' },
                { id: 'search', type: 'buttonInput', width: 200},
                { id: 'separator'},
                { id: 'actions', type: 'buttonSelect', text: 'Actions', options: [
                ]},
                { id: 'separator'}
            ]
        });

        this.toolBar.attachEvent('onClick', function (id) {
            var parentId = this.getParentId(id);
            if (parentId === 'actions') {
                if (id === 'expandAll') {
                    _this.expandCollapseAll(true);
                }
                else if (id === 'collapseAll') {
                    _this.expandCollapseAll(false);
                }
            }
            return true;
        });

        //init accorion section

        this.accordion = this.tab.attachAccordion();
        this.accordion.enableMultiMode(true); // enable to add few opened sections
        this.accordion.cont.style.overflowY = "auto"; // add scroll (if needed) to the top section of accordion
    };

    this.load = function () {
        if (CM.localhost) {
            this.fillTab(dhx.copy(otherShipments));
        }
        else {
            sendAjaxRequest('Export/GetShipmentsByCode?code=' + this.tabCode + '&account=' + helpers.getUserName(), 'GET', null, function (response) {
                var data = dhx4.s2j(response.xmlDoc.responseText);
                if (data && data.Status && data.Status.Status) {
                    _this.fillTab(data.Data);
                }
            });
        }
    };


    this.fillTab = function (data) {
        var section = this.accordion.addItem(data.id, data.nodeText ? data.nodeText: '' ),
                grid, headerData, types = 'ch,', i,
                sortType = 'na,', colAlign = 'center,', sectionHeight;
        headerData = data.ColumnNames.split(',');
        for (i = 0; i < headerData.length; i++) {
            types += 'ro,';
            sortType += 'str,';
            colAlign += 'center,';
        }
        section.attachStatusBar({
            text: "<div>Test</div>", // status bar init text
            height: 35     // status bar init height
        });
        grid = section.attachGrid();
        types = types.substr(0, types.length - 1);
        colAlign = colAlign.substr(0, types.length - 1);
        sortType = sortType.substr(0, sortType.length - 1);
        headerData.unshift('#master_checkbox');
        grid.setHeader(headerData);
        grid.setIconPath(dhtmlx.image_path);
        grid.setColSorting(sortType);
        grid.setColTypes(types);
        grid.setColAlign(colAlign);
        grid.init();
        if (data.data) {
            for (i = 0; i < data.data.rows.length; i++) {
                data.data.rows[i].data.unshift('0');
            }
            grid.parse(data.data, 'json');
        }
        sectionHeight = (data.data ? 31 * data.data.rows.length : 0) + section.cell.childNodes[0].offsetHeight + 35 + section.cell.childNodes[0].offsetHeight + 16 + 25; //31 - row height, 35 * 2 - header and footer height 16 - paddings
        section.setHeight(sectionHeight);
        this.grids[data.id] = grid;
    };

    this.init();

    return this;
};

dhx4.attachEvent("onOrderTabSelect", function (tab, tabData) {
    if (tab && !orderTabs[tab.getId()]) {
        orderTabs[tab.getId()] = new orderTab(tab, tabData.Code);
        orderTabs[tab.getId()].load();
    }
});