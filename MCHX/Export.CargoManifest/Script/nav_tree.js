﻿var navTree;

var navigationTree = function (cell) {

	var _this = this;
	var _combos = {
		'shipment_type': false,
		'filter_by': false
	};
	this.cell = cell;

	this.init = function () {

		//init tab bar

		var div = document.createElement('div'),
			t, temp;
		this.tabBar = this.cell.attachTabbar();
		this.tabBar.setArrowsMode("auto");
		this.tabBar.attachEvent('onSelect', function (id) {
			if (id === 'search' && !_this.searchForm) {
				_this.initSearchTab();
			}
			else if (id === 'view_weight' && !_this.viewWeightForm) {
				_this.initViewWeightForm();
			}
			return true;
		});
		this.tabBar.addTab('view_weight', 'View Weight', null, null, false, false);
		this.tabBar.addTab('search', 'Search', null, null, false, false);

		// add custom collapse/expand functionality

		div.className = "expandCollapse";
		div.id = "expandCollapse";
		div.onclick = function () {
			_this.cell.collapse();
		};
		this.tabBar.tabsArea.parentNode.appendChild(div);

		// init viewWeight tab layout

		t = this.tabBar.tabs('view_weight').attachLayout('2E');
		t.cells('a').hideHeader();
		t.cells('a').setHeight('265');
		t.cells('b').hideHeader();
		this.viewWeightTabLayout = t;

		// init search tab layout

		temp = this.tabBar.tabs('search').attachLayout('2E');
		temp.cells('a').hideHeader();
		temp.cells('a').setHeight('265');
		temp.cells('b').hideHeader();
		this.searchTabLayout = temp;

		// init nav tree
		this.navTree = t.cells('b').attachTree();
        this.navTree.setImagePath(dhtmlx.image_path + 'dhxtree_web/');
        this.navTree.setIconsPath(baseUrl);
		this.navTree.attachEvent('onClick', function (id) {
			var date, pId = this.getParentId(id), label, t;
			//if (this.hasChildren(id) === 0 && this.getParentId(id) !== _this.topLevelTreeId && this.previousItemId !== id && !this.getUserData(id, "process")) {
			label = pId ? this.getItemText(pId): null;
			label = label.toLowerCase();
			if (label === 'routes') {
				date = dhx4.date2str(_this.viewWeightForm.getItemValue('operation_date'), '%Y-%m-%d');
				dhx4.callEvent('onRightSideInit', [null, CM.gateway, id, date, _this.comboData]);
			}
			else if(label === 'consols') {
				//consol, airbillNo, gateway
				t = this.getItemText(id);
				t = t.split('-');
				dhx4.callEvent('onConsolTabCreate', [t[t.length - 1], null, t[t.length - 2]]);
			}
			else if(label === 'domestic consols') {

			}
		});

		//set active tab
		this.tabBar.tabs('view_weight').setActive();
		t = temp = null;
	};


	/**
	 * Load data to the tree
	 * @param tree
	 * @private
	 */
	this.loadTreeData = function () {
		var date = this.viewWeightForm.getItemValue('operation_date', true),
			filterBy = this.viewWeightForm.getItemValue('filter_by');
		this.viewWeightTabLayout.cells('b').progressOn();
		if (date && filterBy) {
			if (CM.localhost) {
				this.fillTree(treeData);
				this.viewWeightTabLayout.cells('b').progressOff();
				return true;
			}
			else {
				sendAjaxRequest('Export/GetSearchResult?gatewayId=' + CM.gateway + '&userId=' + helpers.getUserName() + '&filter=' + filterBy + '&selDate=' + date, 'GET', null, function (response) {
					var data = dhx4.s2j(response.xmlDoc.responseText);
					if (data && data.Status && data.Status.Status) {
						_this.fillTree(data);
						_this.viewWeightTabLayout.cells('b').progressOff();
					}
				});
			}
		}
	};

	this.fillTree = function (data) {
		var i, k, j, id, q, treeData = {
				id: 0,
				item: []
			},
			route = this.viewWeightForm.getItemValue('route');
		this.comboData = [];
		if (this.topLevelTreeId) {
			this.navTree.deleteChildItems(0);
		}
		if (data && data.Status && data.Status.Status) {
			for (i = 0; i < data.Data.item.length; i++) {
				id = data.Data.item[i].id;
				id = id ? id : 0 + "_1";
				this.topLevelTreeId = id;
				treeData.item.push({
					id: id,
					text: data.Data.item[i].text,
					im0: data.Data.item[i].img,
					im1: data.Data.item[i].img,
					im2: data.Data.item[i].img,
					userdata: data.Data.item[i].userdata,
					item: []
				});
				for (k = 0; k < data.Data.item[i].item.length; k++) {
					treeData.item[treeData.item.length - 1].item.push(
						{
							id: data.Data.item[i].item[k].id,
							text: data.Data.item[i].item[k].text,
							im0: data.Data.item[i].item[k].img,
							im1: data.Data.item[i].item[k].img,
							im2: data.Data.item[i].item[k].img,
							userdata: data.Data.item[i].item[k].userdata,
							item: []
						}
					);
					if (data.Data.item[i].item[k].hasOwnProperty('item')) {
						for (j = 0; j < data.Data.item[i].item[k].item.length; j++) {
							treeData.item[treeData.item.length - 1].item[treeData.item[treeData.item.length - 1].item.length - 1].item.push(
								{
									id: data.Data.item[i].item[k].item[j].id,
									text: data.Data.item[i].item[k].item[j].text,
									im0: data.Data.item[i].item[k].item[j].img,
									im1: data.Data.item[i].item[k].item[j].img,
									im2: data.Data.item[i].item[k].item[j].img,
									userdata: data.Data.item[i].item[k].item[j].userdata,
									item: []
								}
							);
							if (data.Data.item[i].item[k].text.toLowerCase() === 'routes') {
								this.comboData.push({
									text: data.Data.item[i].item[k].item[j].id,
									value: data.Data.item[i].item[k].item[j].id
								})
							}
							if (data.Data.item[i].item[k].item[j].hasOwnProperty('item') && data.Data.item[i].item[k].text.toLowerCase() === 'routes') {
								for (q = 0; q < data.Data.item[i].item[k].item[j].item.length; q++) {
									treeData.item[treeData.item.length - 1].item[treeData.item[treeData.item.length - 1].item.length - 1].item[treeData.item[treeData.item.length - 1].item[treeData.item[treeData.item.length - 1].item.length - 1].item.length - 1].item.push(
										{
											id: data.Data.item[i].item[k].item[j].item[q].id,
											text: data.Data.item[i].item[k].item[j].item[q].text,
											im0: 'Images/' + data.Data.item[i].item[k].item[j].item[q].img,
											im1: 'Images/' + data.Data.item[i].item[k].item[j].item[q].img,
											im2: 'Images/' + data.Data.item[i].item[k].item[j].item[q].img,
											userdata: data.Data.item[i].item[k].item[j].item[q].userdata,
											item: []
										}
									);
								}
							}
						}
					}
				}
			}
		}
		this.navTree.parse(treeData, 'json');
		this.viewWeightForm.reloadOptions('route', this.comboData);
		this.navTree.openAllItems(0);
		if (route) {
			this.navTree.selectItem(route, null, null);
		}
	};


	this.initViewWeightForm = function () {
		this.viewWeightTabLayout.cells('a').progressOn();
		this.viewWeightForm = this.viewWeightTabLayout.cells('a').attachForm(
			[
				{type: "calendar", name: "operation_date", label: "Operation date:", dateFormat: CM.calendarDateFormat, position: "label-top"},
				{type: "combo", name: "shipment_type", label: "Shipment type", inputWidth: 200, position: "label-top"},
				{type: "combo", name: "filter_by", label: "Filter by:", inputWidth: 200, position: "label-top"},
				{type: "combo",	name: "route", label: "Enter Route:", inputWidth: 130, position: "label-left", filtering: true},
				{type: "block", name: "form_block_1", offsetTop: 15, list: [
					{type: "button", name: "refresh", value: "Refresh View"},
					{type: "newcolumn"},
					{type: "button", name: "legend", value: "Legend"}
				]
				}
			]
		);

		this.viewWeightForm.attachEvent('onChange', function (id) {
			if (id === 'route') {
				_this.navTree.selectItem(this.getItemValue(id), true);
			}
			else if (id === 'filter_by' || id === 'operation_date') {
				_this.loadTreeData();
			}
		});

		this.viewWeightForm.attachEvent('onButtonClick', function (id) {
			if (id === 'legend') {
				_this.showLegendWindow();
			}
			else if (id === 'refresh') {
				_this.loadTreeData()
			}
		});
		this.viewWeightForm.setItemValue('operation_date', new Date());
		if (CM.localhost) {
			this.viewWeightForm.reloadOptions('shipment_type', shipmentType.options);
			this.viewWeightForm.reloadOptions('filter_by', filterBy.Data.options);
			this.viewWeightForm.reloadOptions('route', routeData.Data.options);
			this.loadTreeData();
		}
		else {
			this.loadComboData('filter_by', 'Export/GetFilterBy?gatewayId=' + CM.gateway + '&department=Export Office');
			this.loadComboData('shipment_type', 'Export/GetShipmentType');
		}
	};

	this.showLegendWindow = function () {
		sendAjaxRequest('Export/GetLegend?mode=Export', 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText),
				i, t, grid, gridData = {rows: []}, temp, height;
            height = window.outerHeight <= 720 ? (window.outerHeight - 300): 720;
			if (data && data.Status && data.Status.Status) {
				for (i = 0; i < data.Data.rows.length; i++) {
					temp = dhx4._copyObj(data.Data.rows[i]);
					temp.data = [];
					temp.data.push('<img src="' + baseUrl + '' + data.Data.rows[i].data[1] + '"/>');
					temp.data.push(data.Data.rows[i].data[0]);
					gridData.rows.push(temp)
				}
				// window init

				t = CM.W.createWindow("legend", 0, 0, 350, height);
				t.denyPark();
				t.denyResize();
				t.setText('Legend');

				//init grid

				grid = t.attachGrid();
				grid.setHeader(['', 'Name']);
				grid.setInitWidths('40,*');
				grid.setIconPath(dhtmlx.image_path);
				grid.setImagePath('/');
				grid.setColSorting('na,str');
				grid.setColTypes('ro,ro');
				grid.setColAlign('center,left');
				grid.enableBlockSelection(false);
				grid.init();
				grid.attachEvent("onBeforeSelect", function () {
					return false;
				});
				grid.parse(gridData, 'json');
				t.center();
				t = grid = null;
			}

		});
	};

	this.loadComboData = function (comboName, comboURL) {
		sendAjaxRequest(comboURL, 'GET', null, function (response) {
			var data = dhx4.s2j(response.xmlDoc.responseText);
			if (data && (data.Data && data.Data.options || data.options)) {
				_this.viewWeightForm.reloadOptions(comboName, data.options ? data.options : data.Data.options);
				_this.viewWeightForm.getCombo(comboName).selectOption(0, false, true);
				_this.checkCombosFilling(comboName);
			}
		});
	};

	this.checkCombosFilling = function (comboName) {
		var filled = true, key;
		_combos[comboName] = true;
		for (key in _combos) {
			filled = _combos[key] && filled;
		}
		if (filled) {
			this.viewWeightTabLayout.cells('a').progressOff();
			//this.loadTreeData();
		}
	};

	this.initSearchTab = function () {
		//init search form

		this.searchForm = this.searchTabLayout.cells('a').attachForm(
			[
				{type: "radio",	name: "search",	value: "housebill",	label: "Housebill #", position: 'label-right', checked: true},
				{type: "radio", name: "search", value: "masterbill", label: "Masterbill #", position: 'label-right'},
				{type: "radio", name: "search", value: "reference", label: "Custom Ref. #", position: 'label-right'},
				{type: "radio",	name: "search",	value: "domesticConsole", label: "Domestic MAWB #",	position: 'label-right'},
				{type: "input", name: "search_field", width: 171},
				{
					type: "block", name: "button_block", blockOffset: 0, offsetLeft: 0, offsetTop: 10, list: [ //search reset download
					{type: "button", name: "search", value: "Search"},
					{type: "newcolumn"},
					{type: "button", name: "reset", value: "Reset"},
					{type: "newcolumn"},
					{type: "button", name: "download", value: "Download"}
				]
				}
			]
		);

		this.searchForm.attachEvent('onButtonClick', function (id) {
			var searchText = this.getItemValue('search_field'), searchBy = this.getCheckedValue('search'), url, userData = [];
			if (id === 'search') {
				if (searchText) {
                    _this.searchTabLayout.cells('b').progressOn();
					_this.searchResTree.deleteChildItems(0);
					sendAjaxRequest('Search/GetListResult?' + searchBy + '=' + searchText, 'GET', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {
							if (response.Data && response.Data && response.Data.item) {
								//_this.searchResTree.parentItemId = true;
								_this.searchResTree.parse(response.Data, 'json');
							}
							else {
								showAlert('Search Result', 'No items were found.');
							}
						}
						_this.searchTabLayout.cells('b').progressOff();
					});
				}
                else{
                    showAlert('Search', 'Please fill search field');
                }
				//
			}
			else if (id === 'reset') {
				this.setItemValue('search_field', '');
			}
			else if (id === 'download') {
				userData.customDownload = {};
				url = 'Search/DownloadShipmentQueueGet';
				if(searchText.trim()) {
					if (searchBy === 'housebill' || searchBy === 'masterbill') {
						if (searchBy === 'masterbill') {
							if (searchText.indexOf('-') === -1 || searchText.indexOf('-') !== 2) {
								showAlert("Search", "Please enter proper format for masterbill download 'CARRIER-MASTER' (000-00000000)");
								return;
							}
						}
						url = 'Search/FastDownloadShipment?type=' + searchBy + '&shipment=' + searchText;
					}
				}
				userData.url = url;
				dhx4.callEvent('showDownloadShipmentsWindow', [userData]);
			}
		});

		//init search tree
		this.searchResTree = this.searchTabLayout.cells('b').attachTree();
		this.searchResTree.setImagePath(dhtmlx.image_path + 'dhxtree_web/');

		this.searchResTree.attachEvent('onClick', function(id) {
			var consol = this.getUserData(id, "MasterBillNo"),
				airbillNo = this.getUserData(id, "AirbillNo"),
				destination = this.getUserData(id, "Destination"),
				date;
			if (this.hasChildren(id) === 0 && consol && airbillNo) {
				dhx4.callEvent('onConsolTabCreate', [consol, airbillNo, CM.gateway])
			}
			else if(this.hasChildren(id) === 0 && destination) {
				//date = dhx4.date2str(_this.viewWeightForm.getItemValue('operation_date'), '%Y-%m-%d');
				if(_this.navTree.getItemText(destination)) {
					_this.tabBar.tabs('view_weight').setActive();
					_this.navTree.callEvent('onClick', [destination]);
				}
				//dhx4.callEvent('')
			}
		});
	};

	this.init();

	return this;

};

dhx4.attachEvent('onLeftSideInit', function (cell) {
	if (cell && !navTree) {
		navTree = new navigationTree(cell);
	}
	dhx4.detachEvent('onLeftSideInit')
});
