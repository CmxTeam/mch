﻿CM.f = {
	filterGrids: function (grids, filterBy, callBack) {
		var key, visibRows = 0;
		filterBy = filterBy.toLowerCase().trim();
		for (key in grids) {
			if (grids.hasOwnProperty(key)) {
				visibRows = 0;
				grids[key].forEachRow(function (rowId) {
					if (filterBy) {
						var val = '', i, cellVal;
						for (i = 0; i < this.getColumnsNum(); i++) {
							cellVal = this.cells(rowId, i).getValue();
							if (cellVal.indexOf('<img') > -1) {
								continue;
							}
							val += cellVal.toLowerCase() + ',';
						}
						if (val.toLowerCase().indexOf(filterBy) !== -1) {
							visibRows += 1;
							this.setRowHidden(rowId, false);
						}
						else {
							this.setRowHidden(rowId, true);
						}
					}
					else {
						visibRows += 1;
						this.setRowHidden(rowId, false);
					}
				});
				if (callBack) {
					callBack(grids[key], key, visibRows);
				}
			}
		}
	},
	textTemplate: function (name, value) {
		return '<div class="template">' + value + '</div>';
	},
	getSelectedShipmentData: function (grids, name) {
		var key, ids, i, selectedRows = [], notInGrid = false;
		for (key in grids) {
			if (grids.hasOwnProperty(key)) {
				if (name && grids[key].gridName.toLowerCase() !== name) {
					notInGrid = true;
					continue
				}
				ids = grids[key].getCheckedIdsAsArray(0);
				if (ids.length > 0) {
					for (i = 0; i < ids.length; i++) {
						selectedRows.push({
							'airbillNo': grids[key].getUserData(ids[i], 'AirbillNo'),
							'origin': grids[key].getUserData(ids[i], 'Origin'),
							'masterbillNo': grids[key].getUserData(ids[i], 'MasterBillNo'),
							'zipcode': grids[key].getUserData(ids[i], 'ZipCode')
						});
					}
				}
			}
		}
		return {
			selectedRows: selectedRows,
			notInGrid: notInGrid
		};
	},
	overrideAMS: function(shipments, event) {
		var i, promise = [];
		showConfirm('Are you sure you want to Override AMS Hold on the Selected Shipment(s)?', function () {
			for (i = 0; i < shipments.length; i++) {
				promise.push(
					sendAjaxRequest('Export/OverrideAsmHold?hawb=' + shipments[i].airbillNo, 'POST', null, function(response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {
							resolve();
						}
						else {
							reject();
						}
					})
				);
			}
			Promise.all(promise).then(function () {
				dhx4.callEvent(event, [])
			}).catch(function (err) {
				showAlert('Error', 'something went wrong');
			});
		});
	},
	expediteShipments: function(data, event) {
		var i, promise = [];
		for (i = 0; i < data.length; i++) {
			promise.push(
				new Promise(function (resolve, reject) {
					sendAjaxRequest('Export/ChangeExpediteStatus?airbillNo=' + data[i].airbillNo, 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if (response && response.Status && response.Status.Status) {
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function () {
			dhx4.callEvent(event, []);
		}).catch(function (err) {
			showAlert('Error', 'something went wrong');
		});
	},
	importShipmentsByHousebill: function(airbill, masterbill, consol, callback) {
		sendAjaxRequest('Export/ImportShipmentsbyHousebill?airbillNo=' + airbill + '&masterbillNo=' + masterbill + '&typeConsol=' + consol, 'POST', null, function (response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(callback) {
				callback(response);
			}
		});
	},
	updateScreeningStatus: function (airbill, event) {
		sendAjaxRequest('Export/UpdateScreeningStatus?airbillNo=' + airbill, 'POST', null, function (response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data) {
				if (event) {
					dhx4.callEvent(event, []);
				}
			}
			else{
				showAlert('Error', response.Status.Message);
			}
		});
	},
	cancelScreeningStatus: function(airbill, event){
		sendAjaxRequest('Export/CancelScreening?airbillNo=' + airbill, 'POST', null, function (response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data) {
				if (event) {
					dhx4.callEvent(event, []);
				}
			}
			else{
				showAlert('Error', response.Status.Message);
			}
		});
	}
};