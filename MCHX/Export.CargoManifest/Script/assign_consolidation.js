﻿var consolidation;
var assignConsolidation = function (destination, gateway, date, code) {

    var _this = this;
    this.destination = destination;
    this.gateway = gateway;
    this.date = date;
    this.code = code;
	this.UK;

    this.init = function () {

        //init window

        var t;
        this.consWin = CM.W.createWindow("assign_consol", 0, 0, 1000, 500);
        this.consWin.denyPark();
        this.consWin.denyResize();
        this.consWin.setText('Assign Consolidation');
        this.consWin.center();

        this.consWin.attachEvent('onClose', function () {
            _this.calendar.unload();
            return true;
        });

        //init toolbar

        this.toolBar = this.consWin.attachToolbar({
            json: [
				{id: 'route', type: 'text', text: '<span class="bold">Route:' + this.destination + '</span>'},
				{type: 'separator'},
				{id: 'show_consol_for_label', type: 'text', text: 'Show Consols for:'},
				{id: 'show_consol_for', type: 'buttonInput', width: 100},
				{type: 'separator'},
				{id: 'dest_label', type: 'text', text: 'Dest:'},
				{id: 'destination_from', type: 'buttonInput', width: 40, value: this.destination},
				{id: 'delimetr', type: 'text', text: '-'},
				{id: 'destination_to', type: 'buttonInput', width: 40},
				{type: 'separator'},
				{id: 'go', type: 'button', text: 'Go'},
				{type: 'separator'},
				{id: 'newConsol', type: 'button', text: 'Create New Consol'},
				{type: 'separator'}
            ]
        });

        this.toolBar.attachEvent('onClick', function (id) {
            if (id === 'go') {
				_this.loadConsGridData();
            }
            else if (id === 'newConsol') {
                _this.createNewConsol();
            }
        });
        //attach calendar to toolbar input
        t = this.toolBar.getInput('show_consol_for');
        this.calendar = new dhtmlXCalendarObject(t);
        this.calendar.setDateFormat(CM.calendarDateFormat);
		this.calendar.hideTime();
        this.toolBar.setValue("show_consol_for", dhx4.date2str(new Date(this.date), CM.calendarDateFormat));

        //init grid
        this.consGrid = this.consWin.attachGrid();
        this.consGrid.setHeader(['', '', 'Carrier', 'Carrier Name', 'Flight #', 'Masterbill No', 'Origin', 'Dest', 'Dep. Date', 'Arr. Date', 'Cut-Off Time']);
        this.consGrid.setIconPath(dhtmlx.image_path);
        this.consGrid.setIconPath(baseUrl);
        this.consGrid.setImagesPath(dhtmlx.image_path);
        this.consGrid.setColSorting('na,na,str,str,str,str,str,str,date,date,str');
        this.consGrid.setColTypes('button,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro');
        this.consGrid.setColAlign('center,center,left,left,left,left,left,left,left,left,left');
        this.consGrid.setInitWidths('75,35,55,170,60,85,50,50,130,130,130');
        this.consGrid.init();

		this.consGrid.attachEvent('onRowButtonClick', function (rowId, colId, name) {
			if(name === 'Select') {
				_this.createNewConsol();
			}
		});
        t = null;

		this.loadConsGridData();
		this.getUKparam();

    };

	this.getUKparam = function() {
		if(this.code) {
			sendAjaxRequest('Consolidation/GetUk?masterbill=' + this.code, 'GET', null, function(response) {
				response = dhx4.s2j(response.xmlDoc.responseText);
				_this.UK = response.Data;
			});
		}
		else{
			this.UK = 0;
		}
	};

    this.loadConsGridData = function () {
		this.consWin.progressOn();
		this.consGrid.clearAll();
		sendAjaxRequest('Consolidation/GetFromMawbByDate?destination=' + this.toolBar.getValue('destination_from') + '&execDate=' + this.toolBar.getValue('show_consol_for'), 'GET', null, function(response) {
			var i;
			response = dhx4.s2j(response.xmlDoc.responseText);
			if(response && response.Data && response.Data.rows) {
				for(i = 0; i < response.Data.rows.length; i++) {
					response.Data.rows[i].data.unshift('Select');
				}
				_this.consGrid.parse(response.Data, 'json');
			}
			_this.consWin.progressOff();
		});
    };

    this.createNewConsol = function () {
        showAlert('', 'will be done in the next sprint');
    };

    this.init();
    return this;
};

dhx4.attachEvent('showAssignConsolidation', function (destination, gateway, date, code) {
    consolidation = new assignConsolidation(destination, gateway, date, code)
});