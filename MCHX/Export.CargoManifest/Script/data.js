﻿var tabContent = {
    "Status": { "Status": true, "Message": "" }, "Data": [
        { "TabOrder": 0, "TabCaption": "View Weight", "TabCode": "E-JFK-08.27.2014-08.27.2014-PAR", "Code": "" },
        { "TabOrder": 1, "TabCaption": "PAR [1]", "TabCode": "P-92315142956", "Code": "92315142956" },
        { "TabOrder": 1, "TabCaption": "PAR [10]", "TabCode": "P-92315161614", "Code": "92315161614" },
        { "TabOrder": 1, "TabCaption": "PAR [11]", "TabCode": "P-92415111235", "Code": "92415111235" },
        { "TabOrder": 1, "TabCaption": "PAR [2]", "TabCode": "P-92315143107", "Code": "92315143107" },
        { "TabOrder": 1, "TabCaption": "PAR [3]", "TabCode": "P-92315143302", "Code": "92315143302" },
        { "TabOrder": 1, "TabCaption": "PAR [4]", "TabCode": "P-92315144104", "Code": "92315144104" },
        { "TabOrder": 1, "TabCaption": "PAR [5]", "TabCode": "P-92315144213", "Code": "92315144213" },
        { "TabOrder": 1, "TabCaption": "PAR [6]", "TabCode": "P-92315144340", "Code": "92315144340" },
        { "TabOrder": 1, "TabCaption": "PAR [7]", "TabCode": "P-92315144444", "Code": "92315144444" },
        { "TabOrder": 1, "TabCaption": "PAR [8]", "TabCode": "P-92315144538", "Code": "92315144538" },
        { "TabOrder": 1, "TabCaption": "PAR [9]", "TabCode": "P-92315144959", "Code": "92315144959" },
        { "TabOrder": 2, "TabCaption": "001-PAR-81782433", "TabCode": "C-81782433", "Code": "001-81782433" },
        { "TabOrder": 2, "TabCaption": "057-PAR-25768223", "TabCode": "C-25768223", "Code": "057-25768223" },
        { "TabOrder": 2, "TabCaption": "057-PAR-25768341", "TabCode": "C-25768341", "Code": "057-25768341" }
    ]
};

var shipmentType = {
    "options": [
        { "value": "GR", "text": "Ground", selected: true },
        { "value": "EX", "text": "Air Express" },
        { "value": "FT", "text": "Air Freight" },
        { "value": "OC", "text": "Ocean Freight" }
    ]
};

var filterBy = {
    "Status": { "Status": true, "Message": "" }, "Data": {
        "options": [
            { "value": "RWW", "text": "View Weight", selected: true },
            { "value": "RWPS", "text": "Pending Shipments" },
            { "value": "RWUKS", "text": "Unknown Shippers" },
            { "value": "RWHZ", "text": "RA Shipments" },
            { "value": "RWOS", "text": "Upperdeck/Over Length" },
            { "value": "OHS", "text": "On-Hold" },
            { "value": "RWIC", "text": "Incomplete Shipments" },
            { "value": "AESERR", "text": "AES Error Routes" },
            { "value": "RWSS", "text": "Short Shipments" },
            { "value": "RWPP", "text": "Perishable Shipments" },
            { "value": "VACON", "text": "View All Consols" },
            { "value": "CBC", "text": "Sort Consols By Carrier" },
            { "value": "CBD", "text": "Sort Consols By Destination" },
            { "value": "VSC", "text": "View Selected Consols" },
            { "value": "CBL", "text": "Sort Consols By Lockout" },
            { "value": "LCP", "text": "Load Consol in Progress" },
            { "value": "VCC", "text": "Pulled And Labeled Consols" },
            { "value": "CRP", "text": "Consol Rating Inprogress" },
            { "value": "VCL", "text": "Closed And Not Transmitted Consols" },
            { "value": "VTRC", "text": "Transmitted Consols" }
        ]
    }
};

var routeData = {
    "Status": { "Status": true, "Message": "" }, "Data": {
        "options": [
            { "value": "LHR", "text": "LHR" },
            { "value": "MAN", "text": "MAN" },
            { "value": "ORD", "text": "ORD" },
            { "value": "OSL", "text": "OSL" },
            { "value": "PAR", "text": "PAR" },
            { "value": "PEN", "text": "PEN" },
            { "value": "PRG", "text": "PRG" },
            { "value": "RIC", "text": "RIC" },
            { "value": "ROC", "text": "ROC" },
            { "value": "ROM", "text": "ROM" },
            { "value": "SCL", "text": "SCL" },
            { "value": "SIN", "text": "SIN" },
            { "value": "STO", "text": "STO" },
            { "value": "STR", "text": "STR" },
            { "value": "SYD", "text": "SYD" },
            { "value": "SYR", "text": "SYR" },
            { "value": "TRN", "text": "TRN" },
            { "value": "VIE", "text": "VIE" },
            { "value": "WAW", "text": "WAW" },
            { "value": "YYZ", "text": "YYZ" }
        ]
    }
};

var viewWeightTab = {
    "Status": { "Status": true, "Message": "" }, "Data": [
        {
            "id": 1, "nodeText": "Pending", "nodeValue": "Pending", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": {
                "rows": [
                    { "id": 0, "bgColor": "LightGreen", "userdata": [], "data": ["DCA", "5MEY876", "8/13/2015", "PAR", "1 Of 1", "1", "0,38", "8/14/2015 06:20", "PAR", "CARESTREAM HEALTH INC", "AP-HM", "2", "45", "45", "AA5", "2", "1"] },
                    { "id": 1, "bgColor": "LightGreen", "userdata": [], "data": ["DCA", "5MEY883", "8/13/2015", "PAR", "1 Of 1", "1", "0,38", "8/14/2015 06:20", "PAR", "CARESTREAM HEALTH INC", "CENTRE HOSPITALIER GENERAL", "2", "45", "45", "AA5", "2", "1"] },
                    { "id": 2, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3524", "8/12/2015", "PAR", "1 Of 1", "51,2", "6,67", "8/13/2015 16:39", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "51,5", "0,95", "48,93", "AA3", "31,5", "1"] },
                    { "id": 3, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3525", "8/12/2015", "PAR", "1 Of 1", "51,2", "6,67", "8/13/2015 16:38", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "51,5", "0,95", "48,93", "AA3", "31,5", "1"] },
                    { "id": 4, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3526", "8/12/2015", "PAR", "2 Of 2", "102,5", "13,33", "8/13/2015 16:38", "PAR", "MOOG INC", "KN TOULOUSE AIRBUS POLE", "102,5", "0,95", "97,38", "AA3", "63", "2"] },
                    { "id": 5, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3527", "8/13/2015", "PAR", "2 Of 2", "36,2", "5,83", "8/14/2015 06:19", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "36,5", "0,95", "34,68", "AA3", "27,5", "2"] },
                    { "id": 6, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3528", "8/13/2015", "PAR", "2 Of 2", "61,6", "7,64", "8/14/2015 06:19", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "62", "0,95", "58,9", "AA3", "36,5", "2"] },
                    { "id": 7, "bgColor": "LightGreen", "userdata": [], "data": ["JFK", "6RQ0186", "8/14/2015", "PAR", "0 Of 8", "65,3", "37,22", "", "LOCATION (JFK)", "BF GOODRICH", "AIRBUS C O KUEHNE AND NAGEL", "176", "0,31", "54,56", "AA3", "176", "8"] }
                ]
            }
        },
        { "id": 9, "nodeText": "Short Shipments", "nodeValue": "Short", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 7, "nodeText": "Upperdeck / Over Length", "nodeValue": "Oversize", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 3, "nodeText": "Unknown Shippers", "nodeValue": "UKS", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        {
            "id": 4, "nodeText": "Hazmat", "nodeValue": "Hazmat", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": {
                "rows": [
                    { "id": 0, "bgColor": "White", "userdata": [], "data": ["JFK", "6RQ1532", "8/14/2015", "PAR", "1 Of 1", "57,1", "10,03", "8/14/2015 06:13", "HAZMAT", "AIR CRUISERS CO INC", "AIRBUS OPERATIONS GMBH", "57,5", "32,06", "32,06", "HAZ", "47,5", "1"] }
                ]
            }
        },
        { "id": 8, "nodeText": "Ocean Shipments", "nodeValue": "OceanSh", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 2, "nodeText": "On-Hold", "nodeValue": "On-Hold", "allowSelect": true, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 5, "nodeText": "AES Error", "nodeValue": "AES", "allowSelect": true, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 6, "nodeText": "Incomplete", "nodeValue": "Incomplete", "allowSelect": true, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 11, "nodeText": "AMS Hold", "nodeValue": "AMSHold", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null },
        { "id": 12, "nodeText": "Pending Zone / Rate", "nodeValue": "UPSRateMissing", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": null }
    ]
};


var otherShipments = {
    "Status": { "Status": true, "Message": "" }, "Data":
        {
            "id": 1, "nodeText": "Pending", "nodeValue": "Pending", "allowSelect": false, "orderBy": 0, "ColumnNames": "Origin,AirbillNo,ShipmentDate,Destination,NoOfPackages,GrossWeight,Volume,ReceiveDate,ReceiveLocation,SenderName,ReceiverName,ChargeableWeight,Rate,WeightCharge,ServiceType,DimWeight,Slac", "data": {
                "rows": [
                    { "id": 0, "bgColor": "LightGreen", "userdata": [], "data": ["DCA", "5MEY876", "8/13/2015", "PAR", "1 Of 1", "1", "0,38", "8/14/2015 06:20", "PAR", "CARESTREAM HEALTH INC", "AP-HM", "2", "45", "45", "AA5", "2", "1"] },
                    { "id": 1, "bgColor": "LightGreen", "userdata": [], "data": ["DCA", "5MEY883", "8/13/2015", "PAR", "1 Of 1", "1", "0,38", "8/14/2015 06:20", "PAR", "CARESTREAM HEALTH INC", "CENTRE HOSPITALIER GENERAL", "2", "45", "45", "AA5", "2", "1"] },
                    { "id": 2, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3524", "8/12/2015", "PAR", "1 Of 1", "51,2", "6,67", "8/13/2015 16:39", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "51,5", "0,95", "48,93", "AA3", "31,5", "1"] },
                    { "id": 3, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3525", "8/12/2015", "PAR", "1 Of 1", "51,2", "6,67", "8/13/2015 16:38", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "51,5", "0,95", "48,93", "AA3", "31,5", "1"] },
                    { "id": 4, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3526", "8/12/2015", "PAR", "2 Of 2", "102,5", "13,33", "8/13/2015 16:38", "PAR", "MOOG INC", "KN TOULOUSE AIRBUS POLE", "102,5", "0,95", "97,38", "AA3", "63", "2"] },
                    { "id": 5, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3527", "8/13/2015", "PAR", "2 Of 2", "36,2", "5,83", "8/14/2015 06:19", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "36,5", "0,95", "34,68", "AA3", "27,5", "2"] },
                    { "id": 6, "bgColor": "LightGreen", "userdata": [], "data": ["ROC", "6LX3528", "8/13/2015", "PAR", "2 Of 2", "61,6", "7,64", "8/14/2015 06:19", "PAR", "MOOG AIRCRAFT GROUP", "KN TOULOUSE AIRBUS POLE", "62", "0,95", "58,9", "AA3", "36,5", "2"] },
                    { "id": 7, "bgColor": "LightGreen", "userdata": [], "data": ["JFK", "6RQ0186", "8/14/2015", "PAR", "0 Of 8", "65,3", "37,22", "", "LOCATION (JFK)", "BF GOODRICH", "AIRBUS C O KUEHNE AND NAGEL", "176", "0,31", "54,56", "AA3", "176", "8"] }
                ]
            }
        }
};

var treeData = {
    "Status": { "Status": true, "Message": "" }, "Data": {
        "id": "0", "item": [
            {
                "id": null, "text": "JFK", "child": null, "im0": "./images/_open.gif", "item": [
                   {
                       "id": "1", "im0": "./Images/root.gif", "text": "Routes", "item": [
                          { "id": "LHR", "im0": "./Images/xpNetPlace.gif", "text": "LHR - 29,166 KG ( 8 )" },
                          { "id": "MAN", "im0": "./Images/xpNetPlace.gif", "text": "MAN - 18,583 KG ( 1 )" },
                          { "id": "ORD", "im0": "./Images/xpNetPlace.gif", "text": "ORD - 20,339 KG ( 13 )" },
                          { "id": "OSL", "im0": "./Images/xpNetPlace.gif", "text": "OSL - 6 KG ( 1 )" },
                          { "id": "PAR", "im0": "./Images/xpNetPlace.gif", "text": "PAR - 2,586 KG ( 22 )" },
                          { "id": "PEN", "im0": "./Images/xpNetPlace.gif", "text": "PEN - 29 KG ( 2 )" },
                          { "id": "PRG", "im0": "./Images/xpNetPlace.gif", "text": "PRG - 160 KG ( 1 )" },
                          { "id": "RIC", "im0": "./Images/xpNetPlace.gif", "text": "RIC - 7,865 KG ( 24 )" },
                          { "id": "ROC", "im0": "./Images/xpNetPlace.gif", "text": "ROC - 203,999 KG ( 160 )" },
                          { "id": "ROM", "im0": "./Images/xpNetPlace.gif", "text": "ROM - 165 KG ( 1 )" },
                          { "id": "SCL", "im0": "./Images/xpNetPlace.gif", "text": "SCL - 418 KG ( 3 )" },
                          { "id": "SIN", "im0": "./Images/xpNetPlace.gif", "text": "SIN - 157 KG ( 4 )" },
                          { "id": "STO", "im0": "./Images/xpNetPlace.gif", "text": "STO - 160 KG ( 1 )" },
                          { "id": "STR", "im0": "./Images/xpNetPlace.gif", "text": "STR - 531 KG ( 3 )" },
                          { "id": "SYD", "im0": "./Images/xpNetPlace.gif", "text": "SYD - 127 KG ( 1 )" },
                          { "id": "SYR", "im0": "./Images/xpNetPlace.gif", "text": "SYR - 8,396 KG ( 12 )" },
                          { "id": "TRN", "im0": "./Images/xpNetPlace.gif", "text": "TRN - 145 KG ( 3 )" },
                          { "id": "VIE", "im0": "./Images/xpNetPlace.gif", "text": "VIE - 632 KG ( 4 )" },
                          { "id": "WAW", "im0": "./Images/xpNetPlace.gif", "text": "WAW - 1 KG ( 1 )" },
                          { "id": "YYZ", "im0": "./Images/xpNetPlace.gif", "text": "YYZ - 674 KG ( 3 )" }
                       ]
                   },
                   { "id": "2", "im0": "./Images/xpNetwork.gif", "text": "Consols", "item": [] },
                   { "id": "8", "im0": "./Images/InboundScan.gif", "text": "Domestic Consols", "item": [] }
                ], "userdata": []
            }
        ]
    }
}