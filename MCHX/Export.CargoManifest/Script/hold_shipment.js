﻿var holdReason;
var holdReasonWindow = function (data) {

	var _this = this;
	this.data = data[0];

	this.init = function () {

		//init window

		this.holdWin = CM.W.createWindow("hold_shipment", 0, 0, 400, 300);
		this.holdWin.denyPark();
		this.holdWin.denyResize();
		this.holdWin.setText('Hold/Release Shipment');
		this.holdWin.center();

		this.holdWin.attachEvent('onClose', function () {
			_this.holdWin = null;
			return true;
		});

		this.getFormData()

	};

	this.getFormData = function () {
		this.holdWin.progressOn();
		sendAjaxRequest('Export/GetShipmentHoldRelease?airbillNo=' + this.data.airbillNo + '&origin=' + this.data.origin, 'GET', null, function (response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if (response.Data && response.Data[0]) {
				if (response.Data[0].StatusDescription.toLowerCase() !== 'release') {
					_this.initForm(true, response.Data[0]);
				}
				else {
					_this.initForm(false, response.Data[0]);
				}
				_this.holdWin.progressOff();
			}
		})
	};

	this.initForm = function (status, data) {

		if (status) {
            this.holdWin.setDimension(280, 220);
			this.form = this.holdWin.attachForm([
				{type: "label", name: "label", label: "Housebill:" + this.data.airbillNo + '-' + this.data.origin},
				{type: "input",	name: "reason",	label: "Hold Reason:", labelWidth: 100,	position: "label-top", width: 215, rows: 3},
				{type: "hidden", name: "StatusDescription"},
				{type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5, list: [
						{type: "button", name: "hold_shipment", value: "Hold Shipment"},
						{type: "newcolumn"},
						{type: "button", name: "cancel", value: "Cancel"}
					]
				}
			]);

		}
		else {
			this.form = this.holdWin.attachForm([
				{type: "label", name: "label", label: "Housebill:" + this.data.airbillNo + '-' + this.data.origin},
				{type: "input", name: "HDReason", label: "Hold Reason:", width: 215, labelWidth: 100, rows: 3},
				{type: "input", name: "HDBy", label: "Hold By:", labelWidth: 100, width: 215},
				{type: "calendar", name: "HDDate", label: "Hold Date:", labelWidth: 100, dateFormat: CM.calendarDateFormat,	width: 215},
				{type: "input", name: "reason", label: "Release reason:", labelWidth: 100, width: 215, rows: 3},
				{type: "hidden", name: "StatusDescription"},
				{
					type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5,	list: [
						{type: "button", name: "release_shipment", value: "Release Shipment"},
						{type: "newcolumn"},
						{type: "button", name: "cancel", value: "Cancel"}
					]
				}
			]);

		}
		this.form.attachEvent('onButtonClick', function (id) {
			var reason;
			if (id === 'cancel') {
				_this.holdWin.close();
			}
			else if (id === 'release_shipment' || id === 'hold_shipment') {
				reason = this.getItemValue('reason');
				if (reason) {
					_this.holdReason(reason);
				}
				else {
					showAlert('Hold Shipment', 'You must enter a reason before continue')
				}
			}
		});
		data.HDDate = dhx4.date2str(new Date(data.HDDate), CM.calendarDateFormat);
		this.form.setFormData(data);
	};

	this.holdReason = function () {
		sendAjaxRequest('/Export/HoldReleaseShipment?airbillNo=' + _this.data.airbillNo + '&origin=' + _this.data.origin + '&reason=' + _this.form.getItemValue('reason') + '&userId=' + helpers.getUserName() + '&gateway=' + CM.gateway + '&holdStatus=' + _this.form.getItemValue('StatusDescription'), 'POST', null, function (response) {
			response = dhx4.s2j(response.xmlDoc.responseText);
			if (response && response.Data) {
				_this.holdWin.close();
				dhx4.callEvent('reloadSelectedTab', []);
			}
		});
	};

	this.init();
	return this;
};

dhx4.attachEvent('showHoldReasonWindow', function (data) {
	holdReason = new holdReasonWindow(data)
});