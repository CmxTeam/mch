﻿var changeDestination;
var changeDestinationWindow = function (data, routes) {

	var _this = this;
	this.data = data;
	this.routes = routes;

	this.init = function () {

		//init window

		this.destWin = CM.W.createWindow("change_destination", 0, 0, 260, 150);
		this.destWin.denyPark();
		this.destWin.denyResize();
		this.destWin.setText('Change Masterbill Destination');
		this.destWin.center();

		this.destWin.attachEvent('onClose', function () {
			_this.destWin = null;
			return true;
		});

		this.form = this.destWin.attachForm([
			{type: "input", name: "dest", label: "Enter New Destination:", position: "label-top", width: 215},
			{
				type: "block", name: "info_block", blockOffset: 0, offsetLeft: 0, offsetTop: 5,
				list: [
					{type: "button", name: "change_destination", value: "Change Destination"},
					{type: "newcolumn"},
					{type: "button", name: "cancel", value: "Cancel"}
				]
			}
		]);

		this.form.attachEvent('onButtonClick', function (id) {
			var destination;
			if (id === 'cancel') {
				_this.destWin.close();
			}
			else if (id === 'change_destination') {
				destination = this.getItemValue('dest');
				if (destination) {
					_this.changeDestination(destination);
				}
				else {
					showAlert('Masterbill Destination', 'Please enter some Destination')
				}
			}
		});
	};

	this.changeDestination = function (destination) {
		var i, exist = false, promise = [];
		for (i = 0; i < this.routes.length; i++) {
			if (this.routes[i].value.toLowerCase() === destination.toLowerCase()) {
				exist = true;
				break;
			}
		}
		if (!exist) {
			showAlert('Masterbill Destination', 'Unable to Find Destination');
			return false;
		}
		for (i = 0; i < this.data.length; i++) {
			promise.push(
				new Promise(function(resolve, reject) {
					sendAjaxRequest('Export/ChangeMasterbillDest?housebill=' + (_this.data[i].airbillNo + '-' + _this.data[i].origin) + '&newDest=' + destination + '&userId=' + helpers.getUserName() + '&gateway=' + CM.gateway, 'POST', null, function (response) {
						response = dhx4.s2j(response.xmlDoc.responseText);
						if(response && response.Status && response.Status.Status){
							resolve();
						}
						else {
							reject();
						}
					});
				})
			);
		}
		Promise.all(promise).then(function(){
			_this.destWin.close();
		}).catch(function(err) {
			showAlert('Error', 'Something went wrong');
		});

	};

	this.init();
	return this;
};

dhx4.attachEvent('showChangeDestinationWindow', function (data, routes) {
	changeDestination = new changeDestinationWindow(data, routes)
});