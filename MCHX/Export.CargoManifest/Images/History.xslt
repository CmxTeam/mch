<?xml version="1.0" encoding="ISO-8859-1" ?>



<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/History">

			<TABLE id="Table3" class="HistoryTable" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR class="Smallheadings">
					<TD colspan="4" align="Center"> Source </TD>
				</TR>
				<TR>
					<TD align="right">Masterbill No:</TD>
					<TD class="Colordef"><xsl:value-of select="Summary/Origin"/>-<xsl:value-of select="Summary/Carrier"/>-<xsl:value-of select="Summary/Consol"/>-<xsl:value-of select="Summary/Destination"/></TD>
					<TD align="right">Rate Source:</TD>
					<TD class="Colordef"><xsl:value-of select="Summary/Source"/></TD>
				</TR>
				<TR>
					<TD align="right">Service Group:</TD>
					<TD class="Colordef"><xsl:value-of select="Summary/ServiceGroup"/></TD>
					<TD align="right">Service Code:</TD>
					<TD class="Colordef"><xsl:value-of select="Summary/ServiceCode"/></TD>
				</TR>
				<TR>
					<TD align="right">Dim Weight: </TD>
					<TD class="Colordef"><xsl:value-of select='format-number(Summary/DimWeight, "##,##0.00 KG")'/></TD>
					<TD align="right"> Contract Number:</TD>
					<TD class="Colordef"><xsl:value-of select="WeightCharges/ContactNumber" /></TD>
				</TR>				
				<TR>
					<TD align="right">Actual Weight:</TD>
					<TD class="Colordef"><xsl:value-of select='format-number(Summary/ActualWeight, "##,##0.00 KG")'/></TD>
					<TD align="right"></TD>
					<TD class="Colordef"></TD>
				</TR>				
				<TR>
					<TD align="right">Chargeable Weight:</TD>
					<TD class="Colordef"><xsl:value-of select='format-number(Summary/ChargeableWeight, "##,##0.00 KG")'/></TD>
					<TD align="right"></TD>
					<TD class="Colordef"></TD>
				</TR>
			</TABLE>
<br/>			  
<xsl:choose xml:space="default">
	<xsl:when test="Summary/IsLoose = 'True'">								
  <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0" class="HistoryTable">
				<TR>
					<TD align="center" colSpan="4" class="Smallheadings">Loose Rates</TD>
				</TR>
              			<TR bgcolor="whitesmoke">
							<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Break</TD>
							<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Weight</TD>
							<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Rate</TD>
							<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Selected</TD>
						</TR>				
				<xsl:for-each select="LooseRates">
					<xsl:sort select="Break" data-type="number"/>
						<TR onmouseover="this.style.backgroundColor = 'whitesmoke';" onmouseout="this.style.backgroundColor = '';">
							<TD align="center" class="Colordef"><xsl:value-of select='Break'/></TD>
							<TD align="center" class="Colordef"><xsl:value-of select="BreakWeight"/></TD>
							<TD align="center" class="Colordef"><xsl:value-of select='format-number(BreakRate, "##,##0.00")'/></TD>
							<TD align="center">
								<xsl:choose xml:space="default">
									<xsl:when test="BreakSelected = 'True'">
										<img alt="Selected Weight Break" hspace="0" src="images/savemode.gif" align="baseline" border="0"/>
									</xsl:when>
								</xsl:choose>
							</TD>
						</TR>
				</xsl:for-each>
          

				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<br/>
				</xsl:when>
	</xsl:choose>			
<xsl:choose xml:space="default">
	<xsl:when test="Summary/IsUld = 'True'">
									
				
			
			<TABLE class="HistoryTable" id="Table2" cellSpacing="0" cellPadding="0" width="95%" align="center"
					border="0">
					<TR class="Smallheadings">
						<TD align="center" colSpan="7">ULD Rates</TD>
					</TR>
					<TR bgcolor="whitesmoke">
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">ULD</TD>
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">PivotWeight</TD>
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">PivotRate</TD>
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">OverPivotRate</TD>
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Flat</TD>
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Min</TD>
						<TD align="center" style="BORDER-BOTTOM: black 1px solid;">Max</TD>
					</TR>
					<xsl:for-each select="UldRates">
				
					<TR onmouseover="this.style.backgroundColor = 'whitesmoke';" onmouseout="this.style.backgroundColor = '';">
						<TD align="center" class="Colordef"><xsl:value-of select="Uld"/></TD>
						<TD align="center" class="Colordef"><xsl:value-of select="PivotWeight"/></TD>
						<TD align="center" class="Colordef"><xsl:value-of select="PivotRate"/><xsl:text> </xsl:text> <xsl:value-of select="PivotRateType"/></TD>
						<TD align="center" class="Colordef"><xsl:value-of select="OverPivotRate"/></TD>
						<TD align="center" class="Colordef"><xsl:value-of select='format-number(Flat, "##,##0.00")'/></TD>
						<TD align="center" class="Colordef"><xsl:value-of select='format-number(Min, "##,##0.00")'/></TD>
						<TD align="center" class="Colordef"><xsl:value-of select='format-number(Max, "##,##0.00")'/></TD>
					</TR>
				</xsl:for-each>

					<TR>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
				</TABLE>
				<br/>
				</xsl:when>
	</xsl:choose>

<TABLE id="CompleteTotals" class="HistoryTable" cellSpacing="0" cellPadding="0" width="95%" style="FONT-SIZE: x-small; FONT-FAMILY: verdana"
					align="center">
					<TR class="Smallheadings">
						<TD vAlign="top" colspan="4" align="Center">Totals</TD>

					</TR>
					<TR bgcolor="whitesmoke"  >
						<TD vAlign="top" align="right" style="BORDER-BOTTOM: black 1px solid;"><![CDATA[ ]]></TD>
						<TD vAlign="top" align="right" style="BORDER-BOTTOM: black 1px solid;">Rate</TD>
						<TD vAlign="top" align="right" style="BORDER-BOTTOM: black 1px solid;">Weight</TD>
						<TD vAlign="top" align="right" style="BORDER-BOTTOM: black 1px solid;">Total</TD>
					</TR>
					<xsl:choose xml:space="default">
						<xsl:when test="WeightCharges/Rate != 0">	
							<TR>
								<TD vAlign="top" align="right">Weight Charge:</TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="WeightCharges/Rate" /><![CDATA[ ]]><xsl:value-of select="WeightCharges/RateType" /></TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(WeightCharges/Weight, "##,##0.00 KG")' /></TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(WeightCharges/WeightChargesTotal, "$ ###,###,##0.00")' /></TD>
							</TR>
						</xsl:when>
					</xsl:choose>						
					<xsl:choose xml:space="default">
						<xsl:when test="Tact/TactRate != 0">	
							<TR>
								<TD vAlign="top" align="right">Tact:</TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="Tact/TactRate" /><![CDATA[ ]]><xsl:value-of select="Tact/TactRateType" /></TD>
								<TD vAlign="top" align="right" class="Colordef"></TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(Tact/TactRateTotal, "$ ###,###,##0.00")' /></TD>
							</TR>
						</xsl:when>
					</xsl:choose>					
					<xsl:choose xml:space="default">
						<xsl:when test="Spot/SpotRate != 0">	
							<TR>
								<TD vAlign="top" align="right">Spot:(# <xsl:value-of select="Spot/SpotNumber" />)</TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="Spot/SpotRate" /><![CDATA[ ]]><xsl:value-of select="Spot/SpotRateType" /></TD>
								<TD vAlign="top" align="right" class="Colordef"></TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(Spot/SpotRateTotal, "$ ###,###,##0.00")' /></TD>
							</TR>
						</xsl:when>
					</xsl:choose>	
					<xsl:choose xml:space="default">
						<xsl:when test="Fuel/FuelRate != 0">	
							<TR>
								<TD vAlign="top" align="right">Fuel:</TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="Fuel/FuelRate" /><![CDATA[ ]]><xsl:value-of select="Fuel/FuelRateType" /></TD>
								<TD vAlign="top" align="right" class="Colordef"><![CDATA[ ]]></TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(Fuel/FuelRateTotal, "$ ###,###,##0.00")' /></TD>
							</TR>			
						</xsl:when>
					</xsl:choose>					

					<xsl:choose xml:space="default">
						<xsl:when test="Security/SecurityRate != 0">		
									<TR>
										<TD vAlign="top" align="right">Security:</TD>
										<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="Security/SecurityRate" /><![CDATA[ ]]><xsl:value-of select="Security/SecurityRateType" /></TD>
										<TD vAlign="top" align="right" class="Colordef"><![CDATA[ ]]></TD>
										<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(Security/SecurityRateTotal, "$ ###,###,##0.00")' /></TD>
									</TR>			
						</xsl:when>
					</xsl:choose>
					<xsl:choose xml:space="default">
						<xsl:when test="Mdc/MdcRate != 0">		
									<TR>
										<TD vAlign="top" align="right">Mdc:</TD>
										<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="Mdc/MdcRate" /><![CDATA[ ]]><xsl:value-of select="Mdc/MdcRateType" /></TD>
										<TD vAlign="top" align="right" class="Colordef"><![CDATA[ ]]></TD>
										<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(Mdc/MdcRateTotal, "$ ###,###,##0.00")' /></TD>
									</TR>			
						</xsl:when>
					</xsl:choose>					
					<xsl:choose xml:space="default">
						<xsl:when test="Dgr/DgrRate != 0">		
							<TR>
								<TD vAlign="top" align="right">Dgr:</TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select="Dgr/DgrRate" /><![CDATA[ ]]><xsl:value-of select="Dgr/DgrRateType" /></TD>
								<TD vAlign="top" align="right" class="Colordef"><![CDATA[ ]]></TD>
								<TD vAlign="top" align="right" class="Colordef"><xsl:value-of select='format-number(Dgr/DgrRateTotal, "$ ###,###,##0.00")' /></TD>
							</TR>			
						</xsl:when>
					</xsl:choose>					
					
					<TR>
						<TD vAlign="top" align="right">Totals Other Charges:</TD>
						<TD vAlign="top" align="right" class="Colordef"><![CDATA[ ]]></TD>
						<TD vAlign="top" align="right" class="Colordef">Show Total: <xsl:value-of select='format-number(Summary/ShowOtherCharges, "$ ###,###,##0.00")' /></TD>
						<TD vAlign="top" align="right" class="Colordef">Net-Net Total: <xsl:value-of select='format-number(Summary/TotalOtherCharges, "$ ###,###,##0.00")' /></TD>
					</TR>
					<TR>
						<TD vAlign="top" align="right">Total Freight Charges:</TD>
						<TD vAlign="top" align="right" class="Colordef"></TD>
						<TD vAlign="top" align="right" class="Colordef">Show Total: <xsl:value-of select='format-number(Summary/ShowFreightCharge, "$ ###,###,##0.00")' /></TD>
						<TD vAlign="top" align="right" class="Colordef">Net-Net Total: <xsl:value-of select='format-number(Summary/TotalFreightCharge, "$ ###,###,##0.00")' /></TD>
					</TR>
				</TABLE>		
<br/>
<xsl:choose xml:space="default">
	<xsl:when test="Errors != ''">
		<TABLE class="HistoryTable" id="Errors" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
			<TR class="Smallheadings">
			<td width="1px"><img id="ersimg" src="images/ig_tblMinus.gif" onclick='showhide("ers",this.id);'></img></td>
				<TD class="HistoryCell">Errors</TD>
			</TR>
			<td> </td>
			<TR>
				<td>
				</td>
				<td>
					<table id="ers" style=" FONT-SIZE: x-small; FONT-FAMILY: verdana" class="labelsHeadings">
						<xsl:for-each select="Errors">
							<tr>
								<td><li></li><xsl:value-of select="Error" /></td>
							</tr>									
						</xsl:for-each>									
					</table>
				</td>
			</TR>
		</TABLE>	
				</xsl:when>
	</xsl:choose>	

		<xsl:choose xml:space="default">
			<xsl:when test="Messages != ''">
				<TABLE class="HistoryTable" id="Messages" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
					<TR class="Smallheadings">
					<td width="1px"><img id="msgsimg" src="images/ig_tblMinus.gif" onclick='showhide("msgs",this.id);'></img></td>
						<TD class="HistoryCell">Messages</TD>
					</TR>
					<td> </td>
					<TR>
						<td>
						</td>
						<td>
							<table id="msgs" style=" FONT-SIZE: x-small; FONT-FAMILY: verdana" class="labelsHeadings">
								<xsl:for-each select="Messages">
									<tr>
										<td><li></li><xsl:value-of select="Message" /></td>
									</tr>									
								</xsl:for-each>									
							</table>
						</td>
					</TR>
				</TABLE>	
			</xsl:when>
		</xsl:choose>


						
    </xsl:template>
</xsl:stylesheet>
