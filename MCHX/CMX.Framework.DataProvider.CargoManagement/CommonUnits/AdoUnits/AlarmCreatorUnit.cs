﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class AlarmCreatorUnit : CmCoreDatabaseConnector<bool>
    {
        private string _connectionString;

        public AlarmCreatorUnit(string connectionString, string shipmentNumber, string gateway, string remark, string userName)
            : base(new CommandParameter("@HouseBillNo", shipmentNumber),
                   new CommandParameter("@Gateway", gateway),
                   new CommandParameter("@Description", remark),
                   new CommandParameter("@UserName", userName))
        {
            _connectionString = connectionString;
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"
                            Declare @Remark nvarchar(max) = 'ALARM CLEARED: ' + @Description
                            Declare @Origin nvarchar(3)
                            Declare @Destination nvarchar(3)

                            Select @Origin = Origin, @Destination = Destination From CMXGLOBALDB.Fsp.Queue where CMXGLOBALDB.Fsp.Queue.HouseBillNO = @HouseBillNo

                            UPDATE CMXGlobalDB.Fsp.Screening SET IsProcessed = 1, CancelledDate = GETDATE() 
                            WHERE IsProcessed = 0 AND HousebillNumber = @HouseBillNo AND [ResultCode] = 'ALARM'

                            INSERT INTO CMXGLOBALDB.dbo.Remarks(RecDate, Reference, Description, UserID, Gateway, ActionID, StatusID, Part) 
                            VALUES(GetDate(), Isnull(@Origin, '???') + '-' + @HouseBillNo + '-' + Isnull(@Destination, '???'), @Remark, @UserName, @Gateway, 105, 56, '');

                            DELETE FROM Hawb_Statuses WHERE AirbillNo = @HouseBillNo AND StatusId = 56
                            DELETE FROM Hawb_Screening WHERE HouseBillID = (SELECT TOP(1) RecID FROM Hawb WHERE AirbillNo = @HouseBillNo)

                            INSERT INTO Remarks(RecDate, [Reference], [Description], [UserID], [Gateway], [ActionID], [StatusId]) 
                            VALUES (GetDate(), UPPER(@HouseBillNo), UPPER('SCREENING ALARM WAS CLEARED BY ' + @UserName), UPPER(@UserName), UPPER(@Gateway), 105, 56);

                            INSERT INTO CMXGLOBALDB.dbo.Remarks(RecDate, [Reference], [Description], [UserID], [Gateway], [ActionID], [StatusId]) 
                            VALUES (GetDate(), Isnull(@Origin, '???') + '-' + @HouseBillNo + '-' + Isnull(@Destination, '???'), UPPER('SCREENING ALARM WAS CLEARED BY ' + @UserName), UPPER(@UserName), UPPER(@Gateway), 105, 56);

                            INSERT INTO HostPlus_Remarks(AirbillNo, Remark, Status, Gateway) VALUES (UPPER(@HouseBillNo), UPPER('SCREENING ALARM WAS CLEAR BY ' + @UserName), 0, UPPER(@Gateway))";
            }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }
    }
}