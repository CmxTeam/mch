﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class HPLocationCreator : CmCoreDatabaseConnector<IdNameEntity>
    {
        public HPLocationCreator(string gateway)
            : base(new CommandParameter("@Gateway", gateway))
        {
        }

        public override string CommandQuery
        {
            get { return @"SELECT RecId, Location FROM HostPlus_Locations WHERE @Gateway is null or Location = @Gateway"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override IdNameEntity ObjectInitializer(List<DataRow> rows)
        {
            return rows.Select(s => new IdNameEntity
            {
                Id = Convert.ToInt64(s["RecId"]),
                Name = (s["Location"] as string)
            }).FirstOrDefault();
        }
    }
}