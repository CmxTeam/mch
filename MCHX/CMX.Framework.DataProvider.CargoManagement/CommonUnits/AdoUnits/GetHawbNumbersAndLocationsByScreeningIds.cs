﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class GetHawbNumbersAndLocationsByScreeningIds : CmCoreDatabaseConnector<IEnumerable<Pair<string, Location>>>
    {        
        public GetHawbNumbersAndLocationsByScreeningIds(IEnumerable<long> ids)
            : base(new CommandParameter("@Ids", string.Join(",", ids)))
        {            
        }
     
        public override IEnumerable<Pair<string, Location>> ObjectInitializer(List<DataRow> rows)
        {
            return rows.Select(r => new Pair<string, Location>
            {
                First = r[0].ToString(),
                Second = new Location
                {
                    Id = r[1].TryParse<long>(),
                    LocationName = r[2].TryParse<string>(),
                    WebConnectionName = r[3].TryParse<string>()
                }
            }).ToList();
        }

        public override string CommandQuery
        {
            get 
            {
                return @"Select Distinct HousebillNumber, LocationId, Location, HostPlus_Locations.WebConnectionName from Fsp.Screening Inner Join HostPlus_Locations on HostPlus_Locations.RecId = Fsp.screening.LocationId
                            Where Fsp.Screening.RecId in (Select Item From dbo.Split(@Ids, ','))";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
