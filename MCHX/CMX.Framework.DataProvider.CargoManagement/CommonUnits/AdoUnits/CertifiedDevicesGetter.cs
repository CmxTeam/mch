﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class CertifiedDevicesGetter : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public override IEnumerable<IdNameEntity> ObjectInitializer(List<DataRow> rows)
        {
            return rows.Select(r => new IdNameEntity 
            {
                Id = r["RecId"].TryParse<long>(),
                Name = r["ModelNumber"].TryParse<string>()
            });
        }

        public override string CommandQuery
        {
            get { return @"Select RecId, ModelNumber from CertifiedDevices"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}