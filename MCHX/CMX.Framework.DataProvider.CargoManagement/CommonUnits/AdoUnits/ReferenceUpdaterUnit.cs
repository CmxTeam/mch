﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class ReferenceUpdaterUnit : CmCoreDatabaseConnector<bool>
    {
        public ReferenceUpdaterUnit(IEnumerable<long> ids, string newShipmentNumber, string userName)
            : base(new CommandParameter("@Ids", string.Join(",", ids)),
                new CommandParameter("@NewHouseBillNo", newShipmentNumber),
                new CommandParameter("@UserName", userName))
        {
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Declare @Reference nvarchar(max)
                         Declare @Gateway nvarchar(3)
                         Select top 1 @Reference = Origin + '-' + @NewHouseBillNo + '-' + Destination, @Gateway = Gateway
                         from fsp.Queue where HousebillNo = @NewHouseBillNo
                         Order by RecDate desc  
    
                        Update fsp.Screening Set HousebillNumber = @NewHouseBillNo 
                        Output
	                        Getutcdate(), 
	                        @Reference, 
	                        'Plasmagram HAWB changed from ' + Deleted.HousebillNumber + ' to ' + @NewHouseBillNo, 
	                        @UserName, 
	                        @Gateway, null, null, null, null, null, null
                        INTO REMARKS
                        Where RecId in (Select Item From dbo.Split(@Ids, ','))";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}