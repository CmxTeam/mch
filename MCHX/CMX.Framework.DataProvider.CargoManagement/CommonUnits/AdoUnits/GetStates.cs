﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class GetStates : CmCoreDatabaseConnector<IEnumerable<StateModel>>
    {
        public GetStates(long? countryId = null)
            : base(new CommandParameter("@CountryId", countryId))
        {
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override string CommandQuery
        {
            get { return @"select RecId, Name from Profile.States where (@CountryId is null OR @CountryId = CountryID)"; }
        }
        public override IEnumerable<StateModel> ObjectInitializer(List<DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(r => new StateModel
            {
                Id = r["RecId"].TryParse<long>(),
                StateProvince = r["Name"].ToString()
            });

        }
    }
}
