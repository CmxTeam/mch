﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class SimpleDeviceCreator : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
	{
		public int? LocationId { get; set; }
		public int? ScreeningMethodId { get; set; }
		public int? ScreeningAreaId { get; set; }

		public override string CommandQuery
		{
			get
			{
				return string.Format(
					  @"Select SM.Code + '-' + FD.SerialNumber as Device, FD.RecId from fsp.Devices FD
                        left join CertifiedDevices CD On CD.RecId = FD.CertifiedDeviceId
                        left join ScreeningMethods SM On SM.RecId = CD.ScreeningMethodId
                        where ({0} = 0 or SM.RecId = {0}) and ({1} = 0 or FD.LocationId = {1}) and ({2} = 0 or FD.WarehouseLocationId = {2})
                        Order By Device",
					  LocationId.GetValueOrDefault(), ScreeningMethodId.GetValueOrDefault(), ScreeningAreaId.GetValueOrDefault());
			}
		}

		public override string ConnectionString
		{
			get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
		}

		public override IEnumerable<IdNameEntity> ObjectInitializer(List<DataRow> rows)
		{
			return rows.Select(r => new IdNameEntity
			{
				Id = Convert.ToInt64(r["RecId"]),
				Name = r["Device"].ToString()
			});
		}
	}
}
