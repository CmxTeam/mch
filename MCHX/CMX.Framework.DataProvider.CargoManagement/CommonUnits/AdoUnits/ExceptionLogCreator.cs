﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class ExceptionLogCreator : CmCoreDatabaseConnector<long>
    {
        public ExceptionLogCreator(string applicationName, string exception, string stackTrace)
            : base(new CommandParameter("@ApplicationName", applicationName),
                new CommandParameter("@Exception", exception),
                new CommandParameter("@StackTrace", stackTrace))
        {            
        }

        public override string CommandQuery
        {
            get { return "Insert Into ApplicationExceptionLogs (ApplicationName, Exception, StackTrace) Values (@ApplicationName, @Exception, @StackTrace)"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override long ObjectInitializer(List<DataRow> rows)
        {
            return 0;
        }        
    }
}