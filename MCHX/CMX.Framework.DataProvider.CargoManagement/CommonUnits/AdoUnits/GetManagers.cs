﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Authentication;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class GetManagers : CmCoreDatabaseConnector<IEnumerable<AddressbookAccountModel>>
    {
        public GetManagers(long? managerId = null)
            : base(new CommandParameter("@ManagerId", managerId))
        {

        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override string CommandQuery
        {
            get { return @"select ID, AddressBook.FirstName + ' ' + AddressBook.LastName as Name,Firstname,LastName from AddressBook where (@ManagerId is null OR @ManagerId = Id)"; }
        }
        public override IEnumerable<AddressbookAccountModel> ObjectInitializer(List<DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;          

            return rows.Select(r => new AddressbookAccountModel
            {
                Id = r["ID"].TryParse<long>(),
                UserID = r["Name"].ToString(),
                FirstName = r["Firstname"].ToString(),
                LastName = r["LastName"].ToString()
            });

        }
    }
}
