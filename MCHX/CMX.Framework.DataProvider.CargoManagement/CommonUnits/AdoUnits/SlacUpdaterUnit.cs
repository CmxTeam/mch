﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class SlacUpdaterUnit : CmCoreDatabaseConnector<bool>
    {
        private readonly string _connectionString;

        public SlacUpdaterUnit(string connectionString, string shipmentNumber, int newSlac, string gateway, string userName)
            : base(new CommandParameter("@HouseBillNo", shipmentNumber),
                new CommandParameter("@Slac", newSlac),
                new CommandParameter("@UserName", userName),
                new CommandParameter("@Gateway", gateway))
        {
            _connectionString = connectionString;
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @"Declare @Origin nvarchar(3)
                        Declare @Destination nvarchar(3)
                        Declare @SlacToBeAdded int
                        Declare @QueueLastItem bigint
            
                        Select top 1 @Origin = Origin, @Destination = Destination From Hawb where AirbillNo = @HouseBillNo

                        Set @Slac = case when @Slac < (Select Sum(isnull(TotalPieces, 0)) From CMXGLOBALDB.fsp.Queue 
	                        where CMXGLOBALDB.fsp.Queue.HousebillNo = @HouseBillNo) then 
	                        (Select Sum(isnull(TotalPieces, 0)) From CMXGLOBALDB.fsp.Queue 
	                        where CMXGLOBALDB.fsp.Queue.HousebillNo = @HouseBillNo) else @Slac end
                            
                        Update Hawb Set Slac = @Slac Where AirbillNo = @HouseBillNo
                        
                        Set @SlacToBeAdded = @Slac - isnull((Select Sum(isnull(HbSlac, 0)) 
                        from CMXGLOBALDB.fsp.Queue 
                        Where HouseBillNo = @HouseBillNo 
                        and (@Origin is null or Origin = @Origin) 
                        and (@Destination is null or Destination = @Destination)), 0)

                        Select @QueueLastItem = RecId from CMXGLOBALDB.fsp.queue 
                        Where HouseBillNo = @HouseBillNo 
                        and (@Origin is null or Origin = @Origin) 
                        and (@Destination is null or Destination = @Destination)
                        Order By RecDate desc

                        Update CMXGLOBALDB.fsp.queue Set HbSlac = HbSlac + @SlacToBeAdded Where RecId = @QueueLastItem
                        
                        Declare @Description nvarchar(max) = 'HAWB ' + @HouseBillNo + ' SLAC changed to ' + cast(@Slac as nvarchar(max))
                        
                        INSERT INTO CMXGLOBALDB.dbo.REMARKS (Reference, Description, UserID, Gateway) 
                        VALUES (Isnull(@Origin, '???') + '-' + @HouseBillNo + '-' + Isnull(@Destination, '???'), @Description, @UserName, @Gateway)

                        Insert Into Remarks (Reference, Description, UserID, Gateway)
                        VALUES (@HouseBillNo, @Description, @UserName, @Gateway)";
            }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }
    }
}
