﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class PortsCreator : CmCoreDatabaseConnector<IEnumerable<PortModel>>
    {
        public override string CommandQuery
        {
            get { return @"SELECT Id, IATACode FROM Ports Order By IATACode"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override IEnumerable<PortModel> ObjectInitializer(List<DataRow> rows)
        {
            return rows.Select(r => new PortModel
            {
                Id = Convert.ToInt64(r["Id"]),
                Name = r["IATACode"].ToString()
            });
        }
    }
}