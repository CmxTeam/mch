﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class GetCountries : CmCoreDatabaseConnector<IEnumerable<CountryModel>>
    {
        public GetCountries(long? countryId = null)
            : base(new CommandParameter("@CountryId", countryId))
        {
        }
        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
        public override string CommandQuery
        {
            get { return @"select RecId, Name, Code2 from Profile.Countries where (@CountryId is null OR @CountryId = RecId)"; }
        }
        public override IEnumerable<CountryModel> ObjectInitializer(List<DataRow> rows)
        {
            if (rows == null || rows.Count == 0) return null;
            return rows.Select(r => new CountryModel
            {
                Id = r["RecId"].TryParse<long>(),
                CountryName = r["Name"].ToString(),
                TwoCharacterCode = r["Code2"].ToString()
            });           
        }
    }
}
