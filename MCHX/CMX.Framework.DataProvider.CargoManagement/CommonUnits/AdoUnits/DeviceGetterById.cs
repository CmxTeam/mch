﻿using System;
using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class DeviceGetterById : CmCoreDatabaseConnector<DeviceModel>
    {
        public DeviceGetterById(long id)
            : base(new CommandParameter("@Id", id))
        {
        }

        public override DeviceModel ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return null;
            var r = rows[0];

            return new DeviceModel
                   {
                Id = Convert.ToInt32(r["RecId"]),
                SerialNumber = r["SerialNumber"].ToString(),
                DeviceBarcode = r["DeviceBarcode"].ToString(),
                CertifiedDeviceId = r["CertifiedDeviceId"].TryParse<long>(),
                LocationId = r["LocationId"].TryParse<long>(),
                WarehouseLocationId = r["WarehouseLocationId"].TryParse<long>(),
                StatusId = r["StatusId"].TryParse<int>(),
                DeviceType = r["DeviceType"].ToString()
            };
        }

        public override string CommandQuery
        {
            get { return "Select RecId, SerialNumber, DeviceBarcode, CertifiedDeviceId, LocationId, WarehouseLocationId, StatusId, DeviceType from Fsp.Devices Where RecId = @Id"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }
    }
}
