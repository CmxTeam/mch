﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class GetUserIdByLoginUnit : CmCoreDatabaseConnector<long>
    {
        public GetUserIdByLoginUnit(string login)
            : base(new CommandParameter("@Login", login))
        {           
        }

        public override string CommandQuery
        {
            get { return @"select top 1 ID from AddressBook where UserId = @Login"; }
        }

        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }

        public override long ObjectInitializer(List<DataRow> rows)
        {
            if (rows.Count == 0) return 0;
            var r = rows[0];

            return Convert.ToInt64(r["ID"]);
            
        }
    }
}