﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class ProcessFSP : CmCoreDatabaseConnector<bool>
    {
        private string _webConnectionName;
        private string _shipmentNumber;
        public ProcessFSP(string webConnectionName, string shipmentNumber)
        {
            _shipmentNumber = shipmentNumber;
            _webConnectionName = webConnectionName;
        }

        public override bool Execute()
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(string.Format(ConfigurationManager.AppSettings["ProcessFSP"] + "?connectionName={0}&houseBillNo={1}", _webConnectionName, _shipmentNumber));
            request.Method = "GET";
            using (StreamReader responseReader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                var result = responseReader.ReadToEnd();
            }
            return true;
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get { throw new NotImplementedException(); }
        }

        public override string ConnectionString
        {
            get { throw new NotImplementedException(); }
        }
    }
}
