﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class WarehouseLocationsGetterUnit : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
    {
        public WarehouseLocationsGetterUnit(long userId, long applicationId, string locationId)
            : base(new CommandParameter("@UserId", userId),
                   new CommandParameter("@LocationId", locationId),
                   new CommandParameter("@ApplicationId", applicationId))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select Distinct Warehouses.Code + ' - ' + WL.Name AS ScreeningArea, WL.RecId 
                        from WarehouseLocations WL
                        left Join Warehouses On Warehouses.RecId = WL.WarehouseId
                        inner join HostPlus_Locations on HostPlus_Locations.EntityId = Warehouses.GatewayId
                        Inner Join AppUserLocations on AppUserLocations.LocationId = HostPlus_locations.RecId
                        where WL.TypeId = 16 and PGDBEnabled = 1 
                            and (@ApplicationId is null or ApplicationId = @ApplicationId) 
	                        and (@UserId is null or UserId = @UserId) 
	                        and (@LocationId is null or HostPlus_Locations.RecId in (select * from dbo.Split(@LocationId,default)))
                        Order By ScreeningArea";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override IEnumerable<IdNameEntity> ObjectInitializer(List<DataRow> rows)
        {
            return rows.Select(r => new IdNameEntity
                {
                    Id = r["RecId"].TryParse<long>(),
                    Name = r["ScreeningArea"].ToString()
                });
        }
    }
}