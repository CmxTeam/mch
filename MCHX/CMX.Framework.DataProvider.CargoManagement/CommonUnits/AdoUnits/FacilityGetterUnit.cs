﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Helpers;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class FacilityGetterUnit : CmCoreDatabaseConnector<IEnumerable<FacilityModel>>
    {
        public FacilityGetterUnit(long? locationId, long userId, string filter)
            : base(new CommandParameter("@UserId", userId), 
                   new CommandParameter("@LocationId", locationId), 
                   new CommandParameter("@Filter", filter))
        {
        }

        public override string CommandQuery
        {
            get
            {
                return @"Select distinct HostPlus_locations.RecId, Location, Description as Name, AddressLine, LocationDetails.CCSF,
	                    (Select Top 1 RecId from TSA_Approved_Certified_Screening_Facilities where TSAApprovalNumber = LocationDetails.CCSF) as CCSFId, 
	                    City, LocationDetails.CountryId, StateId, 
	                    ISNULL(LocationDetails.AddressLine,'') + ' ' + ISNULL(LocationDetails.City, '') + ' ' + ISNULL(States.Name,'')
                                    + ' ' + ISNULL(Country.Name,'') + ' ' + ISNULL('','') as Address,	
	                    LocationDetails.Latitude, LocationDetails.Longitude, ManagerId, 
	                    AddressBook.FirstName + ' ' + AddressBook.LastName as SecurityManagerName,	
	                    PostalCode, AddressBook.Email, AddressBook.WorkPhone, AddressBook.MobilePhone, 
	                    StatusId,
	                    case when LocationDetails.StatusId = 520 then 'Active' when LocationDetails.StatusId = 521 then 'InActive' else '' end as Status,
	                    (select count(0) from fsp.Devices d where d.LocationId = HostPlus_Locations.RecId) TotalDevices,

	                    (Select Count(0)
                                    from AddressBook as Screeners
                                    Inner join ScreenerCertifications on ScreenerCertifications.ScreenerId = Screeners.Id
                                    inner join AppUserLocations on AppUserLocations.UserId = Screeners.Id 
                                    Inner join CertificationTypes on CertificationTypes.RecId = ScreenerCertifications.CertificationTypeId
                                    inner join ScreeningMethods on ScreeningMethods.RecId = CertificationTypes.ScreeningMethodId       
                                    where AppUserLocations.LocationId = HostPlus_Locations.RecId
                                    and AppUserLocations.ApplicationId = 3
                                    and ScreenerCertifications.ValidTo >= getutcdate()) as ScreenersCount

                    From HostPlus_locations
                    Inner Join AppUserLocations on AppUserLocations.LocationId = HostPlus_locations.RecId
                    Left Join LocationDetails on LocationDetails.LocationId = HostPlus_Locations.RecId
                    Left Join AddressBook on LocationDetails.ManagerId = AddressBook.ID
                    Left Join [Profile].[States] as States  on LocationDetails.StateId = States.[RecID]
                    Left Join [Profile].[Countries] as Country on States.CountryID = Country.RecID
                    where PGDBEnabled = 1 
                    and AppUserLocations.UserId = @UserId 
                    and (@LocationId is null or HostPlus_Locations.RecId = @LocationId)
                    and (@Filter is null or LOWER(Location) LIKE LOWER('%'+ @Filter + '%')) 
                    Order By Location";
            }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override IEnumerable<FacilityModel> ObjectInitializer(List<DataRow> rows)
        {            
            return rows.Select(row => new FacilityModel
            {
                Id = Convert.ToInt64(row["RecId"]),
                Name = row["Name"].ToString(),
                Location = row["Location"].ToString(),
                AddressLine = row["AddressLine"].ToString(),
                Address = row["Address"].ToString(),
                CCSF = row["CCSF"].ToString(),
                CCSFId = row["CCSFId"].TryParse<long?>(),
                City = row["City"].ToString(),
                CountryId = row["CountryId"].TryParse<long?>(),
                StateId = row["StateId"].TryParse<long?>(),
                Latitude = row["Latitude"].TryParse<decimal?>(),
                Longitude = row["Longitude"].TryParse<decimal?>(),                
                ManagerId = row["ManagerId"].TryParse<long?>(),
                SecurityManagerName = row["SecurityManagerName"].ToString(),
                PostalCode = row["PostalCode"].ToString(),
                Email = row["Email"].ToString(),
                WorkPhone = row["WorkPhone"].ToString(),
                MobilePhone = row["MobilePhone"].ToString(),
                StatusId = row["StatusId"].TryParse<long?>(),
                Status = row["Status"].ToString(),
                TotalDevices = row["TotalDevices"].TryParse<int>(),
                ScreenersCount = row["ScreenersCount"].TryParse<int>()
            }).ToList();
        }
    }
}
