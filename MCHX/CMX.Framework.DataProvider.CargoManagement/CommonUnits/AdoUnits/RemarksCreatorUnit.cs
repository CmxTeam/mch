﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class RemarksCreatorUnit : CmCoreDatabaseConnector<bool>
    {
        private string _connectionString;

        public RemarksCreatorUnit(string connectionString, string shipmentNumber, string gateway, string remark, long actionId, string userName, long statusId)
            : base(new CommandParameter("@HouseBillNo", shipmentNumber),
                new CommandParameter("@Gateway", gateway),
                new CommandParameter("@Description", remark),
                new CommandParameter("@ActionID", actionId),
                new CommandParameter("@UserID", userName),
                new CommandParameter("@StatusID", statusId))
        {
            _connectionString = connectionString;
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }

        public override string CommandQuery
        {
            get
            {
                return @" Declare @Origin nvarchar(3)
                          Declare @Destination nvarchar(3)
                    Select @Origin = Origin, @Destination = Destination From CMXGlobalDB.fsp.Queue where HousebillNo = @HouseBillNo

                    INSERT INTO Remarks(Reference, [Description], UserID, Gateway, ActionID, Part, StatusID) 
                    VALUES (UPPER(@HouseBillNo), UPPER(@Description), UPPER(@UserID), UPPER(@Gateway), @ActionID, '', @StatusID)

                    INSERT INTO HostPlus_Remarks(AirbillNo, Remark, Status, Gateway) 
                    VALUES (UPPER(@HouseBillNo), UPPER(@Description), 0, UPPER(@Gateway))

                    INSERT INTO CMXGlobalDB.dbo.Remarks(Reference, [Description], UserID, Gateway, ActionID, Part, StatusID) 
                    VALUES (UPPER(isnull(@Origin, '???') + '-' + @HouseBillNo + '-' + isnull(@Destination, '???')), UPPER(@Description), UPPER(@UserID), UPPER(@Gateway), @ActionID, '', @StatusID);";
            }
        }

        public override string ConnectionString
        {
            get { return _connectionString; }
        }
    }
}
