﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;
using CMX.Framework.DataProvider.Helpers;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class DeviceCreatorUnit : CmCoreDatabaseConnector<Pair<bool, string>>
    {
        public DeviceCreatorUnit(long userId, DeviceModel device)
            : base(new CommandParameter("@Id", device.Id),
                new CommandParameter("@SerialNumber", device.SerialNumber),
                new CommandParameter("@LocationId", device.LocationId),
                new CommandParameter("@CertifiedDeviceId", device.CertifiedDeviceId),
                new CommandParameter("@DeviceBarcode", device.DeviceBarcode),
                new CommandParameter("@WarehouseLocationId", device.WarehouseLocationId),
                new CommandParameter("@StatusId", device.StatusId),
                new CommandParameter("@UserId", userId))
        {
        }

        public override Pair<bool, string> ObjectInitializer(List<DataRow> rows)
        {
            return new Pair<bool, string> { First = rows[0][0].TryParse<bool>(), Second = rows[0][1].ToString() };
        }

        public override string CommandQuery
        {
            get { return "CMXNewCreateDevice"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }
    }
}