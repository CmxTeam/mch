﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class HPLocationListCreator : CmCoreDatabaseConnector<IEnumerable<IdNameEntity>>
	{
        public HPLocationListCreator(string applicationName, long? userId)
            : base(new CommandParameter("@AppName", applicationName), new CommandParameter("@UserId", userId))
        { }
		public override string CommandQuery
		{
			get
			{
                return @"SELECT distinct HostPlus_Locations.RecId, HostPlus_Locations.Location FROM HostPlus_Locations
                         LEFT JOIN AppUserLocations ON HostPlus_Locations.RecId = AppUserLocations.LocationId
                         LEFT JOIN Applications ON AppUserLocations.ApplicationId = Applications.Id AND Applications.Name = @AppName
                         WHERE (@UserId is null or AppUserLocations.UserId = @UserId) and (@AppName is null or Applications.Name = @AppName)";				
			}
		}

		public override string ConnectionString
		{
			get { return GlobalConnectionString; }
		}

		public override IEnumerable<IdNameEntity> ObjectInitializer(List<DataRow> rows)
		{
			return rows.Select(s => new IdNameEntity
			{
				Id = Convert.ToInt64(s["RecId"]),
				Name = (s["Location"] as string)
			});
		}		
	}
}