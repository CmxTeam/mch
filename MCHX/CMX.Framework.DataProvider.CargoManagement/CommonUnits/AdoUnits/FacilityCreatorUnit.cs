﻿using System.Collections.Generic;
using System.Data;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.DataProviders;
using CommandType = CMX.Framework.DataProvider.DataProviders.CommandType;

namespace CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits
{
    public class FacilityCreatorUnit : CmCoreDatabaseConnector<bool>
    {
        public FacilityCreatorUnit(long userId, FacilityModel facility)
            : base(new CommandParameter("@Id", facility.Id),
                new CommandParameter("@Location", facility.Location),
                new CommandParameter("@Name", facility.Name),
                new CommandParameter("@AddressLine", facility.AddressLine),
                new CommandParameter("@CCSFId", facility.CCSFId),
                new CommandParameter("@CountryId", facility.CountryId),
                new CommandParameter("@StateId", facility.StateId),
                new CommandParameter("@City", facility.City),
                new CommandParameter("@PostalCode", facility.PostalCode),
                new CommandParameter("@Latitude", facility.Latitude),
                new CommandParameter("@Longitude", facility.Longitude),
                new CommandParameter("@ManagerId", facility.ManagerId),
                new CommandParameter("@StatusId", facility.StatusId),
                new CommandParameter("@UserId", userId))
        {
        }

        public override string CommandQuery
        {
            get { return "CMXNewCreateFacility"; }
        }

        public override string ConnectionString
        {
            get { return GlobalConnectionString; }
        }

        public override CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public override bool ObjectInitializer(List<DataRow> rows)
        {
            return true;
        }
    }
}
