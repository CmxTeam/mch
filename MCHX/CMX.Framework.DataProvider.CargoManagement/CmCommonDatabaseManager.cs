﻿using System.Collections.Generic;
using CMX.Framework.DataProvider.CargoManagement.CommonUnits.AdoUnits;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.CargoManagement.LiveScreening.AdoUnits;
using CMX.Framework.DataProvider.DataProviders;
using CMX.Framework.DataProvider.Entities.Authentication;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement
{
    public class CmCommonDatabaseManager
    {
        public IEnumerable<PortModel> GetPorts()
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new PortsCreator());
        }
        public IdNameEntity GetLocation(string gateway)
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new HPLocationCreator(gateway));
        }
        public IEnumerable<IdNameEntity> GetHostPlusLocationList(long? userId = null, string applicationName = null)
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new HPLocationListCreator(applicationName, userId));
        }
        public IEnumerable<IdNameEntity> GetWarehouseLocations(long userId, long applicationId, string locationId)
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new WarehouseLocationsGetterUnit(userId, applicationId, locationId));
        }
        public Pair<bool, string> CreateDevice(long userId, DeviceModel device)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new DeviceCreatorUnit(userId, device));
        }
        public IEnumerable<IdNameEntity> GetCertifiedDevices()
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new CertifiedDevicesGetter());
        }
        public DeviceModel GetDeviceById(long id)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new DeviceGetterById(id));
        }
        public IEnumerable<Pair<string, Location>> GetHawbNumbersAndLocationsByScreeningIds(IEnumerable<long> ids)
        {
            var connectorExecutor = new ConnectorExecutor();
            return connectorExecutor.Execute(() => new GetHawbNumbersAndLocationsByScreeningIds(ids));
        }
        public bool AddHawbRemark(IEnumerable<Pair<string, Location>> hawbs, string remark, long actionId, string userName, long statusId)
        {
            var connectorExecutor = new ConnectorExecutor();
            foreach (var hawb in hawbs)
            {
                var localConnection = GetLocalConnectionString(connectorExecutor, hawb.Second.Id);
                connectorExecutor.Execute(() => new RemarksCreatorUnit(localConnection, hawb.First, hawb.Second.LocationName, remark, actionId, userName, statusId));
            }
            return true;
        }
        public bool ClearAlarm(IEnumerable<Pair<string, Location>> data, string remark, string userName)
        {            
            var connectorExecutor = new ConnectorExecutor();
            foreach (var hawb in data)
            {
                var localConnection = GetLocalConnectionString(connectorExecutor, hawb.Second.Id);
                connectorExecutor.Execute(() => new AlarmCreatorUnit(localConnection, hawb.First, hawb.Second.LocationName, remark, userName));
                connectorExecutor.Execute(() => new ProcessFSP(hawb.Second.WebConnectionName, hawb.First));                                
            }
            return true;
        }

        public bool UpdateReference(IEnumerable<long> ids, string newShipmentNumber, string userName)
        {
            var connectorExecutor = new ConnectorExecutor();
            connectorExecutor.Execute(() => new ReferenceUpdaterUnit(ids, newShipmentNumber, userName));
            return true;
        }
        public bool UpdateSlac(IEnumerable<Pair<string, Location>> data, int slac, string userName)
        {
            var connectorExecutor = new ConnectorExecutor();
            foreach (var hawb in data)
            {
                var localConnection = GetLocalConnectionString(connectorExecutor, hawb.Second.Id);
                connectorExecutor.Execute(() => new SlacUpdaterUnit(localConnection, hawb.First, slac, hawb.Second.LocationName, userName));
            }
            return true;
        }
        private string GetLocalConnectionString(ConnectorExecutor executor, long locationId)
        {
            return executor.Execute(() => new GetLocalDatabaseConnection(locationId));
        }
        public IEnumerable<CountryModel> GetCountries(long? countryId = null)
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new GetCountries(countryId));
        }
        public IEnumerable<StateModel> GetStates(long? stateId = null)
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new GetStates(stateId));
        }
        public IEnumerable<AddressbookAccountModel> GetManagers(long? managerId = null)
        {
            var _connectorExecutor = new ConnectorExecutor();
            return _connectorExecutor.Execute(() => new GetManagers(managerId));
        }

    }
}