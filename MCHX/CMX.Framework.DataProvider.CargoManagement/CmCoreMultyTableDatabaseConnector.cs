﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement
{
    public abstract class CmCoreMultyTableDatabaseConnector<T> : AdoNetMultyTableDatabaseConnector<T>
    {       
        public CmCoreMultyTableDatabaseConnector()
        {

        }

        public CmCoreMultyTableDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }        
    }
}