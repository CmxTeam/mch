﻿using System.Configuration;
using CMX.Framework.DataProvider.DataProviders;

namespace CMX.Framework.DataProvider.CargoManagement
{
    public abstract class CmCoreDatabaseConnector<T> : AdoNetDatabaseConnector<T>
    {
        public CmCoreDatabaseConnector()
        { 
        
        }

        public CmCoreDatabaseConnector(params CommandParameter[] parameters)
            : base(parameters)
        {

        }

        public override string GlobalConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CMXGlobalDB"].ConnectionString; }
        }        
    }
}