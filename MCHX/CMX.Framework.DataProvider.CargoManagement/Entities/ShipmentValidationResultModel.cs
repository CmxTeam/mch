﻿namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
	public class ShipmentValidationResultModel
	{
		public bool IsOriginValid { get; set; }
		public bool IsDestinationValid { get; set; }
	}
}