﻿using System;

namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
    public class ScanTransactionModel
    {
        public string ReferenceId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}