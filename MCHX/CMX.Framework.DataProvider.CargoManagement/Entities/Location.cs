﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
    public class Location : IdEntity
    {
        public string LocationName { get; set; }
        public string WebConnectionName { get; set; }
    }
}