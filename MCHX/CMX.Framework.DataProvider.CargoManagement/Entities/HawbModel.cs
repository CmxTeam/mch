﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
    public class HawbModel : OriginDestinationEntity
    {
        public int TotalPieces { get; set; }
        public int TotalSlac { get; set; }
        public string TotalWeight { get; set; }
        public int ScannedSlac { get; set; }
        public string GoodsDescription { get; set; }
        public PartnerModel Shipper { get; set; }
        public PartnerModel Consignee { get; set; }
    }
}