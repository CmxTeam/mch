﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
    public class PartnerModel : IdNameEntity
    {        
        public string Address { get; set; }
        public string City { get; set; }
        public string Postal { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
    }
}