﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
    public class FacilityModel : IdNameEntity
    {
        public string Location { get; set; }
        public string AddressLine { get; set; }
        public string Address { get; set; }
        public string CCSF { get; set; }
        public long? CCSFId { get; set; }
        public string City { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }        
        public long? ManagerId { get; set; }
        public string SecurityManagerName { get; set; } 
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public long? StatusId { get; set; }
        public string Status { get; set; }
        public int TotalDevices { get; set; }
        public int ScreenersCount { get; set; }
    }
}