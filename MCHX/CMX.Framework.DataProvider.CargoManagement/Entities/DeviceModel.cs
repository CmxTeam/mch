﻿using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.Entities
{
    public class DeviceModel : IdNameEntity
    {
        public string SerialNumber { get; set; }
        public string DeviceType { get; set; }
        public string DeviceBarcode { get; set; }
        public string UserHolder { get; set; }
        public long? LocationId { get; set; }
        public bool InUse { get; set; }
        public long CertifiedDeviceId { get; set; }
        public int StatusId { get; set; }
        public long? WarehouseLocationId { get; set; }
    }
}