﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using CMX.Framework.DataProvider.CargoManagement.Entities;
using CMX.Framework.DataProvider.Controllers;
using CMX.Framework.DataProvider.Entities.Authentication;
using CMX.Framework.DataProvider.Entities.Common;

namespace CMX.Framework.DataProvider.CargoManagement.Controllers
{
    [EnableCors("*", "*", "*")]
    public abstract class CmCommonController : CommonController
    {        
        public IEnumerable<PortModel> GetPorts()
        {
            var manager = new CmCommonDatabaseManager();
            return manager.GetPorts();
        }

        [HttpGet]
        public DataContainer<IdNameEntity> GetLocation(string gateway)
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetLocation(gateway));
        }

        [HttpGet]
        public DataContainer<List<IdNameEntity>> GetHostPlusLocationList()
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetHostPlusLocationList().ToList());
        }

		[Authorize]
		[HttpGet]
		public DataContainer<List<IdNameEntity>> GetHostPlusLocationListForUSer(string applicationName)
		{
			var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetHostPlusLocationList(ApplicationUser.UserId, applicationName).ToList());
		}

        [Authorize]
        [HttpPost]
        public DataContainer<Pair<bool, string>> CreateDevice(DeviceModel device)
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.CreateDevice(ApplicationUser.UserId, device));
        }

        [Authorize]
        [HttpGet]
        public DataContainer<DeviceModel> GetDeviceById(long id)
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetDeviceById(id));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> AddRemark(RemarkUpdater<string> remark)
        {
            var manager = new CmCommonDatabaseManager();
            var data = manager.GetHawbNumbersAndLocationsByScreeningIds(remark.Ids);
            return MakeTransactional(() => manager.AddHawbRemark(data, remark.Content, 105, ApplicationUser.UserName, 56));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> UpdateSlac(RemarkUpdater<int> slac)
        {
            var manager = new CmCommonDatabaseManager();
            var data = manager.GetHawbNumbersAndLocationsByScreeningIds(slac.Ids);
            return MakeTransactional(() => manager.UpdateSlac(data, slac.Content, ApplicationUser.UserName));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> ClearAlarm(RemarkUpdater<string> alarm)
        {
            var manager = new CmCommonDatabaseManager();
            var data = manager.GetHawbNumbersAndLocationsByScreeningIds(alarm.Ids);
            return MakeTransactional(() => manager.ClearAlarm(data, alarm.Content, ApplicationUser.UserName));
        }

        [Authorize]
        [HttpPost]
        public DataContainer<bool> UpdateReference(RemarkUpdater<string> reference)
        {
            var manager = new CmCommonDatabaseManager();            
            return MakeTransactional(() => manager.UpdateReference(reference.Ids, reference.Content, ApplicationUser.UserName));
        }

        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<CountryModel>> GetCountries(long? countryId = null)
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetCountries(countryId));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<StateModel>> GetStates(long? countryId = null)
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetStates(countryId));
        }
        [HttpGet]
        [Authorize]
        public DataContainer<IEnumerable<AddressbookAccountModel>> GetManagers(long? managerId = null)
        {
            var manager = new CmCommonDatabaseManager();
            return MakeTransactional(() => manager.GetManagers(managerId));
        }
    }
}
