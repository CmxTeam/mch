﻿using System.Collections.Generic;

namespace DataExporter.Excel
{
    public class ExcelData
    {        
        public List<string> Headers { get; set; }
        public List<List<string>> DataRows { get; set; }
        public string SheetName { get; set; }

        public ExcelData()
        {            
            Headers = new List<string>();
            DataRows = new List<List<string>>();
        }
    }
}
