﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DataExporter.Excel
{
    public class ExcelManager
    {        
        public byte[] GenerateExcel(ExcelData data)
        {
            return GenerateExcel(new List<ExcelData> { data });
        }
        public byte[] GenerateDummyExcel()
        {
            return GenerateExcel(new ExcelData() {SheetName = "Empty"});
        }
        public byte[] GenerateExcel(IEnumerable<ExcelData> data)
        {
            if (data == null || (data != null && !data.Any()))
                return GenerateDummyExcel();

            var stream = new MemoryStream();
            var document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook);

            var workbookpart = document.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();
            var sheets = document.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());
            
            foreach (var item in data)
            {
                var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = CreateWorksheet(item);

                sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                uint sheetId = 1;
                if (sheets.Elements<Sheet>().Any())                
                    sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;                
                
                var sheet = new Sheet()
                {
                    Id = document.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = sheetId,
                    Name = string.IsNullOrEmpty(item.SheetName) ? ("Sheet" + sheetId) : item.SheetName
                };

                sheets.Append(sheet);
                workbookpart.Workbook.Save();
            }
            document.Close();
            return stream.ToArray();
        }
       
        private Worksheet CreateWorksheet(ExcelData data)
        {
            var sheetData = new SheetData();

            uint rowIdex = 0;
            var row = new Row { RowIndex = ++rowIdex };
            sheetData.AppendChild(row);
            var cellIdex = 0;

            foreach (var header in data.Headers)
            {
                row.AppendChild(CreateTextCell(ColumnLetter(cellIdex++), rowIdex, header ?? string.Empty));
            }

            foreach (var rowData in data.DataRows)
            {
                cellIdex = 0;
                row = new Row { RowIndex = ++rowIdex };
                sheetData.AppendChild(row);
                foreach (var callData in rowData)
                {
                    var cell = CreateTextCell(ColumnLetter(cellIdex++), rowIdex, callData ?? string.Empty);
                    row.AppendChild(cell);
                }
            }
            var workSheet = new Worksheet(sheetData);

            return workSheet;
        }

        public void ExtractDataFromExcel(string base64, IEnumerable<ExtractSheetInfo> sheetsToExtract)
        {
            if (base64.StartsWith("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"))
                base64 = base64.Replace("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,", "");

            var bytes = Convert.FromBase64String(base64);
            var stream = new MemoryStream(bytes);
            var document = SpreadsheetDocument.Open(stream, false);

            var workbookPart = document.WorkbookPart;

            foreach (Sheet sheet in document.WorkbookPart.Workbook.Sheets)
            {
                var sheetInfo = sheetsToExtract.SingleOrDefault(sh => sh.SheetName == sheet.Name);
                if (sheetInfo == null) continue;

                var worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                var worksheet = worksheetPart.Worksheet;
                var rows = worksheet.Descendants<Row>();
                var t = rows;
            }
        }

        private string ColumnLetter(int intCol)
        {
            var intFirstLetter = ((intCol) / 676) + 64;
            var intSecondLetter = ((intCol % 676) / 26) + 64;
            var intThirdLetter = (intCol % 26) + 65;

            var firstLetter = (intFirstLetter > 64)
                ? (char)intFirstLetter : ' ';
            var secondLetter = (intSecondLetter > 64)
                ? (char)intSecondLetter : ' ';
            var thirdLetter = (char)intThirdLetter;

            return string.Concat(firstLetter, secondLetter,
                thirdLetter).Trim();
        }

        private Cell CreateTextCell(string header, UInt32 index,
            string text)
        {
            var cell = new Cell
            {
                DataType = CellValues.InlineString,
                CellReference = header + index
            };

            var istring = new InlineString();
            var t = new Text { Text = text };
            istring.AppendChild(t);
            cell.AppendChild(istring);
            return cell;
        }
    }
}
