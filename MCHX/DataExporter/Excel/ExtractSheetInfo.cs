﻿
namespace DataExporter.Excel
{
    public class ExtractSheetInfo
    {
        public string SheetName { get; set; }
        public uint StartFromRow { get; set; }
    }
}